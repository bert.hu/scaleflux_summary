```c++
bool bsp_platform::monitor_parity_buffer()
{
    BM_RSC_STATUS_20_U bm_rsc_status_20;
    bm_rsc_status_20.data = BM_CSR_AUTO_RD(BM_RSC_STATUS_20);
    u32 vp2_ocpy_cnt = bm_rsc_status_20.bits.vp2_ocpy_cnt; // num of 2K (1 pool unit is 2K)
    BM_POOL_CFG_U bm_pool_cfg;
    bm_pool_cfg.data = BM_CSR_AUTO_RD(BM_POOL_CFG);
    u32 vp2_total_cnt = bit_count(bm_pool_cfg.bits.vpool_map_2) * 256; // num of 2K (pool size is 512K, 1 pool unit is 2K)
    /*
    raid rebuild will be safe if remaining parity buffer bigger than 0xe8 
        (232 * 2kb = 64KB x 6 streams + 80KB for rebuild)
    */
    return ((vp2_total_cnt - vp2_ocpy_cnt) > 0xe8);
}

```

```
[昨天 16:23] Alex Gu
看下parity buffer usage

[昨天 16:24] Alex Gu
mem32 0x231800e4 1

[昨天 16:32] Tim Lin
XScope> read 0x231800e4 1
0x231800e4: bm_rsc_status_20: 0x00000180  bm resource status counters 20.        
vp2_ocpy_cnt            [ 11:  0]:      180        
vp3_ocpy_cnt            [ 23: 12]:      0

[昨天 16:35] Jeremy Li
这应该没满？算出来只占了768K=0x180(384)*2k
```

```
fcq sq head tail  cq head tail
void arch_get_fcq_ptr(u32 qid, u16 *qptr)
{
    FCQ_Q_PTR_U ptr;
    ACE_CSR_AUTO_WR(FCQ_Q_SEL, qid);
    ptr.data = ACE_CSR_AUTO_RD(FCQ_Q_PTR);
    
    if (qptr != NULL) {
        qptr[0] = (u16)ptr.bits.sq_hd;
        qptr[1] = (u16)ptr.bits.sq_tail;
        qptr[2] = (u16)ptr.bits.cq_hd;
        qptr[3] = (u16)ptr.bits.cq_tail;
    }
}

debug_data->ck_w2p_fcq[0]            = (u8)0;
debug_data->ck_w2p_fcq[1]            = (u8)1;
debug_data->ck_w2p_fcq[2]            = (u8)2;
debug_data->ck_w2p_fcq[3]            = (u8)3;
0x03020100
```

```
[2022-11-29 13:03:04] check_panic_dump_info
[2022-11-29 13:03:04] read nor addr 1141000, got magic st ffffffff
[2022-11-29 13:03:04] no valid panic dump, type 0
[2022-11-29 13:03:04] read nor addr 1141100, got magic st ffffffff
[2022-11-29 13:03:04] no valid panic dump, type 1
```

