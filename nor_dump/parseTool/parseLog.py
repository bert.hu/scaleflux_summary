import struct
import os
import re

DEBUG_BUFFER = 1

# 定义各个数据结构的格式字符串
pf_root_data_format = 'IHHIQQQQQQQQ'


# 定义pf_debug_data的格式字符串
pf_debug_data_format = (
    # 1st row
    'HHHHHHHHHHHHHHHH'
    # 2nd row
    'IIIIIIII'
    # 3rd-4th rows
    '64B'
    # 5th row
    'Q'
    '24B'
    # 6th row
    '8B'
    '8B'
    '8B'
    'HHI'
    # 7th row
    '32B'
    # 8th-9th row
    'B'
    'B'
    'B'
    'B'
    '12B'
    '12I'
    # 10th row
    '12B'
    'B'
    'B'
    'H'
    'H'
    'H'
    'Q'
    'I'
    # 11th row
    '3I'
    'Q'
    'B'
    'B'
    'B'
    'B'
    'I'
    'I'
    # 12th row
    '4B'
    '4B'
    '4B'
    '4B'
    '4B'
    '4B'
    '4B'
    '4B'
    # 13th row
    'Q'
    'Q'
    'Q'
    'Q'
    # 14th row
    '8Q'
    'I'
    'I'
    'I'
    'I'
    'I'
    'B'
    'B'
    'B'
    'B'
    '6I'
    '6I'
    '227I'
)

# 定义pf_nor_data的格式字符串
pf_nor_data_format = f'{pf_root_data_format}IQQ'

# log文件路径
file_path = r'D:\scaleflux\05_summary\scaleflux_summary\nor_dump\parseTool\nor_dump_bert1.log'

# 获取文件大小
file_size = os.path.getsize(file_path)
print("文件大小：", file_size, "字节")

# 打开文本文件并以文本模式读取
# 定义正则表达式模式，匹配字段
pattern = re.compile(r'(\w+)')

# 打开文本文件并以文本模式读取
with open(file_path, 'r') as file:
    # 读取文本数据
    text_data = file.read()

# 使用正则表达式模式匹配字段
fields = re.findall(pattern, text_data)

# 解析pf_nor_data的数据
pf_cnt = int(fields[0], 16)
nor_sub_sector_idx = int(fields[1], 16) & 0xFFFF  # 提取低16位
pf_dump_time = (int(fields[1], 16) >> 16) & 0xFFFF  # 提取高16位并右移16位
current_pf_sblock = int(fields[2], 16)
high_bits = int(fields[4], 16)  # 将高位部分转换为整数
low_bits = int(fields[3], 16)  # 将低位部分转换为整数
summary_data_pba = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[6], 16)  # 将高位部分转换为整数
low_bits = int(fields[5], 16)  # 将低位部分转换为整数
last_written_pba = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[8], 16)  # 将高位部分转换为整数
low_bits = int(fields[7], 16)  # 将低位部分转换为整数
user_data_start_pba = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[10], 16)  # 将高位部分转换为整数
low_bits = int(fields[9], 16)  # 将低位部分转换为整数
user_data_end_pba = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[12], 16)  # 将高位部分转换为整数
low_bits = int(fields[11], 16)  # 将低位部分转换为整数
parity_data_start_pba = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[14], 16)  # 将高位部分转换为整数
low_bits = int(fields[13], 16)  # 将低位部分转换为整数
parity_data_end_pba = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[16], 16)  # 将高位部分转换为整数
low_bits = int(fields[15], 16)  # 将低位部分转换为整数
meta_data_start_pba = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[18], 16)  # 将高位部分转换为整数
low_bits = int(fields[17], 16)  # 将低位部分转换为整数
meta_data_end_pba = (high_bits << 32) | low_bits  # 合并高位和低位
crc = int(fields[19], 16)
high_bits = int(fields[21], 16)  # 将高位部分转换为整数
low_bits = int(fields[20], 16)  # 将低位部分转换为整数
signature1 = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[23], 16)  # 将高位部分转换为整数
low_bits = int(fields[22], 16)  # 将低位部分转换为整数
signature2 = (high_bits << 32) | low_bits  # 合并高位和低位

# 打印每个字段的值
print("\n====== pf root data info ======")
print("pf_cnt:", hex(pf_cnt))
print("nor_sub_sector_idx:", hex(nor_sub_sector_idx))
print("pf_dump_time:", hex(pf_dump_time))
print("current_pf_sblock:", hex(current_pf_sblock))
print("summary_data_pba:", hex(summary_data_pba))
print("last_written_pba:", hex(last_written_pba))
print("user_data_start_pba:", hex(user_data_start_pba))
print("user_data_end_pba:", hex(user_data_end_pba))
print("parity_data_start_pba:", hex(parity_data_start_pba))
print("parity_data_end_pba:", hex(parity_data_end_pba))
print("meta_data_start_pba:", hex(meta_data_start_pba))
print("meta_data_end_pba:", hex(meta_data_end_pba))
print("crc:", hex(crc))
print("pf dump complete signature:", hex(signature1))
print("pf restore complete signature:", hex(signature2))

# 解析pf_debug_data的数据
# 使用正则表达式模式匹配字段
start_offset = 2304
if DEBUG_BUFFER == 1:
    print(text_data[start_offset:])
fields = re.findall(pattern, text_data[start_offset:])
# 第一行的解析
pf_cnt = int(fields[0], 16) & 0xFFFF  # 提取低16位
pf_dump_state = (int(fields[0], 16) >> 16) & 0xFFFF  # 提取高16位并右移16位
flush_meta_state = int(fields[1], 16) & 0xFFFF  # 提取低16位
flush_summary_state = (int(fields[1], 16) >> 16) & 0xFFFF  # 提取高16位并右移16位
flush_user_data_state = int(fields[2], 16) & 0xFFFF  # 提取低16位
flush_parity_state = (int(fields[2], 16) >> 16) & 0xFFFF  # 提取高16位并右移16位
parity_padding_state = int(fields[3], 16) & 0xFFFF  # 提取低16位
flush_stream = (int(fields[3], 16) >> 16) & 0xFFFF  # 提取高16位并右移16位
flush_index = int(fields[4], 16) & 0xFFFF  # 提取低16位
flush_page_type = (int(fields[4], 16) >> 16) & 0xFFFF  # 提取高16位并右移16位
chunk_cnt = int(fields[5], 16) & 0xFFFF  # 提取低16位
b2n_sent = (int(fields[5], 16) >> 16) & 0xFFFF  # 提取高16位并右移16位
b2n_received = int(fields[6], 16) & 0xFFFF  # 提取低16位
w2p_sent = (int(fields[6], 16) >> 16) & 0xFFFF  # 提取高16位并右移16位
w2p_received = int(fields[7], 16) & 0xFFFF  # 提取低16位
patch_free_fifo_cnt = (int(fields[7], 16) >> 16) & 0xFFFF  # 提取高16位并右移16位
print("\n====== pf debug data in nor dump info ======")
print("pf_cnt:", hex(pf_cnt))
print("pf_dump_state:", hex(pf_dump_state))
print("flush_meta_state:", hex(flush_meta_state))
print("flush_summary_state:", hex(flush_summary_state))
print("flush_user_data_state:", hex(flush_user_data_state))
print("flush_parity_state:", hex(flush_parity_state))
print("parity_padding_state:", hex(parity_padding_state))
print("flush_stream:", hex(flush_stream))
print("flush_index:", hex(flush_index))
print("flush_page_type:", hex(flush_page_type))
print("chunk_cnt:", hex(chunk_cnt))
print("b2n_sent:", hex(b2n_sent))
print("b2n_received:", hex(b2n_received))
print("w2p_sent:", hex(w2p_sent))
print("w2p_received:", hex(w2p_received))
print("patch_free_fifo_cnt:", hex(patch_free_fifo_cnt))

start_offset += 72
if DEBUG_BUFFER == 1:
    print(text_data[start_offset:])
fields = re.findall(pattern, text_data[start_offset:])
# 第二行的解析
event_handled_flags = int(fields[0], 16)
pf_event_start_timestamp = int(fields[1], 16)
pf_dump_task_start_timestamp = int(fields[2], 16)
pf_hw_abort_start_timestamp = int(fields[3], 16)
pf_hw_abort_complete_timestamp = int(fields[4], 16)
pf_dump_wait_all_cmd_done_timestamp = int(fields[5], 16)
pf_dump_task_complete_timestamp = int(fields[6], 16)
recovery_window_size = int(fields[7], 16)

print("event_handled_flags:", hex(event_handled_flags))
print("pf_event_start_timestamp:", hex(pf_event_start_timestamp))
print("pf_dump_task_start_timestamp:", hex(pf_dump_task_start_timestamp))
print("pf_hw_abort_start_timestamp:", hex(pf_hw_abort_start_timestamp))
print("pf_hw_abort_complete_timestamp:", hex(pf_hw_abort_complete_timestamp))
print("pf_dump_wait_all_cmd_done_timestamp:", hex(pf_dump_wait_all_cmd_done_timestamp))
print("pf_dump_task_complete_timestamp:", hex(pf_dump_task_complete_timestamp))
print("recovery_window_size:", hex(recovery_window_size))

start_offset += 72
if DEBUG_BUFFER == 1:
    print(text_data[start_offset:])
fields = re.findall(pattern, text_data[start_offset:])
# 第三、四行的解析
task_states_0 = int(fields[0], 16)& 0xFF  # 提取低8位
task_states_1 = (int(fields[0], 16) >> 8)& 0xFF  # 提取次低8位
task_states_2 = (int(fields[0], 16) >> 16)& 0xFF  # 提取次高8位
task_states_3 = (int(fields[0], 16) >> 24)& 0xFF  # 提取高8位
task_states_4 = int(fields[1], 16)& 0xFF  # 提取低8位
task_states_5 = (int(fields[1], 16) >> 8)& 0xFF  # 提取次低8位
task_states_6 = (int(fields[1], 16) >> 16)& 0xFF  # 提取次高8位
task_states_7 = (int(fields[1], 16) >> 24)& 0xFF  # 提取高8位
task_states_8 = int(fields[2], 16)& 0xFF  # 提取低8位
task_states_9 = (int(fields[2], 16) >> 8)& 0xFF  # 提取次低8位
task_states_10 = (int(fields[2], 16) >> 16)& 0xFF  # 提取次高8位
task_states_11 = (int(fields[2], 16) >> 24)& 0xFF  # 提取高8位
task_states_12 = int(fields[3], 16)& 0xFF  # 提取低8位
task_states_13 = (int(fields[3], 16) >> 8)& 0xFF  # 提取次低8位
task_states_14 = (int(fields[3], 16) >> 16)& 0xFF  # 提取次高8位
task_states_15 = (int(fields[3], 16) >> 24)& 0xFF  # 提取高8位
task_states_16 = int(fields[4], 16)& 0xFF  # 提取低8位
task_states_17 = (int(fields[4], 16) >> 8)& 0xFF  # 提取次低8位
task_states_18 = (int(fields[4], 16) >> 16)& 0xFF  # 提取次高8位
task_states_19 = (int(fields[4], 16) >> 24)& 0xFF  # 提取高8位
task_states_20 = int(fields[5], 16)& 0xFF  # 提取低8位
task_states_21 = (int(fields[5], 16) >> 8)& 0xFF  # 提取次低8位
task_states_22 = (int(fields[5], 16) >> 16)& 0xFF  # 提取次高8位
task_states_23 = (int(fields[5], 16) >> 24)& 0xFF  # 提取高8位
task_states_24 = int(fields[6], 16)& 0xFF  # 提取低8位
task_states_25 = (int(fields[6], 16) >> 8)& 0xFF  # 提取次低8位
task_states_26 = (int(fields[6], 16) >> 16)& 0xFF  # 提取次高8位
task_states_27 = (int(fields[6], 16) >> 24)& 0xFF  # 提取高8位
task_states_28 = int(fields[7], 16)& 0xFF  # 提取低8位
task_states_29 = (int(fields[7], 16) >> 8)& 0xFF  # 提取次低8位
task_states_30 = (int(fields[7], 16) >> 16)& 0xFF  # 提取次高8位
task_states_31 = (int(fields[7], 16) >> 24)& 0xFF  # 提取高8位
task_states_32 = int(fields[8], 16)& 0xFF  # 提取低8位
task_states_33 = (int(fields[8], 16) >> 8)& 0xFF  # 提取次低8位
task_states_34 = (int(fields[8], 16) >> 16)& 0xFF  # 提取次高8位
task_states_35 = (int(fields[8], 16) >> 24)& 0xFF  # 提取高8位
task_states_36 = int(fields[9], 16)& 0xFF  # 提取低8位
task_states_37 = (int(fields[9], 16) >> 8)& 0xFF  # 提取次低8位
task_states_38 = (int(fields[9], 16) >> 16)& 0xFF  # 提取次高8位
task_states_39 = (int(fields[9], 16) >> 24)& 0xFF  # 提取高8位
task_states_40 = int(fields[10], 16)& 0xFF  # 提取低8位
task_states_41 = (int(fields[10], 16) >> 8)& 0xFF  # 提取次低8位
task_states_42 = (int(fields[10], 16) >> 16)& 0xFF  # 提取次高8位
task_states_43 = (int(fields[10], 16) >> 24)& 0xFF  # 提取高8位
task_states_44 = int(fields[11], 16)& 0xFF  # 提取低8位
task_states_45 = (int(fields[11], 16) >> 8)& 0xFF  # 提取次低8位
task_states_46 = (int(fields[11], 16) >> 16)& 0xFF  # 提取次高8位
task_states_47 = (int(fields[11], 16) >> 24)& 0xFF  # 提取高8位
task_states_48 = int(fields[12], 16)& 0xFF  # 提取低8位
task_states_49 = (int(fields[12], 16) >> 8)& 0xFF  # 提取次低8位
task_states_50 = (int(fields[12], 16) >> 16)& 0xFF  # 提取次高8位
task_states_51 = (int(fields[12], 16) >> 24)& 0xFF  # 提取高8位
task_states_52 = int(fields[13], 16)& 0xFF  # 提取低8位
task_states_53 = (int(fields[13], 16) >> 8)& 0xFF  # 提取次低8位
task_states_54 = (int(fields[13], 16) >> 16)& 0xFF  # 提取次高8位
task_states_55 = (int(fields[13], 16) >> 24)& 0xFF  # 提取高8位
task_states_56 = int(fields[14], 16)& 0xFF  # 提取低8位
task_states_57 = (int(fields[14], 16) >> 8)& 0xFF  # 提取次低8位
task_states_58 = (int(fields[14], 16) >> 16)& 0xFF  # 提取次高8位
task_states_59 = (int(fields[14], 16) >> 24)& 0xFF  # 提取高8位
task_states_60 = int(fields[15], 16)& 0xFF  # 提取低8位
task_states_61 = (int(fields[15], 16) >> 8)& 0xFF  # 提取次低8位
task_states_62 = (int(fields[15], 16) >> 16)& 0xFF  # 提取次高8位
task_states_63 = (int(fields[15], 16) >> 24)& 0xFF  # 提取高8位

print("task_states_0:", hex(task_states_0))
print("task_states_1:", hex(task_states_1))
print("task_states_2:", hex(task_states_2))
print("task_states_3:", hex(task_states_3))
print("task_states_4:", hex(task_states_4))
print("task_states_5:", hex(task_states_5))
print("task_states_6:", hex(task_states_6))
print("task_states_7:", hex(task_states_7))
print("task_states_8:", hex(task_states_8))
print("task_states_9:", hex(task_states_9))
print("task_states_10:", hex(task_states_10))
print("task_states_11:", hex(task_states_11))
print("task_states_12:", hex(task_states_12))
print("task_states_13:", hex(task_states_13))
print("task_states_14:", hex(task_states_14))
print("task_states_15:", hex(task_states_15))
print("task_states_16:", hex(task_states_16))
print("task_states_17:", hex(task_states_17))
print("task_states_18:", hex(task_states_18))
print("task_states_19:", hex(task_states_19))
print("task_states_20:", hex(task_states_20))
print("task_states_21:", hex(task_states_21))
print("task_states_22:", hex(task_states_22))
print("task_states_23:", hex(task_states_23))
print("task_states_24:", hex(task_states_24))
print("task_states_25:", hex(task_states_25))
print("task_states_26:", hex(task_states_26))
print("task_states_27:", hex(task_states_27))
print("task_states_28:", hex(task_states_28))
print("task_states_29:", hex(task_states_29))
print("task_states_30:", hex(task_states_30))
print("task_states_31:", hex(task_states_31))
print("task_states_32:", hex(task_states_32))
print("task_states_33:", hex(task_states_33))
print("task_states_34:", hex(task_states_34))
print("task_states_35:", hex(task_states_35))
print("task_states_36:", hex(task_states_36))
print("task_states_37:", hex(task_states_37))
print("task_states_38:", hex(task_states_38))
print("task_states_39:", hex(task_states_39))
print("task_states_40:", hex(task_states_40))
print("task_states_41:", hex(task_states_41))
print("task_states_42:", hex(task_states_42))
print("task_states_43:", hex(task_states_43))
print("task_states_44:", hex(task_states_44))
print("task_states_45:", hex(task_states_45))
print("task_states_46:", hex(task_states_46))
print("task_states_47:", hex(task_states_47))
print("task_states_48:", hex(task_states_48))
print("task_states_49:", hex(task_states_49))
print("task_states_50:", hex(task_states_50))
print("task_states_51:", hex(task_states_51))
print("task_states_52:", hex(task_states_52))
print("task_states_53:", hex(task_states_53))
print("task_states_54:", hex(task_states_54))
print("task_states_55:", hex(task_states_55))
print("task_states_56:", hex(task_states_56))
print("task_states_57:", hex(task_states_57))
print("task_states_58:", hex(task_states_58))
print("task_states_59:", hex(task_states_59))
print("task_states_60:", hex(task_states_60))
print("task_states_61:", hex(task_states_61))
print("task_states_62:", hex(task_states_62))
print("task_states_63:", hex(task_states_63))

start_offset += 72*2
if DEBUG_BUFFER == 1:
    print(text_data[start_offset:])
fields = re.findall(pattern, text_data[start_offset:])
# 第五行的解析
high_bits = int(fields[1], 16)  # 将高位部分转换为整数
low_bits = int(fields[0], 16)  # 将低位部分转换为整数
task_paused = (high_bits << 32) | low_bits  # 合并高位和低位
admin_cmd_state_nvme_opc_delete_io_sq = int(fields[2], 16)& 0xFF  # 提取低8位
admin_cmd_state_nvme_opc_create_io_sq = (int(fields[2], 16) >> 8)& 0xFF  # 提取次低8位
admin_cmd_state_nvme_opc_get_log_page = (int(fields[2], 16) >> 16)& 0xFF  # 提取次高8位
admin_cmd_state_nvme_opc_delete_io_cq = (int(fields[2], 16) >> 24)& 0xFF  # 提取高8位
admin_cmd_state_nvme_opc_create_io_cq = int(fields[3], 16)& 0xFF  # 提取低8位
admin_cmd_state_nvme_nvme_opc_identify = (int(fields[3], 16) >> 8)& 0xFF  # 提取次低8位
admin_cmd_state_nvme_opc_abort = (int(fields[3], 16) >> 16)& 0xFF  # 提取次高8位
admin_cmd_state_nvme_opc_set_features = (int(fields[3], 16) >> 24)& 0xFF  # 提取高8位
admin_cmd_state_nvme_opc_get_features = int(fields[4], 16)& 0xFF  # 提取低8位
admin_cmd_state_nvme_opc_async_event_request = (int(fields[4], 16) >> 8)& 0xFF  # 提取次低8位
admin_cmd_state_nvme_opc_ns_management = (int(fields[4], 16) >> 16)& 0xFF  # 提取次高8位
admin_cmd_state_nvme_opc_firmware_commit = (int(fields[4], 16) >> 24)& 0xFF  # 提取高8位
admin_cmd_state_nvme_opc_firmware_image_download = int(fields[5], 16)& 0xFF  # 提取低8位
admin_cmd_state_nvme_opc_ns_attachment = (int(fields[5], 16) >> 8)& 0xFF  # 提取次低8位
admin_cmd_state_nvme_opc_format_nvm = (int(fields[5], 16) >> 16)& 0xFF  # 提取次高8位
admin_cmd_state_nvme_opc_query_cap_info = (int(fields[5], 16) >> 24)& 0xFF  # 提取高8位
admin_cmd_state_nvme_opc_change_cap = int(fields[6], 16)& 0xFF  # 提取低8位
admin_cmd_state_nvme_opc_change_provision_cap = (int(fields[6], 16) >> 8)& 0xFF  # 提取次低8位
admin_cmd_state_OPCODE_VU = (int(fields[6], 16) >> 16)& 0xFF  # 提取次高8位
admin_cmd_state_nvme_XX = (int(fields[6], 16) >> 24)& 0xFF  # 提取高8位

print("task_paused:", hex(task_paused))
print("admin_cmd_state_nvme_opc_delete_io_sq:", hex(admin_cmd_state_nvme_opc_delete_io_sq))
print("admin_cmd_state_nvme_opc_create_io_sq:", hex(admin_cmd_state_nvme_opc_create_io_sq))
print("admin_cmd_state_nvme_opc_get_log_page:", hex(admin_cmd_state_nvme_opc_get_log_page))
print("admin_cmd_state_nvme_opc_delete_io_cq:", hex(admin_cmd_state_nvme_opc_delete_io_cq))
print("admin_cmd_state_nvme_opc_create_io_cq:", hex(admin_cmd_state_nvme_opc_create_io_cq))
print("admin_cmd_state_nvme_nvme_opc_identify:", hex(admin_cmd_state_nvme_nvme_opc_identify))
print("admin_cmd_state_nvme_opc_abort:", hex(admin_cmd_state_nvme_opc_abort))
print("admin_cmd_state_nvme_opc_set_features:", hex(admin_cmd_state_nvme_opc_set_features))
print("admin_cmd_state_nvme_opc_get_features:", hex(admin_cmd_state_nvme_opc_get_features))
print("admin_cmd_state_nvme_opc_async_event_request:", hex(admin_cmd_state_nvme_opc_async_event_request))
print("admin_cmd_state_nvme_opc_ns_management:", hex(admin_cmd_state_nvme_opc_ns_management))
print("admin_cmd_state_nvme_opc_firmware_commit:", hex(admin_cmd_state_nvme_opc_firmware_commit))
print("admin_cmd_state_nvme_opc_firmware_image_download:", hex(admin_cmd_state_nvme_opc_firmware_image_download))
print("admin_cmd_state_nvme_opc_ns_attachment:", hex(admin_cmd_state_nvme_opc_ns_attachment))
print("admin_cmd_state_nvme_opc_format_nvm:", hex(admin_cmd_state_nvme_opc_format_nvm))
print("admin_cmd_state_nvme_opc_query_cap_info:", hex(admin_cmd_state_nvme_opc_query_cap_info))
print("admin_cmd_state_nvme_opc_change_cap:", hex(admin_cmd_state_nvme_opc_change_cap))
print("admin_cmd_state_nvme_opc_change_provision_cap:", hex(admin_cmd_state_nvme_opc_change_provision_cap))
print("admin_cmd_state_OPCODE_VU:", hex(admin_cmd_state_OPCODE_VU))

start_offset += 72
if DEBUG_BUFFER == 1:
    print(text_data[start_offset:])
fields = re.findall(pattern, text_data[start_offset:])
# 第六行的解析
smp_switch_stage_0 = int(fields[0], 16)& 0xFF  # 提取低8位
smp_switch_stage_1 = (int(fields[0], 16) >> 8)& 0xFF  # 提取次低8位
smp_switch_stage_2 = (int(fields[0], 16) >> 16)& 0xFF  # 提取次高8位
smp_switch_stage_3 = (int(fields[0], 16) >> 24)& 0xFF  # 提取高8位
smp_switch_stage_4 = int(fields[1], 16)& 0xFF  # 提取低8位
smp_switch_stage_5 = (int(fields[1], 16) >> 8)& 0xFF  # 提取次低8位
smp_switch_stage_6 = (int(fields[1], 16) >> 16)& 0xFF  # 提取次高8位
smp_switch_stage_7 = (int(fields[1], 16) >> 24)& 0xFF  # 提取高8位
active_task_idx_0 = int(fields[2], 16)& 0xFF  # 提取低8位
active_task_idx_1 = (int(fields[2], 16) >> 8)& 0xFF  # 提取次低8位
active_task_idx_2 = (int(fields[2], 16) >> 16)& 0xFF  # 提取次高8位
active_task_idx_3 = (int(fields[2], 16) >> 24)& 0xFF  # 提取高8位
active_task_idx_4 = int(fields[3], 16)& 0xFF  # 提取低8位
active_task_idx_5 = (int(fields[3], 16) >> 8)& 0xFF  # 提取次低8位
active_task_idx_6 = (int(fields[3], 16) >> 16)& 0xFF  # 提取次高8位
active_task_idx_7 = (int(fields[3], 16) >> 24)& 0xFF  # 提取高8位
core_smpmgr_state_0 = int(fields[4], 16)& 0xFF  # 提取低8位
core_smpmgr_state_1 = (int(fields[4], 16) >> 8)& 0xFF  # 提取次低8位
core_smpmgr_state_2 = (int(fields[4], 16) >> 16)& 0xFF  # 提取次高8位
core_smpmgr_state_3 = (int(fields[4], 16) >> 24)& 0xFF  # 提取高8位
core_smpmgr_state_4 = int(fields[5], 16)& 0xFF  # 提取低8位
core_smpmgr_state_5 = (int(fields[5], 16) >> 8)& 0xFF  # 提取次低8位
core_smpmgr_state_6 = (int(fields[5], 16) >> 16)& 0xFF  # 提取次高8位
core_smpmgr_state_7 = (int(fields[5], 16) >> 24)& 0xFF  # 提取高8位
io_clear_flags = int(fields[6], 16)& 0xFFFF  # 提取低16位
b2n_clear_flags = (int(fields[6], 16) >> 16)& 0xFFFF  # 提取高16位
switch_from_state = int(fields[7], 16)

print("smp_switch_stage_0:", hex(smp_switch_stage_0))
print("smp_switch_stage_1:", hex(smp_switch_stage_1))
print("smp_switch_stage_2:", hex(smp_switch_stage_2))
print("smp_switch_stage_3:", hex(smp_switch_stage_3))
print("smp_switch_stage_4:", hex(smp_switch_stage_4))
print("smp_switch_stage_5:", hex(smp_switch_stage_5))
print("smp_switch_stage_6:", hex(smp_switch_stage_6))
print("smp_switch_stage_7:", hex(smp_switch_stage_7))
print("active_task_idx_0:", hex(active_task_idx_0))
print("active_task_idx_1:", hex(active_task_idx_1))
print("active_task_idx_2:", hex(active_task_idx_2))
print("active_task_idx_3:", hex(active_task_idx_3))
print("active_task_idx_4:", hex(active_task_idx_4))
print("active_task_idx_5:", hex(active_task_idx_5))
print("active_task_idx_6:", hex(active_task_idx_6))
print("active_task_idx_7:", hex(active_task_idx_7))
print("core_smpmgr_state_0:", hex(core_smpmgr_state_0))
print("core_smpmgr_state_1:", hex(core_smpmgr_state_1))
print("core_smpmgr_state_2:", hex(core_smpmgr_state_2))
print("core_smpmgr_state_3:", hex(core_smpmgr_state_3))
print("core_smpmgr_state_4:", hex(core_smpmgr_state_4))
print("core_smpmgr_state_5:", hex(core_smpmgr_state_5))
print("core_smpmgr_state_6:", hex(core_smpmgr_state_6))
print("core_smpmgr_state_7:", hex(core_smpmgr_state_7))
print("io_clear_flags:", hex(io_clear_flags))
print("b2n_clear_flags:", hex(b2n_clear_flags))
print("switch_from_state:", hex(switch_from_state))

start_offset += 72
if DEBUG_BUFFER == 1:
    print(text_data[start_offset:])
fields = re.findall(pattern, text_data[start_offset:])
# 第七行的解析
fct_free_cnt_0 = int(fields[0], 16)& 0xFF  # 提取低8位
fct_free_cnt_1 = (int(fields[0], 16) >> 8)& 0xFF  # 提取次低8位
fct_free_cnt_2 = (int(fields[0], 16) >> 16)& 0xFF  # 提取次高8位
fct_free_cnt_3 = (int(fields[0], 16) >> 24)& 0xFF  # 提取高8位
fct_free_cnt_4 = int(fields[1], 16)& 0xFF  # 提取低8位
fct_free_cnt_5 = (int(fields[1], 16) >> 8)& 0xFF  # 提取次低8位
fct_free_cnt_6 = (int(fields[1], 16) >> 16)& 0xFF  # 提取次高8位
fct_free_cnt_7 = (int(fields[1], 16) >> 24)& 0xFF  # 提取高8位
fct_free_cnt_8 = int(fields[2], 16)& 0xFF  # 提取低8位
fct_free_cnt_9 = (int(fields[2], 16) >> 8)& 0xFF  # 提取次低8位
fct_free_cnt_10 = (int(fields[2], 16) >> 16)& 0xFF  # 提取次高8位
fct_free_cnt_11 = (int(fields[2], 16) >> 24)& 0xFF  # 提取高8位
fct_free_cnt_12 = int(fields[3], 16)& 0xFF  # 提取低8位
fct_free_cnt_13 = (int(fields[3], 16) >> 8)& 0xFF  # 提取次低8位
fct_free_cnt_14 = (int(fields[3], 16) >> 16)& 0xFF  # 提取次高8位
fct_free_cnt_15 = (int(fields[3], 16) >> 24)& 0xFF  # 提取高8位
fct_free_cnt_16 = int(fields[4], 16)& 0xFF  # 提取低8位
fct_free_cnt_17 = (int(fields[4], 16) >> 8)& 0xFF  # 提取次低8位
fct_free_cnt_18 = (int(fields[4], 16) >> 16)& 0xFF  # 提取次高8位
fct_free_cnt_19 = (int(fields[4], 16) >> 24)& 0xFF  # 提取高8位
fct_free_cnt_20 = int(fields[5], 16)& 0xFF  # 提取低8位
fct_free_cnt_21 = (int(fields[5], 16) >> 8)& 0xFF  # 提取次低8位
fct_free_cnt_22 = (int(fields[5], 16) >> 16)& 0xFF  # 提取次高8位
fct_free_cnt_23 = (int(fields[5], 16) >> 24)& 0xFF  # 提取高8位
fct_free_cnt_24 = int(fields[6], 16)& 0xFF  # 提取低8位
fct_free_cnt_25 = (int(fields[6], 16) >> 8)& 0xFF  # 提取次低8位
fct_free_cnt_26 = (int(fields[6], 16) >> 16)& 0xFF  # 提取次高8位
fct_free_cnt_27 = (int(fields[6], 16) >> 24)& 0xFF  # 提取高8位
fct_free_cnt_28 = int(fields[7], 16)& 0xFF  # 提取低8位
fct_free_cnt_29 = (int(fields[7], 16) >> 8)& 0xFF  # 提取次低8位
fct_free_cnt_30 = (int(fields[7], 16) >> 16)& 0xFF  # 提取次高8位
fct_free_cnt_31 = (int(fields[7], 16) >> 24)& 0xFF  # 提取高8位

print("fct_free_cnt_0:", hex(fct_free_cnt_0))
print("fct_free_cnt_1:", hex(fct_free_cnt_1))
print("fct_free_cnt_2:", hex(fct_free_cnt_2))
print("fct_free_cnt_3:", hex(fct_free_cnt_3))
print("fct_free_cnt_4:", hex(fct_free_cnt_4))
print("fct_free_cnt_5:", hex(fct_free_cnt_5))
print("fct_free_cnt_6:", hex(fct_free_cnt_6))
print("fct_free_cnt_7:", hex(fct_free_cnt_7))
print("fct_free_cnt_8:", hex(fct_free_cnt_8))
print("fct_free_cnt_9:", hex(fct_free_cnt_9))
print("fct_free_cnt_10:", hex(fct_free_cnt_10))
print("fct_free_cnt_11:", hex(fct_free_cnt_11))
print("fct_free_cnt_12:", hex(fct_free_cnt_12))
print("fct_free_cnt_13:", hex(fct_free_cnt_13))
print("fct_free_cnt_14:", hex(fct_free_cnt_14))
print("fct_free_cnt_15:", hex(fct_free_cnt_15))
print("fct_free_cnt_16:", hex(fct_free_cnt_16))
print("fct_free_cnt_17:", hex(fct_free_cnt_17))
print("fct_free_cnt_18:", hex(fct_free_cnt_18))
print("fct_free_cnt_19:", hex(fct_free_cnt_19))
print("fct_free_cnt_20:", hex(fct_free_cnt_20))
print("fct_free_cnt_21:", hex(fct_free_cnt_21))
print("fct_free_cnt_22:", hex(fct_free_cnt_22))
print("fct_free_cnt_23:", hex(fct_free_cnt_23))
print("fct_free_cnt_24:", hex(fct_free_cnt_24))
print("fct_free_cnt_25:", hex(fct_free_cnt_25))
print("fct_free_cnt_26:", hex(fct_free_cnt_26))
print("fct_free_cnt_27:", hex(fct_free_cnt_27))
print("fct_free_cnt_28:", hex(fct_free_cnt_28))
print("fct_free_cnt_29:", hex(fct_free_cnt_29))
print("fct_free_cnt_30:", hex(fct_free_cnt_30))
print("fct_free_cnt_31:", hex(fct_free_cnt_31))

start_offset += 72
if DEBUG_BUFFER == 1:
    print(text_data[start_offset:])
fields = re.findall(pattern, text_data[start_offset:])
# 第八、九行的解析
pf_event_start = int(fields[0], 16)& 0xFF  # 提取低8位
task_clear_after_hw_flag = (int(fields[0], 16) >> 8)& 0xFF  # 提取次低8位
pf_chk_power_fail_reg_cnt = (int(fields[0], 16) >> 16)& 0xFF  # 提取次高8位
events_chk_flags = (int(fields[0], 16) >> 24)& 0xFF  # 提取高8位
b2n_sent_cnt_0 = int(fields[1], 16)& 0xFF  # 提取低8位
b2n_sent_cnt_1 = (int(fields[1], 16) >> 8)& 0xFF  # 提取次低8位
b2n_sent_cnt_2 = (int(fields[1], 16) >> 16)& 0xFF  # 提取次高8位
b2n_sent_cnt_3 = (int(fields[1], 16) >> 24)& 0xFF  # 提取高8位
b2n_sent_cnt_4 = int(fields[2], 16)& 0xFF  # 提取低8位
b2n_sent_cnt_5 = (int(fields[2], 16) >> 8)& 0xFF  # 提取次低8位
b2n_sent_cnt_6 = (int(fields[2], 16) >> 16)& 0xFF  # 提取次高8位
b2n_sent_cnt_7 = (int(fields[2], 16) >> 24)& 0xFF  # 提取高8位
b2n_sent_cnt_8 = int(fields[3], 16)& 0xFF  # 提取低8位
b2n_sent_cnt_9 = (int(fields[3], 16) >> 8)& 0xFF  # 提取次低8位
b2n_sent_cnt_10 = (int(fields[3], 16) >> 16)& 0xFF  # 提取次高8位
b2n_sent_cnt_11 = (int(fields[3], 16) >> 24)& 0xFF  # 提取高8位
hot_b2n_fcq_sq_hd = int(fields[4], 16)& 0xFF  # 提取低8位
hot_b2n_fcq_sq_tail = (int(fields[4], 16) >> 8)& 0xFF  # 提取次低8位
hot_b2n_fcq_cq_hd = (int(fields[4], 16) >> 16)& 0xFF  # 提取次高8位
hot_b2n_fcq_cq_tail = (int(fields[4], 16) >> 24)& 0xFF  # 提取高8位

tcg_b2n_fcq_sq_hd = int(fields[5], 16)& 0xFF  # 提取低8位
tcg_b2n_fcq_sq_tail = (int(fields[5], 16) >> 8)& 0xFF  # 提取次低8位
tcg_b2n_fcq_cq_hd = (int(fields[5], 16) >> 16)& 0xFF  # 提取次高8位
tcg_b2n_fcq_cq_tail = (int(fields[5], 16) >> 24)& 0xFF  # 提取高8位

gc_b2n_fcq_sq_hd = int(fields[6], 16)& 0xFF  # 提取低8位
gc_b2n_fcq_sq_tail = (int(fields[6], 16) >> 8)& 0xFF  # 提取次低8位
gc_b2n_fcq_cq_hd = (int(fields[6], 16) >> 16)& 0xFF  # 提取次高8位
gc_b2n_fcq_cq_tail = (int(fields[6], 16) >> 24)& 0xFF  # 提取高8位

wl_b2n_fcq_sq_hd = int(fields[7], 16)& 0xFF  # 提取低8位
wl_b2n_fcq_sq_tail = (int(fields[7], 16) >> 8)& 0xFF  # 提取次低8位
wl_b2n_fcq_cq_hd = (int(fields[7], 16) >> 16)& 0xFF  # 提取次高8位
wl_b2n_fcq_cq_tail = (int(fields[7], 16) >> 24)& 0xFF  # 提取高8位

eh_b2n_fcq_sq_hd = int(fields[8], 16)& 0xFF  # 提取低8位
eh_b2n_fcq_sq_tail = (int(fields[8], 16) >> 8)& 0xFF  # 提取次低8位
eh_b2n_fcq_cq_hd = (int(fields[8], 16) >> 16)& 0xFF  # 提取次高8位
eh_b2n_fcq_cq_tail = (int(fields[8], 16) >> 24)& 0xFF  # 提取高8位

footer_b2n_fcq_sq_hd = int(fields[9], 16)& 0xFF  # 提取低8位
footer_b2n_fcq_sq_tail = (int(fields[9], 16) >> 8)& 0xFF  # 提取次低8位
footer_b2n_fcq_cq_hd = (int(fields[9], 16) >> 16)& 0xFF  # 提取次高8位
footer_b2n_fcq_cq_tail = (int(fields[9], 16) >> 24)& 0xFF  # 提取高8位

l2p_b2n_fcq_sq_hd = int(fields[10], 16)& 0xFF  # 提取低8位
l2p_b2n_fcq_sq_tail = (int(fields[10], 16) >> 8)& 0xFF  # 提取次低8位
l2p_b2n_fcq_cq_hd = (int(fields[10], 16) >> 16)& 0xFF  # 提取次高8位
l2p_b2n_fcq_cq_tail = (int(fields[10], 16) >> 24)& 0xFF  # 提取高8位

patch_b2n_fcq_sq_hd = int(fields[11], 16)& 0xFF  # 提取低8位
patch_b2n_fcq_sq_tail = (int(fields[11], 16) >> 8)& 0xFF  # 提取次低8位
patch_b2n_fcq_cq_hd = (int(fields[11], 16) >> 16)& 0xFF  # 提取次高8位
patch_b2n_fcq_cq_tail = (int(fields[11], 16) >> 24)& 0xFF  # 提取高8位

rfs_b2n_fcq_sq_hd = int(fields[12], 16)& 0xFF  # 提取低8位
rfs_b2n_fcq_sq_tail = (int(fields[12], 16) >> 8)& 0xFF  # 提取次低8位
rfs_b2n_fcq_cq_hd = (int(fields[12], 16) >> 16)& 0xFF  # 提取次高8位
rfs_b2n_fcq_cq_tail = (int(fields[12], 16) >> 24)& 0xFF  # 提取高8位

pf_b2n_fcq_sq_hd = int(fields[13], 16)& 0xFF  # 提取低8位
pf_b2n_fcq_sq_tail = (int(fields[13], 16) >> 8)& 0xFF  # 提取次低8位
pf_b2n_fcq_cq_hd = (int(fields[13], 16) >> 16)& 0xFF  # 提取次高8位
pf_b2n_fcq_cq_tail = (int(fields[13], 16) >> 24)& 0xFF  # 提取高8位

share_b2n_fcq_sq_hd = int(fields[14], 16)& 0xFF  # 提取低8位
share_b2n_fcq_sq_tail = (int(fields[14], 16) >> 8)& 0xFF  # 提取次低8位
share_b2n_fcq_cq_hd = (int(fields[14], 16) >> 16)& 0xFF  # 提取次高8位
share_b2n_fcq_cq_tail = (int(fields[14], 16) >> 24)& 0xFF  # 提取高8位

rmw_mgr_state = int(fields[15], 16)& 0xFF  # 提取低8位

print("pf_event_start:", hex(pf_event_start))
print("task_clear_after_hw_flag:", hex(task_clear_after_hw_flag))
print("pf_chk_power_fail_reg_cnt:", hex(pf_chk_power_fail_reg_cnt))
print("events_chk_flags:", hex(events_chk_flags))
print("b2n_sent_cnt_0:", hex(b2n_sent_cnt_0))
print("b2n_sent_cnt_1:", hex(b2n_sent_cnt_1))
print("b2n_sent_cnt_2:", hex(b2n_sent_cnt_2))
print("b2n_sent_cnt_3:", hex(b2n_sent_cnt_3))
print("b2n_sent_cnt_4:", hex(b2n_sent_cnt_4))
print("b2n_sent_cnt_5:", hex(b2n_sent_cnt_5))
print("b2n_sent_cnt_6:", hex(b2n_sent_cnt_6))
print("b2n_sent_cnt_7:", hex(b2n_sent_cnt_7))
print("b2n_sent_cnt_8:", hex(b2n_sent_cnt_8))
print("b2n_sent_cnt_9:", hex(b2n_sent_cnt_9))
print("b2n_sent_cnt_10:", hex(b2n_sent_cnt_10))
print("b2n_sent_cnt_11:", hex(b2n_sent_cnt_11))
print("hot_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail:", hex(hot_b2n_fcq_sq_hd), hex(hot_b2n_fcq_sq_tail), hex(hot_b2n_fcq_cq_hd), hex(hot_b2n_fcq_cq_tail))
print("tcg_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail:", hex(tcg_b2n_fcq_sq_hd), hex(tcg_b2n_fcq_sq_tail), hex(tcg_b2n_fcq_cq_hd), hex(tcg_b2n_fcq_cq_tail))
print("gc_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail:", hex(gc_b2n_fcq_sq_hd), hex(gc_b2n_fcq_sq_tail), hex(gc_b2n_fcq_cq_hd), hex(gc_b2n_fcq_cq_tail))
print("wl_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail:", hex(wl_b2n_fcq_sq_hd), hex(wl_b2n_fcq_sq_tail), hex(wl_b2n_fcq_cq_hd), hex(wl_b2n_fcq_cq_tail))
print("eh_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail:", hex(eh_b2n_fcq_sq_hd), hex(eh_b2n_fcq_sq_tail), hex(eh_b2n_fcq_cq_hd), hex(eh_b2n_fcq_cq_tail))
print("footer_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail:", hex(footer_b2n_fcq_sq_hd), hex(footer_b2n_fcq_sq_tail), hex(footer_b2n_fcq_cq_hd), hex(footer_b2n_fcq_cq_tail))
print("l2p_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail:", hex(l2p_b2n_fcq_sq_hd), hex(l2p_b2n_fcq_sq_tail), hex(l2p_b2n_fcq_cq_hd), hex(l2p_b2n_fcq_cq_tail))
print("patch_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail:", hex(patch_b2n_fcq_sq_hd), hex(patch_b2n_fcq_sq_tail), hex(patch_b2n_fcq_cq_hd), hex(patch_b2n_fcq_cq_tail))
print("rfs_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail:", hex(hot_b2n_fcq_sq_hd), hex(hot_b2n_fcq_sq_tail), hex(hot_b2n_fcq_cq_hd), hex(hot_b2n_fcq_cq_tail))
print("pf_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail:", hex(pf_b2n_fcq_sq_hd), hex(pf_b2n_fcq_sq_tail), hex(pf_b2n_fcq_cq_hd), hex(pf_b2n_fcq_cq_tail))
print("share_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail:", hex(share_b2n_fcq_sq_hd), hex(share_b2n_fcq_sq_tail), hex(share_b2n_fcq_cq_hd), hex(share_b2n_fcq_cq_tail))
print("rmw_mgr_state:", hex(rmw_mgr_state))


start_offset += 72*2
if DEBUG_BUFFER == 1:
    print(text_data[start_offset:])

fields = re.findall(pattern, text_data[start_offset:])
# 第十行的解析

flush_cnt_stream0 = (int(fields[0], 16))& 0xFF  # 提取次低8位
flush_cnt_stream1 = (int(fields[0], 16) >> 8)& 0xFF  # 提取次低8位
flush_cnt_stream2 = (int(fields[0], 16) >> 16)& 0xFF  # 提取次低8位
flush_cnt_stream3 = (int(fields[0], 16) >> 24)& 0xFF  # 提取次低8位
flush_cnt_stream4 = (int(fields[1], 16))& 0xFF  # 提取次低8位
flush_cnt_stream5 = (int(fields[1], 16) >> 8)& 0xFF  # 提取次低8位
flush_cnt_stream6 = (int(fields[1], 16) >> 16)& 0xFF  # 提取次低8位
flush_cnt_stream7 = (int(fields[1], 16) >> 24)& 0xFF  # 提取次低8位
flush_cnt_stream8 = (int(fields[2], 16))& 0xFF  # 提取次低8位
flush_cnt_stream9 = (int(fields[2], 16) >> 8)& 0xFF  # 提取次低8位
flush_cnt_stream10 = (int(fields[2], 16) >> 16)& 0xFF  # 提取次低8位
flush_cnt_stream11 = (int(fields[2], 16) >> 24)& 0xFF  # 提取次低8位
pending_info_state = (int(fields[3], 16))& 0xFF  # 提取次低8位
pending_info_opc = (int(fields[3], 16) >> 8)& 0xFF  # 提取次低8位
pending_info_split_cnt = (int(fields[3], 16) >> 16)& 0xFFFF  # 提取高16位
pending_info_trig_cnt = (int(fields[4], 16))& 0xFFFF  # 提取低16位
pending_info_cpl_cnt = (int(fields[4], 16) >> 16)& 0xFFFF  # 提取高16位
high_bits = int(fields[5], 16)  # 将高位部分转换为整数
low_bits = int(fields[6], 16)  # 将低位部分转换为整数
pending_info_lba = (high_bits << 32) | low_bits  # 合并高位和低位
pending_info_len = int(fields[7], 16)  # 将高位部分转换为整数
print("flush_cnt_stream0:", hex(flush_cnt_stream0))
print("flush_cnt_stream1:", hex(flush_cnt_stream1))
print("flush_cnt_stream2:", hex(flush_cnt_stream2))
print("flush_cnt_stream3:", hex(flush_cnt_stream3))
print("flush_cnt_stream4:", hex(flush_cnt_stream4))
print("flush_cnt_stream5:", hex(flush_cnt_stream5))
print("flush_cnt_stream6:", hex(flush_cnt_stream6))
print("flush_cnt_stream7:", hex(flush_cnt_stream7))
print("flush_cnt_stream8:", hex(flush_cnt_stream8))
print("flush_cnt_stream9:", hex(flush_cnt_stream9))
print("flush_cnt_stream10:", hex(flush_cnt_stream10))
print("flush_cnt_stream11:", hex(flush_cnt_stream11))
print("pending_info_state:", hex(pending_info_state))
print("pending_info_opc:", hex(pending_info_opc))
print("pending_info_split_cnt:", hex(pending_info_split_cnt))
print("pending_info_trig_cnt:", hex(pending_info_trig_cnt))
print("pending_info_cpl_cnt:", hex(pending_info_cpl_cnt))

start_offset += 72
if DEBUG_BUFFER == 1:
    print(text_data[start_offset:])
fields = re.findall(pattern, text_data[start_offset:])
# 第十一行的解析
pending_info_update_fail_0 = int(fields[0], 16)
pending_info_update_fail_1 = int(fields[1], 16)
pending_info_update_fail_2 = int(fields[2], 16)
high_bits = int(fields[3], 16)  # 将高位部分转换为整数
low_bits = int(fields[4], 16)  # 将低位部分转换为整数
pending_info_common_flag = (high_bits << 32) | low_bits  # 合并高位和低位
is_map_bgtriming = int(fields[5], 16)& 0xFF  # 提取低8位
has_bg_trim = (int(fields[5], 16) >> 8)& 0xFF  # 提取次低8位
console_task_state_before_pf = (int(fields[5], 16) >> 16)& 0xFF  # 提取次高8位
warning_timeout_task = (int(fields[5], 16) >> 24)& 0xFF  # 提取高8位
pf_nor_dump_timestamp = int(fields[6], 16)
io_idle_time = int(fields[7], 16)

print("pending_info_update_fail_0:", hex(pending_info_update_fail_0))
print("pending_info_update_fail_1:", hex(pending_info_update_fail_1))
print("pending_info_update_fail_2:", hex(pending_info_update_fail_2))
print("pending_info_common_flag:", hex(pending_info_common_flag))
print("is_map_bgtriming:", hex(is_map_bgtriming))
print("has_bg_trim:", hex(has_bg_trim))
print("console_task_state_before_pf:", hex(console_task_state_before_pf))
print("warning_timeout_task:", hex(warning_timeout_task))
print("pf_nor_dump_timestamp:", hex(pf_nor_dump_timestamp))
print("io_idle_time:", hex(io_idle_time))

start_offset += 72
if DEBUG_BUFFER == 1:
    print(text_data[start_offset:])
fields = re.findall(pattern, text_data[start_offset:])
# 第十二行的解析
ck_w2p_fcq_sq_hd = int(fields[0], 16)& 0xFF  # 提取低8位
ck_w2p_fcq_sq_tail = (int(fields[0], 16) >> 8)& 0xFF  # 提取次低8位
ck_w2p_fcq_cq_hd = (int(fields[0], 16) >> 16)& 0xFF  # 提取次高8位
ck_w2p_fcq_cq_tail = (int(fields[0], 16) >> 24)& 0xFF  # 提取高8位
patch_w2p_fcq_sq_hd = int(fields[1], 16)& 0xFF  # 提取低8位
patch_w2p_fcq_sq_tail = (int(fields[1], 16) >> 8)& 0xFF  # 提取次低8位
patch_w2p_fcq_cq_hd = (int(fields[1], 16) >> 16)& 0xFF  # 提取次高8位
patch_w2p_fcq_cq_tail = (int(fields[1], 16) >> 24)& 0xFF  # 提取高8位
evtlog_w2p_fcq_sq_hd = int(fields[2], 16)& 0xFF  # 提取低8位
evtlog_w2p_fcq_sq_tail = (int(fields[2], 16) >> 8)& 0xFF  # 提取次低8位
evtlog_w2p_fcq_cq_hd = (int(fields[2], 16) >> 16)& 0xFF  # 提取次高8位
evtlog_w2p_fcq_cq_tail = (int(fields[2], 16) >> 24)& 0xFF  # 提取高8位
evtlog_b2n_fcq_sq_hd = int(fields[3], 16)& 0xFF  # 提取低8位
evtlog_b2n_fcq_sq_tail = (int(fields[3], 16) >> 8)& 0xFF  # 提取次低8位
evtlog_b2n_fcq_cq_hd = (int(fields[3], 16) >> 16)& 0xFF  # 提取次高8位
evtlog_b2n_fcq_cq_tail = (int(fields[3], 16) >> 24)& 0xFF  # 提取高8位
pf_w2p_fcq_sq_hd = int(fields[4], 16)& 0xFF  # 提取低8位
pf_w2p_fcq_sq_tail = (int(fields[4], 16) >> 8)& 0xFF  # 提取次低8位
pf_w2p_fcq_cq_hd = (int(fields[4], 16) >> 16)& 0xFF  # 提取次高8位
pf_w2p_fcq_cq_tail = (int(fields[4], 16) >> 24)& 0xFF  # 提取高8位
err_validation_wr_fcq_sq_hd = int(fields[5], 16)& 0xFF  # 提取低8位
err_validation_wr_fcq_sq_tail = (int(fields[5], 16) >> 8)& 0xFF  # 提取次低8位
err_validation_wr_fcq_cq_hd = (int(fields[5], 16) >> 16)& 0xFF  # 提取次高8位
err_validation_wr_fcq_cq_tail = (int(fields[5], 16) >> 24)& 0xFF  # 提取高8位
b2n_hot_data_fcq_sq_hd = int(fields[6], 16)& 0xFF  # 提取低8位
b2n_hot_data_fcq_sq_tail = (int(fields[6], 16) >> 8)& 0xFF  # 提取次低8位
b2n_hot_data_fcq_cq_hd = (int(fields[6], 16) >> 16)& 0xFF  # 提取次高8位
b2n_hot_data_fcq_cq_tail = (int(fields[6], 16) >> 24)& 0xFF  # 提取高8位
b2n_shared_fcq_sq_hd = int(fields[7], 16)& 0xFF  # 提取低8位
b2n_shared_fcq_sq_tail = (int(fields[7], 16) >> 8)& 0xFF  # 提取次低8位
b2n_shared_fcq_cq_hd = (int(fields[7], 16) >> 16)& 0xFF  # 提取次高8位
b2n_shared_fcq_cq_tail = (int(fields[7], 16) >> 24)& 0xFF  # 提取高8位

print("ck_w2p_fcq_sq_hd:", hex(ck_w2p_fcq_sq_hd))
print("ck_w2p_fcq_sq_tail:", hex(ck_w2p_fcq_sq_tail))
print("ck_w2p_fcq_cq_hd:", hex(ck_w2p_fcq_cq_hd))
print("ck_w2p_fcq_cq_tail:", hex(ck_w2p_fcq_cq_tail))
print("patch_w2p_fcq_sq_hd:", hex(patch_w2p_fcq_sq_hd))
print("patch_w2p_fcq_sq_tail:", hex(patch_w2p_fcq_sq_tail))
print("patch_w2p_fcq_cq_hd:", hex(patch_w2p_fcq_cq_hd))
print("patch_w2p_fcq_cq_tail:", hex(patch_w2p_fcq_cq_tail))
print("evtlog_w2p_fcq_sq_hd:", hex(evtlog_w2p_fcq_sq_hd))
print("evtlog_w2p_fcq_sq_tail:", hex(evtlog_w2p_fcq_sq_tail))
print("evtlog_w2p_fcq_cq_hd:", hex(evtlog_w2p_fcq_cq_hd))
print("evtlog_w2p_fcq_cq_tail:", hex(evtlog_w2p_fcq_cq_tail))

print("evtlog_b2n_fcq_sq_hd:", hex(evtlog_b2n_fcq_sq_hd))
print("evtlog_b2n_fcq_sq_tail:", hex(evtlog_b2n_fcq_sq_tail))
print("evtlog_b2n_fcq_cq_hd:", hex(evtlog_b2n_fcq_cq_hd))
print("evtlog_b2n_fcq_cq_tail:", hex(evtlog_b2n_fcq_cq_tail))
print("pf_w2p_fcq_sq_hd:", hex(pf_w2p_fcq_sq_hd))
print("pf_w2p_fcq_sq_tail:", hex(pf_w2p_fcq_sq_tail))
print("pf_w2p_fcq_cq_hd:", hex(pf_w2p_fcq_cq_hd))
print("pf_w2p_fcq_cq_tail:", hex(pf_w2p_fcq_cq_tail))
print("err_validation_wr_fcq_sq_hd:", hex(err_validation_wr_fcq_sq_hd))
print("err_validation_wr_fcq_sq_tail:", hex(err_validation_wr_fcq_sq_tail))
print("err_validation_wr_fcq_cq_hd:", hex(err_validation_wr_fcq_cq_hd))
print("err_validation_wr_fcq_cq_tail:", hex(err_validation_wr_fcq_cq_tail))

print("b2n_hot_data_fcq_sq_hd:", hex(b2n_hot_data_fcq_sq_hd))
print("b2n_hot_data_fcq_sq_tail:", hex(b2n_hot_data_fcq_sq_tail))
print("b2n_hot_data_fcq_cq_hd:", hex(b2n_hot_data_fcq_cq_hd))
print("b2n_hot_data_fcq_cq_tail:", hex(b2n_hot_data_fcq_cq_tail))
print("b2n_shared_fcq_sq_hd:", hex(b2n_shared_fcq_sq_hd))
print("b2n_shared_fcq_sq_tail:", hex(b2n_shared_fcq_sq_tail))
print("b2n_shared_fcq_cq_hd:", hex(b2n_shared_fcq_cq_hd))
print("b2n_shared_fcq_cq_tail:", hex(b2n_shared_fcq_cq_tail))

start_offset += 72
if DEBUG_BUFFER == 1:
    print(text_data[start_offset:])
fields = re.findall(pattern, text_data[start_offset:])
# 第十三行的解析
high_bits = int(fields[1], 16)  # 将高位部分转换为整数
low_bits = int(fields[0], 16)  # 将低位部分转换为整数
elr = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[3], 16)  # 将高位部分转换为整数
low_bits = int(fields[2], 16)  # 将低位部分转换为整数
esr = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[5], 16)  # 将高位部分转换为整数
low_bits = int(fields[4], 16)  # 将低位部分转换为整数
far = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[7], 16)  # 将高位部分转换为整数
low_bits = int(fields[6], 16)  # 将低位部分转换为整数
softirq_active_flag = (high_bits << 32) | low_bits  # 合并高位和低位

print("elr:", hex(elr))
print("esr:", hex(esr))
print("far:", hex(far))
print("softirq_active_flag:", hex(softirq_active_flag))

start_offset += 72
if DEBUG_BUFFER == 1:
    print(text_data[start_offset:])
fields = re.findall(pattern, text_data[start_offset:])
# 第十四行的解析
high_bits = int(fields[1], 16)  # 将高位部分转换为整数
low_bits = int(fields[0], 16)  # 将低位部分转换为整数
last_prog_pba_0 = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[3], 16)  # 将高位部分转换为整数
low_bits = int(fields[2], 16)  # 将低位部分转换为整数
last_prog_pba_1 = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[5], 16)  # 将高位部分转换为整数
low_bits = int(fields[4], 16)  # 将低位部分转换为整数
last_prog_pba_2 = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[7], 16)  # 将高位部分转换为整数
low_bits = int(fields[6], 16)  # 将低位部分转换为整数
last_prog_pba_3 = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[9], 16)  # 将高位部分转换为整数
low_bits = int(fields[8], 16)  # 将低位部分转换为整数
last_prog_pba_4 = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[11], 16)  # 将高位部分转换为整数
low_bits = int(fields[10], 16)  # 将低位部分转换为整数
last_prog_pba_5 = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[13], 16)  # 将高位部分转换为整数
low_bits = int(fields[12], 16)  # 将低位部分转换为整数
last_prog_pba_6 = (high_bits << 32) | low_bits  # 合并高位和低位
high_bits = int(fields[15], 16)  # 将高位部分转换为整数
low_bits = int(fields[14], 16)  # 将低位部分转换为整数
last_prog_pba_7 = (high_bits << 32) | low_bits  # 合并高位和低位


assert_id = int(fields[16], 16)& 0xFFFF  # 提取低16位
task_assert_id = (int(fields[16], 16) >> 16)& 0xFFFF  # 提取高16位

intr_id = int(fields[17], 16)
daifSetValue = int(fields[18], 16)
ddr_ecc_status = int(fields[19], 16)
rsv2 = int(fields[20], 16)
fw_active_state = int(fields[21], 16)& 0xFF  # 提取低8位
fw_active_immed_state = (int(fields[21], 16) >> 8)& 0xFF  # 提取次低8位
warning_timeout_task_state = (int(fields[21], 16) >> 16)& 0xFF  # 提取次低8位
hw_abort_step = (int(fields[21], 16) >> 24)& 0xFF  # 提取高8位
hw_abort_regs_0 = int(fields[22], 16)
hw_abort_regs_1 = int(fields[23], 16)
hw_abort_regs_2 = int(fields[24], 16)
hw_abort_regs_3 = int(fields[25], 16)
hw_abort_regs_4 = int(fields[26], 16)
hw_abort_regs_5 = int(fields[27], 16)
fe_regs_0 = int(fields[28], 16)
fe_regs_1 = int(fields[29], 16)


print("last_prog_pba_0:", hex(last_prog_pba_0))
print("last_prog_pba_1:", hex(last_prog_pba_1))
print("last_prog_pba_2:", hex(last_prog_pba_2))
print("last_prog_pba_3:", hex(last_prog_pba_3))
print("last_prog_pba_4:", hex(last_prog_pba_4))
print("last_prog_pba_5:", hex(last_prog_pba_5))
print("last_prog_pba_6:", hex(last_prog_pba_6))
print("last_prog_pba_7:", hex(last_prog_pba_7))

print("assert_id:", hex(assert_id))
print("task_assert_id:", hex(task_assert_id))
print("intr_id:", hex(intr_id))
print("daifSetValue:", hex(daifSetValue))
print("ddr_ecc_status:", hex(ddr_ecc_status))
print("fw_active_state:", hex(fw_active_state))
print("fw_active_immed_state:", hex(fw_active_immed_state))
print("warning_timeout_task_state:", hex(warning_timeout_task_state))
print("hw_abort_step:", hex(hw_abort_step))

print("hw_abort_regs_0:", hex(hw_abort_regs_0))
print("hw_abort_regs_1:", hex(hw_abort_regs_1))
print("hw_abort_regs_2:", hex(hw_abort_regs_2))
print("hw_abort_regs_3:", hex(hw_abort_regs_3))
print("hw_abort_regs_4:", hex(hw_abort_regs_4))
print("hw_abort_regs_5:", hex(hw_abort_regs_5))
print("fe_regs_0:", hex(fe_regs_0))
print("fe_regs_1:", hex(fe_regs_1))


# # 读取文本文件，将每行的数据存储在lines列表中
# with open(file_path, 'r') as file:
    # log_data = file.read()
    # # 将log_data转换为字节对象
    # binary_data = log_data.encode()
    # #print(binary_data)
    # pf_nor_data_format_partial = f'{pf_root_data_format}'
    # pf_nor_data_size = struct.calcsize(pf_nor_data_format_partial)
    # print("pf_nor_data_format_partial 字节数:", pf_nor_data_size)
    # lines = file.readlines()

    # # 根据实际的二进制数据长度，更新pf_nor_data的格式字符串
    # pf_nor_data_format = f'{pf_root_data_format}IQQ{len(log_data) - struct.calcsize(pf_nor_data_format)}s'
    # print("pf_nor_data_format 字节数:", struct.calcsize(pf_nor_data_format))
    # # 检查二进制数据的长度是否与pf_nor_data的长度匹配
    # if len(log_data) != struct.calcsize(pf_nor_data_format):
        # print("Error: Invalid binary data length.")
    # else:
        # # 解析每个字段
        # pf_nor_data_format_partial = f'{pf_root_data_format}IQQ'
        # parsed_data = struct.unpack(pf_nor_data_format_partial, binary_data[:struct.calcsize(pf_nor_data_format_partial)])
        # print(binary_data[:struct.calcsize(pf_nor_data_format_partial)])

        # # 提取解析后的值并存储在相应的变量中
        # # 请根据具体的格式字符串和变量名修改以下代码

        # # 第一行的解析
        # pf_cnt = parsed_data[0]
        # pf_dump_state = parsed_data[1]
        # flush_meta_state = parsed_data[2]
        # flush_summary_state = parsed_data[3]

        # # 打印每个字段的值
        # print("pf_cnt:", hex(pf_cnt))
        # print("nor_sub_sector_idx:", hex(nor_sub_sector_idx))
        # print("pf_dump_time:", hex(pf_dump_time))
        # print("current_pf_sblock:", hex(current_pf_sblock))
        # print("summary_data_pba:", hex(summary_data_pba))
        # print("last_written_pba:", hex(last_written_pba))
        # print("user_data_start_pba:", hex(user_data_start_pba))
        # print("user_data_end_pba:", hex(user_data_end_pba))
        # print("parity_data_start_pba:", hex(parity_data_start_pba))
        # print("parity_data_end_pba:", hex(parity_data_end_pba))
        # print("meta_data_start_pba:", hex(meta_data_start_pba))
        # print("meta_data_end_pba:", hex(meta_data_end_pba))