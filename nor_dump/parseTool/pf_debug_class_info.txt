struct pf_debug_data {
    // 1st row
    u16 pf_cnt;
    u16 pf_dump_state;
    u16 flush_meta_state;
    u16 flush_summary_state;
    u16 flush_user_data_state;
    u16 flush_parity_state;
    u16 parity_padding_state;
    u16 flush_stream;
    u16 flush_index;
    u16 flush_page_type;
    u16 chunk_cnt;
    u16 b2n_sent;
    u16 b2n_received;
    u16 w2p_sent;
    u16 w2p_received;
    u16 patch_free_fifo_cnt;

    // 2nd row
    u32 event_handled_flags;
    u32 pf_event_start_timestamp;
    u32 pf_dump_task_start_timestamp;
    u32 pf_hw_abort_start_timestamp;
    u32 pf_hw_abort_complete_timestamp;
    u32 pf_dump_wait_all_cmd_done_timestamp;
    u32 pf_dump_task_complete_timestamp;
    u32 recovery_window_size;

    // 3th-4th rows
    u8 task_states[MAX_TASK_NUMBER_IN_SCHEDULER];

    // 5th row
    u64 task_paused;
    u8 admin_cmd_state[PF_MAX_ADMIN_CMD_IN_NOR];

    // 6th row
    u8 smp_switch_stage[PLATFORM_CORE_COUNT];
    u8 active_task_idx[PLATFORM_CORE_COUNT];
    u8 core_smpmgr_state[PLATFORM_CORE_COUNT];
    u16 io_clear_flags;
    u16 b2n_clear_flags;
    u32 switch_from_state;

    // 7th row
    u8  fct_free_cnt[MAX_FCT_POOL_CNT];

    // 8th-9th row
    u8  pf_event_start;
    u8  task_clear_after_hw_flag;
    u8  pf_chk_power_fail_reg_cnt;
    u8  events_chk_flags;
    u8  b2n_sent_cnt[STREAM_ID_MAX];
    u8  hot_b2n_fcq[4]; // sq_hd, sq_tail, cq_hd, cq_tail
    u8  tcg_b2n_fcq[4];
    u8  gc_b2n_fcq[4];
    u8  wl_b2n_fcq[4];
    u8  eh_b2n_fcq[4];
    u8  footer_b2n_fcq[4];
    u8  l2p_b2n_fcq[4];
    u8  patch_b2n_fcq[4];
    u8  rfs_b2n_fcq[4];
    u8  pf_b2n_fcq[4];
    u8  share_b2n_fcq[4];
    u8  rmw_mgr_state;
    u8  no_b2n_strm;
    u16 dump_to_nor_timecost;

    // 10th row
    u8  flush_cnt[STREAM_ID_MAX];
    u8  pending_info_state;
    u8  pending_info_opc;
    u16 pending_info_split_cnt;
    u16 pending_info_trig_cnt;
    u16 pending_info_cpl_cnt;
    u64 pending_info_lba;
    u32 pending_info_len;

    // 11th row
    u32 pending_info_update_fail[3];
    u64 pending_info_common_flag;
    u8  is_map_bgtriming;
    u8  has_bg_trim;
    u8  console_task_state_before_pf;
    u8  warning_timeout_task;
    u32 pf_nor_dump_timestamp;
    u32 io_idle_time;

    // 12th row
    u8 ck_w2p_fcq[4]; // sq_hd, sq_tail, cq_hd, cq_tail
    u8 patch_w2p_fcq[4];
    u8 evtlog_w2p_fcq[4];
    u8 evtlog_b2n_fcq[4];
    u8 pf_w2p_fcq[4];
    u8 err_validation_wr_fcq[4];
    u16 pending_erase_cmd_cnt;
    u16 free_block_cnt;
    u32 chk_b2n_cnt;

    // 13th row
    u64 elr;
    u64 esr;
    u64 far;
    u64 softirq_active_flag;

    // 14th row
    u64 last_prog_pba[8];
    u16 assert_id;
    u16 task_assert_id;
    u32 intr_id;
    u32 daifSetValue;
    u32 ddr_ecc_status;
    u32 rsv2;
    u8  fw_active_state;
    u8  fw_active_immed_state;
    u8  warning_timeout_task_state;
    u8  hw_abort_step;
    u32 hw_abort_regs[6];
    u32 fe_regs[6];
    u32 be_regs[PF_NOR_DUMP_BE_REG_NUM];

} __attribute__((packed));