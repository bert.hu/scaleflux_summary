import os,sys,re

MAX_TASK_NUMBER_IN_SCHEDULER = 64
PF_MAX_ADMIN_CMD_IN_NOR = 24
PLATFORM_CORE_COUNT = 8
MAX_FCT_POOL_CNT = 32
STREAM_ID_MAX = 16
PF_NOR_DUMP_BE_REG_NUM  = 231

def get_array_size(text):
    pattern = r'\[([^\]]+)\]'
    match = re.search(pattern, text)
    if match:
        result = match.group(1)
        #print(result)
        return result

filename = None

if len(sys.argv) == 2:
	filename = sys.argv[1]

f = open(filename)
contents = f.readlines()
f.close()
new_contents = []
new_contents.append("class pba_bits(ctypes.LittleEndianStructure):			")	      
new_contents.append("    _fields_ = [")

result_list = []
for line in contents:
    if '{' in line or '}' in line:
        continue
    if " " not in line:
        var_type = "none"
    else:
        var_type, var = line.split()[:2]
    if var_type not in ["u16", "u32", "u8", "u64"]:
        new_contents.append("#" + line)
        continue
    if '[' not in var: #not array type
        var = var.strip(';')
        new_line = '("%s", %s),'%(var, var_type)
        result_line = 'print("%s: ", hex(pf_dbg_t.fields.%s))'%(var, var)
        result_list.append(result_line)
    else:
        array_size = get_array_size(var)
        var = var.split('[')[0]
        var = var.strip(';')
        new_line = '("%s", %s*%s),'%(var, var_type, array_size)
        for i in range(int(eval(array_size))):
            result_line = 'print("%s[%d]: ", hex(pf_dbg_t.fields.%s[%d]))'%(var, i, var, i)
            result_list.append(result_line)
    new_contents.append(new_line)
new_contents.append("]")
print("\n".join(new_contents))
print("=============================")
print("\n".join(result_list))