import os,sys								      
import ctypes								      
u64 = ctypes.c_uint64
u32 = ctypes.c_uint32
u16 = ctypes.c_uint16
u8 = ctypes.c_uint8
filename = None

if len(sys.argv) == 2:
	filename = sys.argv[1]								  


MAX_TASK_NUMBER_IN_SCHEDULER = 64
PF_MAX_ADMIN_CMD_IN_NOR = 24
PLATFORM_CORE_COUNT = 8
MAX_FCT_POOL_CNT = 32
STREAM_ID_MAX = 16
PF_NOR_DUMP_BE_REG_NUM  = 231
class pf_debug(ctypes.LittleEndianStructure):
        _fields_ = [
#    // 1st row

("pf_cnt", u16),
("pf_dump_state", u16),
("flush_meta_state", u16),
("flush_summary_state", u16),
("flush_user_data_state", u16),
("flush_parity_state", u16),
("parity_padding_state", u16),
("flush_stream", u16),
("flush_index", u16),
("flush_page_type", u16),
("chunk_cnt", u16),
("b2n_sent", u16),
("b2n_received", u16),
("w2p_sent", u16),
("w2p_received", u16),
("patch_free_fifo_cnt", u16),
#

#    // 2nd row

("event_handled_flags", u32),
("pf_event_start_timestamp", u32),
("pf_dump_task_start_timestamp", u32),
("pf_hw_abort_start_timestamp", u32),
("pf_hw_abort_complete_timestamp", u32),
("pf_dump_wait_all_cmd_done_timestamp", u32),
("pf_dump_task_complete_timestamp", u32),
("recovery_window_size", u32),
#

#    // 3th-4th rows

("task_states", u8*MAX_TASK_NUMBER_IN_SCHEDULER),
#

#    // 5th row

("task_paused", u64),
("admin_cmd_state", u8*PF_MAX_ADMIN_CMD_IN_NOR),
#

#    // 6th row

("smp_switch_stage", u8*PLATFORM_CORE_COUNT),
("active_task_idx", u8*PLATFORM_CORE_COUNT),
("core_smpmgr_state", u8*PLATFORM_CORE_COUNT),
("io_clear_flags", u16),
("b2n_clear_flags", u16),
("switch_from_state", u32),
#

#    // 7th row

("fct_free_cnt", u8*MAX_FCT_POOL_CNT),
#

#    // 8th-9th row

("pf_event_start", u8),
("task_clear_after_hw_flag", u8),
("pf_chk_power_fail_reg_cnt", u8),
("events_chk_flags", u8),
("b2n_sent_cnt", u8*STREAM_ID_MAX),
("hot_b2n_fcq", u8*4),
("tcg_b2n_fcq", u8*4),
("gc_b2n_fcq", u8*4),
("wl_b2n_fcq", u8*4),
("eh_b2n_fcq", u8*4),
("footer_b2n_fcq", u8*4),
("l2p_b2n_fcq", u8*4),
("patch_b2n_fcq", u8*4),
("rfs_b2n_fcq", u8*4),
("pf_b2n_fcq", u8*4),
("share_b2n_fcq", u8*4),
("rmw_mgr_state", u8),
("no_b2n_strm", u8),
("dump_to_nor_timecost", u16),
#

#    // 10th row

("flush_cnt", u8*STREAM_ID_MAX),
("pending_info_state", u8),
("pending_info_opc", u8),
("pending_info_split_cnt", u16),
("pending_info_trig_cnt", u16),
("pending_info_cpl_cnt", u16),
("pending_info_lba", u64),
("pending_info_len", u32),
#

#    // 11th row

("pending_info_update_fail", u32*3),
("pending_info_common_flag", u64),
("is_map_bgtriming", u8),
("has_bg_trim", u8),
("console_task_state_before_pf", u8),
("warning_timeout_task", u8),
("pf_nor_dump_timestamp", u32),
("io_idle_time", u32),
#

#    // 12th row

("ck_w2p_fcq", u8*4),
("patch_w2p_fcq", u8*4),
("evtlog_w2p_fcq", u8*4),
("evtlog_b2n_fcq", u8*4),
("pf_w2p_fcq", u8*4),
("err_validation_wr_fcq", u8*4),
("pending_erase_cmd_cnt", u16),
("free_block_cnt", u16),
("chk_b2n_cnt", u32),
#

#    // 13th row

("elr", u64),
("esr", u64),
("far", u64),
("softirq_active_flag", u64),
#

#    // 14th row

("last_prog_pba", u64*8),
("assert_id", u16),
("task_assert_id", u16),
("intr_id", u32),
("daifSetValue", u32),
("ddr_ecc_status", u32),
("rsv2", u32),
("fw_active_state", u8),
("fw_active_immed_state", u8),
("warning_timeout_task_state", u8),
("hw_abort_step", u8),
("hw_abort_regs", u32*6),
("fe_regs", u32*6),
("be_regs", u32*PF_NOR_DUMP_BE_REG_NUM),
#

	]


class pf_debug_all(ctypes.Union):
    _fields_ = [("fields", pf_debug),
                ("dw", u32 * 384)]

pf_dbg_t = pf_debug_all()


f = open(filename)
contents = f.readlines()
f.close()

dw_cnt = 0
for line in contents:
	if " |" in line:
		data_8dw = line.split(" |")[-1].split()[:8]
		if len(data_8dw) != 8:
			print("hit invalid string")
			sys.exit(1)
		for dw_val in data_8dw:
			hex_val = int(dw_val, 16)
			#print(dw_val)
			pf_dbg_t.dw[dw_cnt] = hex_val
			dw_cnt += 1

print("pf_cnt: ", hex(pf_dbg_t.fields.pf_cnt))
print("pf_dump_state: ", hex(pf_dbg_t.fields.pf_dump_state))
print("flush_meta_state: ", hex(pf_dbg_t.fields.flush_meta_state))
print("flush_summary_state: ", hex(pf_dbg_t.fields.flush_summary_state))
print("flush_user_data_state: ", hex(pf_dbg_t.fields.flush_user_data_state))
print("flush_parity_state: ", hex(pf_dbg_t.fields.flush_parity_state))
print("parity_padding_state: ", hex(pf_dbg_t.fields.parity_padding_state))
print("flush_stream: ", hex(pf_dbg_t.fields.flush_stream))
print("flush_index: ", hex(pf_dbg_t.fields.flush_index))
print("flush_page_type: ", hex(pf_dbg_t.fields.flush_page_type))
print("chunk_cnt: ", hex(pf_dbg_t.fields.chunk_cnt))
print("b2n_sent: ", hex(pf_dbg_t.fields.b2n_sent))
print("b2n_received: ", hex(pf_dbg_t.fields.b2n_received))
print("w2p_sent: ", hex(pf_dbg_t.fields.w2p_sent))
print("w2p_received: ", hex(pf_dbg_t.fields.w2p_received))
print("patch_free_fifo_cnt: ", hex(pf_dbg_t.fields.patch_free_fifo_cnt))
print("event_handled_flags: ", hex(pf_dbg_t.fields.event_handled_flags))
print("pf_event_start_timestamp: ", hex(pf_dbg_t.fields.pf_event_start_timestamp))
print("pf_dump_task_start_timestamp: ", hex(pf_dbg_t.fields.pf_dump_task_start_timestamp))
print("pf_hw_abort_start_timestamp: ", hex(pf_dbg_t.fields.pf_hw_abort_start_timestamp))
print("pf_hw_abort_complete_timestamp: ", hex(pf_dbg_t.fields.pf_hw_abort_complete_timestamp))
print("pf_dump_wait_all_cmd_done_timestamp: ", hex(pf_dbg_t.fields.pf_dump_wait_all_cmd_done_timestamp))
print("pf_dump_task_complete_timestamp: ", hex(pf_dbg_t.fields.pf_dump_task_complete_timestamp))
print("recovery_window_size: ", hex(pf_dbg_t.fields.recovery_window_size))
print("task_states[0]: ", hex(pf_dbg_t.fields.task_states[0]))
print("task_states[1]: ", hex(pf_dbg_t.fields.task_states[1]))
print("task_states[2]: ", hex(pf_dbg_t.fields.task_states[2]))
print("task_states[3]: ", hex(pf_dbg_t.fields.task_states[3]))
print("task_states[4]: ", hex(pf_dbg_t.fields.task_states[4]))
print("task_states[5]: ", hex(pf_dbg_t.fields.task_states[5]))
print("task_states[6]: ", hex(pf_dbg_t.fields.task_states[6]))
print("task_states[7]: ", hex(pf_dbg_t.fields.task_states[7]))
print("task_states[8]: ", hex(pf_dbg_t.fields.task_states[8]))
print("task_states[9]: ", hex(pf_dbg_t.fields.task_states[9]))
print("task_states[10]: ", hex(pf_dbg_t.fields.task_states[10]))
print("task_states[11]: ", hex(pf_dbg_t.fields.task_states[11]))
print("task_states[12]: ", hex(pf_dbg_t.fields.task_states[12]))
print("task_states[13]: ", hex(pf_dbg_t.fields.task_states[13]))
print("task_states[14]: ", hex(pf_dbg_t.fields.task_states[14]))
print("task_states[15]: ", hex(pf_dbg_t.fields.task_states[15]))
print("task_states[16]: ", hex(pf_dbg_t.fields.task_states[16]))
print("task_states[17]: ", hex(pf_dbg_t.fields.task_states[17]))
print("task_states[18]: ", hex(pf_dbg_t.fields.task_states[18]))
print("task_states[19]: ", hex(pf_dbg_t.fields.task_states[19]))
print("task_states[20]: ", hex(pf_dbg_t.fields.task_states[20]))
print("task_states[21]: ", hex(pf_dbg_t.fields.task_states[21]))
print("task_states[22]: ", hex(pf_dbg_t.fields.task_states[22]))
print("task_states[23]: ", hex(pf_dbg_t.fields.task_states[23]))
print("task_states[24]: ", hex(pf_dbg_t.fields.task_states[24]))
print("task_states[25]: ", hex(pf_dbg_t.fields.task_states[25]))
print("task_states[26]: ", hex(pf_dbg_t.fields.task_states[26]))
print("task_states[27]: ", hex(pf_dbg_t.fields.task_states[27]))
print("task_states[28]: ", hex(pf_dbg_t.fields.task_states[28]))
print("task_states[29]: ", hex(pf_dbg_t.fields.task_states[29]))
print("task_states[30]: ", hex(pf_dbg_t.fields.task_states[30]))
print("task_states[31]: ", hex(pf_dbg_t.fields.task_states[31]))
print("task_states[32]: ", hex(pf_dbg_t.fields.task_states[32]))
print("task_states[33]: ", hex(pf_dbg_t.fields.task_states[33]))
print("task_states[34]: ", hex(pf_dbg_t.fields.task_states[34]))
print("task_states[35]: ", hex(pf_dbg_t.fields.task_states[35]))
print("task_states[36]: ", hex(pf_dbg_t.fields.task_states[36]))
print("task_states[37]: ", hex(pf_dbg_t.fields.task_states[37]))
print("task_states[38]: ", hex(pf_dbg_t.fields.task_states[38]))
print("task_states[39]: ", hex(pf_dbg_t.fields.task_states[39]))
print("task_states[40]: ", hex(pf_dbg_t.fields.task_states[40]))
print("task_states[41]: ", hex(pf_dbg_t.fields.task_states[41]))
print("task_states[42]: ", hex(pf_dbg_t.fields.task_states[42]))
print("task_states[43]: ", hex(pf_dbg_t.fields.task_states[43]))
print("task_states[44]: ", hex(pf_dbg_t.fields.task_states[44]))
print("task_states[45]: ", hex(pf_dbg_t.fields.task_states[45]))
print("task_states[46]: ", hex(pf_dbg_t.fields.task_states[46]))
print("task_states[47]: ", hex(pf_dbg_t.fields.task_states[47]))
print("task_states[48]: ", hex(pf_dbg_t.fields.task_states[48]))
print("task_states[49]: ", hex(pf_dbg_t.fields.task_states[49]))
print("task_states[50]: ", hex(pf_dbg_t.fields.task_states[50]))
print("task_states[51]: ", hex(pf_dbg_t.fields.task_states[51]))
print("task_states[52]: ", hex(pf_dbg_t.fields.task_states[52]))
print("task_states[53]: ", hex(pf_dbg_t.fields.task_states[53]))
print("task_states[54]: ", hex(pf_dbg_t.fields.task_states[54]))
print("task_states[55]: ", hex(pf_dbg_t.fields.task_states[55]))
print("task_states[56]: ", hex(pf_dbg_t.fields.task_states[56]))
print("task_states[57]: ", hex(pf_dbg_t.fields.task_states[57]))
print("task_states[58]: ", hex(pf_dbg_t.fields.task_states[58]))
print("task_states[59]: ", hex(pf_dbg_t.fields.task_states[59]))
print("task_states[60]: ", hex(pf_dbg_t.fields.task_states[60]))
print("task_states[61]: ", hex(pf_dbg_t.fields.task_states[61]))
print("task_states[62]: ", hex(pf_dbg_t.fields.task_states[62]))
print("task_states[63]: ", hex(pf_dbg_t.fields.task_states[63]))
print("task_paused: ", hex(pf_dbg_t.fields.task_paused))
print("admin_cmd_state[0]: ", hex(pf_dbg_t.fields.admin_cmd_state[0]))
print("admin_cmd_state[1]: ", hex(pf_dbg_t.fields.admin_cmd_state[1]))
print("admin_cmd_state[2]: ", hex(pf_dbg_t.fields.admin_cmd_state[2]))
print("admin_cmd_state[3]: ", hex(pf_dbg_t.fields.admin_cmd_state[3]))
print("admin_cmd_state[4]: ", hex(pf_dbg_t.fields.admin_cmd_state[4]))
print("admin_cmd_state[5]: ", hex(pf_dbg_t.fields.admin_cmd_state[5]))
print("admin_cmd_state[6]: ", hex(pf_dbg_t.fields.admin_cmd_state[6]))
print("admin_cmd_state[7]: ", hex(pf_dbg_t.fields.admin_cmd_state[7]))
print("admin_cmd_state[8]: ", hex(pf_dbg_t.fields.admin_cmd_state[8]))
print("admin_cmd_state[9]: ", hex(pf_dbg_t.fields.admin_cmd_state[9]))
print("admin_cmd_state[10]: ", hex(pf_dbg_t.fields.admin_cmd_state[10]))
print("admin_cmd_state[11]: ", hex(pf_dbg_t.fields.admin_cmd_state[11]))
print("admin_cmd_state[12]: ", hex(pf_dbg_t.fields.admin_cmd_state[12]))
print("admin_cmd_state[13]: ", hex(pf_dbg_t.fields.admin_cmd_state[13]))
print("admin_cmd_state[14]: ", hex(pf_dbg_t.fields.admin_cmd_state[14]))
print("admin_cmd_state[15]: ", hex(pf_dbg_t.fields.admin_cmd_state[15]))
print("admin_cmd_state[16]: ", hex(pf_dbg_t.fields.admin_cmd_state[16]))
print("admin_cmd_state[17]: ", hex(pf_dbg_t.fields.admin_cmd_state[17]))
print("admin_cmd_state[18]: ", hex(pf_dbg_t.fields.admin_cmd_state[18]))
print("admin_cmd_state[19]: ", hex(pf_dbg_t.fields.admin_cmd_state[19]))
print("admin_cmd_state[20]: ", hex(pf_dbg_t.fields.admin_cmd_state[20]))
print("admin_cmd_state[21]: ", hex(pf_dbg_t.fields.admin_cmd_state[21]))
print("admin_cmd_state[22]: ", hex(pf_dbg_t.fields.admin_cmd_state[22]))
print("admin_cmd_state[23]: ", hex(pf_dbg_t.fields.admin_cmd_state[23]))
print("smp_switch_stage[0]: ", hex(pf_dbg_t.fields.smp_switch_stage[0]))
print("smp_switch_stage[1]: ", hex(pf_dbg_t.fields.smp_switch_stage[1]))
print("smp_switch_stage[2]: ", hex(pf_dbg_t.fields.smp_switch_stage[2]))
print("smp_switch_stage[3]: ", hex(pf_dbg_t.fields.smp_switch_stage[3]))
print("smp_switch_stage[4]: ", hex(pf_dbg_t.fields.smp_switch_stage[4]))
print("smp_switch_stage[5]: ", hex(pf_dbg_t.fields.smp_switch_stage[5]))
print("smp_switch_stage[6]: ", hex(pf_dbg_t.fields.smp_switch_stage[6]))
print("smp_switch_stage[7]: ", hex(pf_dbg_t.fields.smp_switch_stage[7]))
print("active_task_idx[0]: ", hex(pf_dbg_t.fields.active_task_idx[0]))
print("active_task_idx[1]: ", hex(pf_dbg_t.fields.active_task_idx[1]))
print("active_task_idx[2]: ", hex(pf_dbg_t.fields.active_task_idx[2]))
print("active_task_idx[3]: ", hex(pf_dbg_t.fields.active_task_idx[3]))
print("active_task_idx[4]: ", hex(pf_dbg_t.fields.active_task_idx[4]))
print("active_task_idx[5]: ", hex(pf_dbg_t.fields.active_task_idx[5]))
print("active_task_idx[6]: ", hex(pf_dbg_t.fields.active_task_idx[6]))
print("active_task_idx[7]: ", hex(pf_dbg_t.fields.active_task_idx[7]))
print("core_smpmgr_state[0]: ", hex(pf_dbg_t.fields.core_smpmgr_state[0]))
print("core_smpmgr_state[1]: ", hex(pf_dbg_t.fields.core_smpmgr_state[1]))
print("core_smpmgr_state[2]: ", hex(pf_dbg_t.fields.core_smpmgr_state[2]))
print("core_smpmgr_state[3]: ", hex(pf_dbg_t.fields.core_smpmgr_state[3]))
print("core_smpmgr_state[4]: ", hex(pf_dbg_t.fields.core_smpmgr_state[4]))
print("core_smpmgr_state[5]: ", hex(pf_dbg_t.fields.core_smpmgr_state[5]))
print("core_smpmgr_state[6]: ", hex(pf_dbg_t.fields.core_smpmgr_state[6]))
print("core_smpmgr_state[7]: ", hex(pf_dbg_t.fields.core_smpmgr_state[7]))
print("io_clear_flags: ", hex(pf_dbg_t.fields.io_clear_flags))
print("b2n_clear_flags: ", hex(pf_dbg_t.fields.b2n_clear_flags))
print("switch_from_state: ", hex(pf_dbg_t.fields.switch_from_state))
print("fct_free_cnt[0]: ", hex(pf_dbg_t.fields.fct_free_cnt[0]))
print("fct_free_cnt[1]: ", hex(pf_dbg_t.fields.fct_free_cnt[1]))
print("fct_free_cnt[2]: ", hex(pf_dbg_t.fields.fct_free_cnt[2]))
print("fct_free_cnt[3]: ", hex(pf_dbg_t.fields.fct_free_cnt[3]))
print("fct_free_cnt[4]: ", hex(pf_dbg_t.fields.fct_free_cnt[4]))
print("fct_free_cnt[5]: ", hex(pf_dbg_t.fields.fct_free_cnt[5]))
print("fct_free_cnt[6]: ", hex(pf_dbg_t.fields.fct_free_cnt[6]))
print("fct_free_cnt[7]: ", hex(pf_dbg_t.fields.fct_free_cnt[7]))
print("fct_free_cnt[8]: ", hex(pf_dbg_t.fields.fct_free_cnt[8]))
print("fct_free_cnt[9]: ", hex(pf_dbg_t.fields.fct_free_cnt[9]))
print("fct_free_cnt[10]: ", hex(pf_dbg_t.fields.fct_free_cnt[10]))
print("fct_free_cnt[11]: ", hex(pf_dbg_t.fields.fct_free_cnt[11]))
print("fct_free_cnt[12]: ", hex(pf_dbg_t.fields.fct_free_cnt[12]))
print("fct_free_cnt[13]: ", hex(pf_dbg_t.fields.fct_free_cnt[13]))
print("fct_free_cnt[14]: ", hex(pf_dbg_t.fields.fct_free_cnt[14]))
print("fct_free_cnt[15]: ", hex(pf_dbg_t.fields.fct_free_cnt[15]))
print("fct_free_cnt[16]: ", hex(pf_dbg_t.fields.fct_free_cnt[16]))
print("fct_free_cnt[17]: ", hex(pf_dbg_t.fields.fct_free_cnt[17]))
print("fct_free_cnt[18]: ", hex(pf_dbg_t.fields.fct_free_cnt[18]))
print("fct_free_cnt[19]: ", hex(pf_dbg_t.fields.fct_free_cnt[19]))
print("fct_free_cnt[20]: ", hex(pf_dbg_t.fields.fct_free_cnt[20]))
print("fct_free_cnt[21]: ", hex(pf_dbg_t.fields.fct_free_cnt[21]))
print("fct_free_cnt[22]: ", hex(pf_dbg_t.fields.fct_free_cnt[22]))
print("fct_free_cnt[23]: ", hex(pf_dbg_t.fields.fct_free_cnt[23]))
print("fct_free_cnt[24]: ", hex(pf_dbg_t.fields.fct_free_cnt[24]))
print("fct_free_cnt[25]: ", hex(pf_dbg_t.fields.fct_free_cnt[25]))
print("fct_free_cnt[26]: ", hex(pf_dbg_t.fields.fct_free_cnt[26]))
print("fct_free_cnt[27]: ", hex(pf_dbg_t.fields.fct_free_cnt[27]))
print("fct_free_cnt[28]: ", hex(pf_dbg_t.fields.fct_free_cnt[28]))
print("fct_free_cnt[29]: ", hex(pf_dbg_t.fields.fct_free_cnt[29]))
print("fct_free_cnt[30]: ", hex(pf_dbg_t.fields.fct_free_cnt[30]))
print("fct_free_cnt[31]: ", hex(pf_dbg_t.fields.fct_free_cnt[31]))
print("pf_event_start: ", hex(pf_dbg_t.fields.pf_event_start))
print("task_clear_after_hw_flag: ", hex(pf_dbg_t.fields.task_clear_after_hw_flag))
print("pf_chk_power_fail_reg_cnt: ", hex(pf_dbg_t.fields.pf_chk_power_fail_reg_cnt))
print("events_chk_flags: ", hex(pf_dbg_t.fields.events_chk_flags))
print("b2n_sent_cnt[0]: ", hex(pf_dbg_t.fields.b2n_sent_cnt[0]))
print("b2n_sent_cnt[1]: ", hex(pf_dbg_t.fields.b2n_sent_cnt[1]))
print("b2n_sent_cnt[2]: ", hex(pf_dbg_t.fields.b2n_sent_cnt[2]))
print("b2n_sent_cnt[3]: ", hex(pf_dbg_t.fields.b2n_sent_cnt[3]))
print("b2n_sent_cnt[4]: ", hex(pf_dbg_t.fields.b2n_sent_cnt[4]))
print("b2n_sent_cnt[5]: ", hex(pf_dbg_t.fields.b2n_sent_cnt[5]))
print("b2n_sent_cnt[6]: ", hex(pf_dbg_t.fields.b2n_sent_cnt[6]))
print("b2n_sent_cnt[7]: ", hex(pf_dbg_t.fields.b2n_sent_cnt[7]))
print("hot_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail : ", hex(pf_dbg_t.fields.hot_b2n_fcq[0]), hex(pf_dbg_t.fields.hot_b2n_fcq[1]),hex(pf_dbg_t.fields.hot_b2n_fcq[2]),hex(pf_dbg_t.fields.hot_b2n_fcq[3]))
print("tcg_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail : ", hex(pf_dbg_t.fields.tcg_b2n_fcq[0]), hex(pf_dbg_t.fields.tcg_b2n_fcq[1]),hex(pf_dbg_t.fields.tcg_b2n_fcq[2]),hex(pf_dbg_t.fields.tcg_b2n_fcq[3]))
print("gc_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail : ", hex(pf_dbg_t.fields.gc_b2n_fcq[0]), hex(pf_dbg_t.fields.gc_b2n_fcq[1]),hex(pf_dbg_t.fields.gc_b2n_fcq[2]),hex(pf_dbg_t.fields.gc_b2n_fcq[3]))
print("wl_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail : ", hex(pf_dbg_t.fields.wl_b2n_fcq[0]), hex(pf_dbg_t.fields.wl_b2n_fcq[1]),hex(pf_dbg_t.fields.wl_b2n_fcq[2]),hex(pf_dbg_t.fields.wl_b2n_fcq[3]))
print("eh_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail : ", hex(pf_dbg_t.fields.eh_b2n_fcq[0]), hex(pf_dbg_t.fields.eh_b2n_fcq[1]),hex(pf_dbg_t.fields.eh_b2n_fcq[2]),hex(pf_dbg_t.fields.eh_b2n_fcq[3]))
print("footer_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail : ", hex(pf_dbg_t.fields.footer_b2n_fcq[0]), hex(pf_dbg_t.fields.footer_b2n_fcq[1]),hex(pf_dbg_t.fields.footer_b2n_fcq[2]),hex(pf_dbg_t.fields.footer_b2n_fcq[3]))
print("l2p_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail : ", hex(pf_dbg_t.fields.l2p_b2n_fcq[0]), hex(pf_dbg_t.fields.l2p_b2n_fcq[1]),hex(pf_dbg_t.fields.l2p_b2n_fcq[2]),hex(pf_dbg_t.fields.l2p_b2n_fcq[3]))
print("patch_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail : ", hex(pf_dbg_t.fields.patch_b2n_fcq[0]), hex(pf_dbg_t.fields.patch_b2n_fcq[1]),hex(pf_dbg_t.fields.patch_b2n_fcq[2]),hex(pf_dbg_t.fields.patch_b2n_fcq[3]))
print("rfs_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail : ", hex(pf_dbg_t.fields.rfs_b2n_fcq[0]), hex(pf_dbg_t.fields.rfs_b2n_fcq[1]),hex(pf_dbg_t.fields.rfs_b2n_fcq[2]),hex(pf_dbg_t.fields.rfs_b2n_fcq[3]))
print("pf_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail : ", hex(pf_dbg_t.fields.pf_b2n_fcq[0]), hex(pf_dbg_t.fields.pf_b2n_fcq[1]),hex(pf_dbg_t.fields.pf_b2n_fcq[2]),hex(pf_dbg_t.fields.pf_b2n_fcq[3]))
print("share_b2n_fcq sq_hd, sq_tail, cq_hd, cq_tail : ", hex(pf_dbg_t.fields.share_b2n_fcq[0]), hex(pf_dbg_t.fields.share_b2n_fcq[1]),hex(pf_dbg_t.fields.share_b2n_fcq[2]),hex(pf_dbg_t.fields.share_b2n_fcq[3]))
print("rmw_mgr_state: ", hex(pf_dbg_t.fields.rmw_mgr_state))
print("no_b2n_strm: ", hex(pf_dbg_t.fields.no_b2n_strm))
print("dump_to_nor_timecost: ", hex(pf_dbg_t.fields.dump_to_nor_timecost))


