```
#define PF_META_DATA_PBA_CNT          (50)
> meta_data_start_pba: 20802000      
> meta_data_end_pba: 20802c80          3200
> summary_data_pba: 20802cc0
> user_data_start_pba: 20801800      > user_data_end_pba: 20801880 
> parity_data_start_pba: 208018c0    > parity_data_end_pba: 20801980  192
```

```
#define PF_META_DATA_PBA_CNT          (45)
Summary Data pba: 0x1801440
Last Written pba: 0x18017c0
Meta data pba: 0x1800800 to 0x1801400  3072

Summary Data pba: 0x1802c40
Last Written pba: 0x1802fc0
Meta data pba: 0x1802000 to 0x1802c00  3072

/home/tcnsh/samanea/Samanea_Platform/spdk/scripts/setup.sh reset
root@192.168.33.36:/home/tcnsh/junw/myrtle_bin/bl3.bin
```

```
BOMB(PF_RW_DDR_BUF_OVERFLOW, ((u64)_pf_user_data_rw_buf_start + (4128 * CFG_ONE_PROG_NUM)) <= PF_RW_DDR_BUF_END);
4128*16*3=4128*48 = 4.1*48k= 194k
PF_FLUSH_META_DATA_SIZE = PF_META_DATA_PBA_CNT*64k
PF_FLUSH_SUMMARY_DATA_SIZE = 64k
PF_FLUSH_ROOT_DATA_SIZE = 4k
padding_size = 4k

总共4M= 4*1024k=4096k
4096k-4k-4k-64k-194k=3830k
3830k/64k=59.84

_pf_meta_data_rw_buf_start    = (u8 *)PF_RW_DDR_BUF_START;
_pf_summary_data_rw_buf_start = (u8 *)_pf_meta_data_rw_buf_start + ROUND_UP(PF_FLUSH_META_DATA_SIZE, DATA_LEN_4K);
_pf_root_data_rw_buf_start = (u8 *)_pf_summary_data_rw_buf_start + ROUND_UP(PF_FLUSH_SUMMARY_DATA_SIZE, DATA_LEN_4K);
_pf_padding_data_rw_buf_start = (u8 *)_pf_root_data_rw_buf_start + ROUND_UP(PF_FLUSH_ROOT_DATA_SIZE, DATA_LEN_4K);
_pf_user_data_rw_buf_start    = (u8 *)_pf_padding_data_rw_buf_start + DATA_LEN_4K;
大小端的问题
```

```
u16 test[4];
debug_data->test[0]            = 1;
debug_data->test[1]            = 2;
debug_data->test[2]            = 3;
debug_data->test[3]            = 4;
0x00020001 0x00040003

u8 test[4];
debug_data->test[0]            = 1;
debug_data->test[1]            = 2;
debug_data->test[2]            = 3;
debug_data->test[3]            = 4;
0x04030201
```
Dump meta data to pba 0x1806800, _flush_index 0
Dump meta data to pba 0x1806840, _flush_index 1
Dump meta data to pba 0x1806880, _flush_index 2
Dump meta data to pba 0x18068c0, _flush_index 3
Dump meta data to pba 0x1806900, _flush_index 4
Dump meta data to pba 0x1806940, _flush_index 5
Dump meta data to pba 0x1806980, _flush_index 6
Dump meta data to pba 0x18069c0, _flush_index 7
Dump meta data to pba 0x1806a00, _flush_index 8
Dump meta data to pba 0x1806a40, _flush_index 9
Dump meta data to pba 0x1806a80, _flush_index 10
Dump meta data to pba 0x1806ac0, _flush_index 11
Dump meta data to pba 0x1806b00, _flush_index 12
Dump meta data to pba 0x1806b40, _flush_index 13
Dump meta data to pba 0x1806b80, _flush_index 14
Dump meta data to pba 0x1806bc0, _flush_index 15
Dump meta data to pba 0x1806c00, _flush_index 16
Dump meta data to pba 0x1806c40, _flush_index 17
Dump meta data to pba 0x1806c80, _flush_index 18
Dump meta data to pba 0x1806cc0, _flush_index 19
Dump meta data to pba 0x1806d00, _flush_index 20
Dump meta data to pba 0x1806d40, _flush_index 21
Dump meta data to pba 0x1806d80, _flush_index 22
Dump meta data to pba 0x1806dc0, _flush_index 23
Dump meta data to pba 0x1806e00, _flush_index 24
Dump meta data to pba 0x1806e40, _flush_index 25
Dump meta data to pba 0x1806e80, _flush_index 26
Dump meta data to pba 0x1806ec0, _flush_index 27
Dump meta data to pba 0x1806f00, _flush_index 28
Dump meta data to pba 0x1806f40, _flush_index 29
Dump meta data to pba 0x1806f80, _flush_index 30
Dump meta data to pba 0x1807000, _flush_index 31
Dump meta data to pba 0x1807040, _flush_index 32
Dump meta data to pba 0x1807080, _flush_index 33
Dump meta data to pba 0x18070c0, _flush_index 34
Dump meta data to pba 0x1807100, _flush_index 35
Dump meta data to pba 0x1807140, _flush_index 36
Dump meta data to pba 0x1807180, _flush_index 37
Dump meta data to pba 0x18071c0, _flush_index 38
Dump meta data to pba 0x1807200, _flush_index 39
Dump meta data to pba 0x1807240, _flush_index 40
Dump meta data to pba 0x1807280, _flush_index 41
Dump meta data to pba 0x18072c0, _flush_index 42
Dump meta data to pba 0x1807300, _flush_index 43
Meta data pba: 0x1806800 to 0x1807300

        // MYR-2345 workaround, poll perst every time before task run
        if ((g_kernel.get_admin_smpmgr()->get_current_state() != SMP_STATE_PD) &&
            bsp_platform::is_task_scheduler_polling_perst()) {
            if (bsp_platform::nfe_get_dualporten_pin_status() == 0) {
                bsp_platform::nfe_reset_lane(true);
                bsp_platform::nfe_reset_phy(true);
                bsp_platform::set_task_scheduler_polling_perst(false);
            }
        }
    // disable lane and reset phy
    bsp_platform::nfe_reset_lane(true);
    bsp_platform::nfe_reset_phy(true);
