```c++
u32 pf_restore_cleaner::clean_up_nor()
{
    u32 total_sub_sector_cnt = NOR_SECTOR_SIZE / NOR_SUB_SECTOR_SIZE;
    u64 pf_restore_cpl_signature = PF_RESTORE_COMPLETE_SIGNATURE;

    u32 ping_addr = (NOR_PF_DATA_PING_SECTOR * NOR_SECTOR_SIZE) + (g_pf_mgr.root_data.nor_sub_sector_idx * NOR_SUB_SECTOR_SIZE);
    u32 pong_addr = (NOR_PF_DATA_PONG_SECTOR * NOR_SECTOR_SIZE) + (g_pf_mgr.root_data.nor_sub_sector_idx * NOR_SUB_SECTOR_SIZE);

    BOMB(PF_RESTORE_CLEAN_UP_NOR, true);

    // Mark current 4k subsector as invalid
    u32 nor_addr = (ping_addr + offsetof(pf_nor_data, signature2));
    u32 data_size = NOR_ACCESS_LEN(sizeof(u64));
    u32 *data_ptr  = (u32 *)&pf_restore_cpl_signature;
    NOR_OPERATION_STATUS_E job_status = 
        (NOR_OPERATION_STATUS_E)bsp_platform::spi_nor_write_data(nor_addr, data_ptr, data_size);
        //g_nor_task.submit_nor_write_and_poll_cmpl(nor_addr, data_ptr, data_size, false);
    if (job_status != NOR_OPERATION_SUCCESS) {
        INF_LOG("%s, ping write compl op failed\n", __FUNCTION__);
    }

    nor_addr = (pong_addr + offsetof(pf_nor_data, signature2));
    job_status = (NOR_OPERATION_STATUS_E)bsp_platform::spi_nor_write_data(nor_addr, data_ptr, data_size);
        //g_nor_task.submit_nor_write_and_poll_cmpl(nor_addr, data_ptr, data_size, false);
    if (job_status != NOR_OPERATION_SUCCESS) {
        INF_LOG("%s, pong write compl op failed\n", __FUNCTION__);
    }

    // update the data for next time write
    g_pf_mgr.root_data.nor_sub_sector_idx = (g_pf_mgr.root_data.nor_sub_sector_idx + 1) % total_sub_sector_cnt;
    EVT_LOG(nvm_subsystem_unsafe_shutdown_evtlog, g_pf_mgr.root_data.pf_cnt);

    // erase the next 4k subsector for next time write
    ping_addr = (NOR_PF_DATA_PING_SECTOR * NOR_SECTOR_SIZE) + (g_pf_mgr.root_data.nor_sub_sector_idx * NOR_SUB_SECTOR_SIZE);
    pong_addr = (NOR_PF_DATA_PONG_SECTOR * NOR_SECTOR_SIZE) + (g_pf_mgr.root_data.nor_sub_sector_idx * NOR_SUB_SECTOR_SIZE);
    job_status = (NOR_OPERATION_STATUS_E)bsp_platform::spi_nor_4k_erase(ping_addr);
        //g_nor_task.submit_nor_erase_and_poll_cmpl(ping_addr, false, false);
    if (job_status != NOR_OPERATION_SUCCESS) {
        INF_LOG("%s, ping nor op failed\n", __FUNCTION__);
    }
    job_status = (NOR_OPERATION_STATUS_E)bsp_platform::spi_nor_4k_erase(pong_addr);
        //g_nor_task.submit_nor_erase_and_poll_cmpl(pong_addr, false, false);
    if (job_status != NOR_OPERATION_SUCCESS) {
        INF_LOG("%s, pong nor op failed\n", __FUNCTION__);
    }

    return PF_RESTORE_NO_ERROR;
}
```

```
Solve the problem that enable PF dump has not been opened when PF is clean up nor
e6c0e10c0bd947d85042acd525b12284fa2649a5
```

```
        case CLEAN_UP_NOR_SEND_MAIN_WRITE_NOR: {
            // Mark current 4k subsector as invalid

            u32 ping_addr  =  (NOR_PF_DATA_PING_SECTOR * NOR_SECTOR_SIZE) + (g_pf_mgr.root_data.nor_sub_sector_idx * NOR_SUB_SECTOR_SIZE);

            u32 nor_addr   =  (ping_addr + offsetof(pf_nor_data, signature2));

            u32 data_size  =  NOR_ACCESS_LEN(sizeof(u64));

            u32 *data_ptr  =  (u32 *)&pf_restore_cpl_signature;

            _nor_job_id    =  g_nor_task.submit_nor_write_job(nor_addr, data_ptr, data_size, false);

            if (_nor_job_id != INVALID_32BIT) {

                _nor_cleanup_state = CLEAN_UP_NOR_SEND_MAIN_WRITE_NOR_WAIT;

            } else {

                _nor_cleanup_state = CLEAN_UP_NOR_SEND_MAIN_WRITE_NOR;

            }


           break;

        }


        case CLEAN_UP_NOR_SEND_MAIN_WRITE_NOR_WAIT: {
            NOR_OPERATION_STATUS_E job_status = NOR_OPERATION_STATUS_UNKNOWN;

            NOR_CHECK_JOB_RESULT_E result = g_nor_task.nor_check_job_done(_nor_job_id, job_status);

            if (result == NOR_CURRENT_JOB_DONE) {

                g_nor_task.nor_release_job_id(_nor_job_id);

                _nor_cleanup_state = CLEAN_UP_NOR_SEND_MIRROR_WRITE_NOR;

            } else {

                _nor_cleanup_state = CLEAN_UP_NOR_SEND_MAIN_WRITE_NOR_WAIT;

            }


            break;

        }


        case CLEAN_UP_NOR_SEND_MIRROR_WRITE_NOR: {
            // Mark current 4k subsector as invalid

            u32 pong_addr  =  (NOR_PF_DATA_PONG_SECTOR * NOR_SECTOR_SIZE) + (g_pf_mgr.root_data.nor_sub_sector_idx * NOR_SUB_SECTOR_SIZE);

            u32 nor_addr   =  (pong_addr + offsetof(pf_nor_data, signature2));

            u32 data_size  =  NOR_ACCESS_LEN(sizeof(u64));

            u32 *data_ptr  =  (u32 *)&pf_restore_cpl_signature;

            _nor_job_id    =  g_nor_task.submit_nor_write_job(nor_addr, data_ptr, data_size, false);

            if (_nor_job_id != INVALID_32BIT) {

                _nor_cleanup_state = CLEAN_UP_NOR_SEND_MIRROR_WRITE_NOR_WAIT;

            } else {

                _nor_cleanup_state = CLEAN_UP_NOR_SEND_MIRROR_WRITE_NOR;

            }


            break;

        }


        case CLEAN_UP_NOR_SEND_MIRROR_WRITE_NOR_WAIT: {
            NOR_OPERATION_STATUS_E job_status = NOR_OPERATION_STATUS_UNKNOWN;

            NOR_CHECK_JOB_RESULT_E result = g_nor_task.nor_check_job_done(_nor_job_id, job_status);

            if (result == NOR_CURRENT_JOB_DONE) {

                u32 total_sub_sector_cnt = NOR_SECTOR_SIZE / NOR_SUB_SECTOR_SIZE;

                g_pf_mgr.root_data.nor_sub_sector_idx = (g_pf_mgr.root_data.nor_sub_sector_idx + 1) % total_sub_sector_cnt;


                g_nor_task.nor_release_job_id(_nor_job_id);

                _nor_cleanup_state = CLEAN_UP_NOR_SEND_MAIN_ERASE_NOR;

            } else {

                _nor_cleanup_state = CLEAN_UP_NOR_SEND_MIRROR_WRITE_NOR_WAIT;

            }


           break;

        }
```

```
1170685 ^[[0K[5-0:0:0D|0:0:4:7]   pf_recovery_sanity_check_start:
1170686 ^[[0K
1170687 ^[[0K
1170688 ^[[0K[4-0:0:0D|0:0:10:0]   risk_low_space:
1170689 ^[[0K  > _usage_phy:          9a29c27340
1170690 ^[[0K  > _usage_logic:        b7ec68d000
1170691 ^[[0K  > _total_logic_size:   1467e92e000
1170692 ^[[0K  > _total_phy_size:     a33f9c2000
1170693 ^[[0K  > _total_free_blk:           11
1170694 ^[[0K
1170695 ^[[0K
1170696 ^[[0K[4-0:0:0D|0:0:10:0]   risk_board:
1170697 ^[[0K  > _action:             RISK_BOARD_SET_RISK
1170698 ^[[0K  > _evt_bitmap:              800
1170699 ^[[0K
1170700 ^[[0K[4-0:0:0D|0:0:10:0]        Bkops:                    >>> Event Log:  nvm_subsystem_critical_warn
1170701 ^[[0K[4-0:0:0D|0:0:10:0]        Bkops:                        > heec:          6
1170702 ^[[0K[4-0:0:0D|0:0:10:0]        Bkops:                       > critical_warn: 1
1170703 ^[[0K
1170704 ^[[0K[5-0:0:0D|0:0:19:12]   pf_recovery_sanity_check_complete:
```

![image-20220419153432135](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220419153432135.png)
