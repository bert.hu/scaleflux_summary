```c++
如何打印info log
void logger::log(PRINT_LEVEL_E lvl, const char *fmt, ...)
{
    // only when log level smaller than our logger's level, we do print
    if ((g_enable_logger || g_vu_force_output_logger) && (lvl <= this->_lvl)) {
        log_entry entry_buf;

        // fill its header
        log_formatter(entry_buf, COLOR_DEFAULT, lvl, bt_depth());

        // prepare to write messages
        va_list args;
        va_start(args, fmt);
        vsnprintf((char *)&entry_buf.msg, sizeof(log_entry::msg), fmt, args);
        va_end(args);

        if (lvl >= DEBUG) {
            // if log's level bigger than DEBUG, then we force print out, so we will not lost logs
            // and make debug easiler
            print_log_out(&entry_buf);
        }
        else {
            // note
            //     normal log print will be print to log buffer first, if print too fast, log buffer will be full
            //     and logs will be overwritten!
            g_txt_logbuf.add_log(entry_buf);
            g_dump_logbuf.add_log(entry_buf);
        }
    }
}

void uart_print_out_enable(void)
{
    g_uart_print_enabled = true;
}
bsp_platform::print_out_enable();
bsp_platform::print_out_disable();
void trigger_power_fail(void)
{
    INF_LOG("begin trigger power fail...\n");
    // def_console_task.disable_console();
    // bsp_platform::print_out_disable();
    #if __WITH_SIMULATOR__ == 1
    g_pf_dump_task.set_pf_event_start();
    softirq::raise_softirq(SOFTIRQ_PF);
    #endif
    bsp_platform::trigger_pf();
}
```

```
getthrtlinfo 看看io和gc是不是有credit
```

#### 串口的打印

```c
bool bsp_platform::is_print_out_enabld(void) { return is_uart_print_out_enabled(); }

void bsp_platform::print_out_enable(void) { uart_print_out_enable(); }

void bsp_platform::print_out_disable(void) { uart_print_out_disable(); }

bool is_uart_print_out_enabled(void)
{
    return g_uart_print_enabled;
}

void uart_print_out_enable(void)
{
    g_uart_print_enabled = true;
}

void uart_print_out_disable(void)
{
    g_uart_print_enabled = false;
}

void putchar_serial(int data)
{
    if (!g_uart_init_done || !g_uart_print_enabled)
        return;
    UART_REG_WR(DEBUG_UART_PORT, UARTDR_ADDR, (u8)data);

    // wait BUSY flag set to zero
    while ((UART_REG_RD(DEBUG_UART_PORT, UARTFR_ADDR) & BIT(3)) != 0)
        ;
}

void putstr_len(const char *msg, int len)
{
    const char *p = (const char *)msg;

    while (len) {
        putchar_serial(*p++);
        len--;
    }
}
```

#### 中断的函数

```
// irq handlers
void handle_uart0_isr(u8 c);
void handle_uart1_isr(u8 c);
void handle_power_fail_isr(void);

softirq_entry softirq_array[64] __cacheline_aligned = {
    {&softirq_poweron_handler,              0},
    {&softirq_powerdn_handler,              0},
    {&softirq_abrtpowerdn_handler,          0},
    {&softirq_hotrst_handler,               0},
    {&softirq_smpstate_change_handler,      0},
    {&softirq_conrst_handler,               0},
    {&softirq_assert_handler,               0},
    {&softirq_powerfail_handler,            0},
    {nullptr,                               0},
};

// this need implement at each aarch64 platform
void arch_handle_interrupts(void);

vu_handler_fakepf
pf （默认mode=0）

pf 0:
#if __WITH_SIMULATOR__ == 1
        g_pf_dump_task.set_pf_event_start();
        softirq::raise_softirq(SOFTIRQ_PF);
#endif
        bsp_platform::trigger_pf();

pf 1:
        g_pf_dump_task.set_pf_fake_flag();
        g_pf_dump_task.set_pf_event_start();
        softirq::raise_softirq(SOFTIRQ_PF);
pf 2或其他:
        bsp_platform::trigger_pf(); // pmic操作
```

#### 中断处理函数

```c++
    static inline SMP_STATE_E do_softirq(SMP_STATE_E cur_admin_state)
    {
        SMP_STATE_E new_state = SMP_STATE_NONE;

        u64 act_flag = _active_flag.atomic_read();
        FOR_EACH_BIT_IN_FLAG(bit, act_flag)
        {
            /*
             * exec the softirq handler and get the new system state
             * note there maybe multiple softirq happen at same time
             * different softirq maybe want system goto different state,
             *
             * But we only handle one softirq at one time, we once we find a active bit, handle it
             * and then just return
             */
            // if we are in PF state, when we will not change to any other
            // 如果是SMP_STATE_PF，则不进行状态切换
            if (cur_admin_state == SMP_STATE_PF) {
                // Finish it, then clear this soft irq
                clear_softirq((SOFT_IRQ_TYPE_E)bit);
                return cur_admin_state;
            }
            else {
                new_state = _irq_array[bit].handler(cur_admin_state, _irq_array[bit].param);
                if (new_state != SMP_STATE_NONE) {
                    // Finish it, then clear this soft irq
                    clear_softirq((SOFT_IRQ_TYPE_E)bit);
                }
                return new_state;
            }
        }

        return new_state;
    };
```

