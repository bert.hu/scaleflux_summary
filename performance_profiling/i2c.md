```
    /*
     * - i2c master interface
     */
    static inline void  i2c_set_data(u32 data)  {_i2c_data = data; };
    static inline u32   i2c_get_data(void)      { return _i2c_data; };
    static void         i2c_init_main(void);
    static void         i2c_reset(u32 i2c_bus_id);
    static u8           i2c_master_write(u32 i2c_bus_id, u8 device_addr, u8 reg_addr, u8* data, u8 len);
    static u8           i2c_master_read(u32 i2c_bus_id, u8 device_addr, u8 reg_addr, u8* data, u8 len);
    static void         i2c_handle_timer_info(void);
    static u32  get_i2c_data() { return _i2c_data; }
    
    i2c_master_write_for_pf_cmd
    i2c_init
    
    __DISABLE_I2C_INTERFACE__
#if __DISABLE_I2C_INTERFACE__ == 1
    return true;
#endif


i2c_master_write_for_pf_cmd
i2c_master_write
i2c_master_read
```

