#### 先从PF debug data看起

```
    for (u32 i = 0; i < MAX_TASK_NUMBER_IN_SCHEDULER; i++) {
        if (task_tbl[i]._ptask != nullptr) {
            debug_data->task_states[i] = task_tbl[i]._ptask->get_task_state();
        }
        else {
            debug_data->task_states[i] = INVALID_8BIT;
        }
    }

    for (u32 i = 0; i < PLATFORM_CORE_COUNT; i++) {
        task_base* ptask;

        if (i == bsp_platform::get_admin_coreid()) {
            debug_data->smp_switch_stage[i] = g_kernel.get_admin_smpmgr_switch_stage();
            ptask = g_kernel.get_admin_sched()->get_active_task();
        }
        else {
            debug_data->smp_switch_stage[i] = g_kernel.get_slave_smpmgr_switch_stage(i);
            ptask = g_kernel.get_slave_sched(i)->get_active_task();
        }

        if (ptask) {
            debug_data->active_task_idx[i] = ptask->get_index_of_array();
        }
        else {
            debug_data->active_task_idx[i] = 0xFF;
        }
    }

    debug_data->softirq_active_flag = softirq::get_softirq_active_flag();
    debug_data->smp_state = g_kernel.get_admin_smpmgr_state();
```

#### GDB中调试

```
(gdb) p task_tbl[0]._ptask .
_asserted                  _task_logger               get_assertid               get_tgt_smp_state          set_assertid
_depended_bitmap           _task_name                 get_current_smp_state      inactive                   set_depending_bitmap
_depending_bitmap          _task_state_cost           get_depended_bitmap        init                       start
_idx_of_task_array         _vptr.task_base            get_depending_bitmap       is_active                  stop
_next                      active                     get_index_of_array         is_asserted                task_name
_next_sched_time           add_depended               get_logger                 operator=                  wait_for_child
_parent                    add_depended_task          get_next_task              pause                      wakeup_parent
_scheduler                 check_event_handle_status  get_parent_task            recovery_from_assert       ~task_base
_smp_state_resume_to       check_status               get_scheduler              reset
_start_sched_time          clr_assert                 get_task_state             resume
_state                     diagself                   get_task_state_cost        run

(gdb) p task_tbl[0]._ptask ._asserted
$21 = FIRST_ASSERT_ID
(gdb) p task_tbl[0]._ptask ._idx_of_task_array
$22 = 0
(gdb) p task_tbl[0]._ptask ._smp_state_resume_to
$23 = SMP_STATE_RUNNING
(gdb) p task_tbl[0]._ptask ._state
$24 = 0
(gdb) p task_tbl[0]._ptask ._task_name
$25 = 0x5b0bf0 "io_task0"

```

```
show_all_task_status



```

```
// init timeout check parameters
    auto init_timeout_chk = [&](void) {
    start_tick = bsp_platform::get_time_us();
    print_out  = true;
};

    // do timeout check
    auto do_timeout_chk = [&](u64 timeout_val) {
        // check whether timeout
        if (print_out) {
            // if really timeout, then check_smp_mgr_timeout will return true, so print out will be false,
            // then it will not do timeout check again
            print_out = !check_smp_mgr_timeout(start_tick, timeout_val);
        }
    };
```



#### 其他DDR信息

```
/*
 * Define the DDR space
 */
// real ddr start address
#define DDR_START                       ULL(0x400000000)

// note first 256MB used by elf file, global variable, etc..
// read ddr space can be used is start from here
#define ELF_MAX_SIZE                    ULL(0x100000 * 256)
#define ALLOCATABLE_DDR_START           (DDR_START + ELF_MAX_SIZE)

/*
 *-- For AARCH64 platform, also need has following definitons at
 *   board_def.h
 */

#define SRAM_SIZE                       (DATA_LEN_2M)
#define SRAM_BASE                       ULL(0x7FE00000)

#define PAGE_TABLE_SIZE                 DATA_LEN_512K
#define PAGE_TABLE_BASE_ADDR            (SRAM_BASE + 0x170000)

// Bootloader
#define BL2_BASE                        SRAM_BASE
/*do build check of bl2 size to 136K for pcie init 100ms requirement*/
#define BL2_IMG_LIMIT					(SRAM_BASE + (DATA_LEN_128K + DATA_LEN_8K))
#define BL2_SIZE                        DATA_LEN_256K

// Prodcution FW, first 32B of DDR can't access by FW
#define BL3_BASE                        (DDR_START + 0x1000)
#define BL3_SIZE                        (ALLOCATABLE_DDR_START - BL3_BASE)

#define ROM_SIZE                        (DATA_LEN_64K)
#define ROM_BASE                        ULL(0x0)


_pf_meta_data_rw_buf_start    = (u8 *)PF_RW_DDR_BUF_START;
_pf_summary_data_rw_buf_start = (u8 *)_pf_meta_data_rw_buf_start + ROUND_UP(sizeof(pf_meta_data), DATA_LEN_4K);
_pf_root_data_rw_buf_start = (u8 *)_pf_summary_data_rw_buf_start + ROUND_UP(sizeof(pf_summary_data), DATA_LEN_4K);
_pf_padding_data_rw_buf_start = (u8 *)_pf_root_data_rw_buf_start + ROUND_UP(sizeof(pf_nor_data), DATA_LEN_4K);
_pf_user_data_rw_buf_start    = (u8 *)_pf_padding_data_rw_buf_start + DATA_LEN_4K;

[restore]PF_RW_DDR_BUF_START 17553956864(0x4164C2000), footer_dump_addr 17555025920(0x4165C7000), gap:1044K, sizeof(pf_meta_data) 1068016(0x104BF0,至少261个4K，换算成16进制就是105)

[2022-04-05 03:00:01.724] [5-0:0:0D|0:0:0:598] recovery_tas:                   meta_data 0x4164c2000, summary_data 0x4165c7000, root_data 0x4165d3000

[2022-04-05 03:00:01.736] [5-0:0:0D|0:0:0:598] recovery_tas:                   padding_data 0x4165d4000, user_data 0x4165d5000, buf_end 17558151168(0x4168C2000)

所以可得
ROUND_UP(sizeof(pf_meta_data), DATA_LEN_4K) = 0x105000 = (261*4K)
ROUND_UP(sizeof(pf_summary_data), DATA_LEN_4K) = 0xC000 = (12*4k)
ROUND_UP(sizeof(pf_nor_data), DATA_LEN_4K) = 0x1000 = (4k)
buf_end - user_data = 0x2ED000 = (749*4K)
```

| 名称                             | 内存地址开始  | 对齐大小    | 内存地址终止  |
| -------------------------------- | ------------- | ----------- | ------------- |
| BL1_BASE/ROM_BASE                | 0             | 0x1 0000    | 0x1 0000      |
| BL2_BASE                         | 0x7FE0 0000   | 0x4 0000    | 0x7FE4 0000   |
| BL2_IMG_LIMIT                    | 0x7FE2 2000   |             |               |
| BL1_ELF_BUF_ADDR                 | 0x7FF0 0000   |             |               |
| PAGE_TABLE_BASE_ADDR             | 0x7FF7 0000   | 0x8 0000    | 0x7FFF 0000   |
| CPU_STACK_BASE_ADDR              | 0x7FFF 0000   | 0x1 0000    | 0x8000 0000   |
| SRAM_BASE                        | 0x7FE0 0000   | 0x20 0000   | 0x8000 0000   |
| DDR_START                        | 0x4 0000 0000 |             |               |
| BL3_RUNNING_MODE_SCRASH_REG      | 0x4 0000 0100 |             |               |
| CPU_STACK_BASE_ADDR              | 0x4 0F80 0000 | 0x80 0000   | 0x4 1000 0000 |
| BL3_BASE                         | 0x4 0000 1000 | 0x1000 0000 | 0x4 1000 0000 |
| STATIC_DDR_MAP_ADDRESS_MAP_START | 0x4 1000 0000 |             |               |
| ALLOCATABLE_DDR_START            | 0x4 1000 0000 |             | 0x4 2000 0000 |
| BL2_ELF_BUF_ADDR                 | 0x4 2000 0000 | 0x80 0000   | 0x4 2080 0000 |
| BL2_RFS_BUF_ADDR                 | 0x4 2080 0000 | 0x10 0000   | 0x4 2090 0000 |
| BL2_VU_BUF_ADDR                  | 0x4 2090 0000 | 0x100 0000  | 0x4 2190 0000 |
| BL2_XMODE_BUF_ADDR               | 0x4 2190 0000 |             |               |
