#### 如何定位SHIM ERR的问题？

```
simu_flash_c:                simu_nand_read: #### NAND READ ERR: Meet SHIM ERR at 0x14       909460
recovery_tas:                  RD_ERR: gc_rd scheme 4 fct_idx 1362 overall_status 0x0        crc 0 erase_page 0 rd_cmd_err 0 cwerr 0x0 ccperr 0x0 lperr 0x0 lbamis 0x0 dmaerr 0x0, current vt 0 pbah 2d00 pbal 11cc296
```

```
admin_task:                         Nand config info:
admin_task:                             Num of Block     : 556
admin_task:                             Num of Page      : 712
admin_task:                             Num of Slc Page  : 704
admin_task:                             Num of Page Type : 3
admin_task:                             Num of Plane     : 4
admin_task:                             Num of Chan      : 16
admin_task:                             Num of CE        : 2
admin_task:                             Num of LUN       : 2
admin_task:                             Num of Offset    : 4
admin_task:                             Num of Die       : 64


pbah 100 pbal 1290628
那么这里的nand_addr_t nand_pba = 0x2d00011cc296

看下simu_nand_read这个函数的实现
    pba_t *ppba = (pba_t *)nand_addr;
    u32 die = ppba->ch + (ppba->ce_lun << CFG_PBA_CHAN_BITS);
    u32 blk = ppba->block;
    u32 pl  = ppba->plane;
    nand_addr_t nand_pba = g_be_mgr.pba_to_nand_addr(*ppba);
    u32 page = (slc ? ppba->page :     g_be_mgr.get_small_page_index_by_nand_addr(nand_pba));
    
在文件/home/tcnsh/bert/crash_recovery/myrtle_fw/config/nand_cfg/cfg_2T_sim_nand_b47.h中找到CFG_PBA_CHAN_BITS=2

gdb里面首先通过nand_pba获取block相关信息
(gdb) p (nand_addr_t)(u64)0x2d00011cc296
$5 = {{{{page = 662, plane = 0, block = 115, lun = 1, rsvd = 0}, row_addr = 18662038}, {{la = 0, rsvd2 = 0, page_type = 0, rsvd3 = 0, ln = 1, ce = 3, ch = 2}, high_32bit = 11520}}, all = 49478041911958}


这里可以知道ln = 1, ce = 3, ch = 2
那么根据page = (slc ? ppba->page : g_be_mgr.get_small_page_index_by_nand_addr(nand_pba));
获取page
(gdb) p g_be_mgr.get_small_page_index_by_nand_addr((nand_addr_t)(u64)0x2d00011cc296)
$7 = 662
再根据nand_addr_t nand_pba = g_be_mgr.pba_to_nand_addr(*ppba)
nand_addr_t b47_layout::pba_to_nand_addr_t(pba_t pba, u8 tlc_open)
{
    nand_addr_t nand_addr = {.all = 0};
    nand_addr.ch          = pba.ch;
    nand_addr.ce          = CE_OF_CE_LUN(pba.ce_lun);
    nand_addr.lun         = LUN_OF_CE_LUN(pba.ce_lun);
    nand_addr.ln          = LUN_OF_CE_LUN(pba.ce_lun);
    nand_addr.block       = pba.block;
    nand_addr.page        = _ftl_page_table[tlc_open][pba.page].nand_page_index + pba.page_type;
    // nand_addr.page_type = 0; b47 nand_addr_t has no page type field
    nand_addr.plane = pba.plane;
    nand_addr.la    = (pba.offset << 1) + pba.ep_idx;
    return nand_addr;
}
可以算出来
pba.ch = nand_addr.ch = 2
pba.block = nand_addr.block = 115
CFG_CE_NUM = 4 
CE_OF_CE_LUN(pba.ce_lun) = ((pba.ce_lun) % (CFG_CE_NUM)) = nand_addr.ce = 3
LUN_OF_CE_LUN(pba.ce_lun) = ((pba.ce_lun) / (CFG_CE_NUM)) = nand_addr.ln = 1
所以pba.ce_lun = 7
最后计算die = ppba->ch + (ppba->ce_lun << CFG_PBA_CHAN_BITS) = pba.ch + (pba.ce_lun << 2) = 2 + 28 = 30
所以根据
struct header_content *ptr = NULL;
ptr = (struct header_content *)(nand_mgr.blk_para[die][blk].page_addr[page]);
可以计算出simulator上的ptr是
(gdb) p nand_mgr.blk_para[30][115].page_addr[662]
$8 = 58346725456

在gdb中将这段内存地址打印出来
(gdb) x /100xw 58346725456
0xd95bc6050:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc6060:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc6070:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc6080:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc6090:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc60a0:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc60b0:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc60c0:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc60d0:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc60e0:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc60f0:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc6100:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc6110:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc6120:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc6130:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc6140:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc6150:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc6160:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc6170:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc6180:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc6190:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc61a0:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc61b0:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc61c0:    0x55504144      0x4857464c      0x55504144      0x4857464c
0xd95bc61d0:    0x55504144      0x4857464c      0x55504144      0x4857464c
那这个0x55504144 0x4857464c到底是什么?
#define CCP_PADDED_SIGN                     (0x4857'464c'5550'4144ULL) // "HWFLUPAD"
pad the b2n to page boundary(以每个B2N_NUM_OF_ONE_STRIPE)
B2N_NUM_OF_ONE_STRIPE = 4,表示一个stripe有4个b2n
我们再看看正常的内存中映射的nand中的内容是什么
(gdb) p nand_mgr.blk_para[30][114].page_addr[662]
$9 = 58162406608
(gdb) x /100xw 58162406608
0xd8abfe4d0:    0x06434d50      0x00000004      0x051f9154      0x00000000
0xd8abfe4e0:    0xaf652372      0x64518041      0x06434d50      0x00000004
0xd8abfe4f0:    0x051f9155      0x00000000      0xaf652372      0x64518041
0xd8abfe500:    0x06434d50      0x00000004      0x051f9156      0x00000000
0xd8abfe510:    0xaf652372      0x64518041      0x06434d50      0x00000004
0xd8abfe520:    0x051f9157      0x00000000      0xaf652372      0x64518041
0xd8abfe530:    0x06434d50      0x00000004      0x051f9158      0x00000000
0xd8abfe540:    0xaf652372      0x64518041      0x06434d50      0x00000004
0xd8abfe550:    0x051f9159      0x00000000      0xaf652372      0x64518041
0xd8abfe560:    0x06434d50      0x00000004      0x051f915a      0x00000000
0xd8abfe570:    0xaf652372      0x64518041      0x06434d50      0x00000004
0xd8abfe580:    0x051f915b      0x00000000      0xaf652372      0x64518041
0xd8abfe590:    0x06434d50      0x00000004      0x051f915c      0x00000000
0xd8abfe5a0:    0xaf652372      0x64518041      0x06434d50      0x00000004
0xd8abfe5b0:    0x051f915d      0x00000000      0xaf652372      0x64518041
0xd8abfe5c0:    0x06434d50      0x00000004      0x051f915e      0x00000000
0xd8abfe5d0:    0xaf652372      0x64518041      0x06434d50      0x00000004
0xd8abfe5e0:    0x051f915f      0x00000000      0xaf652372      0x64518041
0xd8abfe5f0:    0x06434d50      0x00000004      0x051f9160      0x00000000
0xd8abfe600:    0xaf652372      0x64518041      0x06434d50      0x00000004
0xd8abfe610:    0x051f9161      0x00000000      0xaf652372      0x64518041
0xd8abfe620:    0x06434d50      0x00000004      0x051f9162      0x00000000
0xd8abfe630:    0xaf652372      0x64518041      0x06434d50      0x00000004
0xd8abfe640:    0x051f9163      0x00000000      0xaf652372      0x64518041
0xd8abfe650:    0x00000000      0x00000000      0x00000000      0x00000000
那么0x06434d50这个到底是什么?
#define CCP_HEADER_SIGN         (0x434D50)   // "CMP" first 24 bits in ccp header
查看下ccp数据结构
struct ccp_header {
    // total 8 bytes
    u64 signature:       24;     // "CMP" (0x434D50)
    u64 comp_size_4B:    12;     // compressed CP size (4B unit), includes CCP, LP header & payload
    u64 compressible:     1;     // 1, compressed;  0, uncompressed
    u64 pad_size_4B:      2;     // Padding size with 4B
    u64 aes:              1;
    u64 ccp_crc:         24;     // includes CCP, LP header & payload
};
0x06434d50
434d50就是CCP_HEADER_SIGN,就是24bits

```

#### NAND的相关总结

```
recovery_tas:                    next pba 0x88828b00, block 546, page 40
recovery_tas:                    next pba 0x88829b00, block 546, page 41
recovery_tas:                    next pba 0x8882ab00, block 546, page 42
recovery_tas:                    next pba 0x8882bb00, block 546, page 43
recovery_tas:                    next pba 0x8882cb00, block 546, page 44
recovery_tas:                    next pba 0x8882db00, block 546, page 45
recovery_tas:                    next pba 0x8882eb00, block 546, page 46
recovery_tas:                    next pba 0x8882fb00, block 546, page 47
recovery_tas:                    next pba 0x88830b00, block 546, page 48
recovery_tas:                    next pba 0x88831b00, block 546, page 49
recovery_tas:                    next pba 0x88832b00, block 546, page 50
recovery_tas:                    next pba 0x88833b00, block 546, page 51
recovery_tas:                    next pba 0x88834b00, block 546, page 52
recovery_tas:                    next pba 0x88835b00, block 546, page 53
recovery_tas:                    next pba 0x88836b00, block 546, page 54
recovery_tas:                    next pba 0x88837b00, block 546, page 55
recovery_tas:                    next pba 0x88838b00, block 546, page 56
recovery_tas:                    next pba 0x88839b00, block 546, page 57
recovery_tas:                    next pba 0x8883ab00, block 546, page 58
recovery_tas:                    next pba 0x8883bb00, block 546, page 59
recovery_tas:                    next pba 0x8883cb00, block 546, page 60
....
recovery_tas:                    next pba 0x88872b00, block 546, page 114
每次next pba增加0x1000,就是4K大小

```

```
pba_t b47_layout::nand_addr_to_pba_t(nand_addr_t nand_addr, u8 tlc_open, u64 ccp_off, u64 comp_len)
{
    pba_t pba    = {.all = 0};
    pba.all      = 0;
    pba.ch       = nand_addr.ch;
    pba.ce_lun   = GET_CE_LUN(nand_addr.ce, nand_addr.lun);
    pba.block    = nand_addr.block;
    pba.page     = _nand_page_table[tlc_open][nand_addr.page].ftl_page_index;
    u8 page_type = _nand_page_table[tlc_open][nand_addr.page].page_type;
    BOMB(INVALID_B47_NAND_PAGE_TYPE, ((page_type > NO_PAGE_TYPE) && (page_type < TOT_PAGE_TYPE_NUM)));
    if (page_type > MLC_UPPER_PAGE) {
        pba.page_type = page_type - TLC_LOWER_PAGE;
    }
    else if (page_type > SLC_PAGE) {
        pba.page_type = page_type - MLC_LOWER_PAGE;
    }
    else {
        pba.page_type = page_type - SLC_PAGE;// 0
    }
    pba.plane    = nand_addr.plane;
    pba.offset   = nand_addr.la >> 1;
    pba.ep_idx   = nand_addr.la & 0x1;
    pba.ccp_off  = ccp_off;
    pba.comp_len = comp_len;
    return pba;
}
```



#### 查看smp state change的debug

```
(gdb) p g_kernel ._slave_scheduler [5]._smpmgr ._switch_stage
$7 = SMP_SWITCH_STAGE_WAIT_ADMIN_START_RESUME
```



#### 实战

```
^[[0K[2-0:0:0D|0:1:21:512] simu_flash_c:                simu_nand_read: #### NAND READ ERR: Meet SHIM ERR at 0x8aba790^M
^[[0K[2-0:0:0D|0:1:21:512] simu_flash_c:                simu_nand_read: #### NAND READ ERR: Meet SHIM ERR at 0x8aba7a0^M
^[[0K[5-0:0:0D|0:1:21:512] recovery_tas:                  RD_ERR: gc_rd scheme 4 fct_idx 1284 overall_status 0x0 crc 0 erase_page 0 rd_cmd_err 0 cwerr 0x0 ccperr 0x0 lperr 0x0 lbamis 0x0 dmaerr 0x0, current vt 0 pbah 2d00 pbal 111444d^M
^[[0K[5-0:0:0D|0:1:21:512] recovery_tas:                   debug_gc_rd_cpl_status ecc status 0 be_timeout 0 from_buf 0 xfr_only 0 zero_page 0 ldpc_err_cnt 0x0 snap_read 0 erase_pg 0 crc 0 dw2 0^M
^[[0K[5-0:0:0D|0:1:21:512] recovery_tas:                   gc read error, dma_err 0, cw_err 0, crc_err 0, lp_crc 0, lba_mis 0, ext_cpl 80000, nlb 5^M
^[[0K[5-0:0:0D|0:1:21:512] recovery_tas:                   ext status, lp_ind 0, rd_cmd_err 0 nodata 0 ccp_hd_err_ind 0 last 0 raid_cmd 0 shim_err 1 xfer_size_8b 0 ccp_crc_err 0^M
^[[0K[5-0:0:0D|0:1:21:512] recovery_tas:                  RD_ERR: gc_rd scheme 4 fct_idx 1285 overall_status 0x0 crc 0 erase_page 0 rd_cmd_err 0 cwerr 0x0 ccperr 0x0 lperr 0x0 lbamis 0x0 dmaerr 0x0, current vt 0 pbah 2d00 pbal 111444e^M
^[[0K[5-0:0:0D|0:1:21:512] recovery_tas:                   debug_gc_rd_cpl_status ecc status 0 be_timeout 0 from_buf 0 xfr_only 0 zero_page 0 ldpc_err_cnt 0x0 snap_read 0 erase_pg 0 crc 0 dw2 0^M
^[[0K[5-0:0:0D|0:1:21:512] recovery_tas:                   gc read error, dma_err 0, cw_err 0, crc_err 0, lp_crc 0, lba_mis 0, ext_cpl 80000, nlb 5^M
^[[0K[5-0:0:0D|0:1:21:512] recovery_tas:                   ext status, lp_ind 0, rd_cmd_err 0 nodata 0 ccp_hd_err_ind 0 last 0 raid_cmd 0 shim_err 1 xfer_size_8b 0 ccp_crc_err 0^M

首先看pbah 2d00 pbal 111444d
拼起来nand_addr_t nand_pba = 0x2d000111444d
如果在串口里面,我们很快就能通过n2p命令获取pba
NOT-SET@:/$ n2p 0x2d000111444d 0 0
[0-0:0:0D|1:47:0:650] ppatch_task0:            pba : 0x8aba790
查看一下pba
NOT-SET@:/$ parsepba 0x8aba790
pba.all 0x8aba790:
ch:       2
ce_lun:   7
block:    69
page:     372
plane:    0
offset:   0x0
type:     1
ep_idx:   0
ccp_off:  0x0
comp_len: 0x0

nand addr fields:
lun:      0x1
block:    0x45
plane:    0x0
page:     0x44d
row_addr: 0x111444d
la:       0x0
type:     0x0
ln:       0x1
ce:       0x3
ch:       0x2
high32:   0x2d00
这样就能简单的知道pba的信息
ch:       2
ce_lun:   7
block:    69
page:     372
再根据
#define GET_CE_LUN(ce, lun)             (((lun) * CFG_CE_NUM) + (ce))
#define GET_DIE_BY_CH_CE_LUN(ch, ce, lun)  (((((lun) * CFG_CE_NUM) + (ce)) << CFG_PBA_CHAN_BITS) | (ch))
可以算出来
die = ((1)*4 + 3)<<2 + 2 = 30
再根据simu_nand_read中计算出nand addr映射到simulator中的内存地址
struct header_content *ptr = NULL;
ptr = (struct header_content *)(nand_mgr.blk_para[die][blk].page_addr[page]);
在GDB中获取:
(gdb) p nand_mgr.blk_para[30][69].page_addr[372]
$1 = 54411806992
这样就拿到了simulator中这个nand addr对应的内存地址
打印出来看看:
(gdb) x /100x 54411806992
0xcab324910:    0x06434d50      0x00000004      0x05c09a4b      0x00000000
0xcab324920:    0x804e1b00      0xd8d92dfd      0x06434d50      0x00000004
0xcab324930:    0x06b9c486      0x00000000      0x80000000      0x00000000
0xcab324940:    0x06434d50      0x00000004      0x06b9c487      0x00000000
0xcab324950:    0x80000000      0x00000000      0x06434d50      0x00000004
0xcab324960:    0x06b9c488      0x00000000      0x80000000      0x00000000
0xcab324970:    0x06434d50      0x00000004      0x06b9c489      0x00000000
0xcab324980:    0x80000000      0x00000000      0x06434d50      0x00000004
0xcab324990:    0x06b9c48a      0x00000000      0x80000000      0x00000000
0xcab3249a0:    0x06434d50      0x00000004      0x06b9c48b      0x00000000
0xcab3249b0:    0x80000000      0x00000000      0x06434d50      0x00000004
0xcab3249c0:    0x06b9c48c      0x00000000      0x80000000      0x00000000
0xcab3249d0:    0x06434d50      0x00000004      0x06b9c48d      0x00000000
0xcab3249e0:    0x80000000      0x00000000      0x06434d50      0x00000004
0xcab3249f0:    0x06b9c48e      0x00000000      0x80000000      0x00000000
0xcab324a00:    0x06434d50      0x00000004      0x06b9c48f      0x00000000
0xcab324a10:    0x80000000      0x00000000      0x06434d50      0x00000004
0xcab324a20:    0x06b9c490      0x00000000      0x80000000      0x00000000
0xcab324a30:    0x06434d50      0x00000004      0x06b9c491      0x00000000
0xcab324a40:    0x80000000      0x00000000      0x06434d50      0x00000004
0xcab324a50:    0x06b9c492      0x00000000      0x80000000      0x00000000
0xcab324a60:    0x06434d50      0x00000004      0x06b9c493      0x00000000
0xcab324a70:    0x80000000      0x00000000      0x06434d50      0x00000004
0xcab324a80:    0x06b9c494      0x00000000      0x80000000      0x00000000
0xcab324a90:    0x00000000      0x00000000      0x00000000      0x00000000
if (CCP_HEADER_SIGN != ((header_content *)data_addr)->ccp_hdr.signature)
所以这里就报SHIM ERR

好，假如没有串口，只用GDB，我们怎么通过nand_pba来获取pba?
首先通过GDB拿到nand_pba的结构信息
(gdb) p (nand_addr_t)(u64)0x2d000111444d
$2 = {{{{page = 1101, plane = 0, block = 69, lun = 1, rsvd = 0}, row_addr = 17908813}, {{la = 0, rsvd2 = 0, page_type = 0, rsvd3 = 0,
        ln = 1, ce = 3, ch = 2}, high_32bit = 11520}}, all = 49478041158733}
所以我们可以知道:
nand addr fields:
lun:      1
block:    69
plane:    0
page:     1101
row_addr: 0x111444d
la:       0
type:     0
ln:       1
ce:       3
ch:       2
high32:   0x2d00
接着根据公式
    pba.ch       = nand_addr.ch;
    pba.ce_lun   = GET_CE_LUN(nand_addr.ce, nand_addr.lun);
    pba.block    = nand_addr.block;
    pba.page     = _nand_page_table[tlc_open][nand_addr.page].ftl_page_index;
    pba.plane    = nand_addr.plane;
    pba.offset   = nand_addr.la >> 1;
    pba.ep_idx   = nand_addr.la & 0x1;
其中#define GET_CE_LUN(ce, lun)             (((lun) * CFG_CE_NUM) + (ce))
那么pba.ce_lun = 1*4+3 = 7
pba.page     = _nand_page_table[tlc_open][nand_addr.page].ftl_page_index;
但这时候我们不知道这个pba是哪个stream的,也就无从判断是否是tlc还是slc
在log中搜索block:69,换算成16进制是0x45
故搜索> sblock:                45
得到log:
^[[0K[5-0:0:0D|0:0:10:576]   scan_tlc_data_candidate_sblock:^M
^[[0K  > sblock:                45^M
^[[0K  > open_seq_num:         26e^M
^[[0K  > stream:                 2^M
可知是stream 2的tlc的block
看看这里_nand_layout->init(CFG_SLC_PAGE_NUM, CFG_PAGE_NUM);
CFG_SLC_PAGE_NUM = 704, CFG_PAGE_NUM = 712
我们根据
else {// tlc
nand_page_index = (i < 352) ? (((i - 4) * 3) + 4) : (((i - 360) * 3) + 1064);
_ftl_page_table[1][i].nand_page_index = nand_page_index;
_ftl_page_table[1][i].page_type_num   = 3;

_nand_page_table[1][nand_page_index].ftl_page_index = i;
_nand_page_table[1][nand_page_index].page_type      = TLC_LOWER_PAGE;

_nand_page_table[1][nand_page_index + 1].ftl_page_index = i;
_nand_page_table[1][nand_page_index + 1].page_type      = TLC_UPPER_PAGE;

_nand_page_table[1][nand_page_index + 2].ftl_page_index = i;
_nand_page_table[1][nand_page_index + 2].page_type      = TLC_EXTRA_PAGE;
}
可以算出来i>352 && (((i - 360) * 3) + 1064) = 1101,所以i = 372
即:
_nand_page_table[1][1100 + 1].ftl_page_index = 372;
_nand_page_table[1][1100 + 1].page_type = TLC_UPPER_PAGE;

还有一种办法,查看表格
https://sfxsw.atlassian.net/wiki/spaces/MYR/pages/5013592/B47+B2N+Page+to+LUN+Page+Cheat+Sheet
nand page: 1101 对应b2n page 372
计算得到
pba.all :
ch:       2
ce_lun:   7
block:    69
page:     372
plane:    0
offset:   0
type:     1
ep_idx:   0

再根据
    u8 page_type = _nand_page_table[tlc_open][nand_addr.page].page_type;
    BOMB(INVALID_B47_NAND_PAGE_TYPE, ((page_type > NO_PAGE_TYPE) && (page_type < TOT_PAGE_TYPE_NUM)));
    if (page_type > MLC_UPPER_PAGE) {
        pba.page_type = page_type - TLC_LOWER_PAGE;
    }
    else if (page_type > SLC_PAGE) {
        pba.page_type = page_type - MLC_LOWER_PAGE;
    }
    else {
        pba.page_type = page_type - SLC_PAGE;// 0
    }
来计算page_type,这就很简单了
page_type = TLC_UPPER_PAGE;
故pba.page_type = 5 - 4 = 1;
或者直接在GDB里面输入:
(gdb) p g_b47_layout.
_ftl_page_table                    get_nand_page_index                nand_family_type
_nand_page_table                   get_page_type_num                  pba_to_nand_addr_t
_vptr.basic_nand_layout            get_small_page_index_by_nand_addr  ~b47_layout
get_ftl_page_index                 get_total_small_page_num           ~basic_nand_layout
get_hw_page_type                   init
get_nand_family_type               nand_addr_to_pba_t
(gdb) p g_b47_layout._nand_page_table[1][1101].
ftl_page_index  page_type
(gdb) p g_b47_layout._nand_page_table[1][1101].page_type
$3 = 5 '\005'
(gdb) p g_b47_layout._nand_page_table[1][1101].ftl_page_index
$4 = 372

再根据
#define GET_CE_LUN(ce, lun)             (((lun) * CFG_CE_NUM) + (ce))
#define GET_DIE_BY_CH_CE_LUN(ch, ce, lun)  (((((lun) * CFG_CE_NUM) + (ce)) << CFG_PBA_CHAN_BITS) | (ch))
可以算出来
die = ((1)*4 + 3)<<2 + 2 = 30
再根据simu_nand_read中计算出nand addr映射到simulator中的内存地址
struct header_content *ptr = NULL;
ptr = (struct header_content *)(nand_mgr.blk_para[die][blk].page_addr[page]);
在GDB中获取:
(gdb) p nand_mgr.blk_para[30][69].page_addr[372]
$1 = 54411806992
这样就拿到了simulator中这个nand addr对应的内存地址

再反过来看一下
(gdb) p g_b47_layout ._ftl_page_table[1][372].nand_page_index
$7 = 1100
(gdb) p g_b47_layout ._ftl_page_table[1][372].page_type_num
$8 = 3 '\003'

```

| B2N Page | Page Type Num | Total LUN Page Count |
| -------- | ------------- | -------------------- |
| None     | None          | 0                    |
| 0        | 1             | 1                    |
| 1        | 1             | 2                    |
| 2        | 1             | 3                    |
| 3        | 1             | 4                    |
| 4        | 3             | 7                    |
| 5        | 3             | 10                   |
| 6        | 3             | 13                   |
| 7        | 3             | 16                   |
| 8        | 3             | 19                   |
| 9        | 3             | 22                   |
| 10       | 3             | 25                   |
| 11       | 3             | 28                   |
| 12       | 3             | 31                   |
| 13       | 3             | 34                   |
| 14       | 3             | 37                   |
| 15       | 3             | 40                   |
| 16       | 3             | 43                   |
| 17       | 3             | 46                   |
| 18       | 3             | 49                   |
| 19       | 3             | 52                   |
| 20       | 3             | 55                   |
| 21       | 3             | 58                   |
| 22       | 3             | 61                   |
| 23       | 3             | 64                   |
| 24       | 3             | 67                   |
| 25       | 3             | 70                   |
| 26       | 3             | 73                   |
| 27       | 3             | 76                   |
| 28       | 3             | 79                   |
| 29       | 3             | 82                   |
| 30       | 3             | 85                   |
| 31       | 3             | 88                   |
| 32       | 3             | 91                   |
| 33       | 3             | 94                   |
| 34       | 3             | 97                   |
| 35       | 3             | 100                  |
| 36       | 3             | 103                  |
| 37       | 3             | 106                  |
| 38       | 3             | 109                  |
| 39       | 3             | 112                  |
| 40       | 3             | 115                  |
| 41       | 3             | 118                  |
| 42       | 3             | 121                  |
| 43       | 3             | 124                  |
| 44       | 3             | 127                  |
| 45       | 3             | 130                  |
| 46       | 3             | 133                  |
| 47       | 3             | 136                  |
| 48       | 3             | 139                  |
| 49       | 3             | 142                  |
| 50       | 3             | 145                  |
| 51       | 3             | 148                  |
| 52       | 3             | 151                  |
| 53       | 3             | 154                  |
| 54       | 3             | 157                  |
| 55       | 3             | 160                  |
| 56       | 3             | 163                  |
| 57       | 3             | 166                  |
| 58       | 3             | 169                  |
| 59       | 3             | 172                  |
| 60       | 3             | 175                  |
| 61       | 3             | 178                  |
| 62       | 3             | 181                  |
| 63       | 3             | 184                  |
| 64       | 3             | 187                  |
| 65       | 3             | 190                  |
| 66       | 3             | 193                  |
| 67       | 3             | 196                  |
| 68       | 3             | 199                  |
| 69       | 3             | 202                  |
| 70       | 3             | 205                  |
| 71       | 3             | 208                  |
| 72       | 3             | 211                  |
| 73       | 3             | 214                  |
| 74       | 3             | 217                  |
| 75       | 3             | 220                  |
| 76       | 3             | 223                  |
| 77       | 3             | 226                  |
| 78       | 3             | 229                  |
| 79       | 3             | 232                  |
| 80       | 3             | 235                  |
| 81       | 3             | 238                  |
| 82       | 3             | 241                  |
| 83       | 3             | 244                  |
| 84       | 3             | 247                  |
| 85       | 3             | 250                  |
| 86       | 3             | 253                  |
| 87       | 3             | 256                  |
| 88       | 3             | 259                  |
| 89       | 3             | 262                  |
| 90       | 3             | 265                  |
| 91       | 3             | 268                  |
| 92       | 3             | 271                  |
| 93       | 3             | 274                  |
| 94       | 3             | 277                  |
| 95       | 3             | 280                  |
| 96       | 3             | 283                  |

#### nandcfg的理解

```
CFG_LUN_PAGE_SIZE
如果是SLC Page：每个LUN上有4个Planes，每个Plane有4个4K，那么每个LUN上有4*4*4k=16个4K=64K
如果是TLC Page：则每个LUN上有3*SLC Page=192K
u64   lba[CFG_LUN_PAGE_SIZE]
意味着，每个lba代表一个4K

u32 read_size   = (CFG_LUN_PAGE_SIZE - (CFG_FOURK_PER_PLANE * bad_pl_cnt)) % 32;
u64 lp_bitmap   = N_BIT_MASK(read_size);
这个如何理解？

pf_read_cmd_submit，这是submit，肯定能发送成功，但可能要等polling
这个每次从指定的Nand pba读4120B的数据到buffer
process_read_pending，这里是polling后的结果

pf_raw_write_b2n_submit，这是submit，肯定能发送成功，但可能要等polling
要将数据写到指定的stream的Nand pba的请求，算预发b2n
pf_raw_write_w2p_submit，，这是submit，肯定能发送成功，但可能要等polling
写每个w2p，write to pba，每次4120B，直到将b2n填满，再刷到指定的Nand pba

is_all_pending_write_cmd_cpl，这个是等待所有的polling回来

```

