```
​~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~         第二轮优化后 4T 满盘 recovery - CPU Clock: 1500 MHz               
-------------------------------+------------+-----------------+----------------       
Code Block /stage Description  | Start time |   CPU Cycles    | Elapsed Time/ms       
-------------------------------+------------+-----------------+----------------       
before PF recovery             | 0:0:0:461  |     691,500,000 |     461   ms           
before raise PF irq            | 0:0:0:577  |     174,000,000 |     116   ms  
-------------------------------+------------+-----------------+----------------       
PF Restore Start               | 0:0:0:601  |      36,000,000 |      24   ms           
read root data                 |            |       4,500,000 |       3   ms           
read summary data              |            |               0 |       0   ms           
read meta data                 |            |      13,500,000 |       9   ms           
copy back data                 |            |      22,500,000 |      15   ms           
parity rebuild                 |            |       3,000,000 |       2   ms           
clean up nor                   |            |      13,500,000 |       9   ms           
PF Restore Complete            | 0:0:0:656  |      25,500,000 |      17   ms 
-------------------------------+------------+-----------------+----------------       
FTL Recovery Start             | 0:0:0:656  |               0 |       0   ms           
pre init for pf recovery       |            |     109,500,000 |      73   ms      
summary load parse complete    |            |     655,500,000 |     437   ms           
patch parse complete           |            |     672,000,000 |     448   ms           
mpage load complete            |            |   3,726,000,000 |    2484   ms    
patch parse complete           |            |   1,933,500,000 |    1289   ms     
partial block handle complete  |            |       3,000,000 |       2   ms 
FTL Recovery Complete          | 0:0:5:525  |               0 |       0   ms 
-------------------------------+------------+-----------------+---------------- 
Total Time                     |            |    8,287,500,000|    5525   ms           
​~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  
```

```
​~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~         第二轮优化后 4T 空盘 recovery - CPU Clock: 1500 MHz               
-------------------------------+------------+-----------------+----------------       
Code Block /stage Description  | Start time |   CPU Cycles    | Elapsed Time/ms       
-------------------------------+------------+-----------------+----------------       
before PF recovery             | 0:0:0:328  |     492,000,000 |     328   ms           
before raise PF irq            | 0:0:0:592  |     396,000,000 |     264   ms  
-------------------------------+------------+-----------------+----------------       
PF Restore Start               | 0:0:0:617  |      37,500,000 |      25   ms           
read root data                 |            |       3,000,000 |       2   ms           
read summary data              |            |               0 |       0   ms           
read meta data                 |            |      12,000,000 |       8   ms           
copy back data                 |            |               0 |       0   ms           
parity rebuild                 |            |               0 |       0   ms           
clean up nor                   |            |      12,000,000 |       8   ms           
PF Restore Complete            | 0:0:0:636  |           1,500 |       1   ms 
-------------------------------+------------+-----------------+----------------       
FTL Recovery Start             | 0:0:0:636  |               0 |       0   ms           
pre init for pf recovery       |            |     109,500,000 |      73   ms      
summary load parse complete    |            |               0 |       0   ms           
patch parse complete           |            |               0 |       0   ms           
mpage load complete            |            |   4,591,500,000 |    3061   ms    
patch parse complete           |            |               0 |       0   ms     
partial block handle complete  |            |       3,000,000 |       2   ms 
FTL Recovery Complete          | 0:0:3:927  |               0 |       0   ms 
-------------------------------+------------+-----------------+---------------- 
Total Time                     |            |    5,890,500,000|    3927   ms           
​~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  
```

```
​~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      第一轮优化后 4T 满盘 recovery - CPU Clock: 1500 MHz               
-------------------------------+------------+-----------------+----------------       
Code Block /stage Description  | Start time |   CPU Cycles    | Elapsed Time/ms       
-------------------------------+------------+-----------------+----------------       
before PF recovery             | 0:0:0:695  |   1,042,500,000 |     695   ms           
before raise PF irq            | 0:0:0:930  |     352,500,000 |     235   ms  
-------------------------------+------------+-----------------+----------------       
PF Restore Start               | 0:0:0:951  |      31,500,000 |      21   ms           
read root data                 |            |      33,000,000 |      22   ms           
read summary data              |            |       1,500,000 |       1   ms           
read meta data                 |            |      21,000,000 |      14   ms     
copy back data                 |            |      37,500,000 |      25   ms           
parity rebuild                 |            |       3,000,000 |       2   ms           
clean up nor                   |            |      51,500,000 |      34   ms           
PF Restore Complete            | 0:0:1:061  |      18,000,000 |      12   ms 
-------------------------------+------------+-----------------+----------------       
FTL Recovery Start             | 0:0:1:061  |                 |           ms           
pre init for pf recovery       |            |      46,500,000 |      31   ms      
summary load parse complete    |            |     829,500,000 |     553   ms           
patch parse complete           |            |     688,500,000 |     459   ms           
mpage load complete            |            |    9090,000,000 |    6060   ms    
patch parse complete           |            |    1945,500,000 |    1297   ms     
partial block handle complete  |            |    1092,000,000 |     728   ms 
FTL Recovery Complete          | 0:0:10:189 |       1,350,000 |       0   ms 
-------------------------------+------------+-----------------+---------------- 
Total Time                     |            |  15,286,500,000 |   10191   ms           
​~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```

```
​~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~       
第一轮优化后 4T 空盘 recovery - CPU Clock: 1500 MHz               
-------------------------------+------------+-----------------+----------------       
Code Block /stage Description  | Start time |   CPU Cycles    | Elapsed Time/ms       
-------------------------------+------------+-----------------+----------------       
before PF recovery             | 0:0:0:694  |   1,041,000,000 |     694   ms           
before raise PF irq            | 0:0:0:923  |     343,500,000 |     229   ms  
-------------------------------+------------+-----------------+----------------       
PF Restore Start               | 0:0:0:944  |      31,500,000 |      21   ms           
read root data                 | 0:0:0:946  |        3000,000 |       2   ms           
read summary data              | 0:0:0:946  |                 |       0   ms           
read meta data                 | 0:0:0:958  |      18,000,000 |      12   ms           
clean up nor                   | 0:0:0:966  |      12,000,000 |       8   ms           
PF Restore Complete            | 0:0:0:966  |                 |       0   ms 
-------------------------------+------------+-----------------+----------------       
FTL Recovery Start             | 0:0:0:966  |                 |       0   ms           
pre init for pf recovery       | 0:0:0:998  |      48,000,000 |      32   ms      
summary load parse complete    | 0:0:0:998  |                 |       0   ms           
patch parse complete           | 0:0:0:998  |                 |       0   ms           
mpage load complete            | 0:0:5:660  |   6,993,000,000 |    4662   ms    
patch parse complete           | 0:0:5:660  |                 |       0   ms     
partial block handle complete  | 0:0:5:660  |                 |       0   ms 
FTL Recovery Complete          | 0:0:5:660  |                 |       0   ms 
-------------------------------+------------+-----------------+---------------- 
Total Time                     |            |   8,490,000,000 |    5660   ms           
​~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```

```
// each task state most can run 2ms
#define MAX_TASK_STATE_COST_IN_US 2000
```

UD2214C0520H@CSDU5SPC76M1:/$ pftimecost
[4-2-2023:7:6|2:34:13:858]    ====== Recovery stage timecost: ======
[4-2-2023:7:6|2:34:13:858]    RECOVERY total timecost: 17050 ms
[4-2-2023:7:6|2:34:13:858]    RECOVERY_PF_RESTORE timecost: 151 ms
[4-2-2023:7:6|2:34:13:858]    RECOVERY_FTL_RECOVERY timecost: 16150 ms
[4-2-2023:7:6|2:34:13:858]    ====== PF Restore stage timecost: ======
[4-2-2023:7:6|2:34:13:858]    LOW_LEVEL_STATE_READ_ROOT_DATA, timecost: 9 ms.
[4-2-2023:7:6|2:34:13:858]    LOW_LEVEL_STATE_READ_SUMMARY_DATA, timecost: 0 ms.
[4-2-2023:7:6|2:34:13:858]    LOW_LEVEL_STATE_READ_META_DATA, timecost: 15 ms.
[4-2-2023:7:6|2:34:13:858]    LOW_LEVEL_STATE_DATA_COPY_BACK, timecost: 19 ms.
[4-2-2023:7:6|2:34:13:858]    LOW_LEVEL_STATE_PARITY_REBUILD, timecost: 3 ms.
[4-2-2023:7:6|2:34:13:858]    LOW_LEVEL_STATE_RESTORE_CLEAN_UP, timecost: 8 ms.
[4-2-2023:7:6|2:34:13:858]    ====== FTL recovery stage timecost: ======
[4-2-2023:7:6|2:34:13:858]    ftl_state: 1, timecost: 532 ms.
[4-2-2023:7:6|2:34:13:858]    ftl_state: 2, timecost: 14402 ms.
[4-2-2023:7:6|2:34:13:858]    ftl_state: 3, timecost: 1744 ms.
[4-2-2023:7:6|2:34:13:858]    ftl_state: 5, timecost: 3 ms.