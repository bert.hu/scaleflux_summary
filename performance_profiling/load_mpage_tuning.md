```
bsp_platform::gdma_copy(u32 ch, void *dest, const void *src, u64 n)
void arch_gdma_memset(void *dest, u64 c, u64 n)

_gdma_jobid = sys_gdma_memset((void *)&_l2_map[_l2_alloc_for_recovery], INVALID_PBA,
                                      (L1_MAP_ENTRY_NUM - _l2_alloc_for_recovery) * L2_MAP_ENTRY_SIZE);
                                      
                                      881a94cb5e5d1834274fe64bb6508ced79d94670
```

#### 上电启动信息

```
2K_MPAGE 0, RAW_CAPACITY 0x40000000000, DDR_SIZE 17179869184, provision_capacity 3840, format_capacity 3840, cache ratio 100, MAX_LBA 0x1bf1f72af, MAX_LPA 0x37e3ee55

L1_MAP_ENTRY_NUM 0x1bf1f8, L2_MAP_ENTRY_NUM 0x1bf1f8, max_cache_l2_num 0x37e3ea, MAX_CACHE_L2_MAP_ENTRY_NUM 0x3c1b0a, DEFAULT_L1_MAP_ENTRY_NUM 0x1bf1f8, MAX_L1_MAP_ENTRY_NUM 0x37e3ea, MAX_DIRTY_LIST_LEN 0x168a23

DDR_L2P_LOCK_SIZE 0xdf8fa8, DDR_L1_MAP_SIZE 0x1bf2000, DDR_L2_MAP_SIZE 0x3c1b0a000, DDR_L2_NODE_SIZE 0x3c1b0a0, ALLOC_L2_MAP_SIZE 0x3c1b0a000

ptr: lock 0x43796f000, l1_map 0x438ce8000, l2_map 0x43e4f6000, l2_node 0x43a8da000
```



```
rcvry_pf_ftl_ctrl::load_mpage()
#define MAX_PAGEIN_PER_LOOP       (40)
#define MPAGE_POOL_NUM (5)

5e3c89c5b5c9445dafd77b1958a052b146f4ec42
```

```
#define TEMP_BUF_SIZE 0x2'0000
u8 g_gdma_temp_buf[TEMP_BUF_SIZE] __aligned(16) __section(".sram_section");

void arch_gdma_memset(void *dest, u64 c, u64 n)
{
    unsigned char *d = (unsigned char *)dest;

    if (n <= 0x20000) {
        memset64(dest, c, n);
    }
    else {
        // first, prepare temp buf.
        memset64(g_gdma_temp_buf, c, TEMP_BUF_SIZE);

        u64 len = n;

        // maximum len of gdma_copy is 0x4000000, split to 0x4000000.
        while (len > TEMP_BUF_SIZE) {
            bsp_platform::gdma_copy(0, d, g_gdma_temp_buf, TEMP_BUF_SIZE);
			while (1) {
				// 0 means gdma is not busy
				if (bsp_platform::gdma_get_ch_status(0) == 0) {
					break;
				}
			}
            len -= TEMP_BUF_SIZE;
            d += TEMP_BUF_SIZE;
        }
        bsp_platform::gdma_copy(0, d, g_gdma_temp_buf, len);
        while (1) {
            // 0 means gdma is not busy
            if (bsp_platform::gdma_get_ch_status(0) == 0) {
                break;
            }
        }
    }
}

    inline void gdma_l2p_memset_invalid(u32 l2_id)
    {
        bsp_platform::gdma_copy(0, &_l2_map[l2_id], _gdma_buf, L2_MAP_ENTRY_SIZE);
    }
```

#### 实现

```
enum L2P_DEBUG_TYPE
{
    L2P_CLEAR_DEBUG,
    L2P_INFO,
    L2P_DEBUG,
    L2P_CKP_INFO,
    L2P_GET_L1_INFO,
    L2P_GET_L2_INFO,
    L2P_DEBUG_ALL,
    L2P_THROTTLE_DEBUG,
    L2P_FIND_PATCH,
};

优化结果：
 >>> WARNING: Task: recovery_task, State: 6, Time cost: 2203530 us, Exceed allowed exec time!
[2022-04-16 00:59:27.087] [5-0:0:0D|0:0:0:869]   mpage_load_start:
[2022-04-16 00:59:27.091]
[2022-04-16 00:59:27.092] [5-0:0:0D|0:0:0:869] recovery_tas:                 {MPAGE_LOAD_STATE_INIT}
[2022-04-16 00:59:27.099] start_l2_id              7fffff
[2022-04-16 00:59:27.102] end_l2_id                7fffff
[2022-04-16 00:59:27.106] accumulate_l2_id_count   0
[2022-04-16 00:59:27.109] start_l2_id              0
[2022-04-16 00:59:27.112] end_l2_id                1bf1f7
[2022-04-16 00:59:27.115] accumulate_l2_id_count   1831416
[2022-04-16 00:59:27.118]
[2022-04-16 00:59:27.119] [5-0:0:0D|0:0:3:73]   l2p_recovery:
[2022-04-16 00:59:27.123]   > _l2_alloc_for_recovery:   1bf1f8
[2022-04-16 00:59:27.127]   > _full_cache_flag:    true
[2022-04-16 00:59:27.130]   > _format_capacity:         f00
[2022-04-16 00:59:27.133]
[2022-04-16 00:59:27.134] [5-0:0:0D|0:0:3:73] recovery_tas:               >>> WARNING: Task: recovery_task, State: 6, Time cost: 22!
[2022-04-16 00:59:27.147] start_l2_id              1bf1f7
[2022-04-16 00:59:27.150] end_l2_id                1bf1f7
[2022-04-16 00:59:27.154] accumulate_l2_id_count   0
[2022-04-16 00:59:27.157]
[2022-04-16 00:59:27.157] [5-0:0:0D|0:0:3:99]   mpage_load_complete:
[2022-04-16 00:59:27.162]   > mpage_load_success:  true

对半开
[2022-04-16 01:13:17.296] [5-0:0:0D|0:0:0:866]   mpage_load_start:
[2022-04-16 01:13:17.300]
[2022-04-16 01:13:17.301] [5-0:0:0D|0:0:0:866] recovery_tas:                 {MPAGE_LOAD_STATE_INIT}
[2022-04-16 01:13:17.308] start_l2_id              7fffff
[2022-04-16 01:13:17.312] end_l2_id                7fffff
[2022-04-16 01:13:17.315] accumulate_l2_id_count   0
[2022-04-16 01:13:17.318] start_l2_id              0
[2022-04-16 01:13:17.321] end_l2_id                df8fb
[2022-04-16 01:13:17.324] accumulate_l2_id_count   915708
[2022-04-16 01:13:17.327] [5-0:0:0D|0:0:1:4] recovery_tas:                >>> WARNING: Task: recovery_task, State: 6, Time cost: 137747 us, Exceed allowed exec time!
[2022-04-16 01:13:17.340] start_l2_id              0
[2022-04-16 01:13:17.343] end_l2_id                1bf1f7
[2022-04-16 01:13:17.347] accumulate_l2_id_count   1831416
[2022-04-16 01:13:17.350]
[2022-04-16 01:13:17.351] [5-0:0:0D|0:0:3:70]   l2p_recovery:
[2022-04-16 01:13:17.354]   > _l2_alloc_for_recovery:   1bf1f8
[2022-04-16 01:13:17.358]   > _full_cache_flag:    true
[2022-04-16 01:13:17.361]   > _format_capacity:         f00
[2022-04-16 01:13:17.365]
[2022-04-16 01:13:17.365] [5-0:0:0D|0:0:3:70] recovery_tas:               >>> WARNING: Task: recovery_task, State: 6, Time cost: 2065715 us, Exceed allowed exec time!
[2022-04-16 01:13:17.379] start_l2_id              1bf1f7
[2022-04-16 01:13:17.382] end_l2_id                1bf1f7
[2022-04-16 01:13:17.385] accumulate_l2_id_count   0
[2022-04-16 01:13:17.388]
[2022-04-16 01:13:17.389] [5-0:0:0D|0:0:3:96]   mpage_load_complete:
[2022-04-16 01:13:17.393]   > mpage_load_success:  true



```

#### 问题

```
1840965 [2022-04-18 11:11:54.158] [1-0:0:0D|8:6:9:661]   gc_vpc_err:
1840966 [2022-04-18 11:11:54.158]   > _unmove_pba:         2030006aec36cc
1840967 [2022-04-18 11:11:54.164]   > _unmove_lba:            561f4
1840968 [2022-04-18 11:11:54.168]
1840969 [2022-04-18 11:11:54.174]
1840970 [2022-04-18 11:11:54.174] [1-0:0:0D|8:6:9:691]   gc_vpc_err:
1840971 [2022-04-18 11:11:54.174]   > _unmove_pba:         2030006aec3320
1840972 [2022-04-18 11:11:54.179]   > _unmove_lba:            f9d54
1840973 [2022-04-18 11:11:54.184]
1840974 [2022-04-18 11:11:54.190]
1840975 [2022-04-18 11:11:54.190] [1-0:0:0D|8:6:9:699]   gc_vpc_err:
1840976 [2022-04-18 11:11:54.190]   > _unmove_pba:         2030006aec34c4
1840977 [2022-04-18 11:11:54.195]   > _unmove_lba:           123e91

0000000400085280 <_ZN8io_write14reset_last_pbaEv>:
   400085280:   90000000        adrp    x0, 400085000 <_ZN8io_write10calc_splitEP13st_pending_ioj+0xe0>
   400085284:   f9414c00        ldr     x0, [x0, #664]
   400085288:   92800001        mov     x1, #0xffffffffffffffff         // #-1
   40008528c:   a9000401        stp     x1, x1, [x0]
   400085290:   a9010401        stp     x1, x1, [x0, #16]
   400085294:   d65f03c0        ret
   400085298:   00216bc8        .word   0x00216bc8
   40008529c:   00000004        .word   0x00000004
```

```
结果
[2022-04-18 16:58:29.539] [5-0:0:0D|0:0:0:673]   mpage_load_start:                                                        [361/1893]
[2022-04-18 16:58:29.544]
[2022-04-18 16:58:29.544] start_l2_id              0
[2022-04-18 16:58:29.547] end_l2_id                1bf1f7
[2022-04-18 16:58:29.551] accumulate_l2_id_count   1831416
[2022-04-18 16:58:29.554]
[2022-04-18 16:58:29.555] [5-0:0:0D|0:0:3:340]   l2p_recovery:
[2022-04-18 16:58:29.559]   > _l2_alloc_for_recovery:   1bf1f8
[2022-04-18 16:58:29.562]   > _full_cache_flag:    true
[2022-04-18 16:58:29.565]   > _format_capacity:         f00
[2022-04-18 16:58:29.569]
[2022-04-18 16:58:29.570] [5-0:0:0D|0:0:3:340] recovery_tas:              >>> WARNING: Task: recovery_task, State: 6, Time cost: 2692 ms.
[2022-04-18 16:58:29.583]
[2022-04-18 16:58:29.583] [5-0:0:0D|0:0:3:366]   mpage_load_complete:
[2022-04-18 16:58:29.588]   > mpage_load_success:  true
[2022-04-18 16:58:29.591]
[2022-04-18 16:58:29.591] [5-0:0:0D|0:0:3:366] recovery_tas:                 [FTL_Recovery]load mpage time cost 2692 ms.


[2022-04-18 17:20:44.752] [5-0:0:0D|0:0:0:719]   mpage_load_start:                                                        [361/1970]
[2022-04-18 17:20:44.756]
[2022-04-18 17:20:44.757] start_l2_id              0
[2022-04-18 17:20:44.760] end_l2_id                1bf1f7
[2022-04-18 17:20:44.763] accumulate_l2_id_count   1831416
[2022-04-18 17:20:44.766]
[2022-04-18 17:20:44.767] [5-0:0:0D|0:0:1:92]   l2p_recovery:
[2022-04-18 17:20:44.771]   > _l2_alloc_for_recovery:   1bf1f8
[2022-04-18 17:20:44.775]   > _full_cache_flag:    true
[2022-04-18 17:20:44.778]   > _format_capacity:         f00
[2022-04-18 17:20:44.781]
[2022-04-18 17:20:44.782] [5-0:0:0D|0:0:1:92] recovery_tas:               >>> WARNING: Task: recovery_task, State: 6, Time cost: 37$
[2022-04-18 17:20:44.795]
[2022-04-18 17:20:44.795] [5-0:0:0D|0:0:1:118]   mpage_load_complete:
[2022-04-18 17:20:44.800]   > mpage_load_success:  true
[2022-04-18 17:20:44.803]
[2022-04-18 17:20:44.804] [5-0:0:0D|0:0:1:118] recovery_tas:                 [FTL_Recovery]load mpage time cost 399 ms.
[2022-04-18 17:20:44.813]

bl3_secondary_main
dispatch_cores
fw_slave_main
smpmgr12smpmgr_start
task_scheduler12run_all_task
task_scheduler12run_one_task
io_task3run
io_read12fast_read_4k
io_task13fetch_new_cmd
l2p_mgr14l2p_map_lookup

```



```
#elif NAND_TYPE == MICRON_NAND_B47R_4T
    // Here is 4T 4plane b47r config
    #define MAX_ACTIVE_B2N     128
    #if LIMIT_COMPRESS_RATE
        #define B2N_MAX_CCP    (CFG_ONE_PROG_NUM * 16)
    #else
    #define B2N_MAX_CCP    (CFG_ONE_PROG_NUM * CFG_CCP_UNIT_SIZE / CFG_CCP_MIN_SIZE)
    #endif
    #define MAPLOG_B2N_BUF     (B2N_MAX_CCP * MAPLOG_P2L_ENTRY_SIZE)
    /**
     *  MAPLOG_B2N_BUF will occupy 3,090 * 16 = 49锛�440
     *  MAPLOG_B2N_BUF_NUM * MAPLOG_B2N_BUF = MAPLOG_B2N_BUF_NUM * 3,090 * 16 = 3,090k
     */
    #define MAPLOG_B2N_BUF_NUM      (MAX_ACTIVE_B2N)
    #define MAPLOG_TOT_B2N_BUF_SIZE ((u64)((MAPLOG_B2N_BUF_NUM + 1) * MAPLOG_B2N_BUF))
    /**
     *  MAPLOG BUF lay out : (p2l entry * 256)
     *  TODO
     *    maybe production fw use 32K as maplog size
     */
    #define MAPLOG_BUF (DATA_LEN_4K)
    /**
     *  For 64 die, max compression
     *  maplog occupy 704 * 62 * B2N_MAX_CCP * 16 = 2,157,957,120 = 2,107,380k
     *  maplog pba and maplog range occupy 2,107,380k
     *  2,107,380k * 16 / 4K = 8,429,520 = 8,232k, 4k aligned will be 8,232k
     *  footer should include 128 maplog b2n buf + 128 maplog buf
     *  MAPLOG_B2N_BUF_NUM * MAPLOG_B2N_BUF + MAPLOG_HOT_BUF_NUM * MAPLOG_BUF 
     *  = MAPLOG_B2N_BUF_NUM * MAPLOG_B2N_BUF + 128 * 4k = 3,602k, 4k aligned will be 3,604k
     *  So the largest footer 
     *  = 8,232k + 3,604k = 11,836k
     *  1 slc super page size = 62 * 16 * 4096 = 4,063,232 = 3,968k
     *  So need 3 slc page for footer
     *
     *  and every block almost 20% bad plane,
     *  maplog occupy 704 * 62 * B2N_MAX_CCP * 16 * 0.8 = 1,726,365,696
     *  maplog pba and maplog range occupy 
     *  1,685,904k * 16 / 4K = 6,586k, 4K aligned will be 6,588k
     *  So the largest footer 
     *  = 6,588k + 3,604k = 10,192k 
     *  in 64 die, 1 super page have 256 plane, minimum good plane is 204, so user good plane is 196
     *  1 slc super page size (80% good plane) = 196 * 4 * 4096 = 3,136k
     *  1 tlc super page size (80% good plane) = 196 * 12 * 4096 = 9,408k
     *  So need 4 slc page for footer
     *
     *  NOTE:
     *      for hot stream maplog:
     *          because FCT_POOL_W2P_META_DATA only have 128 entry
     *          so there is 128 maplog_buf_num for hot stream, just 512K
     *
     *  For folding task, max compression
     *  1 parity group maplog size : 62 * 3090 * 16 = 2,994K
     *  so 749 buffer is enough for 1 folding page
     *  we need 3 page to handle issue
     */
    #define FOOTER_PAGE_NUM      (4)
    #define MAPLOG_COLD_BUF_NUM  (2247ULL)
    #define MAPLOG_COLD_USER_NUM (3)
    #define MAPLOG_HOT_BUF_NUM   (128ULL)
    #define MAPLOG_HOT_USER_NUM  (1)
    #define MAPLOG_USER_NUM      (4)
    #define MAPLOG_BUF_TOT_NUM   ((MAPLOG_COLD_BUF_NUM * MAPLOG_COLD_USER_NUM) + MAPLOG_HOT_BUF_NUM)
    #define MAPLOG_TOT_BUF_SIZE  ((u64)(MAPLOG_BUF_TOT_NUM * MAPLOG_BUF))
    #define FOOTER_BUF           (FOOTER_PAGE_NUM * CFG_SLC_ONE_PROG_NUM * (CFG_DIE_NUM - 1) * DATA_LEN_4K)
```

![image-20220413145300880](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220413145300880.png)

PC_TrimFormatAdminWRMixPF.py
