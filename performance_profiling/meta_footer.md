```c++
void pf_mgr::prepare_l2p_meta_data()
{
    l2p_meta *buffer = (l2p_meta *)(g_pf_mgr._pf_meta_data_rw_buf_start + offsetof(pf_meta_data, mpage_meta));
    memcpy((void *)&(buffer->sys_blk), (void *)&g_sys_blk_mgr, sizeof(sys_blk_mgr));
}

void pf_mgr::restore_l2p_meta_data()
{
    l2p_meta *buffer = (l2p_meta *)(g_pf_mgr._pf_meta_data_rw_buf_start + offsetof(pf_meta_data, mpage_meta));
    memcpy((void *)&g_sys_blk_mgr, (void *)&(buffer->sys_blk), sizeof(sys_blk_mgr));
}
```

```
    u64 data_addr{0};
    u32 offset{0};
    collect_stream_footer(STREAM_ID_HOT, data_addr, offset);
```

```c++
void gsd_meta_flusher::collect_stream_footer(u32 stream_id, u64 &data_addr, u32 &offset)
{
    u32 blk = g_pu_mgr.pblk_mgr[0].get_open_sblock(stream_id, ACTION_TYPE_E::PEEK);
    if (INVALID_32BIT == blk) {
        data_addr = INVALID_64BIT;
        offset    = 0;
        return;
    }

    footer_buf_t &buf = g_footer_task.get_unflush_footer(blk);
    if (0 == buf.offset) {
        EVT_LOG(gsd_collect_footer_warn_evtlog, stream_id);
        data_addr = INVALID_64BIT;
        offset    = 0;
        return;
    }

    ASSERT(GSD_FOOTER_HAS_WRONG_BLK, (blk == (GET_BLOCK_FROM_PBA(((footer_entry_t *)(buf.data_addr))->maplog_pba))),
           "INVALID blk. blk: %u, maplog_pba: 0x%llx\n", blk, ((footer_entry_t *)(buf.data_addr))->maplog_pba);
    switch (stream_id) {
    case STREAM_ID_HOT: {
        _footer_meta.hot_open_blk = blk;
        _footer_meta.hot_offset   = buf.offset;
        break;
    }
    case STREAM_ID_GC: {
        _footer_meta.gc_open_blk = blk;
        _footer_meta.gc_offset   = buf.offset;
        break;
    }
    case STREAM_ID_WL: {
        _footer_meta.wl_open_blk = blk;
        _footer_meta.wl_offset   = buf.offset;
        break;
    }
    case STREAM_ID_EH: {
        _footer_meta.eh_open_blk = blk;
        _footer_meta.eh_offset   = buf.offset;
        break;
    }
    default: {
        ASSERT(GSD_MAPLOG_HAS_INVALID_STREAM_ID, false, "INVALID stream. stream_id: %u\n", stream_id);
        break;
    }
    }

    // g_footer_task.gsd_padding_footer_buf_4K_align(buf);
    // BOMB(GSD_FOOTER_OFFSET_NOT_ALIGNED, 0 == (buf.offset % (DATA_LEN_4K)));

    footer_entry_t entry = g_footer_task.check_footer_buf_first_entry(blk);
    ASSERT(GSD_COLLECT_FOOTER_START_BLK_WRONG, (entry.maplog_b2n_range.b2n_pba_start_block == blk),
           "blk[%u] does not equal to b2n_pba_start_block[%llu]\n", blk, entry.maplog_b2n_range.b2n_pba_start_block);
    ASSERT(GSD_COLLECT_FOOTER_END_BLK_WRONG, (entry.maplog_b2n_range.b2n_pba_end_block == blk),
           "blk[%u] does not equal to b2n_pba_end_block[%llu]\n", blk, entry.maplog_b2n_range.b2n_pba_end_block);

    data_addr = buf.data_addr;
    offset = buf.offset;
#if __WITH_SIMULATOR__ == 1
    INF_LOG("GSD collects footer, stream:[%u], data_addr:[0x%llx], offset:[%u]\n", stream_id, data_addr, offset);
#endif

    return;
}
```

```
u64 footer_task::get_gsd_recovery_data_addr(u32 block, u32 offset)
{
    if (_footer_buf[block].data_addr != 0) {
        BOMB(FOOTER_BUF_WRONG, 0);
    }

    /* footer free buffer should always be enough */
    if (!_footer_buf_resource_q.pop(_footer_buf[block].data_addr)) {
        BOMB(FOOTER_BUF_EMPTY, 0);
    }
    _footer_buf[block].offset = offset;
    return (_footer_buf[block].data_addr);
}


footer_buf_t &get_unflush_footer(u32 block) { return _footer_buf[block]; }
```

每个LBA 是512字节

每个LPA 是4K

所以LPA的个数 = LBA的个数 / 8

```
// hot streams
#define STREAM_ID_HOT  0
#define STREAM_ID_HOT2 1

// cold streams
#define STREAM_ID_GC 2
#define STREAM_ID_WL 3
#define STREAM_ID_EH 4
// all data streams
#define STREAM_ID_FOOTER 5

// slc stream
#define STREAM_ID_L2P       6
#define STREAM_ID_L2P_PATCH 7
```

```
每个stream的blokc上user data的flush的结构体是
struct pf_user_data_entry {
    u32 vqid[CFG_PAGE_TYPE_NUM];
    u32 backup_vqid[CFG_PAGE_TYPE_NUM];
    u32 page_type_cnt;
    u32 bad_pl_mask;
    u64 orignal_pba;
} __attribute__((packed));

struct pf_user_data_info {
    u32 flush_cnt;
    pf_user_data_entry pf_user_entry[PF_MAX_FLUSH_CNT];
} __attribute__((packed));

struct pf_mapping_info_entry {
    u64 orignal_pba;
    u32 bad_pl_mask;
    u64 target_pba_main[CFG_PAGE_TYPE_NUM];
    u64 target_pba_backup[CFG_PAGE_TYPE_NUM];
} __attribute__((packed));

struct pf_mapping_info {
    u32 signature;
    u32 stream;
    u32 flush_cnt;
    pf_mapping_info_entry pf_mp_entry[PF_MAX_FLUSH_CNT];
} __attribute__((packed));
```

```
struct pf_parity_info {
    u32 valid;
    u32 page_type_cnt;
    u64 orignal_pba;
    u32 vqid[CFG_PAGE_TYPE_NUM];
    u32 backup_vqid[CFG_PAGE_TYPE_NUM];
    u64 target_pba_main[CFG_PAGE_TYPE_NUM];
    u64 target_pba_backup[CFG_PAGE_TYPE_NUM];
} __attribute__((packed));

struct active_b2n
{
    u32 stream;
    u32 pu_sblk;
    u32 pu_id;
    // u32 qid;
    u32    cur_prog_page;
    u64    next_xlate_seq_die;
    u64    next_b2n_pba;
    u64    cur_b2n_pba;
    u64    cur_prog_b2n_pba{INVALID_64BIT};
    u64    first_b2n_pba;
    u64    last_b2n_pba;
    u64    last_prog_b2n_pba;  // b47 support
    atomic outstanding_b2n_cnt;// outstanding b2n count
    atomic cmpl_b2n_cnt;
    u32    partial_parity_group;
    u32    last_prog_b2n_parity_vqid[CFG_PAGE_TYPE_NUM];
    u32    rsvd[16];
};
```



```
pf_dump_task::run(void)
pf dump每PF_DUMP_NOR_DEBUG_DATA_INTERVAL_US=15ms就将debug data dump到nor里面
1、PF_TASK_STATE_START，会打印ps，初始化所有dump 的pba，g_pf_mgr.prepare_for_dump()。如果是Bypass PF dump，则打印pb
2、PF_TASK_STATE_CHECK_TASK_CLEAR_BEFORE_ABORT，这里会打印hs
3、PF_TASK_STATE_ABORT_HW，如果hw abort成功，则打印ha，失败则打印hf
4、PF_TASK_STATE_FLUSH_USER_DATA，flush从b2n收集的pf_user_data_info结构的user data，保存到pf_mapping_info结构的信息里面，这里面的orignal_pba保存的是原本b2n的地址，target_pba_main是写到PF Stream Block上的地址，从stream0-7，当_flush_index == user_data_info->flush_cnt的时候，这个stream上的user data就fulsh完毕，开始下一个stream
   FLUSH_USER_DATA_ALLOCATE_BACKUP_VQID，向BM申请BACKUP_VQID
   FLUSH_USER_DATA_SEND_VQID_WRITE，g_pf_mgr.pf_get_cur_b2n_pba获取到PF Stream Block上的地址b2n_pba，即为target_pba_main，并记录g_pf_mgr.root_data.user_data_start_pba和g_pf_mgr.root_data.user_data_end_pba
   
5、PF_TASK_STATE_FLUSH_PARITY_DATA，这里就是看各个stream的active_b2n *act_pu_b2n = &g_b2n_task.act_b2n[_flush_stream]，active_b2n结构体数据记录的是每个stream的Block的情况，然后将数据保存到pf_parity_info这个结构体信息里面，需要注意的是act_pu_b2n->partial_parity_group这个是什么判断？
   FLUSH_PARITY_SEND_VQID_WRITE，也是一样的，g_pf_mgr.pf_get_cur_b2n_pba获取到PF Stream Block上的地址b2n_pba，即为target_pba_main，并记录g_pf_mgr.root_data.parity_data_start_pba和g_pf_mgr.root_data.parity_data_end_pba以及g_pf_mgr.root_data.last_written_pba
   
6、PF_TASK_STATE_PARITY_PADDING_0，memset(g_pf_mgr._pf_padding_data_rw_buf_start, 0, DATA_LEN_4K)，通过g_pf_mgr.pf_get_cur_b2n_pba获取当前的b2n pba，更新g_pf_mgr.root_data.last_written_pba为当前的b2n pba

7、PF_TASK_STATE_PREPARE_META_DATA，data_collector.prepare_meta_data()，主要是将数据保存到g_pf_mgr._pf_meta_data_rw_buf_start

8、PF_TASK_STATE_FLUSH_META_DATA，data_flusher.flush_meta_data()，dump到PF Stream Block上的大小是g_pf_mgr._pf_meta_data_rw_buf_start + (_chunk_cnt * DATA_LEN_4K)，其中_chunk_cnt = CFG_ONE_PROG_NUM / CFG_PAGE_TYPE_NUM * PF_META_DATA_PBA_CNT

9、PF_TASK_STATE_FLUSH_SUMMARY_DATA，memcpy((void *)g_pf_mgr._pf_summary_data_rw_buf_start, (void *)&g_pf_mgr.summary_data, sizeof(g_pf_mgr.summary_data));并记录dump到nand的地址g_pf_mgr.root_data.summary_data_pba，需要注意的是_chunk_cnt = CFG_ONE_PROG_NUM / CFG_PAGE_TYPE_NUM，dump到PF Stream Block上的大小就是g_pf_mgr._pf_summary_data_rw_buf_start + (_chunk_cnt * DATA_LEN_4K)

9、PF_TASK_STATE_FLUSH_ROOT_DATA，主要是将debug data保存到(pf_nor_data *)g_pf_mgr._pf_root_data_rw_buf_start，并在PF_TASK_STATE_FLUSH_FINISHED，打印出pe，并将_pf_handled_flag设置为1（vu使用的）

```

```
gsd保存的meta data
GSD_EXIT_TASK_STATE_WAIT_MAPLOG_SEAL_DONE中调用gsd_trig_meta_flusher()
首先在gsd_meta_flusher::execute()中执行保存数据
1、prepare_l1_map()，dump_addr【DDR_L1_MAP_START】，size：DDR_L1_MAP_SIZE+padding
然后将【DDR_L1_MAP_START】写到nand上【GSD_L1MAP_LBA】并记录地址到_flush_params._begin_lba，_flush_meta中记录写入的4K大小的个数，并随时更新g_pu_mgr.pblk_mgr[0].set_sblock_last_append_PBA(INVALID_32BIT, pba)
在等待将_flush_meta刷到nand的过程中，获取mpage summary的个数g_mpage_gc_task.get_summary_flush_offset()，并保存到rfs中g_small_share_rfs_file.set_mpage_summary_offset(offset)

2、prepare_mpage_summary()，dump_addr【DDR_MPAGE_SUMMARY_FLUSH_START】，size：(g_mpage_gc_task.get_summary_flush_offset()) * (sizeof(struct mpage_summary))+padding
然后将【DDR_MPAGE_SUMMARY_FLUSH_START】写到nand上【GSD_MPAGE_SUMMARY_LBA】并记录地址到_flush_params._begin_lba，_flush_meta中记录写入的4K大小的个数，并随时更新g_pu_mgr.pblk_mgr[0].set_sblock_last_append_PBA(INVALID_32BIT, pba)

3、prepare_sys_blk_mgr_meta()，dump_addr【GSD_MEM_BUF_START】，size： sizeof(sys_blk_mgr)，按4K对齐
然后将【GSD_MEM_BUF_START】写到nand上【GSD_SYS_BLK_MGR_LBA】并记录地址到_flush_params._begin_lba，_flush_meta中记录写入的4K大小的个数，并随时更新g_pu_mgr.pblk_mgr[0].set_sblock_last_append_PBA(INVALID_32BIT, pba)

4、prepare_maplogs()，dump_addr【DOWNLOAD_BUF_START】，size： collect_maplogs计算得到
然后将【DOWNLOAD_BUF_START】写到nand上【GSD_MAPLOG_LBA】并记录地址到_flush_params._begin_lba，_flush_meta中记录写入的4K大小的个数，并随时更新g_pu_mgr.pblk_mgr[0].set_sblock_last_append_PBA(INVALID_32BIT, pba)
注意：
// FIXME: there will be one open block for hot stream at most
_maplog_meta.hot_open_blk_range  = buf->maplog_b2n_range.all
_maplog_meta.hot_open_blk_offset = buf->offset


5、prepare_footer_hot()，dump_addr和size： collect_stream_footer(STREAM_ID_HOT, data_addr, offset)计算得到
然后将【buf_addr】写到nand上【GSD_FOOTER_HOT_LBA】并记录地址到_flush_params._begin_lba，_flush_meta中记录写入的4K大小的个数，并随时更新g_pu_mgr.pblk_mgr[0].set_sblock_last_append_PBA(INVALID_32BIT, pba)
注意：
_footer_meta.hot_open_blk = blk;
_footer_meta.hot_offset   = buf.offset;

6、prepare_blk_mgr_meta()，dump_addr【GSD_MEM_BUF_START】，size： sizeof(u64)+sizeof(blk_mgr)+sizeof(u64)+padding，4K对齐
然后将【GSD_MEM_BUF_START】写到nand上【GSD_BLK_MGR_LBA】并记录地址到_flush_params._begin_lba，_flush_meta中记录写入的4K大小的个数，并随时更新g_pu_mgr.pblk_mgr[0].set_sblock_last_append_PBA(INVALID_32BIT, pba)

7、prepare_metadata()，dump_addr【GSD_MEM_BUF_START】，size： sizeof(flush_meta_s)+sizeof(maplog_meta_s)+sizeof(footer_meta_s)+padding，4K对齐
然后将【GSD_MEM_BUF_START】写到nand上【GSD_METADATA_LBA】并记录地址到_flush_params._begin_lba，_flush_meta中记录写入的4K大小的个数，并随时更新g_pu_mgr.pblk_mgr[0].set_sblock_last_append_PBA(INVALID_32BIT, pba)

做完以后会在GSD_EXIT_TASK_STATE_REFRESH_RFS中调用trig_gsd_rfs_meta_flush()
将_metadata刷到rfs里面

gsd Recovery的时候
在rcvry_gsd_ctrl::execute()中恢复数据
1、load_gsd_meta_from_rfs()，将rfs数据读取到_gsd_metadata

2、GSD_RECOVERY_STATE_META_PRELOAD，将nand上【GSD_METADATA_LBA】dump到【GSD_MEM_BUF_START】，然后restore_metadata()将【GSD_MEM_BUF_START】恢复到_flush_meta，_maplog_hot_meta和_footer_meta等

3、GSD_RECOVERY_STATE_BLK_MGR_PRELOAD，将nand上【GSD_BLK_MGR_LBA】dump到【GSD_MEM_BUF_START】，然后restore_blk_mgr()将【GSD_MEM_BUF_START】恢复到g_pu_mgr.pblk_mgr[0]

4、GSD_RECOVERY_STATE_L1MAP_PRELOAD，将nand上【GSD_L1MAP_LBA】dump到【DDR_L1_MAP_START】，然后restore_l1map()啥都不用做(l1map is directly coped into its own address)，并获取g_small_share_rfs_file.get_mpage_summary_offset()

5、GSD_RECOVERY_STATE_MPAGE_SUMMARY_PRELOAD，将nand上【GSD_MPAGE_SUMMARY_LBA】dump到【DDR_MPAGE_SUMMARY_FLUSH_START】，然后restore_mpage_summary()啥都不用做(mpage summary is directly coped into its own address),只需要设置大小g_mpage_gc_task.set_summary_flush_offset(g_small_share_rfs_file.get_mpage_summary_offset())

6、GSD_RECOVERY_STATE_SYS_BLK_MGR_PRELOAD，将nand上【GSD_SYS_BLK_MGR_LBA】dump到【GSD_MEM_BUF_START】，然后restore_sys_blk_mgr()将【GSD_MEM_BUF_START】恢复到g_sys_blk_mgr

7、GSD_RECOVERY_STATE_MAPLOG_PRELOAD，将nand上【GSD_MAPLOG_LBA】dump到【DOWNLOAD_BUF_START】，然后restore_maplogs()将【DOWNLOAD_BUF_START】恢复，首先需要用到maplog_range.all = _maplog_hot_meta[0]，然后调用g_maplog_task.gsd_recovery_maplog

8、GSD_RECOVERY_STATE_FOOTER_HOT_PRELOAD，首先通过_footer_meta调用g_footer_task.get_gsd_recovery_data_addr获取需要dump的地址【_footer_data_addr】，然后将nand上【GSD_FOOTER_HOT_LBA】dump到【_footer_data_addr】，然后啥都不用做


```

```
获取b2n_pba
void pf_mgr::pf_get_cur_b2n_pba(u64 *pba, u32 *raid_cmd)
{
    active_b2n *act_pu_b2n = &g_b2n_task.act_b2n[STREAM_ID_PF];
    u32         block      = act_pu_b2n->pu_sblk;
    u32         pu_id      = act_pu_b2n->pu_id;
    u64         b2n_pba    = act_pu_b2n->next_b2n_pba;

    if (pba != nullptr) {
        *pba = b2n_pba;
    }

    if (raid_cmd != nullptr) {
        *raid_cmd = (u32)g_pu_mgr.pu_info[pu_id][block].good_die[act_pu_b2n->next_xlate_seq_die].raid_cmd;
    }
}

struct pf_summary_data {
    pf_mapping_info mapping_info[STREAM_ID_MAX];
    pf_parity_info parity_info[STREAM_ID_MAX];
    u64 meta_data_pba[PF_META_DATA_PBA_CNT];
} __attribute__((packed));
```

```
poll_hdr_extraction_cpl()
```

```
partial blk handle
主要考虑两种case:
1.footer高压缩率下超出1M大小,做Head Extraction,是否正确
2.PF的block做GC,能否成功,因为footer只有在GC的时候才会用到,GC找到block上的footer,找到的maplog,收集有效数据进行搬移

[2022-04-05 22:07:02.688] [5-0:0:0D|0:0:0:562]   root_data:
[2022-04-05 22:07:02.688]   > root_data_error_code:    0
[2022-04-05 22:07:02.693]   > pf_cnt:                 3
[2022-04-05 22:07:02.697]   > current_pf_sblock:      1
[2022-04-05 22:07:02.701]   > summary_data_pba:      404a40
[2022-04-05 22:07:02.705]   > last_written_pba:      404fc0
[2022-04-05 22:07:02.710]   > nor_sub_sector_idx:     2
[2022-04-05 22:07:02.714]   > user_data_start_pba:   403800
[2022-04-05 22:07:02.719]   > user_data_end_pba:     403bc0
[2022-04-05 22:07:02.723]   > parity_data_start_pba:   403c00
[2022-04-05 22:07:02.728]   > parity_data_end_pba:   403e40
[2022-04-05 22:07:02.733]   > meta_data_start_pba:   404000
[2022-04-05 22:07:02.737]   > meta_data_end_pba:     404a00

```

![image-20220413180942625](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220413180942625.png)

![image-20220413181214851](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220413181214851.png)

#### ![image-20220413183014824](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220413183014824.png)



#### 案例：

```
之前的一次
[2022-04-13 19:15:35.914] [5-0:0:0D|0:0:3:925]   partial_block_handle_hot_stream_footer_scan:
[2022-04-13 19:15:35.921]   > hot_stream_block:           2
[2022-04-13 19:15:35.924]   > scan_start_pba:        80ba40
[2022-04-13 19:15:35.928]   > hot_open_blk_index:     0
[2022-04-13 19:15:35.931]   > b2n_end_page:           b
[2022-04-13 19:15:35.934]
[2022-04-13 19:15:35.935]
[2022-04-13 19:15:35.935] [5-0:0:0D|0:0:3:925]   partial_block_handle_second_scan:
[2022-04-13 19:15:35.941]   > start_read_pba:        80ba40
[2022-04-13 19:15:35.945]   > end_read_pba:          80c000

第一次
[2022-04-13 19:18:52.558] PF
[2022-04-13 19:18:52.568] ps 1
[2022-04-13 19:18:52.618] hs 2
[2022-04-13 19:18:52.618] ha 3
[2022-04-13 19:18:52.618] bad
[2022-04-13 19:18:52.618] pe 8

[2022-04-13 19:19:14.598] [5-0:0:0D|0:0:0:607]   no_need_restore_footer_meta_data:
[2022-04-13 19:19:14.598]   > hot_open_blk1:          7
[2022-04-13 19:19:14.602]   > hot_open_blk2:       ffffffff
[2022-04-13 19:19:14.607]   > hot_offset1:         478f0
[2022-04-13 19:19:14.611]   > hot_offset2:            0
[2022-04-13 19:19:14.615]   > hot_open_blk_cnt:       1

[2022-04-13 19:19:16.703] [5-0:0:0D|0:0:4:184]   partial_block_handle_get_block:
[2022-04-13 19:19:16.709]   > current_read_sblock:    7
[2022-04-13 19:19:16.712]   > stream:                 0
[2022-04-13 19:19:16.716]   > last_maplog_pba:     20312001e869aa
[2022-04-13 19:19:16.720]   > footer_pba:          ffffffffffffffff
[2022-04-13 19:19:16.725]   > last_prog_b2n_pba:    1e86fc0
[2022-04-13 19:19:16.729]   > final_user_b2n_pba:  ffffffffffffffff

[2022-04-13 19:19:16.555] [5-0:0:0D|0:0:4:184]   partial_block_handle_get_footer_meta:
[2022-04-13 19:19:16.561]   > hot_open_blk1:          7
[2022-04-13 19:19:16.565]   > hot_open_blk2:       ffffffff
[2022-04-13 19:19:16.568]   > hot_offset1:         478f0
[2022-04-13 19:19:16.572]   > hot_offset2:            0
[2022-04-13 19:19:16.575]   > is_hot_bad_footer:   1

[2022-04-13 19:19:16.735] [5-0:0:0D|0:0:4:185]   partial_block_handle_hot_stream_footer_scan:
[2022-04-13 19:19:16.742]   > hot_stream_block:           7
[2022-04-13 19:19:16.746]   > scan_start_pba:      ffffffffffffffff
[2022-04-13 19:19:16.751]   > hot_open_blk_index:     0
[2022-04-13 19:19:16.754]   > b2n_end_page:           0


下一次的时候
[2022-04-13 19:22:10.120] [4-0:0:PF
[2022-04-13 19:22:18.839] ps 2
[2022-04-13 19:22:18.840] hs 3
[2022-04-13 19:22:18.840] ha 4
[2022-04-13 19:22:18.840] pe 9

[2022-04-13 19:22:41.191] [5-0:0:0D|0:0:0:604]   restore_footer_meta_data:
[2022-04-13 19:22:41.191]   > hot_open_blk:           7
[2022-04-13 19:22:41.195]   > hot_offset:          10b0
[2022-04-13 19:22:41.200]   > restore_footer_addr: 426343000
[2022-04-13 19:22:41.205]   > footer_data_offset:      2000
[2022-04-13 19:22:41.209]   > is_bad_footer:       0
[2022-04-13 19:22:41.213]   > hot_open_blk_cnt:    1

[2022-04-13 19:22:44.240] [5-0:0:0D|0:0:5:961]   partial_block_handle_get_footer_meta:
[2022-04-13 19:22:44.240]   > hot_open_blk1:          7
[2022-04-13 19:22:44.244]   > hot_open_blk2:       ffffffff
[2022-04-13 19:22:44.249]   > hot_offset1:         10b0
[2022-04-13 19:22:44.253]   > hot_offset2:            0
[2022-04-13 19:22:44.258]   > is_hot_bad_footer:   0

[2022-04-13 19:22:44.427] [5-0:0:0D|0:0:5:961]   partial_block_handle_get_block:
[2022-04-13 19:22:44.427]   > current_read_sblock:    7
[2022-04-13 19:22:44.431]   > stream:                 0
[2022-04-13 19:22:44.436]   > last_maplog_pba:     20300001e90700
[2022-04-13 19:22:44.441]   > footer_pba:          ffffffffffffffff
[2022-04-13 19:22:44.446]   > last_prog_b2n_pba:    1e90fc0
[2022-04-13 19:22:44.451]   > final_user_b2n_pba:  ffffffffffffffff

[2022-04-13 19:22:44.464] [5-0:0:0D|0:0:5:961]   partial_block_handle_get_b2n_pba_end:
[2022-04-13 19:22:44.464]   > hot_open_blk_index:     0
[2022-04-13 19:22:44.469]   > footer_data_addr:    426343000
[2022-04-13 19:22:44.473]   > hot_offset:          10b0
[2022-04-13 19:22:44.478]   > footer_entry_size:     10
[2022-04-13 19:22:44.482]   > b2n_pba_end:                0


// check
u32 tmp_blk = (u32)GET_BLOCK_FROM_PBA(((footer_entry_t *)(footer_dump_addr)->maplog_pba);
if (blk[index] != tmp_blk) {
INF_LOG("PF Restore footer not match ");
}
```

![image-20220414154731259](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220414154731259.png)

```
    case GSD_EXIT_TASK_STATE_WAIT_MAPLOG_SEAL_DONE: {

        INF_LOG("task state:[%u]\n", _state);

        INF_LOG("maplog seal done:[%u]\n", u32(is_maplog_seal_done()));

        INF_LOG("gsd progress:[%u]\n", _gsd_progress._value);

        break;

    }
```

