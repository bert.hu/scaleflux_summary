```c++
====== Recovery stage timecost: ======
    ====== mpage stage timecost: ======
        ====== mpage load timecost: ======
FTL_RECOVERY_STATE_MPAGE_LOAD
watch g_recovery_task._pf_ctrl.restore_ctrl._data_parser._sub_sector_index
    
    bypasspfdump 1
bool l2p_mgr::init()
    sys_gdma_memset

 _gdma_jobid = sys_gdma_memset((void *)_l2_map, INVALID_PBA, ALLOC_L2_MAP_SIZE);
BOMB(GDMA_JOB_ERR, _gdma_jobid != SYS_GDMA_INVALID_JOB);
if (sys_gdma_check_and_release_job(_gdma_jobid))
            
        _l2p_timecost
g_l2p_mgr.prepare_free_pool_for_recovery()
g_l2p_mgr.config_prepare_free_pool_for_recovery()
    
bool l2p_mgr::page_in_for_recovery(u32 l1_id, bool gsd_flag)        
    _l2p_clean_start_time_us = INVALID_64BIT;
    _l2p_clean_end_time_us = INVALID_64BIT;
    _l2p_pagein_start_time_us = INVALID_64BIT;
    _l2p_pagein_end_time_us = INVALID_6 4BIT;

python3 PC_CapacityFullDriverWithPF.py --testLevel=Daily | tee /home/tcnsh/log/PC_CapacityFullDriverWithPF_0302.log
    
enum RECOVERY_STAGE_E
{
    RECOVERY_PF_RESTORE,
    RECOVERY_FTL_RECOVERY,
    RECOVERY_FULL_SCAN_RECOVERY,
    RECOVERY_TOTAL,
    RECOVERY_STAGE_CNT
};

enum CLEAN_UP_NOR_STAGE_E
{
    CLEAN_UP_NOR_PAUSED,
    CLEAN_UP_NOR_START,
    CLEAN_UP_NOR_SEND_MAIN_WRITE_NOR,
    CLEAN_UP_NOR_SEND_MAIN_WRITE_NOR_WAIT,
    CLEAN_UP_NOR_SEND_MIRROR_WRITE_NOR,
    CLEAN_UP_NOR_SEND_MIRROR_WRITE_NOR_WAIT,
    CLEAN_UP_NOR_SEND_MAIN_ERASE_NOR,
    CLEAN_UP_NOR_SEND_MIAN_ERASE_NOR_WAIT,
    CLEAN_UP_NOR_SEND_MIRROR_ERASE_NOR,
    CLEAN_UP_NOR_SEND_MIRROR_ERASE_NOR_WAIT,
    CLEAN_UP_NOR_COMPLETE,
    CLEAN_UP_NOR_CNT
};

class recovery_task:
    u64  _pf_recovery_start_time_us;
    u64  _pf_recovery_end_time_us;
    u64  _pf_restore_start_time_us;
    u64  _pf_restore_end_time_us;
    u64  _pf_ftl_start_time_us;
    u64  _pf_ftl_end_time_us;
    u64  _pf_full_start_time_us;
    u64  _pf_full_end_time_us;
    // task state timecost profiling
    u64   _recovery_stage_timecost[RECOVERY_STAGE_E::RECOVERY_STAGE_CNT];
    u64   _restore_state_timecost[LOW_LEVEL_STATE_E::LOW_LEVEL_STATE_RESTORE_FAIL];
    u64   _full_recovery_state_timecost[FULL_RECOVERY_STATE_E::FULL_RECOVERY_STATE_FAILED];

class rcvry_pf_ftl_ctrl:
    u64  _start_time_us;
    u64  _end_time_us;    
    u64   _ftl_stage_timecost[FTL_RECOVERY_STATE_E::FTL_RECOVERY_STATE_FAILED];

    // task state timecost profiling
    u64   _l1_recovery_state_timecost[L1_RECOVERY_STATE_E::L1_RECOVERY_STATE_FAILED];
    u64   _mpage_load_state_timecost[MPAGE_LOAD_STATE_E::MPAGE_LOAD_FAILED];
    u64   _patch_dispatch_state_timecost[PATCH_DISPATCH_STATE_E::PATCH_DISPATCH_STATE_FAILED];
    u64   _partial_blk_handle_state_timecost[PARTIAL_BLOCK_HANDLE_STATE_E::PARTIAL_BLOCK_HANDLE_COMPLETE];
    u64   _full_drive_scan_state_timecost[FULL_DRIVE_SCAN_STATE_E::FULL_DRIVE_SCAN_STATE_FAILED];

class pf_data_parser:
    u64  _start_time_us;
    u64  _end_time_us;
    u64   _restore_stage_timecost[LOW_LEVEL_STATE_E::LOW_LEVEL_STATE_RESTORE_FAIL];
class pf_restore_cleaner
_pf_cleaner._nor_cleanup_state
 
u64 time_start = bsp_platform::get_time_us();
u64 time_end = bsp_platform::get_time_us();
u64 time_used = time_end - time_start;
if (_restore_state_timecost[LOW_LEVEL_STATE_READ_ROOT_DATA] < time_used) { //cstat !MISRAC2004-13.7_b !MISRAC++2008-0-1-2_b
    _restore_state_timecost[LOW_LEVEL_STATE_READ_ROOT_DATA] = time_used;
}

_start_time_us = bsp_platform::get_time_us();
_end_time_us = bsp_platform::get_time_us();
u64 time_used = _end_time_us - _start_time_us;
_restore_stage_timecost[RESTORE_STAGE_READ_ROOT_DATA] = time_used;
g_recovery_task._pf_ctrl.restore_ctrl._restore_stage_timecost[RESTORE_STAGE_READ_META_DATA] = time_used;



    u32 _sent_cnt;
    u32 _sub_sector_index;
    u32 _job_id;
    bool _find_valid;
    bool _is_main;
    u32 _read_meta_state;
    bool _read_meta_success;
    u32 _read_summary_state;
    u32 _read_root_data_state;
    bool _read_summary_success;
    u64  _start_time_us;
    u64  _end_time_us;
```

```c++
void task_scheduler::show_task_state_time_cost(u32 opt __unused)
{
    for (u32 i = 0; i < MAX_TASK_NUMBER_IN_SCHEDULER; i++) {
        if (task_tbl[i]._ptask != nullptr) {
            if (get_coreid() == task_tbl[i]._enabled_core) {
                printf("\tTask: [%s]\n", task_tbl[i]._ptask->task_name());
                for (int j = 0; j < MAX_STATE_COUNT_IN_TASK; j++) {
                    u64 tick = task_tbl[i]._ptask->_task_state_cost[j];
                    if (tick) {
                        printf("\t\tState %2d: %16lld tick / %lld us\n", j, tick, bsp_platform::tickcnt2us(tick));
                    }
                }
            }
        }
    }
}

void task_scheduler::init_task_time_cost()
{
    memset64((void*)&_time_cost, 0, sizeof(_time_cost));
    memset64((void*)&_call_cnt, 0, sizeof(_call_cnt));
    memset64((void*)&_fin_cnt, 0, sizeof(_fin_cnt));

    for (u32 i = 0; i < MAX_TASK_NUMBER_IN_SCHEDULER; i++) {
        if (task_tbl[i]._ptask != nullptr) {
            for (int j = 0; j < MAX_STATE_COUNT_IN_TASK; j++) {
                (task_tbl[i]._ptask)->_task_state_cost[j] = 0;
            }
        }
    }
}
```

```txt
===================Batch Summary===================
PC_AsyncWriteMixRandomPF.py                    ------Test Pass
PC_FormatMixPF.py                              ------Test Pass
PC_IOMixPF.py                                  ------Test Pass
PC_SequentialIOMixAsyncPF.py                   ------Test Pass
PC_ProgramErrorDuringPFWithGCTask.py           ------Test Pass
PC_ProgramErrorDuringPFWithRDTask.py           ------Test Pass

PC_PFMixFormatOnFullDrive.py                   ------Test Fail   simulator pass -- IO timeout b2nstat
PF_FullQDsMixAsyncPF.py                        ------Test Fail   simulator pass -- DMC

PC_RandomWriteWithPFCrashRecovery.py           ------Test Fail
PC_AsyncWriteWithPFCrashRecovery.py            ------Test Fail
PC_CapacityWithPFCrashRecovery.py              ------Test Fail
PC_DiffIOSizeWithPFCrashRecovery.py            ------Test Fail

PC_OverlapDataMixAsyncPF.py                    ------Test pass   simulator pass
PC_PFWithDoubleReadErrorInXorOnOpenBlock.py    ------Test pass   simulator pass
===================================================
d5444b6f77413cdd534a58ff8084354513dbe054
```

### 使用nvme命令更新bl3

```txt
scp root@192.168.33.2:/home/tcnsh/bert/crash_recovery/myrtle_fw/myrtle_fw.asic.wout.bl3.dev_build.user_img.csdu4spc38a0_vU.0.0@9f8f13_13354.strip.elf.signed.bin .

lspci -d cc53:
sudo /root/rescanpcie.sh
modprobe nvme
cd /etc/modprobe.d/blacklist.conf

sudo nvme fw-download /dev/nvme0n1 -f /home/tcnsh/b47_img/myrtle_fw.asic.wout.bl3.dev_build.user_img.csdu4spc38a0_vU.0.0@561e45_13417.strip.elf.signed.bin -x 0x20000

sudo nvme fw-activate /dev/nvme0n1 -s 1 -a 1

sudo /home/tcnsh/samanea/Samanea_Platform/spdk/scripts/setup.sh reset
sudo /root/rescanpcie.sh
sudo modprobe nvme

nvme list

sudo nvme fw-download /dev/nvme0n1 -f /home/tcnsh/b47_img/test.bin -x 0x20000

sudo nvme fw-activate /dev/nvme0n1 -s 1 -a 1

其他
killall python3
```

```txt
python3 ../NameSpace/NS_MultiNamespaceIOMixGSD.py --testLevel=Daily | tee /home/tcnsh/log/NS_MultiNamespaceIOMixGSD_0301.log


python3 PC_IOMixPF.py --testLevel=Daily | tee /home/tcnsh/log/PC_IOMixPF_0228.log


python3 PC_RandomWriteWithPFCrashRecovery.py | tee /home/tcnsh/log/PC_RandomWriteWithPFCrashRecovery_0218.log
```

#### Crash Recovery DMC

Read data from slba 0x1bca10c16, elba 0x1be610c15, lba count 0x100, sector Size 0x200

Send read command from slba 7459743744 * 512 and dmc slba 7459743878 nsid 1



slba  0x1BCA2A800 

dmc slba 0x1BCA2A886, lpa:0x37495510, pba:0x4a0e700c78b2f

写入数据的lpa

lba:0x1bca2a816 , lpa: 0x37945502, pba is: 0x20306f00cb200b

lbacnt=0x400

lpa: 37945502 -> 

0x1bca2ac16

版本commit

**da1d0753e9db4ac3d82682182270ae95ae36650bz**

```
找到dmc的slba，计算lpa和pba，根据脚本的打印找到最近slba写入的数据，计算其lpa和pba，用getpba将这段lba打印出来，发现dmc的刚好是解析出来pba为0xffffffff的位置，怀疑时pba2lba没获取到，因此在pba2lba的vu函数parse_hdr_extraction_pba2lba里面将hdr_buf全打出来，发现是有该dmc地址的数据，只是没有读上来，所以review crash recovery代码，发现hdr_extraction中有tot_count的限制，修改限制后问题解决
```

hotfix/MYR-944

```
=============== Test Summary ==============
Test Case Name:    PC_RandomWriteWithPFCrashRecovery
Test Case Execution Time:   2:02:34.555831 (hh:mm:ss.xxxx)
Test Case Result:  Pass
```

PC_RandomWriteWithPFCrashRecovery.py

```
vim /home/tcnsh/samanea/Samanea_Lib/IO.py
# if self.testCaseIns.options.enabl eIOLogger:
脚本打开log开关
parse_hdr_extraction_pba2lba
    //INF_LOG("hdr_buf: %p\n", hdr_buf);
    //print_mem(hdr_buf, HDR_BUF_SIZE, "hdr_buf\n");
    hdr_buf: 0x416062000
```

#### Add timecost statistics for interruptions

* Add timecost statistics for interruptions
* Used to locate and solve problems quickly

```c++
@@ -430,6 +430,7 @@ void arch_handle_interrupts(void)
         u32 coreid = bsp_platform::get_coreid();
         task_scheduler *psched = g_kernel.get_sched(coreid);
         psched->set_state_timecost_check(false);
+        u64 time_start = bsp_platform::get_time_us();

         if ((intr == IRQ_UART0) || (intr == IRQ_UART1)) {
             u8 ch = handle_uart_isr(intr - IRQ_UART0);
@@ -480,7 +481,12 @@ void arch_handle_interrupts(void)
         }

         gicv3_end_of_interrupt(intr);
-
+        u64 time_end  = bsp_platform::get_time_us();
+        u64 time_used = time_end - time_start;
+        if ((intr != IRQ_UART0) && (intr != IRQ_UART1) && (time_used > 2000)) {
+            COLOR_LOG(COLOR_RED,
+                ">>> WARNING: intr %d time cost: %lld us, Exceed allowed exec time!\n", intr, time_used);
+        }
     } while (1);
 #endif
 }

9f8f1317a44ce9dca76d11249fc81fccc1f61a15
pf recovery timecost profiling
```

```
AssertionID/AID_AsyncIOWithAssertPFRecovery.py
AssertionID/AID_FoldingWithAssertPFRecovery.py
AssertionID/AID_FormatWithAssertPFRecovery.py
AssertionID/AID_GCWithAssertPFRecovery.py
AssertionID/AID_ProgramErrorWithB2NAssert.py
AssertionID/AID_ReadScrubWithAssertGC.py
AssertionID/AID_SyncIOWithAssertPFRecovery.py
BadDiePlane/BDP_MarkBBDieWlTrimFormatGSDPF.py
BadDiePlane/BDP_MarkBBEraseErrorWLTargetBlockPFGSD.py
BadDiePlane/BDP_MarkBBRSHighLdpcReadErrorProgramErrorPFGSD.py
BadDiePlane/BDP_MarkBBRSHighLdpcReadErrorProgramErrorTrimFormatPFGSD.py
BadDiePlane/BDP_MarkBBRSReadErrorProgramErrorPFGSD.py
BadDiePlane/BDP_MarkBBRSReadErrorProgramErrorTrimFormatPFGSD.py
BadDiePlane/BDP_MarkBBReadErrorProgramErrorEraseErrorPFGSD.py
BadDiePlane/BDP_MarkBBReadErrorProgramErrorFormatTrimPFGSD.py
BadDiePlane/BDP_MarkBBReadErrorProgramErrorPFGSD.py
ErrorHandle/EH_MultiReadErrorGCSourcePFGSD.py
ErrorHandle/EH_MultiReadErrorPFGSD.py
ErrorHandle/EH_ProgramErrorAsyncIOFormatTrimPFGSD.py
ErrorHandle/EH_ProgramErrorEHTargetPFGSD.py
ErrorHandle/EH_ProgramErrorGCPFGSD.py
ErrorHandle/EH_ProgramErrorMixReadErrorAndPFOnMpageBlock.py
ErrorHandle/EH_ProgramErrorMultiBlockPFGSD.py
ErrorHandle/EH_ProgramErrorOnMPageBlockMixPF.py
ErrorHandle/EH_ProgramErrorPaddingMeetProgramErrorMixPFOnOpenParity.py
ErrorHandle/EH_ReadErrorGCTargetPFGSD.py
ErrorHandle/EH_ReadErrorGCTargetTrimFormatPFGSD.py
FeatureIDs/FID_InterruptCoalescingBasicVerify.py    ------Test Pass
FeatureIDs/FID_NumberOfQueuesMixQueuesNumberTest.py    ------Test Pass
FeatureIDs/FID_TimestampVerify.py    ------Test Pass
NameSpace/NS_MultiNamespaceIOMixGSD.py
NameSpace/NS_MultiNamespaceIOMixPF.py
PowerCycle/PC_AsyncIOMixContinueProbePF.py        ------Test Pass
PowerCycle/PC_AsyncIOMixGCWithPF.py    ------Test Fail 
PowerCycle/PC_AsyncIOMixProbePF.py        ------Test Pass
PowerCycle/PC_AsyncWriteMixGCWithPF.py    ------Test Fail 
PowerCycle/PC_CapacityFullDriverWithPF.py    ------Test Fail reconnect failed
PowerCycle/PC_CapacityOverlapFullDriverWithPF.py    ------Test Fail DMC
PowerCycle/PC_CapacityPageInFormatDuringWithPF.py        ------Test Pass
PowerCycle/PC_CapacityPageInMixAdminWithPF.py        ------Test Pass
PowerCycle/PC_CapacityPageInMixAsyncMutilWriteAndTrimWithPF.py    ------Test Fail dmc
PowerCycle/PC_CapacityPageInMixDoubleReadErrorWithPF.py        ------Test Pass
PowerCycle/PC_CapacityPageInMixFoldingDoneWithPF.py        ------Test Pass
PowerCycle/PC_CapacityPageInMixFormatWithPF.py        ------Test Pass
PowerCycle/PC_CapacityPageInMixGCNotDoneWithPF.py        ------Test Pass
PowerCycle/PC_CapacityPageInMixGCWithPF.py        ------Test Pass
PowerCycle/PC_CapacityPageInMixIOWithPF.py        ------Test Pass
PowerCycle/PC_CapacityPageInMixMutilWriteAndTrimWithPF.py    ------Test Pass
PowerCycle/PC_CapacityPageInMixReadDisturbWithPF.py          ------Test Pass
PowerCycle/PC_CapacityRandomTypeDiffSizeIOMixFullQDWithPF.py ------Test Fail IO timeout
PowerCycle/PC_CapacityWithPF.py                   ------Test Pass
SysFileSystem/SYS_FS_EXT4WithPFByRelay.py                   ------Test Pass
SysFileSystem/SYS_FS_xfsWithPFByRelay.py  ------Test Fail mount: /mnt/nvme0n1: special device /dev/nvme0n1 does not exist(nvme1n2)
SystemPF/SPF_ShutdownWithDataIntegration.py  ------Test Fail mount: /mnt/nvme0n1: special device /dev/nvme0n1 does not exist.(nvme1n2)
SystemPF/SPF_ShutdownWithoutIO.py  ------Test Fail mount: /mnt/nvme0n1: special device /dev/nvme0n1 does not exist.(nvme1n2)
```

```
BL2 Core-0 start, Feb 16 2022 16:43:10, clk:80032000, fpga ver:63686970, cpuclk:1500 MHz

BL2 cores state:
Core-1: arrived in BL2
Core-2: arrived in BL2
Core-3: arrived in BL2
Core-4: arrived in BL2
Core-5: arrived in BL2
Core-6: arrived in BL2
Core-7: arrived in BL2
BL2 Core-0 calling board_setup()
BL2 Core-0 calling set_hw_cache_attr()
BL2 Core-0 calling mmu_setup()
BL2 Core-0 pcie_init()...
PCIe Port0 Link Status[4:0] status changed to: 0x00
PCIe Port0 Link Status[4:0] status changed to: 0x02
PCIe Port0 Link Status[4:0] status changed to: 0x00
PCIe Port0 Link Status[4:0] status changed to: 0x0c
PCIe Port0 Link Status[4:0] status changed to: 0x0e
PCIe Port0 Link Status[4:0] status changed to: 0x10
done
BL2 Core-0 calling ddr_init()...
ddr_init() rc: 0
BL2 Core-0 clear all ddr 0xc00000000 size 0x400000000 by GDMA...done
BL2 Core-0 invalidate cache for DDR...done
BL2 Core-0 check ECC status after clear DDR


[4-0:0:0D|0:0:4:458]   pf_restore_start:
PF Restore Start.
```

NOR读取异步接口

```
submit_nor_read_job
nor_check_job_done
nor_release_job_id

    case EVT_STATE_ERASE_NOR_CHECKPOINT: {
        u32 nor_addr = CFG_NOR_EVT_LOG_META_START;
        _nor_job_id  = g_nor_task.submit_nor_erase_job(nor_addr, false, false);

        if (INVALID_32BIT != _nor_job_id) {
            INF_LOG("EVT Erase Nor\n");
            _state = EVT_STATE_ERASE_NOR_CHECKPOINT_WAIT;
        }

        break;
    }

    case EVT_STATE_ERASE_NOR_CHECKPOINT_WAIT: {
        NOR_OPERATION_STATUS_E job_status = NOR_OPERATION_STATUS_UNKNOWN;
        NOR_CHECK_JOB_RESULT_E result     = g_nor_task.nor_check_job_done(_nor_job_id, job_status);

        if (result == NOR_CURRENT_JOB_DONE) {
            g_nor_task.nor_release_job_id(_nor_job_id);
            INF_LOG("EVT Erase Nor Complete\n");
            _state = EVT_STATE_WRITE_NOR_CHECKPOINT;
        }

        break;
    }

    case EVT_STATE_WRITE_NOR_CHECKPOINT: {
        evt_nor_data_t *buffer = (evt_nor_data_t *)evt_cache_buf;
        buffer->signature      = 0x474F4C545645;
        memcpy((void *)&buffer->key_info, (void *)&evt_key_info, sizeof(evt_key_info_t));

        u32  nor_addr  = CFG_NOR_EVT_LOG_META_START;
        u32 *data_addr = (u32 *)buffer;
        u32  data_size = NOR_ACCESS_LEN(sizeof(evt_nor_data_t));
        _nor_job_id    = g_nor_task.submit_nor_write_job(nor_addr, data_addr, data_size, false);

        if (INVALID_32BIT != _nor_job_id) {
            INF_LOG("EVT Write Nor Checkpoint\n");
            INF_LOG("next pba: %llx, curdie: %d, next die: %d\n", get_stb_mgr()->get_next_b2n_pba().all,
                    get_stb_mgr()->get_stb_die_ctrl().cur_die, get_stb_mgr()->get_stb_die_ctrl().next_die);
            _state = EVT_STATE_WRITE_NOR_CHECKPOINT_WAIT;
        }

        break;
    }

    case EVT_STATE_WRITE_NOR_CHECKPOINT_WAIT: {
        NOR_OPERATION_STATUS_E job_status = NOR_OPERATION_STATUS_UNKNOWN;
        NOR_CHECK_JOB_RESULT_E result     = g_nor_task.nor_check_job_done(_nor_job_id, job_status);

        if (result == NOR_CURRENT_JOB_DONE) {
            g_nor_task.nor_release_job_id(_nor_job_id);
            INF_LOG("EVT Write Nor Checkpoint Complete\n");

            set_evtlog_write_ready();
            set_evtlog_read_ready();
            evt_key_info._die_switch_flag = false;
            _state                        = EVT_STATE_IDLE;
        }

        break;
    }

    case EVT_STATE_READ_NOR_CHECKPOINT: {
        evt_nor_data_t *buffer    = (evt_nor_data_t *)evt_cache_buf;
        u32             nor_addr  = CFG_NOR_EVT_LOG_META_START;
        u32 *           data_addr = (u32 *)buffer;
        u32             data_size = NOR_ACCESS_LEN(sizeof(evt_nor_data_t));
        _nor_job_id               = g_nor_task.submit_nor_read_job(nor_addr, data_addr, data_size, false);

        if (INVALID_32BIT != _nor_job_id) {
            INF_LOG("EVT read NOR checkpoint\n");
            _state = EVT_STATE_READ_NOR_CHECKPOINT_WAIT;
        }

        break;
    }

    case EVT_STATE_READ_NOR_CHECKPOINT_WAIT: {
        NOR_OPERATION_STATUS_E job_status = NOR_OPERATION_STATUS_UNKNOWN;
        NOR_CHECK_JOB_RESULT_E result     = g_nor_task.nor_check_job_done(_nor_job_id, job_status);

        if (result == NOR_CURRENT_JOB_DONE) {
            g_nor_task.nor_release_job_id(_nor_job_id);
            evt_nor_data_t *buffer = (evt_nor_data_t *)evt_cache_buf;

            INF_LOG("EVT read NOR checkpoint complete\n");
            if (buffer->signature != 0x474F4C545645) {
                INF_LOG("No valid checkpoint in Nor\n");

                // set read ready, host can only read some from cache buffer
                set_evtlog_read_ready();

                _state = EVT_STATE_IDLE;
            }
            else {
                memcpy((void *)&evt_key_info, (void *)&buffer->key_info, sizeof(evt_key_info_t));
                INF_LOG("next pba: %llx, curdie: %d, next die: %d, total size: %lld\n",
                        get_stb_mgr()->get_next_b2n_pba().all, get_stb_mgr()->get_stb_die_ctrl().cur_die,
                        get_stb_mgr()->get_stb_die_ctrl().next_die, get_log_tot_size());

                _state = EVT_STATE_READ_SCAN_LAST_PROG_PBA_INIT;
            }
        }

        break;
    }
```

```
_full_cache_flag 1, L1_MAP_ENTRY_NUM 1831416, L2_MAP_ENTRY_SIZE: 4096, total: 7501479936
[l2p_mgr::init]sys_gdma_memset timecost 1527545 us
```

