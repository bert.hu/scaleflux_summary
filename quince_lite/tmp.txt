GET_POOL_ID // 拿到的是0~3
通过POOL ID 去获取全局的FCT POLL ID
需要知道FCQ ID
那么用 FCQ_ID * 4 + POOL_ID去获取全局的FCT_POLL_ID

#define GET_GLOBAL_POOL_ID(fcq_qid, pool_id) \
    (((fcq_qid) * POOL_CNT_PER_FCQ) + pool_id)
#define GET_FCQ_QID(global_pool_id) (global_pool_id / POOL_CNT_PER_FCQ)

 u32 global_pool_id = GET_GLOBAL_POOL_ID(fcq_id, pool_id);


FCE_CSR_AUTO_RD(NAND_ADDR_CONFIG_0);
u8 bsp_platform::get_nand_addr_lun_width_config()
N_BIT_MASK(width) 


union nand_addr_t {
    struct {
        union {
            struct {
                u32 page    : CFG_NAND_ADDR_PAGE_BITS;
                u32 plane   : CFG_NAND_ADDR_PLANE_BITS;
                u32 block   : CFG_NAND_ADDR_BLOCK_BITS;
                u32 lun     : CFG_NAND_ADDR_LUN_BITS;
                u32 rsvd    : (32- NAND_ADDR_BITS);
            };

            u32 row_addr;
        };

        union {
            struct {
                u32 la          : 2; // Plane page logic address, 4K offset Range: 0~3
                u32 rsvd2       : 1;
                u32 die_id_high : 1;
                u32 page_type   : 4; // Range: 0~3 for regular page, 7 is for BiCS6 LM Page
                u32 die_id_low  : 4; // lun_width=1: lun_id = die_id[0]
                u32 ch          : 4;
            };
            u32 high_32bit;
        };
    };

    u64 all;
};

pba.offset                                0                 1                2                3
pba.ccp_off                               0 1 2 3 4 5 6 7   0 1 2 3 4 5 6 7  0 1 2 3 4 5 6 7  0 1 2 3 4 5 6 7
(pba.offset << 1) + pba.ccp_off / 4       0 0 0 0 1 1 1 1   2 2 2 2 3 3 3 3  4 4 4 4 5 5 5 5  6 6 6 6 7 7 7 7



packer_handle_newsqe

4K 4K 4K 4K
4K 4K 4K 4K
4K 4K 4K 4K
4K 4K 4K 4K
总共64k

4K 4K 4K 4K 4K 4K
4K 4K 4K 4K 4K 4K
4K 4K 4K 4K 4K 4K
4K 4K 4K 4K 4K 4K
总共96k


3+4+1+68=76
3+2+1+70=76

fct_alloc_entries_idx(fct_pool_id, &fct_idx, 1);
这里申请的pool_id是全局的fct_pool_id
拿到的fct_idx就是idx

fct_entry_erase_cmd::set_fct_idx这里的fct_idx是12个bit的full_fct_idx，包含了pool_idx（0~3）和idx（0~511）的组合
GEN_FULL_FCT_IDX(pool_id, fct_idx),这里的pool_id无论是填fct_pool_id还是pool_idx都可以的

Erase Command
1、allocate fct entries idx
fct_alloc_entries_idx(fct_pool_id, &idx, 1);

2、通过申请的fct_pool_id和fct_idx去获取fct_entry
fct_get_entry(fct_pool_id, idx)

2、fcl_erase_cmd::send_slc_erase_cmd/fcl_erase_cmd::send_tlc_erase_cmd
入参fct_entry， GEN_FULL_FCT_IDX(pool_id, idx)， fcq_qid, pba, bad_plane_mask
首先通过pba拿到nand addr
通过 GEN_FULL_FCT_IDX(pool_id, fct_idx)拿到full_fct_idx
接着填写fct_entry_erase_cmd的信息

3、 fcl_be_alloc_send_msg
入参是fcq_qid, opcode， full_fct_idx， fuse和sub_opc
还是先通过full_fct_idx去拿到idx和pool_id
然后fcq_alloc_sq_entries(fcq_qid, &ace_msg, 1)去拿到ace_msg
然后填写fcq_sq_cmd_msg

4、fcq_send_sq_msg(fcq_qid, 1)

收：
1、fcl_be_poll_cpl
入参是fcq_qid，回调函数cmd_cpl_cb_t cmd_cpl_cb， cnt， timeout， assert_imm
首先fcq_fetch_cq_msg(fcq_qid)
拿到cpl以后，从cpl中拿到full_fct_idx，然后调用回调函数cmd_cpl_cb(fcq_qid, full_fct_idx)
接着释放cq entry --> fcq_release_cq_entry(fcq_qid)

2、回调函数erase_task_check_erase_cpl
入参是fcq_qid和full_fct_idx
通过full_fct_idx拿到fct_idx和pool_id
通过fct_idx和pool_id拿到fct_entry
从fct_entry_erase_cmd中拿到nand_addr
通过nand_addr拿到pba
从fct_entry_erase_cmd拿到nand_status
调用eh处理nand_status

最后释放fct entries --> fct_release_entries_by_idx(pool_id, &fct_idx, 1);


bool fpga_send_normal_write_cmd(FCL_WRITE_SRC_E src_sel, FCL_WRITE_DESTINATION_E dst_sel, u32 stream_id, 
                                u64 lba, u64 pba, u8 *write_buf, u8 bad_plane_mask, u8 nlb, u8 stripe_idx,
                                bool bstart, bool pstart, bool blast, bool plast, FCL_READ_LBA_MODE_E lba_md, u8 lp_fmt);

bool fpga_send_program_cmd(FCL_PROGRAM_DATA_SOURCE_E src_sel, bool slc_mode, u32 stream_id, 
                            u64 pba, u64 src_addr, u64 pgd_vqid, u8 bad_plane_mask, u8 stripe_idx,
                            FCL_RAID_CMD_E raid_cmd, u8 lp_fmt, u8 raid_grp_id, bool sealed, u8 pe_cnt)

bool fpga_send_read_command(FCL_READ_SCHEME_E read_scheme,
                            FCL_READ_SUB_OPC_E sub_opc,
                            bool slc_mode,
                            u64 pba,
                            u8 bad_plane_mask,
                            u64 buf_addr,
                            u64 lba,
                            FCL_READ_LBA_MODE_E lba_md,
                            u32 lp_bitmap,
                            FCL_LP_FMT_DATA_SIZE_E lp_fmt,
                            FCL_RAID_CMD_E raid_cmd,
                            u32 fcq_id,
                            u32 pool_id,
                            u8 rfnd,
                            u8 vt,
                            u8 pe_cnt)


[0-2-1970/1/1|0:0:14:159]    TestBody : test write_pba.block 20
[0-2-1970/1/1|0:0:14:159]    TestBody g_write_buf[0x31c0000] g_read_buf[0x3178000], read_buf_addr 0x3178000
[0-2-1970/1/1|0:0:14:159]    pba 0x14, slc_mode 0, bad_plane_mask c0
[0-2-1970/1/1|0:0:14:159]    Wait erase poll cpls.
[0-2-1970/1/1|0:0:14:160]    fpga_erase_cpl_cb: nand_status: e00, fcq_qid 15, fct pool id 60
[0-2-1970/1/1|0:0:14:160]    Send W2P, page_type_num 1, b2n_w2p_num 24
[0-2-1970/1/1|0:0:14:160]    w2p lba 0
[0-2-1970/1/1|0:0:14:160]    write_pba 0x14000000, 1 1 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 1
[0-2-1970/1/1|0:0:14:160]    write_pba 0x14000001, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 2
[0-2-1970/1/1|0:0:14:160]    write_pba 0x14000002, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 3
[0-2-1970/1/1|0:0:14:160]    write_pba 0x14000003, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 4
[0-2-1970/1/1|0:0:14:160]    write_pba 0x14000004, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 5
[0-2-1970/1/1|0:0:14:160]    write_pba 0x14000005, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 6
[0-2-1970/1/1|0:0:14:160]    write_pba 0x14000006, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 7
[0-2-1970/1/1|0:0:14:160]    write_pba 0x14000007, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 8
[0-2-1970/1/1|0:0:14:160]    write_pba 0x14000008, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 9
[0-2-1970/1/1|0:0:14:160]    write_pba 0x14000009, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 10
[0-2-1970/1/1|0:0:14:160]    write_pba 0x1400000a, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 11
[0-2-1970/1/1|0:0:14:160]    write_pba 0x1400000b, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 12
[0-2-1970/1/1|0:0:14:160]    write_pba 0x1400000c, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 13
[0-2-1970/1/1|0:0:14:160]    write_pba 0x1400000d, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 14
[0-2-1970/1/1|0:0:14:160]    write_pba 0x1400000e, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 15
[0-2-1970/1/1|0:0:14:160]    write_pba 0x1400000f, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 16
[0-2-1970/1/1|0:0:14:160]    write_pba 0x14000010, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 17
[0-2-1970/1/1|0:0:14:160]    write_pba 0x14000011, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 18
[0-2-1970/1/1|0:0:14:160]    write_pba 0x14000012, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 19
[0-2-1970/1/1|0:0:14:160]    write_pba 0x14000013, 0 0 0 0
[0-2-1970/1/1|0:0:14:160]    w2p lba 20
[0-2-1970/1/1|0:0:14:160]    write_pba 0x14000014, 0 0 0 0
[0-2-1970/1/1|0:0:14:161]    w2p lba 21
[0-2-1970/1/1|0:0:14:161]    write_pba 0x14000015, 0 0 0 0
[0-2-1970/1/1|0:0:14:161]    w2p lba 22
[0-2-1970/1/1|0:0:14:161]    write_pba 0x14000016, 0 0 0 0
[0-2-1970/1/1|0:0:14:161]    w2p lba 23
[0-2-1970/1/1|0:0:14:161]    write_pba 0x14000017, 0 0 1 1
[0-2-1970/1/1|0:0:14:161]    Send program command
[0-2-1970/1/1|0:0:14:161]    Send program command
[2-2-1970/1/1|0:0:14:161]    alloc page_prog_bmp die 0 blk 20 slc 0 pool_cnt 192 18143 adr 0xc163e051b


            ret = fcl_write_cmd::send_normal_write_cmd(fct_entry,
                                                GEN_FULL_FCT_IDX(fct_pool_id, fct_idx),
                                                fcq_qid,
                                                stream_id,
                                                (u64)write_buf,
                                                lba,
                                                pba,
                                                bad_plane_mask,
                                                lba_md,
                                                dst_sel,
                                                nlb,
                                                FCL_WRITE_START_LAST_BMP(bstart, pstart, blast, plast),
                                                lp_fmt);

            ret = fcl_program_cmd::send_vqid_program_cmd(fct_entry,
                                    GEN_FULL_FCT_IDX(fct_pool_id, fct_idx),
                                    fcq_qid,
                                    slc_mode,
                                    pba,
                                    bad_plane_mask,
                                    src_addr,
                                    pgd_vqid,
                                    stream_id,
                                    stripe_idx,
                                    pe_cnt,
                                    lp_fmt,
                                    (u8)raid_cmd,
                                    raid_grp_id);
                fcl_read_cmd::send_normal_read_cmd(fct_entry,
                                                   GEN_FULL_FCT_IDX(pool_id, fct_idx),
                                                   fcq_id,
                                                   slc_mode,
                                                   pba,
                                                   bad_plane_mask,
                                                   buf_addr,
                                                   lba,
                                                   lba_md,
                                                   lp_bitmap,
                                                   lp_fmt, 
                                                   raid_cmd,
                                                   rfnd,
                                                   vt,
                                                   pe_cnt)；
fcl_erase_cmd::send_slc_erase_cmd(fct_entry, GEN_FULL_FCT_IDX(fct_pool_id, fct_idx), fcq_qid,
                                            pba, bad_plane_mask)


[0-2-1970/1/1|0:0:16:759]    w2p lba 0, write_src_addr 0x31d9000
[0-2-1970/1/1|0:0:16:759]    bstart[1], pstart[1], blast[0], plast[0]
[0-2-1970/1/1|0:0:16:759]    write_pba[0x1c1000001]
[0-2-1970/1/1|0:0:16:759]    w2p lba 1, write_src_addr 0x31d9000
[0-2-1970/1/1|0:0:16:759]    bstart[0], pstart[0], blast[0], plast[0]
[0-2-1970/1/1|0:
0
:
!!! Core:10 ASSERT!!!
16    --> Assert ID::    759]    922(WRITE_TO_BAD_PLANE)
write_pba[0x1c1000002]    --> Cond:
    0 == BIT_TEST(bad_plane_mask, input_nand_addr.plane)
    --> File:    /home/tcnsh/bert/ql_fw/driver/sim_quince/feace/buffer_manager.cpp:159
    --> Msg:    input bad_plane_mask 0xc4, input_nand_addr 0x382800, plane 2

[0-2-1970/1/1|0:0:16:759]    bstart[0], pstart[0], blast[0], plast[0]
[0-2-1970/1/1|0:0:16:759]    write_pba[0x1c1000003]
[0-2-1970/1/1|0:0:16:759]    w2p lba 3, write_src_addr 0x31db000
[0-2-1970/1/1|0:0:16:759]    bstart[0], pstart[0], blast[0], plast[0]
[0-2-1970/1/1|0:0:16:759]    write_pba[0x1c1000004]
[0-2-1970/1/1|0:0:16:759]    w2p lba 4, write_src_addr 0x31dc000
[0-2-1970/1/1|0:0:16:759]    bstart[0], pstart[0], blast[0], plast[0]
[0-2-1970/1/1|0:0:16:759]    write_pba[0x1c1000005]
[0-2-1970/1/1|0:0:16:759]    w2p lba 5, write_src_addr 0x31dd000
[0-2-1970/1/1|0:0:16:759]    bstart[0], pstart[0], blast[0], plast[0]
[0-2-1970/1/1|0:0:16:759]    write_pba[0x1c1000006]
[0-2-1970/1/1|0:0:16:759]    w2p lba 6, write_src_addr 0x31de000
[0-2-1970/1/1|0:0:16:759]    bstart[0], pstart[0], blast[0], plast[0]
[0-2-1970/1/1|0:0:16:759]    write_pba[0x1c1000007]
[0-2-1970/1/1|0:0:16:759]    w2p lba 7, write_src_addr 0x31df000
[0-2-1970/1/1|0:0:16:759]    bstart[0], pstart[0], blast[0], plast[0]
[0-2-1970/1/1|0:0:16:760]    write_pba[0x1c1000008]
[0-2-1970/1/1|0:0:16:760]    w2p lba 8, write_src_addr 0x31e0000
[0-2-1970/1/1|0:0:16:760]    bstart[0], pstart[0], blast[0], plast[0]




2c72fc7 异常
689d547 异常
0c0ea2d 异常
5dd39db 异常
e97e755
0e87af6 正常
90bbadc 正常
9dcd1e8 正常
77fa32d 正常
e3f1e83 正常

void rcvry_gsd_ctrl::l2p_restore_processor::execute()

ASSERT_TRUE(fw_io_cmd::send_erase_cmd(FCT_POOL_ERASE, FCQ_ERASE, false, write_pba.all, bad_plane_mask));

stream 0 0x000000007b99c5c0
stream 1  
stream 2  
stream 3  
stream 4 0x0000000034c46880
stream 5 0x000000003657ffc0 parity 63
0x0000000075579a40
0x000000003511ac0


scp tcn@192.68.3.40:/share/crossTools/QuinceLite/gcc-arm-11.2-2022.02-x86_64-aarch64-none-elf.tar.xz .
​
tar -xvf  gcc-arm-11.2-2022.02-x86_64-aarch64-none-elf.tar.xz