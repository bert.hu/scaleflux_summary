```c++
#define CE_OF_CE_LUN(ce_lun)            ((ce_lun) % (CFG_CE_NUM))
#define LUN_OF_CE_LUN(ce_lun)           ((ce_lun) / (CFG_CE_NUM))
#define GET_CE_LUN(ce, lun)             (((lun) * CFG_CE_NUM) + (ce))
#define GET_DIE_BY_CH_CE_LUN(ch, ce, lun, pl_gp)  ((((lun * CFG_CE_NUM + ce) << CFG_PBA_CHAN_BITS) | (ch)) + (pl_gp * CFG_PHYSICAL_DIE_NUM))

ce_lun = ((lun) * CFG_CE_NUM) + (ce)
ce_lun / CFG_CE_NUM = lun
ce_lun % CFG_CE_NUM = ce
```

```
lun_width=1: lun_id = die_id[0]; ce_id=die_id[3:1];
lun_width=2: lun_id = die_id[1:0]; ce_id=die_id[4:2];
lun_width=3: lun_id = die_id[2:0]; ce_id=die_id[4:3];

lun_width:一个ce上有多少个lun？

lun_width=1: lun_id = die_id[0]; 0,1, ce_id=die_id[3:1], 0,1,2,3,4,5,6,7;
lun_width=2: lun_id = die_id[1:0] 0,1,2,3; ce_id=die_id[4:2], 0,1,2,3,4,5,6,7;
lun_width=3: lun_id = die_id[2:0] 0,1,2,3,4,5,6,7; ce_id=die_id[4:3], 0,1,2,3;

10111:23，
lun_width=1: [CFG_CE_NUM=8,CFG_LUN_NUM=2]lun=1，ce=3
lun_width=2: [CFG_CE_NUM=8,CFG_LUN_NUM=4]lun=3，ce=5
lun_width=3: [CFG_CE_NUM=4,CFG_LUN_NUM=8]lun=7，ce=2
```

```
pba.offset = nand_addr.la >> 1;
pba.ccp_off = ccp_off;
nand_addr.la = (pba.offset << 1) + pba.ccp_off / 4;
(ccp_off >> 2) != (nand_addr.la & 1)

以前
nand_addr.la        0 1 2 3 4 5 6 7
nand_addr.la >> 1   0 1 0 1 0 1 0 1 【pba.offset】

pba.offset                                0                 1                2                3
pba.ccp_off                               0 1 2 3 4 5 6 7   0 1 2 3 4 5 6 7  0 1 2 3 4 5 6 7  0 1 2 3 4 5 6 7
(pba.offset << 1) + pba.ccp_off / 4       0 0 0 0 1 1 1 1   2 2 2 2 3 3 3 3  4 4 4 4 5 5 5 5  6 6 6 6 7 7 7 7【nand_addr.la】

CFG_FOURK_PER_PLANE
```

```c++
#define DIE_ID_LOW_BITS         (4)
#define DIE_ID_HIGH_BITS        (1)
#define GET_DIE_ID_HIGH(die_id) (((die_id) >> DIE_ID_LOW_BITS) & 0x1)
#define GET_DIE_ID_LOW(die_id) ((die_id) & N_BIT_MASK(DIE_ID_LOW_BITS))
#define GET_DIE_ID(die_id_low, die_id_high) ((die_id_high << DIE_ID_LOW_BITS) | (die_id_low))
#define CE_OF_DIE_ID(die_id)            ((die_id) / (CFG_LUN_NUM))
#define LUN_OF_DIE_ID(die_id)           ((die_id) % (CFG_LUN_NUM))
#define GET_DIE_ID_BY_CE_LUN(ce, lun)   (((ce) * CFG_LUN_NUM) + (lun))

#define CE_OF_CE_LUN(ce_lun)            ((ce_lun) % (CFG_CE_NUM))
#define LUN_OF_CE_LUN(ce_lun)           ((ce_lun) / (CFG_CE_NUM))
#define GET_CE_LUN(ce, lun)             (((lun) * CFG_CE_NUM) + (ce))
```

```
u32 plane_shift_size  = ((nand_addr.la * CFG_EP_UNIT_SIZE) + (ccp_offset * CFG_QCCP_SEC_SZ) + ((nxs + 1) * CFG_EP_UNIT_SIZE));

la             0  1  2  3  4  5  6  7
ccp_offset     0  1  0  1  0  1  0  1【距离2K的边界有多少个CFG_QCCP_SEC_SZ的大小】
nxs            0  1  2  3  4  5  6  7

_nand_addr->la          = (_pba.offset << 1) + (_pba.ccp_off > 3 ? 1 : 0);
```

```
python3 Samanea_Scripts/DataPath/DP_IOWith4K.py --interface 1 --enableIOLogger 1
```

