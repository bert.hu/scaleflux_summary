每个FCQ里面有4个FCT POOL，因此pool id为0,1,2,3共4个

```
#define FCT_ENTRY_POOL_ID_BITS (2)
#define FCT_ENTRY_POOL_ID_MASK (0x3UL)
```

因为FCQ的最大深度depth是512，因此FCQ中的FCT POOL的entry size是512，最大的index in pool就是512

```
#define FCT_ENTRY_IDX_BITS (10)
#define FCT_ENTRY_IDX_MASK (0x1FFUL) // fcq depth is 512
```

那么如何获取每个FCT Entry在哪个FCT POOL中，以及在这个FCT POOL的那个index呢？

对于每个FCT Entry，我们得知道它属于那个FCQ，即需要知道它的fcq qid，需要通过fcq qid + pool id来找到它在哪个FCT POOL中

index in pool就是FCT idx

把pool id + fct idx组合成12bit（2 + 10）的数字，就得到FULL FCT IDX，里面包含了pool id和fct idx

```
#define GEN_FULL_FCT_IDX(pool_id, fct_idx) \
    (((u16)(pool_id) << FCT_ENTRY_IDX_BITS) | (u16)((fct_idx)&FCT_ENTRY_IDX_MASK))
#define GET_POOL_ID(qid, full_fct_idx) (((qid) << FCT_ENTRY_POOL_ID_BITS) | (((u16)(full_fct_idx) >> FCT_ENTRY_IDX_BITS) & FCT_ENTRY_POOL_ID_MASK))
#define GET_FCT_IDX(full_fct_idx) ((full_fct_idx)&FCT_ENTRY_IDX_MASK)
```

那么我们来看下，我如何通过一个FW定义的FCT POOL去找到和生成FULL FCT IDX的呢？

以FCQ_IO_CKP_RD_C0  = 17 为例

首先获取FCQ_IO_CKP_RD_C0 的第一个FCT POOL ID的值

用 (17 << 2 | 0 ) = 68, 从而获取到FCT_POOL_PAGEIN_C0 = FCQ_IO_CKP_RD_C0  * FCT_POOL_MAX_NUM + 0,这个FCT POOL

