Date: Fri, 13 Oct 2023 06:29:52 +0000 (UTC)
Message-ID: <988910734.7.1697178592204@c1f88d8d0884>
Subject: Exported From Confluence
MIME-Version: 1.0
Content-Type: multipart/related; 
	boundary="----=_Part_6_1541052063.1697178592204"

------=_Part_6_1541052063.1697178592204
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: quoted-printable
Content-Location: file:///C:/exported.html

<html xmlns:o=3D'urn:schemas-microsoft-com:office:office'
      xmlns:w=3D'urn:schemas-microsoft-com:office:word'
      xmlns:v=3D'urn:schemas-microsoft-com:vml'
      xmlns=3D'urn:w3-org-ns:HTML'>
<head>
    <meta http-equiv=3D"Content-Type" content=3D"text/html; charset=3Dutf-8=
">
    <title>Read path</title>
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:TargetScreenSize>1024x640</o:TargetScreenSize>
            <o:PixelsPerInch>72</o:PixelsPerInch>
            <o:AllowPNG/>
        </o:OfficeDocumentSettings>
        <w:WordDocument>
            <w:View>Print</w:View>
            <w:Zoom>90</w:Zoom>
            <w:DoNotOptimizeForBrowser/>
        </w:WordDocument>
    </xml>
    <![endif]-->
    <style>
                <!--
        @page Section1 {
            size: 8.5in 11.0in;
            margin: 1.0in;
            mso-header-margin: .5in;
            mso-footer-margin: .5in;
            mso-paper-source: 0;
        }

        table {
            border: solid 1px;
            border-collapse: collapse;
        }

        table td, table th {
            border: solid 1px;
            padding: 5px;
        }

        td {
            page-break-inside: avoid;
        }

        tr {
            page-break-after: avoid;
        }

        div.Section1 {
            page: Section1;
        }

        /* Confluence print stylesheet. Common to all themes for print medi=
a */
/* Full of !important until we improve batching for print CSS */

@media print {
    #main {
        padding-bottom: 1em !important; /* The default padding of 6em is to=
o much for printouts */
    }

    body {
        font-family: Arial, Helvetica, FreeSans, sans-serif;
        font-size: 10pt;
        line-height: 1.2;
    }

    body, #full-height-container, #main, #page, #content, .has-personal-sid=
ebar #content {
        background: var(--ds-surface, #fff) !important;
        color: var(--ds-text, #000) !important;
        border: 0 !important;
        width: 100% !important;
        height: auto !important;
        min-height: auto !important;
        margin: 0 !important;
        padding: 0 !important;
        display: block !important;
    }

    a, a:link, a:visited, a:focus, a:hover, a:active {
        color: var(--ds-text, #000);
    }

    #content h1,
    #content h2,
    #content h3,
    #content h4,
    #content h5,
    #content h6 {
        font-family: Arial, Helvetica, FreeSans, sans-serif;
        page-break-after: avoid;
    }

    pre {
        font-family: Monaco, "Courier New", monospace;
    }

    #header,
    .aui-header-inner,
    #navigation,
    #sidebar,
    .sidebar,
    #personal-info-sidebar,
    .ia-fixed-sidebar,
    .page-actions,
    .navmenu,
    .ajs-menu-bar,
    .noprint,
    .inline-control-link,
    .inline-control-link a,
    a.show-labels-editor,
    .global-comment-actions,
    .comment-actions,
    .quick-comment-container,
    #addcomment {
        display: none !important;
    }

    /* CONF-28544 cannot print multiple pages in IE */
    #splitter-content {
        position: relative !important;
    }

    .comment .date::before {
        content: none !important; /* remove middot for print view */
    }

    h1.pagetitle img {
        height: auto;
        width: auto;
    }

    .print-only {
        display: block;
    }

    #footer {
        position: relative !important; /* CONF-17506 Place the footer at en=
d of the content */
        margin: 0;
        padding: 0;
        background: none;
        clear: both;
    }

    #poweredby {
        border-top: none;
        background: none;
    }

    #poweredby li.print-only {
        display: list-item;
        font-style: italic;
    }

    #poweredby li.noprint {
        display: none;
    }

    /* no width controls in print */
    .wiki-content .table-wrap,
    .wiki-content p,
    .panel .codeContent,
    .panel .codeContent pre,
    .image-wrap {
        overflow: visible !important;
    }

    /* TODO - should this work? */
    #children-section,
    #comments-section .comment,
    #comments-section .comment .comment-body,
    #comments-section .comment .comment-content,
    #comments-section .comment p {
        page-break-inside: avoid;
    }

    #page-children a {
        text-decoration: none;
    }

    /**
     hide twixies

     the specificity here is a hack because print styles
     are getting loaded before the base styles. */
    #comments-section.pageSection .section-header,
    #comments-section.pageSection .section-title,
    #children-section.pageSection .section-header,
    #children-section.pageSection .section-title,
    .children-show-hide {
        padding-left: 0;
        margin-left: 0;
    }

    .children-show-hide.icon {
        display: none;
    }

    /* personal sidebar */
    .has-personal-sidebar #content {
        margin-right: 0px;
    }

    .has-personal-sidebar #content .pageSection {
        margin-right: 0px;
    }

    .no-print, .no-print * {
        display: none !important;
    }
}
-->
    </style>
</head>
<body>
    <h1>Read path</h1>
    <div class=3D"Section1">
        <h3 id=3D"Readpath-Overview">Overview</h3>
<p>Read path in this document describes how FTL handle host read commands i=
n normal state and in abnormal state so that the pre-defined performance sp=
ec could be reached.</p>
<p>In Quince Lite, two dedicated R-82 CPU cores (Core0 + Core1) will be use=
d in a pipelined manner to deal with host IO requests. Read/write steps wil=
l be distributed into these two cores as even as possible to balance the to=
tal workload. </p>
<h3 id=3D"Readpath-Commonprocedure">Common procedure</h3>
<p>Common detailed steps for read path includes</p>
<ol start=3D"1">
<li><p>Fetch ACT/SCT command</p></li>
<li><p>Parse command</p></li>
<li><p>Fill and submit L2P lookup commands</p></li>
<li><p>L2P lookup completion polling</p></li>
<li><p>Fetch corresponding PBA and split command</p></li>
<li><p>CPU prefetch vt</p></li>
<li><p>Fill and submit corresponding ACT/SCT command </p></li>
<li><p>ACT completion polling/status check and release</p></li>
<li><p>Read cound statistics</p></li>
</ol>
<p>In current stage, we tend to distribute the above steps to two IO cores =
in the following way</p>
<p>Core 0: step 1, 2, 3, 8, 9</p>
<p>Core 1: step 4, 5, 6, 7</p>
<p></p>
<p>Given below is more information about each step.</p>
<ol start=3D"1">
<li><p>Fetch ACT/SCT command</p></li>
</ol>
<p style=3D"margin-left: 30.0px;">In Quince Lite, host read/write commands =
will be dispatched into separate HW queues, which helps to reduce the sched=
ule overhead in FW. FW will fetch commands one by one from the read queue a=
nd write queue with a round-robin fashion. In 4K read scenario, HW will acc=
umulate at most four 4K read requests into one single SCT command to reduce=
 the overall command processing overhead.</p>
<p style=3D"margin-left: 30.0px;">SCT table will be located in TCM of Core =
1, with each SCT entry 64B. The total number of SCT entries is 128, so the =
total TCM cost of SCT table is 64B * 128 =3D 8KB (<strong><span data-colori=
d=3D"ltohs27x3o">need to confirm</span></strong>).</p>
<p style=3D"margin-left: 30.0px;">Given multiple aligned 4K read, SCT will =
be used for efficiency; otherwise ACT will be used instead. </p>
<ol start=3D"2">
<li><p>Parse command</p></li>
</ol>
<p style=3D"margin-left: 30.0px;">After fetching a read command, FW will pa=
rse this command to get LBA information. For a SCT command, at most 4 align=
ed 4KB read commands need to be parsed. For a ACT command, it may contain u=
naligned reads or &gt;4KB reads.</p>
<ol start=3D"3">
<li><p> Fill and submit L2P lookup commands</p></li>
</ol>
<p style=3D"margin-left: 30.0px;">L2P lookup operation could be processed b=
y HW engine in Quince Lite, but FW needs to prepare the LBA list first. Eac=
h lookup command corresponds to a FCT entry. For a SCT command with multipl=
e 4K aligned read commands, we could directly borrow the multiple LBAs in t=
he command, i.e., we do not need to allocate buffers for LBAs any more; for=
 a ACT command, we need to allocate the LBA buffer corresponding to the LBA=
 length in the command and fill the buffer with consecutive LBAs accordingl=
y. PBA buffer needs to be allocated in any case which is used to buffer the=
 PBAs obtained by HW engine with the same order as LBAs.</p>
<ol start=3D"4">
<li><p> L2P lookup completion polling</p></li>
</ol>
<p style=3D"margin-left: 30.0px;">After submitting L2P lookup commands in C=
ore 0, FW will poll L2P lookup completion messages continuously on Core 1 t=
o get the results promptly.</p>
<ol start=3D"5">
<li><p>Fetch corresponding PBA and split command</p></li>
</ol>
<p style=3D"margin-left: 30.0px;">PBA information will be ready in the prep=
ared PBA buffer after the corresponding L2P lookup completion message recei=
ved. These PBAs will then be used to construct read bitmask and/or split re=
ad command. For a 4K read, no bitmap or command split is required; for read=
 with larger size, FW will set read bitmask according to returned PBAs if r=
ead is inside a physical page, and/or split command when read across die pa=
ges. When it is necessary to split command, FW will also apply for FCT entr=
ies to submit those split commands. (<strong><span data-colorid=3D"vgu8g818=
04">Need to confirm whether ACT can be reused if command does not need to b=
e split</span></strong>) </p>
<p style=3D"margin-left: 30.0px;">FCT pool is also located in Core 1 TCM to=
 guaranteen fast 1-CPU-cycle access. FCT pool contains 512 entries, each wi=
th 64B. Therefore, the total TCM cost is 64B * 512 =3D 32KB.</p>
<ol start=3D"6">
<li><p>CPU prefetch vt</p></li>
</ol>
<p style=3D"margin-left: 30.0px;">With obtained PBAs, FW needs to further g=
et the optimal vt(s) for the corresponding read command. This needs another=
 access of DDR. To reduce the potential overhead, FW uses CPU prefetch inst=
ead of HW engine to hide the DDR access latence as much as possible.</p>
<ol start=3D"7">
<li><p>Fill and submit corresponding ACT/SCT/FCT command</p></li>
</ol>
<p style=3D"margin-left: 30.0px;">With PBAs and vt, FW could fill correspon=
ding ACT/SCT/FCT command fields and then submit them.</p>
<ol start=3D"8">
<li><p>ACT/FCT completion polling/status check and release</p></li>
</ol>
<p style=3D"margin-left: 30.0px;">FW polls ACT/FCT completions on Core 0. G=
iven a SCT containing multiple 4K aligned read commands, the same number of=
 ACT completions will be received eventually, i.e., each 4K read correspond=
s to one ACT completion.</p>
<p style=3D"margin-left: 30.0px;">Another work after receiving a completion=
 is to check the error status. If no error happens, FW releases ACT/FCT ent=
ries directly; if read error happens, FW needs a read retry or read rebuild=
 to continue the read process (detailed process can be found in following p=
arts).</p>
<ol start=3D"9">
<li><p>Read cound statistics</p></li>
</ol>
<p style=3D"margin-left: 30.0px;">The last step is to update the read cound=
 of each physical die block for read disturb purpose. Similar CPU prefetch =
optimization is used as in vt access scenario to reduce the overhead. Anoth=
er optimization for this step is to use a random number to indicate whether=
 a read access needs to be added for its corresponding die block. For examp=
le, if number is randomized between 0 and 16, and 0 is the indicator for re=
ad count increasement, then we will add 16 to the corresponding die block o=
nce we meet random number 0. This helps to further reduce the DDR access ov=
erhead by decreasing the update frequency. </p>
<p style=3D"margin-left: 30.0px;">It is also possible to perform this step =
on Core 1 by CPU prefetch after PBA is obtained. This could be evaluated an=
d compared with above method on Core 0 during implementation. The target is=
 to get more balanced workloads on two IO cores.</p>
<p></p>
<p><strong>Read retry</strong></p>
<p>When a read error happens and read retry is required, FW will reuse the =
corresponding ACT/FCT entry and resend the command on Core 1 to keep the en=
tire read submission process uniform. Since the error status check is perfo=
rmed on Core 0, it is necessary to have a message channel between two IO co=
res. Here we tend to use a small buffer (&lt;=3D4KB is enough) in Core 1 TC=
M, e.g., a command entry list, as the communication channel. When Core 0 en=
counters a read error and needs read retry, it will update the retry buffer=
 list related content with the same ACT/FCT entry information. Core 1 will =
check this list before submiting command to ensure read retry can be proces=
sed in time.</p>
<p></p>
<style>[data-colorid=3D"vgu8g81804"]{color:#ff5630} html[data-color-mode=3D=
dark] [data-colorid=3D"vgu8g81804"]{color:#cf2600}[data-colorid=3D"ltohs27x=
3o"]{color:#ff5630} html[data-color-mode=3Ddark] [data-colorid=3D"ltohs27x3=
o"]{color:#cf2600}</style>
    </div>
</body>
</html>
------=_Part_6_1541052063.1697178592204--
