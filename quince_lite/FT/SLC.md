```c++
/*
 * ---------------------------------------------------------------------------
 *
 * Portions Copyright (c) 2015-2020, ScaleFlux, Inc.
 *
 * ALL RIGHTS RESERVED. These coded instructions and program statements are
 * copyrighted works and confidential proprietary information of Scaleflux Corp.
 * They may not be modified, copied, reproduced, distributed, or disclosed to
 * third parties in any manner, medium, or form, in whole or in part.
 *
 * ---------------------------------------------------------------------------
 */

/*
 * Test slc support
 */

#if 0//__INCLUDE_UNIT_TEST__ == 1
#include "fw_common_def.h"
#include "fct.h"
#include "fcq.h"
#include "fw_fct_manager.h"
#include "fw_fcq_manager.h"
#include "fw_be_common.h"
#include "raid_rebuild.h"
#include "fpga_test_common.h"
#include "b2n_manager.h"
#include "fw_fct_def.h"
#include "fw_fct_manager.h"
#include "b2n_manager.h"
#include "folding_base_type.h"
#include "folding.h"
#include "gc.h"
#include "gsd.h"
#include "footer.h"
#include "l2p.h"


static u8 g_slc_write_buf[4096 * CFG_LUN_PAGE_SIZE * 6] __aligned(4096);
static u8 g_slc_read_buf[4096 * CFG_LUN_PAGE_SIZE * 6] __aligned(4096);
static u8 g_tlc_write_buf[4096 * CFG_ONE_PROG_NUM] __aligned(4096);
static u64 g_tot_pba[8] = {FIRST_TLC_BLK << CFG_PBA_BLOCK_BIT_SHIFT,
                            FIRST_TLC_BLK << CFG_PBA_BLOCK_BIT_SHIFT,
                            FIRST_TLC_BLK << CFG_PBA_BLOCK_BIT_SHIFT,
                            FIRST_TLC_BLK << CFG_PBA_BLOCK_BIT_SHIFT,
                            FIRST_TLC_BLK << CFG_PBA_BLOCK_BIT_SHIFT,
                            FIRST_TLC_BLK << CFG_PBA_BLOCK_BIT_SHIFT,
                            FIRST_TLC_BLK << CFG_PBA_BLOCK_BIT_SHIFT,
                            FIRST_TLC_BLK << CFG_PBA_BLOCK_BIT_SHIFT};

static void send_all_b2n_cmd() // total 8
{
    u8 i;
    COLOR_LOG(COLOR_GREEN, "######## slc_test_2 %s ########\n", __FUNCTION__);


    for (i = 0; i < 8; i++) {
        if (i < 3) {

            u16 fct_idx;
            fct_alloc_entries_idx(FCT_POOL_B2N, &fct_idx, 1);
            FCT_ENTRY_FOR_ACE_B2N_CMD_U *fct_entry = (FCT_ENTRY_FOR_ACE_B2N_CMD_U *)fct_get_entry(FCT_POOL_B2N, fct_idx);
            INF_LOG("current b2n fct index %d addr %p, fct idx: %d, pool_id: %d\n",
                    i, (u64 *)(&fct_idx), fct_idx, FCT_POOL_B2N);
            // set the b2n to slc b2n
            fw_b2n_cmd::send_b2n_cmd(  fct_entry,
                                (FCT_POOL_B2N << FCT_ENTRY_IDX_BITS) + fct_idx,
                                g_b2n_pba,
                                STREAM_ID_L2P,
                                g_b2n_qid_of_stream[STREAM_ID_L2P],
                                true);
            g_tot_pba[i] = g_b2n_pba;
            g_b2n_pba = get_next_b2n_pba(g_b2n_pba);
        }


        if (i >= 3) {
            u16 fct_idx;
            fct_alloc_entries_idx(FCT_POOL_B2N, &fct_idx, 1);
            FCT_ENTRY_FOR_ACE_B2N_CMD_U *fct_entry = (FCT_ENTRY_FOR_ACE_B2N_CMD_U *)fct_get_entry(FCT_POOL_B2N, fct_idx);
            INF_LOG("current b2n fct index %d addr %p, fct idx: %d, pool_id: %d\n",
                    i, (u64 *)(&fct_idx), fct_idx, FCT_POOL_B2N);
            // set the b2n to tlc b2n
            fw_b2n_cmd::send_b2n_cmd(  fct_entry,
                                (FCT_POOL_B2N << FCT_ENTRY_IDX_BITS) + fct_idx,
                                g_b2n_pba,
                                STREAM_ID_HOT,
                                g_b2n_qid_of_stream[STREAM_ID_HOT],
                                false);
            g_tot_pba[i] = g_b2n_pba;
            g_b2n_pba = get_next_b2n_pba(g_b2n_pba);
        }
    }
}



// send 1 b2n cmd
static bool send_single_slc_b2n_cmd()
{
    bool send_succeed = false;

    COLOR_LOG(COLOR_GREEN, "######## slc_test_1 %s ########\n", __FUNCTION__);

    u16 fct_idx;
    fct_alloc_entries_idx(FCT_POOL_B2N, &fct_idx, 1);
    FCT_ENTRY_FOR_ACE_B2N_CMD_U *fct_entry = (FCT_ENTRY_FOR_ACE_B2N_CMD_U *)fct_get_entry(FCT_POOL_B2N, fct_idx);
    INF_LOG("addr %p, fct idx: %d, pool_id: %d\n",
            (u64 *)(&fct_idx), fct_idx, FCT_POOL_B2N);
    // set the b2n to slc b2n
    fct_entry->fields.ccs_cmd_write_ctrl.ctr_b2n.slc = 1;
    fw_b2n_cmd::send_b2n_cmd( fct_entry,
                        (FCT_POOL_B2N << FCT_ENTRY_IDX_BITS) + fct_idx,
                        g_b2n_pba,
                        STREAM_ID_L2P,
                        g_b2n_qid_of_stream[STREAM_ID_L2P]);
    g_tot_pba[0] = g_b2n_pba;
    g_b2n_pba = get_next_b2n_pba(g_b2n_pba);
    send_succeed = true;
    return send_succeed;
}

static void send_erase_cmd()
{
    for (u32 i = 0; i < 8; i++) {
        u16 fct_idx;
        fct_alloc_entries_idx(FCT_POOL_ERASE, &fct_idx, 1);
        FCT_ENTRY_FOR_ACE_ERASE_CMD_U *fct_entry = (FCT_ENTRY_FOR_ACE_ERASE_CMD_U *)fct_get_entry(FCT_POOL_ERASE, fct_idx);
        INF_LOG("%s: fct idx: %d, pba: %llx\n",
                __FUNCTION__, fct_idx, g_tot_pba[i]);
        fpga_send_erase_cmd(fct_entry,
                            (FCT_POOL_ERASE << FCT_ENTRY_IDX_BITS) +
                            fct_idx,
                            g_tot_pba[i],
                            0);
    }
}

static void write_meta_data(u8 *write_buf, u64 start_lba)
{
    u64 buff_addr;
    u64 lba;

    u8 data_pattern = 0x70;

    COLOR_LOG(COLOR_GREEN, "######## slc_test_1 %s ########\n", __FUNCTION__);

    for (u32 i = 0; i < CFG_LUN_PAGE_SIZE; i++) {

        INF_LOG("%s: write data: 0x%x, write buf addr: %p\n",
                __FUNCTION__, data_pattern, write_buf + i * 4096);
        memset(write_buf + i * 4096, data_pattern, 16);
        data_pattern++;
        buff_addr = (u64)(write_buf + i * 4096);
        lba = start_lba + i;
        fpga_submit_w2p_cmd(SCHEME_NORMAL_WRITE, STREAM_ID_L2P, (u64)(buff_addr), lba);
    }
}

static void write_user_data(u32 page_type_num)
{
    u8 i, loop, fourk_idx;
    u64 buff_addr;
    u64 lba;

    COLOR_LOG(COLOR_GREEN, "######## slc_test_2 %s ########\n", __FUNCTION__);

    for (loop = 0; loop < page_type_num; loop++) {
        for (i = 0; i < CFG_LUN_PAGE_SIZE; i++) {

            INF_LOG("loop %d, %d\n", loop, i);
            fourk_idx = loop * CFG_LUN_PAGE_SIZE + i;
            memset((u8 *)g_tlc_write_buf + fourk_idx * 4096, 0x30 + fourk_idx, 4096);
            memset((u8 *)g_tlc_write_buf + fourk_idx * 4096, 0x0, 1);
            buff_addr = (u64)((u8 *)g_tlc_write_buf + fourk_idx * 4096);
            lba = fourk_idx + 1;
            fpga_submit_w2p_cmd(SCHEME_NORMAL_WRITE, STREAM_ID_HOT, (u64)(buff_addr), lba);
        }
    }
}

static void read_meta_data(u64 pba)
{
    u64 lba = 0;
    u8 vt = 0;

    COLOR_LOG(COLOR_GREEN, "######## slc_test %s ########\n", __FUNCTION__);

    memset((u8 *)g_slc_read_buf, 0, (4096 * CFG_LUN_PAGE_SIZE));

    for (u8 i = 0; i < CFG_LUN_PAGE_SIZE; i++) {
        u16 fct_idx;
        fct_alloc_entries_idx(FCT_POOL_W2P, &fct_idx, 1);
        FCT_ENTRY_FOR_FLASH_RD_CMD_U *rd_entry = (FCT_ENTRY_FOR_FLASH_RD_CMD_U *)fct_get_entry(FCT_POOL_FLASH_RD, fct_idx);

        INF_LOG("loop %d addr %p, fct idx: %d\n",
                i, (u64 *)(&fct_idx), fct_idx);
        set_flash_rd_cmd_ctrl(rd_entry);
        // set to slc read
        rd_entry->fields.ccs_cmd_ext_ctrl.extctr_rd.slc = 1;
        fpga_send_flash_rd_cmd( rd_entry,
                                (FCT_POOL_FLASH_RD << FCT_ENTRY_IDX_BITS) + fct_idx,
                                (u64)((u8 *)g_slc_read_buf),
                                1,
                                pba,
                                0xff,
                                &lba,
                                vt,
                                true);
        break;
    }
}

static void check_read_meta_data_cpl(ACE_FLASH_RD_CPL_MSG_U *cpl, void *write_buf_in)
{
    FCT_ENTRY_FOR_FLASH_RD_CMD_U *fct_entry;
    fct_entry = (FCT_ENTRY_FOR_FLASH_RD_CMD_U *)fct_get_entry(FCT_POOL_FLASH_RD, cpl->fields.fct_idx);
    u32 opcode = cpl->fields.opcode;
    u32 cpl_status = fct_entry->fields.cpl_status.cpl_val;
    u32 lba = 0;
    u64 *read_buf = (u64 *)(fct_entry->fields.data_addr_sim);
    u64 *write_buf = nullptr;
    u8 idx = 0;

#if(__QUINCE__  == 1)
    u64 cs_bitmap = fct_entry->fields.ccs_cmd_cs_bitmap[0];
    u64 cs_bitmap_h = fct_entry->fields.ccs_cmd_cs_bitmap[1];
    u32 once = 0;
#else
    u64 cs_bitmap = fct_entry->fields.ccs_cmd_cs_bitmap[0];
#endif

    if (fct_flash_rd_cmd::get_cpl_status_overall_status(fct_entry) ||
        fct_flash_rd_cmd::get_cpl_status_crc(fct_entry) ||
        fct_flash_rd_cmd::get_cpl_status_erase_page(fct_entry)) {
        debug_flash_rd_cpl_status(fct_entry);
    }

    while (cs_bitmap) {
        if (cs_bitmap & 0x1) {
            lba = fct_entry->fields.lba[idx];
            write_buf = (u64 *)write_buf_in + ((idx * 4096) / CFG_LUN_PAGE_SIZE);
            INF_LOG("%s: idx: %d, lba 0x%x, "
                    "data 0x%llx, should be 0x%llx, "
                    "cpl opcode: %x, cpl status: 0x%x, read_buf: %p\n",
                    __FUNCTION__, idx, lba,
                    *read_buf, *write_buf,
                    opcode, cpl_status, read_buf);
            ASSERT_TRUE((*(read_buf)) == (*write_buf));
            read_buf += 4096/8;
        }

        cs_bitmap >>= 1;
        idx++;
        
#if(__QUINCE__  == 1)
        if (once == 0 && cs_bitmap == 0)
        {
            once = 1;
            cs_bitmap = cs_bitmap_h;
            idx = 64;
        }
#endif 
    }
}


static void test_1() // just about SLC write and read
{
    COLOR_LOG(COLOR_GREEN, "######## slc_test_1 %s ########\n", __FUNCTION__);
    // write
    ASSERT_EQ(true, send_single_slc_b2n_cmd());
    write_meta_data(g_slc_write_buf, 1);
    fpga_wait_w2p_cq_msg(CFG_LUN_PAGE_SIZE,
                         STREAM_ID_L2P); // STREAM_ID_L2P
    fpga_wait_b2n_cq_msg(1);

    // read
    read_meta_data(0);
    fpga_wait_flash_rd_cq_msg(1, g_slc_write_buf, check_read_meta_data_cpl);
    COLOR_LOG(COLOR_GREEN, "Test finished.\n");
}

static void test_2() // SLC full write, erase and write again to test slc poll
{
    COLOR_LOG(COLOR_GREEN, "######## slc_test_2 %s ########\n", __FUNCTION__);
    // erase
    send_erase_cmd();
    fpga_wait_erase_cq_msg(8);

    // write
    send_all_b2n_cmd(); // total 8 SLC 3 TLC 5

    for (u32 i = 0; i < 3; i++) {
        write_meta_data(g_slc_write_buf, 1);
        fpga_wait_w2p_cq_msg(CFG_LUN_PAGE_SIZE,
                             STREAM_ID_L2P);
        fpga_wait_b2n_cq_msg(1, NULL, g_b2n_qid_of_stream[STREAM_ID_L2P]);
    }

    for (u32 i = 0; i < 5; i++) {
        u64 b2n_pba = g_tot_pba[i + 3];
        g_be_mgr.set_block_stream(GET_BLOCK_FROM_PBA(b2n_pba), STREAM_ID_HOT);
        u32 page_type_num  = g_be_mgr.get_page_type_num(GET_BLOCK_FROM_PBA(b2n_pba), GET_PAGE_FROM_PBA(b2n_pba));
        u32 w2p_num_of_b2n = page_type_num * CFG_LUN_PAGE_SIZE;
        write_user_data(page_type_num);
        fpga_wait_w2p_cq_msg(w2p_num_of_b2n, STREAM_ID_HOT);
        fpga_wait_b2n_cq_msg(1, NULL, g_b2n_qid_of_stream[STREAM_ID_HOT]);
    }

    // erase
    send_erase_cmd();
    fpga_wait_erase_cq_msg(8);
    g_b2n_pba = (FIRST_TLC_BLK << CFG_PBA_BLOCK_BIT_SHIFT);

    // write again
    send_all_b2n_cmd(); // total 8 SLC 3 TLC 5

    for (u32 i = 0; i < 3; i++) {
        write_meta_data(g_slc_write_buf, 1);
        fpga_wait_w2p_cq_msg(CFG_LUN_PAGE_SIZE,
                             STREAM_ID_L2P);
        fpga_wait_b2n_cq_msg(1, NULL, g_b2n_qid_of_stream[STREAM_ID_L2P]);
    }

    for (u32 i = 0; i < 5; i++) {
        u64 b2n_pba = g_tot_pba[i + 3];
        g_be_mgr.set_block_stream(GET_BLOCK_FROM_PBA(b2n_pba), STREAM_ID_HOT);
        u32 page_type_num  = g_be_mgr.get_page_type_num(GET_BLOCK_FROM_PBA(b2n_pba), GET_PAGE_FROM_PBA(b2n_pba));
        u32 w2p_num_of_b2n = page_type_num * CFG_LUN_PAGE_SIZE;
        write_user_data(w2p_num_of_b2n);
        fpga_wait_w2p_cq_msg(w2p_num_of_b2n, STREAM_ID_HOT);
        fpga_wait_b2n_cq_msg(1, NULL, g_b2n_qid_of_stream[STREAM_ID_HOT]);
    }

    COLOR_LOG(COLOR_GREEN, "Test finished.\n");
}

static void test_all()
{

    //    test_1();
    //test_2();
}

void fpga_test_slc(int test_case)
{
    switch (test_case) {
        case -1:
            // execute all test case
            test_all();
            break;

        case 1:
            //B2N command test
            test_1();
            break;

        default:
            break;
    }

    return;
}

FPGA_TEST(FPGA, slc_test, BIT(0))
{
    fpga_test_slc(-1);
}

#endif


```

```c++
/*
 * ---------------------------------------------------------------------------
 *
 * Portions Copyright (c) 2015-2020, ScaleFlux, Inc.
 *
 * ALL RIGHTS RESERVED. These coded instructions and program statements are
 * copyrighted works and confidential proprietary information of Scaleflux Corp.
 * They may not be modified, copied, reproduced, distributed, or disclosed to
 * third parties in any manner, medium, or form, in whole or in part.
 *
 * ---------------------------------------------------------------------------
 */

/*
 * Test slc support
 */

#if 0//__INCLUDE_UNIT_TEST__ == 1
#include "fw_common_def.h"
#include "fct.h"
#include "fcq.h"
#include "fw_fct_manager.h"
#include "fw_fcq_manager.h"
#include "fw_be_common.h"
#include "raid_rebuild.h"
#include "fpga_test_common.h"
#include "b2n_manager.h"
#include "fw_fct_def.h"
#include "fw_fct_manager.h"
#include "rfs_msg.h"
#include "fw_common_def.h"

const static u64 g_rfs_lba = (LP_SET_USER_TYPE(0, USER_RFS));
static u64 g_test_blk_pba = (FIRST_TLC_BLK << CFG_PBA_BLOCK_BIT_SHIFT);

static bool send_slc_b2n(u64 pba)
{
    COLOR_LOG(COLOR_GREEN, "######## %s ########\n", __FUNCTION__);

    u16 fct_idx;
    fct_alloc_entries_idx(FCT_POOL_B2N, &fct_idx, 1);
    void *fct_entry = fct_get_entry(FCT_POOL_B2N, fct_idx);
    INF_LOG("fct addr %p, fct idx: %d, pool_id: %d, qid: %d, pba: 0x%llx\n",
            (u64 *)(&fct_idx),
            fct_idx,
            FCT_POOL_B2N,
            FCQ_B2N_RFS,
            pba);

    return fw_b2n_cmd::send_b2n_cmd( fct_entry,
                            (FCT_POOL_B2N << FCT_ENTRY_IDX_BITS) | fct_idx,
                            pba,
                            STREAM_ID_RFS,
                            FCQ_B2N_RFS,
                            true);
}

static void w2p_cpl_cb(u8 qid __unused, u32 fct_idx)
{
    // The write cmd is now completed - we are in the write_cpl callback func
    u16 idx = GET_FCT_IDX(fct_idx);
    fct_release_entries_by_idx(FCT_POOL_W2P, &idx, 1);
}

static void b2n_cpl_cb(u8 qid __unused, u32 fct_idx)
{
    // The b2n (for write) cmd is now completed - we are in the write_cpl callback func
    u16 idx = GET_FCT_IDX(fct_idx);
    fct_release_entries_by_idx(FCT_POOL_B2N, &idx, 1);
}

static bool send_slc_flash_rd(u64 pba, u8* read_buf, u8 cnt)
{
    static u64 lba[8] = {   g_rfs_lba, g_rfs_lba, g_rfs_lba, g_rfs_lba,
                            g_rfs_lba, g_rfs_lba, g_rfs_lba, g_rfs_lba};
    u64 cs_bitmap = N_BIT_MASK(cnt);
    u8 bad_plane_mask = DEFAULT_BAD_PLANE_MASK;
    u8 nxs = cal_nxs(SCHEME_NORMAL_READ, cs_bitmap, bad_plane_mask);
    u16 fct_idx;
    void *fct_entry;

    fct_alloc_entries_idx(FCT_POOL_FLASH_RD, &fct_idx, 1);
    fct_entry = fct_get_entry(FCT_POOL_FLASH_RD, fct_idx);

    return fw_flash_read_cmd::read_slc_page_by_pba_to_buf(fct_entry, (FCT_POOL_FLASH_RD << FCT_ENTRY_IDX_BITS) + fct_idx,
                                                          FCQ_IO_RFS_READ,
                                                          (u64)read_buf, pba, cs_bitmap, lba,
                                                          SCHEME_NORMAL_READ, nxs, bad_plane_mask);
}

static void slc_read_cpl_cb(u32 fct_idx)
{
    // The read cmd is now completed - we are in the read_cpl callback func
    u16 idx = GET_FCT_IDX(fct_idx);
    void *fct_entry = fct_get_entry(FCT_POOL_FLASH_RD, idx);
    if (fct_flash_rd_cmd::get_cpl_status_overall_status(fct_entry) ||
        fct_flash_rd_cmd::get_cpl_status_crc(fct_entry) ||
        fct_flash_rd_cmd::get_cpl_status_erase_page(fct_entry)) {
        debug_flash_rd_cpl_status(fct_entry);
    }

    // retrieve the read cmd status from BE, for nand_read op to check
//    u32 cpl_val = fct_flash_rd_cmd::get_cpl_status_val(fct_entry);

//    print_mem(fct_entry, 0x400, "fct entry\n");

    fct_release_entries_by_idx(FCT_POOL_FLASH_RD, &idx, 1);
}

FPGA_TEST(SLCT, sp, BIT(0))
{
    const u8 total_b2n = 3;
    u64 b2n[total_b2n];
    u64 buffer_offset = 0;

    COLOR_LOG(COLOR_GREEN, "######## SLC - r/w test - single plane ########\n");

    memset(g_write_buf, 0xcd, SIZE_OF_ONE_PROG_BUF);
    memset(g_read_buf, 0, SIZE_OF_ONE_PROG_BUF);

    COLOR_LOG(COLOR_GREEN, "write data\n");
    for (u8 i = 0; i < total_b2n; i++) {
        b2n[i] = g_test_blk_pba;
        g_test_blk_pba = get_next_b2n_pba(g_test_blk_pba);
        ASSERT_EQ(true, send_slc_b2n(b2n[i]));
        for (u8 j = 0; j < CFG_LUN_PAGE_SIZE; j ++) {
            buffer_offset = (((i * CFG_LUN_PAGE_SIZE) + j) * 4096);
            ASSERT_EQ(true, rfs_send_write_cmd(&g_write_buf[buffer_offset]));
        }

        ASSERT_EQ(CFG_LUN_PAGE_SIZE, be_poll_cpl(FCQ_IO_RFS_WRITE, w2p_cpl_cb, CFG_LUN_PAGE_SIZE, 10000));
        ASSERT_EQ(1, be_poll_cpl(FCQ_B2N_RFS, b2n_cpl_cb, 1, 10000));
    }

    COLOR_LOG(COLOR_GREEN, "read data\n");
    for (u8 i = 0; i < total_b2n; i++) {
        buffer_offset = ((i * CFG_LUN_PAGE_SIZE) * 4096);
        ASSERT_EQ(true, send_slc_flash_rd(b2n[i], &g_read_buf[buffer_offset], 1));
        ASSERT_EQ(1, be_poll_cpl(FCQ_IO_RFS_READ, slc_read_cpl_cb, 1, 10000));

        print_mem(&g_read_buf[buffer_offset], 0x20, "data\n");
    }
}

FPGA_TEST(SLCT, mp, BIT(0))
{
    const u8 total_b2n = 3;
    u64 b2n[total_b2n];
    u64 buffer_offset = 0;

    COLOR_LOG(COLOR_GREEN, "######## SLC - r/w test - single plane ########\n");

    memset(g_write_buf, 0xef, SIZE_OF_ONE_PROG_BUF);
    memset(g_read_buf, 0, SIZE_OF_ONE_PROG_BUF);

    COLOR_LOG(COLOR_GREEN, "write data\n");
    for (u8 i = 0; i < total_b2n; i++) {
        b2n[i] = g_test_blk_pba;
        g_test_blk_pba = get_next_b2n_pba(g_test_blk_pba);
        ASSERT_EQ(true, send_slc_b2n(b2n[i]));
        for (u8 j = 0; j < CFG_LUN_PAGE_SIZE; j ++) {
            buffer_offset = (((i * CFG_LUN_PAGE_SIZE) + j) * 4096);
            ASSERT_EQ(true, rfs_send_write_cmd(&g_write_buf[buffer_offset]));
        }

        ASSERT_EQ(CFG_LUN_PAGE_SIZE, be_poll_cpl(FCQ_IO_RFS_WRITE, w2p_cpl_cb, CFG_LUN_PAGE_SIZE, 10000));
        ASSERT_EQ(1, be_poll_cpl(FCQ_B2N_RFS, b2n_cpl_cb, 1, 10000));
    }

    COLOR_LOG(COLOR_GREEN, "read data\n");
    for (u8 i = 0; i < total_b2n; i++) {
        buffer_offset = ((i * CFG_LUN_PAGE_SIZE) * 4096);
        ASSERT_EQ(true, send_slc_flash_rd(b2n[i], &g_read_buf[buffer_offset], CFG_LUN_PAGE_SIZE));
        ASSERT_EQ(1, be_poll_cpl(FCQ_IO_RFS_READ, slc_read_cpl_cb, 1, 10000));

        for (int j = 0; j < CFG_LUN_PAGE_SIZE; j++) {
            print_mem(&g_read_buf[buffer_offset], 0x20, "data\n");
            buffer_offset += 4096;
        }
    }
}

FPGA_TEST(SLCT, vsc, BIT(0))
{
    const u8 total_b2n = 1;
    u64 b2n_pba = (FIRST_TLC_BLK << CFG_PBA_BLOCK_BIT_SHIFT);
    u64 buffer_offset = 0;
    u32 blk = FIRST_TLC_BLK;

    COLOR_LOG(COLOR_GREEN, "######## SLC - r/w test - single plane ########\n");


    COLOR_LOG(COLOR_GREEN, "Prepare for SLC Test, erase blk %d\n", blk);

    ft_erase_sblock(blk, false);
    COLOR_LOG(COLOR_GREEN, "erase cmd sent\n");
    fpga_wait_erase_cq_msg(CFG_CHAN_NUM * CFG_CE_NUM * CFG_LUN_NUM);

    ((pba_t *)(&g_test_blk_pba))->block = blk;
    memset(g_write_buf, 0xcd, SIZE_OF_ONE_PROG_BUF);
    memset(g_read_buf, 0, SIZE_OF_ONE_PROG_BUF);

    COLOR_LOG(COLOR_GREEN, "write data\n");
    for (u8 i = 0; i < total_b2n; i++) {
        ASSERT_EQ(true, send_slc_b2n(b2n_pba));
        for (u8 j = 0; j < CFG_LUN_PAGE_SIZE; j ++) {
            buffer_offset = (((i * CFG_LUN_PAGE_SIZE) + j) * 4096);
            ASSERT_EQ(true, rfs_send_write_cmd(&g_write_buf[buffer_offset]));
        }

        ASSERT_EQ(CFG_LUN_PAGE_SIZE, be_poll_cpl(FCQ_IO_RFS_WRITE, w2p_cpl_cb, CFG_LUN_PAGE_SIZE, 100000));
        ASSERT_EQ(1, be_poll_cpl(FCQ_B2N_RFS, b2n_cpl_cb, 1, 10000));
    }

    COLOR_LOG(COLOR_GREEN, "read data\n");

    ASSERT_EQ(true, send_slc_flash_rd(b2n_pba, &g_read_buf[0], 1));
    ASSERT_EQ(1, be_poll_cpl(FCQ_IO_RFS_READ, slc_read_cpl_cb, 1, 10000));

    print_mem(&g_read_buf[0], 0x20, "data\n");


    ASSERT_EQ(true, send_slc_flash_rd(b2n_pba, &g_read_buf[4096], 1));
    ASSERT_EQ(1, be_poll_cpl(FCQ_IO_RFS_READ, slc_read_cpl_cb, 1, 10000));

    print_mem(&g_read_buf[4096], 0x20, "data\n");
    ft_erase_sblock(blk, false);
    fpga_wait_erase_cq_msg(CFG_DIE_NUM);
}

FPGA_TEST(SLCT, vsc1, BIT(0))
{
    const u8 total_b2n = 1;
    pba_t b2n_pba = {.all = 0};
    u64 buffer_offset = 0;
    u32 blk = 1;

    b2n_pba.block = blk;

    COLOR_LOG(COLOR_GREEN, "######## SLC - r/w test - single plane ########\n");


    COLOR_LOG(COLOR_GREEN, "Prepare for SLC Test, erase blk %d\n", blk);

    ft_erase_sblock(blk, false);
    COLOR_LOG(COLOR_GREEN, "erase cmd sent\n");
    fpga_wait_erase_cq_msg(CFG_CHAN_NUM * CFG_CE_NUM * CFG_LUN_NUM);

    ((pba_t *)(&g_test_blk_pba))->block = blk;
    memset(g_write_buf, 0xcd, SIZE_OF_ONE_PROG_BUF);
    memset(g_read_buf, 0, SIZE_OF_ONE_PROG_BUF);

    COLOR_LOG(COLOR_GREEN, "write data\n");
    for (u8 i = 0; i < total_b2n; i++) {
        ASSERT_EQ(true, send_slc_b2n(b2n_pba.all));
        for (u8 j = 0; j < CFG_LUN_PAGE_SIZE; j ++) {
            buffer_offset = (((i * CFG_LUN_PAGE_SIZE) + j) * 4096);
            ASSERT_EQ(true, rfs_send_write_cmd(&g_write_buf[buffer_offset]));
        }

        ASSERT_EQ(CFG_LUN_PAGE_SIZE, be_poll_cpl(FCQ_IO_RFS_WRITE, w2p_cpl_cb, CFG_LUN_PAGE_SIZE, 100000));
        ASSERT_EQ(1, be_poll_cpl(FCQ_B2N_RFS, b2n_cpl_cb, 1, 10000));
    }

    COLOR_LOG(COLOR_GREEN, "read data\n");

    ASSERT_EQ(true, send_slc_flash_rd(b2n_pba.all, &g_read_buf[0], 1));
    ASSERT_EQ(1, be_poll_cpl(FCQ_IO_RFS_READ, slc_read_cpl_cb, 1, 10000));

    print_mem(&g_read_buf[0], 0x1000, "data\n");
}

FPGA_TEST(SLCT, vsc2, BIT(0))
{
    const u8 total_b2n = 1;
    u64 b2n_pba = (FIRST_TLC_BLK << CFG_PBA_BLOCK_BIT_SHIFT);
    u64 buffer_offset = 0;
    u32 blk = FIRST_TLC_BLK;

    COLOR_LOG(COLOR_GREEN, "######## SLC - r/w test - single plane ########\n");


    COLOR_LOG(COLOR_GREEN, "Prepare for SLC Test, erase blk %d\n", blk);

    ft_erase_sblock(blk, false);
    COLOR_LOG(COLOR_GREEN, "erase cmd sent\n");
    fpga_wait_erase_cq_msg(CFG_CHAN_NUM * CFG_CE_NUM * CFG_LUN_NUM);

    ((pba_t *)(&g_test_blk_pba))->block = blk;
    memset(g_write_buf, 0xcd, SIZE_OF_ONE_PROG_BUF);
    memset(g_read_buf, 0, SIZE_OF_ONE_PROG_BUF);

    COLOR_LOG(COLOR_GREEN, "write data\n");
    for (u8 i = 0; i < total_b2n; i++) {
        ASSERT_EQ(true, send_slc_b2n(b2n_pba));
        for (u8 j = 0; j < CFG_LUN_PAGE_SIZE; j ++) {
            buffer_offset = (((i * CFG_LUN_PAGE_SIZE) + j) * 4096);
            ASSERT_EQ(true, rfs_send_write_cmd(&g_write_buf[buffer_offset]));
        }

        ASSERT_EQ(CFG_LUN_PAGE_SIZE, be_poll_cpl(FCQ_IO_RFS_WRITE, w2p_cpl_cb, CFG_LUN_PAGE_SIZE, 10000));
        ASSERT_EQ(1, be_poll_cpl(FCQ_B2N_RFS, b2n_cpl_cb, 1, 10000));
    }

    COLOR_LOG(COLOR_GREEN, "read data\n");

    ASSERT_EQ(true, send_slc_flash_rd(b2n_pba, &g_read_buf[0], 1));
    ASSERT_EQ(1, be_poll_cpl(FCQ_IO_RFS_READ, slc_read_cpl_cb, 1, 10000));

    print_mem(&g_read_buf[0], 0x20, "data\n");


    ASSERT_EQ(true, send_slc_flash_rd(b2n_pba, &g_read_buf[4096], 1));
    ASSERT_EQ(1, be_poll_cpl(FCQ_IO_RFS_READ, slc_read_cpl_cb, 1, 10000));

    print_mem(&g_read_buf[4096], 0x20, "data\n");

}

FPGA_TEST(SLCT, pre, BIT(0))
{
    u32 blk = FIRST_TLC_BLK;

    COLOR_LOG(COLOR_GREEN, "Prepare for SLC Test, erase blk %d\n", blk);

    ft_erase_sblock(blk, true);
    COLOR_LOG(COLOR_GREEN, "erase cmd sent\n");
    fpga_wait_erase_cq_msg(CFG_DIE_NUM);
    ((pba_t *)(&g_test_blk_pba))->block = blk;
}

#endif

```

