```c++
/*
 * ---------------------------------------------------------------------------
 *
 * Portions Copyright (c) 2015-2020, ScaleFlux, Inc.
 *
 * ALL RIGHTS RESERVED. These coded instructions and program statements are
 * copyrighted works and confidential proprietary information of Scaleflux Corp.
 * They may not be modified, copied, reproduced, distributed, or disclosed to
 * third parties in any manner, medium, or form, in whole or in part.
 *
 * ---------------------------------------------------------------------------
 */

#if __INCLUDE_UNIT_TEST__ == 1

#include "fct.h"
#include "fcq.h"
#include "fw_be_common.h"
#include "raid_rebuild.h"
#include "fpga_test_common.h"
#include "b2n_manager.h"
#include "fw_common_def.h"
#include "fw_fct_def.h"
#include "fw_fct_manager.h"
#include "fw_fcq_manager.h"

#include "nor_fw_hw_interface.h"
#include "nor_config.h"
#include "nor_task.h"
#include "nor_mat.h"
#include "blk_manager.h"
#include "die_rebuild.h"
#include "bad_block_detection.h"
#include "pu_manager.h"
#include "be_mgr.h"

// static void read_command_cpl_cb(u8 qid, u32 fct_idx)
// {

// }

u8 g_write_buf[MAX_SIZE_OF_ONE_PROG_BUF] __aligned(4096);
u8 g_read_buf[MAX_SIZE_OF_ONE_PROG_BUF] __aligned(4096);

static void get_next_pba_t(pba_t &pba)
{
    // INF_LOG("write data, pba 0x%llx, die_id 0x%x, ch %u, ce %u, lun %u, ln %u, block %u, plane %u, page %u, page_type %u, la %u\n",
    //         pba.all, tmp_die_id, pba.ch, tmp_ce, pba.lun, LUN_OF_DIE_ID(tmp_die_id), pba.block, pba.plane, pba.page, pba.page_type, pba.la);
    pba.offset += 1;
    if (CFG_FOURK_PER_PLANE == pba.offset || 0 == pba.offset) {
        pba.offset = 0;
        pba.plane++;
        if (CFG_PLANE_NUM == pba.plane || 0 == pba.plane) {
            pba.plane = 0;
            pba.page_type++;
            if (g_be_mgr.get_page_type_num(0, pba.block, pba.page) == pba.page_type || 0 == pba.page_type) {
                pba.page_type = 0;
                pba.page++;
            }
        }
    }
}

static void get_pba_info(pba_t pba, bool &bstart, bool &pstart, bool &blast, bool &plast)
{
    bstart = false, pstart = false, blast = false, plast = false;

    if (0 == pba.plane && 0 == pba.page_type && 0 == pba.offset) {
        bstart = true;
    }
    if (0 == pba.plane && 0 == pba.offset) {
        pstart = true;
    }
    if ((CFG_PLANE_NUM - 1) == pba.plane && (g_be_mgr.get_page_type_num(0, pba.block, pba.page) - 1) == pba.page_type && (CFG_FOURK_PER_PLANE - 1) == pba.offset) {
        blast = true;
    }
    if ((CFG_PLANE_NUM - 1) == pba.plane && (CFG_FOURK_PER_PLANE - 1) == pba.offset) {
        plast = true;
    }
}

FPGA_TEST(nandrd, basic_test, BIT(0))
{
    // parameters needed by read interface
    FCL_READ_SCHEME_E read_scheme = FCL_READ_SCHEME_NORMAL;
    FCL_READ_SUB_OPC_E sub_opc = FCL_READ_SUB_OPC_READ_TO_DDR;
    bool slc_mode = false;
    u8 bad_plane_mask = DEFAULT_BAD_PLANE_MASK;
    u64 read_buf_addr =(u64)&g_read_buf;
    u64 write_src_addr =  (u64)&g_write_buf;
    u64 program_src_addr = INVALID_64BIT;
    u64 lba = 0;
    u32 page_type_num = 0;
    u32 b2n_w2p_num = 0;
    FCL_READ_LBA_MODE_E lba_md = FCL_READ_LBA_MODE_TURE_LBA_OF_1ST_LP;
    u32 lp_bitmap = 0x1;
    FCL_LP_FMT_DATA_SIZE_E lp_fmt = FCL_LP_FMT_4KB_SECTORSIZE_NO_META;
    FCL_RAID_CMD_E raid_cmd = FCL_RAID_BYPASS;
    // parameters needed by program interface
    FCL_PROGRAM_DATA_SOURCE_E program_src_sel = FCL_PROGRAM_DATA_SOURCE_VQID;
    u32 stream_id = STREAM_ID_HOT;
    u64 pgd_vqid = INVALID_64BIT;
    u8 stripe_idx = INVALID_8BIT;
    u8 raid_grp_id = INVALID_8BIT;
    bool sealed = true;
    u8 pe_cnt = 0;
    // parameters needed by write interface
    FCL_WRITE_SRC_E write_src_sel = FCL_WRITE_SRC_FROM_DDR_SRAM_COMBINE_MODE;
    FCL_WRITE_DESTINATION_E dst_sel = FCL_WRITE_DES_DDR_ONLY;
    u8 nlb = 0;
    bool bstart = false, pstart = false, blast = false, plast = false;

    // parameters needed by this FT
    pba_t write_pba = {.all = 0};
    write_pba.block = 20;
    pba_t read_pba = write_pba;
    pba_t program_pba = write_pba;
    u32 poll_cnt = 0;
    u32 err_cnt = 0;
    u16 vqid[MAX_CFG_PAGE_TYPE_NUM] = {0};
    u16 program_vqid[MAX_CFG_PAGE_TYPE_NUM] = {0};

    // init g_write_buf
    u32 ret = initbuf();
    if (ret != 0) {
        INF_LOG("Init buffer failed!!!\n");
    }

    COLOR_LOG(COLOR_GREEN, "%s : test write_pba.block %llu\n", __FUNCTION__, write_pba.block);
    INF_LOG("%s g_write_buf[%p] g_read_buf[%p], read_buf_addr 0x%llx\n", __FUNCTION__, g_write_buf, g_read_buf, read_buf_addr);
    // 先擦 block
    while(!fpga_send_erase_cmd(write_pba.block, slc_mode, bad_plane_mask));
    while(!fpga_poll_erase_cmd(1, FCQ_ERASE));

    // 再发写命令，1次写一个4k,写一个 die page 大小
    page_type_num = g_be_mgr.get_page_type_num(slc_mode, write_pba.block, write_pba.page);
    b2n_w2p_num = page_type_num * CFG_LUN_PAGE_SIZE;
    
    INF_LOG("Send W2P\n");
    lba = 0;
    for (u32 i = 0; i < page_type_num; i++) {
        for (u32 j = 0; j < CFG_LUN_PAGE_SIZE; j++) {
            // 写命令
            INF_LOG("w2p lba %llu\n", lba);
            write_src_addr = (u64)&g_write_buf + (i * CFG_LUN_PAGE_SIZE + j) * DATA_LEN_4K;
            get_pba_info(write_pba, bstart, pstart, blast, plast);
            while(!fpga_send_normal_write_cmd(write_src_sel, dst_sel, stream_id, lba, write_pba.all, (u8 *)write_src_addr,
                                              bad_plane_mask, nlb, stripe_idx, bstart, pstart, blast, plast, lba_md, lp_fmt));
            // poll
            while(!fpga_poll_write_cmd(vqid[i], FCQ_PADDING));
            get_next_pba_t(write_pba);
            lba++;
        }
        ASSERT(WRONG_PAGE_TYPE, i == write_pba.page_type && 0 == write_pba.offset, "write_pba 0x%llx, page_type %u, offset %u\n", write_pba.all, write_pba.page_type, write_pba.offset);
    }

    // 再发 program 命令，把所有 page type 写下去
    INF_LOG("Send program command\n");
    program_src_addr = 0;
    for (u32 i = 0; i < page_type_num; i++) {
        program_src_addr = program_src_addr | (vqid[i] << (i * 16));
    }
    while(!fpga_send_program_cmd(program_src_sel, slc_mode, stream_id, program_pba.all, program_src_addr, pgd_vqid,
                                 bad_plane_mask, stripe_idx, raid_cmd, lp_fmt, raid_grp_id, sealed, pe_cnt));
    // poll
    while(!fpga_poll_program_cmd(1, program_vqid[0], FCQ_B2N_DATA));

    // 读 die page，先把所有 page type 的读命令都发完
    INF_LOG("Send program command\n");
    lba = 0;
    memset((void *)g_read_buf, 0, SIZE_OF_ONE_PROG_BUF);
    for (u32 i = 0; i < page_type_num; i++) {
        for (u32 j = 0; j < CFG_LUN_PAGE_SIZE; j++) {
            INF_LOG("read lba %llu, lp_bitmap %x\n", lba, lp_bitmap);
            read_buf_addr =(u64)&g_read_buf + (i * CFG_LUN_PAGE_SIZE + j) * DATA_LEN_4K;
            while(!fpga_send_read_command(read_scheme, sub_opc, slc_mode, read_pba.all, bad_plane_mask, read_buf_addr,
                                          lba, lba_md, lp_bitmap, lp_fmt, raid_cmd));

            get_next_pba_t(read_pba);
            lba++;
        }
    }

    // poll 读命令
    poll_cnt = 0;
    do {
        if(fpga_poll_read_command() != INVALID_32BIT) {
            poll_cnt++;
        }
    } while (poll_cnt < b2n_w2p_num);

    // 做数据校验
    err_cnt = 0;
    for (u32 i = 0; i < b2n_w2p_num; i++) {
#if __WITH_SIMULATOR__ == 1
        // check_data((u8 *)g_read_buf, i*4096, 8, err_cnt);
        for (u32 j = 0; j < 8; j++) {
            if (g_write_buf[i * DATA_LEN_4K + j * DATA_LEN_512] != g_read_buf[i * DATA_LEN_4K + j * DATA_LEN_512]) {
                err_cnt++;
                INF_LOG("DATA MISCOMPARE, i %u, j %u, write data 0x%x, read data 0x%x, err_cnt %u\n",
                        i, j, g_write_buf[i * DATA_LEN_4K + j * DATA_LEN_512], g_read_buf[i * DATA_LEN_4K + j * DATA_LEN_512], err_cnt);
            }
        }
        
#else
        check_data((u8 *)g_read_buf, i*4096, 4096, err_cnt);
#endif
    }
ASSERT_EQ(err_cnt, 0U);
    INF_LOG("Num of err bytes : %x Test block : %llu\n", err_cnt, write_pba.block);
    COLOR_LOG(COLOR_GREEN, "Test finished.\n");
}
#if 0
FPGA_TEST(nandrd, read_scheme_opc_test, BIT(0))
{
    FCL_READ_SCHEME_E read_scheme = FCL_READ_SCHEME_NORMAL;
    FCL_READ_SUB_OPC_E sub_opc = FCL_READ_SUB_OPC_READ_TO_DDR;
    bool slc_mode = false,
    pba_t pba = {.all = 0};
    u8 bad_plane_mask = DEFAULT_BAD_PLANE_MASK;
    u64 read_buf_addr =(u64)&g_read_buf;
    u64 lba = 0;
    u32 page_type_num = 0;
    u32 b2n_w2p_num = 0;
    FCL_READ_LBA_MODE_E lba_md = FCL_READ_LBA_MODE_TURE_LBA_OF_1ST_LP;
    u32 lp_bitmap = 0xFF'FFFF;
    FCL_LP_FMT_DATA_SIZE_E lp_fmt = FCL_LP_FMT_4KB_SECTORSIZE_NO_META;
    FCL_RAID_CMD_E raid_cmd = FCL_RAID_BYPASS;
 

    pba_t write_pba = {.all = 0};
    write_pba.block = 21;
    pba_t read_pba = write_pba;
    u32 poll_cnt = 0;
    u32 err_cnt = 0;
    u16 vqid[MAX_CFG_PAGE_TYPE_NUM] = {0};
    u16 bufid[MAX_CFG_ONE_PROG_NUM] = {0};

    // init g_write_buf
    u32 ret = initbuf();
    if (ret != 0) {
        INF_LOG("Init buffer failed!!!\n");
    }

    COLOR_LOG(COLOR_GREEN, "%s : test write_pba.block %x\n", __FUNCTION__, write_pba.block);
    INF_LOG("%s g_write_buf[%p] g_read_buf[%p], read_buf_addr 0x%llx\n", __FUNCTION__, g_write_buf, g_read_buf, read_buf_addr);
    // 先擦 block
    while(!fpga_send_erase_cmd(write_pba.block));
    while(!fpga_poll_erase_cmd(1, FCQ_ERASE));

    INF_LOG("Test sub_opc %u\n", sub_opc);
    // 再发写命令，1次写一个 small die page 大小
    page_type_num = g_be_mgr.get_page_type_num(slc, write_pba.block, write_pba.page);
    b2n_w2p_num = page_type_num * CFG_LUN_PAGE_SIZE;
    for (u32 i = 0; i < page_type_num; i++) {
        // 写命令
bool fpga_send_normal_write_cmd(FCL_WRITE_SRC_FROM_DDR_SRAM_COMBINE_MODE, FCL_WRITE_DES_DDR_ONLY, STREAM_ID_HOT, 
                                lba, write_pba.all, u8 *write_buf, u8 bad_plane_mask, u8 nlb, u8 stripe_idx,
                                bool bstart, bool pstart, bool blast, bool plast, FCL_READ_LBA_MODE_E lba_md, u8 lp_fmt)
        // poll

        vqid[i] = 0;// 拿到写命令里的 vqid

        write_pba.page_type++;
    }
    // poll

    // 再发 program 命令，把所有 page type 写下去
    
    // poll

    // 读 die page，先把所有 page type 的读命令都发完
    memset((void *)g_read_buf, 0, SIZE_OF_ONE_PROG_BUF);
    for (u32 i = 0; i < page_type_num; i++) {
        read_buf_addr =(u64)&g_read_buf + i * CFG_LUN_PAGE_SIZE * DATA_LEN_4K;
        fpga_send_read_command(read_scheme,
                               sub_opc,
                               slc_mode,
                               read_pba.all,
                               bad_plane_mask,
                               read_buf_addr,
                               lba,
                               lba_md,
                               lp_bitmap,
                               lp_fmt,
                               raid_cmd);
        read_pba.page_type += 1;
    }

    // poll 读命令
    poll_cnt = 0;
    do {
        if(fpga_poll_read_command() != INVALID_32BIT) {
            poll_cnt++;
        }
    } while (poll_cnt < page_type_num);

    // 做数据校验
    err_cnt = 0;
    for (u32 i = 0; i < b2n_w2p_num; i++) {
#if __WITH_SIMULATOR__ == 1
        check_data((u64 *)read_buf_addr, i*4096, 8, err_cnt);
        ASSERT_EQ(err_cnt, 0U);
#else
        check_data((u64 *)read_buf_addr, i*4096, 4096, err_cnt);
#endif
    }

    INF_LOG("Num of err bytes : %x Test block : %d\n", err_cnt, write_pba.block);

    read_scheme = FCL_READ_SCHEME_GC;
    sub_opc = FCL_READ_SUB_OPC_READ_TO_HW_4K_BUF;
    read_buf_addr = INVALID_64BIT;
    for (u32 i = 0; i < page_type_num; i++) {
        fpga_send_read_command(read_scheme,
                               sub_opc,
                               slc_mode,
                               read_pba.all,
                               bad_plane_mask,
                               read_buf_addr,
                               lba,
                               lba_md,
                               lp_bitmap,
                               lp_fmt,
                               raid_cmd);

        // poll
        // poll 读命令
        do {
            u32 full_fct_idx = fpga_poll_read_command(nullptr, FCQ_FPGA_TEST_RD, false);
            if(INVALID_32BIT != full_fct_idx) {
                // 拿到 fct_entry_for_extra_READ_cpl_status
                pool_id   = GET_POOL_ID(fcq_id, full_fct_idx);
                fct_index = GET_FCT_IDX(full_fct_idx);
                fct_entry = fct_get_entry(pool_id, fct_index);
                // 复制其中的 bufid 到 bufid[] 数组
                // memcpy(&bufid[poll_cnt * CFG_LUN_PAGE_SIZE], &extr[], CFG_LUN_PAGE_SIZE);
                // 释放 entry
                fcq_release_cq_entry(fcq_id);
                fct_release_entries_by_idx(pool_id, &fct_index, 1);
            }
        } while(INVALID_32BIT == full_fct_idx);

        read_pba.page_type += 1;
    }

    write_pba.page++;
    write_pba.page_type = 0;
    write_pba.offset = 0;
    read_pba = write_pba;

    // 发 bufid 的写
    // poll

    // 读出刚写的数据
    
    memset((void *)g_read_buf, 0, SIZE_OF_ONE_PROG_BUF);
    for (u32 i = 0; i < page_type_num; i++) {
        read_buf_addr =(u64)&g_read_buf + i * CFG_LUN_PAGE_SIZE * DATA_LEN_4K;
        fpga_send_read_command(read_scheme,
                               sub_opc,
                               slc_mode,
                               read_pba.all,
                               bad_plane_mask,
                               read_buf_addr,
                               lba,
                               lba_md,
                               lp_bitmap,
                               lp_fmt,
                               raid_cmd);
        read_pba.page_type += 1;
    }

    // poll 读命令
    poll_cnt = 0;
    do {
        if(fpga_poll_read_command() != INVALID_32BIT) {
            poll_cnt++;
        }
    } while (poll_cnt < page_type_num);

    // 做数据校验
    err_cnt = 0;
    for (u32 i = 0; i < b2n_w2p_num; i++) {
#if __WITH_SIMULATOR__ == 1
        check_data((u64 *)read_buf_addr, i*4096, 8, err_cnt);
        ASSERT_EQ(err_cnt, 0U);
#else
        check_data((u64 *)read_buf_addr, i*4096, 4096, err_cnt);
#endif
    }

    INF_LOG("Num of err bytes : %x Test block : %d\n", err_cnt, write_pba.block);
    COLOR_LOG(COLOR_GREEN, "Test finished.\n");
}


FPGA_TEST(nandrd, slc_read_test, BIT(0))
{
    FCL_READ_SCHEME_E read_scheme = FCL_READ_SCHEME_NORMAL;
    FCL_READ_SUB_OPC_E sub_opc = FCL_READ_SUB_OPC_READ_TO_DDR;
    bool slc_mode = true,
    pba_t pba = {.all = 0};
    u8 bad_plane_mask = DEFAULT_BAD_PLANE_MASK;
    u64 read_buf_addr =(u64)&g_read_buf;
    u64 lba = 0;
    u32 page_type_num = 0;
    u32 b2n_w2p_num = 0;
    FCL_READ_LBA_MODE_E lba_md = FCL_READ_LBA_MODE_TURE_LBA_OF_1ST_LP;
    u32 lp_bitmap = 0x1;
    FCL_LP_FMT_DATA_SIZE_E lp_fmt = FCL_LP_FMT_4KB_SECTORSIZE_NO_META;
    FCL_RAID_CMD_E raid_cmd = FCL_RAID_BYPASS;
 

    pba_t write_pba = {.all = 0};
    write_pba.ch = 1;
    write_pba.block = 20;
    pba_t read_pba = write_pba;
    u32 poll_cnt = 0;
    u32 err_cnt = 0;
    u16 vqid[MAX_CFG_PAGE_TYPE_NUM] = {0};

    // init g_write_buf
    u32 ret = initbuf();
    if (ret != 0) {
        INF_LOG("Init buffer failed!!!\n");
    }

    COLOR_LOG(COLOR_GREEN, "%s : test write_pba.block %x\n", __FUNCTION__, write_pba.block);
    INF_LOG("%s g_write_buf[%p] g_read_buf[%p], read_buf_addr 0x%llx\n", __FUNCTION__, g_write_buf, g_read_buf, read_buf_addr);
    // 先擦 block
    while(!fpga_send_erase_cmd(write_pba.block));
    while(!fpga_poll_erase_cmd(1, FCQ_ERASE));


    // 再发写命令，1次写一个 die page 大小
    page_type_num = g_be_mgr.get_page_type_num(slc, write_pba.block, write_pba.page);
    b2n_w2p_num = page_type_num * CFG_LUN_PAGE_SIZE;
    for (u32 i = 0; i < page_type_num; i++) {
        // 写命令

        // poll

        vqid[i] = 0;// 拿到写命令里的 vqid

        write_pba.page_type++;
    }

    // 再发 program 命令，把所有 page type 写下去
    
    // poll

    // 读 die page，先把所有 page type 的读命令都发完
    for (u32 i = 0; i < page_type_num; i++) {
        for (u32 j = 0, j < CFG_LUN_PAGE_SIZE, j++) {
            fpga_send_read_command(read_scheme,
                                   sub_opc,
                                   slc_mode,
                                   read_pba.all,
                                   bad_plane_mask,
                                   read_buf_addr,
                                   lba,
                                   lba_md,
                                   lp_bitmap,
                                   lp_fmt,
                                   raid_cmd);
            read_pba.offset++;
        }
        read_pba.page_type += 1;
        read_pba.offset = 0;
    }

    // poll 读命令
    poll_cnt = 0;
    do {
        if(fpga_poll_read_command() != INVALID_32BIT) {
            poll_cnt++;
        }
    } while (poll_cnt < b2n_w2p_num);

    // 做数据校验
    err_cnt = 0;
    for (u32 i = 0; i < b2n_w2p_num; i++) {
#if __WITH_SIMULATOR__ == 1
        check_data((u64 *)read_buf_addr, i*4096, 8, err_cnt);
        ASSERT_EQ(err_cnt, 0U);
#else
        check_data((u64 *)read_buf_addr, i*4096, 4096, err_cnt);
#endif
    }

    INF_LOG("Num of err bytes : %x Test block : %d\n", err_cnt, write_pba.block);
    COLOR_LOG(COLOR_GREEN, "Test finished.\n");
}

FPGA_TEST(nandrd, pe_cnt_test, BIT(0))
{
    // 只在 fpga 上能测这个参数，以特定 pe cnt program 的数据，只有以该 pe cnt 做读才能读到正确的数据；模拟器上不支持
}

FPGA_TEST(nandrd, bad_plane_mask_test, BIT(0))
{
    
}

FPGA_TEST(nandrd, rfnd_test, BIT(0))
{
    
}
#endif
#endif


```

