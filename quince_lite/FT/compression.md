```c++
/*
 * ---------------------------------------------------------------------------
 *
 * Portions Copyright (c) 2015-2020, ScaleFlux, Inc.
 *
 * ALL RIGHTS RESERVED. These coded instructions and program statements are
 * copyrighted works and confidential proprietary information of Scaleflux Corp.
 * They may not be modified, copied, reproduced, distributed, or disclosed to
 * third parties in any manner, medium, or form, in whole or in part.
 *
 * ---------------------------------------------------------------------------
 */

/*
 * Test compression support
 */
#if 0//__INCLUDE_UNIT_TEST__ == 1

#include "fw_common_def.h"
#include "fct.h"
#include "fw_fct_manager.h"
#include "fcq.h"
#include "fw_fcq_manager.h"
#include "fw_be_common.h"
#include "raid_rebuild.h"
#include "fpga_test_common.h"
#include "b2n_manager.h"
#include "fw_fct_def.h"
#include "fw_fcq_def.h"




static u8 g_compression_pattern __unused = 2048 / 16;

static u64 g_l2p[2 * CFG_ONE_PROG_NUM + 1];    // we use lba 1~96, for easy usage
static u64 g_comp_ft_start_lba = 1;

static bool test_compressible_data(u8 *write_buf, u64 start_lba, u8 compression_pattern, u32 w2p_cnt)
{
    u8 data_pattern = 0x60;

    return fpga_test_write_data(write_buf, start_lba, data_pattern, compression_pattern, 
                                w2p_cnt, STREAM_ID_HOT, FCQ_FPGA_TEST_WR);
}

static void check_w2p_cpl_compressible(ACE_W2P_CPL_MSG_U *cpl)
{
    void *fct_entry = nullptr;
    u32 full_fct_idx = fcq_ace_cpl_msg::get_fct_idx(cpl);
    fct_entry = fct_get_entry(FCT_POOL_W2P, GET_FCT_IDX(full_fct_idx));
    u64 lba = fct_w2p_cmd::get_ccs_cmd_lba(fct_entry);
    u64 nand_addr = fct_w2p_cmd::get_pba_h(fct_entry);
    nand_addr = (nand_addr << 32) | fct_w2p_cmd::get_pba_l(fct_entry);
    u32 ccp_offset = fct_w2p_cmd::get_cpl_pl_ep_ofst(fct_entry);
    u32 comp_size_qc = fct_w2p_cmd::get_cpl_comp_size_qc(fct_entry);
    u64 pba = 0;
    nand_addr_to_pba(&nand_addr, &pba, ccp_offset, comp_size_qc);
    g_l2p[lba] = pba;
    u32 cpl_status = fct_w2p_cmd::get_cpl_val(fct_entry);
    u32 comp_pattern = *(u8 *)fct_w2p_cmd::get_data_addr_sim(fct_entry);
    u32 comp_size_4b_should_be = 4120 / 4;

    if (comp_pattern) {
        comp_size_4b_should_be = ((comp_pattern * 16) + 24) / 4;
    }

    //g_pba_table[g_total_pba] = pba;
    //g_total_pba++;
    INF_LOG("%s: fct_idx %x, lba 0x%llx, pba 0x%llx, nand_addr 0x%llx, ccp_offset 0x%x, cpl status: %x\n",
            __FUNCTION__, fcq_ace_cpl_msg::get_fct_idx(cpl), lba, pba, nand_addr,
             ccp_offset, cpl_status);
    INF_LOG("%s: comp_size_qc: %d, should be: %d\n",
            __FUNCTION__, comp_size_qc, comp_size_4b_should_be);
    ASSERT_EQ(comp_size_qc, comp_size_4b_should_be);
}

// single lba 1~CFG_ONE_PROG_NUM read from pba 0
static bool read_incompressible_data(u64 pba_in, u64 start_lba)
{
    u16 fct_indexes[CFG_LUN_PAGE_SIZE] = {0};
    void *rd_entry = nullptr;
    u8 fourk_idx = 0;
    u64 lba[CFG_ONE_PROG_NUM];
    memset((u8 *)g_read_buf_du, 0, (4096 * CFG_ONE_PROG_NUM));
    pba_t pba;
    pba.all = pba_in;
    g_be_mgr.set_block_stream(pba.block, STREAM_ID_HOT);

    INF_LOG("%s\n", __FUNCTION__);

    for (u8 i = 0; i < CFG_ONE_PROG_NUM; i++) {
        lba[i] = i + start_lba;
    }

    u64 cs_bitmap;
    u8 bad_plane_mask = DEFAULT_BAD_PLANE_MASK;
    u8 nxs;
    u32 page_type_num  = g_be_mgr.get_page_type_num(pba.block, pba.page);
    for (u8 loop = 0; loop < page_type_num; loop++) {
        if (CFG_LUN_PAGE_SIZE != fct_alloc_entries_idx(FCT_POOL_FLASH_RD, fct_indexes, CFG_LUN_PAGE_SIZE)) {
            //BOMB(0);
            return false;
        }

        for (u8 i = 0; i < CFG_LUN_PAGE_SIZE; i++) {
            fourk_idx = loop * CFG_LUN_PAGE_SIZE + i;
            INF_LOG("lba 0x%x fct idx: %d\n",
                    fourk_idx + 1, fct_indexes[i]);
            rd_entry = fct_get_entry(FCT_POOL_FLASH_RD, fct_indexes[i]);

            cs_bitmap = (1 << i);

            nxs = cal_nxs(SCHEME_NORMAL_READ, cs_bitmap, bad_plane_mask);

            fw_flash_read_cmd::read_tlc_page_by_pba_to_buf(rd_entry, ((FCT_POOL_FLASH_RD << FCT_ENTRY_IDX_BITS) | fct_indexes[i]),
                                                          FCQ_FPGA_TEST_RD,
                                                          (u64)((u8 *)g_read_buf_du + fourk_idx * 4096),
                                                          pba.all,
                                                          cs_bitmap,
                                                          &(lba[loop * CFG_LUN_PAGE_SIZE]),
                                                          SCHEME_NORMAL_READ, nxs, bad_plane_mask);
        }

        pba.page_type++;
    }

    return true;
}

// single lba 25~48 read from pba get_next_b2n_pba(0)
static bool read_compressible_data_continuously()
{
    u16   fct_index = 0;
    void *rd_entry = nullptr;
    u64 lba[2][15];

    for (int i = 0; i < 15; i++) {
        lba[0][i] = 25 + i;
        lba[1][i] = 40 + i;
    }

    memset((u8 *)g_read_buf_du, 0, (4096 * CFG_ONE_PROG_NUM));
    u8 *read_buf = g_read_buf_du;
    pba_t pba;
    pba.all = g_l2p[lba[0][0]];

    COLOR_LOG(COLOR_GREEN, "######## compression_test %s ########\n", __FUNCTION__);

    u64 cs_bitmap = 0x7fff;
    u8 bad_plane_mask = DEFAULT_BAD_PLANE_MASK;
    u8 nxs = cal_nxs(SCHEME_NORMAL_READ, cs_bitmap, bad_plane_mask);

    for (u8 loop = 0; loop < 2; loop++) {
        if (1 != fct_alloc_entries_idx(FCT_POOL_FLASH_RD, &fct_index, 1)) {
            //BOMB(0);
            return false;
        }

        // INF_LOG("lba 0x%llx, fct idx: %d\n", lba[loop][0], fct_index);
        rd_entry = fct_get_entry(FCT_POOL_FLASH_RD, fct_index);

        fw_flash_read_cmd::read_tlc_page_by_pba_to_buf(rd_entry, ((FCT_POOL_FLASH_RD << FCT_ENTRY_IDX_BITS) | fct_index),
                                                       FCQ_FPGA_TEST_RD,
                                                       (u64)(read_buf),
                                                       pba.all,
                                                       cs_bitmap,
                                                       lba[loop],
                                                       SCHEME_NORMAL_READ, nxs, bad_plane_mask);

        read_buf += 15 * 4096;
        cs_bitmap = 0x1ff;
        pba.all = g_l2p[lba[1][0]];
    }

    return true;
}

// single lba 25 read from pba 0
static bool read_compressible_data_respectively(u32 w2p_num_of_b2n)
{
    u16 fct_indexes[CFG_LUN_PAGE_SIZE] = {0};
    void *rd_entry = nullptr;
    u64 lba = g_comp_ft_start_lba;
    memset((u8 *)g_read_buf_du, 0, (4096 * CFG_ONE_PROG_NUM));

    COLOR_LOG(COLOR_GREEN, "######## compression_test %s ########\n", __FUNCTION__);

    u64 cs_bitmap = 0x1;
    u8 bad_plane_mask = DEFAULT_BAD_PLANE_MASK;
    u8 nxs = cal_nxs(SCHEME_NORMAL_READ, cs_bitmap, bad_plane_mask);

    for (u8 loop = 0; loop < w2p_num_of_b2n; loop++) {
        if (CFG_LUN_PAGE_SIZE != fct_alloc_entries_idx(FCT_POOL_FLASH_RD, fct_indexes, CFG_LUN_PAGE_SIZE)) {
            //BOMB(0);
            return false;
        }

        for (u8 i = 0; i < CFG_LUN_PAGE_SIZE; i++) {
            INF_LOG("lba 0x%llx, fct idx: %d\n",
                    lba, fct_indexes[i]);
            rd_entry = fct_get_entry(FCT_POOL_FLASH_RD, fct_indexes[i]);

            fw_flash_read_cmd::read_tlc_page_by_pba_to_buf(rd_entry, ((FCT_POOL_FLASH_RD << FCT_ENTRY_IDX_BITS) | fct_indexes[i]),
                                                          FCQ_FPGA_TEST_RD,
                                                          (u64)((u8 *)g_read_buf_du + ((lba - g_comp_ft_start_lba)* 4096)),
                                                          g_l2p[lba],
                                                          cs_bitmap,
                                                          &lba,
                                                          SCHEME_NORMAL_READ, nxs, bad_plane_mask);

            lba++;
        }
    }

    return true;
}

static void check_compressible_data_rd_cpl(ACE_FLASH_RD_CPL_MSG_U *cpl, void *write_buf_in)
{
    void *fct_entry = nullptr;
    fct_entry = fct_get_entry(FCT_POOL_FLASH_RD, GET_FCT_IDX(fcq_ace_cpl_msg::get_fct_idx(cpl)));
    u32 opcode = fcq_ace_cpl_msg::get_opcode(cpl);
    u32 cpl_status = fct_flash_rd_cmd::get_ext_cpl_status_val(fct_entry);
    u32 lba = 0;
    u64 *read_buf = (u64 *)(fct_flash_rd_cmd::get_data_addr_sim(fct_entry));
    u64 *write_buf = nullptr;
    u8 idx = 0;
    u64 cs_bitmap = fct_flash_rd_cmd::get_ccs_cmd_cs_bitmap(fct_entry);
    u32 t_len = 0;

    while (cs_bitmap) {
        if (cs_bitmap & 0x1) {
            lba = fct_flash_rd_cmd::get_lba(fct_entry, &t_len)[idx];
            write_buf = (u64 *)((u8 *)write_buf_in + ((lba-1)*4096));
            INF_LOG("%s: idx: %d, lba 0x%x, "
                    "data 0x%llx, should be 0x%llx, "
                    "cpl opcode: %x, cpl status: 0x%x, read_buf: %p\n",
                    __FUNCTION__, idx, lba,
                    *read_buf, *write_buf,
                    opcode, cpl_status, read_buf);
            ASSERT_TRUE((*read_buf) == (*write_buf));
            read_buf += (4096/8);
        }

        cs_bitmap >>= 1;
        idx++;
    }
}

/* static void flash_rd_cpl_callback_release(u32 fct_idx)
{
    u16 fct_idx_without_poolid = GET_FCT_IDX(fct_idx);
    fct_release_entries_by_idx(GET_POOL_ID(fct_idx), &fct_idx_without_poolid, 1);
} */

/* const u8 cmd_num = CFG_LUN_PAGE_SIZE;
pba_t pba_back[cmd_num];
u32 pba_back_head = 0;
static void w2p_cpl_callback(u32 fct_idx)
{
    u16 fct_idx_without_poolid = GET_FCT_IDX(fct_idx);
    FCT_ENTRY_FOR_ACE_W2P_CMD_U *fct_entry;
    fct_entry = (FCT_ENTRY_FOR_ACE_W2P_CMD_U *)fct_get_entry(FCT_POOL_W2P, GET_FCT_IDX(fct_idx));

    u64 nand_addr = ((u64)(fct_w2p_cmd::get_pba_h(fct_entry)) << 32) + fct_w2p_cmd::get_pba_l(fct_entry);
    u32 ccp_offset = (fct_w2p_cmd::get_cpl_pl_ep_ofst(fct_entry) & 0x3ff);    // low 10 bits are ccp offset
    u64 *pba = (u64 *)&(pba_back[pba_back_head++]);
    u32 comp_len = fct_w2p_cmd::get_cpl_comp_size_qc(fct_entry);

    nand_addr_to_pba(&nand_addr, pba, ccp_offset, comp_len);
    check_w2p_cpl_idx(fct_idx);

    fct_release_entries_by_idx(GET_POOL_ID(fct_idx), &fct_idx_without_poolid, 1);
}; */

static void check_flash_rd_cpl_comp_ft(ACE_FLASH_RD_CPL_MSG_U *cpl, void *write_buf_in)
{
    FCT_ENTRY_FOR_FLASH_RD_CMD_U *fct_entry;
    fct_entry = (FCT_ENTRY_FOR_FLASH_RD_CMD_U *)fct_get_entry(FCT_POOL_FLASH_RD, GET_FCT_IDX(cpl->fields.fct_idx));
    u32 opcode = cpl->fields.opcode;
    u32 cpl_status = fct_flash_rd_cmd::get_cpl_status_val(fct_entry);
    u64 lba = 0;
    u64 lba_h = 0;
    u64 *read_buf = (u64 *)fct_flash_rd_cmd::get_data_addr_sim(fct_entry);
    u64 *write_buf = nullptr;
    u8 idx = 0;
    u64 cs_bitmap = fct_flash_rd_cmd::get_ccs_cmd_cs_bitmap(fct_entry);
    u32 lba_arr_len;
    u32 *lba_arr = (u32 *)fct_flash_rd_cmd::get_lba(fct_entry, &lba_arr_len); 

    while (cs_bitmap) {
        if (cs_bitmap & 0x1) {
            lba_h = ((u64)lba_arr[(idx * 2) + 1]&0xFFFFFFFFFFFFFFFFULL);
            lba_h <<= 32;
            lba = (lba_h | (u64)lba_arr[(idx * 2)]);
            write_buf = (u64 *)((u8 *)write_buf_in + ((lba - g_comp_ft_start_lba)*4096));
            DBG_LOG("%s: idx: %d, lba 0x%llx, "
                    "data 0x%llx, should be 0x%llx, "
                    "cpl opcode: %x, cpl status: 0x%x, read_buf: %p\n",
                    __FUNCTION__, idx, lba,
                    *read_buf, *write_buf,
                    opcode, cpl_status, read_buf);
            if ((*read_buf) != (*write_buf)) {
                BOMB(FT_ASSERT, 0);
            }
            ASSERT_TRUE((*read_buf) == (*write_buf));
            read_buf += (4096/8);
        }

        cs_bitmap >>= 1;
        idx++;
    }
}

#if __X86__ == 1
// test incompressible data
FPGA_TEST(compressTest, basic, BIT(0))
{
    COLOR_LOG(COLOR_GREEN, "######## compression_test %s ########\n", __FUNCTION__);
    const u32 block = 0x2d;    // test on this block
    pba_t b2n_pba = {.all = 0};
    b2n_pba.block = block;
    INF_LOG("erase block: 0x%x\n", block);
    ft_erase_sblock(block);
    fpga_wait_erase_cq_msg(CFG_CHAN_NUM * CFG_CE_NUM * CFG_LUN_NUM);

    g_comp_ft_start_lba = 1;
    // write
    ASSERT_EQ(true, send_single_b2n_cmd(b2n_pba.all));
    g_be_mgr.set_block_stream(b2n_pba.block, STREAM_ID_HOT);
    u32 page_type_num  = g_be_mgr.get_page_type_num(b2n_pba.block, b2n_pba.page);
    u32 w2p_num_of_b2n = page_type_num * CFG_LUN_PAGE_SIZE;
    test_compressible_data(g_write_buf_du, g_comp_ft_start_lba, 0, w2p_num_of_b2n);
    fpga_wait_w2p_cq_msg(w2p_num_of_b2n, STREAM_ID_HOT, check_w2p_cpl_compressible);
    fpga_wait_b2n_cq_msg(1);

    // read
    read_incompressible_data(b2n_pba.all, g_comp_ft_start_lba);
    fpga_wait_flash_rd_cq_msg(w2p_num_of_b2n, g_write_buf_du, check_flash_rd_cpl_comp_ft);
}

// test compressible data with commpression rate 2 (compressed size 2048 bytes)
FPGA_TEST(compressTest, rate2, 0) //BIT(0)
{
    COLOR_LOG(COLOR_GREEN, "######## compression_test %s ########\n", __FUNCTION__);
    const u32 block = 0x2d;    // test on this block
    pba_t b2n_pba = {.all = 0};
    b2n_pba.block = block;
    INF_LOG("erase block: 0x%x\n", block);
    ft_erase_sblock(block);
    fpga_wait_erase_cq_msg(CFG_CHAN_NUM * CFG_CE_NUM * CFG_LUN_NUM);

    g_comp_ft_start_lba = 25;
    // write
    ASSERT_EQ(true, send_single_b2n_cmd(b2n_pba.all));
    g_be_mgr.set_block_stream(b2n_pba.block, STREAM_ID_HOT);
    u32 page_type_num  = g_be_mgr.get_page_type_num(b2n_pba.block, b2n_pba.page);
    u32 w2p_num_of_b2n = page_type_num * CFG_LUN_PAGE_SIZE;
    test_compressible_data(g_write_buf_du, g_comp_ft_start_lba, g_compression_pattern, w2p_num_of_b2n);
    fpga_wait_w2p_cq_msg(w2p_num_of_b2n, STREAM_ID_HOT, check_w2p_cpl_compressible);
    // --> since the data is compressible, has not fill one b2n here, so send b2n flush
    u32 b2n_flush_mode = 1;
    send_single_b2n_flush_cmd(STREAM_ID_HOT, b2n_flush_mode);
    fpga_wait_b2n_cq_msg(1);
    fpga_wait_b2n_flush_cq_msg(1);

    // read
    read_compressible_data_continuously();
    fpga_wait_flash_rd_cq_msg(2, g_write_buf_du, check_flash_rd_cpl_comp_ft);

    read_compressible_data_respectively(w2p_num_of_b2n);
    fpga_wait_flash_rd_cq_msg(w2p_num_of_b2n, g_write_buf_du, check_flash_rd_cpl_comp_ft);
}
#endif
#endif

```

