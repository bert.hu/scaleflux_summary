```c++
#if (RFS_UNITTEST_SYSCREATE != 0)
#if __INCLUDE_UNIT_TEST__ == 1
TEST(RFS_Create, rfs_unit_test, BIT(0))
{
    g_rfs_file_task._rfs_ut_test = true;
    rfs_system_unit_test();
    g_rfs_file_task._state = RFS_FILE_TASK_STATE_PAUSED;
    g_rfs_file_task._rfs_ut_test = false;
}
#endif
#else

static u8 g_slc_write_buf[4096 * CFG_LUN_PAGE_SIZE];
static u8 g_slc_read_buf[4096 * CFG_LUN_PAGE_SIZE];

void fpga_test_rfs()
{
    static u64 start_pba = 0;
    u8 i = 0;
    u8 cnt = 0;

    INF_LOG("%s: pba 0x%llx\n", __FUNCTION__, start_pba);

    if (((pba_t *)&start_pba)->block != 0) {
        return;
    }

    rfs_send_erase_cmd(start_pba);

    while (RFS_RC_NAND_IN_CMD_POLLING == rfs_poll_erase_cpl()) {
        // empty polling loop
    }

    for (i = 0; i < CFG_LUN_PAGE_SIZE; i++) {
        seed_rand(bsp_platform::timer_get_tickcount());
        g_slc_write_buf[4096*i] = (u8)randnum();
    }

    rfs_send_b2n_cmd(start_pba);

    for (i = 0; i < CFG_LUN_PAGE_SIZE; i++) {
        rfs_send_write_cmd(g_slc_write_buf+4096*i);
        cnt++;
    }

    while (cnt) {
        if (rfs_poll_write_cpl()) {
            cnt--;
        }
    }

    while (RFS_RC_NAND_IN_CMD_POLLING == rfs_poll_b2n_cpl()) {
        // empty polling loop
    }

    rfs_send_read_cmd(g_slc_read_buf, start_pba);

    while (RFS_RC_NAND_IN_CMD_POLLING == rfs_poll_read_cpl()) {
        // empty polling loop
    }

    for (i = 0; i < CFG_LUN_PAGE_SIZE; i++) {
        ASSERT_TRUE(g_slc_read_buf[4096*i] == g_slc_write_buf[4096*i]);
    }

    start_pba = get_next_b2n_pba(start_pba);
}

FPGA_TEST(FPGA, rfs_test, BIT(0))
{
    fpga_test_rfs();
}

#endif // (RFS_UNITTEST_SYSCREATE != 0)
```

