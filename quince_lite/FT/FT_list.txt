Write Command FT
1. basic write cmd, erase&write&program+read + check
2. basic + Bad_pl_msk + check
3. multi-stream write
4. multi-q write on same stream write
5. deffrent Des + src test
6. deffrent Des + src  + Bad_pl_msk test
7. different nlb + program cmd
8. nlb + program cmd + Bad_pl_msk
9. LBA_md的write，read匹配 test
10. LBA_md + nlb + LP_bmp test
11. LBA_md + nlb + LP_bmp + Bad_pl_msk
12. stripe_idx test
13. stripe_idx + Bad_pl_msk test
14. pbam related test --不明确
15. different LP_fmt test --hw
16. bstart+pstart+blast+plast validation test
17. different scheme + Bad_pl_msk test
18. LP_fmt + Bad_pl_msk program test
19. write multi-page 

Read Command FT
1. basic read (normal read + read to DDR)
2. TLC read (normal read + read to DDR) + Bad_pl_msk test
3. diffrent read_scheme + sub_opc test
4. lbac + LBA_md + LP_bmp + lbam check
5. lbac + LBA_md + LP_bmp + lbam +  Bad_pl_msk test
6. Rfnd + check rfwb
7. Rfnd + Bad_pl_msk + check rfwb test
8. different Page_type read + check
9. different Page_type read + check + Bad_pl_msk test
10. hw-related variable (fuse, pe cnt, vt)test -- hw
11. nlb write + LP_bmp read test
12.  nlb write + LP_bmp + Bad_pl_msk test
13. multi-read on same block test
14. LP_fmt + Bad_pl_msk read test

Program Command FT
1. basic program test
2. double program test
3. multi stream program test
4. multi fcq on same stream program test
5. different src + des program test
6. different src + des + Bad_pl_msk program test
7. measure Tprog SLC mode test
8. measure Tprog TLC mode test
9. stripe idx test
10. stripe idx + Bad_pl_msk program test
11. program with raid cmd parity test
12. program with raid cmd parity + Bad_pl_msk test
13. hw-related variable (fuse, pe cnt)test -- hw
14. LP_fmt + Bad_pl_msk program test


GC related FT
1. gc program + gc read basic test -- normal write + gc read + program + normal read + check
2. header extraction + read to DDR
3. header extraction + read to DDR + Bad_pl_msk test
4. gc program + read + lbac + LBA_md + LP_bmp + lbam check
5. gc program + read + lbac + LBA_md + LP_bmp + lbam +  Bad_pl_msk test
6. measure GC program + read test
7. measure header extraction test
8. deffrent Des + src + gc program
9. multi-core gc program + read

