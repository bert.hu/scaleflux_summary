```
无论是NVMe的命令本身，还是要传输的数据，最终都会被封装成为TLP包进行传输。
AIC、U.2以及M.2等形态的NVMe SSD也都是借助PCIe插槽与host进行交互的。
```

```
本文使用的是《NVM Express Revision 1.2a》（以下简称“NVMe协议”）
```

```
介绍的是NVMe over PCIe的读I/O，不涉及NVMe over Fabrics或者其他网络传输。
```

```
1. 一个I/O命令执行过程中，host和SSD是两个主角。读I/O也就是host发起读请求，SSD响应请求，并且将数据写到host指定的内存地址上的过程。
2. Submission Queue(SQ) 和Completion Queue(CQ)是host内存中的两个队列，host通过SQ告知SSD的Controller要处理的命令，Controller通过CQ告知Host已经被处理完毕的命令以及这些命令执行的状态。

需要补充的是， NVMe的命令分为Admin Command和NVM Command，Admin Command执行一些SSD管控操作，比如Namespace管理和固件升级等，系统中只有一对Admin Submission Queue和Admin Completion Queue；NVM Command用于执行读写等I/O命令，可以有多对I/O Submission Queue和I/O Completion Queue。

3. Submission Queue Tail Doorbell和Completion Queue Head Doorbell，这是两个Controller Registers中的字段，会映射在host内存中。host通过更新Submission Queue Tail Doorbell映射的内存上的记录，就可以达到告知SSD有新的命令需要处理，同样，在命令执行尾声，host也是通过更新Completion Queue head Doorbell映射的内存上的信息而实现告知SSD命令已经执行完毕。

4. 至于SSD要把数据往内存哪段地址上写，怎么写，就要靠PRP或者SGL

NVMe规定了PRP和SGL两种数据传输方式，后者在NVMe over Fabrics中使用比较普遍，这里不再赘述。采用PRP方式的I/O命令都会有PRP1和PRP2两个地址，它们可以指向一个内存地址，也可以是一个物理地址列表，被称为PRP list，如果传输数据量超过两个内存页，就需要PRP list指明数据存放的地址。本文中例子数据量为4KiB，恰好为Linux的1个内存页大小，所以使用PRP1传一个物理地址就可以。

名词&概念
SLBA（Starting LBA）：数据在SSD上的起始地址，这里的值是0x8。
NLB（Number of Logical Blocks）:读取数据的blocks数，这里的值是0x7，需要指出的是这是一个0’s based value，所以最终传的是8个block。
NSID（Namespace Identifier）：指明了此次读操作的namespace id，这里值为0x1。
```

