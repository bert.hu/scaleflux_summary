mpages会分为很多的zones，每个zones有1024个mpages

zone0 ： mpage0~1023

zone1 ： mpage1024~2047

zone2 ： mpage2048~3071

......

ZONE是管理l2p flush,  GC，load and recovery的基本单元

一个zone table包含了1024个mpages的PBA，每个PBA为4 bytes，那么一个zone table的大小是4bytes * 1024 = 4K

open zone：表示正在flushing的zone

close zone：表示已经flush到nand上的zone

因此zone只会占用4k的DDR，这个DDR是被复用的，当PF来的时候，这4K的DDR会被flush到nand上

在L2P stream的block上，是各个Mpages和Patchs的块，每1024个Mpages称为一个zone，每个zone的最后是zone table，里面包含的1024个Mpages的PBA，大小就是4K

更上一级，是globle zone table，记录的是每个zone的PBA，那么如果是12G DDR的L2P table，就是12G/4K=3M的Mpages，就是3M/1024=3K个zones，依旧，每个zone的PBA还是用4 bytes来记录，那么global zone table的大小就是3K * 4bytes = 12K的大小

这个global zone table在PF的时候也需要dump到nand上

那么这两部分，总共4K的open zone + 12K的global zone table=16K，将会在PF的时候dump到nand上面



L2P Recovery---from Mpage

L2P load：

对于close zone，可以从PF Recovery最新的global zone table上找到最后一个记录的close zone的PBA，并从这个close zone中找到每个Mpage的PBA

对于open zone，可以从PF dump的open zone table来恢复



L2P Recovery---from Patch

从global zone table上找到最后一个记录的close zone的PBA，在这个PBA之前的Patch都是无效的，因为已经可以通过Mpage来恢复之前的L2P了，所以只需要考虑从nand上load这个最后一个记录的close zone的PBA之后的Patch，解析并更新相应的L2P