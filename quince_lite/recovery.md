极简流程

```c++
for (u32 i = 0; i < CFG_BLOCK_NUM; i++) {
     INF_LOG("get_sblock_valid_lpa_cnt(0x%x) 0x%x\n", i, g_pu_mgr.pblk_mgr[0].get_sblock_valid_lpa_cnt(i));
}
for (u32 i = 0; i < CFG_BLOCK_NUM; i++) {
     INF_LOG("get_sblock_stream(0x%x) 0x%x, 0x%llx, 0x%llx\n", i, 
             g_pu_mgr.pblk_mgr[0].get_sblock_stream(i), 
             g_pu_mgr.pblk_mgr[0].get_sblock_open_seq_num(i), 
             g_pu_mgr.pblk_mgr[0].get_sblock_last_prog_b2n_pba_all(i));
}
```

去掉raid rebuild

```c++
if (g_panic_recovery) {
    eh_act &= (~EH_ACT_REBUILD);
}
```

nor_addr 0x1fb0000, struct size 1152

```
u32  get_sblock_status(u32 blk) { return _block_table[blk]._status; };
u32  get_sblock_stream(u32 blk) { return _block_table[blk]._stream.atomic_read(); };
u64  get_sblock_open_seq_num(u32 blk) { return _block_table[blk]._open_seq_num.atomic_read(); };
u64  get_sblock_last_prog_b2n_pba_all(u32 blk) { return _block_table[blk]._last_prog_b2n_pba.atomic_read(); };
```

