| **Name****** | **Description**               |
| ------------ | ----------------------------- |
| ACP          | Accelerated Coherency Port    |
| APS          | Application Process Subsystem |
| B2N          | Buffer to NAND                |
| BE           | Back End                      |
| BEACE        | Back End Auto Command Engine  |
| BM           | Buffer Manager                |
| CAC          | Crypto Assist Controller      |
| CCS          | Central Command Subsystem     |
| CPUSS        | CPU Subsystem                 |
| CSR          | Control and  Status Register  |
| DAP          | Direct Access Port            |
| DCS          | Data Compute Subsystem        |
| DFD          | Design for Debug              |
| DMA          | Direct Memory Access          |
| FCE          | Flash Controller Engine       |
| FEACE        | Front End Auto Command Engine |
| FIS          | Front End Interface Subsystem |
| L2P          | Logical to Physical           |
| LOC          | LBA Overlap Check             |
| MEC          | Media Encryption Controller   |
| MXSS         | Mixed Signal Subsystem        |
| NPS          | NAND PHY Subsystem            |
| NVME         | Non-Volatile Memory Express   |
| PRP          | Physical Region Page          |
| PSS          | PCIe Subsystem                |
| SED          | Self-Encrypt Drive            |
| SDS          | Storage Datapath Subsystem    |
| SSS          | Security Sub-System           |
| STP          | Serial trace Port             |
| TBD          | To Be Determined              |
| W2P          | Write to Program              |

## [1.1  Key Procedure Definitions]()

² **RD_DMA Trigger**

The rd_dma_trig_cmd is sent by FW after FW received read commandfrom NVME-IP and data is ready in SDSR or DRAM. It’s used to trigger host toread data from SDSR or DRAM. To separate the two sources, rd_dma_trigger hastwo types of triggers: one is read from SDSR to Host, the other one is readfrom DRAM to Host.

² **PRP_Trigger**

The prp_trig_cmd is used to read the physical address from NVME-IPif the read command wants to read more range’s physical address from FLASH. Itmust be sent out to FEACE earlier than RD_DMA trigger if the physical addressrange is over two.

² **WR_DMA Trigger**

The wr_dma_trig command is sent by FW, after FW received writeCommand from NVME-IP and B2N command is sent out to HW.  There are two types of wr_dma_trig: one iswriting from Host to landing buffer, the other one is from Host to DRAM. Ifwrite to landing buffer, ACE need to fetch the landing buffer address, but ifwrite to DRAM, FW should set the destination address in command message. 

² **W2P (Write to Program)**

W2P commandis a command caused by wr_dma_trig which sent to BEACE. This command’s mainpurpose is to give BEACE information for flash command table. The detailinformation can be got specific in function description. Like FLBA is used totell BEACE where the write data should be written to in FLASH.

² **FE2BE Read (BE Read)**

FE2BEread command is a command caused by cmd_fetch, which fetch SCT or ACT commandfrom FW and sent to BEACE to read data from FLASH. Completion would return toFEACE when data stored in SDSR.

² **L2P (Logic to Physical translate)**

L2Pcommand is by L2P engine to translate logic address to physical address. TheL2P FCT was prepared by FW, fetched by FEACE and passed to L2P enginetransparently.  L2P engine returnscompletion to FEACE when finishing translation.