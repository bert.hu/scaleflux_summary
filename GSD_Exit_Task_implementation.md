![image-20211204171941569](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20211204171941569.png)

GSD Exit Task implementation



![image-20211205122721058](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20211205122721058.png)





素材

Plan

•Myrtle code walk-through

Utility

üPlatform

•GSD Exit workflow implementation

VU implementation



§Future work

•Trim simulator/FPGA verification

 Verify Trim on simulator & FPGA, such as push trim in admin queue, loc stop/resume, trim effect.



•Trim Frontend/backend design/implementation/debug

 Design and implement complete trim progress in frontend and backend and do debugging.



•Myrtle architecture(simulator, firmware-hardware)

 Learn the whole architecture of system on simulator and interaction between firmware and hardware.





Agenda

**Myrtle source code walk-through**

Feature development

Summary



**The important feature**

   Data is read and written at the page level,

but erased at the block level.

**Channel** : The ability to read and write concurrently

**CE_LUN** : the smallest logical unit can be read and written independently



•**Command**(**Admin & I/O** **)**

•**SQ** **and CQ:**

•Appear in pairs

•Admin SQ/CQ is only one pair 

•**DB**

•Host can only write , can not read DB

•Host know the tail of SQ and the head of CQ, SSD know the head of SQ and the tail of CQ

•DB records the head and the tail of SQ and CQ



SQ: a circular buffer with a fixed slot size that the host software uses to submit commands.

CQ: a circular buffer with a fixed slot size used to post status for completed commands. CQ info contain SQ Head Pointer and Phase Tag to inform host where is the latest SQ head and CQ tail.

Doorbell: store head and tail of SQ and CQ, host only can write SQ tail DB and CQ head DB,

Host software writes the Submission Queue Tail Doorbell and the Completion Queue Head to communicate new values of the corresponding entry pointers to the controller. 

![image-20211205122413170](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20211205122413170.png)

![image-20211205122421402](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20211205122421402.png)



PRP & SGL：

Host tells the SSD, where is the data in the memory.

•PRP(Admin only)

PRP entry describes a physical page space.



•SGL(Admin or IO)

SGL is a linked list, composed of SGL Segments, SGL Segments are composed of SGL Descriptors. SGL Descriptor describes a continuous physical memory space.



PRP && SGL: Classic methods for SSD to find address in host (necessary because SSD is the absolute active one in the interaction with host)

**1.PRP(Physical Region Page):**



​    One PRP entry indicate one address which composed of page base address and offset in page.

​    If one PRP is not enough, it can point to a PRP list, whose each node indicates one page and offset must be 0.

**2.SGL(Scatter Gather List):**



​     SGL is a list composed of one or more SGL segments. Each SGL segment composed of one or more SGL Descriptors that indicates a section of continuous memory 

PRP: 描述一段连续的物理内存的起始地址，entry可以指向一个物理数据地址，也可以指向一个entry list。基本的操作单元就是一个物理页，Admin cmd只能使用PRP传数据。



SGL：是一个数据结构，用以描述一段数据空间，这个空间可以是数据源所在的空间，也可以是数据目标空间。

![image-20211205122601935](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20211205122601935.png)

![image-20211205122651900](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20211205122651900.png)



![image-20211205122524434](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20211205122524434.png)

![image-20211205122537410](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20211205122537410.png)





CCS

   The CCS is the central command processing sub-system. The CCS receives commands from FIS (OC Mode) (SID=5), ACE(SID=1), FW (SID=0) and others as an AXI3 slave. Then CCS will co-work with other data path modules (SDS/BM/BE) to finish different tasks like write, read and others as a center control.

![image-20211204174200640](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20211204174200640.png)

ACT and FCT

ACT command used to represent NVMe commands, it lives on the frontend of system. FCT is used to store the read/write/erase/… commands that target for backend of system.

Command Table structure

Command table is a data structure array located at 2MB SRAM.

Command table allocation and release

For ACT command table entries, HW(FE ACE, specifically) will be allocator, FW will be responsible for release them. For FCT, FW will do the allocation and release.



§Block cannot be over programmed, must program after erasing

§Block has limited number of PE

§Has bad block naturally

§MLC, TLC has lower page corruption problem

§Always write from head of page

§Read distribution/Write distribution/Charge leakage/Coupling capacitance etc.



FTL tasks:

​    L2p mapping, GC, trim, wear leaving, bad block management, read distribution process, data retention

§L2p mapping: Mapping method between LBA and PBA, need to take performance, power fail, DRAM capacity etc. into consideration. 

§GC: Clear garbage and space memory for new data, need to take wear leaving, timing of GC, size of OP etc. into consideration.

§Wear leaving: Keep the erasing and writing tasks allocating on every blocks as balanced as possible

§Bad block management: Manage how to handle bad blocks, such as skip or replace the bad block.

§RD: Bit may converse because of too many times read on one wordline, need to record number of read on each block, and set a threshold to move the data which is read more than threshold.

§DR: Data could change over a long time because of electrons leakage, FTL should do detection and update regularly when power on.

§EH(Error Handle):Error may happen when read/write/erase because of device itself, such as RD, DR. EH depends on the type of error

§Trim: After user deleting file, trim inform SSD that the corresponding page is invalid in time.