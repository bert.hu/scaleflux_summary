```
root_data
[2022-09-21 14:40:12.412] [6-1970:1:1|0:0:0:214]    U0018929_ROOT_DATA:
[2022-09-21 14:40:12.412]   > root_data_error_code:    0
[2022-09-21 14:40:12.412]   > pf_cnt:                 1
[2022-09-21 14:40:12.412]   > current_pf_sblock:    17c
[2022-09-21 14:40:12.412]   > summary_data_pba:    5f001340
[2022-09-21 14:40:12.412]   > last_written_pba:    5f0017c0
[2022-09-21 14:40:12.412]   > nor_sub_sector_idx:     0
[2022-09-21 14:40:12.412]   > user_data_start_pba: 5f000000
[2022-09-21 14:40:12.412]   > user_data_end_pba:   5f000340
[2022-09-21 14:40:12.412]   > parity_data_start_pba: 5f000380
[2022-09-21 14:40:12.468]   > parity_data_end_pba: 5f000540
[2022-09-21 14:40:12.468]   > meta_data_start_pba: 5f000800
[2022-09-21 14:40:12.468]   > meta_data_end_pba:   5f001300

user_data_start_pba: 5f000000, blk 380, pege 0, die 0, ce 0, lun 0, ch 0
user_data_end_pba:   5f000340, blk 380, pege 0, die 13, ce 0, lun 0, ch 13
parity_data_start_pba: 5f000380, blk 380, pege 0, die 14, ce 0, lun 0, ch 14
parity_data_end_pba: 5f000540, blk 380, pege 0, die 21, ce 1, lun 0, ch 5
meta_data_start_pba: 5f000800, blk 380, pege 0, die 32, ce 0, lun 1, ch 0
meta_data_end_pba:   5f001300, blk 380, pege 1, die 12, ce 0, lun 0, ch 12
summary_data_pba:    5f001340, blk 380, pege 1, die 13, ce 0, lun 0, ch 13
last_written_pba:    5f0017c0, blk 380, pege 1, die 31, ce 1, lun 0, ch 15

下一次
[2022-09-21 14:41:58.571] [6-1970:1:1|0:0:0:161]    U0018929_ROOT_DATA:
[2022-09-21 14:41:58.571]   > root_data_error_code:    0
[2022-09-21 14:41:58.571]   > pf_cnt:                 2
[2022-09-21 14:41:58.633]   > current_pf_sblock:    17c
[2022-09-21 14:41:58.633]   > summary_data_pba:    5f002b40
[2022-09-21 14:41:58.633]   > last_written_pba:    5f002fc0
[2022-09-21 14:41:58.633]   > nor_sub_sector_idx:     1
[2022-09-21 14:41:58.633]   > user_data_start_pba: 5f001800
[2022-09-21 14:41:58.633]   > user_data_end_pba:   5f001b40
[2022-09-21 14:41:58.633]   > parity_data_start_pba: 5f001b80
[2022-09-21 14:41:58.633]   > parity_data_end_pba: 5f001d40
[2022-09-21 14:41:58.633]   > meta_data_start_pba: 5f002000
[2022-09-21 14:41:58.633]   > meta_data_end_pba:   5f002b00

user_data_start_pba: 5f001800, blk 380, pege 1, die 32, ce 0, lun 1, ch 0
```

```
b2n get sblock stream 0
[2022-09-21 14:40:12.576] [6-1970:1:1|0:0:0:222]    U0018929_B2N_GET_SBLOCK:
[2022-09-21 14:40:12.576]   > _blk_id:              183
[2022-09-21 14:40:12.576]   > _pe_cnt:              117
[2022-09-21 14:40:12.576]   > _stream:                0
[2022-09-21 14:40:12.576]   > _last_program_b2n_pba: 60ca66c0
Total number of w2ps is: 2095104 from start pba: 60c00000 to end pba: 60ec7f80, sblock 387
Total number of w2ps is: 1607680 from start pba: 60ca6700 to end pba: 60ec7f80, sblock 387
Total number of w2ps is: 2093682 from start pba: 60c00000 to end pba: 60ec7f80, sblock 387
Total number of w2ps is: 1606590 from start pba: 60ca6700 to end pba: 60ec7f80, sblock 387
start pba: 60c00000，block 387, page 0, ce 0, lun 0 ,ch 0, die 0
start pba: 60ca6700，block 387, page 166, ce 1, lun 0 ,ch 12, die 28
end pba: 60ec7f80，block 387, page 711, ce 1, lun 1 ,ch 14, die 62

copy back
[2022-09-21 14:40:13.183] [6-1970:1:1|0:0:0:781]    U0018929_COPY_BACK_DATA:
[2022-09-21 14:40:13.183]   > orignal_pba:         60ca6600
[2022-09-21 14:40:13.183]   > target_pba_main:     5f000000
blk 387, page 166, lun 0, ce 1, ch 8, die 24

[2022-09-21 14:40:13.246] [6-1970:1:1|0:0:0:786]    U0018929_COPY_BACK_DATA:
[2022-09-21 14:40:13.246]   > orignal_pba:         60ca6640
[2022-09-21 14:40:13.246]   > target_pba_main:     5f0000c0
blk 387, page 166, lun 0, ce 1, ch 9, die 25

[2022-09-21 14:40:13.357] [6-1970:1:1|0:0:0:792]    U0018929_COPY_BACK_DATA:
[2022-09-21 14:40:13.357]   > orignal_pba:         60ca6680
[2022-09-21 14:40:13.357]   > target_pba_main:     5f000180

[2022-09-21 14:40:13.474] [6-1970:1:1|0:0:0:797]    U0018929_COPY_BACK_DATA:
[2022-09-21 14:40:13.474]   > orignal_pba:         60ca66c0
[2022-09-21 14:40:13.474]   > target_pba_main:     5f000240

padding task 
useup b2n, last sent b2n pba 1623877376, block 387, page 166, die 28
padding task 
useup b2n, last parity(or footer page) b2n pba 1623877568, block 387, page 166, die 31
> _before_start_padding_pba: 20300060ca66ef
> _start_padding_pba:  60ca6700, block 387, page 166, ce 1, lun 0 ,ch 12, die 28
> _end_padding_pba:    60ca67c0, block 387, page 166, ce 1, lun 0 ,ch 15, die 31
总结，padding到这个spage的parity group的边界

[2022-09-21 14:40:14.463] [5-1970:1:1|0:0:0:818]    U0018929_PADDING_END:
[2022-09-21 14:40:14.463]   > _w2p_num:              8f
[2022-09-21 14:40:14.463]   > _w2p_sent_cnt:         8f
[2022-09-21 14:40:14.463]   > _w2p_cpl_cnt:          8f
[2022-09-21 14:40:14.463]   > _flush_cnt:             1
[2022-09-21 14:40:14.463]   > _flush_boundary_pba: 20300060ca67af
[2022-09-21 14:40:14.463]   > _last_cpl_pba:              0
_flush_boundary_pba: 20300060ca67af, block 387, page 166, ce 1, lun 0 ,ch 14, die 30

第二次

```

```
b2n get sblock stream 2
[2022-09-21 14:40:12.677] [6-1970:1:1|0:0:0:222]    U0018929_B2N_GET_SBLOCK:
[2022-09-21 14:40:12.677]   > _blk_id:              17e
[2022-09-21 14:40:12.677]   > _pe_cnt:              117
[2022-09-21 14:40:12.677]   > _stream:                2
[2022-09-21 14:40:12.732]   > _last_program_b2n_pba: ffffffffffffffff
Total number of w2ps is: 2095104 from start pba: 5f800000 to end pba: 5fac7f80, sblock 382
Total number of w2ps is: 2095104 from start pba: 5f800000 to end pba: 5fac7f80, sblock 382
```

```
b2n get sblock stream 4
[2022-09-21 14:40:12.792] [6-1970:1:1|0:0:0:222]    U0018929_B2N_GET_SBLOCK:
[2022-09-21 14:40:12.792]   > _blk_id:              17f
[2022-09-21 14:40:12.792]   > _pe_cnt:              117
[2022-09-21 14:40:12.792]   > _stream:                4
[2022-09-21 14:40:12.792]   > _last_program_b2n_pba: ffffffffffffffff
Total number of w2ps is: 2095104 from start pba: 5fc00000 to end pba: 5fec7f80, sblock 383
Total number of w2ps is: 2095104 from start pba: 5fc00000 to end pba: 5fec7f80, sblock 383


```

```
b2n get sblock stream 6
[2022-09-21 14:40:12.846] [6-1970:1:1|0:0:0:222]    U0018929_B2N_GET_SBLOCK:
[2022-09-21 14:40:12.846]   > _blk_id:              1d4
[2022-09-21 14:40:12.846]   > _pe_cnt:              104
[2022-09-21 14:40:12.846]   > _stream:                6
[2022-09-21 14:40:12.846]   > _last_program_b2n_pba: ffffffffffffffff
Total number of w2ps is: 695552 from start pba: 75000000 to end pba: 752bff80, sblock 468
Total number of w2ps is: 695552 from start pba: 75000000 to end pba: 752bff80, sblock 468


```

```
b2n get sblock stream 7
[2022-09-21 14:40:12.900] [6-1970:1:1|0:0:0:222]    U0018929_B2N_GET_SBLOCK:
[2022-09-21 14:40:12.900]   > _blk_id:              1ec
[2022-09-21 14:40:12.958]   > _pe_cnt:              104
[2022-09-21 14:40:12.958]   > _stream:                7
[2022-09-21 14:40:12.958]   > _last_program_b2n_pba: 7b03f8c0
Total number of w2ps is: 698368 from start pba: 7b000000 to end pba: 7b2bff80, sblock 492
Total number of w2ps is: 635312 from start pba: 7b03f900 to end pba: 7b2bff80, sblock 492
_last_program_b2n_pba: 7b03f8c0, block 492, page 63, ce 0, lun 1 ,ch 3, die 35

start pba: 7b03f900, block 492, page 63, ce 0, lun 1 ,ch 4, die 36
end pba: 7b2bff80, block 492, page 703, ce 1, lun 1 ,ch 14, die 62
总结：padding到slc的block的倒数第二个die，除开parity die

[2022-09-21 14:40:13.066] [6-1970:1:1|0:0:0:223]    U0018929_RESTORE_L2P_PATCH_MT_D:
[2022-09-21 14:40:13.066]   > patch_start_pba:     2030007b007800
[2022-09-21 14:40:13.066]   > buffer_cnt:             4


copy back
[2022-09-21 14:40:13.534] [6-1970:1:1|0:0:0:803]    U0018929_COPY_BACK_DATA:
[2022-09-21 14:40:13.534]   > orignal_pba:         7b03f8c0
[2022-09-21 14:40:13.534]   > target_pba_main:     5f000300
[2022-09-21 14:40:13.534]   > target_pba_backup:   5f000340

[2022-09-21 14:40:14.876] [5-1970:1:1|0:0:0:818]    U0018929_PADDING_PRECOMPUTE_DONE:
[2022-09-21 14:40:14.876]   > _w2p_num:             1b0
[2022-09-21 14:40:14.876]   > _before_start_padding_pba: 2030007b03f8cf
[2022-09-21 14:40:14.876]   > _start_padding_pba:  7b03f900
[2022-09-21 14:40:14.876]   > _end_padding_pba:    7b03ffc0
_start_padding_pba:  7b03f900, block 492, page 63, ce 0, lun 1 ,ch 4, die 36
_end_padding_pba:    7b03ffc0, block 492, page 63, ce 1, lun 1 ,ch 15, die 63
padding到这个spage的parity group的最后

[2022-09-21 14:40:14.932] [5-1970:1:1|0:0:0:821]    U0018929_PADDING_END:
[2022-09-21 14:40:14.932]   > _w2p_num:             1b0
[2022-09-21 14:40:14.933]   > _w2p_sent_cnt:        1b0
[2022-09-21 14:40:14.933]   > _w2p_cpl_cnt:         1b0
[2022-09-21 14:40:14.933]   > _flush_cnt:             0
[2022-09-21 14:40:14.933]   > _flush_boundary_pba:        0
[2022-09-21 14:40:14.933]   > _last_cpl_pba:       2030007b03ff8f

第二次
[2022-09-21 14:41:59.121] [6-1970:1:1|0:0:0:170]    U0018929_B2N_GET_SBLOCK:
[2022-09-21 14:41:59.121]   > _blk_id:              1ec
[2022-09-21 14:41:59.121]   > _pe_cnt:              104
[2022-09-21 14:41:59.121]   > _stream:                7
[2022-09-21 14:41:59.121]   > _last_program_b2n_pba: 7b06be00
[2022-09-21 14:41:59.121]   > _recovered_consume_space: 2030
[2022-09-21 14:41:59.121]   > _good_plane_num:      100
[2022-09-21 14:41:59.121]   > _deallocated_sblock_num:  218
[2022-09-21 14:41:59.121]   > _real_free_sblock_num:   13
[2022-09-21 14:41:59.172]   > _recycle_fifo_num:      0
[2022-09-21 14:41:59.172]   > _sealed_blk_num:        3
_last_program_b2n_pba: 7b06be00, block 492, page 107, ce 1, lun 1 ,ch 8, die 56

Total number of w2ps is: 698368 from start pba: 7b000000 to end pba: 7b2bff80, sblock 492
Total number of w2ps is: 591328 from start pba: 7b06be40 to end pba: 7b2bff80, sblock 492
start pba: 7b06be40, block 492, page 107, ce 1, lun 1 ,ch 1, die 49
end pba: 7b2bff80, block 492, page 703, ce 1, lun 1 ,ch 14, die 62
```

```
raid rebuild copy back
[2022-09-21 14:40:13.645] [6-1970:1:1|0:0:0:805]    U0018929_PARITY_REBUILD_DATA:
[2022-09-21 14:40:13.645]   > valid:               true
[2022-09-21 14:40:13.645]   > target_pba_main:     5f000380
[2022-09-21 14:40:13.645]   > target_pba_backup:   5f0003c0
[2022-09-21 14:40:13.645]   > stream:                 0

Read PF stream pba: 0x5f000380， blk 380, ce 0, lun 0, page 0, ch 14, die 14
Read PF stream pba: 0x5f000400， blk 380, ce 1, lun 0, page 0, ch 0, die 16
Read PF stream pba: 0x5f000480， blk 380, ce 1, lun 0, page 0, ch 2, die 18

pf_b2n_cpl_cb: cpl status: 0x0, pba 60ca66c0, stream: 0

[2022-09-21 14:40:13.700] [6-1970:1:1|0:0:0:807]    U0018929_PARITY_REBUILD_DATA:
[2022-09-21 14:40:13.700]   > valid:               true
[2022-09-21 14:40:13.700]   > target_pba_main:     5f000500
[2022-09-21 14:40:13.700]   > target_pba_backup:   5f000540
[2022-09-21 14:40:13.700]   > stream:                 7
Read PF stream pba: 0x5f000500
pf_b2n_cpl_cb: cpl status: 0x0, pba 7b03f8c0, stream: 7
```

```
FTL recovery
[2022-09-21 14:40:15.858] [6-1970:1:1|0:0:0:953]    U0018929_L1_TABLE_RCV_GET_SBK:
[2022-09-21 14:40:15.858]   > current_read_sblock:  1d4
[2022-09-21 14:40:15.858]   > block_status:           1
[2022-09-21 14:40:15.858]   > early_switch:           0

```

```
patch
[2022-09-21 14:41:59.227] [6-1970:1:1|0:0:0:171]    U0018929_RESTORE_L2P_PATCH_MT_D:
[2022-09-21 14:41:59.227]   > patch_start_pba:     2030007b04b000
[2022-09-21 14:41:59.227]   > buffer_cnt:             4

[2022-09-21 14:40:15.919] [6-1970:1:1|0:0:0:953]    U0018929_PATCH_PARSE_START:
[2022-09-21 14:40:15.919]   > mode:                   1

[2022-09-21 14:40:15.919] [6-1970:1:1|0:0:0:953]    U0018929_SCAN_PATCH_READ_CDT_SBK:
[2022-09-21 14:40:15.919]   > sblock:               1ec
[2022-09-21 14:40:15.919]   > open_seq_num:         ff4

[2022-09-21 14:40:15.919] [6-1970:1:1|0:0:0:953]    U0018929_PATCH_LOAD_DISPATCH:
[2022-09-21 14:40:15.919]   > current_read_sblock:  1ec
[2022-09-21 14:40:15.919]   > patch_parse_mode:       1
[2022-09-21 14:40:15.919]   > start_read_pba:      2030007b007800
[2022-09-21 14:40:15.920]   > end_read_pba:        7b040000
[2022-09-21 14:40:15.920]   > patch_start_pba:     2030007b007800

[2022-09-21 14:40:16.329] [6-1970:1:1|0:1:0:115]    U0018929_DISPATCH_PATCH_IN_PF_MT:
[2022-09-21 14:40:16.329]   > buffer_cnt:             4

[2022-09-21 14:40:16.446] [6-1970:1:1|0:1:2:929]    U0018929_L2P_RECOVERY:
[2022-09-21 14:40:16.446]   > _l2_alloc_for_recovery:   1bf1f8
[2022-09-21 14:40:16.446]   > _full_cache_flag:    true
[2022-09-21 14:40:16.446]   > _format_capacity:         f00
[2022-09-21 14:40:16.460]
[2022-09-21 14:40:19.306]
[2022-09-21 14:40:19.306] [6-1970:1:1|0:1:8:256]    U0018929_MPAGE_LOAD_COMPLETE:
[2022-09-21 14:40:19.367]   > mpage_load_success:  true
[2022-09-21 14:40:19.367]
[2022-09-21 14:40:19.367]
[2022-09-21 14:40:19.367] [6-1970:1:1|0:1:8:256]    U0018929_PATCH_PARSE_START:
[2022-09-21 14:40:19.367]   > mode:                   0
[2022-09-21 14:40:19.367]
[2022-09-21 14:40:19.367]
[2022-09-21 14:40:19.367] [6-1970:1:1|0:1:8:256]    U0018929_PATCH_LOAD_DISPATCH:
[2022-09-21 14:40:19.367]   > current_read_sblock:  1ec
[2022-09-21 14:40:19.367]   > patch_parse_mode:       0
[2022-09-21 14:40:19.368]   > start_read_pba:      2030007b007800
[2022-09-21 14:40:19.368]   > end_read_pba:        7b040000
[2022-09-21 14:40:19.368]   > patch_start_pba:     2030007b007800
[2022-09-21 14:40:19.368]
[2022-09-21 14:40:19.744]
[2022-09-21 14:40:19.744] [6-1970:1:1|0:1:8:630]    U0018929_DISPATCH_PATCH_IN_PF_MT:
[2022-09-21 14:40:19.744]   > buffer_cnt:             4

```

```
cold stream 每次需要从一个parity group的开始去收集数据，所以为了保证逻辑的统一性
所以需要对于hot stream也是这样
padding到每个parity group的边界
而且为了parity来做raid  rebuild方便

对于user data在pf block上是没有parity XOR的保护
对于meta data和summary data，在pf block上是有parity XOR的保护
因此，user data和parity data存在和meta data和summary data在不同的parity die上
所以第一次parity padding是RAID BYPASS到parity的边界，然后meta data和summary data从下一个parity的开始写起，最后的parity padding是带有XOR

那么为什么user data和parity data在pf blk上无法使用parity XOR进行保护？
因为user data和parity data是从vqid拷贝过来的，所以没有parity buf的XOR保护，这个只能用双拷贝和本身的位置的raid rebuild进行保护

一个patch有256个entry
因为每次刷4K的patch，每个entry是16字节，所以entry的个数是4096/16=256

patch每次都是按照16k，即一个plane去读，但是按4k来解析
```

```c++
    // call panic
    #if __BL3__ == 1
    // multiple core may assert at same time
    if (g_assert_handler.get_assert_trylock()) {
        // panic dump only dump once
        do_panic_dump();

        // show all diagnosis info
        system_diagnosis("asserted");

        // print again
        print_assert_info();

        EVT_LOG_FORCE_FLUSH(handle_assert_evtlog, coreid, id, cond, fileln, (char*)&g_assert_text[coreid]);
        // we do not release this lock, so make sure panic dump only do once
    }
    #endif
```

PF 中断的优先级

```c++
#define ASIC_IRQ_PROPS(grp)                                                                   \
    INTR_PROP_DESC(IRQ_UART0, GIC_PRIORITY(0x8), (grp), GIC_INTR_CFG_LEVEL),                  \
        INTR_PROP_DESC(IRQ_UART1, GIC_PRIORITY(0x8), (grp), GIC_INTR_CFG_LEVEL),              \
        INTR_PROP_DESC(IRQ_POWER_FAIL, GIC_PRIORITY(0x0), (grp), GIC_INTR_CFG_EDGE),          \
        INTR_PROP_DESC(IRQ_FIS_EDGE0, GIC_PRIORITY(0x8), (grp), GIC_INTR_CFG_EDGE),           \
        INTR_PROP_DESC(IRQ_FIS_EDGE1, GIC_PRIORITY(8), (grp), GIC_INTR_CFG_EDGE),             \
        INTR_PROP_DESC(IRQ_FIS_EDGE2, GIC_PRIORITY(8), (grp), GIC_INTR_CFG_EDGE),             \
        INTR_PROP_DESC(IRQ_FIS_EDGE3, GIC_PRIORITY(8), (grp), GIC_INTR_CFG_EDGE),             \
        INTR_PROP_DESC(IRQ_FIS_EDGE4, GIC_PRIORITY(8), (grp), GIC_INTR_CFG_EDGE),             \
        INTR_PROP_DESC(IRQ_FIS_EDGE5, GIC_PRIORITY(8), (grp), GIC_INTR_CFG_EDGE),             \
        INTR_PROP_DESC(IRQ_FIS_EDGE6, GIC_PRIORITY(8), (grp), GIC_INTR_CFG_EDGE),             \
        INTR_PROP_DESC(IRQ_FIS_EDGE7, GIC_PRIORITY(8), (grp), GIC_INTR_CFG_EDGE),             \
        INTR_PROP_DESC(IRQ_FIS_LEVEL3, GIC_PRIORITY(8), (grp), GIC_INTR_CFG_EDGE),            \
        INTR_PROP_DESC(IRQ_FIS_LEVEL9, GIC_PRIORITY(8), (grp), GIC_INTR_CFG_LEVEL),           \
        INTR_PROP_DESC(IRQ_FIS_LEVEL12, GIC_PRIORITY(8), (grp), GIC_INTR_CFG_LEVEL),          \
        INTR_PROP_DESC(IRQ_FIS_LEVEL13, GIC_PRIORITY(8), (grp), GIC_INTR_CFG_LEVEL),          \
        INTR_PROP_DESC(IRQ_DCS_TOP_LEVEL_INTR8, GIC_PRIORITY(0), (grp), GIC_INTR_CFG_LEVEL),  \
        INTR_PROP_DESC(IRQ_DCS_TOP_LEVEL_INTR9, GIC_PRIORITY(0), (grp), GIC_INTR_CFG_LEVEL),  \
        INTR_PROP_DESC(IRQ_DCS_TOP_LEVEL_INTR10, GIC_PRIORITY(0), (grp), GIC_INTR_CFG_LEVEL), \
        INTR_PROP_DESC(IRQ_DCS_TOP_LEVEL_INTR11, GIC_PRIORITY(0), (grp), GIC_INTR_CFG_LEVEL), \
        INTR_PROP_DESC(IRQ_DCS_TOP_LEVEL_INTR12, GIC_PRIORITY(0), (grp), GIC_INTR_CFG_LEVEL), \
        INTR_PROP_DESC(IRQ_DCS_TOP_LEVEL_INTR13, GIC_PRIORITY(0), (grp), GIC_INTR_CFG_LEVEL), \
        INTR_PROP_DESC(IRQ_DCS_TOP_LEVEL_INTR14, GIC_PRIORITY(0), (grp), GIC_INTR_CFG_LEVEL), \
        INTR_PROP_DESC(IRQ_DCS_TOP_LEVEL_INTR15, GIC_PRIORITY(0), (grp), GIC_INTR_CFG_LEVEL)
```

目前dump 1m的数据到nand差不多需要1ms

hw abort产生的user data数据可能最多达到20m，也就是说可能最多需要20ms来刷到nand上