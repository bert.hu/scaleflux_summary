```c++
UD2214C0023M@CSDU5SPC38M1:/$ l2pdbg 1
init state: 6, init_idx 0x0
2K_MPAGE 0, RAW_CAPACITY 0x40000000000, DDR_SIZE 17179869184, provision_capacity 3840, format_capacity 3840, cache ratio 100, MAX_LBA 0x1bf1f72af, MAX_LPA 0x37e3ee55
L1_MAP_ENTRY_NUM 0x1bf1f8, L2_MAP_ENTRY_NUM 0x1bf1f8, max_cache_l2_num 0x37e3ea, MAX_CACHE_L2_MAP_ENTRY_NUM 0x3a71f0, DEFAULT_L1_MAP_ENTRY_NUM 0x1bf1f8, MAX_L1_MAP_ENTRY_NUM 0x37e3ea, MAX_DIRTY_LIa
DDR_L2P_LOCK_SIZE 0xdf8fa8, DDR_L1_MAP_SIZE 0x1bf2000, DDR_L2_MAP_SIZE 0x3a71f0000, DDR_L2_NODE_SIZE 0x37e4000, ALLOC_L2_MAP_SIZE 0x3a71f0000
ptr: lock 0x43c921000, l1_map 0x43dc9a000, l2_map 0x458e10000, l2_node 0x43f88c000

  
  MAX_LBA 0x1bf1f72af
  MAX_LPA 0x37e3ee55
  MAX_LBA = 8 * MAX_LPA
  LPA = LBA / 8
  
  MAX_LPA 0x37e3ee55
  MAX_LPA_NUM = MAX_LPA + 1 = 0x37E3EE56
  
  
  lba 0  --->  lpa 0
               lpa 1
               lpa 2
               lpa 3
               lpa 4
               lpa 5
               lpa 6
               lpa 7
  lba 1  --->  lpa 0
               lpa 1
               lpa 2
               lpa 3
               lpa 4
               lpa 5
               lpa 6
               lpa 7  
  
  load summary的时候，每次读一个4k，每个4k中有NUM_SUMMARY_PER_4K个mpage_summary
  读到一个mpage_summary，就更新
  type = REGULAR_SUMMARY
  l1_id
  pba
  rsv = SUMMARY_SIGNATURE
  
  从sealed blk和open blk上读取mpage_summary的主要目的是为了恢复l1_map,用l1_id和pba来关联
  即l1_map[l1_id]=mpage_pba的关系
  
  
  每个patch id都有自己的recovery window
  patch id下面会挂很多mpage的l2_id
  每插入一个到这个patch id的dirty list，则_window[patch_id].dirty_cnt.atomic_inc()
  用_l1_map[l1_id].in_dirty_list来表示这个mpage是不是在dirty list里面
  
  可以通过l1_id获取到这个group_id，group_id=(l1_id / L1_NUM_OF_GRANULARITY) % L2P_PATCH_GROUP_NUM=(l1_id / 64) % 4
  然后将l1_id加到这个group_id的g_dirty_list_array[group_id].append(l1_id)
  
  flush mpage的时候每次flush的粒度是4k
  
```

```
BIT_SET(_dirty_mpage_bitmap[l1_id / 64], l1_id % 64);

void l2p_patch_mgr::init_dirty_mpage_bitmap() { memset(_dirty_mpage_bitmap, 0x1, _dirty_mpage_bitmap_size); }

if (_patch_parse_mode == PATCH_PARSE_MODE_RECOVER_L2P)

L2P_PATCH_WRITE_ENTRY
l2p_mark_dirty(l2_id, patch_id);
insert_to_dirty_list(l2_id, patch_id);
recovery_window::insert_dirty_node(u32 l1_id)

g_l2p_mgr._full_cache_flag


void patch_load_dispatch_evtlog::gen_log(void *, u32 _current_read_sblock, u32 _patch_parse_mode, u64 _start_read_pba,
                                         u64 _end_read_pba, u64 _patch_start_pba)
{
    current_read_sblock = _current_read_sblock;
    patch_parse_mode    = _patch_parse_mode;
    start_read_pba      = _start_read_pba;
    end_read_pba        = _end_read_pba;
    patch_start_pba     = _patch_start_pba;
}

handle_exceptions(cpu_context_t *cpu_ctxt __unused)
handle_assert_error(id)
system_diagnosis
assert_panic_body(ASSERT_ID_E id, bool isinject, char* cond, char* fileln, char* fmt, ...)
```

```
原始数据
====== Recovery stage timecost: ======
RECOVERY total timecost: 11998 ms
RECOVERY_PF_RESTORE timecost: 48 ms
RECOVERY_FTL_RECOVERY timecost: 11798 ms
 ====== PF Restore stage timecost: ======
 LOW_LEVEL_STATE_READ_ROOT_DATA, timecost: 1 ms.
 LOW_LEVEL_STATE_READ_SUMMARY_DATA, timecost: 0 ms.
 LOW_LEVEL_STATE_READ_META_DATA, timecost: 10 ms.
 LOW_LEVEL_STATE_DATA_COPY_BACK, timecost: 17 ms.
 LOW_LEVEL_STATE_PARITY_REBUILD, timecost: 1 ms.
 LOW_LEVEL_STATE_RESTORE_CLEAN_UP, timecost: 8 ms.
====== FTL recovery stage timecost: ======
ftl_state: 1, timecost: 1 ms.
ftl_state: 2, timecost: 564 ms.
ftl_state: 3, timecost: 10849 ms.
ftl_state: 4, timecost: 564 ms.
ftl_state: 5, timecost: 3 ms.

====== Recovery stage timecost: ======
RECOVERY total timecost: 11666 ms
RECOVERY_PF_RESTORE timecost: 48 ms
RECOVERY_FTL_RECOVERY timecost: 10625 ms
 ====== PF Restore stage timecost: ======
 LOW_LEVEL_STATE_READ_ROOT_DATA, timecost: 3 ms.
 LOW_LEVEL_STATE_READ_SUMMARY_DATA, timecost: 0 ms.
 LOW_LEVEL_STATE_READ_META_DATA, timecost: 10 ms.
 LOW_LEVEL_STATE_DATA_COPY_BACK, timecost: 15 ms.
 LOW_LEVEL_STATE_PARITY_REBUILD, timecost: 1 ms.
 LOW_LEVEL_STATE_RESTORE_CLEAN_UP, timecost: 8 ms.
====== FTL recovery stage timecost: ======
ftl_state: 1, timecost: 840 ms.
ftl_state: 2, timecost: 325 ms.
ftl_state: 3, timecost: 10078 ms.
ftl_state: 4, timecost: 325 ms.
ftl_state: 5, timecost: 2 ms.

====== Recovery stage timecost: ======
RECOVERY total timecost: 12130 ms
RECOVERY_PF_RESTORE timecost: 54 ms
RECOVERY_FTL_RECOVERY timecost: 10591 ms
 ====== PF Restore stage timecost: ======
 LOW_LEVEL_STATE_READ_ROOT_DATA, timecost: 4 ms.
 LOW_LEVEL_STATE_READ_SUMMARY_DATA, timecost: 0 ms.
 LOW_LEVEL_STATE_READ_META_DATA, timecost: 10 ms.
 LOW_LEVEL_STATE_DATA_COPY_BACK, timecost: 16 ms.
 LOW_LEVEL_STATE_PARITY_REBUILD, timecost: 1 ms.
 LOW_LEVEL_STATE_RESTORE_CLEAN_UP, timecost: 8 ms.
====== FTL recovery stage timecost: ======
ftl_state: 1, timecost: 1334 ms.
ftl_state: 2, timecost: 610 ms.
ftl_state: 3, timecost: 9566 ms.
ftl_state: 4, timecost: 610 ms.
ftl_state: 5, timecost: 4 ms.


====== Recovery stage timecost: ======
RECOVERY total timecost: 12914 ms
RECOVERY_PF_RESTORE timecost: 65 ms
RECOVERY_FTL_RECOVERY timecost: 11913 ms
 ====== PF Restore stage timecost: ======
 LOW_LEVEL_STATE_READ_ROOT_DATA, timecost: 8 ms.
 LOW_LEVEL_STATE_READ_SUMMARY_DATA, timecost: 0 ms.
 LOW_LEVEL_STATE_READ_META_DATA, timecost: 12 ms.
 LOW_LEVEL_STATE_DATA_COPY_BACK, timecost: 17 ms.
 LOW_LEVEL_STATE_PARITY_REBUILD, timecost: 2 ms.
 LOW_LEVEL_STATE_RESTORE_CLEAN_UP, timecost: 8 ms.
====== FTL recovery stage timecost: ======
ftl_state: 1, timecost: 788 ms.
ftl_state: 2, timecost: 611 ms.
ftl_state: 3, timecost: 10885 ms.
ftl_state: 4, timecost: 611 ms.
ftl_state: 5, timecost: 8 ms.

```

```
优化后
====== Recovery stage timecost: ======
RECOVERY total timecost: 12152 ms
RECOVERY_PF_RESTORE timecost: 118 ms
RECOVERY_FTL_RECOVERY timecost: 11053 ms
 ====== PF Restore stage timecost: ======
 LOW_LEVEL_STATE_READ_ROOT_DATA, timecost: 16 ms.
 LOW_LEVEL_STATE_READ_SUMMARY_DATA, timecost: 0 ms.
 LOW_LEVEL_STATE_READ_META_DATA, timecost: 15 ms.
 LOW_LEVEL_STATE_DATA_COPY_BACK, timecost: 17 ms.
 LOW_LEVEL_STATE_PARITY_REBUILD, timecost: 4 ms.
 LOW_LEVEL_STATE_RESTORE_CLEAN_UP, timecost: 56 ms.
====== FTL recovery stage timecost: ======
ftl_state: 1, timecost: 829 ms.
ftl_state: 3, timecost: 10551 ms.
ftl_state: 4, timecost: 499 ms.
ftl_state: 5, timecost: 1 ms.


====== Recovery stage timecost: ======
RECOVERY total timecost: 11990 ms
RECOVERY_PF_RESTORE timecost: 113 ms
RECOVERY_FTL_RECOVERY timecost: 10895 ms
 ====== PF Restore stage timecost: ======
 LOW_LEVEL_STATE_READ_ROOT_DATA, timecost: 6 ms.
 LOW_LEVEL_STATE_READ_SUMMARY_DATA, timecost: 0 ms.
 LOW_LEVEL_STATE_READ_META_DATA, timecost: 15 ms.
 LOW_LEVEL_STATE_DATA_COPY_BACK, timecost: 19 ms.
 LOW_LEVEL_STATE_PARITY_REBUILD, timecost: 3 ms.
 LOW_LEVEL_STATE_RESTORE_CLEAN_UP, timecost: 57 ms.
====== FTL recovery stage timecost: ======
ftl_state: 1, timecost: 832 ms.
ftl_state: 3, timecost: 10393 ms.
ftl_state: 4, timecost: 498 ms.
ftl_state: 5, timecost: 3 ms.
```

0x300000890000