#### pf_w2p_submit

```
fct_alloc_entries_idx(FCT_POOL_W2P_META_DATA, &fct_idx, 1)
fct_entry = fct_get_entry(FCT_POOL_W2P_META_DATA, fct_idx)
fw_w2p_cmd::send_noncomprs_wr_cmd(fct_entry, (FCT_POOL_W2P_META_DATA << 7) + fct_idx, stream, (u64)write_buf, lpa, FCQ_IO_PF)
_sent_w2p_cnt++
```

#### pf_raw_write_w2p_submit

```
fct_alloc_entries_idx(FCT_POOL_W2P_META_DATA, &fct_idx, 1)
fct_entry = fct_get_entry(FCT_POOL_W2P_META_DATA, fct_idx)
fw_w2p_cmd::send_raw_wr_cmd(fct_entry, (FCT_POOL_W2P_META_DATA << 7) + fct_idx, stream, (u64)write_buf, FCQ_IO_PF)
_sent_w2p_cnt++
```

#### pf_page_in_w2p_submit

```
fct_alloc_entries_idx(FCT_POOL_W2P_META_DATA, &fct_idx, 1)
fct_entry = fct_get_entry(FCT_POOL_W2P_META_DATA, fct_idx)
fw_w2p_cmd::send_w2p_page_in_cmd(fct_entry, ((FCT_POOL_W2P_META_DATA << 7) | fct_idx), stream, (u64)(write_buf), FCQ_IO_PF)
_sent_w2p_cnt++
```

#### pf_vqid_b2n_submit

```
u8    pf_stream = STREAM_ID_PF
u32   pool_id   = g_b2n_pool_id_of_stream[pf_stream]
u32   qid       = g_b2n_qid_of_stream[pf_stream]
fct_alloc_entries_idx(pool_id, &fct_idx, 1)
fct_entry = fct_get_entry(pool_id, fct_idx)
fw_b2n_cmd::send_slc_vqid_b2n_cmd(fct_entry, (pool_id << 7) + fct_idx, b2n_pba, pf_stream, qid, bd_pl_mask, RAID_BYPASS, vqid)
_sent_b2n_cnt++
```

#### pf_normal_b2n_submit

```
stB2nParams.bad_pl_mask = g_pu_mgr.bb_mgr.get_ebb_info(pba)
b2n_send_b2n_cmd(stB2nParams)
_sent_b2n_cnt++
```

#### pf_raw_write_b2n_submit

```
stB2nParams.bad_pl_mask = g_pu_mgr.bb_mgr.get_ebb_info(pba)
b2n_send_b2n_cmd(stB2nParams)
_sent_b2n_cnt++
```

#### pf_page_in_b2n_submit

```
u8    bad_plane_mask = DEFAULT_BAD_PLANE_MASK
u32   qid            = g_b2n_qid_of_stream[stream]
fct_alloc_entries_idx(FCT_POOL_B2N, &fct_idx, 1)
fct_entry = fct_get_entry(FCT_POOL_B2N, fct_idx)
fw_b2n_cmd::send_b2n_cmd(fct_entry, ((FCT_POOL_B2N << 7) | fct_idx),
                                        pba,// PBA field is not required because data                                                won't be sent to NAND
                                        stream, qid,
                                        slc,  // slc mode
                                        false,// gts mode
                                        bad_plane_mask, RAID_PAGE_IN)
_sent_b2n_cnt++
```

#### pf_read_cmd_submit

```
u64   lba[CFG_LUN_PAGE_SIZE]
for (u32 index = 0; index < CFG_LUN_PAGE_SIZE; index++) {
    lba[index] = PF_LBA; // 每个lba都是以4K为粒度
}

u32 bad_pl_cnt  = bit_count(bad_plane_mask);
u32 read_size   = (CFG_LUN_PAGE_SIZE - (CFG_FOURK_PER_PLANE * bad_pl_cnt)) % 32;
u64 lp_bitmap   = N_BIT_MASK(read_size);
u8  nxs         = cal_nxs(scheme, lp_bitmap, bad_plane_mask);

fct_alloc_entries_idx(FCT_POOL_FLASH_RD, &fct_idx, 1)
fct_entry = fct_get_entry(FCT_POOL_FLASH_RD, fct_idx)
fw_flash_read_cmd::read_slc_page_by_pba_to_buf(fct_entry, (FCT_POOL_FLASH_RD << 7) + fct_idx, FCQ_IO_PF, (u64)read_buf, pba, lp_bitmap, lba, scheme, nxs, bad_plane_mask)

fw_flash_read_cmd::read_tlc_page_by_pba_to_buf(fct_entry, (FCT_POOL_FLASH_RD << 7) + fct_idx, FCQ_IO_PF, (u64)read_buf, pba, lp_bitmap, lba, scheme, nxs, bad_plane_mask)

_sent_read_cnt++

```

#### 处理发的cmd数量与接受的cmd数量不对等的情况

```c++
bool pf_mgr::is_all_pending_write_cmd_cpl()
{
    bool ret = false;

    // pf pending command including b2n and w2p commands
    if ((_sent_b2n_cnt == _received_b2n_cnt) && (_sent_w2p_cnt == _received_w2p_cnt)) {
        DBG_LOG("All write commands are completed: B2N sent: %d, B2N received: %d, W2P sent: %d, W2P received: %d\n", _sent_b2n_cnt, _received_b2n_cnt, _sent_w2p_cnt, _received_w2p_cnt);
        ret = true;
    }

    return ret;
}

void pf_mgr::process_write_pending(bool pf_stream_only)
{
    u32 received_cnt = 0;
    // process outstanding b2n commands
    while (_sent_b2n_cnt > _received_b2n_cnt) {
        if (pf_stream_only) {
            received_cnt = be_poll_cpl(FCQ_B2N_PF, pf_b2n_cpl_cb);
        }
        else {
            received_cnt = 0;
            for (u32 stream = 0; stream <= STREAM_ID_L2P_PATCH; stream++) {
                received_cnt += be_poll_cpl(g_b2n_qid_of_stream[stream], pf_b2n_cpl_cb);
            }
        }

        _received_b2n_cnt += received_cnt;
        if (received_cnt == 0) {
            break;
        }
    }

    // process outstanding w2p commands
    while (_sent_w2p_cnt > _received_w2p_cnt) {
        received_cnt = be_poll_cpl(FCQ_IO_PF, pf_w2p_cpl_cb);

        _received_w2p_cnt += received_cnt;
        if (received_cnt == 0) {
            break;
        }
    }
}

bool pf_mgr::is_all_pending_read_cmd_cpl()
{
    bool ret = false;

    if (_sent_read_cnt == _received_read_cnt) {
        DBG_LOG("All read commands are completed: Read sent: %d, Read received: %d\n", _sent_read_cnt,
                _received_read_cnt);
        ret = true;
    }

    return ret;
}

u32 pf_mgr::process_read_pending(bool expect_erased, bool retry_only)
{
    u32 read_status = PF_NAND_READ_STILL_POLLING;
    u32 fct_idx = INVALID_32BIT;

    if (expect_erased) {
        fct_idx = poll_single_read_cpl(FCQ_IO_PF, EH_ACT_DISABLE);
    }
    else if (retry_only) {
        fct_idx = poll_single_read_cpl(FCQ_IO_PF, EH_ACT_RETRY);
    }
    else {
        fct_idx = poll_single_read_cpl(FCQ_IO_PF, (EH_ACT_RETRY | EH_ACT_REBUILD));
    }

    if (fct_idx != INVALID_32BIT) {
        cmd_cpl cmd_status;
        cmd_status.cpl_val = pf_read_cpl_cb(fct_idx);

        if (0 != cmd_status.cpl_rd.erase_page) {
            read_status = PF_NAND_READ_ERASED;
        }
        // else if ((0xC != (cmd_status.cpl_rd.nand_status_7_4 & 0xD)) ||
        else if ((0 != (cmd_status.cpl_rd.overall_status)) || (0 != cmd_status.cpl_rd.zero_page) ||
                 (0 != cmd_status.cpl_rd.crc)) {
            read_status = PF_NAND_READ_FAILED;
        }
        else {
            read_status = PF_NAND_READ_SUCCESS;
        }

        _received_read_cnt++;
    }

    return read_status;
}
```

