```
    g_pf_mgr.prepare_footer_meta_data(); // The data of blk mgr needs to be updated, so it is placed in the front
    g_get_sblk_lock.spin_lock();//sync with b2n get sblock
    g_pf_mgr.prepare_blk_mgr_meta_data();
    g_pf_mgr.prepare_l2p_meta_data();
    g_get_sblk_lock.spin_unlock();
    g_pf_mgr.prepare_trim_bmp();
    g_pf_mgr.prepare_ns_bmp();
    g_pf_mgr.prepare_l2p_patch_meta_data();
    g_pf_mgr.prepare_rdc_array_data();
    g_pf_mgr.prepare_nvme_array_data();
    g_pf_mgr.prepare_ns_comp_array_data();
    g_pf_mgr.prepare_ns_user_array_data();
    g_pf_mgr.prepare_ns_cfg_array_data();
    g_pf_mgr.prepare_gsd_count_array_data();
    g_pf_mgr.prepare_time_stamp_array_data();
	g_pf_mgr.prepare_tcg_meta_data();
    g_pf_mgr.prepare_gts_meta_data();
    g_pf_mgr.prepare_nvme_vpd_array_data();
    g_pf_mgr.prepare_fw_statistic_meta_data();
    g_pf_mgr.prepare_life_meter_meta_data();
    g_pf_mgr.prepare_throttle_meta_data();
    g_pf_mgr.prepare_folding_feed_data();
    g_pf_mgr.prepare_recent_pf_log_data();
    g_pf_mgr.prepare_read_scrub_data();
    g_pf_mgr.prepare_nvme_self_test_array_data();
    g_pf_mgr.prepare_rmw_buf();
```

```c++
void l2p_mgr::config_prepare_free_pool_for_recovery()
{
    _init_idx = _l2_alloc_for_recovery;
    g_ckp_task.set_scan_l2_id(_l2_alloc_for_recovery);
    if (_full_cache_flag) {
        g_rsv_gdma_jobid = sys_gdma_memset((void *)DDR_RSV_START, INVALID_PBA, DDR_RSV_SIZE);

        _gdma_jobid      = sys_gdma_memset((void *)get_l2_map(_l2_alloc_for_recovery), INVALID_PBA,
                                           ALLOC_L2_MAP_SIZE - _l2_alloc_for_recovery * L2_MAP_ENTRY_SIZE);
    }
    else {
        _gdma_jobid = sys_gdma_memset((void *)get_l2_map(_l2_alloc_for_recovery), INVALID_PBA,
                                      (_max_cache_l2_num - _l2_alloc_for_recovery) * L2_MAP_ENTRY_SIZE);
    }
    BOMB(GDMA_JOB_ERR, _gdma_jobid != SYS_GDMA_INVALID_JOB);

    // INF_LOG("%s: _l2_alloc_for_recovery %u, _full_cache_flag %u\n", __FUNCTION__, _l2_alloc_for_recovery,
    // _full_cache_flag);
    EVT_LOG(l2p_recovery_evtlog, _l2_alloc_for_recovery);
}

// only part of l2 will be used during recovery, need init those left to invalid state
bool l2p_mgr::prepare_free_pool_for_recovery()
{
    u64 time_start = bsp_platform::get_time_us();
    u64 time_end;

    for (u32 i = _init_idx; i < _max_cache_l2_num; i++) {
        _l2_node[i].state = L2P_STATE_INVALID;
        time_end          = bsp_platform::get_time_us();
        if (__unlikely(i == (_max_cache_l2_num - 1))) {
            _init_idx = i + 1;
        }
        else if ((time_end - time_start) > _tickcnt_limit_us) {//cstat !MISRAC2004-13.7_b !MISRAC++2008-0-1-2_b
            _init_idx = i + 1;
            return false;
        }
    }
    if (g_ckp_task.prepare_free_pool()) {
        return false;
    }

    if (!sys_gdma_check_and_release_job(_gdma_jobid) || (!sys_gdma_check_and_release_job(g_rsv_gdma_jobid))) {
        return false;
    }
    _init_idx = 0;

    DBG_LOG("%s: finish prepare pool\n", __FUNCTION__);
    return true;
}
```

```c++
u32 sys_gdma_memcpy(void *dest, const void *src, u64 n)
{
    return g_sys_gdma_task.submit_gdma_memcpy(dest, src, n);
}

bool sys_gdma_check_job_done(u32 jobid)
{
    return g_sys_gdma_task.check_gdma_done(jobid);
}

bool sys_gdma_release_job(u32 jobid)
{
    return g_sys_gdma_task.release_gdma_job(jobid);
}

bool sys_gdma_check_and_release_job(u32 jobid)
{
    return g_sys_gdma_task.check_and_release_gdma_job(jobid);
}
```

bsp_platform::gdma_copy(ch, (void *)dst_addr, (void *)src_addr, this_len)

```
    inline bool gdma_check_busy() { return (bsp_platform::gdma_get_ch_status(0) != 0); }
    inline void gdma_l2p_memset_invalid(u32 l2_id)
    {
        bsp_platform::gdma_copy(0, &_l2_map[l2_id * NUM_LPA_PER_4K_UNIT_MPAGE], _gdma_buf, L2_MAP_ENTRY_SIZE);
    }
```

扩容

## change cap through delete ns and create ns

### Step 1: delete ns

`sudo nvme delete-ns -n 1 /dev/nvme0`

### Step 2: create ns

| **Raw Cap** | **Target Map Unit** | **Target Formatted Cap** | **Command**                              |
| ----------- | ------------------- | ------------------------ | ---------------------------------------- |
| 8 TiB       | 8K                  | 15360 GB                 | sudo nvme create-ns -s 30005842608 -c 15002931888 -f 0 /dev/nvme0 |
|             | 16K                 | 23040 GB                 | sudo nvme create-ns -s 45008753328 -c 15002931888 -f 0 /dev/nvme0 |
| 4 TiB       | 8K                  | 11520 GB                 | sudo nvme create-ns -s 22504387248 -c 7501476528 -f 0 /dev/nvme0 |
|             | 16K                 | 23040 GB                 | sudo nvme create-ns -s 45008753328 -c 7501476528 -f 0 /dev/nvme0 |

### Step 3: attach ns

`sudo nvme attach-ns -n 1 /dev/nvme0`

### Step 4: ns-rescan

`sudo nvme ns-rescan /dev/nvme0`

### Map Unit Size Boundary

| **OPN**    | **4K upper bound** | **8K upper bound** | **16K upper bound**(Max Formatted Cap) |
| ---------- | ------------------ | ------------------ | -------------------------------------- |
| C3SIMU800G | 1089 GB            | 2179 GB            | 3320 GB                                |
| B47/Bics5  | 7680 GB            | 15360 GB           | 23040 GB                               |

```c++
if (__PF_DUMP_OPTIMIZE__ == 1) {
    task_array_element* pTaskArray = g_kernel.get_slave_sched(7)->get_task_array();
    for (u32 idx = 0; idx < MAX_TASK_NUMBER_IN_SCHEDULER; idx++) {
        if ((pTaskArray[idx]._ptask) != nullptr) {
            if ((pTaskArray[idx]._enabled_core == 7) && (pTaskArray[idx]._ptask != this)) {
                pTaskArray[idx]._ptask->inactive();
            }
        }
    }
}
```

