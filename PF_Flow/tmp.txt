说明：主机要从从设备读取数据时，主机先发送起始信号，再发送从机地址+读写位（AD+W），收到从机ACK后，主机发送要读取的寄存器地址，收到从机ACK后，因为要转换数据流方向，主机要重新发送起始信号，接着发送从机地址+读写位（AD+R），收到从机的ACK后，开始读取数据并发送ACK信号，数据接收完成后，主发送NACK信号和停止信号。

说明：主机要向从设备写入数据时，主机先发送起始信号，再发送从机地址+读写位（AD+W），收到从机ACK后，主机发送写入的寄存器地址，收到从机ACK后，主机发送要写入的数据，收到从机的ACK后，主机发送停止位。

空闲电平：SCL和SDA都是高电平；
起始信号：SCL高电平的时候，SDA从高拉低；
结束信号：SCL高电平的时候，SDA从低拉高；
ACK信号：在第9个时钟的时候，SDA为低电平；
NACK信号：在第9个时钟的时候，SDA为高电平；

I2C协议规定，总线上数据的传输必须以一个起始信号作为开始条件，以一个结束信号作为传输的停止条件。起始和结束信号总是由主设备产生。总线在空闲状态时，SCL和SDA都保持着高电平，当SCL为高电平时，SDA由高到低的跳变，则会产生一个起始条件；当SCL为高时，SDA由低到高的跳变，则会产生一个停止条件。
在起始条件产生后，总线处于忙状态，由本次数据传输的主从设备独占，其他I2C器件无法访问总线；而在停止条件产生后，本次数据传输的主从设备将释放总线，总线再次处于空闲状态，当然也可以不释放总线，可以直接选取下一个从设备并且开始下一次传输，最后在释放总线即可。



sudo nvme create-ns -s 15002931888 -c 15002931888 -f 0 /dev/nvme1

0x24000000419d
pba : 0x119d280






curl http://admin:validation@10.33.118.144/script.cgi?run=cycle_outlet3
9776 [4-2-2023:3:25|23:40:35:820]    trigger assert: effect:1, id:90
 2101 [4-2-2023:3:25|22:3:8:339]    trigger assert: effect:1, id:88
26834 [5-2-2023:3:25|22:45:26:127]    begin trigger power fail...


flashrd 0x92a290 0 raw tlc a^H ^H1
@:/$ parsepba 0x92a290

pba.all 0x92a290:

ch:       2

ce_lun:   1 // ce = 1, lun = 0

block:    4

page:     298

plane:    0

offset:   0x0

type:     1

ep_idx:   0
Gzz
ccp_off:  0x0

comp_len: 0x0

unc:      0

pba : 0x929280
@:/$ parsepba 0x929280

pba.all 0x929280:

ch:       2

ce_lun:   1

block:    4

page:     297

plane:    0

offset:   0x0

type:     0

ep_idx:   0

ccp_off:  0x0

comp_len: 0x0

unc:      0

​	

@:/$ parsepba 0x92a250

pba.all 0x92a250:

ch:       1

ce_lun:   1

block:    4

page:     298

plane:    0

offset:   0x0

type:     1

ep_idx:   0

ccp_off:  0x0

comp_len: 0x0

unc:      0

["bootloader", "utcase", "validation", "elog_parser", "compute", "unit_test", "myrtle_model", "v2", "x86_64", "bl1", "bl2", "fpga", "s5p6818", "utility", "iar", "docs", "rom", "obj"]
"bootloader", "utcase", "validation", "elog_parser", "compute", "unit_test", "myrtle_model", "v2", "x86_64", "bl1", "bl2", "asic", "s5p6818", "utility", "iar", "docs", "rom", "obj"],
                              'fpu_support'   : ["fplib"],
                              'cc_prefix'     : "aarch64-elf-",
                              'cc_opt'        : arm_baisc_flag + " -D__BL3__=1 -D__FPGA__=1 -D__ONE_CLUSTER__=1 ",
                              'as_opt'        : " -D__BL3__=1 -D__FPGA__=1 ",
                              'ld'            : "ld",
                              'lds_src'       : "./platform/aarch64/bl3/bl3.lds.pre",
                              'lds_file'      : "bl3.lds",
                              'ld_flag'       : arm_ld_flag + " -T"
                           },


                              'fpu_support'   : ["fplib"],
                              'cc_prefix'     : "aarch64-elf-",
                              'cc_opt'        : arm_baisc_flag + " -D__BL3__=1 -D__ASIC__=1 -D__NOR_BOOT_ROM__=1",
                              'as_opt'        : " -D__BL3__=1 -D__ASIC__=1 ",
                              'ld'            : "ld",
                              'lds_src'       : "./platform/aarch64/bl3/bl3.lds.pre",
                              'lds_file'      : "bl3.lds",
                              'ld_flag'       : arm_ld_flag + " -T"
                           },
http://192.168.3.3/



172e2b8d7ba46f7035eb6f2fda81a3e5c15630d9 出错 U0021383_FWVER


PF
pe 3
43
180
211
237
256
289
1044
1295
2628
2715
2756
3338
3697
0
0
0
[0]:2
[1]:126
[2]:8
[3]:62
[4]:47
[5]:7
[6]:977
[7]:7
[8]:1
[9]:1
[10]:1
[11]:0
[12]:0
[13]:2
[14]:1
[15]:1
[16]:2
[17]:0
[18]:22
[19]:1
[20]:32
[21]:0
[22]:1
[23]:0

0x684104f81886f

354
1518
5396
5427
7443
7745
8211
8686
10019
10107
10148
10734
11138

sudo python3 ./sfx-recovery.py --cmd "updateNorImg /dev/nvme0 file=/home/tcn/bl2.bin image=0"

pr_dbg("user:%d\n", data_flusher._user_date_b2n_cnt);
pr_dbg("parity:%d\n", data_flusher._parity_date_b2n_cnt);
pr_dbg("padding0:%d\n", _pf_dump_padding_b2n_cnt[0]);
pr_dbg("meta:%d\n", data_flusher._meta_data_b2n_cnt);
pr_dbg("summary:%d\n", data_flusher._summary_data_b2n_cnt);
pr_dbg("padding1:%d\n", _pf_dump_padding_b2n_cnt[1]);

OPN of FW image: CSDU5SPC38M1
WARN:fw_img_valid_check_364:RSA verify fail 
nordisrsa

e2f3a7dd57925a24a9731ddd0abd0de7af4617b1