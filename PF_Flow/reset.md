```
7122678 [6-1970:1:10|19:28:43:213]    U3019036_RECOVERY:
7122679   > _recovery_mode:         3
7122680   > _pre_state:            1c
7122681   > _state:                 1
7122682   > _basetime:           36eb02a45
7122683
7122684
7122685 [6-1970:1:10|19:28:43:213]    U3019036_RECOVERY_CNT:
7122686   > _recover_active:        0
7122687   > _first_enter_recovery:    0
7122688   > _pf_cnt:               4b
7122689   > _gsd_cnt:              32
7122690   > _has_ongoing_se:        0
7122691   > _change_cap_type:    CHANGE_CAP_TYPE_INVALID
7122692   > _change_cap_job_obj: CHANGE_CAP_OBJ_PROVISION
7122693   > _change_cap_job_caller: CHANGE_CAP_CALLER_DEFAULT
7122694   > _change_cap_job_cap_in_gb:        0
7122695
7122696
7122697 [6-1970:1:10|19:28:43:213]    U3019036_RECOVERY_CSTS_READY:
7122698   > _stage:                 0
7122699
7122700
7122701 [6-1970:1:10|19:28:43:213]    U3019036_RECOVERY_CSTS_READY:
7122702   > _stage:                 1
7122703
7122704
7122705 [6-1970:1:10|19:28:43:213]    U3019036_RECOVERY:
7122706   > _recovery_mode:         3
7122707   > _pre_state:            1c
7122708   > _state:                 0
7122709   > _basetime:           36eb02a45
7122710
7122711
7122712 [4-1970:1:10|19:28:43:214]    U3019036_SMPSTATE_CHANGE:
7122713   > _src_state:          SMP_STATE_IDLE
7122714   > _tgt_state:          SMP_STATE_RECOVERY
7122715   > _stage:              SMP_SWITCH_STAGE_FINISHED
7122716
7122717
7122718 [4-1970:1:10|19:28:43:214]    U3019036_SMPSTATE_CHANGE:
7122719   > _src_state:          SMP_STATE_RECOVERY
7122720   > _tgt_state:          SMP_STATE_RUNNING
7122721   > _stage:              SMP_SWITCH_STAGE_START

```

```
7122604 [4-1970:1:10|15:33:55:565]    U3019036_IDLE_PLL_STATE:
7122605   > _before_cpu_pll_clk:      5dc
7122606   > _before_sys_pll_clk:      8ca
7122607   > _cur_cpu_pll_clk:          19
7122608   > _cur_sys_pll_clk:          19
7122609   > _start_time:         26ffb4e7
7122610   > _end_time:           26ffb5cf
7122611
7122612
7122613 [4-1970:1:10|15:33:55:573]    U3019036_SMPSTATE_CHANGE:
7122614   > _src_state:          SMP_STATE_RUNNING
7122615   > _tgt_state:          SMP_STATE_IDLE
7122616   > _stage:              SMP_SWITCH_STAGE_FINISHED
7122617
7122618
7122619 [4-1970:1:10|19:28:43:109]    U3019036_NFE_RESET:
7122620   > _type:                  2
7122621   > _stage:                 1
7122622
7122623
7122624 [4-1970:1:10|19:28:43:212]    U3019036_NFE_RESET:
7122625   > _type:                  1
7122626   > _stage:                 1
7122627
7122628
7122629 [4-1970:1:10|19:28:43:212]    U3019036_SMPSTATE_CHANGE:
7122630   > _src_state:          SMP_STATE_IDLE
7122631   > _tgt_state:          SMP_STATE_RECOVERY
7122632   > _stage:              SMP_SWITCH_STAGE_START
7122633
7122634
7122635 [4-1970:1:10|19:28:43:213]    U3019036_IDLE_PLL_STATE:
7122636   > _before_cpu_pll_clk:       19
7122637   > _before_sys_pll_clk:       19
7122638   > _cur_cpu_pll_clk:         5dc
7122639   > _cur_sys_pll_clk:         8ca
7122640   > _start_time:         36eb0298c
7122641   > _end_time:           36eb02a2e
7122642
7122643
7122644 [6-1970:1:10|19:28:43:213]    U3019036_RECOVERY_RESUME:
7122645   > _to_state:              4
7122646   > _basetime:           36eb02a3a


```

```
中断相关
void arch_handle_interrupts(void)
u32 intr = bsp_platform::plat_ic_get_interrupt_id();

g_admin_task.add_fe_event(FE_EVENT_HOT_RST);

if intr == IRQ_POWER_FAIL：
handle_power_fail_isr

reset流程--为什么reset最终会走到SMP_STATE_RECOVERY
void nfe_isr(void)
/**
 * @brief monitor front-end hw event,
 *        includes pcie link change event, reset event,
 *        cc.en change event.
 */
void admin_task::fe_event_monitor(void)

else if ((intr == IRQ_FIS_EDGE0) || 
((intr >= IRQ_FIS_EDGE3) && (intr <= IRQ_FIS_EDGE15))) {
nfe_isr();
}

1、如果nvme_sts.cechg
pr_info("nvme_sts: %x, ce enable\n"）
如果nvme_sts.ce的ce enable
如果是BURNIN image and GOLDEN image：
// set CSTS.RDY to 1
arch_nfe_set_csts_ready(fid, 1);
如果是USER_MODE
// record cc.en event
g_admin_task.add_fe_event(FE_EVENT_SET_READY);

如果nvme_sts.ce的ce not enable
pr_info("nvme_sts: %x, ce disable, controller reset\n"）
// for BURNIN image and GOLDEN image：
如果是BURNIN image and GOLDEN image：
arch_nfe_disable(fid);
// set CSTS.RDY to 0
arch_nfe_set_csts_ready(fid, 0);
如果是USER_MODE
// record cc.en event
g_admin_task.add_fe_event(FE_EVENT_CONTROLLER_RST);
// let io task stop excuting more cmds
stop_io_by_reset();

2、如果nvme_sts.shnchg
pr_info("nvme_sts: %x, shutdown change\n"）
如果是BURNIN image and GOLDEN image：
REG_NL_CTRL_STS_WR(fid, value_ql.csts.all);
如果是USER_MODE
if ((nvme_sts.shn == 1) || (nvme_sts.shn == 2))：
    softirq::raise_softirq(SOFTIRQ_NORMAL_PD);

reset流程
nvme_sts: a, shutdown change
    IDLE->RUNNING->PD
nvme_sts: 3, ce disable, controller reset
    g_admin_task.add_fe_event(FE_EVENT_CONTROLLER_RST);
    Enable PF Dump.
    RECOVERY_MODE_PF
    set CSTS.RDY to 0
    U3019036_NFE_RESET:
      > _type:                  2
      > _stage:                 1

nvme_sts: 3, shutdown change
    PD->PD
    
nvme_sts: 5, ce enable 
    g_admin_task.add_fe_event(FE_EVENT_SET_READY);
    // resume io task
    resume_io_from_reset();
    // raise softirq
    softirq::raise_softirq(SOFTIRQ_POWERON);
    softirq_poweron_handler
    U3019036_NFE_RESET:
      > _type:                  1
      > _stage:                 1
```

```c++
SMP_STATE_E softirq_poweron_handler(SMP_STATE_E cur_admin_state, u64 param __unused)
{
    // when currently it's in running state, no need to do recovery.
    if (cur_admin_state == SMP_STATE_RUNNING || cur_admin_state == SMP_STATE_READONLY || cur_admin_state == SMP_STATE_FREEZE) {
        if (g_admin_task.is_ccen_ready()) {
            INF_LOG("set csts rdy done..\n");
            bsp_platform::nfe_set_csts_ready(0, 1);
        }

        return cur_admin_state;
    }
    else if (cur_admin_state == SMP_STATE_PD) {
        if (g_gsd_exit_task.get_task_state() != GSD_EXIT_TASK_STATE_PAUSE) {
            return SMP_STATE_NONE;
        }
        else {
            return SMP_STATE_RECOVERY;
        }
    }
    else {
        return SMP_STATE_RECOVERY;
    }
}
```

```
 >>> WARNING: intr 80 time cost: 2102 us, Exceed allowed exec time!
```

```
那我重申一下哈，几种情况的处理。

如果有串口，假设掉电了但没有看到"PF" 的打印字样，在启动时看到 "Warning!!! This is reset" 的log，那就是异常复位，这种检查环境，找硬件同事确认。

如果没有串口，从evtlog里看，是否仍然有"This is reset“，是的话也同样操作。

如果有"PF" 打印，看前面是否有高温的log，有的话就是高温盘片自己下电。

如果没有高温的log，但有"PF"，也是检查环境，看看是否被人为异常下电了。

简单来说，reset和PF 除了高温，FW 没有手段主动去触发，异常也是卡死在那不动，而不会是自己莫名复位了。

有串口飞线，有933飞线的板子，如果发现有异常复位现象，大概率是干扰耦合到飞线里头去造成的，要证明这个问题，只能是把飞线去掉再看问题会不会再出。
```

