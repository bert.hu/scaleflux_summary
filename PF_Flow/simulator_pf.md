```
5100 [0-1970:1:1|0:7:33:47]    U0018731_BLK_MESSAGE_SHOW:^M
5101   > _sblk:                  2^M
5102   > _status:                2^M
5103   > _stream:                0^M
5104   > _pe_cnt:                1^M
5105   > _valid_lpa_cnt:      1db660^M
5106   > _tot_lpa_cnt:        1db660^M
5107   > _is_bad_footer:         0^M
5108   > _maplog_sealed:         1^M
5109   > _free_ratio:            1^M
5110   > _how_long_lived:      e4e1c00^M
5111   > _release_space:         e04e8^M
5112   > _consume_space:      f8d51300^M
5113   > _last_append_PBA:    10180000563f4f^M
5114   > _latest_outstanding_PBA:   563fc0^M
5115   > _last_program_PBA:     563fc0^M
5116   > _last_user_PBA:        5637c0^M
5117   > _last_footer_PBA:      563fc0^M
5118   > _last_maplog_PBA:    10180000563800^M
5119   > _footer_PBA:         10180000563801^M
5120   > _end_footer_PBA:     1018000056384e^M
5121   > _final_user_PBA:       5637c0^M
5122   > _avg_gts:                   1^M
5123   > _tot_aval_space:     f8d51300^M
5124   > _open_seq_num:              2^M



5154 [0-1970:1:1|0:7:45:773]    U0018731_BLK_MESSAGE_SHOW:^M
5155   > _sblk:                  7^M
5156   > _status:                1^M
5157   > _stream:                0^M
5158   > _pe_cnt:                1^M
5159   > _valid_lpa_cnt:         0^M
5160   > _tot_lpa_cnt:           0^M
5161   > _is_bad_footer:         0^M
5162   > _maplog_sealed:         0^M
5163   > _free_ratio:           64^M
5164   > _how_long_lived:      e4e1c00^M
5165   > _release_space:             0^M
5166   > _consume_space:             0^M
5167   > _last_append_PBA:    ffffffffffffffff^M
5168   > _latest_outstanding_PBA:   e00fc0^M
5169   > _last_program_PBA:   ffffffffffffffff^M
5170   > _last_user_PBA:             0^M
5171   > _last_footer_PBA:           0^M
5172   > _last_maplog_PBA:    ffffffffffffffff^M
5173   > _footer_PBA:         ffffffffffffffff^M
5174   > _end_footer_PBA:     ffffffffffffffff^M
5175   > _final_user_PBA:     ffffffffffffffff^M
5176   > _avg_gts:                   0^M
5177   > _tot_aval_space:     fcfac240^M
5178   > _open_seq_num:              7^M

```

```
3713 [6-1970:1:1|0:0:0:130]    U0018731_ROOT_DATA:^M
3714   > root_data_error_code:    0^M
3715   > pf_cnt:                 1^M
3716   > current_pf_sblock:      1^M
3717   > summary_data_pba:      201400^M
3718   > last_written_pba:      2017c0^M
3719   > nor_sub_sector_idx:     0^M
3720   > user_data_start_pba:   200000^M
3721   > user_data_end_pba:     200140^M
3723   > parity_data_start_pba:   200180^M
3724   > parity_data_end_pba:   200240^M
3725   > meta_data_start_pba:   200800^M
3727   > meta_data_end_pba:     2013c0^M

die的计算方式，ce_lun * (NUM of chan) + ch
总结：pba的0x40=64，相当于64*4k=4(page_type)*4(planes)*4(ccp_offset)*4k(一个ccp的大小)，表示spage的一个die
user_data_start_pba:   200000，blk 1, lun 0, ce 0, page 0, ch 0,die 0
user_data_end_pba:     200140，blk 1, lun 0, ce 1, page 0, ch 1, die 5
parity_data_start_pba:   200180，blk 1, lun 0, ce 1, page 0, ch 2, die 6
parity_data_start_pba:   200240，blk 1, lun 0, ce 2, page 0, ch 1, die 9
meta_data_start_pba:   200800，blk 1, lun 0, ce 0, page 1, ch 0, die 0
meta_data_end_pba:     2013c0, blk 1, lun 0, ce 3, page 2, ch 0, die 15
```

```
blk信息
3860 [6-1970:1:1|0:0:0:134]    U0018731_B2N_GET_SBLOCK:^M
3861   > _blk_id:                2^M
3862   > _pe_cnt:                1^M
3863   > _stream:                0^M
3864   > _last_program_b2n_pba:   5633c0^M
3865   > _recovered_consume_space: f8c6fe00^M
3866   > _good_plane_num:       7c^M
3867   > _deallocated_sblock_num:   bc^M
3868   > _real_free_sblock_num:   13^M
3869   > _recycle_fifo_num:      0^M
3870   > _sealed_blk_num:        0^M

Total number of w2ps is: 1013049 from start pba: 400000 to end pba: 563f40, sblock 2
Total number of w2ps is: 703 from start pba: 563400 to end pba: 563f40, sblock 2

_last_program_b2n_pba:   5633c0, blk 2, lun 0, ce 3, page 710, ch 3, die 15
start pba: 563400, blk 2, lun 1, ce 0, page 710, ch 0, die 16
end pba: 563f40, blk 2, lun 1, ce 0, page 710, ch 0, die 16
```

