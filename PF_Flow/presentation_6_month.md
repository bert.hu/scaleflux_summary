计划

1、pf nor和panic dump的同步nor接口冲突

2、Warning中的task清理

3、patch dispatch去掉第一步

4、飞线去掉以后，disable lane and reset phy这步要去掉

bsp_platform::nfe_reset_lane(true);

bsp_platform::nfe_reset_phy(true);

需要在haps上验证，B0芯片验证

5、crash recovery做完

6、erase page在copy back之前的阈值调整和之后的恢复

7、考虑还有什么debug信息可以加的

8、如果b2n 导致hwhang的问题解决，可以不等io clear，将pf dump做下去

9、是不是可以考虑root data不放到nor里面，写到rfs里面