```
recovery_tas: RD_ERR: flash_rd scheme 1, fct_idx 230 pba 7d800080 nand_addr 0x2000007d8000 current vt 0 raid 0 lp_bmp 0xffff
recovery_tas: overall_status 0xf, crc 0, rfnd 0, erase_page 0, zero pg 0 rd_cmd_err 0 cwerr ffffffff ccperr ffff lperr 0 lbamis 0 dmaerr 0, ignoreshim 0 1
recovery_tas: addr 0x7ffb3820, debug_flash_rd_cpl_status ecc status 0xf be_timeout 0 from_buf 0 xfr_only 0 rfnd 0 zero_page 0 ldpc_err_cnt 0xfff snap_read 0 erase_pg 0 crc 0 dw2 0xfff40cf

打印参考：
void read_status_check(void *rd_entry, bool ignore_shim, bool ignore_erase, bool ignore_addr_update)

2527650 [2022-10-18 02:14:38.569] [6-1970:1:1|0:0:0:628]    U0019096_PF_READ_CPL:
2527651 [2022-10-18 02:14:38.570]   > ecc_status:             f
2527652 [2022-10-18 02:14:38.570]   > erase_page:             0
2527653 [2022-10-18 02:14:38.570]   > crc:                    0
2527654 [2022-10-18 02:14:38.570]   > zero_page:              0
2527655 [2022-10-18 02:14:38.570]   > vt:                     0
2527656 [2022-10-18 02:14:38.570]   > dw2:                 fff40cf

typedef union
{
    struct cpl_rd_msg    cpl_rd;
    struct cpl_wr_msg    cpl_wr;
    struct cpl_erase_msg cpl_erase;
    struct cpl_b2n_msg   cpl_b2n;
    u32                  cpl_val;
} cmd_cpl;

struct cpl_rd_msg {
    u32 overall_status: 4;
    u32 nand_status_7_4: 4;      // NAND read failure
    u32 be_to: 1;
    u32 abort_status :2;
    u32 be_dqsl :1;
    u32 rd_fr_wbuf: 1;
    u32 data_xfr_only: 1;
    u32 rsvd3: 1;
    u32 zero_page: 1;
    u32 ldpc_err_cnt: 12;
    u32 snap_read: 1;
    u32 erase_page: 1;
    u32 crc: 1;             // Internal CRC Error
    u32 rsvd: 1;
};
dw2: fff40cf
0000 1111 1111 1111 0100 0000 1100 1111
overall_status就是ecc_status， 0b1111=0xf
fct_flash_rd_cmd::get_cpl_status_val(rd_entry)可以拿到dw2

2527659 [2022-10-18 02:14:38.570] [6-1970:1:1|0:0:0:628]    U0019096_READERR:
2527660 [2022-10-18 02:14:38.645]   > _act:                   0
2527661 [2022-10-18 02:14:38.645]   > _ctag:                  0
2527662 [2022-10-18 02:14:38.645]   > _fct_idx:              e6
2527663 [2022-10-18 02:14:38.645]   > _fct_dw0:
2527664 [2022-10-18 02:14:38.645]         [0]:2cc1cd   [1]:   0   [2]:a0200   [3]:c011000   [4]:1ff00100   [5]:7d8000   [6]:2000   [7]:ffff
2527665 [2022-10-18 02:14:38.646]         [8]:   0   [9]:   0   [a]:20000   [b]:   0   [c]:20000
2527666 [2022-10-18 02:14:38.646]   > _fct_dw135:
2527667 [2022-10-18 02:14:38.646]         [0]:   0   [1]:   0   [2]:186a5000   [3]:   4   [4]:   0   [5]:c068800f   [6]:fff40cf   [7]:ffffffff
2527668 [2022-10-18 02:14:38.646]         [8]:ffff   [9]:   0   [a]:   0   [b]:   0   [c]:   0   [d]:   0   [e]:   0   [f]:   0
2527669 [2022-10-18 02:14:38.716]         [10]:   0
2527670 [2022-10-18 02:14:38.716]   > _aes_key:
2527671 [2022-10-18 02:14:38.716]         [0]:91a4ee69   [1]:6a092bb2   [2]:1e3dab88   [3]:f5059f12   [4]:444f55b7   [5]:f6d8e882   [6]:778b98b3   [7]:54321d4b
2527672 [2022-10-18 02:14:38.716]         [8]:   0   [9]:   0   [a]:   0   [b]:   0   [c]:   0   [d]:   0   [e]:   0   [f]:   0
2527673 [2022-10-18 02:14:38.716]   > _user:                  0
2527674 [2022-10-18 02:14:38.716]
2527675 [2022-10-18 02:14:38.716]
2527676 [2022-10-18 02:14:38.716] [6-1970:1:1|0:0:0:628]    U0019096_COPY_BACK_DATA_READ_UEC:
2527677 [2022-10-18 02:14:38.716]   > orignal_pba:         7d800080
2527678 [2022-10-18 02:14:38.716]   > target_pba_main:     79400080
2527679 [2022-10-18 02:14:38.716]   > target_pba_backup:   794000c0
2527680 [2022-10-18 02:14:38.716]   > bad_pl_mask:            0
2527681 [2022-10-18 02:14:38.773]   > stream:                 7



```

```c++
if (fct_flash_rd_cmd::get_cpl_status_overall_status(rd_entry) &&      !fct_flash_rd_cmd::get_cpl_status_erase_page(rd_entry)) {
  EVT_LOG(readerr_evtlog, (u64)rd_entry, FLASH_RD);
}
```

```
需要修改的地方
pf_mgr::process_read_pending(bool expect_erased, bool retry_only)
可以在这里判断是不是ecc status和erasepage

fw_flash_read_cmd::read_slc_page_by_pba_to_buf
pf_mgr::pf_read_cmd_submit(u8 *read_buf, u64 pba, u8 scheme, u8 bad_plane_mask, bool slc)
这里修改每次读取4k的大小来判断是否是erasepage
```

