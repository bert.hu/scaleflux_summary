```
状态切换：从INIT DONE 切换到Recovery
[2022-09-20 10:10:26.009] [4-1970:1:1|0:0:0:125]    U0018929_SMPSTATE_CHANGE:
[2022-09-20 10:10:26.061]   > _src_state:          SMP_STATE_INIT_DONE
[2022-09-20 10:10:26.061]   > _tgt_state:          SMP_STATE_RECOVERY
[2022-09-20 10:10:26.061]   > _stage:              SMP_SWITCH_STAGE_START

Chip Reset Check, reg: 0x7, data: 0x1f
WARNING: This is unexpected chip reset! nor addr 0x117c000
```

```
开始读root data
[2022-09-20 10:10:26.478] [6-1970:1:1|0:0:0:137]    U0018929_ROOT_DATA:
[2022-09-20 10:10:26.478]   > root_data_error_code:    0
[2022-09-20 10:10:26.478]   > pf_cnt:                 3
[2022-09-20 10:10:26.478]   > current_pf_sblock:    1d4
[2022-09-20 10:10:26.478]   > summary_data_pba:    75004340
[2022-09-20 10:10:26.478]   > last_written_pba:    750047c0
[2022-09-20 10:10:26.478]   > nor_sub_sector_idx:     2
[2022-09-20 10:10:26.478]   > user_data_start_pba: 75003000
[2022-09-20 10:10:26.478]   > user_data_end_pba:   75003000
[2022-09-20 10:10:26.478]   > parity_data_start_pba: 75003040
[2022-09-20 10:10:26.478]   > parity_data_end_pba: 75003080
[2022-09-20 10:10:26.532]   > meta_data_start_pba: 75003800
[2022-09-20 10:10:26.532]   > meta_data_end_pba:   75004300

在pf block上
第一个是
user_data_start_pba   75003000
user_data_end_pba     75003000
parity_data_start_pba 75003040
parity_data_end_pba   75003080
meta_data_start_pba   75003800
meta_data_end_pba     75004300 (meta data在4tb47上是2816byte)
summary_data_pba      75004340
last_written_pba      750047c0 （summary大小是1152byte）
```

```
再读summary data
再看b2n get block
445 [2022-09-20 10:10:26.689] [6-1970:1:1|0:0:0:146]    U0018929_B2N_GET_SBLOCK:
 446 [2022-09-20 10:10:26.689]   > _blk_id:              1ec
 447 [2022-09-20 10:10:26.689]   > _pe_cnt:              103
 448 [2022-09-20 10:10:26.689]   > _stream:                0
 449 [2022-09-20 10:10:26.689]   > _last_program_b2n_pba: 7b002800
 450 [2022-09-20 10:10:26.689]   > _recovered_consume_space: 9cea00
 451 [2022-09-20 10:10:26.689]   > _good_plane_num:      100
 452 [2022-09-20 10:10:26.689]   > _deallocated_sblock_num:  21d
 453 [2022-09-20 10:10:26.689]   > _real_free_sblock_num:   14
 454 [2022-09-20 10:10:26.689]   > _recycle_fifo_num:      0
 455 [2022-09-20 10:10:26.743]   > _sealed_blk_num:        0

io_task0:                           Num of Chan      : 16
io_task0:                           Num of CE        : 2
io_task0:                           Num of LUN       : 2
io_task0:                           Num of Offset    : 4
io_task0:                           Num of Die       : 64


 Total number of w2ps is: 2095104 from start pba: 7b000000 to end pba: 7b2c7f80, sblock 492
 (lun*ce_num+ce)<<4+ch=0
 ce_lun=0,ch=0,ce=(ce_lun%2)=0,lun=(ce_lun/2)=0,die=0
 ce_lun=3,ch=14,ce=1,lun=1,die=62
 首先从blk 492的page 0扫描到page 711，从die0扫描到die62
 Total number of w2ps is: 2092608 from start pba: 7b002840 to end pba: 7b2c7f80, sblock 492
 ce_lun=2,ch=1,ce=0,lun=1,die=33
 这次是从blk 492的page2扫描到page 711，从die=33扫描到die62
 _last_program_b2n_pba: 7b002800
 也就是说会从last_program_b2n_pba扫描到最后除开parity的die
```

```
恢复l2p patch
525 [2022-09-20 10:10:27.132] [6-1970:1:1|0:0:0:148]    U0018929_RESTORE_L2P_PATCH_MT_D:
 526 [2022-09-20 10:10:27.132]   > patch_start_pba:     20300059400000
 527 [2022-09-20 10:10:27.132]   > buffer_cnt:             0

[2022-09-20 11:08:41.808] pba.all 0x20300059400000:
[2022-09-20 11:08:41.808] ch:       0
[2022-09-20 11:08:41.808] ce_lun:   0
[2022-09-20 11:08:41.808] block:    357
[2022-09-20 11:08:41.808] page:     0
[2022-09-20 11:08:41.808] plane:    0
[2022-09-20 11:08:41.808] offset:   0x0
[2022-09-20 11:08:41.808] type:     0
[2022-09-20 11:08:41.808] ep_idx:   0
[2022-09-20 11:08:41.808] ccp_off:  0x0
[2022-09-20 11:08:41.808] comp_len: 0x406
[2022-09-20 11:08:41.808] unc:      0

```

```
恢复gts
 538 [2022-09-20 10:10:27.132] [6-1970:1:1|17:33:16:709]    U0018929_RESTORE_GTS_META_DATA:
 539 [2022-09-20 10:10:27.132]   > gts:                   48b5c1

```

```
再进行数据copy back
562 [2022-09-20 10:10:27.248] [6-1970:1:1|17:33:16:709]    U0018929_PENDING_FLUSHED_PBA:
 563 [2022-09-20 10:10:27.248]   > stream:                 0
 564 [2022-09-20 10:10:27.248]   > flush_cnt:              1

每个flush cnt指的是每个stream被abort的user_data_entry的个数
以stream 0为例
先看stream 0的block
 445 [2022-09-20 10:10:26.689] [6-1970:1:1|0:0:0:146]    U0018929_B2N_GET_SBLOCK:
 446 [2022-09-20 10:10:26.689]   > _blk_id:              1ec
 447 [2022-09-20 10:10:26.689]   > _pe_cnt:              103
 448 [2022-09-20 10:10:26.689]   > _stream:                0
 449 [2022-09-20 10:10:26.689]   > _last_program_b2n_pba: 7b002800
 450 [2022-09-20 10:10:26.689]   > _recovered_consume_space: 9cea00
 451 [2022-09-20 10:10:26.689]   > _good_plane_num:      100
 452 [2022-09-20 10:10:26.689]   > _deallocated_sblock_num:  21d
 453 [2022-09-20 10:10:26.689]   > _real_free_sblock_num:   14
 454 [2022-09-20 10:10:26.689]   > _recycle_fifo_num:      0
 455 [2022-09-20 10:10:26.743]   > _sealed_blk_num:        0
记录写到最后的pba是7b002800，copy back的这个pba也是7b002800
 567 [2022-09-20 10:10:27.248] [6-1970:1:1|17:33:16:709]    U0018929_COPY_BACK_DATA:
 568 [2022-09-20 10:10:27.248]   > orignal_pba:         7b002800
 569 [2022-09-20 10:10:27.248]   > target_pba_main:     75003000
 570 [2022-09-20 10:10:27.305]   > target_pba_backup:   ffffffffffffffff
 571 [2022-09-20 10:10:27.305]   > bad_pl_mask:            0
 572 [2022-09-20 10:10:27.305]   > stream:                 0

数据copy back以后再恢复rebuid data
Read PF stream pba: 0x75003040
```

```
pf restore clean up阶段
 597 [2022-09-20 10:10:27.416] [6-1970:1:1|17:33:16:713]    U0018929_NOR_TASK_ADD_JOB:
 598 [2022-09-20 10:10:27.416]   > _state:              NOR_TASK_STATE_IDLE
 599 [2022-09-20 10:10:27.416]   > _job_id:                6
 600 [2022-09-20 10:10:27.416]   > _job_type:           NOR_ERASE_OPERATION
 601 [2022-09-20 10:10:27.416]   > _address:            1fe3000
 602 [2022-09-20 10:10:27.416]   > _size_four_byte:        0
 603 [2022-09-20 10:10:27.416]
 604 [2022-09-20 10:10:27.416] [6-0:0:0D|0:0:2:155]     nor_task:                erase job 6 took 3784600ns

擦nor的一个4k需要的时间是3.7846ms

 621 [2022-09-20 10:10:27.531] [6-1970:1:1|17:33:16:716]    U0018929_NOR_TASK_ADD_JOB:
 622 [2022-09-20 10:10:27.531]   > _state:              NOR_TASK_STATE_IDLE
 623 [2022-09-20 10:10:27.531]   > _job_id:                7
 624 [2022-09-20 10:10:27.531]   > _job_type:           NOR_ERASE_OPERATION
 625 [2022-09-20 10:10:27.531]   > _address:            1fd3000
 626 [2022-09-20 10:10:27.531]   > _size_four_byte:        0

 631 [2022-09-20 10:10:27.585] [6-0:0:0D|0:0:2:159]     nor_task:                erase job 7 took 3781560ns
 
 padding_task:               Enable padding task during recovery
Enable PF Dump
clean_up already complete!
```

```
_before_start_padding_pba: 2030007b00280f
_start_padding_pba:  7b002840
_end_padding_pba:    7b002fc0

blk:492
7b002840
ce_lun=2,ch=1,ce=0,lun=1,die=33,page=2
7b002fc0
ce_lun=3,ch=15,ce=1,lun=1,die=63,page=2
2030007b00280f
ce_lun=2,ch=0,ce=0,lun=1,die=33,page=2
[2022-09-20 14:29:09.277] plane:    3
[2022-09-20 14:29:09.277] offset:   0x3
[2022-09-20 14:29:09.277] type:     0
[2022-09-20 14:29:09.277] ep_idx:   0
[2022-09-20 14:29:09.277] ccp_off:  0x0
[2022-09-20 14:29:09.277] comp_len: 0x406

padding的stream
0，2，3，4，5，6，7

padding_task:                   Updating padding flush maplog for pba: 7b002f80
7b002f80
ce_lun=3,ch=14,ce=1,lun=1,die=62,page=2
这里padding只到这个page的边界，还没有到parity

```

```
看看pba
2030007b002f8f
和
7b002f8f
区别就在于comp_len

[2022-09-20 14:50:46.905] pba.all 0x7b002f8f:
[2022-09-20 14:50:46.905] ch:       14
[2022-09-20 14:50:46.905] ce_lun:   3
[2022-09-20 14:50:46.905] block:    492
[2022-09-20 14:50:46.905] page:     2
[2022-09-20 14:50:46.905] plane:    3
[2022-09-20 14:50:46.905] offset:   0x3
[2022-09-20 14:50:46.905] type:     0
[2022-09-20 14:50:46.905] ep_idx:   0
[2022-09-20 14:50:46.906] ccp_off:  0x0
[2022-09-20 14:50:46.906] comp_len: 0x0
[2022-09-20 14:50:46.906] unc:      0

[2022-09-20 14:51:02.700] pba.all 0x2030007b002f8f:
[2022-09-20 14:51:02.700] ch:       14
[2022-09-20 14:51:02.700] ce_lun:   3
[2022-09-20 14:51:02.700] block:    492
[2022-09-20 14:51:02.700] page:     2
[2022-09-20 14:51:02.700] plane:    3
[2022-09-20 14:51:02.700] offset:   0x3
[2022-09-20 14:51:02.700] type:     0
[2022-09-20 14:51:02.700] ep_idx:   0
[2022-09-20 14:51:02.700] ccp_off:  0x0
[2022-09-20 14:51:02.700] comp_len: 0x406
[2022-09-20 14:51:02.700] unc:      0
```

```
FTL Recovery Start
scan_summary_read_candidate_sblock
扫描的是l2p stream的block，包括sealed和open的
这里还会根据open seq num对block进行排序


```

debug信息

```c++
        // put task in same core together
        for (u32 taskid = 0; taskid < MAX_TASK_NUMBER_IN_SCHEDULER; taskid++) {
            task_base *ptask = task_tbl[taskid]._ptask;
            if (ptask != nullptr) {
                if (coreid == task_tbl[taskid]._enabled_core) {
                    auto &te = _task_status[fill_idx];
                    //
                    strncpy(te.name, ptask->task_name(), sizeof(te.name));
                    te.state      = ptask->get_task_state();
                    te.coreid     = coreid;
                    te.run_status = ptask->check_status();
                    te.asserted   = ptask->is_asserted();
                    te.call_cnt   = psched->get_task_call_cnt(taskid);
                    te.fin_cnt    = psched->get_task_fin_cnt(taskid);
                    te.tick_cnt   = psched->get_task_time_cost(taskid);
                    te.cpu_usage  = (te.tick_cnt * 100) / psched->get_smpmgr()->get_tot_sys_time();
                    total_tickcnt += psched->get_task_time_cost(taskid);
                    fill_idx++;
                }
            }
        }
```

```
[2022-11-07 17:54:52] === qid 43====
[2022-11-07 17:54:52] fcq_q_proerty_0 = 0x10157 [qtype 0x2, strm_id 0x0]
[2022-11-07 17:54:52] fcq_q_proerty_1 = 0x7fe692500ff02b
[2022-11-07 17:54:52] fcq_q_proerty_2 = 0x7fe696500ff12b
[2022-11-07 17:54:52] fcq_q_ptr = 0x0
[2022-11-07 17:54:52] === qid 43====
[2022-11-07 17:54:52]
[2022-11-07 17:54:52] === qid 44====
[2022-11-07 17:54:52] fcq_q_proerty_0 = 0x10159 [qtype 0x2, strm_id 0x0]
[2022-11-07 17:54:52] fcq_q_proerty_1 = 0x7fe69e900ff02c
[2022-11-07 17:54:52] fcq_q_proerty_2 = 0x7fe6a2900ff12c
[2022-11-07 17:54:52] fcq_q_ptr = 0x0
[2022-11-07 17:54:52] === qi
[2022-11-07 17:54:52] PF
[2022-11-07 17:54:52] ps 53
[2022-11-07 17:54:52] hs 54
[2022-11-07 17:54:52] ha 55

vu:
diag
nvmedbg: vu_handler_dumpnvmestatus
hwdbg:vu_handler_dumphwstatus
```

