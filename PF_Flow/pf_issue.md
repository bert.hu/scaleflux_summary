在什么情况下我们不能保证PF的数据完成性

1. 如果IO读写发生了异常，比如IO timeout的时候，这时候不能保证数据能写到NAND上，此时发生PF，就不能保证PF的数据完成性；
2. 如果发生了CPU hang或硬件hang的问题，此时发生PF，因为无法调度task，也不能保证PF的数据完成性；

45429 [4-2-1970:1:1|0:0:3:675]    Detect host PERST pin to high
45430 [4-2-1970:1:1|0:0:3:675]    Detect host PERST to run
45431 [4-2-1970:1:1|0:0:3:675]    U4121812_NVME_PERST_MONITOR:
45432   > _state:              PERST_TASK_STATE_INIT
45433   > _misc8_state:        PERST_GPIO_HIGH
45434   > _misc10_state:       PERST_GPIO_HIGH
45435   > _pcie_link:          c0004410
45436   > _cur_time:           38159a
45437   > _hot_rst_time:       0
45438   > _hot_rst_flag:       false

掉电前

```
46642 [4-2-2023:3:25|23:30:54:163]    U4121812_NVME_PERST_MONITOR:
46643   > _state:              PERST_TASK_STATE_HIGH_POLLING
46644   > _misc8_state:        PERST_GPIO_HIGH
46645   > _misc10_state:       PERST_GPIO_NOT_SET
46646   > _pcie_link:          1000
46647   > _cur_time:           14bfd985
46648   > _hot_rst_time:       14bfd72c
46649   > _hot_rst_flag:       false
46650 [4-2-2023:3:25|23:30:54:163]    U4121812_NVME_PERST_MONITOR:
46651   > _state:              PERST_TASK_STATE_HIGH_WAIT_IO_CLEAR
46652   > _misc8_state:        PERST_GPIO_HIGH
46653   > _misc10_state:       PERST_GPIO_LOW
46654   > _pcie_link:          1000
46655   > _cur_time:           14bfd99a
46656   > _hot_rst_time:       14bfd72c
46657   > _hot_rst_flag:       false
46658 [4-2-2023:3:25|23:30:54:165]    U4121812_SMPSTATE_CHANGE:
46659   > _src_state:          SMP_STATE_RUNNING
46660   > _tgt_state:          SMP_STATE_PF
46661   > _stage:              SMP_SWITCH_STAGE_START
46662 [5-2-2023:3:25|23:30:54:165]    b2n task resume: to STATE_PF

```

```
切换smp耗时
[0-2-2023:4:21|15:14:29:359]    U0021964_FIO_STATUS:
  > _qd_estimate:        100
  > _is_rd:              false
  > _is_wr:              true
  > _is_seq_rd:          false
  > _is_seq_wr:          false
  > _fourk_seq_write_flag: 0
  > _single_io_core:     0
  > _fixed_req_len:      1
  > _prev_len:           1
  > g_delay_limit:       0
  > g_split_credit:      true
  > g_one_qd_delay:      0
  > g_delay_active:      0
  > _intr_switch:        true
  > _intr_reg:           61a80000
[7-2-2023:4:21|15:14:29:378]    U0021964_FD_P2L_TO_HDR_EXTRA:
  > _num:                2
  > _stream:             2
  > _src_blk:            136
  > _footer_rd_bad_cnt:  0
  > _is_footer_in_cache: false
  > _src_blk_footer_good: false
  > _start_footer_pba:   0
  > _cur_pba:            9b530000

PF
pe 9
1340
1854
4856
4903
6044
6295
6744
7273
8520
8593
8638
9225
9626
0
0
0
user:181
parity:16
padding0:27
meta:60
summary:1
padding1:3

```

```
[7-2-2023:4:21|15:16:13:989]    U0021964_FD_P2L_TO_HDR_EXTRA:
  > _num:                2
  > _stream:             2
  > _src_blk:            12a
  > _footer_rd_bad_cnt:  0
  > _is_footer_in_cache: false
  > _src_blk_footer_good: false
  > _start_footer_pba:   406000e7d84006
  > _cur_pba:
PF
pe 9
1100
1955
5069
5094
6449
6702
6751
7253
8483
8560
8601
9191
9594
0
0
0
user:204
parity:18
padding0:2
meta:60
summary:1
padding1:3
```

```
[1-2-2023:4:21|15:37:2:822]    TT: 334332 331013 316955 316955
[1-2-2023:4:21|15:37:2:822]    free: 13 , 104058 , 477 , avg:32174
[1-2-2023:4:21|15:37:2:822]    map: 56198 38838 62950 67865
[1-2-2023:4:21|15:37:2:822]    cf: 9760 10000 ind:1 nf:5 0
[1-2-2023:4:21|15:37:2:822]    cr:100 frd:160074 mavg:52272
[1-2-2023:4:21|15:37:2:822]    dec:0 ft: 8 2 wl: 0
[1-2-2023:4:21|15:37:2:822]    gc rd submit_cnt 160074, poll_cnt 160290, avg_tickcount 13658
[1-2-2023:4:21|15:37:2:822]    gc wr submit_cnt 11140, poll_cnt 9974, avg_tickcount 47593
[5-2-2023:4:21|15:37:3:6]    U0021964_BLK_STATUS_CHANGE:
  > _blk_id:             191
  > _stream:             ff
  > _cur_status:         BS_FREE_BLOCK
  > _new_status:         BS_OPEN_BLOCK
  > _pe_cnt:             bb
  > _open_seq_num:       ffffffff
  > _tot_free_cnt:       c
  > _free_fifo_cnt:      8
  > _recycle_fifo_cnt:   0
  > _sealed_blk_cnt:     20e

PF
pe 10
11
3130
5996
6042
7505
7809
7989
8464
9701
9777
9817
10404
10806
0
0
0
user:224
parity:22
padding0:10
meta:60
summary:1
padding1:3
```

```
[7-2-2023:4:21|15:43:55:970]    U0021964_FD_P2L_TO_HDR_EXTRA:
  > _num:                2
  > _stream:             2
  > _src_blk:            9e
  > _footer_rd_bad_cnt:  0
  > _is_footer_in_cache: false
  > _src_blk_footer_good: false
  > _start_footer_pba:   406000fed84007
  > _cur_pba:            4f0b4000
[7-2-2023:4:21|15:43:55:994]    U0021964_FD_P2L_TO_HDR_EXTRA:
  > _num:                2
  > _stream:
PF
pe 8
1742
2527
4764
4809
4926
5145
5173
5720
6962
7048
7090
7681
8087
0
0
0
user:15
parity:16
padding0:1
meta:60
summary:1
padding1:3

```

```
[5-2-2023:4:21|18:10:45:292]    SBLOCK: 138 already initialized in stream: 0, current target init stream: 5
[5-2-2023:4:21|18:10:45:292]    U0021964_B2N_GET_SBLOCK:
  > _blk_id:             8a
  > _pe_cnt:             b9
  > _stream:             5
  > _last_program_b2n_pba: 45583fc0
  > _recovered_consume_space: 1f3c00
  > _good_plane_num:     200
  > _deallocated_sblock_num: d
  > _real_free_sblock_num: 8
  > _recycle_fifo_num:   0
  > _sealed_blk_num:     20e
[5-2-2023:4:21|18:10:45:292]    Total number of w2p
PF
pe 15
1545
6004
10505
10551
12277
12553
13076
13579
14836
14909
14951
15533
15936
0
0
0
user:268
parity:20
padding0:32
meta:60
summary:1
padding1:3
```

​    /*

​    * SYSCLK = SYSPLL / 3

​    * CPUCLK = SYSCLK * 2

​    * UARTCLK = SYSCLK / 4

​    */



\#if (__BL3__ == 1)

\#define PLL_SYS_CLK_MHZ                 ULL(2250) // config in bl3:2250->1700

\#define PLL_CPU_CLK_MHZ                 ULL(1500)

\#else

 // sys and cpu clk fixed value in bl2

\#define PLL_SYS_CLK_MHZ                 ULL(2250)

\#define PLL_CPU_CLK_MHZ                 ULL(1500)

\#endif