```
release_invalid_src_blks
release_invalid_src_blk
blk_has_valid_data
deallocate_sblock
invalidate_sblock
BS_DEALLOCATED_BLOCK
BS_INVALID_BLOCK
can_recycle_block
set_sblock_valid_lpa_cnt

[4-1970:1:1|0:0:0:334]    U3319572_SN_MN:
  > _sn:                 UD2223G0018M
  > _mn:                 CSD-3310


[4-1970:1:1|0:0:0:338]    U3319572_NOR_TASK_ADD_JOB:
  > _state:              NOR_TASK_STATE_FETCH_JOB
  > _job_id:                0
  > _job_type:           NOR_READ_OPERATION
  > _address:            1ff1000
  > _size_four_byte:      400


[4-1970:1:1|0:0:0:340]   power_temperature:
  > _evt_name:             U3319572_POWER_TEMPERATURE
  > power data:            0uW
  > board temp:            43degrees celsius
  > chip_cpu_drr temp:     51degrees celsius
  > chip_serdes temp:      52degrees celsius
  > chip_nand1 temp:       50degrees celsius
  > chip_nand2 temp:       53degrees celsius
  > adc temp:              50degrees celsius


2022-11-26 19:24:35,305 INFO ****** DMC at slba 0x2e50d200 and lbaCnt 0x100 ******
2022-11-26 19:24:35,330 INFO IO --- FullSectorCmp: Data miscompare at offset 0xe100 and lcnt 0x70 (112) DMC LPA 0x5ca1a4e DMC LBA 0x2e50d270 (777048688)

[2022-08-01 20:17:50] [4-53:4:8D|16:12:39:922]   nfe_err:
[2022-08-01 20:17:50]   > _nfe_err_sts:        1000
[2022-08-01 20:17:50]   > _nfe_hw_err_0:          0
[2022-08-01 20:17:50]   > _nfe_hw_err_1:          0
[2022-08-01 20:17:50]   > _nfe_hw_err_2:          0
[2022-08-01 20:17:50]   > _ffe_hw_err_3:          0
[2022-08-01 20:17:50]   > _nfe_status_info:       0
[2022-08-01 20:17:50]   > _nl_err_sts_a:         40
[2022-08-01 20:17:50]   > _nl_err_sts_b:          0
[2022-08-01 20:17:50]   > _pcie_corr_err:         0
[2022-08-01 20:17:50]   > _pcie_uncorr_err:       0
[2022-08-01 20:17:50]   > _pcie_dma_status:    bb002120
[2022-08-01 20:17:50]   > [pcie_dev_status:    2930
```

```c++
void arch_get_fcq_ptr(u32 qid, u16 *qptr)
{
    FCQ_Q_PTR_U ptr;
    ACE_CSR_AUTO_WR(FCQ_Q_SEL, qid);
    ptr.data = ACE_CSR_AUTO_RD(FCQ_Q_PTR);
    
    if (qptr != NULL) {
        qptr[0] = (u16)ptr.bits.sq_hd;
        qptr[1] = (u16)ptr.bits.sq_tail;
        qptr[2] = (u16)ptr.bits.cq_hd;
        qptr[3] = (u16)ptr.bits.cq_tail;
    }
}

debug_data->ck_w2p_fcq[0]            = (u8)0;
debug_data->ck_w2p_fcq[1]            = (u8)1;
debug_data->ck_w2p_fcq[2]            = (u8)2;
debug_data->ck_w2p_fcq[3]            = (u8)3;
0x03020100
```

