```c++
/home/tcnsh/b47_img/myrtle_fw.asic.wout.bl3.dev_build.csdu4spc38a0_vU.0.0@37ea8c_13389.img
lspci -d cc53:
sudo /root/rescanpcie.sh
modprobe nvme
cd /etc/modprobe.d/blacklist.conf

sudo nvme fw-download /dev/nvme0n1 -f /home/tcnsh/b47_img/myrtle_fw.asic.wout.bl3.dev_build.csdu4spc38a0_vU.0.0@64b327_13714.strip.elf.signed.bin -x 0x20000

sudo nvme fw-download /dev/nvme0n1 -f /home/tcnsh/test.bin -x 0x20000
sudo nvme fw-activate /dev/nvme0n1 -s 1 -a 1

sudo /home/tcnsh/samanea/Samanea_Platform/spdk/scripts/setup.sh reset
sudo /root/rescanpcie.sh
sudo modprobe nvme

python3 PC_FIOMixPF.py --capacityChange=1 --compression=1 --sectorSize=4096

其他
killall python3

=============== Test Summary ==============
Test Case Name:    PC_FIOMixPF
Test Case Execution Time:   16:18:59.813943 (hh:mm:ss.xxxx)
Test Case Result:  Pass


git config --global user.name "bert"
git config --global user.email "bert.hu@scaleflux.com"
  
update_bb_crit_warn
g_risk_board.set_risk(RISK_EVT_TOO_MANY_BB)
  
UD2214C0023M@CSDU5SPC38M1:/$ dfcq 12
fcqid:12, (cq_base:0x7fe772d0 head:0 tail:0), (sq_base:0x7fe76ed0 head:0 tail:1).
sq idx opc sub fct fuse | cq opc sub vld fct sqhd | fct_addr  dw0       dw1       dw2       dw3       dw4       dw5       dw6       dw7       dw8       dw9       dw10
   0   0   0   306 0    |    0   0   0   0   0    | 7fe8a860  60d       a0000     0         1         890000    2000      0         0         0         0         e0

0x0000200000890000
```

#### 更新BL2

```
sudo ./utility/prebuild.sh asic.bl2 USER_IMG
sudo ./utility/dobuild.sh asic.bl2 CSDU4SPC38A0 USER_IMG // 4T 16G DDR

sudo ./utility/dobuild.sh asic.bl2 CSDU4SPC38A0 USER_IMG // 8T 16G DDR

sudo ./utility/dobuild.sh asic.bl2 CSDU4SRC76A0 USER_IMG
```

#### 更新BL3

```
GOLDEN BL3:
sudo ./utility/prebuild.sh asic.wout.bl3 GOLDEN_IMG
sudo ./utility/dobuild.sh asic.wout.bl3 CSDU5SPC38M1 GOLDEN_IMG // 4T 16G DDR

sudo ./utility/prebuild.sh asic.wout.bl3 DEV_BUILD
sudo ./utility/dobuild.sh asic.wout.bl3 CSDU5SPC38M1 DEV_BUILD

sudo ./utility/prebuild.sh asic.wout.bl3 USER_IMG
sudo ./utility/dobuild.sh asic.wout.bl3 CSDU5SPC38M1 USER_IMG // 4T 16G DDR

sudo ./utility/dobuild.sh asic.wout.bl3 CSDU4SRC76A0 USER_IMG

./utility/prebuild.sh asic.wout.bl3 DEV_BUILD
./utility/dobuild.sh asic.wout.bl3 CSDU5SPC38M1 DEV_BUILD


更新bl3到DDR
[root@localhost ~]# cat dnld.sh
sudo setpci -s 2:0.0 0x4.l=0x100006
sleep 1
sudo fw_validation/down_pciemem /sys/bus/pci/devices/0000\:02\:00.0/resource0 /home/tcnsh/b47_img/bl3.img trigger

./utility/prebuild.sh asic.wout.bl3
./utility/dobuild.sh asic.wout.bl3 CSDU4SRC38A0 DEV_BUILD

/usr/bin/python3.6
get error lpa

echo "export PATH=/opt/toolchain/gcc-arm-10.3-2021.07-x86_64-aarch64-none-elf/bin:\$PATH" >>/etc/profile

scl enable devtoolset-8 bash


库上的代码在串口输入VU命令时有可能出现I2C timeout的打印，这个是因为之前在i2c里面有一个spinlock，中断来了也不会响应，每2ms读功耗的同时输入串口也没关系，现在去掉了，打印耗时就把i2c时序搞乱了。这个串口输入是可以先把功耗获取的接口关掉，VU：powernogain 0
```



### flashrd的使用

```txt
UD2150C0065M@CSDU4SPC38A0:/$ l2p 0xccbda21
[4-0:0:0D|0:6:39:512] rfs_file_tas:                    lpa: 0xccbda21, the corresponding pba is: 0x20328b01db0a89
UD2150C0065M@CSDU4SPC38A0:/$flashrd 0x20328b01db0a89 0xccbda21 normal tlc

parsepba 0x20328b01db0a89
```

#### tuning

```
commit dcf5bfbb2020facea0241641c496a2d62f2a4579 (HEAD -> feature/task_test)
Author: bert <bert.hu@scaleflux.com>
Date:   Sun Feb 27 16:44:31 2022 +0800

    read nor state timecost tuning

commit 561e4546aaedd1543b623e36dd75a1e11ae2b916
Author: bert <bert.hu@scaleflux.com>
Date:   Tue Feb 22 10:27:38 2022 +0800

    pf recovery timecost profiling

```

#### 测试的脚本

```
AID_AsyncIOWithAssertPFRecovery.py             ------Test Fail
AID_FoldingWithAssertPFRecovery.py
AID_FormatWithAssertPFRecovery.py
AID_GCWithAssertPFRecovery.py
AID_ProgramErrorWithB2NAssert.py
AID_ReadScrubWithAssertGC.py
AID_SyncIOWithAssertPFRecovery.py
BDP_MarkBBDieWlTrimFormatGSDPF.py
BDP_MarkBBEraseErrorWLTargetBlockPFGSD.py
BDP_MarkBBRSHighLdpcReadErrorProgramErrorPFGSD.py
BDP_MarkBBRSHighLdpcReadErrorProgramErrorTrimFormatPFGSD.py
BDP_MarkBBRSReadErrorProgramErrorPFGSD.py
BDP_MarkBBRSReadErrorProgramErrorTrimFormatPFGSD.py
BDP_MarkBBReadErrorProgramErrorEraseErrorPFGSD.py
BDP_MarkBBReadErrorProgramErrorFormatTrimPFGSD.py
BDP_MarkBBReadErrorProgramErrorPFGSD.py
EH_MultiReadErrorGCSourcePFGSD.py
EH_MultiReadErrorPFGSD.py
EH_ProgramErrorAsyncIOFormatTrimPFGSD.py
EH_ProgramErrorEHTargetPFGSD.py
EH_ProgramErrorGCPFGSD.py
EH_ProgramErrorMixReadErrorAndPFOnMpageBlock.py
EH_ProgramErrorMultiBlockPFGSD.py
EH_ProgramErrorOnMPageBlockMixPF.py
EH_ProgramErrorPaddingMeetProgramErrorMixPFOnOpenParity.py
EH_ReadErrorGCTargetPFGSD.py
EH_ReadErrorGCTargetTrimFormatPFGSD.py
FID_InterruptCoalescingBasicVerify.py
FID_NumberOfQueuesMixQueuesNumberTest.py
FID_TimestampVerify.py
Mat_MatWithGSD.py
Mat_MatWithPF.py
NS_MultiNamespaceIOMixGSD.py
NS_MultiNamespaceIOMixPF.py
PC_AsyncIOMixContinueProbePF.py                    ------Test Pass
PC_AsyncIOMixGCWithPF.py                           ------Test Fail segment fault
PC_AsyncIOMixProbePF.py                           ------Test Pass
PC_AsyncWriteMixGCWithPF.py                       ------Test Fail
PC_CapacityFullDriverWithPF.py            ------Test Fail  Timeout waiting for DPDK to remove PCI device 0000:02:00.0.
PC_CapacityOverlapFullDriverWithPF.py             ------Test Fail DMC
PC_CapacityPageInFormatDuringWithPF.py             ------Test Pass
PC_CapacityPageInMixAdminWithPF.py                 ------Test Pass
PC_CapacityPageInMixAsyncMutilWriteAndTrimWithPF.py ------Test Fail DMC
PC_CapacityPageInMixDoubleReadErrorWithPF.py       ------Test Pass
PC_CapacityPageInMixFoldingDoneWithPF.py ------Test FAIL assert:BEMGR_INVALID_PBA_PASSED
PC_CapacityPageInMixFormatWithPF.py                ------Test Pass
PC_CapacityPageInMixGCNotDoneWithPF.py             ------Test Pass
PC_CapacityPageInMixGCWithPF.py                    ------Test Pass
PC_CapacityPageInMixIOWithPF.py
PC_CapacityPageInMixMutilWriteAndTrimWithPF.py
PC_CapacityPageInMixReadDisturbWithPF.py
PC_CapacityRandomTypeDiffSizeIOMixFullQDWithPF.py
PC_CapacityWithPF.py
SYS_FS_EXT4WithPFByRelay.py
SYS_FS_xfsWithPFByRelay.py
SPF_ShutdownWithDataIntegration.py
SPF_ShutdownWithoutIO.py


python3 PC_IOMixPF.py | tee /home/tcnsh/log/PC_IOMixPF_pythonScripts_0225.log

python3 PC_AsyncIOMixGCWithPF.py --testLevel=Daily | tee /home/tcnsh/log/PC_AsyncIOMixGCWithPF_0227.log

python3 PC_CapacityOverlapFullDriverWithPF.py --testLevel=Daily | tee /home/tcnsh/log/PC_CapacityOverlapFullDriverWithPF_0227.log

python3 PF_SequentiallyFullDriverWithPF.py --testLevel=Daily | tee /home/tcnsh/log/PF_SequentiallyFullDriverWithPF_0302.log

./loop.sh bert.list | tee testPClog.log

sudo /home/tcnsh/samanea/Samanea_Platform/spdk/scripts/setup.sh reset
sudo /root/rescanpcie.sh
sudo modprobe nvme

====== Recovery stage timecost: ======
```

```
[root@localhost samanlog]# lspci
pcilib: Cannot open /sys/bus/pci/devices/0000:01:00.0/config
lspci: Unable to read the standard configuration space header of device 0000:01:00.0
00:00.0 Host bridge: Intel Corporation Device 4c53 (rev 01)
00:02.0 VGA compatible controller: Intel Corporation RocketLake-S GT1 [UHD Graphics 750] (rev 04)
00:14.0 USB controller: Intel Corporation Tiger Lake-H USB 3.2 Gen 2x1 xHCI Host Controller (rev 11)
00:14.2 RAM memory: Intel Corporation Tiger Lake-H Shared SRAM (rev 11)
00:15.0 Serial bus controller [0c80]: Intel Corporation Tiger Lake-H Serial IO I2C Controller #0 (rev 11)
00:15.1 Serial bus controller [0c80]: Intel Corporation Device 43e9 (rev 11)
00:16.0 Communication controller: Intel Corporation Tiger Lake-H Management Engine Interface (rev 11)
00:16.3 Serial controller: Intel Corporation Device 43e3 (rev 11)
00:17.0 SATA controller: Intel Corporation Device 43d2 (rev 11)
00:1b.0 PCI bridge: Intel Corporation Device 43c4 (rev 11)
00:1f.0 ISA bridge: Intel Corporation Device 438f (rev 11)
00:1f.3 Audio device: Intel Corporation Tiger Lake-H HD Audio Controller (rev 11)
00:1f.4 SMBus: Intel Corporation Tiger Lake-H SMBus Controller (rev 11)
00:1f.5 Serial bus controller [0c80]: Intel Corporation Tiger Lake-H SPI Controller (rev 11)
00:1f.6 Ethernet controller: Intel Corporation Ethernet Connection (14) I219-LM (rev 11)

```

![image-20220331120442030](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220331120442030.png)

[7-0:0:0D|0:0:3:11] raid_rebuild:                 rrb pass in flashrd parity out



```
[4-2022:8:25|9:49:53:387]    ddr_info:
  > _evt_name:           U0018366_DDR_INFO
  > _DDR_START:          400000000
  > _DDR_SIZE:           400000000
  > _STATIC_DDR_MAP_ADDRESS_MAP_START: 410000000
  > _STATIC_DDR_MAP_ADDRESS_MAP_END: 800000000
  > _DCS_CSR_AUTO_BASE_ADDR_START: 410000000
  > _DCS_CSR_AUTO_BASE_ADDR_END: 410010000
  > _UNALIGNED_BUF_START: 410010000
  > _UNALIGNED_BUF_END:  410410000
  > _DOWNLOAD_BUF_START: 410410000
  > _DOWNLOAD_BUF_END:   410c10000
  > _FW_IMAGE_BUF_START: 410c10000
  > _FW_IMAGE_BUF_END:   411010000
  > _FW_ACTIVATE_BUF_START: 411010000
  > _FW_ACTIVATE_BUF_END: 412010000
  > _COMPARE_DATA_BUF_START: 412010000
  > _COMPARE_DATA_BUF_END: 412418000
  > _PATCH_DIRTY_LIST_START: 412418000
  > _PATCH_DIRTY_LIST_END: 413b18000
  > _VERIFY_DATA_BUF_START: 413b18000
  > _VERIFY_DATA_BUF_END: 413b20000
  > _BM_DDR_BUF_START:   413b20000
  > _BM_DDR_BUF_END:     417b20000
  > _BM_META_BUF_START:  417b20000
  > _BM_META_BUF_END:    417f20000
  > _DDR_BB_TABLE_START: 417f20000
  > _DDR_BB_TABLE_END:   417f7ae40
  > _DDR_INJECT_ERR_TABLE_START: 417f7b000
  > _DDR_INJECT_ERR_TABLE_END: 417f81000
  > _DDR_RFS_SMALL_SHARE_FILE_START: 417f81000
  > _DDR_RFS_SMALL_SHARE_FILE_END: 417f90fec
  > _FTEST_RW_BUF_START: 417f91000
  > _FTEST_RW_BUF_END:   417ff1000
  > _L2P_PATCH_DDR_BUF_START: 417ff1000
  > _L2P_PATCH_DDR_BUF_END: 4183f1000
  > _PF_RW_DDR_BUF_START: 4183f1000
  > _PF_RW_DDR_BUF_END:  4187f1000
  > _FOLDING_RUNTIME_TABLE_START: 4187f1000
  > _FOLDING_RUNTIME_TABLE_END: 4294e7a10
  > _GSD_MEM_BUF_START:  4294e8000
  > _GSD_MEM_BUF_END:    42a963000
  > _TRIM_BITMAP_START:  42a963000
  > _TRIM_BITMAP_END:    42a97ef20
  > _TRIM_PROCESS_BITMAP_START: 42a97f000
  > _TRIM_PROCESS_BITMAP_END: 42a99af20
  > _TRIM_NS_BITMAP_START: 42a99b000
  > _TRIM_NS_BITMAP_END: 42a9b6f20
  > _REVOVERY_WINDOW_START: 42a9b7000
  > _REVOVERY_WINDOW_END: 42b197000
  > _MAPLOG_B2N_BUF_START: 42b197000
  > _MAPLOG_B2N_BUF_END: 42b7ac120
  > _MAPLOG_BUF_START:   42b7ad000
  > _MAPLOG_BUF_END:     42fba7000
  > _FOOTER_BUF_START:   42fba7000
  > _FOOTER_BUF_END:     432ed3000
  > _READ_SCRUB_BUF_START: 432ed3000
  > _READ_SCRUB_BUF_END: 4389b7000
  > _FOLDING_FEED_BUF_START: 4389b7000
  > _FOLDING_FEED_BUF_END: 438df9b00
  > _DDR_MPAGE_SUMMARY_FLUSH_START: 438dfa000
  > _DDR_MPAGE_SUMMARY_FLUSH_END: 4395fa000
  > _DDR_MPAGE_SUMMARY_LOADALL_START: 4395fa000
  > _DDR_MPAGE_SUMMARY_LOADALL_END: 439dfa000
  > _DDR_MPAGE_SUMMARY_LOAD_START: 439dfa000
  > _DDR_MPAGE_SUMMARY_LOAD_END: 439e00000
  > _DDR_L2P_LOCK_START: 439e00000
  > _DDR_L2P_LOCK_END:   43abf8fa8
  > _DDR_GET_LOG_BUF_START: 43abf9000
  > _DDR_GET_LOG_BUF_END: 43ac79000
  > _DDR_NVME_BUF_START: 43ac79000
  > _DDR_NVME_BUF_END:   43ad79000
  > _NOR_DDR_BUF_START:  43ad79000
  > _NOR_DDR_BUF_END:    43af79000
  > _PANIC_DUMP_BUF_START: 43af79000
  > _PANIC_DUMP_BUF_END: 43b179000
  > _DDR_L1_MAP_START:   43b179000
  > _DDR_L1_MAP_END:     43cd6b000
  > _DDR_L2_NODE_START:  43cd6b000
  > _DDR_L2_NODE_END:    44054f000
  > _DDR_PREFETCH_BUF_START: 44054f000
  > _DDR_PREFETCH_BUF_END: 4445ef000
  > _DDR_RSV_START:      4445ef000
  > _DDR_RSV_END:        4562ef000
  > _DDR_L2_MAP_START:   4562ef000
  > _DDR_L2_MAP_END:     800000000

```

```
union Image Location:

http://192.168.39.7/myrtle-builds/release/rc_4.2.5/vU.2.5@e2dce3_16051/

Union image - An image contains bl2, bl3 and PCIe/DDR Config file.

bl3 image - An image file only contains the bl3 production firmware.
```

