### 1、error: ‘vector‘ was not declared in this scope

```
你在程序前面已经声明#include<vector>但是报这个错误，其实这是一个细节问题。
你可能是在程序中直接这样使用了
vector<int>  sim
vector是在std命名空间里的，应该写成下面这样
std::vector<int> sim
或者：
前面需要加上using namespace std;
```

