1. Two Sum

   ```markdown
   Input: nums = [2,7,11,15], target = 9
   Output: [0,1]
   Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
   ```

   solution:

   ```
   如果数组是A=[2,7,11,15],target=9
   那么对应需要的数组是B=[7,2,-2,-6]
   那么对于B中的每个元素，在A中进行查找，如果找到了，就返回index
   那么可以考虑根据A生成无序映射表unordered_map
   那么对于A中的每一个元素a，index=i,如果target - a也在unordered_map中的话，则表示找到了，index=unordered_map[target - a]
   如果没找到，则将unordered_map[a]=i的关系记录下来
   ```

   ```c++
   #include <iostream>
   #include <vector>
   #include <unordered_map>
   using namespace std;
   
   void show_vector(vector<int>& nums) {
       for (auto num:nums) {
           cout << num << " ";
       }
       cout << endl;
   }
   
   class Two_Sum {
   public:
       vector<int> twoSum(vector<int>& nums, int target) {
           printf("Two_Sum\n");
           for (int i = 0; i < nums.size(); i++) {
               for (int j = i + 1; j < nums.size(); j++) {
                   if (nums[i] + nums[j] == target) {
                       return {i, j};
                   }
               }
           }
           return {};
       }
   };
   
   class Two_Sum_2 {
   public:
       vector<int> twoSum_2(vector<int>& nums, int target) {
           unordered_map<int, int> mp;
           printf("Two_Sum_2\n");
           for (int i = 0; i < nums.size(); i++) {
               if (mp.find(target - nums[i]) == mp.end()) {
                   mp[nums[i]] = i;
               } else {
                   return {mp[target - nums[i]], i};
               }
           }
           return {};
       }
   };
   
   int main(int argc, const char** argv) {
       vector<int> nums = {2, 7 ,11, 15};
       int target = 9;
       vector<int> ret = Two_Sum_2().twoSum_2(nums, target);
       show_vector(ret);
       system("pause");
       return 0;
   }
   ```

   **unordered_map**

   ```
   是一个将key和value关联起来的容器，它可以高效的根据单个key值查找对应的value
   key值应该是唯一的，key和value的数据类型可以不相同
   unordered_map存储元素时是没有顺序的，只是根据key的哈希值，将元素存在指定位置，所以根据key查找单个value时非常高效，平均可以在常数时间内完成
   unordered_map查询单个key的时候效率比map高，但是要查询某一范围内的key值时比map效率低
   可以使用[]操作符来访问key值对应的value值
   ```

2. Roman to Integer

   

3. 