unordered_map的使用

```c++
std::unordered_map<std::string, std::int> umap; //定义

umap.insert(Map::value_type("test", 1));//增加

//根据key删除,如果没找到n=0
auto n = umap.erase("test")   //删除

auto it = umap.find(key) //改
if(it != umap.end()) 
    it->second = new_value; 


//map中查找x是否存在
umap.find(x) != map.end()//查
//或者
umap.count(x) != 0

```

## 注意：使用auto循环时候，修改的值作用域仅仅循环之内，出去循环还会变成未修改的数值。

[erase()也不能直接删除，参考这个文章](https://www.cnblogs.com/kex1n/archive/2011/12/06/2278505.html)

```c++
for(auto x:unomap)//遍历整个map，输出key及其对应的value值
{
	x.second = 0;	
	cout<<x.second<<endl;//全是  000；;	
}
cout<<x.second<<endl;//恢复原来的数值的。

彻底改变：使用find彻底找到这个数值，然后在进行改，可以保证作用域是整个程序。
for(auto x:unomap)//遍历整个map，输出key及其对应的value值
{
	auto it = umap.find(key) //改
	if(it != umap.end()) 
	    it->second = new_value; 
}
```

