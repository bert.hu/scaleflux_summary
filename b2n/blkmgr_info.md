#### blk mgr需要设置的参数

```
// although PF block do not care about them, it is better to clear
g_pu_mgr.pblk_mgr[0].clear_sblock_prog_err_happen(sblock);
g_pu_mgr.pblk_mgr[0].clear_sblock_prog_err_bitmap(sblock);
g_pu_mgr.pblk_mgr[0].clear_sblock_switch_flag(sblock);
g_pu_mgr.pblk_mgr[0].set_sblock_last_append_PBA(sblock, INVALID_64BIT);
g_pu_mgr.pblk_mgr[0].set_sblock_last_maplog_PBA(sblock, INVALID_64BIT);
g_pu_mgr.pblk_mgr[0].set_sblock_footer_PBA(sblock, INVALID_64BIT);
g_pu_mgr.pblk_mgr[0].set_sblock_final_user_PBA(sblock, INVALID_64BIT);
g_pu_mgr.pblk_mgr[0].set_sblock_last_prog_b2n_pba_all(sblock, INVALID_64BIT);
g_pu_mgr.pblk_mgr[0].set_sblock_consume_space(sblock, 0);
g_pu_mgr.pblk_mgr[0].set_sblock_maplog_sealed(sblock, 0);

set_sblock_should_do_early_switch_with_check
set_sblock_should_do_early_switch
set_sblock_should_do_normal_switch
set_sblock_should_do_early_switch_and_turn_to_cold_with_check
```

#### early Switch

```c++
VU_STATUS_E vu_handler_triggersblockswitch(vector<string> *pargs __unused, triggersblockswitch *param __unused,
                                           triggersblockswitch_data *pdata __unused)
{
    if (pargs) {
        if (pargs->size() != 2) {
            INF_LOG("Usage: \n");
            INF_LOG("triggersblockswitch stream_id(HOT: 0, HOT2: 1, GC: 2, L2P_PATCH: 3, WL: 4)\n");
            INF_LOG("For example: \n");
            INF_LOG("triggersblockswitch 0\n");
        }
        else {
            long vu_stream_id;
            (*pargs)[1].to_int(vu_stream_id);
            INF_LOG("triggering block switch...\n");
            u32 cur_block = g_b2n_task.act_b2n[vu_stream_id].pu_sblk;
            if (INVALID_32BIT != cur_block) {
                g_pu_mgr.pblk_mgr[0].set_sblock_should_do_early_switch(cur_block);
            }
        }
    }
    else {
        INF_LOG("triggering block switch...\n");
        u32 cur_block = g_b2n_task.act_b2n[pdata->input.stream].pu_sblk;
        if (INVALID_32BIT != cur_block) {
            g_pu_mgr.pblk_mgr[0].set_sblock_should_do_early_switch(cur_block);
        }
    }

    return VU_EXEC_SUCCESS;
}
```

#### head extraction

```
每次做head extraction的时候，拿的都是下一个die的b2n pba
#define HDR_BUF_SIZE  (24 * 1024)
_current_read_pba = get_next_xlat_seq_die_b2n_pba_skip_parity(_current_read_pba);
_current_read_buf = _current_read_buf + HDR_BUF_SIZE;
所以head extraction每次做一个die的，做完再做下一个die
这里每次sent_read_cnt == MAX_HEADER_EXTRACTION_CNT的时候，会去poll，poll完再去发下一批MAX_HEADER_EXTRACTION_CNT的数量

head extraction的解析
smry_rcvr_mgr::parse_header_extraction_result()
hdr_pba.comp_len = CFG_CCP_UNIT_SIZE_4B;
#define CFG_CCP_UNIT_SIZE_4B  (CFG_CCP_UNIT_SIZE>>2)
在这里，parse_header_extraction_result每次解析的是一个die上的（同一个open super block上的，g_pu_mgr.pblk_mgr[0].get_sblock_stream(sblock) == STREAM_ID_L2P)）
while (tot_count < (CFG_FOURK_PER_PLANE * CFG_PLANE_NUM))
所以这里，每次做一个4K，总共做一个die上的总的4K数量，即(CFG_FOURK_PER_PLANE * CFG_PLANE_NUM)
每个head extraction的hdr头结构
struct header_abs_s
{
    ccp_header ccp_hdr;
    u64        lpa;
};
如果只解析mpage
if ((hdr->ccp_hdr.signature == HW_SIGNATURE) && (LP_GET_USER_TYPE(hdr->lpa) == USER_L2P_META))
l1_id如何通过hdr来获取？
u32 l1_id = hdr->lpa & MPAGE_L1_ID_MASK;
如果l1_id是有效的，则更新g_mpage_gc_task.mpage_update_summary(l1_id, hdr_pba.all);
然后拿到这个die上面下一个要做head extraction的4K的pba
hdr_pba.all = get_next_summary_pba(hdr_pba.all);
下一个的hdr_pba要通过当前的hdr_pba以及hdr_pba.comp_len来计算下一个pba
/* cal next ccp pba, by pba and comp_size_4B */
函数：pba_append_by_comp_size
```

#### rcvry_pf_ftl_ctrl::patch_load_dispatch()

```
load l2p patch
找到的是g_pu_mgr.pblk_mgr[0].get_sblock_stream(sblock) == STREAM_ID_L2P_PATCH)的block
初始化_dirty_mpage_bitmap

// temporarily use download buffer
_dirty_mpage_bitmap      = (u64 *)DOWNLOAD_BUF_START;
_dirty_mpage_bitmap_size = DIV_ROUND_UP(MAX_L1_MAP_ENTRY_NUM, 64) * sizeof(u64);
DIV_ROUND_UP表示向上取整，本质是取更大的

_pingpong_operation[PING_OPERATION]._start_read_buf = L2P_PATCH_DDR_BUF_START;
_pingpong_operation[PONG_OPERATION]._start_read_buf = L2P_PATCH_DDR_BUF_START + (L2P_PATCH_DDR_BUF_SIZE / 2);

关键步骤：read_l2p_patch
ftl_ctrl._patch_parse_mode = PATCH_PARSE_MODE_RECOVER_DIRTY_MPAGE;
将(u8 *)_pingpong_operation[operation]._current_read_buf的内容读取到_current_read_pba
但这个每次是读16K，所以要拆分成4K每个方便解析
这16k记录到g_l2p_patch_mgr._buffer_id_2_pba[buffer_id] = pba
其中buffer_id = 0~3，一共4个4k
第二步骤：dispatch_loaded_patches_in_pf_meta
将记录在g_l2p_patch_mgr._patch_buf_in_pf_meta的地址的内容拷贝到g_l2p_patch_mgr.get_buffer_addr(buffer_id)
然后将parse_request放到g_l2p_patch_mgr._parse_request_fifo[group_id]队列中
然后让别的核去做解析
第三步骤：load_mpage  // 还得再看看
// load all the mpage for full cache, load dirty mpage for partial cache
成功后ftl_ctrl._patch_parse_mode = PATCH_PARSE_MODE_RECOVER_L2P;
然后继续
patch_load_dispatch
依旧是将buffer_id和pba记录到g_l2p_patch_mgr._buffer_id_2_pba[buffer_id] = pba
再进行恢复recovery window
// restore recovery window
if (_patch_parse_mode == PATCH_PARSE_MODE_RECOVER_L2P) {
patch_idx = g_l2p_patch_mgr._recovery_window.append_patch(group_id);
BOMB(RECOVERY_WINDOW_APPEND_PATCH_ERROR, patch_idx != INVALID_32BIT);
g_l2p_patch_mgr._recovery_window.set_patch_seq_num(patch_idx);
g_l2p_patch_mgr._recovery_window.patch_map_pba(patch_idx, g_l2p_patch_mgr._buffer_id_2_pba[buffer_id]);
}
最后将parse_request放到g_l2p_patch_mgr._parse_request_fifo[group_id]队列中
然后让别的核去做解析

解析成功就放到g_l2p_patch_mgr._parse_complete_fifo[group_id]
用于记录解析成功的个数，只要fifo不为空，就依次pop出来

最后一个步骤：partial blk handle
pb_handle_parse_hdr_extraction_result(1)


6652 [2022-05-11 11:00:46.476] [6-0:0:0D|0:0:0:934]   restore_l2p_patch_meta_data:
6653 [2022-05-11 11:00:46.476]   > patch_start_pba:     20300001432a84
6654 [2022-05-11 11:00:46.481]   > buffer_cnt:             4

7221 [2022-05-11 11:00:48.851] [6-0:0:0D|0:0:1:297]   dispatch_patch_in_pf_meta:
7222 [2022-05-11 11:00:48.856]   > buffer_cnt:             4

7242 [2022-05-11 11:00:48.899] [6-0:0:0D|0:0:4:34]   patch_parse_start:
7243 [2022-05-11 11:00:48.903]   > mode:                   0
7244 [2022-05-11 11:00:48.906]
7245 [2022-05-11 11:00:48.906]
7246 [2022-05-11 11:00:48.907] [6-0:0:0D|0:0:4:34]   patch_load_dispatch:
7247 [2022-05-11 11:00:48.912]   > current_read_sblock:    5
7248 [2022-05-11 11:00:48.915]   > patch_parse_mode:       0
7249 [2022-05-11 11:00:48.918]   > start_read_pba:      20300001432a84
7250 [2022-05-11 11:00:48.922]   > end_read_pba:         148a800
7251 [2022-05-11 11:00:48.925]   > patch_start_pba:     20300001432a84
7252 [2022-05-11 11:00:48.929]
7253 [2022-05-11 11:00:48.930]
7254 [2022-05-11 11:00:48.931] [6-0:0:0D|0:0:4:484]   dispatch_patch_in_pf_meta:
7255 [2022-05-11 11:00:48.936]   > buffer_cnt:             4


```

#### 如何获取一个block上的end read pba

```c++
u64 rcvry_pf_ftl_ctrl::get_end_read_pba(u64 cur_sblock)
{
    u64 b2n_pba = g_pu_mgr.pblk_mgr[0].get_sblock_last_prog_b2n_pba_all(cur_sblock);
    if (b2n_pba != INVALID_64BIT) {
        b2n_pba = get_next_xlat_seq_die_b2n_pba_skip_parity(b2n_pba);
    }

    return b2n_pba;
}

这里怎么做的呢？
    首先，传入的start_pba和cur_sblock，如果start_pba == INVALID_64BIT,那么就只能通过cur_sblock来获取start_read_pba,先拿到这个cur_sblock上写的最后一个b2n pba，如果这个block写了数据，那么这个值就不是INVALID_64BIT，然后就要想办法通过这个last_prog_b2n_pba来拿到这个block上的第一个pba，所以拿个空的pba，重新设置这个pba的block，page=0，phy_die就可以了，而block就是那么就只能通过cur_sblock,phy_die就是当前这个cur_sblock的xlat_seq_die=0的phy_die
    其次，再来考虑start_pba != INVALID_64BIT的情况，先拿到这个start_pba所在的block，看和cur_sblock是否一致，如果一致，那么就用start_pba来计算start_read_pba,CLEAR_OFFSET_FROM_PBA就是将pba的offset位清零
    
union pba_t {
    struct {
        u64 offset      : CFG_PBA_OFFSET_BITS; //4K unit
        u64 plane       : CFG_PBA_PLANE_BITS;
        u64 page_type   : CFG_PBA_PAGE_TYPE_BITS;
        u64 ch          : CFG_PBA_CHAN_BITS;
        u64 ce_lun      : CFG_PBA_CE_LUN_BITS;
        u64 page        : CFG_PBA_PAGE_BITS;
        u64 block       : CFG_PBA_BLOCK_BITS;
        u64 ep_idx      : CFG_PBA_EP_IDX_BITS;  //bit 0 = first 2k, 1 = second 2k in 4KB read
        u64 ccp_off     : CFG_PBA_CCP_OFF_BITS; //4B
        u64 comp_len    : CFG_PBA_COMP_LEN_BITS;  //4B
        u64 rsvd        : (64 - PBA_ALL_BITS); //for sim, rsvd is 12bit
    };
    u64 all;
};

u64 rcvry_pf_ftl_ctrl::get_start_read_pba(u64 start_pba, u64 cur_sblock)
{
    u64 b2n_pba;

    if (start_pba == INVALID_64BIT) {
        b2n_pba = g_pu_mgr.pblk_mgr[0].get_sblock_last_prog_b2n_pba_all(cur_sblock);
        if (b2n_pba != INVALID_64BIT) {
            b2n_pba = get_start_xlat_seq_die_b2n_pba(cur_sblock);
        }
    }
    else {
        u64 start_sblock = GET_BLOCK_FROM_PBA(start_pba);

        if (start_sblock == cur_sblock) {
            CLEAR_OFFSET_FROM_PBA(start_pba);
            b2n_pba = start_pba;
        }
        else {
            b2n_pba = g_pu_mgr.pblk_mgr[0].get_sblock_last_prog_b2n_pba_all(cur_sblock);
            if (b2n_pba != INVALID_64BIT) {
                b2n_pba = get_start_xlat_seq_die_b2n_pba(cur_sblock);
            }
        }
    }

    return b2n_pba;
}

_current_read_pba = get_start_xlat_seq_die_b2n_pba(blk_id);
_end_read_pba     = get_next_xlat_seq_die_b2n_pba_skip_parity(last_prog_pba);
```

![image-20220518174445182](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220518174445182.png)![image-20220518174708852](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220518174708852.png)

![image-20220518174820049](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220518174820049.png)

![image-20220518175157469](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220518175157469.png)

![image-20220518175225180](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220518175225180.png)

DIE和ce&lun的关系

```
u64 die = pba.ce_lun * CFG_CHAN_NUM + pba.ch;
```

