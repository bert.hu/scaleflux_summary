```sequence
Title: 主机端和SSD的前端的写交互操作
主机 -> 前端: Write FPDMA命令FIS(Frame Information Structure)
Note right of 前端: 判断内部的写缓存是否有空间接收新的数据
Note right of 前端: 如果没有，则不发，处于流控状态，主机等待
前端 --> 主机: 如果Write buffer有空间，则发出DMA Setup FIS给主机端
主机 --> 前端: 发送不大于8KB数据的Data FIS
前端 -> 主机: Status FIS，表示协议层面这条写命令完成
前端 --> 中端FTL: 命令解析Command Decoder将FIS解析给FTL
Note over 前端, 中端FTL: 命令属性是读还是写
Note over 前端, 中端FTL: 写命令的起始LBA和数据长度
Note over 前端, 中端FTL: 命令其他属性，比如FUA
participant 后端
Note right of 后端: 还没轮到
```

```sequence
Title: NVMe命令处理
主机 -> 主机: 主机写命令到SQ
主机 -> SSD控制器:更新SQ Tail DB寄存器
Note right of SSD控制器: 取出SQ的命令
SSD控制器 -> SSD控制器:更新SQ Head DB
Note right of SSD控制器: 执行SQ的命令
SSD控制器 -> SSD控制器:更新CQ Tail DB
SSD控制器 -> 主机:写完成信息到CQ，并中断通知主机
主机 -> 主机: 取出CQ中的完成信息
主机 -> SSD控制器:更新CQ Head DB寄存器
```

