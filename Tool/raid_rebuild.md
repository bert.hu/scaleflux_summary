```
24913 [2022-10-08 10:49:33] UD2150C0117H@CSDU4SPC76A0:/$ n2p 0x300000731000
24914 [2022-10-08 10:49:36] [4-0:0:0D|5:39:26:66] read_scrub_t:                     pba : 0xe60000c4
24915 [2022-10-08 10:49:36] UD2150C0117H@CSDU4SPC76A0:/$ plloff 1
24916 [2022-10-08 10:49:48] [4-0:0:0D|5:39:37:266]   admin_task:                     pll normal mode:1 1
24917 [2022-10-08 10:49:48] UD2150C0117H@CSDU4SPC76A0:/$ softraidrd 0xe60000c4 1 detail
```

```
ERASE_PAGE_THR
arch_disable_scrambler

#define FCE_CSR_AUTO_BASE_ADDR      (0x23160000)

#define FCE_CSR_AUTO_RD(reg_off)           bsp_platform::reg_rd32(FCE_CSR_AUTO_BASE_ADDR + reg_off)
#define FCE_CSR_AUTO_WR(reg_off, value)    bsp_platform::reg_wr32(FCE_CSR_AUTO_BASE_ADDR + reg_off, value)

#define ERASE_PAGE_THR      (0x00c0)    /* erase page threshold. */

// ERASE_PAGE_THR
typedef union {
    u32 data;
    struct {
        u32 erase_page_thr:8;
        u32 zero_page_thr:8;
        u32 reserved0:16;
    } bits;
} ERASE_PAGE_THR_U;
```

