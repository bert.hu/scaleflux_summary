```
root@tcnsh-Precision-3650-Tower:/home/tcnsh/myrtle_fw/utility/scripts/customer_tools/lib# python3 ./sfx_tools_lib.py
--Return--
> /home/tcnsh/myrtle_fw/utility/scripts/customer_tools/lib/sfx_tools_lib.py(1350)<module>()->None
-> pdb.set_trace()
(Pdb) Version().showInfo()
 <class '__main__.Version'>    level:0
build_time                    Oct 18 2022 16:34:04
git_origin_info               git@bitbucket.org:sfxsw/myrtle_fw.git
git_branch_info               HEAD -> release/rc_4.3.0
git_commit_info               0b889c5657ba4d320ed055eefe9ea0eeb9ca3162
git_author_info               bert <bert.hu@scaleflux.com>
git_time_info                 2022-10-17 12:00:30 +0800
fw_version_info               vU.3.0@0b889c_19049
idfy_fw_ver                   U3019049
opn                           0x14C38A0
pcb_ver                       A21
fbid                          2
runmode                       3
ddr_freq                      1600
ddr_size                      17179869184
raw_cap                       4096
default_provision_cap         3840
default_format_cap            3840
default_max_lba               7501476527
default_real_op               6
sys_freq                      2250000000
cpu_freq                      1500000000
pcie_pba                      36864
vdd_config                    0.83v
vcc_config                    0
vpp_config                    0
bl2_version                   B2718127@db2e4
latest_compatible_ver         16893
ddr_cfg_ver                   0013
pcie_cfg_ver                  0010
```

```
vu_handler_gpiowrite
sudo sfx-recovery --cmd "updateNorImg /dev/nvme0 file=./test.bin image=1"
sudo sfx-recovery --cmd "updateNorImg /dev/nvme0 file=./gold.bin image=1"
```

```
XScope> boots 0
 >>>>Start program 4 B data at 0x01ff0000...
Update boot_opt 0 to Nor.
XScope>
XScope>
XScope>
XScope> pf
XScope> exit
Exiting...
root@tcnsh-Precision-3650-Tower:/home/tcnsh/myrtle_fw/utility/scripts#
root@tcnsh-Precision-3650-Tower:/home/tcnsh/myrtle_fw/utility/scripts#
root@tcnsh-Precision-3650-Tower:/home/tcnsh/myrtle_fw/utility/scripts#
root@tcnsh-Precision-3650-Tower:/home/tcnsh/myrtle_fw/utility/scripts# setpci -s 1:0.0 0x4.l=100006
root@tcnsh-Precision-3650-Tower:/home/tcnsh/myrtle_fw/utility/scripts#
root@tcnsh-Precision-3650-Tower:/home/tcnsh/myrtle_fw/utility/scripts#
root@tcnsh-Precision-3650-Tower:/home/tcnsh/myrtle_fw/utility/scripts#
root@tcnsh-Precision-3650-Tower:/home/tcnsh/myrtle_fw/utility/scripts# python3 xscope.py
Xscope 0.1.0
Copyright (C) Scaleflux Inc. 2021

XScope>
XScope>
XScope> help

Documented commands (type help <topic>):
========================================
boots        help                ncl2                 nor_program_img  read
cmdtbldump   hwdump              ncl3                 nor_read_data    read_ddr
dump_ddr     jlink_read          ndl2                 nor_write_data   set_dev
dump_evtlog  jlink_script_read   nid                  npt              set_slot
exit         jlink_script_write  nor_erase_sector     nvmedump         shell
fcelmemdump  jlink_write         nor_erase_subsector  pf               write

XScope> ncl3
update protect area by status 0x37
update protect area by status 0x60
BL3 img erased.
XScope>
XScope>
XScope>
XScope> pf
XScope> dump_evtlog 0x40054cda0
XScope> read 0x231800e4 1
```

parity buffer

```c++
bool bsp_platform::check_no_free_parity_buffer()
{
    BM_RSC_STATUS_20_U bm_rsc_status_20;
    bm_rsc_status_20.data = BM_CSR_AUTO_RD(BM_RSC_STATUS_20);
    u32 vp2_ocpy_cnt = bm_rsc_status_20.bits.vp2_ocpy_cnt; // num of 2K (1 pool unit is 2K)

    BM_POOL_CFG_U bm_pool_cfg;
    bm_pool_cfg.data = BM_CSR_AUTO_RD(BM_POOL_CFG);
    u32 vp2_total_cnt = bit_count(bm_pool_cfg.bits.vpool_map_2) * 256; // num of 2K (pool size is 512K, 1 pool unit is 2K)

    return ((vp2_total_cnt - vp2_ocpy_cnt) < (CFG_ONE_PROG_NUM * 2)); // num of 2K (1 pool unit is 2K)
}

bool bsp_platform::monitor_parity_buffer()
{
    BM_RSC_STATUS_20_U bm_rsc_status_20;
    bm_rsc_status_20.data = BM_CSR_AUTO_RD(BM_RSC_STATUS_20);
    u32 vp2_ocpy_cnt = bm_rsc_status_20.bits.vp2_ocpy_cnt; // num of 2K (1 pool unit is 2K)

    BM_POOL_CFG_U bm_pool_cfg;
    bm_pool_cfg.data = BM_CSR_AUTO_RD(BM_POOL_CFG);
    u32 vp2_total_cnt = bit_count(bm_pool_cfg.bits.vpool_map_2) * 256; // num of 2K (pool size is 512K, 1 pool unit is 2K)

    /*
    raid rebuild will be safe if remaining parity buffer bigger than 0xe8 
        (232 * 2kb = 64KB x 6 streams + 80KB for rebuild)
    */
    return ((vp2_total_cnt - vp2_ocpy_cnt) > 0xe8);
}
```

