```
        if (_full_cache_flag) {
            _gdma_jobid = sys_gdma_memset((void *)_l2_map, INVALID_PBA, L1_MAP_ENTRY_NUM * L2_MAP_ENTRY_SIZE);
        }
        
```

```
p g_recovery_task ._pf_ctrl .ftl_ctrl ._rebuild_valid_lpa_cnt[1]
p g_pu_mgr.pblk_mgr[0]._block_table[1]._valid_lpa_cnt
set print elements 0

set print pretty on
set print array-indexes on

set logging file /home/tcnsh/debug_simulator_1.log
set logging on

p *(l1_map_entry*)g_l2p_mgr ._l1_map@8192
p g_l2p_mgr ._l2_map[0]@16

p g_l2p_mgr ._l1_map[8191]

334333

NOT-SET@:/$ l2pdbg 1
init state: 0, init_idx 0x0
2K_MPAGE 0, RAW_CAPACITY 0xc800000000, DDR_SIZE 3221225472, provision_capacity 701, format_capacity 701, cache ratio 100, MAX_LBA 0x519fce0f, MAX_LPA 0xa33f9c1
L1_MAP_ENTRY_NUM 0x519fd, L2_MAP_ENTRY_NUM 0x519fd, max_cache_l2_num 0x7edee, MAX_CACHE_L2_MAP_ENTRY_NUM 0x7edee, DEFAULT_L1_MAP_ENTRY_NUM 0x519fd, MAX_L1_MAP_ENTRY_NUM 0xa33f5, MAX_DIRTY_LIST_LEN 0x2f938
DDR_L2P_LOCK_SIZE 0x28cfd4, DDR_L1_MAP_SIZE 0x51a000, DDR_L2_MAP_SIZE 0x7edee000, DDR_L2_NODE_SIZE 0xa34000, ALLOC_L2_MAP_SIZE 0x7edee000
ptr: lock 0x429d17000, l1_map 0x42a524000, l2_map 0x441212000, l2_node 0x42aa3e000


(gdb) p g_recovery_task ._pf_ctrl .ftl_ctrl ._rebuild_valid_lpa_cnt[1]@200
$9 = {0, 944065, 0, 36825, 0, 0, 1006177, 1037401, 987612, 1018469, 1093087, 1000096, 1019554, 1009779, 1059353, 1060445, 880817, 0 <repeats 99 times>, 932404, 1801609, 1480022, 963107, 1012691,
  963150, 99157, 0 <repeats 76 times>, 44, 0, 3, 257, 3, 1, 3, 6, 1, 0, 71303, 0, 75335, 0, 11, 12, 1, 16, 0, 1, 84505072, 0, 75339, 0, 76777, 0, 14, 8, 16, 1, 76779, 0, 77045, 0, 5, 6, 0, 0,
  77049, 0, 77155, 0 <repeats 11 times>, 41, 0, 83, 0, 0, 0, 4032, 0, 1438, 0, 266, 0, 106, 0, 0, 0, 0, 0, 0, 0, 256, 0, 65536, 16, 0 <repeats 525 times>}
```

```
以 16 进制格式打印数组 a 前 16 个 word（4 个 byte）的值： 

(gdb) x/16xw a
0x7fffffffe4a0: 0x03020100      0x07060504      0x0b0a0908      0x0f0e0d0c
0x7fffffffe4b0: 0x13121110      0x17161514      0x1b1a1918      0x1f1e1d1c
0x7fffffffe4c0: 0x23222120      0x27262524      0x2b2a2928      0x2f2e2d2c
0x7fffffffe4d0: 0x33323130      0x37363534      0x3b3a3938      0x3f3e3d3c
```

```
PF_RECOVERY_VPC_MISMATCH分析
p g_pu_mgr.pblk_mgr[0]._block_table[196]._valid_lpa_cnt
(gdb) p _rebuild_valid_lpa_cnt
$4 = {0 <repeats 17 times>, 39234, 0 <repeats 156 times>, 192669, 6154, 1831769, 1943437, 1880627, 1927167, 1418397, 586414, 189440, 0, 1860260, 1903739, 1920244, 1920172, 1936424, 1927183, 0,
  1927120, 1328, 0, 0, 0, 0, 0, 0, 0}


(gdb) p g_pu_mgr.pblk_mgr[0]._block_table[193]._stream
$29 = {_v = 136}

135922 [7-2022:11:23|16:37:49:891]    U0019878_GC:[5-2022:11:23|16:37:49:891]    U0019878_B2N_GET_SBLOCK:
135923
135924  > _blk_id: c1                        > _event: GC_CANDIDATE_PICK_SUCCESS  > _pe_cnt: 3                         > _last_state: USER_GC_TASK_STATE_IDLE > _stream: 0                                > _state: USER_GC_TASK_STATE_SRC_PICKING > _last_program_b2n_pba: ffffffffffffffff
135925  > _cur_trig_type: 1
135926  > _recovered_consume_space: 0        > _cur_src_blk: ac                   > _good_plane_num: 7d                > _deallocated_sblock_num: a7        > _is_max_free: false                >        _real_free_sblock_num: a7          > _target_blk: a9
135927  > _recycle_fifo_num: 0               > _cur_src_pba: 15800000
135928  > _sealed_blk_num: 0
135929  > _early_switch_blk: ffffffff        > _free_blk_cnt: a7                  > _tot_release_4k_cnt: 0


231861 [6-1970:1:1|0:0:0:87]    U0019878_B2N_GET_SBLOCK:
231862  > _blk_id: c1                        > _pe_cnt: 3                         > _stream: 0                         > _last_program_b2n_pba: 18201780
231863  > _recovered_consume_space: 5b4820   > _good_plane_num: 7d                > _deallocated_sblock_num: a9        > _real_free_sblock_num: 9
231864  > _recycle_fifo_num: 0               > _sealed_blk_num: 13

```

```
253474 [4-0:0:0D|0:39:27:658]   admin_task:      ======== 2022-11-24_01-45-13: EH_ProgramErrorOnDifferentPageType.py Start ========

```

