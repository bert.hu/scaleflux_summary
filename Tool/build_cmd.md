```shell
#! /bin/sh



buildType=$1


./utility/prebuild.sh asic.wout.bl3 
if test $? -ne 0
then
	exit 1
fi

#B47R 4T, 8G DDR
opn=CSDU4SPC38A0
./utility/dobuild.sh asic.wout.bl3 ${opn}  $buildType
if test $? -ne 0
then
    exit 1
fi

./utility/dobuild.sh elog_parser ${opn}  $buildType
if test $? -ne 0
then
    exit 1
fi

#B47R 8T, 8G DDR
opn=CSDU4SPC76A0
./utility/dobuild.sh asic.wout.bl3 ${opn}  $buildType
if test $? -ne 0
then
    exit 1
fi

./utility/dobuild.sh elog_parser ${opn}  $buildType
if test $? -ne 0
then
    exit 1
fi

```

```shell
#!/bin/bash

buildType=$1

#include opn_array
abspath=$(cd "$(dirname "$0")";pwd)
source $abspath/opnmap

./utility/prebuild.sh asic.wout.bl3
if test $? -ne 0
then
	exit 1
fi

for opn in $opn_arry
do
	echo "build bl3 for $opn"
	./utility/dobuild.sh asic.wout.bl3 ${opn}  $buildType
	if test $? -ne 0
	then
		exit 1
	fi

	./utility/dobuild.sh elog_parser ${opn}  $buildType
	if test $? -ne 0
	then
		exit 1
	fi

done

```

