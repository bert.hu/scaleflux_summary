Conventional Reset(hot reset)：

```
1. Check PCIe link address：0000:01:00.0
[root@localhost ~]# lspci |grep No
01:00.0 Non-Volatile memory controller: ScaleFlux Inc. Device 0010

2. Get PCIe bridge address：0000:00:01.0
[root@localhost ~]# ll /sys/bus/pci/devices/0000\:01\:00.0
lrwxrwxrwx. 1 root root 0 Jun 6 14:10 /sys/bus/pci/devices/0000:01:00.0 -> ../../../devices/pci0000:00/0000:00:01.0/0000:01:00.0

3. trigger hot reset:
sudo setpci -s 0000:00:01.0 3E.b=0x42
sudo setpci -s 0000:00:01.0 3E.b=0x02

4. PCIe remove/rescan:
echo 1 >/sys/bus/pci/devices/0000\:01\:00.0/remove
echo 1 >/sys/bus/pci/rescan
```

PCIe link down/up:

```
1. Check PCIe link address：0000:01:00.0
[root@localhost ~]# lspci |grep No
01:00.0 Non-Volatile memory controller: ScaleFlux Inc. Device 0010

2. Get PCIe bridge address：0000:00:01.0
[root@localhost ~]# ll /sys/bus/pci/devices/0000\:01\:00.0
lrwxrwxrwx. 1 root root 0 Jun 6 14:10 /sys/bus/pci/devices/0000:01:00.0 -> ../../../devices/pci0000:00/0000:00:01.0/0000:01:00.0

3. trigger pcie link down/up：
sudo setpci -s 0000:00:01.0 CAP_EXP+10.b=0x50
sudo setpci -s 0000:00:01.0 CAP_EXP+10.b=0x60

4. PCIe remove/rescan:
echo 1 >/sys/bus/pci/devices/0000\:01\:00.0/remove
echo 1 >/sys/bus/pci/rescan
```

Function Level Reset:

```
1. Check PCIe link address：0000:01:00.0
[root@localhost ~]# lspci |grep No
01:00.0 Non-Volatile memory controller: ScaleFlux Inc. Device 0010

2. trigger FLR
echo 1 > /sys/bus/pci/devices/0000:02:00.0/reset
```

Controller Reset (CC.EN transitions from ‘1’ to ‘0’):

```
1. trigger controller reset：
nvme reset /dev/nvme0
```

NVM Subsystem Reset:

```
1. trigger subsystem reset：
nvme subsystem-reset /dev/nvme1
```

### Change Capacity with Thin Provisioned Namespaces

Function Spec: [Extended Capacity in NVMe.docx](https://netorg243736.sharepoint.com/:w:/g/Eb2thuGzITtOi94ffAqUS6kBsPlqtCvyVGC0gT73IiOXew?e=zTRdCh)

An example, extend a 3.84TB drive to 7.68T with sector size 4096:

```
sudo nvme delete-ns /dev/nvme0 --namespace-id=0xffffffff
sudo nvme ns-rescan /dev/nvme0
sudo nvme create-ns /dev/nvme0 --nsze=1875366486 --ncap=937684566 --block-size=4096  
sudo nvme attach-ns /dev/nvme0 --namespace-id=1 --controllers=0
sudo nvme ns-rescan /dev/nvme0
```

# How to upgrade a union image

A union image is a image contains the bl2 (bootloader) and golden bl3 (production fw) and the PCIe/DDR config file. 

In the release directory download the asic_img.tar.gz and find the union image in the tarball. Current  DDR frequency is 2933MHz, so choose the 2933 version with matching OPN. 

Use sfx-recovery tool mentioned in Q3 to upgrade the union image that stored in NOR flash:

```
sudo sfx-recovery --cmd "updateNorImg /dev/nvme0n1 file=/home/tcn/myrtle_fw.asic.union.dev_build.csdu5spc38m1_U2516051.ddr_2933.signed.bin image=255"
```

After upgrading the union image, then upgrade the user image in root filesystem in NAND flash following the instruction in Q2. Then clean the drive by following command:

```
sudo sfx-recovery --cmd "cleanCard /dev/nvme0n1"
```

