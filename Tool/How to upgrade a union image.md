```
A union image is a image contains the bl2 (bootloader) and golden bl3 (production fw) and the PCIe/DDR config file. 

In the release directory download the asic_img.tar.gz and find the union image in the tarball. Current  DDR frequency is 2933MHz, so choose the 2933 version with matching OPN. 
```

Use sfx-recovery tool mentioned in Q3 to upgrade the union image that stored in NOR flash:

```
sudo sfx-recovery --cmd "updateNorImg /dev/nvme0n1 file=/home/tcn/myrtle_fw.asic.union.dev_build.csdu5spc38m1_U2516051.ddr_2933.signed.bin image=255"
```

img下载地址

```
http://192.168.39.7/myrtle-builds/release/
http://192.168.39.7/myrtle-builds/active-dev/
```

