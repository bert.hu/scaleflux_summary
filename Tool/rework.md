VU to update nor boot image
==
### 0. break the rework-cable to let it in ROM boot mode



### 1. Update BL3 nor image and switch to fdl to support new VU programNor

~~~
cd utility/scripts/customer_tools/lib
sudo python ./sfx_tools_lib.py
(Pdb) UpdateNorImg(dev='/dev/nvme0',fileName='/home/tcn/whou/bl3.signed.bin',image=1)
(Pdb) SetBootOption(dev='/dev/nvme0', cdw14=0)

~~~

> bl3.signed.bin is the bl3 dev_build signature image.
>
> eg. myrtle_fw.asic.wout.bl3.dev_build.csdu5spc38m1_vU.0.0\@670f07_17034.strip.elf.signed.bin 
>
> **Note** only dev_build bl2/bl3 image support ndl1 uart_cmd or programNor VU cmd.

### 2. Power cycle card

### 3. program *nor.img to update new bl1/bl2 to nor flash

~~~
cd utility/scripts/customer_tools/lib
sudo python ./sfx_tools_lib.py
(Pdb) ProgramNor(dev='/dev/nvme0',fileName='/home/tcn/whou/nor.img',norOffset=0)
~~~

>nor.img is the auto generate nor bin file (include bl1 and bl2) when do bl2 build.  
>
>eg.  myrtle_fw.asic.bl2.dev_build.csdu5spc38m1_vU.0.0\@670f07_17034.nor.img



#### Option 3.1 Check Image program status by xscopy

~~~
[mat_tests@localhost scripts]$ cd utility/scripts/
[mat_tests@localhost scripts]$ sudo python2 ./xscope.py --cmd "read 0x80000000"
    Access:/sys/bus/pci/devices/0000\:01:00.0/resource0
    0x80000000: d53800a1
    
~~~

> #Confirm the 1st 4bytes is the same as 1st 4bytes (**d53800a1**) in nor.img . All *.nor.img should has the same 1st 4bytes as they are 1st instruction in bl1.bin which are the same

#### Option 3.2 Recovery the bootOption to previous slot

~~~
cd utility/scripts/customer_tools/lib
sudo python ./sfx_tools_lib.py
(Pdb) SetBootOption(dev='/dev/nvme0', cdw14=1)
~~~

### 4. rework card to nor boot mode by connect the rework-cable

### 5. power on to check boot success or not

> sfx-status shows card goto fdl mode if did not do step_3.2
>
> sfx-status show card goto user mode if did setp_3.2 if the slot has valid user image

