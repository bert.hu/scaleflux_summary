```c++
        if (bsp_platform::spi_nor_4k_erase(CFG_NOR_EVT_LOG_BLK_INFO_START + i*NOR_SUB_SECTOR_SIZE)) {
            INF_LOG("nor erase fail\n");
        }

    u32 nor_addr = CFG_NOR_RESET_FLAG_START;
    u32 nor_data = (u32)(0<<24 | reset_value);
    u32 *data_ptr  = (u32 *)(nor_data);
    NOR_OPERATION_STATUS_E job_status_0 = (NOR_OPERATION_STATUS_E)bsp_platform::spi_nor_write_data(nor_addr, data_ptr, data_size);
    if (job_status_0 != NOR_OPERATION_SUCCESS) {
        INF_LOG("%s, reset flag write nor failed\n", __FUNCTION__);
    }

    // Use the value of the MFR_SPECIFIC_00 register to determine whether a reset has occurred
    u8 reset_value = (u8)bsp_platform::pmic_get_reset_value();
    if (reset_value != 0) {
        INF_LOG("Waring!!! it is reset, MFR_SPECIFIC_00 0x%x\n", reset_value);
    }

```

