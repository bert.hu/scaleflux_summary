```
https://scaleflux.atlassian.net/wiki/spaces/MYR/pages/94502971/Xscope+user+guide
```

```
首先打开debug开关
/home/tcnsh/myrtle_fw/utility/scripts/xscope_myrtle
vim xspi_nor.py

#SPI NOR ACCESS Class
class XSpi_nor:
    def __init__(self, haps=0, debug=0, verbo=0, dev="", slot=""):
        # debug output
        self.debug = 1

然后
/home/tcnsh/myrtle_fw/utility/scripts
make
生成
i2c_pf.c
pcimem.c

然后lspci -d cc53:
01:00.0 Non-Volatile memory controller: ScaleFlux Inc. Device 0010
执行
sudo python3 xscope.py --pciPort 1

chip reset nor addr
0x117c000

[tcnsh@bogon scripts]$ 
sudo python3 xscope.py --pciPort 01 | tee /home/tcnsh/hwdump.log
sudo python3 xscope.py --pciPort [portid] | tee /home/tcnsh/hwdump.log

2.1.6 Evtlogdump command
Evtlogdump command is used to dump evtlog 1M buffer original bin file from DDR. 

The following is the write command usage:

XScope> dump_evtlog 0x40054cda0

>>> Reading original data from DDR ......

100%|███████████████████████████████████████████████████| 262144/262144 [1:21:19<00:00, 53.72it/s]

>>> tail = 786398 sv_dw = 196599.50

>>> Output original dump_evtlog_2022_10_13_11_00_33.bin success!

Note: The start address of the buffer, such as 0x40054cda0, is not same in different firmware, it can be found in the map file of the firmware by search  keyword “g_evtbuf“ as shown below:
```

```c++
 .bss.g_evtbuf  0x000000040054cda0   0x100060 ./platform/obj/myrtle_fw.asic.wout.bl3.RELEASE_BUILD.csdu5spc38c1/evt_logger.o
                0x000000040054cda0                g_evtbuf
                
```

```
2.1.7 Nvmedump command
Nvmedump command is the same as uart command “nvmedbg“. It can help us to dump nvmedbg register from PCIe. 
We can use this command like Chapter 2.1.5 to save the nvmedbg log, need to replace the hwdump with nvmedump:

XScope> nvmedump
```

```
.bss.g_pf_mgr  0x0000000406d17dd8

.bss.g_assert_handler
                0x0000000400364f10

.bss.g_evtbuf  0x000000040056ce80
```

```
如何计算ddr来读g_pf_mgr的关键数据
0x406d17dd8 这个是g_pf_mgr的起始ddr地址
class pf_mgr
{
public:
    pf_root_data root_data;       // Saved in Nor or RFS
    pf_summary_data summary_data; // Saved in PF Block
}

pf_root_data的大小是19个u32，所以pf_summary_data的起始地址是 0x406d17dd8 + 0x4c = 0x406D17E24
0x406d17e24: 0xba1aba1a,可以看到这里是pf_summary_data中的pf_mapping_info的signature

struct pf_summary_data {
    pf_mapping_info mapping_info[STREAM_ID_MAX];
    pf_parity_info parity_info[STREAM_ID_MAX];
    u64 meta_data_pba[PF_META_DATA_PBA_CNT];
} __attribute__((packed));
其中pf_mapping_info的大小是963个u32，即0xF0c字节
其中pf_parity_info的大小是22个u32，即0x58字节
pf_summary_data的起始地址是 0x406D17E24，所以
stream 0的pf_mapping_info起始地址是 0x406D17E24
stream 1的pf_mapping_info起始地址是 0x406D17E24 + 0xF0c = 0x406D18D30
stream 2的pf_mapping_info起始地址是 0x406D18D30 + 0xF0c = 0x406D19C3C
stream 3的pf_mapping_info起始地址是 0x406D19C3C + 0xF0c = 0x406D1AB48
stream 4的pf_mapping_info起始地址是 0x406D1AB48 + 0xF0c = 0x406D1BA54
stream 5的pf_mapping_info起始地址是 0x406D1BA54 + 0xF0c = 0x406D1C960
stream 6的pf_mapping_info起始地址是 0x406D1C960 + 0xF0c = 0x406D1D86C
stream 7的pf_mapping_info起始地址是 0x406D1D86C + 0xF0c = 0x406D1E778
stream 8的pf_mapping_info起始地址是 0x406D1E778 + 0xF0c = 0x406D1F684
stream 9的pf_mapping_info起始地址是 0x406D1F684 + 0xF0c = 0x406D20590
stream a的pf_mapping_info起始地址是 0x406D20590 + 0xF0c = 0x406D2149C
stream b的pf_mapping_info起始地址是 0x406D2149C + 0xF0c = 0x406D223A8

stream 0的pf_parity_info起始地址是 0x406D223A8 + 0xF0c = 0x406D232B4
stream 1的pf_parity_info起始地址是 0x406D232B4 + 0x58 = 0x406D2330C
stream 2的pf_parity_info起始地址是 0x406D2330C + 0x58 = 0x406D23364
stream 3的pf_parity_info起始地址是 0x406D23364 + 0x58 = 0x406D233BC
stream 4的pf_parity_info起始地址是 0x406D233BC + 0x58 = 0x406D23414
stream 5的pf_parity_info起始地址是 0x406D23414 + 0x58 = 0x406D2346C
stream 6的pf_parity_info起始地址是 0x406D2346C + 0x58 = 0x406D234C4
stream 7的pf_parity_info起始地址是 0x406D234C4 + 0x58 = 0x406D2351C
stream 8的pf_parity_info起始地址是 0x406D2351C + 0x58 = 0x406D23574
stream 9的pf_parity_info起始地址是 0x406D23574 + 0x58 = 0x406D235CC
stream a的pf_parity_info起始地址是 0x406D235CC + 0x58 = 0x406D23624
stream b的pf_parity_info起始地址是 0x406D23624 + 0x58 = 0x406D2367C

meta_data_pba起始地址是 0x406D2367C + 0x58 = 0x406D236D4

```

```c++
XScope> read_ddr 0x406D1E778 20
0x406d1e778: 0xba1aba1a
0x406d1e77c: 0x00000007
0x406d1e780: 0x00000001
0x406d1e784: 0x83831f40
0x406d1e788: 0x00000000
0x406d1e78c: 0x00000000
0x406d1e790: 0x21003180
0x406d1e794: 0x00000000
0x406d1e798: 0xffffffff
0x406d1e79c: 0xffffffff
0x406d1e7a0: 0xffffffff
0x406d1e7a4: 0xffffffff
0x406d1e7a8: 0x210031c0
0x406d1e7ac: 0x00000000
0x406d1e7b0: 0xffffffff
0x406d1e7b4: 0xffffffff
0x406d1e7b8: 0xffffffff
0x406d1e7bc: 0xffffffff
0x406d1e7c0: 0x00000000
0x406d1e7c4: 0x00000000
0x406d1e7c8: 0x00000000
0x406d1e7cc: 0x00000000
0x406d1e7d0: 0x00000000
0x406d1e7d4: 0x00000000
0x406d1e7d8: 0x00000000
0x406d1e7dc: 0x00000000
0x406d1e7e0: 0x00000000
0x406d1e7e4: 0x00000000
0x406d1e7e8: 0x00000000
0x406d1e7ec: 0x00000000
0x406d1e7f0: 0x00000000
0x406d1e7f4: 0x00000000

```

```
XScope> read_ddr 0x406D232B4 20
0x406d232b4: 0x00000001
0x406d232b8: 0x00000003
0x406d232bc: 0x221764c0
0x406d232c0: 0x00000000
0x406d232c4: 0x000002f3
0x406d232c8: 0x00000238
0x406d232cc: 0x000003a7
0x406d232d0: 0x0000049f
0x406d232d4: 0x000002a3
0x406d232d8: 0x00000397
0x406d232dc: 0x21003200
0x406d232e0: 0x00000000
0x406d232e4: 0x21003280
0x406d232e8: 0x00000000
0x406d232ec: 0x21003300
0x406d232f0: 0x00000000
0x406d232f4: 0x21003240
0x406d232f8: 0x00000000
0x406d232fc: 0x210032c0
0x406d23300: 0x00000000
0x406d23304: 0x21003340
0x406d23308: 0x00000000
0x406d2330c: 0x00000000
0x406d23310: 0x00000000
0x406d23314: 0x00000000
0x406d23318: 0x00000000
0x406d2331c: 0xffffffff
0x406d23320: 0xffffffff
0x406d23324: 0xffffffff
0x406d23328: 0xffffffff
0x406d2332c: 0xffffffff
0x406d23330: 0xffffffff

```

```
XScope> read_ddr 0x406D236D4 10
0x406d236d4: 0x21003800
0x406d236d8: 0x00000000
0x406d236dc: 0x21003840
0x406d236e0: 0x00000000
0x406d236e4: 0x21003880
0x406d236e8: 0x00000000
0x406d236ec: 0x210038c0
0x406d236f0: 0x00000000
0x406d236f4: 0x21003900
0x406d236f8: 0x00000000
0x406d236fc: 0x21003940
0x406d23700: 0x00000000
0x406d23704: 0x21003980
0x406d23708: 0x00000000
0x406d2370c: 0x210039c0
0x406d23710: 0x00000000
记录的是每个b2n的pba，一次一个b2n写满一个die的一个page
```

```
UD2214C0023M@CSDU5SPC38M1:/$ parsepba 0x21003800
pba.all 0x21003800:
ch:       0
ce_lun:   2
block:    132
page:     3
plane:    0
offset:   0x0
type:     0
ep_idx:   0
ccp_off:  0x0
comp_len: 0x0
unc:      0

nand addr fields:
lun:      0x1
block:    0x84
plane:    0x0
page:     0x3
row_addr: 0x1210003
la:       0x0
type:     0x0
ln:       0x1
ce:       0x0
ch:       0x0
high32:   0x100

UD2214C0023M@CSDU5SPC38M1:/$ nandcfg
io_task0:                  Nand config info:
io_task0:                      Num of Block     : 556
io_task0:                      Num of Page      : 712
io_task0:                      Num of Slc Page  : 704
io_task0:                      Num of Page Type : 3
io_task0:                      Num of Plane     : 4
io_task0:                      Num of Chan      : 16
io_task0:                      Num of CE        : 2
io_task0:                      Num of LUN       : 2
io_task0:                      Num of Offset    : 4
io_task0:                      Num of Die       : 64

#define CE_OF_CE_LUN(ce_lun)            ((ce_lun) % (CFG_CE_NUM))
#define LUN_OF_CE_LUN(ce_lun)           ((ce_lun) / (CFG_CE_NUM))
#define GET_CE_LUN(ce, lun)             (((lun) * CFG_CE_NUM) + (ce))
#define GET_DIE_BY_CH_CE_LUN(ch, ce, lun)  (((((lun) * CFG_CE_NUM) + (ce)) << CFG_PBA_CHAN_BITS) | (ch)) = (ce_lun << CFG_PBA_CHAN_BITS) |(ch) = ce_lun * CHAN_NUM + ch

举例：
Num of CE        : 2
Num of LUN       : 2
Num of Chan      : 16
如果
ln:       0x1
ce:       0x0
ch:       0
那么ce_lun = ln * (Num of CE) + ce = 2
die = (ce_lun * CHAN_NUM + ch) = 2 * 16 + 0 = 32
```

