```
bbd showebb -s blkid -e blkid

rdnor 0x117c000 0x100

[4-1970:1:1|0:0:0:204]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
  > _evt_name:            U0018467_CHK_CHIP_RESET
  
 1988 [2022-08-26 09:43:22.742] Chip Reset Check, reg: 0x7, data: 0x1e
 1989 [2022-08-26 09:43:22.742] Warning!!! This is reset, nor addr 0x117c000

10077 [2022-08-26 09:48:57.406] [4-1970:1:1|0:0:0:908]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
10078 [2022-08-26 09:48:57.411]   > _evt_name:            U0018469_CHK_CHIP_RESET


pr_dbg("unexpected chip reset chk, reg: 0x%x, data: 0x%x\n", MP8815_REG_TEMP_WARN_THRESHOLD, status[0]);

[tcnsh@localhost ~]$ grep '>>> WARNING: This is unexpected chip reset!' nor_0825_2.log
[6-1970:1:1|0:0:0:63]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:60]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:65]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:62]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:65]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:62]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:63]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:66]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:64]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:97]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:67]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:64]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:66]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:66]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:65]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:64]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:64]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:66]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:62]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:66]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:65]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:67]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[6-1970:1:1|0:0:0:68]   >>> WARNING: This is unexpected chip reset! nor addr 0x117c000
[tcnsh@localhost ~]$ grep '>>> WARNING: This is unexpected chip reset!' nor_0825_2.log | wc -l


UD2214C0539H@CSDU5SPC76M1:/$ getassertid PF_DUMP_STATIC_BLK_LOCK_RACE_CONDITION_WRITE
Assert PF_DUMP_STATIC_BLK_LOCK_RACE_CONDITION_WRITE's id is: 621

UD2214C0023M@CSDU5SPC38M1:/$ getassertid EVT_TEST_W2P


BOMB(EVT_TEST_POLL_W2P,1);
BOMB(EVT_TEST_POLL_B2N,1)
BOMB(EVT_TEST_B2N,1);
BOMB(EVT_TEST_W2P,1);
BOMB(EVT_TEST_SCAN_INIT,1);
BOMB(EVT_TEST_IDLE, 1);
BOMB(EVT_TEST_POLL_B2N_EVT,1);
BOMB(EVT_TEST_STAT_POLL_ERASE, 1);
BOMB(EVT_TEST_STAT_FLUSH, 1);
BOMB(EVT_TEST_STAT_POLL_WRITE, 1);
BOMB(EVT_TEST_SWITCH_DIE,1);

triggerassert 1 243
triggerassert 1 241
triggerassert 1 239
triggerassert 1 248
triggerassert 1 244
triggerassert 1 240
triggerassert 1 242
triggerassert 1 246
triggerassert 1 245
triggerassert 1 247

// fio
sudo fio --buffer_compress_percentage=58 --buffer_compress_chunk=4k --direct=1 --thread=1 --filename=/dev/nvme0n1 --ioengine=libaio  --rw=randwrite --mem_align=512 --bs=128k --iodepth=256 --size=100% --numjobs=1 --runtime=99d --time_based --group_reporting --name=nvme0n1 &

ASSERT_TRIGGER_MAPLOG_PF           = 166,

Assert EVT_TEST_POLL_W2P's id is: 243
Assert EVT_TEST_POLL_B2N's id is: 241
Assert EVT_TEST_B2N's id is: 239
Assert EVT_TEST_W2P's id is: 248
Assert EVT_TEST_SCAN_INIT's id is: 244
Assert EVT_TEST_IDLE's id is: 240
Assert EVT_TEST_POLL_B2N_EVT's id is: 242
Assert EVT_TEST_STAT_POLL_ERASE's id is: 246
Assert EVT_TEST_STAT_FLUSH's id is: 245
Assert EVT_TEST_STAT_POLL_WRITE's id is: 247

计算公式：lun_idx * (n_ch * n_ce) + ce_idx * n_ch + ch_idx

PF_DUMP_STATIC_BLK_LOCK_RACE_CONDITION_ERASE
BOMB(EVT_TEST_STAT_POLL_ERASE, 1);
BOMB(EVT_TEST_STAT_FLUSH, 1);
BOMB(EVT_TEST_STAT_POLL_WRITE, 1);
BOMB(EVT_TEST_SCAN_INIT,1);

```

```
vim
:%!xxd
以查看文件的16进制表示

combpba offset [plane page_type ch ce lun page block ep_idx ccp_off comp_len]
pba: 0x11257e000
UD2214C0539H@CSDU5SPC76M1:/$ parsepba 0x11257e000
pba.all 0x11257e000:
ch:       0
ce_lun:   0
block:    548
page:     703
plane:    0
offset:   0x0
type:     0
ep_idx:   0
ccp_off:  0x0
comp_len: 0x0

nand addr fields:
lun:      0x0
block:    0x224
plane:    0x0
page:     0x2bf
row_addr: 0x8902bf
la:       0x0
type:     0x0
ln:       0x0
ce:       0x0
ch:       0x0
high32:   0x0

combpba 0 0 0 0 0 0 703 548
combpba 0 0 0 1 0 0 703 548

UD2214C0539H@CSDU5SPC76M1:/$ combpba 0 0 0 1 0 0 703 548
[4-0:0:0D|0:2:21:173]     io_task0:                       pba : 0x11257e040
UD2214C0539H@CSDU5SPC76M1:/$ parsepba 0x11257e040
pba.all 0x11257e040:
ch:       1
ce_lun:   0
block:    548
page:     703
plane:    0
offset:   0x0
type:     0
ep_idx:   0
ccp_off:  0x0
comp_len: 0x0

nand addr fields:
lun:      0x0
block:    0x224
plane:    0x0
page:     0x2bf
row_addr: 0x8902bf
la:       0x0
type:     0x0
ln:       0x0
ce:       0x0
ch:       0x1
high32:   0x1000

Usage: lbablk lba namespaceid
lbablk lba namespaceid（1）
lba: 0x12970e339, namespaceid: 1, lpa: 0x252e1c67, the corresponding pba is: 0x803c33d536dd, on block: 414(0x19e)


sudo lspci -d cc53: -vvv 

我在192.168.110.33上面写了个parse pba的脚本，大家可以去使用和完善

scp tcnsh@192.168.110.33:/home/tcnsh/parse.py .

python3 parse.py pba b474t 0x8669c780

FOOTER_PAGE_NUM
```

```
                          UD2214C0023M@CSDU5SPC38M1:/$ showxlatinfo 2
[2022-09-09 16:18:27.519] [4-0:0:0D|0:36:11:107]     io_task0:                   phy die: log die, raid cmd
[2022-09-09 16:18:27.551] [4-0:0:0D|0:36:11:107]     io_task0:                      0:        0,    START
[2022-09-09 16:18:27.589] [4-0:0:0D|0:36:11:107]     io_task0:                      1:        1,      XOR
[2022-09-09 16:18:27.621] [4-0:0:0D|0:36:11:107]     io_task0:                      2:        2,      XOR
[2022-09-09 16:18:27.659] [4-0:0:0D|0:36:11:107]     io_task0:                      3:        3,      XOR
[2022-09-09 16:18:27.691] [4-0:0:0D|0:36:11:107]     io_task0:                      4:        4,      XOR
[2022-09-09 16:18:27.729] [4-0:0:0D|0:36:11:107]     io_task0:                      5:        5,      XOR
[2022-09-09 16:18:27.761] [4-0:0:0D|0:36:11:107]     io_task0:                      6:        6,      XOR
[2022-09-09 16:18:27.799] [4-0:0:0D|0:36:11:108]     io_task0:                      7:        7,      XOR
[2022-09-09 16:18:27.831] [4-0:0:0D|0:36:11:108]     io_task0:                      8:        8,      XOR
[2022-09-09 16:18:27.869] [4-0:0:0D|0:36:11:108]     io_task0:                      9:        9,      XOR
[2022-09-09 16:18:27.901] [4-0:0:0D|0:36:11:108]     io_task0:                     10:       10,      XOR
[2022-09-09 16:18:27.939] [4-0:0:0D|0:36:11:108]     io_task0:                     11:       11,      XOR
[2022-09-09 16:18:27.971] [4-0:0:0D|0:36:11:108]     io_task0:                     12:       12,      XOR
[2022-09-09 16:18:28.009] [4-0:0:0D|0:36:11:108]     io_task0:                     13:       13,      XOR
[2022-09-09 16:18:28.041] [4-0:0:0D|0:36:11:108]     io_task0:                     14:       14,      XOR
[2022-09-09 16:18:28.079] [4-0:0:0D|0:36:11:108]     io_task0:                     15:       15,      XOR
[2022-09-09 16:18:28.111] [4-0:0:0D|0:36:11:109]     io_task0:                     16:       16,      XOR
[2022-09-09 16:18:28.149] [4-0:0:0D|0:36:11:109]     io_task0:                     17:       17,      XOR
[2022-09-09 16:18:28.181] [4-0:0:0D|0:36:11:109]     io_task0:                     18:       18,      XOR
[2022-09-09 16:18:28.219] [4-0:0:0D|0:36:11:109]     io_task0:                     19:       19,      XOR
[2022-09-09 16:18:28.251] [4-0:0:0D|0:36:11:109]     io_task0:                     20:       20,      XOR
[2022-09-09 16:18:28.289] [4-0:0:0D|0:36:11:109]     io_task0:                     21:       21,      XOR
[2022-09-09 16:18:28.321] [4-0:0:0D|0:36:11:109]     io_task0:                     22:       22,      XOR
[2022-09-09 16:18:28.359] [4-0:0:0D|0:36:11:109]     io_task0:                     23:       23,      XOR
[2022-09-09 16:18:28.391] [4-0:0:0D|0:36:11:110]     io_task0:                     24:       24,      XOR
[2022-09-09 16:18:28.429] [4-0:0:0D|0:36:11:110]     io_task0:                     25:       25,      XOR
[2022-09-09 16:18:28.461] [4-0:0:0D|0:36:11:110]     io_task0:                     26:       26,      XOR
[2022-09-09 16:18:28.499] [4-0:0:0D|0:36:11:110]     io_task0:                     27:       27,      XOR
[2022-09-09 16:18:28.531] [4-0:0:0D|0:36:11:110]     io_task0:                     28:       28,      XOR
[2022-09-09 16:18:28.568] [4-0:0:0D|0:36:11:110]     io_task0:                     29:       29,      XOR
[2022-09-09 16:18:28.600] [4-0:0:0D|0:36:11:110]     io_task0:                     30:       30,      XOR
[2022-09-09 16:18:28.639] [4-0:0:0D|0:36:11:110]     io_task0:                     31:       31,   PARITY
[2022-09-09 16:18:28.671] [4-0:0:0D|0:36:11:111]     io_task0:                     32:       32,    START
[2022-09-09 16:18:28.709] [4-0:0:0D|0:36:11:111]     io_task0:                     33:       33,      XOR
[2022-09-09 16:18:28.740] [4-0:0:0D|0:36:11:111]     io_task0:                     34:       34,      XOR
[2022-09-09 16:18:28.778] [4-0:0:0D|0:36:11:111]     io_task0:                     35:       35,      XOR

```

VU_STATUS_E vu_handler_write_sblock(vector<string> *pargs __unused, write_sblock *param __unused,

​                                    write_sblock_data *pdata __unused)

```
UartCmdName="wblk" Description="wblk sblock_id [slc/tlc]">
```

