```shell
for((i=1;i<=50000;i++));
do
        echo "This is $i loops"
        #sudo fio -filename=/dev/nvme0n1 -direct=1 -iodepth=32 -rw=write -ioengine=libaio -bs=128k -offset=0 -size=100% -numjobs=1 -runtime=100s -time_based -group_reporting -name=mytest &
        #sudo fio --filename=/dev/nvme0n1 --ioengine=libaio --norandommap=1 --randrepeat=0 --direct=1 --name=mytest  --size=100% --group_reporting --numjobs=8 --rw=randwrite --iodepth=128 --bs=4k &
        sudo fio --buffer_compress_percentage=58 --buffer_compress_chunk=4k --direct=1 --thread=1 --filename=/dev/nvme0n1 --ioengine=libaio  --rw=randwrite --mem_align=512 --bs=128k --iodepth=256 --size=100% --numjobs=1 --runtime=99d --time_based --group_reporting --name=nvme0n1 &
        sleep 10
        sudo python3 /home/tcnsh/samanea/Samanea_Scripts/Tool/Tool_RelayPowerOff.py
        sleep 2
        sudo python3 /home/tcnsh/samanea/Samanea_Scripts/Tool/Tool_RelayPowerOn.py
        sleep 10
        sudo ./rescanpcie.sh
        sleep 5
        lsblk | grep 3.5T
        if [ $? -eq 1 ]; then
            sudo ./rescanpcie.sh
            sleep 5
            lsblk | grep 3.5T
            if [ $? -eq 1 ]; then
                echo no nvme!!!!!!!
                exit 1
            fi
        fi
done

```

```
[14:27] Mars Ma
@all， Hoolan改进了fio的脚本，我把新的脚本放到了：/myrtle_fw/utility/scripts/Basic_Items_Test里面。


这个新的脚本在之前提供的性能脚本基础上，优化了部分
1.测试前会自动清理环境，包括清理历史log/环境中fio/iostat 等2.新增测试pattern seq write/seq read 4k/1job/qd13.如果测试中手动kill fio 了，脚本会报错退出4.测试结束后，测试数据会跟PRD 值进行对比，如果差异大，会报Fail, 如果gap 值在允许范围内，会打印success


1.该脚本提供的PRD 值对应的测试配置跟Daily perf 测试配置保持一致，既

3.84Tx2/2:1 3.84Tx1/1:1 7.68Tx1/1:1 7.68Tx1/1.2:1

2.该文件解压后，使用方式如下：

sudo sh basic_items_test.sh --device nvme0n1 --runtime 1800--comp_ratio 1

[14:28] Mars Ma
大家仍然要记得 bug fix后，要跑这个脚本来确保基本的读写没有问题。


```

