## Update Image after security boot Merged

After this change merged, the BL3 image stored in Nor Flash or RFS will be encrypted by AES. And BL2 will decrypted the bl3 image when boot up.  So you have to do the following update flow to update your image ( BL2/ BL3 in nor/ BL3 in RFS).



### Pre-condition

- The cards must finished **933 rework and switch to nor boot mode**, If not, plz replace the card with your manager

  ~~~
  PLS: 0x00
  PLS: 0x01
  PLS: 0x02
  PLS: 0x04
  PLS: 0x0c
  PLS: 0x0e
  PLS: 0x0c
  PLS: 0x0e
  PLS: 0x10
  PLS UP: 70 ms
  BL1: Jul 30 2022 13:40:13 , clk:80032000, fpga ver:1667787120
  Nor Boot                    #<<<< boot mode is nor boot 
  Loading bl2 primary from NOR offset 0x00010000
  Done

  ~~~


- The card should running in **user/fdl mode**, any fw version should be ok.

- Install sfx-tools_xxx.rpm toolkit.


### Update Flow



1. VU “updateNorImg“ to update BL2 image

   ~~~
   sudo sfx-recovery --cmd "updateNorImg /dev/nvme0 file=./bl2.signed.bin image=0" 
   myrtle_fw.asic.bl2.dev_build.csdu5spc15c1_vU.0.0@188873_20062.img.signed.bin
   ~~~


2. power cycle, → card goto Burning mode,  can checking by sfx-status

   ~~~
   [tcn@localhost whou]$ sudo sfx-status

   Burning Image card: /dev/nvme0
   PCIe Vendor ID:                    0xcc53
   PCIe Subsystem Vendor ID:          0xcc53
   Manufacturer:                      ScaleFlux
   Model:                             CSD-3310
   Serial Number:                     UD2147D0057M
   OPN:                               CSDU4SRC38A0
   Drive Type:                        U.2-V
   Firmware Revision:                 B0017865
   Critical Warning:                                                     
   Burning Image:                     ON                                 

   #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   Detect card(s) not ready, please wait or contact Admin for help.
   #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   ~~~


3. VU “UpdateNorImg “ to update BL3 image in nor flash

   ~~~
   sudo sfx-recovery --cmd "updateNorImg /dev/nvme0 file=./bl3.signed.bin image=1"
   ~~~


4. power cycle → card goto FDL mode,  can checking by sfx-status

   ~~~
   [tcn@localhost ~]$ sudo sfx-status
   FDL Mode card: /dev/nvme0n1
   PCIe Vendor ID:                    0xcc53
   PCIe Subsystem Vendor ID:          0xcc53
   Manufacturer:                      ScaleFlux
   Model:                             CSD-3310
   Serial Number:                     UD2147D0057M
   OPN:                               CSDU4SRC38A0
   Drive Type:                        U.2-V
   Firmware Revision:                 G0017865
   Critical Warning:                                                     
   FDL Mode:                          ON                                 

   #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   Detect card(s) not ready, please wait or contact Admin for he
   ~~~


5. VU “mat 0”  to rebuild RFS

   ~~~
    sudo sfx-recovery --cmd "rebuildRFS /dev/nvme0" 
   ~~~


6. “nvme fw-download“ & “nvme fw-commit“ to update user image in RFS

   ~~~
   [tcn@localhost whou]$ sudo nvme fw-download /dev/nvme0n1  -f ./bl3.signed.bin  -x 0x20000
   Firmware download success
   [tcn@localhost whou]$ sudo nvme fw-commit /dev/nvme0n1 -s 1 -a 1
   Success committing firmware action:1 slot:1
   ~~~


7. power cycle to active the new image → card goto User Mode, can checking by sfx-status

   ~~~
   [tcn@localhost whou]$ sudo sfx-status

   SFX card: /dev/nvme0n1
   PCIe Vendor ID:                    0xcc53
   PCIe Subsystem Vendor ID:          0xcc53
   Manufacturer:                      ScaleFlux
   Model:                             CSD-3310
   Serial Number:                     UD2147D0057M
   OPN:                               CSDU4SRC38A0
   Drive Type:                        U.2-V
   Firmware Revision:                 U0017865
   Temperature:                       81 C
   Power Consumption:                 15427 mW
   Atomic Write mode:                 ON
   Percentage Used:                   0%
   Data Read:                         0 GiB
   Data Written:                      0 GiB
   Correctable Error Cnt:             0
   Uncorrectable Error Cnt:           0
   PCIe Link Status:                  Speed 16GT/s, Width x4
   PCIe Device Status:                Good
   Formatted Capacity:                0 GB
   Provisioned Capacity:              0 GB
   Compression Ratio:                 100%
   Physical Used Ratio:               0%
   Free Physical Space:               0 GB
   RSA Verify:                        ON
   Critical Warning:                  Read/Write lock mode
   ~~~

8.  cleanCard to remove the Read/Write lock mode

   ~~~
   [tcn@localhost whou]$ sudo sfx-recovery --cmd "cleanCard /dev/nvme0"
   [tcn@localhost whou]$ sudo sfx-status
   
   SFX card: /dev/nvme0n1
   PCIe Vendor ID:                    0xcc53
   PCIe Subsystem Vendor ID:          0xcc53
   Manufacturer:                      ScaleFlux
   Model:                             CSD-3310
   Serial Number:                     UD2147D0057M
   OPN:                               CSDU4SRC38A0
   Drive Type:                        U.2-V
   Firmware Revision:                 U0017865
   Temperature:                       81 C
   Power Consumption:                 15634 mW
   Atomic Write mode:                 ON
   Percentage Used:                   0%
   Data Read:                         0 GiB
   Data Written:                      0 GiB
   Correctable Error Cnt:             0
   Uncorrectable Error Cnt:           0
   PCIe Link Status:                  Speed 16GT/s, Width x4
   PCIe Device Status:                Good
   Formatted Capacity:                0 GB
   Provisioned Capacity:              3840 GB
   Compression Ratio:                 100%
   Physical Used Ratio:               0%
   Free Physical Space:               3840 GB
   RSA Verify:                        ON
   Critical Warning:                  0
   ~~~

   