```
[14:27] Mars Ma
@all， Hoolan改进了fio的脚本，我把新的脚本放到了：/myrtle_fw/utility/scripts/Basic_Items_Test里面。


这个新的脚本在之前提供的性能脚本基础上，优化了部分
1.测试前会自动清理环境，包括清理历史log/环境中fio/iostat 等2.新增测试pattern seq write/seq read 4k/1job/qd13.如果测试中手动kill fio 了，脚本会报错退出4.测试结束后，测试数据会跟PRD 值进行对比，如果差异大，会报Fail, 如果gap 值在允许范围内，会打印success


1.该脚本提供的PRD 值对应的测试配置跟Daily perf 测试配置保持一致，既

3.84Tx2/2:1 3.84Tx1/1:1 7.68Tx1/1:1 7.68Tx1/1.2:1

2.该文件解压后，使用方式如下：

sudo sh basic_items_test.sh --device nvme0n1 --runtime 1800 --comp_ratio 1

[14:28] Mars Ma
大家仍然要记得 bug fix后，要跑这个脚本来确保基本的读写没有问题。



```

```
=============== Test Summary ==============
Test Case Name:    GLP_SmartLogDataUnitsWrite
Test Case Execution Time:   3:25:19 (hh:mm:ss.xxxx)
Test Case Result:  Pass

g_pf_mgr.init(
g_pf_mgr.pf_block_init(
```

```
sudo fio --buffer_compress_percentage=1 --buffer_compress_chunk=4k --name=128kB_seq_WR_1job_QD128 --filename=/dev/nvme1n1 --ioengine=libaio --direct=1 --thread=1 --numjobs=1 --iodepth=128 --rw=write --bs=128k --size=100% --group_reporting
```

```
getblkmgrinfo
```

