```
[12/2 16:02] Peng Li
make user16t16gb47c1

[12/2 16:02] Peng Li
./utility/prebuild.sh asic.bl2
./utility/dobuild.sh asic.bl2 CSDU5SPC15C1 DEV_BUILD

000000040014b7c4 _ZN15folding_rd_task21comupte_pad_after_gtsE5pba_t
000000040014bee0
000000040016f4d0
0000000400202028 ZN14task_scheduler14pause_all_taskE12PAUSE_TYPE_E
00000004002023d8 _ZN14task_scheduler12run_one_taskEP9task_base+0x264:4002023cc
0000000400200e24 _ZN6smpmgr12smpmgr_startEv+0x1d4:400200e1c
00000004001e1258
00000004000013d8 _Z13fw_slave_mainj
00000004000015d8 _Z14dispatch_coresv
00000004000011c8 bl3_secondary_main
```

```
    _pf_meta_data_rw_buf_start    = (u8 *)PF_RW_DDR_BUF_START;
    _pf_summary_data_rw_buf_start = (u8 *)_pf_meta_data_rw_buf_start + ROUND_UP(PF_FLUSH_META_DATA_SIZE, DATA_LEN_4K);
    _pf_root_data_rw_buf_start = (u8 *)_pf_summary_data_rw_buf_start + ROUND_UP(PF_FLUSH_SUMMARY_DATA_SIZE, DATA_LEN_4K);
    _pf_padding_data_rw_buf_start = (u8 *)_pf_root_data_rw_buf_start + ROUND_UP(PF_FLUSH_ROOT_DATA_SIZE, DATA_LEN_4K);
    _pf_user_data_rw_buf_start    = (u8 *)_pf_padding_data_rw_buf_start + DATA_LEN_4K;
```

```
449E6C000: 0080C00018000842 0000018060001800
449E6C010: 0080C22C18000B09 00000180D0001807
449E6C020: 0080CD7C18000C8D 000001812000180D
449E6C030: 0080C65018000EC3 0000018180001812
449E6C040: 0080C3CC18001104 0000018210001818
449E6C050: 0080CCB018001183 0000018250001821
449E6C060: 0080CF50180013C1 00000182C0001825
449E6C070: 0080C460180014CC 000001830000182C
449E6C080: 0080CE501800160D 0000018380001830
449E6C090: 0080C82C18001848 00000183D0001838

449F68370: 0080CE5C1880C6AB 00000000BA1ABA1A
449F68380: 1881D00000000004 0000000000000000
449F68390: 000000001705A000 000000001705A040
449F683A0: 000000001705A080 FFFFFFFFFFFFFFFF
449F683B0: FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF
449F683C0: 000000001881D040 1705A0C000000000
449F683D0: 1705A10000000000 1705A14000000000
449F683E0: FFFFFFFF00000000 FFFFFFFFFFFFFFFF
449F683F0: FFFFFFFFFFFFFFFF 1881D080FFFFFFFF
449F68400: 0000000000000000 000000001705A180
449F68410: 000000001705A1C0 000000001705A200
449F68420: FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF
449F68430: FFFFFFFFFFFFFFFF 000000001881D0C0
449F68440: 1705A24000000000 1705A28000000000
449F68450: 1705A2C000000000 FFFFFFFF00000000
449F68460: FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF
449F68470: 00000000FFFFFFFF 0000000000000000


read_summary_data
g_pf_mgr._pf_summary_data_rw_buf_start

read_meta_data
(u8* )g_pf_mgr._pf_meta_data_rw_buf_start + (_sent_cnt * CFG_LUN_PAGE_SIZE * DATA_LEN_4K)
++_sent_cnt == PF_META_DATA_PBA_CNT

user_data_copy_back
g_pf_mgr._pf_user_data_rw_buf_start

214,528
4,194,304
3,886.5k
PF_META_DATA_PBA_CNT 60
```

