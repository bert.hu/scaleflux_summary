Following are the features of Short Stroke:

l  Capacity: 10% of the native device capacity,the number of blocks should be reduced as well.

l  Performance: Similar to normal drives.

l  Other functionality: The same as normal drives.



## 2.2   Raw capacity setting

Since the number of selected blocks accounts for 1/10 of the total number of 56 blocks, this does not mean that all 56 blocks can be used by users, we must reserve block0 for RFS use.  Second, we also need to allocate three blocks to static blocks, such as EVT,STATISTICS and PANIC.  In addition, the threshold of GC task starting blocks is 14, and we must ensure that 14 free sblocks can be used by GC task. In addition, the threshold of GC task stopping blocks is 18, so we need to reserve 4 additional blocks.  Otherwise, if the number of remaining blocks after the GC task starts is less than 18, the GC task will not stop, which will greatly affect the write performance.  

Therefore, our RAW_CAPICITY value needs to be set to the size of 34 blocks.  For mytrle's 4T B47 SSD a super block size of 8448MB, RAW_CAPICITY is 280GB, for 8T B47 SSD a super block size of 16896MB,  So RAW_CAPICITY is 561GB。

Because of the OP, there is actually less capacity left for the user to use.  

The capacity information of a short stroke SSD of 8T_B47 is shown in the figure below.

## 2.5   Debug command

Short Stroke need provides debug interface through UART, includes:

l  “**getblkinfo**”      ,  get every block’s status and PE count.

l  “**bbd showfbb**”, show factory bad block in RANGE, without input RANGE the default range is the whole disk.

l  “**bbd showebb**”, show effective bad block in RANGE, without input RANGE the default range is the whole disk.

l  “**getshortstroketable**”,show the selected short-stroke blocks.

## 3.2   FIO Test

1. 128K sequence write test

sudo fio --buffer_compress_percentage=1 --buffer_compress_chunk=4k --name=128kB_seq_WR_1job_QD128 --filename=/dev/nvme0n1 --ioengine=libaio --direct=1 --thread=1 --numjobs=1 --iodepth=128 --rw=write --bs=128k --size=100% --group_reporting --log_avg_msec=1000 --bwavgtime=1000 --write_bw_log=128kB_seq_WR_1job_QD128_1

128K sequential write test results are shown below.

2. 4K Random write test

sudo fio --buffer_compress_percentage=1 --buffer_compress_chunk=4k --name=4kB_rand_WR_4job_1h_QD128 --filename=/dev/nvme0n1 --ioengine=libaio --direct=1 --thread=1 --numjobs=4 --iodepth=128 --rw=randwrite --bs=4k --runtime=1h --time_based=1 --size=100% --group_reporting --log_avg_msec=1000 --bwavgtime=1000 --write_bw_log=4kB_rand_WR_4job_1h_QD128

4K random write test results are shown below.