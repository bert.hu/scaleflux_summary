```c++
// could l2p map be full-cached under given formatted capacity
bool l2p_mgr::cap_full_cache(u64 fmt_cap_in_gb) const
{
    u32 l1_map_entry_num = GET_MAP_NUM(COUNT_LPA_BY_CAP(fmt_cap_in_gb), NUM_LPA_PER_L2_MAP_ENTRY);
    return (l1_map_entry_num <= _max_cache_l2_num);
}
```

```
_max_cache_l2_num =
        (MAX_CACHE_L2_MAP_ENTRY_NUM > MAX_L1_MAP_ENTRY_NUM) ? MAX_L1_MAP_ENTRY_NUM : MAX_CACHE_L2_MAP_ENTRY_NUM;
```

```
4T
UE2235G0123N@CSDU5SPC15C1:/$ l2pdbg 1
init state: 0, init_idx 0x0
2K_MPAGE 0, RAW_CAPACITY 0x40000000000, DDR_SIZE 17179869184, provision_capacity 3840, format_capacity 6940, cache ratio 100, MAX_LBA 0x32814932f, MAX_LPA 0x65029265
L1_MAP_ENTRY_NUM 0x32814a, L2_MAP_ENTRY_NUM 0x32814a, max_cache_l2_num 0x34abbb, MAX_CACHE_L2_MAP_ENTRY_NUM 0x34abbb, DEFAULT_L1_MAP_ENTRY_NUM 0x1bf1f8, MAX_L1_MAP_ENTRY_NUM 0x37e3ea, MAX_DIRTY_LIST_LEN 0x13c065
DDR_L2P_LOCK_SIZE 0xdf8fa8, DDR_L1_MAP_SIZE 0x1bf2000, DDR_L2_MAP_SIZE 0x34abbb000, DDR_L2_NODE_SIZE 0x37e4000, ALLOC_L2_MAP_SIZE 0x34abbb000
ptr: lock 0x498f56000, l1_map 0x49a2cf000, l2_map 0x4b5445000, l2_node 0x49bec1000

l2p_update_format_cap: fullcache old 1 new 1,fmt_cap old 3840 new 6940,L1_MAP_ENTRY_NUM 3309898,max_cache_l2_num 3451835
```

```
commit 6de551d8f64627ab8c3a009ef0f871692380ceab (tag: U0020076)
Merge: d6b46efeb0 77151af7f9
Author: AaronWang <aaron.wang@scaleflux.com>
Date:   Thu Dec 1 06:20:43 2022 +0000

    Merged in hotfix/MYR-6045-pmic-cap-measurement-change (pull request #1887)

    hotfix/MYR-6045，Set cap timeout to 60s. If pmic is abnormal, enter running

    Approved-by: Yijun Lu
    Approved-by: Mars Ma

```

```txt
编译会产生错误：invalid application of 'sizeof' to incomplete type

错误原因

sizeof不能用在extern变量
sizeof 的计算发生在代码编译 的时刻
extern 标注的符号 在链接的时刻解析
所以 sizeof 不知道 这个符号到底占用了多少空间
————————————————
版权声明：本文为CSDN博主「墨痕诉清风」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/u012206617/article/details/94383568
```

```
g_blk_err_va:      evt_key_info_t size [0x8A0]
g_blk_err_va:      STB_DATA_FLUSH_SIZE [0x10000]
g_blk_err_va:      sizeof(pf_meta_data) [0x2CBEC8]
g_blk_err_va:      sizeof(evt_log_entry_t) [0x450]
g_blk_err_va:     EVT_CACHE_BUF_CNT [0x10]

```

```
[4-1970:1:1|1:8:48:401]    U0020069_DDR_INFO:

 _DDR_START: 400000000              
 _DDR_SIZE: 400000000               
 _STATIC_DDR_MAP_ADDRESS_MAP_START: 410000000                              
 _STATIC_DDR_MAP_ADDRESS_MAP_END: 800000000
 
 _DCS_CSR_AUTO_BASE_ADDR_START: 410000000 
 _DCS_CSR_AUTO_BASE_ADDR_END: 410010000 
 _UNALIGNED_BUF_START: 410010000    
 _UNALIGNED_BUF_END: 410410000
 
 _DOWNLOAD_BUF_START: 410410000     
 _DOWNLOAD_BUF_END: 410c10000       
 _FW_IMAGE_BUF_START: 410c10000     
 _FW_IMAGE_BUF_END: 411010000
 
 _FW_ACTIVATE_BUF_START: 411010000  
 _FW_ACTIVATE_BUF_END: 412010000    
 _COMPARE_DATA_BUF_START: 412010000 
 _COMPARE_DATA_BUF_END: 412418000
 
 _PATCH_DIRTY_LIST_START: 412418000 
 _PATCH_DIRTY_LIST_END: 413b18000   
 _VERIFY_DATA_BUF_START: 413b18000  
 _VERIFY_DATA_BUF_END: 413b20000
 
 _BM_DDR_BUF_START: 413b20000       
 _BM_DDR_BUF_END: 417b20000         
 _BM_META_BUF_START: 417b20000      
 _BM_META_BUF_END: 417f20000
 
 _DDR_BB_TABLE_START: 417f20000     
 _DDR_BB_TABLE_END: 418013400       
 _DDR_INJECT_ERR_TABLE_START: 418014000 
 _DDR_INJECT_ERR_TABLE_END: 41801a000
 
 _DDR_RFS_SMALL_SHARE_FILE_START: 41801a000 
 _DDR_RFS_SMALL_SHARE_FILE_END: 418029fec 
 _FTEST_RW_BUF_START: 41802a000     
 _FTEST_RW_BUF_END: 41808a000
 
 _L2P_PATCH_DDR_BUF_START: 41808a000 
 _L2P_PATCH_DDR_BUF_END: 41848a000  
 _PF_RW_DDR_BUF_START: 41848a000    
 _PF_RW_DDR_BUF_END: 41888a000
 
 _FOLDING_RUNTIME_TABLE_START: 41888a000 
 _FOLDING_RUNTIME_TABLE_END: 45bc64210 
 _GSD_MEM_BUF_START: 45bc65000      
 _GSD_MEM_BUF_END: 45df7b000
 
 _TRIM_BITMAP_START: 45df7b000      
 _TRIM_BITMAP_END: 45dfb2e3f        
 _TRIM_PROCESS_BITMAP_START: 45dfb3000 
 _TRIM_PROCESS_BITMAP_END: 45dfeae3f
 
 _TRIM_NS_BITMAP_START: 45dfeb000   
 _TRIM_NS_BITMAP_END: 45e022e3f     
 _REVOVERY_WINDOW_START: 45e023000  
 _REVOVERY_WINDOW_END: 45e803000
 
 _MAPLOG_B2N_BUF_START: 45e803000   
 _MAPLOG_B2N_BUF_END: 45ee18120     
 _MAPLOG_BUF_START: 45ee19000       
 _MAPLOG_BUF_END: 465de4000
 
 _FOOTER_BUF_START: 465de4000       
 _FOOTER_BUF_END: 479f74000         
 _READ_SCRUB_BUF_START: 479f74000   
 _READ_SCRUB_BUF_END: 49220c000
 
 _FOLDING_FEED_BUF_START: 49220c000 
 _FOLDING_FEED_BUF_END: 492fa3580   
 _DDR_MPAGE_SUMMARY_FLUSH_START: 492fa4000 
 _DDR_MPAGE_SUMMARY_FLUSH_END: 495fa4000
 
 _DDR_MPAGE_SUMMARY_LOADALL_START: 495fa4000                             
 _DDR_MPAGE_SUMMARY_LOADALL_END: 498fa4000 
 _DDR_MPAGE_SUMMARY_LOAD_START: 498fa4000 
 _DDR_MPAGE_SUMMARY_LOAD_END: 498faa000
 
 _DDR_L2P_LOCK_START: 498faa000     
 _DDR_L2P_LOCK_END: 49ab9bf38       
 _DDR_GET_LOG_BUF_START: 49ab9c000  
 _DDR_GET_LOG_BUF_END: 49ac1c000
 
 _DDR_NVME_BUF_START: 49ac1c000     
 _DDR_NVME_BUF_END: 49ad1c000       
 _NOR_DDR_BUF_START: 49ad1c000      
 _NOR_DDR_BUF_END: 49af1c000
 
 _PANIC_DUMP_BUF_START: 49af1c000   
 _PANIC_DUMP_BUF_END: 49b11c000     
 _DDR_L1_MAP_START: 49b11c000       
 _DDR_L1_MAP_END: 49e900000
 
 _DDR_L2_NODE_START: 49e900000      
 _DDR_L2_NODE_END: 4a58c8000        
 _DDR_PREFETCH_BUF_START: 4a58c8000 
 _DDR_PREFETCH_BUF_END: 4a9968000
 
 _DDR_PATCH_WORKAROND_START: 4a9968000 
 _DDR_PATCH_WORKAROND_END: 4a9969000 
 _DDR_BURNIN_BB_TABLE_START: 4a9969000 
 _DDR_BURNIN_BB_TABLE_END: 4a99af000
 
 _DDR_RSV_START: 4a99af000          
 _DDR_RSV_END: 4bb668000            
 _DDR_L2_MAP_START: 4bb668000       
 _DDR_L2_MAP_END: 800000000
```

```
16T
#define CFG_CHAN_NUM            (16)
#define CFG_CE_NUM              (4)
#define CFG_LUN_NUM             (4)

UE2235G0123N@CSDU5SPC15C1:/$ nandcfg
idle_task:   Nand config info:
idle_task:       Num of Block     : 556
idle_task:       Num of Page      : 712
idle_task:       Num of Slc Page  : 704
idle_task:       Num of Page Type : 3
idle_task:       Num of Plane     : 4
idle_task:       Num of Chan      : 16
idle_task:       Num of CE        : 4
idle_task:       Num of LUN       : 4
idle_task:       Num of Offset    : 4
idle_task:       Num of Die       : 256

> _DDR_RSV_START: 4a378c000          
> _DDR_RSV_END: 4b5445000            
> _DDR_L2_MAP_START: 4b5445000       
> _DDR_L2_MAP_END: 800000000


ALLOC_L2_MAP_SIZE = _DDR_L2_MAP_END - _DDR_L2_MAP_START = 0x34ABBB000
MAX_CACHE_L2_MAP_ENTRY_NUM = (ALLOC_L2_MAP_SIZE) / (L2_MAP_ENTRY_SIZE)=0x34ABBB
_max_cache_l2_num = MAX_CACHE_L2_MAP_ENTRY_NUM = 0x34ABBB

RAW_CAPACITY_IN_GiB=8*1024=8192
MAX_FORMAT_CAPACITY=(2ULL * MAX_PROVISION_CAPACITY)=2*(u64)(RAW_CAPACITY_IN_GiB * 9375 / 10000)=2*7680
COUNT_LBA_BY_CAP(cap)=((((((u64)(cap)-50) * 1953504ULL) + 97696368ULL) >> 3) << 3)
LPA_LIMIT_NUM=(COUNT_LBA_BY_CAP(MAX_FORMAT_CAPACITY) >> 3)=3,750,730,326

MAX_L1_MAP_ENTRY_NUM=GET_MAP_NUM(LPA_LIMIT_NUM, NUM_LPA_PER_L2_MAP_ENTRY)=7,325,646=0x6FC7CE

l1_map_entry_num= GET_MAP_NUM(COUNT_LPA_BY_CAP(fmt_cap_in_gb), NUM_LPA_PER_L2_MAP_ENTRY)
如果fmt_cap_in_gb=7680，则l1_map_entry_num=3,662,826=0x37E3EA
如果l1_map_entry_num <= _max_cache_l2_num，则是_full_cache_flag
否则是partial cache

perst_monito:      evt_key_info_t size [0x8E0]
perst_monito:      STB_DATA_FLUSH_SIZE [0x10000]
perst_monito:      sizeof(pf_meta_data) [0x294088]
perst_monito:      sizeof(evt_log_entry_t) [0x490]
perst_monito:     EVT_CACHE_BUF_CNT [0x20]


8T
#define CFG_CHAN_NUM            (16)
#define CFG_CE_NUM              (4)
#define CFG_LUN_NUM             (2)

UE2235G0123N@CSDU5SPC15C1:/$ nandcfg
rfs_file_tas:    Nand config info:
rfs_file_tas:        Num of Block     : 556
rfs_file_tas:        Num of Page      : 712
rfs_file_tas:        Num of Slc Page  : 704
rfs_file_tas:        Num of Page Type : 3
rfs_file_tas:        Num of Plane     : 4
rfs_file_tas:        Num of Chan      : 16
rfs_file_tas:        Num of CE        : 4
rfs_file_tas:        Num of LUN       : 2
rfs_file_tas:        Num of Offset    : 4
rfs_file_tas:        Num of Die       : 128

> _DDR_RSV_START: 46fe6b000          
> _DDR_RSV_END: 481b46000            
> _DDR_L2_MAP_START: 481b46000       
> _DDR_L2_MAP_END: 800000000
ALLOC_L2_MAP_SIZE = _DDR_L2_MAP_END - _DDR_L2_MAP_START = 0x37E4BA000
MAX_CACHE_L2_MAP_ENTRY_NUM = (ALLOC_L2_MAP_SIZE) / (L2_MAP_ENTRY_SIZE)=0x37E4BA

l1_map_entry_num= GET_MAP_NUM(COUNT_LPA_BY_CAP(fmt_cap_in_gb), NUM_LPA_PER_L2_MAP_ENTRY)
如果fmt_cap_in_gb=7680，则l1_map_entry_num=3,662,826=0x37E3EA
如果l1_map_entry_num <= _max_cache_l2_num，则是_full_cache_flag
否则是partial cache

perst_monito:    evt_key_info_t size [0x4E0]
perst_monito:    STB_DATA_FLUSH_SIZE [0x10000]
perst_monito:    sizeof(pf_meta_data) [0x284208]
perst_monito:    sizeof(evt_log_entry_t) [0x290]
perst_monito:   EVT_CACHE_BUF_CNT [0x20]

    
MICRON_NAND_B47R_4T    
MICRON_NAND_B47R_8T
MICRON_NAND_B47R_16T
```

