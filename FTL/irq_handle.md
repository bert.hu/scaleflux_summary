```
/*
 * power fail isr
 */
void handle_power_fail_isr(void)
{
#if __BL3__ == 1

    __pr("PF\n");
    g_pf_dump_task.set_pf_event_start();
    def_console_task.disable_console();
    bsp_platform::print_out_disable();
    softirq::raise_softirq(SOFTIRQ_PF);
#endif
}
```

```
void arch_handle_interrupts(void)

```

