```c++
/*
 * PBA definition
 * pba example for 3.2T B17A nand on PUMA board
 */
union pba_t {
    struct {
        u64 offset      : CFG_PBA_OFFSET_BITS; //4K unit
        u64 plane       : CFG_PBA_PLANE_BITS;
        u64 page_type   : CFG_PBA_PAGE_TYPE_BITS;
        u64 ch          : CFG_PBA_CHAN_BITS;
        u64 ce_lun      : CFG_PBA_CE_LUN_BITS;
        u64 page        : CFG_PBA_PAGE_BITS;
        u64 block       : CFG_PBA_BLOCK_BITS;
        u64 ep_idx      : CFG_PBA_EP_IDX_BITS;  //bit 0 = first 2k, 1 = second 2k in 4KB read
        u64 ccp_off     : CFG_PBA_CCP_OFF_BITS; //4B
        u64 comp_len    : CFG_PBA_COMP_LEN_BITS;  //4B
        u64 unc         : CFG_PBA_UNC_BITS;  // 1 bit, if set, means the whole 4K is bad secctor
        u64 rsvd        : (64 - PBA_ALL_BITS); //for sim, rsvd is 12bit
    };
    u64 all;
};

CFG_PBA_OFFSET_BITS = 2
CFG_PBA_PLANE_BITS can be 1 or 2
CFG_PBA_PAGE_TYPE_BITS = 2
CFG_PBA_CHAN_BITS can be 2, 3 or 4
CFG_PBA_CE_LUN_BITS can be 1, 2, 3 or 4
CFG_PBA_PAGE_BITS can be 9 or 10
CFG_PBA_BLOCK_BITS = 10
CFG_PBA_EP_IDX_BITS = 1
CFG_PBA_CCP_OFF_BITS = 10
CFG_PBA_COMP_LEN_BITS = 11

#define PBA_BITS   (CFG_PBA_CHAN_BITS + CFG_PBA_CE_LUN_BITS + CFG_PBA_BLOCK_BITS + \
        			CFG_PBA_PAGE_BITS + CFG_PBA_PLANE_BITS + CFG_PBA_PAGE_TYPE_BITS + CFG_PBA_OFFSET_BITS)
#define PBA_ALL_BITS  (PBA_BITS + CFG_PBA_EP_IDX_BITS + CFG_PBA_CCP_OFF_BITS + \
                                            CFG_PBA_COMP_LEN_BITS + CFG_PBA_UNC_BITS)
```

l1_map的每个unit就是mpage，大小是4K，每个mpage的特征用l1_node来表示，每个l1_node的大小是16bytes

每个mpage是4096个bytes，能存多少个pba，就看每个pba的大小 **sizeof(pba_t)**

mpage是依赖于checkpoint task来下刷到nand，每刷完一个block，就把mpage summary的信息写到block的最后

```c++
struct l1_node
{
    u32 state              : 3;// L2P_STATE_E
    u32 dirty_again_flag   : 1;// during flushing, hit write and dirty again.
    u32 in_dirty_list      : 1;
    u32 hit_flag           : 1;// free pool manager will check this flag first to decide if we should free this l2 node
    u32 gc_flag            : 1;// before flush, mark gc node. during flush, 1 for clean node flush, 0 for dirty node flush
    u32 disable_prefetch   : 1;
    u32 prefetched_lpa_cnt : 11;
    u32 rsv                : 13;
    u32 patch_id;// for dirty again case, patch_id is different with flush_patch_id
    u32 flush_patch_id;
#if __WITH_SIMULATOR__ == 0
    rwlock map_lock;
#else
    spinlock map_lock;
#endif
};
```

我们再来看看l1_map的结构，l1_map下面就是挂了一堆的mpage, 每个l1_id代表一个mpage的标识，每个l1_map_entry的大小是8bytes，对应映射一个4K的大小，这个8bytes代表的是mpage的首个pba地址

那么如果是4T的盘，需要的l1_map_entry的个数是4T/4K = ？

```c++
struct l1_map_entry
{
    u64 mpage_pba : MPAGE_VALID_BITS;// mpage using slc uncompress write, some bit in pba_t can be ignore and convert to this 40bit
                       // format
    u64 rsvd      : 64 - MPAGE_VALID_BITS;
};

_l1_map[l1_id].mpage_pba     = INVALID_MPAGE_PBA;
_l1_node[l1_id].state        = L2P_STATE_CLEAN;// must set to clean state
```

```
UD2214C0520H@CSDU5SPC76M1:/$ l2pdbg 1
init state: 0, init_idx 0x0
2K_MPAGE 0, RAW_CAPACITY 0x80000000000, DDR_SIZE 0x400000000
provision_capacity 7680, format_capacity 7680
MAX_LBA 0x37e3e92bf, MAX_LPA 0x6fc7d257
L1_MAP_ENTRY_NUM 0x37e3ea
MAX_CACHE_L2_MAP_ENTRY_NUM 0x38452d, DEFAULT_L1_MAP_ENTRY_NUM 0x37e3ea
MAX_L1_MAP_ENTRY_NUM 0x6fc7ce, MAX_DIRTY_LIST_LEN 0x1519f0
DDR_L1_MAP_SIZE 0x37e4000, DDR_L2_MAP_SIZE 0x38452d000
DDR_L1_NODE_SIZE 0x6fc8000, ALLOC_L2_MAP_SIZE 0x38452d000
ptr: l1_map 0x453b78000, l2_map 0x47bad3000, l1_node 0x45735c000
l2_remain_len: 0 l2_init_buf: 0, target: 0 0x800000000
```

每个mpage对应一个_l1_node，用l1_id表示一个mpage

l1_map的每个entry对应的就是LPA，大小是8bytes，一个LPA对应一个4K的mpage，里面有4K/sizeof(pba_4k_t)=4096/8=512个pba

LBA指的是sector size的大小，可能是512bytes，也可能是4k，如果 SSD 的物理扇区大小为 512 字节，意味着每个物理扇区存储的数据量为 512 字节。这意味着 SSD 在物理级别上以 512 字节的块大小进行读取和写入操作。

其中一个mpage也叫做是l2_map_entry，大小就是4K=4096bytes

1. find the mpage: l2_map_entry &mpage = _l2_map[LBA / 819]
2. find the required PBA inside this mpage by doing `mpage[lba % 512]`

```
/*
    we can get lba info from lp header for this l2_map_entry
*/
struct l2_map_entry
{
    u8 map_unit[4096];
};

struct mpage_entry
{
    u64 map_unit[512];
};
```

如果我们知道盘的容量cap，我们如何计算有多少个LBA

```
#define COUNT_LBA_BY_CAP(cap)      ((((((u64)(cap)-50) * 1953504ULL) + MAP_UNIT_ROUND_UP_SIZE) >> 3) << 3)

#if (ENABLE_8K_MAP_UNIT)
    #define MAP_UNIT_ROUND_UP_SIZE (ULL(97696368))
#else
    #define MAP_UNIT_ROUND_UP_SIZE (ULL(97696368) + ULL(16))
#endif
```

假设8T的盘，则cap = 8 X 1000 GB

那么COUNT_LBA_BY_CAP(cap) = 15,628,053,184

再试一下，4T的盘， 则cap = 4 X 1000 GB

那么COUNT_LBA_BY_CAP(cap) = 7,814,037,184

```
lbacount for 8000G is 15628053168, for 8001G is 15627976704
```

再来，计算有多少LPA

```
#define COUNT_LPA_BY_CAP(cap)     (COUNT_LBA_BY_CAP(cap) >> 3)
```

假设8T的盘，COUNT_LBA_BY_CAP(cap) = 15,628,053,184，则LPA = 1,953,506,648

再试一下，4T的盘， COUNT_LBA_BY_CAP(cap) = 7,814,037,184，则LPA =976,754,648

所以对于sector size是512bytes的盘，一个LPA代表4K，相当于1个LPA有8个LBA



In Myrtle we have 16GB DDR, usually our L2P table can take at most 15.2GB of DDR.

Let us calculate the maximum logical capacity of this L2P table:

```
Max logical capacity = （(Available DDR for L2 map) / (L2 entry + L1 node size)） *
                       (Num of PBA per L2 entry) * (Logical size per PBA)

When using 5 bytes PBA, we have:
Available DDR for L2 map: 15.2 * 1024 ^ 3 (bytes)
L2 entry + L1 node size : 4096 + 16       (bytes)
Num of PBA per L2 entry : 4096 / 5
Logical size per PBA    : 4096            (bytes)
则Max logical capacity = （（（15.2 * 1024 ^ 3）/ 4112 ） * 819 * 4096 ） / （1024 ^ 4）=  12.11TB
```

We can see that 15.2GB DDR can support up to 12.11TB capacity. This is not ideal even for our products with 8TB capacity



**4.3 Maximum capacity supported if big sector size is implemented**

A big sector is a series of PBAs that lies next to each other on NAND. Since they are next to each other, we can record them with much less space than their actual size, since much of the information of the following PBA(s) can be calculated by using the leading PBA.

As you see, we can use a total of `40 + 3 = 43` bits to represent 8 bytes, instead of 80 bits. We call this 43 bits thing a big sector PBA (not two PBAs)

For 16 bytes sector size, it becomes 1 leading PBA with 3 following PBA, then we need `40 + 3 + 3 + 3 = 49` bits.

那么再次计算15.2GB的DDR可以支持多大的容量

```
Max logical capacity = (Available DDR for L2 map) / (L2 entry + L1 node size) *
                       (Num of PBA per L2 entry) * (Logical size per PBA)

Available DDR for L2 map: 15.2 * 1024 ^ 3 (bytes)
L2 entry + L1 node size : 4096 + 16       (bytes)

When using 43 bits PBA, we have:
Num of PBA per L2 entry : 4096 * 8 / 43
Logical size per PBA    : 4096 * 2        (bytes)

When using 49 bits PBA, we have:
Num of PBA per L2 entry : 4096 * 8 / 49
Logical size per PBA    : 4096 * 4        (bytes)
对于8k sector
则Max logical capacity =（（（15.2 * 1024 ^ 3）/ 4112 ） * 762 * 4096 * 2 ） / （1024 ^ 4）= 22.53 TB
对于16k sector
则Max logical capacity =（（（15.2 * 1024 ^ 3）/ 4112 ） * 668 * 4096 * 4 ） / （1024 ^ 4）= 39.5 TB
```

For 43 bits PBA (8k sector), we can support up to 22.5TB.

For 49 bits PBA (16k sector), we can support up to 39.5TB.

所谓(8k sector)就是将两个pba并在一起，用43bit来替代本应该的40*2=80bit，2个PBAs，那么call 43 bits thing a big sector PBA，相当于每个leading pba的大小是43 / 8 bytes，但代表了2个pba

所谓(16k sector)就是将4个pba并在一起，用49bit来替代本应该的40*4=160bit，4个PBAs，那么call 49 bits thing a big sector PBA，相当于每个leading pba的大小是49 / 8 bytes，但代表了4个pba