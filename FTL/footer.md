```c++
/**
 * @brief Collect hot-stream's footer for footer-flushing
 * 
 */
void gsd_meta_flusher::prepare_footer_hot()
{
    u64 data_addr{0};
    u32 offset{0};
    collect_stream_footer(STREAM_ID_HOT, data_addr, offset); // 收集hot stream的footer

    u8 *buf_addr = (INVALID_64BIT == data_addr) ? nullptr : (u8 *)(data_addr);
    u32 cnt_4k   = (0 == offset) ? 0 : ((offset / (DATA_LEN_4K)) + 1);
    u32 amt_4k   = gsd_compute_w2p_amt();
    if (cnt_4k <= amt_4k) {
        _flush_meta._footer_hot_first_cnt  = cnt_4k;
        _flush_meta._footer_hot_second_cnt = 0;
    }
    else {
        _flush_meta._footer_hot_first_cnt  = amt_4k;
        _flush_meta._footer_hot_second_cnt = (cnt_4k - amt_4k);
    }

    _flush_meta._footer_hot_begin_pba_1 = INVALID_64BIT;
    _flush_meta._footer_hot_begin_pba_2 = INVALID_64BIT;

    _flush_params._total_cnt  = (_flush_meta._footer_hot_first_cnt * (DATA_LEN_4K));
    _flush_params._sent_cnt   = 0;
    _flush_params._rcv_cnt    = 0;
    _flush_params._dump_addr  = (u8 *)(buf_addr);
    _flush_params._batch_size = 0;
    _flush_params._begin_lba  = (GSD_FOOTER_HOT_LBA);
    _flush_params._end_lba    = (GSD_FOOTER_HOT_LBA) + (_flush_meta._footer_hot_first_cnt - 1);
}
```

```c++
void gsd_meta_flusher::collect_stream_footer(u32 stream_id, u64 &data_addr, u32 &offset)
{
    u32 blk = g_pu_mgr.pblk_mgr[0].get_open_sblock(stream_id, ACTION_TYPE_E::PEEK);
    if (INVALID_32BIT == blk) {
        data_addr = INVALID_64BIT;
        offset    = 0;
        return;
    }

    footer_buf_t &buf = g_footer_task.get_unflush_footer(blk);
    if (0 == buf.offset) {
        EVT_LOG(gsd_collect_footer_warn_evtlog, stream_id);
        data_addr = INVALID_64BIT;
        offset    = 0;
        return;
    }

    ASSERT(GSD_FOOTER_HAS_WRONG_BLK, (blk == (GET_BLOCK_FROM_PBA(((footer_entry_t *)(buf.data_addr))->maplog_pba))),
           "INVALID blk. blk: %u, maplog_pba: 0x%llx\n", blk, ((footer_entry_t *)(buf.data_addr))->maplog_pba);
    switch (stream_id) {
    case STREAM_ID_HOT: {
        _footer_meta.hot_open_blk = blk;
        _footer_meta.hot_offset   = buf.offset;
        break;
    }
    case STREAM_ID_GC: {
        _footer_meta.gc_open_blk = blk;
        _footer_meta.gc_offset   = buf.offset;
        break;
    }
    case STREAM_ID_WL: {
        _footer_meta.wl_open_blk = blk;
        _footer_meta.wl_offset   = buf.offset;
        break;
    }
    case STREAM_ID_EH: {
        _footer_meta.eh_open_blk = blk;
        _footer_meta.eh_offset   = buf.offset;
        break;
    }
    default: {
        ASSERT(GSD_MAPLOG_HAS_INVALID_STREAM_ID, false, "INVALID stream. stream_id: %u\n", stream_id);
        break;
    }
    }

    // g_footer_task.gsd_padding_footer_buf_4K_align(buf);
    // BOMB(GSD_FOOTER_OFFSET_NOT_ALIGNED, 0 == (buf.offset % (DATA_LEN_4K)));

    footer_entry_t entry = g_footer_task.check_footer_buf_first_entry(blk);
    ASSERT(GSD_COLLECT_FOOTER_START_BLK_WRONG, (entry.maplog_b2n_range.b2n_pba_start_block == blk),
           "blk[%u] does not equal to b2n_pba_start_block[%llu]\n", blk, entry.maplog_b2n_range.b2n_pba_start_block);
    ASSERT(GSD_COLLECT_FOOTER_END_BLK_WRONG, (entry.maplog_b2n_range.b2n_pba_end_block == blk),
           "blk[%u] does not equal to b2n_pba_end_block[%llu]\n", blk, entry.maplog_b2n_range.b2n_pba_end_block);

    data_addr = buf.data_addr;
    offset = buf.offset;
#if __WITH_SIMULATOR__ == 1
    INF_LOG("GSD collects footer, stream:[%u], data_addr:[0x%llx], offset:[%u]\n", stream_id, data_addr, offset);
#endif

    return;
}

_footer_data_addr     = g_footer_task.get_gsd_recovery_data_addr(_footer_meta[FOOTER_HOT_OPEN_BLK],
                                                                         _footer_meta[FOOTER_HOT_OFFSET]);
```

#### PF Recovery过程中的b2n和footer

```
13136 [6-0:0:0D|0:0:0:642]   summary_data:
13137   > summary_data_error_code:    0
13138   > summary_data_read_success: true
13139
13140 [6-0:0:0D|0:0:0:652] recovery_tas:                     Total number of w2ps is: 2095104 from start pba: 650      00000 to end pba: 652c7f80, sblock 404
13141 [6-0:0:0D|0:0:0:652] recovery_tas:                     Total number of w2ps is: 840752 from start pba: 651a      ad00 to end pba: 652c7f80, sblock 404
13142 [6-0:0:0D|0:0:0:652] recovery_tas:                     Total number of w2ps is: 2086656 from start pba: 68c      00000 to end pba: 68ec7f80, sblock 419
13143 [6-0:0:0D|0:0:0:652] recovery_tas:                     Total number of w2ps is: 534556 from start pba: 68e1      0f80 to end pba: 68ec7f80, sblock 419
13144
13145 [6-0:0:0D|0:0:0:654]   b2n_get_sblock:
13146   > _blk_id:              194
13147   > _stream:                0
13148   > _last_program_b2n_pba: 651aacc0
13149   > _recovered_consume_space: 34085b80
13150   > _good_plane_num:      100
13151   > _deallocated_sblock_num:    e
13152   > _real_free_sblock_num:    e

summary data之后的b2n打印，是因为在pf_mgr::restore_blk_mgr_meta_data()中
将blkmgr的数据恢复到原本的地方&g_pu_mgr.pblk_mgr
g_pu_mgr.pblk_mgr[0].scan_sblock();
for (u32 stream = 0; stream < STREAM_ID_MAX; stream++) {
	if (!IS_STREAM_POLLED_BY_B2N_TASK(stream)) {
	// 如果是RFS，PF和shared stream的跳过
		continue;
	}
	g_b2n_task.restore_open_sblock(0, stream); // 恢复其他stream的open block
	g_b2n_task.b2n_block_init(0, stream);
}

首先，blk_mgr::scan_sblock(bool gsd_recovery)
对于IS_DATA_BLOCK的stream，先g_pu_mgr.init_block_dist(0, sblk, stream)，主要配置config_parity_group
如果blk_status == BS_COLD_CANDIDATE，把这个block设置为BS_SEALED_BLOCK
如果blk_status == BS_OPEN_BLOCK且不是gsd recovery，会计算并set_sblock_consume_space

然后，g_b2n_task.restore_open_sblock(0, stream)会将_recovered_open_block_fifo倒腾到_open_block_fifo再倒腾回来
最后，b2n_task::b2n_block_init(u32 pu_id, u32 stream)，这里会调用b2n_get_sblock，所以log里面会有b2n_get_sblock_evtlog，b2n_get_sblock这里会从_recovered_open_block_fifo中获取open block，然后放进_open_block_fifo中；如果是STREAM_ID_FOOTER，会从_footer_pending_fifo中取出来，如果这个block是maplog_sealed，就从_footer_pending_fifo中pop出来，g_pu_mgr.init_block_dist之后，再放进_open_block_fifo中

需要注意
g_footer_task.send_footer_prog_request(block);

b2n_task::update_b2n_cpl_status(u8 stream, u64 b2n_pba)
// For non-user stream, final_footer_pba equals to final_user_pba
这里会把_open_block_fifo中open block给pop出来，并处理这些open block，如果是do early switch的或者是IS_SLC_STREAM(stream)，就标记为BS_SEALED_BLOCK，否则，标记为BS_COLD_CANDIDATE
如果有final_user_b2n_pba，且不是STREAM_ID_FOOTER，那么会把_open_block_fifo中open block给pop出来,放进_footer_pending_fifo中

b2n_task::stream_full_feed_b2n(u8 stream, u16 feed_amt)-->update_b2n_cpl_status
b2n_task::b2n_cmd_cmpl_polling()-->update_b2n_cpl_status

b2n_task::wait_b2n_cq_msg(u32 pu_stream, u64 *b2n_pba)
// do not do error handle for SLC stream (except early switch flag)，STREAM_ID_L2P||STREAM_ID_L2P_PATCH
// for cold stream, do not need padding，STREAM_ID_HOT
```









wendy加pf performance的场景

```
[root@localhost PowerCycle]# find ./ -name "*GC*"
./PC_AsyncIOMixGCWithPF.py
./PC_AsyncWriteMixGCWithPF.py
./PC_AsyncWriteWithNormalGCMixPF.py
./PC_CapacityPageInAdminTrimAsyncIOMixGCNotDoneWithPF.py
./PC_CapacityPageInMixGCNotDoneWithPF.py
./PC_CapacityPageInMixGCWithPF.py
./PC_CapacityPageInMixIdleGCNotDoneWithPF.py
./PC_CapacityPageInMixMpageGCNotDoneWithPF.py
./PC_FormatMixGCWithPF.py
./PC_FormatMixIdleGCWithPF.py
./PC_FormatMixMpageGCWithPF.py
./PC_GCDoneWithPF.py
./PC_GCNotDoneWithPFCrashRecovery.py
./PC_IdleGCNotDoneWithPF.py
./PC_LessIOMixPFWithoutGC.py
./PC_OverlapIOMixPFWIthoutGC.py
./PC_ProgramErrorDuringPFWithGCTask.py
./PC_RandomFullDriverIOmixPFWithoutGC.py
./PC_RandomWriteMixGCNotDoneWithPF.py
./PC_RandomWriteMixMpageGCWithPF.py
./PC_SequentialIOMixPFWithoutGC.py
./PC_TrimAndAdminCmdMixGCWithPF.py
./PC_TrimAndAdminCmdMixIdleGCWithPF.py
./PC_TrimAndAdminCmdMixMpageGCWithPF.py
./PC_WLAdminTrimAsyncIOMixGCWithPF.py
./PC_WLAdminTrimAsyncIOMixIdleGCWithPF.py
./PC_WLAdminTrimAsyncIOMixMpageGCWithPF.py
./PC_IdleGCDoneWithPF.py
./PC_CapacityPageInMixDiffGCWayWithReboot.py

```

