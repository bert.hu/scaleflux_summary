### 1.   Map Log

```
To speed up Recycle, a Map Log (P2L table) table will be flushed to NAND with host data, the position is:

1、For host band, every 64MB host data will have a Fix Length P2L table portion followed it.
2、For cold band, every 64MB host data will have a Varied size P2L table portion followed it.

Map log is a data structure that record the ppa<->lba mapping, we can call it P2L for short, map log data will be written with host or GC data, mixed together, so when written map log data, host io queue must be stopped, and insert barrier command.

Note that map log position in hot stream and cold stream is a little different, in host stream, every 64MB boundary, will flush map log, so it is when host data + map log = 64MB, then map log will be flushed. but in cold stream, it is when write host data 64MB, then flush its map log.

```

### 2.   Header/Footer Page

```
For each super block, it will have a header page and a footer page, record the block sequence and other things.
```

### 3.   RFS

```
Super block 0 and 1 is treated as root filesystem, some of the key data structure are written to these blocks, but currently block 0 and 1 still used as MLC, and just mirror copy, we many need enhance them to SLC and Quad copy in the future.
```

### 4.   Safe Power on/down

```
For safe power down, host FTL will just allocate a new super block and dump all mapping to that new super block. Next time if FTL found last power down is safe power down and mapping table is completed, it will read that table to host memory. And then put this super block that contain mapping table to garbage.
```

### 5.   Parity Group

```
In our FTL, the Parity Group is a series of Die that contain XOR, so 31+1 config is a 31 host data Die plus 1 XOR die, the data in XOR die if the XOR of all host data in previous host data Die, we call this 32 Die as a Parity Group.
In our FTL, the parity is always 31+1.

```

### 6.   Block manager

```
The core structure of block management is BLOCK_MANAGER, it used to track all the super block information, such as free space, LS in it, and it will be saved to boot page. It contains the open block FIFO/free block FIFO /recycle and recovery or other FIFOs.
And there is a field used to check whether it is safe shutdown: bcp->block_manager.power_safe, when safe shutdown, this flag will be set to 1.

```

### 7.   B2N Manager

```
The B2N manager is a simple thread that just provide B2N command to our controller, each B2N command will send a 4K location to controller, when controller used up 1 4K unit, it will send back a cqe, and the B2N manager will send a new 4K location to controller again. The thread function of B2N manager is: buf2nand_cmd_manager.
```

## 8.  GC/WL

```
GC is job is search map log, find valid host data, and rewrite them to new location. Most code of GC is in gc.h, gc.c under blk ftl folder. 
```

