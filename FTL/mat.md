| **VU**      | **Description**                          |
| ----------- | ---------------------------------------- |
| matcfg      | to config mat mode                       |
| mat         | to trigger mat                           |
| matewrcfg   | to config ewr info                       |
| rdmatmeta   | read mat header from nor                 |
| wrmatmeta   | wrmatmeta UD210C0112H CSDU4SPC76A0 CSD-3310 A21 |
| erasematlog | erasematlog 1: erase mat headererasematlog 2: erase mat gbberasematlog 3: erase mat footererasematlog 255: erasemat gbb and footer |
| erasegbb    | erasegbb then mat 3 to clear gbb in rfs. |
| getmatgbb   | read burnin gbb from nor                 |
| matstat     | matstat 0 : show current MAT statusmatstat 1 : show all MAT statusmatstat 2 : check MAT flow finished |
| ewrdbg      | ewrdbg 0 : show erase infoewrdbg 1 : show program infoewrdbg 2 : show read info |

