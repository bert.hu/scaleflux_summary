# 1          Description

Datapath is used to process NVMe's I/O cmd set, including: read module, write module, and credit module. 

- The read module processes NVMe read commands, reads data from NAND, and transmits the data to the host through PCIe. 


- The write module processes NVMe write commands, reads data from host to the landing buffer (aligned write)/SDRAM (unaligned write) through PCIe, and finally writes the data to NAND flash. 


- The credit module is used to control the read and write bandwidth. Currently, only the write bandwidth is controlled, and each core does not exceed 16MB.

## 2.1        pool and queues

![IO1](D:\scaleflux\05_summary\scaleflux_summary\FTL\IO1.png)

ACT poll (application cmd table): Store front-end submitted commands (NVMe)

FCT pool(flash cmd table):  save command for backend

message queues: as a communication channel between fw and hw

There is an ACT, which is shared. Each type of acq and fcq has an independent one for each core, and reading and writing FCT is also exclusive to each core. 