## 1. Introduction

Trim命令可应用于主机将指定的LBA无效，这样带来的好处是，便于FW在执行GC时将这些LBA数据作为垃圾，以此减少写放大，提升SSD使用寿命。

## 2. Architecture

![trim](D:\scaleflux\05_summary\scaleflux_summary\FTL\trim.png)

Trim FrontEnd Task主要用于获取trim range 信息，并完成合法性检查，还需要保证数据一致性做处理，完成首尾非对齐部分处理后，将对齐部分交由Trim Background Task处理，最后成命令响应。

### 4.2 数据一致性保证

Trim命令可能和IO读写命令并发，所以当接受到trim命令时，为保证数据一致性，其前后LBA存在重叠的读写命令，需要严格按照顺序执行。如下图所示，trim命令无效LBA0和LBA1，其前后命令包括写LBA0和读LBA1，需要按照“先执行左边命令，再执行中间trim命令，最后执行后边命令”的顺序。

![trim1](D:\scaleflux\05_summary\scaleflux_summary\FTL\trim1.png)

#### 4.2.1 LOC处理

在myrtle上，由硬件实现LOC处理。收到trim命令时，硬件便自动停止接受后续命令（即上图中右边命令），为保证数据的一致性，FW需要在完成trim range DMA后，再做LOC recover，即硬件可以继续接受后续命令。

在Quince上，因某些原因硬件LOC会关闭，所以需要FW逻辑实现处理，以保证数据一致性。

为保证trim前的命令优先执行。在trim命令处理前会扫描所有ACT entry（总共512个），根据ACT中的seq_num来判断是否存在trim前且LBA重叠的命令。如果存在，则trim命令处理需要等待其完成。

为保证trim后的命令在trim命令处理完成后再执行。在IO命令处理前需要检测Admin Msg Queue，优先处理admin命令（因为trim命令是从Admin Msg Queue中获取，而IO读写命令是从IO Msg Queue中获取）。

### 4.3 4K非对齐部分trim处理

由于L2P映射粒度最小为4K，而协议支持最小粒度为512B。所以trim可能存在4K非对齐的场景。

![trim2](D:\scaleflux\05_summary\scaleflux_summary\FTL\trim2.png)

如上图所示，首尾部分4K都是部分被trim。同个LBA里只涉及到部分数据需要修改，我们通常采用“读改写”的方式，即先从介质中读取完整的4K数据，然后将需要trim的部分数据清成全0，然后再重新写回到介质。

在“读改写”的方式中，会涉及到读介质和写介质，所以需要讨论介质操作的异常处理方案。

#### 4.3.1 读介质UNC

在myrtle trim方案中，读介质发生UNC，trim命令会直接回主机media error信息。

将LBA标记对应位置set上bad sector。被trim的sector，无需保留bad sector。

#### 4.3.2 读介质LBA mismatch

当读操作填写的LBA和介质中实际的LBA不一致时，会发生LBA mismatch。通常分为两种情况：

- LBA高位中bad sector被置位。
- L2P表映射异常，导致PBA中读取的LBA和查表LBA不同。

第一个场景，对于被trim的sector，无需保留bad sector。第二个场景表项映射异常，处理较为复杂，方案暂时不考虑。所以当发生LBA mismatch时，FW会把对应的bad sector清除。

#### 4.3.3 写介质失败

在当前架构中，写失败出现时error handle模块会保证写失败的数据重新下刷正确。因此，无需trim方案额外考虑。

### 4.4 mpage非对齐部分trim处理

由于表项方案分为二级管理，即L1_map和L2_map（mpage），如下图所示。

![trim3](D:\scaleflux\05_summary\scaleflux_summary\FTL\trim3.png)

对于整张mpage被trim，可以在对应的L1_map中标记trim pend bit，回复命令后再后台处理。而首尾mpage不对齐部分，无法做类似处理。因此在命令回复前，首尾mpage不对齐部分直接处理。

#### 4.5.1 Trim mpage设置

被trim覆盖的mpage，其对应L1_map entry中trim bit被置位，代表该mpage被trim但需处理。下刷trim range mpage，用于记录trim范围信息，同时将L1_map entry中mpage_pba指向trim range mpage。

另外，在L2P patch中加入trim range entry，用于记录本次trim范围。如下图所示，mpage 1被trim但patch中保留这其修改记录。所以我们在L2P patch中追加trim range entry，这样L2P recover时就可知道前面LBA 512的修改记录无效。

需要注意的是，如果dirty的mpage被trim，需要将其从dirty list中移除，防止trim后的mpage下刷。

![trim4](D:\scaleflux\05_summary\scaleflux_summary\FTL\trim4.png)

Trim range mpage为4K大小，其里面信息包含trim start mpage num及trim end mpage num。类似的，Trim range entry in l2p patch，里面也包含trim start mpage num和trim end mpage num信息。

#### 4.5.2 Trim mpage查询

当查询L2P时，需要先检查对应的trim bit是否置位。如果置位，直接返回invalid pba。

#### 4.5.3 Trim mpage展开

Trim bit置位的mpage，后续会被展开处理，可分为两种情况：一种是后台展开处理（主动展开）；另外一种是因为IO write需要更新L2P导致的展开处理（被动展开）。

#### 4.5.4 关于mpage GC

由于trim range mpage下刷到L2P stream，所以不可避免会碰到mpage GC。当mpage GC发现某个mpage的pba指向trim range mpage，且trim bit为1情况下，我们通过下刷empty mpage到新的L2P stream block。

#### 4.5.5 Trim range的掉电恢复

L2P recovery主要分成两部分：

1. Mpage load阶段。即根据mpage summary获取每个mpage的pba，然后读取恢复到DDR中。这个过程中，如果碰到trim range mpage，如下图所示，其左边的且被trim range范围覆盖的mpage均trim。

2. L2P patch scan阶段。掉电前dirty的mpage没有及时下刷，其修改记录被记录到L2P patch。所以恢复时，按照patch添加顺序做L2P修改即可。当碰到trim range entry时，如下图所示，其左边的且被trim range范围覆盖的entry均无效，丢弃即可。

   ![trim5](D:\scaleflux\05_summary\scaleflux_summary\FTL\trim5.png)

   ​

需要注意的是，如果被trim的mpage还未展开就发生掉电，L2P recovery无法恢复原始mpage（被trim前的mpage），这样导致mpage无法展开，继而无法恢复block的valid count。有两种解决办法：

- L2P recovery阶段通过扫描全表来恢复block的valid count和namespace信息。因为原始mpage可能因为mpage GC导致丢失，所以L2P recovery也无法找回原始mpage；

- Mpage GC时，顺便对所有被trim的mpage做展开。这样就避免了因mpage GC导致原始mpage丢失的问题，在L2P recovery阶段也不必扫描全表恢复。

  ### 4.6 mpage对齐部分trim处理

  mpage对齐部分的trim处理，有两种方案：

  - Trim bitmap方案。每个bit对应一张mpage，置位代表该mpage被trim但未做mpage展开处理。如果发生掉电，由PF模块下刷整个trim bitmap，以此完成trim信息的保存和恢复。

  - Trim range方案。在L1_map的每个entry维护一个trim bit，同样也代表这对应的mpage被trim但未做展开处理。不同的是，掉电时L1_map不会下刷。所以我们引入trim range mpage和trim range entry in l2p patch这两样东西，来记录trim信息。

    **mpage展开，是指mpage entry清无效，block的valid count维护及namespace维护。**

对比两种方案，如下表：

| **分类**       | **Trim bitmap方案**                        | **Trim range方案**                         |
| ------------ | ---------------------------------------- | ---------------------------------------- |
| 维护方式         | bitmap集中维护                               | 在L1_map中维护                               |
| 访问效率         | 查表不仅需要访问l2p，同时需要访问trim bitmap，访问效率相对低    | 查表只需访问l2p，访问效率相对高                        |
| 资源占用         | 按照8T盘扩容8倍，每bit代表1个mpage计算，需要4MB          | 在每个L1_map entry中占用1bit                   |
| mpage展开      | 展开生成512个patch entry，可压缩成1个patch entry    | 展开不生成patch entry                         |
| mpage GC     | 无                                        | 由于引入trim range mpage，所以mpage GC对trim range mpage需要重刷empty mpage。 |
| l2p recovery | 整张bitmap下刷保存                             | 通过trim range mpage和trim range entry in l2p来恢复trim信息mpage loading阶段，trim range mpage记录的mpage被invalid；patch scanning阶段，trim range entry覆盖的mpage被invalid。 |
| trim后性能      | 由于trim后随机写4K会导致展开生成512个patch entry，即每4K对应4K数据+4K L2P+4K patch，会导致写性能降低90% | 随机写4K会展开但不生成patch entry，和不被trim写4K的性能理论相当 |

综合基于性能上的考虑，方案上我们选择trim range方案。

