There are mainly three different type of errors:

1. Read Error         -- errors occurring during readding data from Nand
2. Program Error  -- errors occurring during programming data to Nand
3. Erase Error                -- errors occurring during erasing Nand

The principle of error handling is protecting user’s data as much as possible.

## 2.1         Read Retry

The easiest way to handle read error is read retry.

Read the Nand with the same cmd except higher vt level. Vt stands for voltage. There are total 10 different vt levels, from level 0 to level 10. Each vt level’s corresponding voltage value is defined by HW, the values are not simply arithmetic sequence.

The original read cmd has vt set as 0, if fail, increase the vt of the fct entry by 1 and resend the cmd.

If the vt level has already been set to 10 and the read still failed, we should call parity rebuild.

## 2.2         Parity Rebuild

The parity rebuild base on raid, when write data to Nand, we write data on 31 dies, and xor each die’s data and save the result, which we called “parity”, to the die no.31(0 based). We call these total 64 (31:1) dies a parity group, it can also perform as 31:1, 15:1 or 7:1.  In quince, we use 31:1.

When we cannot successfully read out certain die’s data, we can read the other 31 dies’ data and xor them, then we will get the correct data of this die.

We perform the raid rebuild by an independent task – RAID Rebuild Task.

# 3          Program Error Handling

**Program error happens not frequently, therefore, once a program error happens, should mark the pba as grown bad block as soon as possible.**

The basic sequence of **hot stream** program error handle(hereinafter referred to as “Prog EH”) task job has following steps:

- B2N task finds a hot stream program error.
- B2N task early switches block at once. And set the prog error flag of this block.
- B2N task sends a prog eh job to prog eh task.
- Prog EH task notices a valid job, set the block state to tbb, get the parity group start pba.
- Trigger a padding task job to speed up the consuming of the b2ns on current block.
- Wait the parity group closed.
- Add a parity group folding job to folding feed task.
-  Wait the folding finish, release bm.
- Prog EH task waits the block sealed.
- Add the block level folding job to folding feed task.



The basic sequence of **cold stream** program error handle(hereinafter referred to as “Prog EH”) task job has following steps:

- B2N task finds a cold stream program error.
- B2N task early switches block at once. And set the prog error flag of this block.
-  B2N task sends a prog eh job to prog eh task.
- The cold stream task notices the block’s prog error flag, immediately stop doing any folding.
- Prog EH task notices a valid job, set the block state to tbb, get the parity group start pba.
- Wait the parity group close.
- Add a parity group folding job to folding feed task.
- Wait the folding finish, release bm.
- Prog EH task waits the block sealed.
- Add the block level folding job to folding feed task.

# 4          Erase Error Handling

**Erase happens during erase, once it happens, just mark grown bad block.**

**Furthermore, when erase a whole super block, convert all the temp bad block on this super block to grown block.**

Note:

When meeting read err or program err, we just add the tbb rather than gbb, since we still need the folding task to move the data, and need read err to trigger raid rebuild to restore the data.

After erase the block, we can convert tbb to gbb then.

### NOTE

有专门的一块buffer分为raid，当发送RAID_START时，读取出来的值直接写进这个buffer中；发送RAID_XOR命令时，将读取的结果与buffer中进行xor，然后写入buffer中；发送RAID_PARITY时，将buffer中的结果写到指定地址去。

4. prog error 时padding的原因及细节

(1)  padding的原因

消耗掉在这个super blk上预发的b2n命令，以便后续进行folding等操作。

(2) 是否需要对后面的几个page做padding

nand要求是需要做padding，但是出现prog error后，数据很快会被搬走，所以不做也可以。

5. BM释放的关系(user data bm/parity data bm)

user data放在ddr中，若出现了prog error，由FW进行释放

parity data是写在sram里面的，不管是写入成功或失败，hw会主动释放。

所以如果是parity data  prog error，FW不能做释放，释放前应进行判断。

8. CECC, UECC 

UECC: Uncorrectable Error Correction Code, 不可以通过read retry 可以修复

CECC: correctable Error Correction Code.  read retry可以修复

10. 软件导致的program/erase error

若pba计算错误，会导致同一个page被多次写入，当这种page个数达到一定的数量时，写入后面的page，此时会产生program error 。

erase命令发送到bad block时，会出现erase error。