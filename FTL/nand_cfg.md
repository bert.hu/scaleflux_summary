```
sudo fdisk -l
查看sector size
```

```
l2p 0x57ECDAF
[4-0:0:0D|2:4:53:503]        Bkops:                       lpa: 0x57ecdaf, the corresponding pba is: 0x1dc29004511684
UD2214C0355H@CSDU5SPC76M1:/$ parsepba 0x1dc2900451684
pba.all 0x1dc2900451684:
ch:       10
ce_lun:   5
block:    512
page:     552
plane:    1
offset:   0x0
type:     0
ep_idx:   0
ccp_off:  0x30a
comp_len: 0x1d

nand addr fields:
lun:      0x1
block:    0x200
plane:    0x1
page:     0x668
row_addr: 0x1801668
la:       0x0
type:     0x0
ln:       0x1
ce:       0x1
ch:       0xa
high32:   0xa500

#define CE_OF_CE_LUN(ce_lun)            ((ce_lun) % (CFG_CE_NUM))
#define LUN_OF_CE_LUN(ce_lun)           ((ce_lun) / (CFG_CE_NUM))
#define GET_CE_LUN(ce, lun)             (((lun) * CFG_CE_NUM) + (ce))
#define GET_DIE_BY_CH_CE_LUN(ch, ce, lun)  (((((lun) * CFG_CE_NUM) + (ce)) << CFG_PBA_CHAN_BITS) | (ch))
```

