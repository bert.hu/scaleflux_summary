```
目前FTL Recovery的流程
1、恢复L1 map
   1)Search summary sblock,找到所有STREAM_ID_L2P的block
   2)如果是BS_SEALED_BLOCK，考虑两种情况，early_switch和normal，如果是early_switch，则处理方式和BS_OPEN_BLOCK相同，normal的是loading summary，而另两种情况是recovering summary
   3)loading summary(l2p summary)，则_smry_buf = (mpage_summary *)DDR_MPAGE_SUMMARY_FLUSH_START(是需要load summary并进行解析的地址，源地址是MPAGE_SUMMARY_LPA)，还有三个重要的数据，blk_id, _curr_pba, _tot_smry，然后发ckp_cmd::ckp_send_rd_cmd并且ckp_cmd::ckp_poll_rd_cpl，每笔需要解析的summary的大小是g_mpage_gc_task.get_summary_flush_offset(),并逐笔更新g_l2p_mgr.mpage_map_update_for_recovery,实际更新的是_l1_map[l1_id].mpage_pba
   4)recovering summary，首先获取这个block的_current_read_pba, _end_read_pba，其中_current_read_pba是指向last_prog_pba的下一个pba，同时g_mpage_gc_task.mpage_reset_summary(),然后开始recover_open_summary,本质上做的就是从这个block的_current_read_pba到_end_read_pba的HEADER_EXTRACTION(ckp_cmd::ckp_send_hdr_ext_cmd(_current_read_pba, L2P_PATCH_DDR_BUF_START, FCQ_IO_PF_RD_SMRY)),每笔HEADER_EXTRACTION的大小是HDR_BUF_SIZE，全部收到完毕以后，开始对L2P_PATCH_DDR_BUF_START地址进行解析parse_header_extraction_result()，只要是LP_GET_USER_TYPE(hdr->lpa) == USER_L2P_META的HEADER_EXTRACTION，如果l1_id有效，则更新g_mpage_gc_task.mpage_update_summary(l1_id, hdr_pba.all)，这是以打补丁的方式去更新之前loading 和 parsing的summary地址DDR_MPAGE_SUMMARY_FLUSH_START
   
2、patch_load_dispatch
  1)Search patch sblock,找到所有STREAM_ID_L2P_PATCH的block，并g_l2p_patch_mgr.init_dirty_mpage_bitmap()
  2)把这个block上的读取到_pingpong_operation[PING_OPERATION]中，read_and_dispatch_patch(PING_OPERATION)，restore recovery window
  
3、partial_blk_handle
  1)Search tlc partial sblock,找到所有有footer的stream的BS_OPEN_BLOCK
  2)依次读取这些block，获取_current_stream和_last_maplog_pba，_current_read_buf=DDR_MPAGE_SUMMARY_LOADALL_START，再获取这个block的_start_read_pba,和_end_read_pba
  3)开始HEADER_EXTRACTION，将结果保存到_hdr_entry[_sent_read_cnt].buffer和_hdr_entry[_sent_read_cnt].pba
  
```

#### FULL Recovery的流程

```
full_scan_recovery
1、g_l2p_mgr.l2p_init(g_l2p_mgr._format_capacity_in_GB)，初始化l2p_mgr::init()，使用的时sys_gdma_memset
2、g_small_share_rfs_file记录_force_flag
3、partial_blk_handle
   1)Search tlc partial sblock,找到BS_OPEN_BLOCK且有footer的stream，存放地址是DDR_MPAGE_SUMMARY_LOADALL_START
   
4、CLEAR_BLK_MGR_DATA
   1)g_pu_mgr.pblk_mgr[0].reset_block_table_for_full_recovery()
5、DEALLOCATE_ALL_SLC_BLOCKS
   1)g_pu_mgr.pblk_mgr[0].deallocate_all_slc_blks()
6、Enable b2n task with normal mode during full recovery
7、FULL_DRIVE_SCAN_AND_RECOVER_L2P:ftl_ctrl.full_drive_scan()
   1)Search tlc data sblock,STREAM_ID_HOT和IS_COLD_STREAM，BS_SEALED_BLOCK和BS_OPEN_BLOCK的
   2)根据block获取_start_read_pba和_end_read_pba，如果两个不相等，则READ_GTS
   3)根据_start_read_pba获取到_gts_pba，并根据_gts_pba将gts读取到_gts_read_buf，并记录到_pending_read_array，直到读完这个parity group
   4）sort_all_pending_pba_by_gts_order
   5) CHECK_NEED_HEADER_EXTRACTION,从DDR_MPAGE_SUMMARY_LOADALL_START开始，每次对一个parity group的从_start_read_pba开始HEADER_EXTRACTION
   6) full_scan_parse_hdr_extraction_result,把lpa和pba对存到_pending_update_array里面
   7) 再利用_pending_update_array保存的结果来UPDATE_L2P，调用g_l2p_mgr.l2p_map_update
   8) 最后update_all_sblock_space()，只更新BS_SEALED_BLOCK的block info

8、清除g_small_share_rfs_file的_force_flag
```



#### L1 map的数据结构

```c++
struct l1_map_entry {
    //mpage using slc uncompress write, some bit in pba_t can be ignore and convert to this 40bit format
    u64 mpage_pba: 40; 
    //index for l2 map, max bit should cover MAX_L2_MAP_NUM, INVALID_L2_ID for un-cached
    u64 l2_id: 23;  
    //to avoid dirty q full
    u64 in_dirty_list: 1; 
};

初始化的地方
    l2p_mgr::pre_init_for_pf_recovery()
    for(i in L1_MAP_ENTRY_NUM){
        _l1_map[i].mpage_pba     = INVALID_MPAGE_PBA;
        _l1_map[i].in_dirty_list = 0;
        _l1_map[i].l2_id         = INVALID_L2_ID;
    }
L1_MAP_ENTRY_NUM的计算方法
    先根据全盘容量计算LPA的个数，每个LBA大小是512B，每个LPA代表的大小是4K
    所以CAP>>3表示有多少个LBA
    然后计算每个L2_MAP_ENTRY(Mpage:4K)可以包括多少条LPA(计算公式为(L2_MAP_ENTRY_SIZE / PBA_SIZE))
    然后就可以算出全盘这么多LPA需要多少条L1_MAP_ENTRY
    
    
```

#### 细读Recovery流程

```
// pf pending command including b2n and w2p commands
bool pf_mgr::is_all_pending_write_cmd_cpl()
涉及变量_sent_b2n_cnt, _received_b2n_cnt, _sent_w2p_cnt, _received_w2p_cnt

void recovery_task::handle_ongoing_shrink_cap()
如果是shrink，则COUNT_LBA_BY_CAP(g_l2p_mgr._format_capacity_in_GB) > COUNT_LBA_BY_CAP(_change_format_cap_cmd)
这两个的区别是什么？
_change_format_cap_cmd，g_l2p_mgr.l2p_update_format_cap(_change_format_cap_cmd, true)
_change_provision_cap_cmd，g_l2p_mgr.l2p_update_provision_cap(_change_provision_cap_cmd)
```

```flow
st=>start: recovery_task::run
e1=>end: switch_state
cd1=>condition: _pause_req？
cd2=>condition: is_all_pending_write_cmd_cpl？
op=>operation: RECOVERY_TASK_PAUSED
st(right)->cd1(yes,right)->cd2(yes,right)->op
cd1(no,bottom)->e1
cd2(no,bottom)->e1
```

```
g_pf_dump_task.enable_pf_dump()
g_pf_dump_task.disable_pf_dump()

    case GSD_EXIT_TASK_STATE_SUCCESS: {
        g_pf_dump_task.disable_pf_dump();
    case GSD_EXIT_TASK_STATE_UNDER_RESET: {
        // GSD => RESET
        g_pf_dump_task.enable_pf_dump();// In case that pf may happen under reset state
    case MAT_TASK_STATE_CLEAR_RECOVER_MODE:
        if (mat_save_recover_mode(false)) {
            g_pf_dump_task.enable_pf_dump();
            _state = MAT_TASK_STATE_SUCCESS;
        }
    case RECOVERY_TASK_REFRESH_RFS_WAIT: {
        if (is_refresh_rfs_done()) {//cstat !MISRAC++2008-0-1-2_a
            if (RECOVERY_MODE_GSD == _recovery_mode) {
                gsd_setup_running();
                _recover_in_progress = false;
            }

            if (RECOVERY_MODE_PF != _recovery_mode) {
                EVT_LOG(enable_pf_dump_evtlog);
                g_pf_dump_task.enable_pf_dump();// from now,start enter normal pf flow
                g_gsd_exit_task.add_gsd_loop_cnt();
            }
            
    case LOW_LEVEL_STATE_RESTORE_CLEAN_UP: {
        DBG_LOG("PF clean up\n");
        u32 ret = restore_ctrl._pf_cleaner.clean_up_nor();
        if (ret != PF_RESTORE_NO_ERROR) {
            if (ret == PF_RESTORE_NEED_RETRY) {//cstat !MISRAC++2008-0-1-2_a !MISRAC2004-13.7_a
                break;
            }
        }
        BOMB(PF_CLEAN_UP_NOR_FAIL, ret == PF_RESTORE_NO_ERROR);
        ret = restore_ctrl._pf_cleaner.clean_up_pf_reserved_blk();
        BOMB(PF_CLEAN_UP_PF_BLOCK_FAIL, ret == PF_RESTORE_NO_ERROR);

        DBG_LOG("Enable PF dump\n");
        EVT_LOG(enable_pf_dump_evtlog);
        g_pf_dump_task.enable_pf_dump();// from now,start enter normal pf flow
        restore_ctrl._restore_state = LOW_LEVEL_STATE_PAD_ALL_DATA_STREAM;
        break;
    }

#define CFG_BLOCK_NUM           (556)
#define CFG_PAGE_NUM            (712)
#define CFG_SLC_PAGE_NUM        (704)
#define CFG_NAND_PAGE_NUM       (2112)
#define CFG_PAGE_TYPE_NUM       (3)
#define CFG_PLANE_NUM           (4)

#define PF_RESTORE_COMPLETE_SIGNATURE U(0xABABABABABABABAB)
#define PF_DUMP_COMPLETE_SIGNATURE    U(0xCDCDCDCDCDCDCDCD)

其中save_root_data()的时候将PF_DUMP_COMPLETE_SIGNATURE写到nor里面
pf_restore_cleaner::clean_up_nor()的时候PF_RESTORE_COMPLETE_SIGNATURE写到nor里面

哪里会检查这两个标志？
在pf_data_parser::read_root_data的时候
bool pf_data_parser::check_data_valid(pf_nor_data* nor_data)
        case READ_ROOT_DATA_CHECK_VALID: {   //cstat !MISRAC2004-15.2
            if (check_data_valid(nor_data)) {
                _find_valid = true;
                _read_root_data_state = READ_ROOT_DATA_COMPLETE;
            } else {
                _find_valid = false;
                _read_root_data_state = READ_ROOT_DATA_SEARCH_NEXT_SECTOR;
                break;
            }
这里会检查PF_DUMP_COMPLETE_SIGNATURE
如果clean_up_nor()的时候PF_RESTORE_COMPLETE_SIGNATURE写到nor里面，但g_pf_dump_task.enable_pf_dump()没有置上就PF的话，那此时PF_RESTORE_COMPLETE_SIGNATURE和PF_DUMP_COMPLETE_SIGNATURE就都存在


pf_dump_task::run
如果从触发PF中断开始超过了PF_DUMP_NOR_DEBUG_DATA_INTERVAL_US=15ms的时候，且is_pf_dump_enabled()，则save_debug_data

如果hw_aborter.abort_hw()失败且从触发PF中断开始超过了PF_DUMP_HW_ABORT_TIMEOUT_US=10ms，则打印hf，走failed流程

在hw_aborter.abort_hw()成功以后还要check io clear should be moved after hw abort
check_all_task_clear_after_abort()
这里主要检查IO的cq是否为空，以及b2n的fcq是否还有pending commands

然后依次flush data
1.flush_user_data
2.flush_parity_data
3.parity_padding
4.prepare_meta_data
5.flush_meta_data
6.flush_summary_data
7.parity_padding
然后开始等待flush 完毕
PF_TASK_STATE_WAIT_CMD_CPL
g_pf_mgr.is_all_pending_write_cmd_cpl()
主要是看这些变量
g_pf_mgr._sent_b2n_cnt, g_pf_mgr._received_b2n_cnt, g_pf_mgr._sent_w2p_cnt, g_pf_mgr._received_w2p_cnt
此时如果is_pf_dump_enabled，则data_flusher.save_root_data()，这里会将PF_DUMP_COMPLETE_SIGNATURE写到nor里面
否则不保存data_flusher.save_root_data()，直接打印pe
/*
VPP must go down to 0V before VCC ramping up,
So you should set VPP disable after PF completion.
VCC表示模拟信号电源
vcc是电路的供电电压。
VPP：编程/擦除电压
一般来说VCC=模拟电源,VDD=数字电源,VSS=数字地,VEE=负电源
*/
PF以后必须保证VPP=0，这样启动的时候才能正常将VCC拉高

注意：
g_pf_mgr.root_data.pf_cnt只有在mat以后才会清零

```

```
_first_enter_recovery，表示的是第一次进入recovery，还没结束
当_recovery_mode从rfs中读出来是RECOVERY_MODE_FREEZE或RECOVERY_MODE_READONLY的时候，_first_enter_recovery会标记为false
或者当recovery成功或失败的时候，_first_enter_recovery会标记为false

RECOVERY_TASK_START的时候
如果_first_enter_recovery会标记为false的时候，那么之前可能是：
1、recovery成功
2、recovery失败
这时候来了SMP_STATE_RECOVERY
进入else的两种情况
1、_recovery_mode == RECOVERY_MODE_GSD，此时g_pf_dump_task.enable_pf_dump()，然后将RECOVERY_MODE_PF刷到rfs里面，如果此时发生PF怎么办？
2、_recovery_mode == RECOVERY_MODE_PF，这里需要处理handle_ongoing_shrink_cap，_has_ongoing_se和_change_format_cap_cmd的事情


GSD => RESET
g_pf_dump_task.enable_pf_dump();// In case that pf may happen under reset state
g_small_share_rfs_file.update_and_save_recovery_mode_async(RECOVERY_MODE_PF, &_flush_cpl_signal)

GSD_EXIT_TASK_STATE_SUCCESS
RECOVERY_MODE_GSD == g_small_share_rfs_file.get_recover_mode()
g_pf_dump_task.disable_pf_dump()

MAT_TASK_STATE_CLEAR_RECOVER_MODE
g_pf_dump_task.enable_pf_dump()


bool small_share_rfs_file_handler::update_and_save_recovery_mode_sync(RECOVERY_MODE_E mode __unused)
{
    set_recover_mode(mode);
    return save_to_root_fs_sync();
}

bool small_share_rfs_file_handler::update_and_save_recovery_mode_async(RECOVERY_MODE_E mode __unused,u8 * comp_signal /*= nullptr*/)
{
    set_recover_mode(mode);
    return save_to_root_fs_async(comp_signal);
}
```

```
UD2214C0520H@CSDU5SPC76M1:/$ l2pdbg 1
init state: 6, init_idx 0x0
2K_MPAGE 0, RAW_CAPACITY 0x80000000000, DDR_SIZE 0x400000000
provision_capacity 7680, format_capacity 7680
MAX_LBA 0x37e3e92bf, MAX_LPA 0x6fc7d257
L1_MAP_ENTRY_NUM 0x37e3ea
MAX_CACHE_L2_MAP_ENTRY_NUM 0x380043, DEFAULT_L1_MAP_ENTRY_NUM 0x37e3ea
MAX_L1_MAP_ENTRY_NUM 0x6fc7ce, MAX_DIRTY_LIST_LEN 0x150018
DDR_L1_MAP_SIZE 0x37e4000, DDR_L2_MAP_SIZE 0x380043000
DDR_L1_NODE_SIZE 0x6fc8000, ALLOC_L2_MAP_SIZE 0x380043000
ptr: l1_map 0x458062000, l2_map 0x47ffbd000, l1_node 0x45b846000
```

计算盘的容量

```
#if (ENABLE_8K_MAP_UNIT)
    #define MAP_UNIT_ROUND_UP_SIZE (ULL(97696368))
#else
    #define MAP_UNIT_ROUND_UP_SIZE (ULL(97696368) + ULL(16))
#endif
#define COUNT_LBA_BY_CAP(cap)      ((((((u64)(cap)-50) * 1953504ULL) + MAP_UNIT_ROUND_UP_SIZE) >> 3) << 3)

#if (OP_CFG == OP_7)
    #define DEFAULT_PROVISION_CAPACITY_IN_GB (u64)(RAW_CAPACITY_IN_GiB * 9375 / 10000)// 7%
    #define MAX_CACHE_CAP_FOR_4K_UNIT (u64)(((CAPACITY_TiB(8)) >> 30) * 9375 / 10000)// 7%
#elif (OP_CFG == OP_14)
    #define DEFAULT_PROVISION_CAPACITY_IN_GB (u64)(RAW_CAPACITY_IN_GiB * 8765 / 10000)// 14%
    #define MAX_CACHE_CAP_FOR_4K_UNIT (u64)(((CAPACITY_TiB(8)) >> 30) * 8765 / 10000)// 14%
#elif (OP_CFG == OP_21)
    #define DEFAULT_PROVISION_CAPACITY_IN_GB (u64)(RAW_CAPACITY_IN_GiB * 8251 / 10000)// 21%
    #define MAX_CACHE_CAP_FOR_4K_UNIT (u64)(((CAPACITY_TiB(8)) >> 30) * 8251 / 10000)// 21%
#elif (OP_CFG == OP_28)
    #define DEFAULT_PROVISION_CAPACITY_IN_GB (u64)(RAW_CAPACITY_IN_GiB * 78125 / 100000)// 28%
    #define MAX_CACHE_CAP_FOR_4K_UNIT (u64)(((CAPACITY_TiB(8)) >> 30) * 78125 / 100000)// 28%
#endif

比如4T的盘，则DEFAULT_PROVISION_CAPACITY_IN_GB = 4* 1024* 9375 / 10000=3840G
则cap=3840
COUNT_LBA_BY_CAP(cap)=（3840-50）*1953504+97696368+16 = 7,501,476,544
```

```
静态检查
//cstat !MISRAC2004-13.7_b !MISRAC++2008-0-1-2_b
!MISRAC2004-13.7_b：不符合MISRA-C 2004规则13.7
!MISRAC++2008-0-1-2_b：不符合MISRA-C++ 2008规则0-1-2
这些标记通常用于静态代码分析工具，用于指示代码中存在不符合特定编码标准的部分。其中，MISRA-C和MISRA-C++是一些常用的编码规范，旨在提高代码的可读性、可维护性和可靠性。
加上这个注释并不能直接使静态工具的报错消失。这个注释只是用来指示静态工具忽略与MISRA-C 2004规则13.7和MISRA-C++ 2008规则0-1-2相关的报错。具体要解决静态工具报错，可能需要根据具体的报错信息进行代码修改或采取其他适当的措施来符合相应的编码标准。注释只是提供了一种标记的方式，用于告知工具不对该行代码进行检查，但并不意味着代码本身是符合规范的。
```

