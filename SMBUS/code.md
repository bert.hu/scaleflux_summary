```
// (i2c clk / (scl freq * 2)) - 1,i2c clk:187.5M
// scl freq:100k(standard mode)
#define API2C_SCL_PERIOD_HIGH_STANDARD             0x3A9       // 5us
#define API2C_SCL_PERIOD_LOW_STANDARD              0x3A8       // 5us
#define API2C_SDA_SETUP_TIME_STANDARD              0x5e        // 500ns
#define API2C_SDA_HOLD_TIME_STANDARD               0x71        // 600ns
#define API2C_SPIKE_FILTER_VAL_STANDARD            0xa         
#define API2C_WAIT_TIME_ENABLE_STANDARD            true        
#define API2C_TIMER_CONTROL_VAL_STANDARD           0xF270      // 80ms
#define API2C_TIMER_CONTROL_IBML_VAL_STANDARD      0xFFFF      // 89ms
```

smbus_get_basic_mi_cmd_info

```
slave_addr：
1、SLAVE_ADDR3_0x6A
2、SLAVE_ADDR2_0x53
3、SLAVE_ADDR0_0x1D
4、SLAVE_ADDR1_0x52 // NOT SUPPORT
```

smbus_slave_read

```
1、TRANSFER_SLAVE_ONLY_READ // invalid option, return dummy data
2、TRANSFER_SLAVE_READ_NEXT // invalid option, return dummy data
3、TRANSFER_SLAVE_WRITE_READ
```

smbus_slave_write

```
pdata[0] = (g_st_smbus_info.cur_slave_addr << 1);

```

smbus_slave_send

```

```

api2c_get_slave_transfer_mode

```
TRANSFER_IDLE
TRANSFER_SLAVE_ONLY_READ
TRANSFER_SLAVE_READ_NEXT
TRANSFER_SLAVE_WRITE_READ
TRANSFER_SLAVE_READ_COMPLETE
TRANSFER_SLAVE_WRITE
TRANSFER_SLAVE_WRITE_COMPLETE
```

smbus_master_write



smbus_check_transfer

```
polling smbus inerrupt status in admin task
1、IDLE_MODE
2、SLAVE_MODE
3、MASTER_MODE // nothing to do
```

```c++
// 0x74
union slave_interrupt0_status_u
{
    struct
    {
        u16 fifo_threshold            : 1;// bit[0]
        u16 write_transfer_complete   : 1;// bit[1]
        u16 addr0_read_start          : 1;// bit[2]
        u16 addr0_read_repeated_start : 1;// bit[3]
        u16 addr0_read_next_data      : 1;// bit[4]
        u16 addr0_read_complete       : 1;// bit[5]
        u16 addr0_read_time_out       : 1;// bit[6]
        u16 addr0_read_error          : 1;// bit[7]
        u16 addr1_read_start          : 1;// bit[8]
        u16 addr1_read_repeated_start : 1;// bit[9]
        u16 addr1_read_next_data      : 1;// bit[10]
        u16 addr1_read_complete       : 1;// bit[11]
        u16 addr1_read_time_out       : 1;// bit[12]
        u16 addr1_read_error          : 1;// bit[13]
        u16 ibml_tlow_sext            : 1;// bit[14]
        u16 ibml_time_out             : 1;// bit[15]
    } interrupt_status_info;
    u16 reg;
};
```

