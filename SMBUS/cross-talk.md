1、NVMe MI协议

在一个存储系统中，将设备管理与业务分离是一个良好的设计，为了能够更规范合理得对NVMe SSD进行管理，NVMe-MI协议应运而生。Nvme-MI(Management Interface)，定义了一套完整的NVMe SSD管理方式，独立于NVMe协议且为NVMe服务。

与NVMe协议不同，NVMe-MI带外协议是通过管理组件传输协议MCTP协议进行传输，同时底层物理层支持PCIe或者SMBus/I2C，物理层使用I2C的好处就是和NVMe协议分离，即使SSD在NVMe看来是故障的，还能有另外一条路径查看SSD状态。

MI 协议主要涉及物理层smbus/I2C， 中间传输层的MCTP，和最上层封装起来的MI 层。

![NVMe_MI_out-of-band](D:\scaleflux\03_summary\SMBUS\NVMe_MI_out-of-band.png)

2、NVMe MI协议的执行过程

```txt
Host 发MI报文消息给 device（slave）
先将MI 报文封装成MCTP报文
将完整MCTP报文通过I2C/smbus 链路发给device
device 收到MCTP报文，解封MCTP报文得到MI报文
解析MI 报文发送消息，slave device 会回复的消息数据以MI 格式封装成MI 报文
再将MI报文封装成MCTP数据封装
再将MCTP报文通过I2C链路发给host
Host 解封MCTP报文，再回到step 1，形成闭环。
```

3、消息传输

定义了一个支持多种消息传输的接口。消息格式与带外机制和带内隧道机制相同。

3.1 NVMe-MI消息

NVMe-MI消息在带外机制和带内隧道机制中都有使用。

在带外机制中，NVMe-MI消息由一个或多个MCTP包的有效负载组成。NVMe-MI消息的最大大小为4224Byte（即：4KB + 128Byte）。长度大于4224Byte的NVMe-MI消息被认为是无效的。

在带内隧道机制中，NVMe-MI消息不拆分为MCTP报文，且NVMe-MI消息的最大大小等于最大数据传输大小（MDTS）

3.1.1 NVMe-MI消息字段

NVMe-MI消息的格式由：Message Header(1dword) + Message Data + Message Integrity Check（1dword）。如果完整性检查（IC）bit设置为“1”，则NVMe-MI消息以IC结束。

![NVMe_MI_Message](D:\scaleflux\03_summary\SMBUS\NVMe_MI_Message.png)

3.2 带外消息传输

带外机制利用MCTP作为管理控制器和管理端点之间可靠的有序消息传输。

3.2.1 MCTP包

在MCTP基本规范中，数据传输的最小单位是MCTP包。将一个或多个报文组合起来创建MCTP消息。在这个规范中，MCTP消息被称为NVMe-MI消息。一个数据包总是包含至少1Byte的有效载荷，但总长度不得超过协商的MCTP传输单元大小。

![MCTP_Packet_Format](D:\scaleflux\03_summary\SMBUS\MCTP_Packet_Format.png)

3.2.2 报文组装成消息

一个NVMe-MI消息可以被分解成多个MCTP包载荷，并作为一系列包发送。下图显示了一个NVMe-MI消息示例，它的内容被分成四个MCTP包。

数据帧主要分为三个部分，physical header（物理层的header） + MCTP header + payload（mi header / data / mic（CRC-32校验））

多个packets中，中间packets不包括mi header和mic（CRC-32校验）

- [ ] command msg每个packets不能超过固定的unit size，unit size可以通过MI Config Set的命令来设置，SMBUS最大是64~256 bytes之间配置，具体看host如何使用，目前固定size是64 bytes，每一个slot每一次只能固定处理一组request / response，需要收到对应的response才能允许下一笔的cmd

- [ ] control primitive的一个cmd对应一个packet，不拆分，允许多个control primitive下发，但最终只会处理最后一笔cmd

![MI-Message](D:\scaleflux\03_summary\SMBUS\MI-Message.png)

![NVMe-MI_Message_Spaning_Multiple_MCTP_Packets](D:\scaleflux\03_summary\SMBUS\NVMe-MI_Message_Spaning_Multiple_MCTP_Packets.png)

3.2.3 附加要求

```txt
除消息中最后一个数据包外，给定消息中所有数据包的MCTP传输单元大小应与协商的MCTP传输单元大小相等。
一个完整的NVMe-MI 消息组装完成后，就需要校验消息的完整性。如果校验通过，则处理NVMe-MI消息；如果校验失败，则丢弃NVMe-MI消息
```

4、消息服务模型

4.1 NVMe-MI消息分类

NVMe-MI消息的两个主要类别是**请求消息**和**响应消息**。

请求消息可以分类为**命令消息**或**控制原语**，其中命令消息还可以进一步分类为**NVMe-MI命令**、**NVMe管理命令**或**PCIe命令**，**控制原语**在带外机制中使用。

响应消息可分为**成功响应**和**错误响应**。

![NVMe-MI_Message_Taxonomy](D:\scaleflux\03_summary\SMBUS\NVMe-MI_Message_Taxonomy.png)

4.2 MI报文格式

一个完整MI报文分为3部分，Header，Data和IC（Integrity Check）。

Header：总共4个字节，主要指明该MI消息的类型；

Data：MI消息的具体数据，数据格式和大小因Header里面指明的类型不同而不同；

IC：总4个字节，Header和Data的CRC校验值，使用的是CRC-32C算法。

1）NVMe-MI Command：定义了获取NVMe SSD设备状态命令。

2）NVMe Admin Command：用MI协议规范模拟封装NVMe命令

3）PCIe Command：用MI协议规范模拟封装PCIe报文

![NVMe-MI_command_request](D:\scaleflux\03_summary\SMBUS\NVMe-MI_command_request.png)

![NVMe-MI_command_opcodes](D:\scaleflux\03_summary\SMBUS\NVMe-MI_command_opcodes.png)

![NVMe-MI_command_response](D:\scaleflux\03_summary\SMBUS\NVMe-MI_command_response.png)

- [ ] VPD Read

  VPD读取命令用于读取重要产品数据（VPD），成功完成VPD Read命令后，将响应数据。

  VPD Read命令使用NVMe Management Dword 0和1，长度为0且没有数据的VPD读取命令是有效的。

- [ ] VPD Write

  VPD写入命令用于更新重要产品数据（VPD），成功完成VPD Write命令后，直接读取FRU信息设备或成功完成VPD Read命令应该返回新的要写入的VPD内容，这些Request Data field中的指定数据应该是写到VPD中。

  VPD Write使用NVMe管理Dword 0和1，长度为0且没有数据的VPD写命令是有效的。

  VPD over SMBUS无需支持MCTP的协议，只需要识别固定的slave地址，对应的offset，即可获取到数据，并立即返回

4）Control Primitive：控制MI命令执行

![control_primitive](D:\scaleflux\03_summary\SMBUS\control_primitive.png)

![control_primitive_opcodes](D:\scaleflux\03_summary\SMBUS\control_primitive_opcodes.png)

![command_servicing_state_diagram](D:\scaleflux\03_summary\SMBUS\command_servicing_state_diagram.png)

5）Success Response：成功返回MI消息，通常带数据返回，每个命令的返回都不一样，详细见具体命令。

![success_response](D:\scaleflux\03_summary\SMBUS\success_response.png)

6）Error Response：失败返回MI消息，无数据，只有错误类型。

![Error_response](D:\scaleflux\03_summary\SMBUS\Error_response.png)

5、SMBUS概述

SMBUS作为物理链路层使用，作为物理链路，SMBUS需要进行主从切换数据，默认SMBUS作为slave模式接收数据，而response可以是slave模式，或者master模式，具体是根据命令区分

Myrtle中存在SMBUS0/SMBUS1两条总线，每条总线中API2C IP可以配置多个slave地址

```c++
    g_st_smbus_cfg[0].addr0              = SLAVE_ADDR0_0x1D;
    g_st_smbus_cfg[0].addr0_enable       = true;
    g_st_smbus_cfg[0].addr1              = SLAVE_ADDR1_0x52;
    g_st_smbus_cfg[0].addr1_enable       = false;  // disable
    // todo
    g_st_smbus_cfg[0].addr2              = SLAVE_ADDR_INVALID;
    g_st_smbus_cfg[0].addr2_enable       = false;
    g_st_smbus_cfg[0].addr3              = SLAVE_ADDR_INVALID;
    g_st_smbus_cfg[0].addr3_enable       = false;

    g_st_smbus_cfg[1].addr0              = SLAVE_ADDR2_0x53;
    g_st_smbus_cfg[1].addr0_enable       = true;
    g_st_smbus_cfg[1].addr1              = SLAVE_ADDR3_0x6A;
    g_st_smbus_cfg[1].addr1_enable       = true;
    // todo
    g_st_smbus_cfg[1].addr2              = SLAVE_ADDR_INVALID;
    g_st_smbus_cfg[1].addr2_enable       = false;
    g_st_smbus_cfg[1].addr3              = SLAVE_ADDR_INVALID;
    g_st_smbus_cfg[1].addr3_enable       = false;
```

Myrtle使用三个salve地址，分配在SMBUS0，SMBUS1，根据中断状态区分不同的总线及对应的地址。

6、设计约束

```
在配置PLL的同时需要重新配置SMBUS时钟参数
SMBUS默认配置为slave模式，使能三个slave地址:0x1D/0x6A/0x53，在识别到nvme normal mi时，需要切换master模式发送数据，处理完成设为slave模式
Master模式，填充数据时，需要保证fifo不能满，预留出一定空间，否则有可能出现NAK，目前的操作是数据填充MAX_LEN – 2
Master模式，需要发送cmd，目前支持Manual/Auto/Sequence三类，区别在于Auto/Sequence会在传输结束时，自动发送Stop
Slave模式，只有根据slave interrupt状态，查看当前操作是读还是写，无法知道传输的长度，需要polling中断状态做出下一步动作
PEC计算，Master模式需要在已有的传输的长度+1，但是不能去填数据，IP会自动补PEC，否则传输的数据会多一个byte；Slave模式需要在传输结束后，填一个dummy数据，并且需要标记一个PEC flag
在初始化SMBUS后，host可能会立马识别到相关的slave地址，ApI2C IP会预先处理数据（包含数据接收、ACK/NAK处理），但是fw可能还没有及时处理，导致超时异常，需要处理IP的状态和FIFO数据
API2C的IP只支持SMBUS2.0协议，所以SMBUS 400K的时序不支持操作，但是I2C可以支持400K
```

7、关键流程

7.1 NVME MI地址分配

| Address | Function                                 |
| ------- | ---------------------------------------- |
| 0x1D    | slave write模式，接收nvme normal mi数据         |
| 0x6A    | slave read模式，接受nvme basic mi数据offset，并且回传数据给host |
| 0x53    | slave read模式，接受nvme mi vpd数据offset，并且回传数据给host |
| 0x10    | master write模式，发送nvme mi数据给BMC           |

7.2 SCL/SDA初始化配置

时钟配置需要满足smbus标准协议，下图是100k的参考值

![config](D:\scaleflux\03_summary\SMBUS\config.png)![config2](D:\scaleflux\03_summary\SMBUS\config2.png)![config3](D:\scaleflux\03_summary\SMBUS\config3.png)

##### 7.2.1 100K配置 APB时钟：187.5MHZ

```txt
时钟频率：I2C module pclk = 187.5MHZ(5.333ns) = (2250MHZ/12)

计算公式：SCL High Period:(Desired_SCL_High_Time/PCLK_period) - 1

Desired_SCL_High_Time:5us

PCLK_period:5.333ns

SCL High Period (Offset 0x80): 937

计算公式：SCL Low Period (Offset 0x84): (Desired_SCL_Low_Time/PCLK_period) - 1

Desired_SCL_Low_Time: 5us

PCLK_period:5.333ns

SCL Low Period (Offset 0x84):938

计算公式：(Desired_Time/PCLK_period)

SDA Setup Time (Offset 0x8C): 95(500ns)

SDA Hold Time (Offset 0x90):113(600ns)
```

7.3 超时机制

API2C IP支持四类超时机制的检测

1、Master/Slave wait timer(0xC)

2、IBML time-out timer(0x10)

3、IBML Master Clock extend timer(0x14)

4、IBML Slave Clock extend timer(0x18)

时钟分频器Timer Clock Divider Control (Offset 0x1C)，这个值含义表示2的幂次方，所以真正的超时时间等于timer val * divider val

注意，IBML timer需要使能两个点，一个是global control register（0x0）中bit2，第二个就是每个timer中的bit15

IP的Timer计时软件无法探测到，实际应用中，IP会预处理host发的数据，而后等待FW进行相关处理，若在这个等待期间，FW没有及时处理，就可能会发生NAK或其他异常。

### 7.3.1 Master/Slave wait timer

Wait timer用于测量SCL信号被拉低的场景。

1、FIFO full

2、FIFO empty

3、master模式中等待下一个cmd

master模式中，FIFO full/empty触发timer out，master会释放SCL，发送NAK，发送STOP信号，IP状态回到IDLE

slave接受数据模式中，当FIFO数据少于两个bytes字节可以存放，触发timer out，slave会释放SCL，发送NAK，IP状态回到IDLE

slave发送数据时，fifo为空，触发timer out，slave会释放SCL，IP自动发送一个预先设定的dummy数据。

目前配置的时间是21972*512*5.333us=89ms

### 7.3.2 IBML Clock Low (tTIMEOUT) Timer

IBML timer用于测量实际SCL拉低的场景。

Master模式中，触发time out，在数据结尾发送一个stop；Slave模式中，立马释放SCL和SDA。目前fw未使用。

### 7.3.3 IBML Master Clock Extend tLOW:MEXT Timer

IMBL slave timer用于在传输过程中，一个Byte（Start-ACK, ACK-ACK, ACK-Stop）数据SCL的拉低时间，触发time out后，产生中断。IBML timer使能值必须大于wait timer的值。目前fw未使用。

### 7.3.4 IBML Slave Clock Extend tLOW:SEXT Timer

IMBL slave timer用于在传输过程中，一整帧（start-stop/start another slave）数据SCL的拉低时间。触发time out，slave释放SCL和SDA至idle状态。IBML timer使能值必须大于wait timer的值。目前配置的时间是32768*512*5.333us=89ms。

## 7.4 Slave读操作

SMBUS读操作存在两种场景，一是写回读，序列为slave addr + write data + slave addr + read data，这里的write data指的是需要读取数据的offset；二是读操作，序列为slave addr + read data。本专题主要考虑的场景是写回读。如何判断这个场景？根据repeat start的信号判断，但是注意，若这个时候有slave 地址切换，也会触发repeat start的信号，需要特备区分这种场景。Host读数据时，无法知道host需要的数据长度，每次只能通过polling slave interrupt的read next data的状态来判断是否可以发送数据。

这个SMBUS读操作目前支持nvme basic mi和vpd命令的传输。Basic mi数据结构长度是256bytes，VPD数据结构长度208bytes，在处理这些命令时需要注意，不同host存在读取比这个长度更大的数据，fw的处理时填PEC（dummy data + pec flag）后，继续获取数据则填0的数据给host。操作流程如下：

![smbus_slave_read](D:\scaleflux\03_summary\SMBUS\smbus_slave_read.png)

7.5 Slave写操作

SMBUS写序列为slave addr + write data，目前主要用于nvme normal mi的response接受。

![smbus_slave_write](D:\scaleflux\03_summary\SMBUS\smbus_slave_write.png)

7.6 Master读操作

这个场景在nvme mi协议中没有涉及。

7.7 Master写操作

NVME MI在处理完一笔数据后，会主动向host发送response，这个时候需要将SMBUS切换到master模式，host作为slave模式，接受数据。

在发送数据完成时，需要HW填充整帧数据的PEC，需注意，这个PEC数据无需软件填充一个dummy的数据，只需要在原有需要传输的LEN+1就可以，HW自动填写。

MASTER模式发送数据时，可以支持三类命令：manual cmd/auto cmd/sequence cmd。Manual cmd发送数据后需要手动发送stop cmd完成命令传输。Auto/sequence cmd在传输完成时HW会自动发送stop完成命令。在发送时，不能讲FIFO填充满，易触发wait time out而导致NAK命令结束，所以目前涉及是MAX_FIFO_LEN-2;

在数据发送结束时，fw需要将SMBUS切换成默认的slave模式。

![smbus_master_write](D:\scaleflux\03_summary\SMBUS\smbus_master_write.png)

8、 异常处理

为了防止SMBUS的中断状态的丢失，FW增加一个超时机制，超过100ms的处理就强制退出SMBUS传输，并且RESET SMBUS，并重新配置，这个操作可以让API2C恢复到一个idle状态，等待下一次正常传输。若连续发生100ms超时，每次间隔的时长为150ms，发送20次，则禁用SMBUS，等待下一次重启后使用。

9、 资源开销及SMP状态

若SMBUS按照100K(10us)的性能，传输一个byte需要消耗80us(8bits)+10us(NAK)=90us的时间，加上软件的消耗，至少100us的时间传输一个数据，一次传输最大可以支持250bytes，理想状态下最大耗时25ms。API2C本身具有预处理功能，和FW polling到IP的状态本身需要一定的时间，因此实际消耗的最大时间会比这个值更大。

SMBUS作为SLAVE传输时，中间不可以间断，SMBUS时序要求，若中断，SCL会被拉低等待数据传输，所以在设计时需要考虑SMBUS耗时场景：1.检测到PF中断时；2.FW检测数据传输超过100ms；3.idle处理时，不能和smbus处于同一个core;

目前nvme mi的task在admin core，若优化到单独的core，可考虑不用polling ip状态，采用CPU（IRQ_SMBUS0/IRQ_SMBUS1）中断上报的方式及时处理SMBUS的命令。

除了SMP_STATE_PF需要ABORT SMBUS的传输，其他SMP状态不影响