VPD:VItal Product Data

```
使用这个定义在IPMI平台的数据格式来管理FRU信息存储定义，VPD数据存储在FRU信息设备中
VPD是能通过支持带外机制或带内隧道机制的来访问任何端口。
如果NVMe存储设备具有SMBus/I2C端口，则使用访问机制可以访问VPD通过IPMI平台管理FRU信息存储定义中定义的I2C
SMBUS地址解析协议(ARP)
```

FRU:Field-Replaceable Unit

```
可以移除、更换系统中的某个物理部件、设备或者组件，而不必更换包括它的整个系统。
那么这个物理部件、设备或者组件，就叫做FRU
```

FRU信息设备用于保存VPD

管理组件传输协议（MCTP）

out-of-band 带外

```
带外管理是对独立于操作系统控制的硬件资源和组件进行操作的管理系统
```

物理层

```
PCI Express被用来作为带外机制和带内隧道的物理层
对于带外机制，Nvme存储设备或者机柜中，一个PCIe Port可能是一个管理端口(EndPoint)
对于带内隧道，主机软件通过PCI Express将Nvme Admin命令发到Nvme Admin Queue

SMBUS/I2C物理层仅仅适用于带外机制
```

消息传输

```
NVMe MI消息用于带外和带内隧道消息的通信
在带外机制中，NVMe MI消息由一个或多个MCTP的有效载荷组成小包。NVMe MI消息的最大大小为4224字节（即4KiB+128字节）。长度大于的NVMe MI消息4224字节被视为无效的NVMe MI消息。
在带内隧道机制中，NVMe MI消息不会拆分为MCTP数据包，最大NVMe MI消息大小等于最大数据传输大小

数据传输的最小数据单元是MCTP包，一个或多个数据包组成一个MCTP消息，也就是NVMe-MI消息
一个NVMe-MI消息可以切分成多个MCTP包
一旦组装成了完整的NVMe-MI消息，则需要进行消息完整性校验
```

