```c++
#ifdef OPN
    #if OPN == C3SIMB58200G // bl3_asic_simu_wout_build_b58.sh
        strncpy(tmp_opn, "C3SIMB58200G", sizeof("C3SIMB58200G"));
    #elif OPN == C3SIMB584T // quince_x86-64_wout_build_b58R.sh
        strncpy(tmp_opn, "C3SIMB584T", sizeof("C3SIMB584T"));
    #elif OPN == C3SIMB58800G // quince_x86-64_wout_build_b58R_3plane.sh
        strncpy(tmp_opn, "C3SIMB58800G", sizeof("C3SIMB58800G"));
    #elif OPN == CSDU4SBC38A0 // quince_bl3_fpga_wout_build_b58.sh
        strncpy(tmp_opn, "CSDU4SBC38A0", sizeof("CSDU4SBC38A0"));
    #elif OPN == C3B58R4T
        strncpy(tmp_opn, "C3B58R4T", sizeof("C3B58R4T"));
    #elif OPN == CSDU4SBC76A0 // quince_bl3_fpga_wout_build_b58_3plane.sh
        strncpy(tmp_opn, "CSDU4SBC76A0", sizeof("CSDU4SBC76A0"));
    #elif OPN == CSDU4SBB38A0
        strncpy(tmp_opn, "CSDU4SBB38A0", sizeof("CSDU4SBB38A0"));
    #elif OPN == CSDU4SBA38A0
        strncpy(tmp_opn, "CSDU4SBA38A0", sizeof("CSDU4SBA38A0"));
    #endif
```

```c++
    else if (strncmp(_opn_str, "C3SIMB584T", sizeof("C3SIMB584T")) == 0) {
        _nand_type = SIM_NAND_MODEL_B58R;
        _op_val_cfg = OP_VAL_14;
        _op_cfg = OP_14;
        _ddr_size = DDR_8GB;

        _cfg_nand = &cfg_4t_b58;
    }
```

```c++
cfg_nand_t cfg_4t_b58 = {
    .raw_cap = CAPACITY_TiB(4),

    .pba_block_bits = 10,
    .pba_page_bits = 10,

#if (__ZEBU__ == 1)
    .pba_ce_lun_bits = 1,
    .pba_chan_bits = 4,
#else
    .pba_ce_lun_bits = 1,
    .pba_chan_bits = 4,
#endif

    .pba_plane_bits = 3,
    .pba_page_type_bits = 2,
    .pba_offset_bits = 2,
    .plane_group = 1,

    .block_num = 567,
    .page_num = 936,
    .real_page_num = 936,
    .nand_page_num = 936,
    .slc_page_num = 928,
    .nand_small_page_num = 2784,
    .page_type_num = 3,

    .physical_plane_num = 6,
#if (__ZEBU__ == 1)
    .chan_num = 16,
    .ce_num = 2,
    .lun_num = 1,
#else
    .chan_num = 16,
    .ce_num = 2,
    .lun_num = 1,
#endif

    .single_plane_page_size = 16384ULL,
    .target_endurance_pe = 5000,

    .nand_addr_lun_bits = 2,
    .nand_addr_block_bits = 10,
    .nand_addr_plane_bits = 3,
    .nand_addr_page_bits = 10,
};

```

```
lun_width=1: lun_id = die_id[0]; ce_id=die_id[3:1];

这个就是说，这个ch上有8个ce，每个ce上有2个lun，die index in ch就是0~15，总共占了5个bit中的4个

lun_width=2: lun_id = die_id[1:0]; ce_id=die_id[4:2];

这个就是说，这个ch上有8个ce，每个ce上有4个lun，die index in ch就是0~31，总共占了5个bit中的5个

lun_width=3: lun_id = die_id[2:0]; ce_id=die_id[4:3];

那这个就是说，这个ch上有4个ce，每个ce上有8个lun，die index in ch就是0~31，总共占了5个bit中的5个

4T的时候，lun_width=1，CFG_CHAN_NUM=16, CFG_CE_NUM=8, CFG_LUN_NUM=2
8T的时候，lun_width=2, CFG_CHAN_NUM=16, CFG_CE_NUM=8, CFG_LUN_NUM=4
16T的时候，lun_width=3, CFG_CHAN_NUM=16, CFG_CE_NUM=4, CFG_LUN_NUM=8

10111,
lun_width=1: lun_id = 1; ce_id=3(011);
lun_width=2: lun_id = 3(11); ce_id=5(101);
lun_width=3: lun_id = 7(111); ce_id=2(10);

```

```
ce = GET_CE_FROM_NAND_ADDR(nand_pba.die_id_low, nand_pba.die_id_high, CFG_LUN_WIDTH);
lun = GET_LUN_FROM_NAND_ADDR(nand_pba.die_id_low, nand_pba.die_id_high, CFG_LUN_WIDTH);
tmp_cur_pba.die_id_high = GET_DIE_ID_HIGH(lun, ce, CFG_LUN_WIDTH);
tmp_cur_pba.die_id_low = GET_DIE_ID_LOW(lun, ce, CFG_LUN_WIDTH);
```

