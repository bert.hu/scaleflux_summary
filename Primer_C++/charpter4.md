数组初始化规则

1、只有在定义数组的时候才能使用初始化，此后就不能使用了

```
int cards[4] = {3, 6, 8, 10}; // ok
cards[4] = = {3, 6, 8, 10}; // not allowed
```

2、不能将一个数组赋给另一个数组

```
int cards[4] = {3, 6, 8, 10};
int hands
hands = cards
```

3、如果只是对数组的一部分进行初始化，则编译器将其他元素设置为0，那么，如果显式将第一个元素初始化为0，那么编译器会将其他元素初始化为0，这样数组中所有元素都将初始化为0

```
long totals[100] = {0};
```

4、如果初始化的大括号内不包含任何东西，那么会将所有元素也都初始化为0

```
unsigned int counts[10] = {};
```

5、列表初始化禁止缩窄转换 

```
long a[] = {25, 92, 3.0}; // 将浮点数转换为整型是缩窄操作，即使浮点数小数点后面为0
char b[] = {'h', 'i', 1122011, '\0'}; // 1122011超出了char的取值范围，因此这个也是缩窄操作
char c[] = {'h', 'i', 112, '\0'}; // ok
```

字符串

```
C++处理的字符串有两种：
1、C-风格字符串：以空字符结尾，空字符被写作\0,其ASCII码是0，用来标记字符串的结尾
2、基于string类库的方法
```

```
char dog[8] = {'b', 'e', 'c', 's', 'f', ' ', 'i', 'p'}; // not a string，没有\0来标记结尾
char dog[8] = {'b', 'e', 'c', 's', 'f', ' ', 'i', '\0'}; // This is a string
```

数组初始化为字符串用上面的方法很冗长，有一种更好的将字符数组初始化为字符串的方法

```
char birds[11] = "hello,chip";
char birds[] = "hello,chip";
```

误区

```
注意字符串常量：使用双引号 不能与字符常量：使用单引号 互换
char a = 'S'; // ok
char a = "S"; // “S"表示两个字符(S和\0)组成的字符串，这个赋值相当于将字符串所在的内存地址赋给a，由于地址在C++中是一个独立的类型，因此会编译出错
```

拼接字符串常量

```
两个用双引号括起来的字符串可以合并为一个
cout << "hello, " "world!\n"; // 拼接时第二个字符串会紧跟在第一个字符串的最后一个非\0字符后面，第一个字符串中的\0会被第二个字符串中的第一个字符取代
```

sizeof和strlen

```
sizeof运算符值的是整个数组的长度，strlen()只计算可见的字符，不会算上\0这个空字符
const int Size = 15
char name1[Size];
char name2[Size] = "C++";
那么
sizeof(name2) = 15
strlen(name2) = 3
```

cin使用空白（空格、制表符和换行符）来确定字符串的结束位置

```
char name[20];
char dessert[20];
cout << "Enter your name:\n";
cin >> name;
cout << "Enter your favorite dessert:\n";
cin >> dessert;
cout << "I have some delicious " << dessert;
cout << " for you, " << name << ".\n";

运行情况
Enter your name:
Hu Hai Tao
Enter your favorite dessert:
I have some delicious Hai for you, Hu.
```

另一个问题是，输入字符串比数组长度要长，这样会出现30个字符的字符串放到20个字符的数组里面

讨论：cin的缺陷--每次只能读取一个单词的问题，那么如何采用面向行而不是面向单词的方法来读取字符串呢？

```
istream中的类提供了一些面向行的类成员函数：getline()和get()
相同点：getline()和get()都是读取一行输入，直到到达换行符
区别：getline()将丢弃换行符，用空字符来替换掉换行符，而get()则将换行符保留在输入序列中

cin.getline(name, 20);
第一个参数name用于存储输入行的数组的名称
第二个参数是要读取的字符数，如果参数为20，则函数最多读取19个字符，余下的空间用于存储自动在结尾处添加的空字符
getline()在读取指定数目的字符或遇到换行符则停止读取

cin.get(name, 20);
cin.get(dessert, 20);
由于第一次调用后，换行符将留在输入队列中，因此第二次调用get的时候，看到的第一个字符就是换行符，因此get会认为已经到达行尾，而没有发现任何可读的内容，如果不借助帮助，get()将不能跨过该换行符
幸运的是，cin.get()不带任何参数的可读取下一个字符，即使是换行符，因此可以用来处理换行符，为读取下一行输入做好准备
```

```
cin.get(name, 20);
cin.get(); // 读取留在输入队列中的换行符，为读取下一行输入做准备
cin.get(dessert, 20);

或者
cin.get(name, 20).get();
这么做，是因为cin.get(name, 20)将返回一个cin对象，该对象随后被用来调用get()函数

于是，还可以有
cin.getline(name, 20).getline(dessert, 20);
```

get()和getline()

```
既然getline()这么方便，为什么还需要用get呢
因为get()使得输入更仔细一些，比如如何直到停止读取的原因是由于已经读取了整行，而不是由于数组已经被填满？
方法：
再读取下一个输入字符，如果下一个是换行符，说明已经读取了整行，否则，说明该行中还有其他输入
所以getline()使用起来方便，但get()使得检查错误更简单一些
```

需要注意的是，cin输入之后，会将回车键生成的换行符保留在输入队列中，这会导致getline()读取为空行，将一个字符串赋给数组

还是可以用cin.get()，在读取数组之前先读取并丢弃换行符

也可以用cin >> year返回cin对象，再将调用拼接起来

```
(cin >> year).get();
```

string类

```
string类位于名字空间std中
类设计可以让程序自动处理string的大小
string str1；
cin >> str1;
因此，与数组相比，使用string对象更方便，也更安全
从理论上来说，可以将char数组视为一组用于存储一个字符串的char存储单元，而string类变量是一个表示字符串的实体
string也可以使用C-风格字符串，用列表初始化
string a = {"This is a string!"};
```

不能将一个数组赋给一个数组，但可以将一个string对象赋给一个string对象

string类还简化了字符串合并操作，可使用运算符+将两个string对象he'bing合并起来，还可使用+=采用追加模式

```
string str1 = {”Hello， “};
string str2 = {”World!\n“};
string str3;
str3 = str1 + str2; 
str1 += str2;
```

误区

```
char a[20];
strlen(a) = 27
这里为什么数组a中的字符串长度为27，还大于数组长度20，因为，未初始化的数组的内容是未定义的,而且，函数strlen()从数组的第一个元素开始计算字节数，直到遇到空字符，而对于未被初始化的数据，第一个空字符出现位置的地方是随机的，所以会有strlen算出来的字符串长度和数组长度不同的情况
```

结构体

```
C++允许再声明结构变量的时候省略关键字struct
struct Sample
{
    char name[10];
    float volume;
    double price;
}
struct Sample a; // C语言的声明结构体变量
Sample a; // C++的声明结构体变量

C++不提倡外部变量(全局变量)的使用，但提倡外部结构声明
```

结构体初始化方式：允许将一个结构体变量赋值给另一个结构体变量

```
Sample a = 
{
    "HuHaiTao",
    1.88,
    29.99
};
也可以放在同一行
这里同样也不允许缩窄转换
```

如果结构体类型没有赋予名称，则无法使用它的名称来创建变量

结构中的位字段

```
struct Register
{
    unsigned int SN : 4; // 4bit for SN value
    unsigned int : 4;    // 4bit unused
    bool goodIn : 1;     // 1bit
    bool goodReg : 1;    // 1bit
};
```

共用体union

```
共用体的长度为其最大成员的长度
优势：
当数据项不同时使用时，可以用于节省内存空间
```

指针

```
指针声明必须指定指针指向的数据类型
在C++中创建指针，计算机将分配用来存储地址的内存，但不会分配用来存储指针所指向的数据的内存
那么：
long *p;
*p = 1000; // 这里将1000的地址赋值给了p，但1000的地址是哪儿呢？不知道
```

在C++中，值为0的指针被称为空指针

使用delete释放内存

```
int* p = new int;
...
delete p; // 释放p指向的内存，但不会删除指针p本身，可以将p重新指向另一个新分配的内存块
```

不要重复释放已经释放的内存块，因为这么做的结果是不确定的，另外，不能使用delete来释放声明变量所获得的内存

注意，对空指针使用delete是安全的

使用new来创建动态数组

```
int* p = new int [10];
delete [] p; // 释放动态数组的内存
```

有两种方式来获取数组的地址

```
double wags[3] = {100.0, 1000.0, 10000.0};
double *p1 = wags;
double *p2 = &wags[0];
```

数组的地址

``` 
short tell[10];
cout << tell << endl;
cout << &tell << endl;
尽管两者的地址是相同的，但从意义上说，&tell[0]表示的是一个2字节内存块的地址，而&tell表示的是一个20字节内存块的地址，因此，tell+1是将地址加2字节，而&tell+1是将地址加20字节
&tell的指针类型是 short(*)[10]，即指向的是一个包含了10个short元素的数组的指针
声明则是
short (*p)[10] = &tell;
```

数组的动态联编和静态联编

```
使用声明来创建数组的时候，采用的是静态联编，这时候数组的长度在编译的时候确定
int array[10];
使用new创建动态数组的时候，采用的是动态联编，这时候在运行时为数组分配空间，长度也在运行时确定
```

strcpy(dest_array, src_string);

如果字符串的长度比dest_array的大，那么strcpy将字符串中剩余的部分复制到数组后面的内存字节中，这可能会覆盖程序正在使用的其他内存，要避免这个问题，可以使用strncpy，但这个函数不会添加空字符，所以在使用strncpy以后，应该手动加上空字符，以标记该字符串的结尾。

模板类vector

```
类似于string类，也是动态数组的一种，可以在运行阶段设置vector对象的长度
因为它也是用new和delete来创建和管理内存的
vector包含在名字空间std中，因此可以使用using namespace std来使用vector
```

模板类array（C++11）

```
vector类相比数组要更强大，但效率稍低，如果需要的是长度固定的数组，使用数组更好，但并不安全和方便
因此C++11新增了模板类array，也在名字空间中
与数组一样，array的对象长度也是固定的，也是使用的栈（静态内存分配），而不是堆（自由存储区），因此效率和数组相同，但更方便和安全
#include <array>
using namespace std;
array<int, 5> a;
array<double, 4> b = {1.2, 1.3, 1.4, 1.5}; 
```

