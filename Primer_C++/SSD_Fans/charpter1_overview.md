### SSD综述

```
SSD:Solid State Drive,固态硬盘，是一种以半导体闪存(NAND Flash)作为介质的存储设备，用纯电子电路实现，没有任何机械设备。
```



#### 1、引子

```
开机速度快
以后还可以使用更快、更可靠、更省电的新介质来替代NAND Flash，比如3D XPoint、MRAM等
SSD使用固态电子存储芯片阵列制成的硬盘，主要部件是控制器和存储芯片
主体就是一个PCB(电源芯片，电阻，电容)，接口(SATA，SAS，PCIe等)，主控制器和NAND Flash
SSD内部运行固件Firmware，FW，负责调度数据从接口端到介质端的读写，还包括一些介质寿命和可靠性管理调度的算法

存储介质按照物理材料的不同可以分为三类：
光学存储介质：DVD、VCD等
磁性存储介质：HDD
半导体存储介质：SSD

半导体存储介质分为非易失性和易失性，易失性的是SRAM和DRAM，非易失性的是Flash，有NOR Flash和NAND Flash，而NAND Flash又分2D NAND和3D NAND
```



#### 2、SSD vs HDD

```
SSD相比于HDD的优点：
1、性能好，主要是读写速度差异，尤其是随机读写性能(速度和延时)的差异
性能测试工具：连续读写吞吐量(Throughput)工具和随机读写IOPS工具，包括IoMeter和FIO等测试工具
用户体验的测试工具：PCMark Vantage，以应用运行和加载时间作为考察对象

2、功耗低
HDD为6~8W，SATA SSD是5W，待机功耗SSD可以降低到mW级别
功耗类别：峰值功耗Peak Power， 读写功耗Active Power，空闲功耗Idle Power，省电功耗Sleep/DevSleep Power
一般是16K闪存页(Page)的读写功耗(闪存)>主控功耗(20%)

3、抗震防摔因为电子和PCB结构，PCB加半导体芯片，跌落时不存在机械损伤问题，因此更加抗震和可靠
4、无噪声
5、外观小且多样
HDD只有3.5寸和2.5寸
SSD除了上述还有可以贴放在主板上的更小的M.2形式，以及小到芯片级的BGA SSD(16mm * 30mm)

```



#### 3、固态存储及SSD技术发展史

```
1、昂贵的RAM SSD时代
2、机械硬盘HDD称霸世界
3、闪存--源于华人科学家的发明(浮栅晶体管)
4、闪存SSD异军突起
5、2006年SSD进入笔记本
6、2007革命之年
7、2008速度大战爆发
8、2009SSD容量赶上HDD
9、2010SSD市场开始繁荣
10、2011~2012上市、收购群雄并起
11、2013PCIe SSD进入消费者市场
12、2014SSD软件平台重构企业级存储
13、2015年3D XPoint
14、2016年NVDIMM开始供货，关键怎么标准化
```



#### 4、SSD基本工作原理

```
主机PC(host)对SSD发出请求，文件系统将读写请求经驱动转化为相应符合协议的读写和其他命令，SSD收到标准命令后执行操作
SSD自己又分为三部分：
1、前端接口和相关的协议模块
2、中间的FTL层
3、后端的闪存通信模块

流程是：主机通过接口发写命令给SSD，SSD首先将主机要写入的数据缓存到SSD内部的RAM中，然后FTL为每个写入的逻辑数据块分配一个闪存地址，当数据凑到一定数量以后，FTL则发写请求给后端，后端根据请求，把RAM缓存中的数据写到NAND上对应的地方
在这个过程中需要注意的是，闪存NAND不能覆盖写，必须擦除才能写入，所以主机发的数据不是写在NAND上固定的位置，而是需要FTL来分配写入的地方，也就是需要一个逻辑数据块地址到NAND物理地址的转换/映射

由于前端接口协议都是标准化的，后端和闪存的接口及操作也是标准化的，所以SSD在前端协议和闪存确定下来以后，差异化就只有FTL算法，所以FTL算法决定了性能、可靠性、功耗等SSD的核心参数
```



#### 5、SSD产品核心参数

```
容量配置Capacity:标称的数据是10进制的,以字节Bytes为单位,以二进制为单位的容量成为裸容量,以十进制为单  位的容量成为用户容量,裸容量比用户容量多出7%,这里是GB级别,TB的话会更多,这7%的多余空间也可以转换成为OP概念(Over Provision),即OP = (裸容量 - 用户容量)/用户容量

介质信息Component:SLC/MLC/TLC, 即单个存储单元存储1/2/3bit的数据
外观尺寸FormFactor:3.5寸/2.5寸/1.8寸/M.2/PCIe card/mSATA/u.2等
重量Weight
环境温度Temperature
震动可靠性Shock and Vibration
认证Certifications
加密Encryption
性能指标：连续读写带宽、随机读写IOPS，时延Lantency，最大时延Quality of Service
数据可靠性和寿命：Reliability，Endurance
功耗：Power Management，Active Power and Idle Power
兼容性：Compliance等
```

```
性能剖析
一般包括:
1.IOPS(input output operation per second),反映的是随机读写性能
2.吞吐量(throughput),单位MB/s,反映的是顺序读写性能
3.Response Time/Latency,响应时间/时延.单位ms或us

访问模式:
1.Random/Sequential:随机和连续数据命令请求,Random指的是前后两条命令的LBA地址是不连续的,连续的地址称为Sequential
2.Block Size:块大小,即单条命令传输的数据的大小,性能测试从4KB~512KB不等,一般来说Random测试用小数据块如4KB,Sequential测试用大块数据,如512KB
3.Read/Write Ratio:读写命令混合的比例

常见的测试模式:
1) 顺序读测试:LBA连续读,块大小256KB,512KB等大尺寸数据块,读写比例为100%:0
2) 随机写测试:LBA不连续写,块大小4KB,读写比列为0:100%
3) 随机混合读写:LBA不连续的读写混合测试,块大小一般4KB,读写保持一定比例

时延指标
1) 平均时延:整个测试过程中所有命令响应时间总和除以命令的总数,反映的是SSD总体平均时延性能
2) 最大时延:在测试周期内所有命令中响应时间最长的那一笔,反映的是用户体验

服务质量指标
Quality of Service, Qos,表达的是时延置信级,在测试规定时间内使用99%~99.999%的百分比的命令中的最大时延Max Lantency,也就是最慢的那条命令的响应时间,整体上,Qos时延分布越靠左越好,即时延越小越好

对企业级用户来说,客户更关注的是稳态性能,即满盘性能,因为满盘的时候写会触发垃圾回收,导致写性能下降

寿命剖析:
衡量SSD寿命的两个指标:
1.DWPD(Drive Writes Per Day):即SSD保质期内,用户每天可以把盘写满多少次,可以根据不同的应用场景来选择所需求的DWPD参数的SSD,因为DWPD越大的单盘价格越高
2.TBW(Terabytes Written),在SSD生命周期内可以写入的总的字节数,总写入量TBW=Capacity(单盘容量)*PE Cycles(NAND写擦除寿命) / WA(写放大)
两者的关系:
DWPD=TBW/(Years * 365 * Capacity)

数据可靠性剖析
1) UBER:Uncorrectable Bit Error Rate, 不可修复的错误比特率
2) RBER:Raw Bit Error Rate,原始错误比特率
3) MTBF:Mean Time Between Failure, 平均故障间隔时间

UBER是一种数据损坏率的衡量标准,表示在应用了任意特定的错误纠正机制后仍然产生每比特读取的数据错误数量占总读取数量的比例
虽然SSD主控会采用纠错码ECC的方式,还有RAID等来修正错误数据,但在某种条件下仍然有纠不回来的可能,UBER就表示了这种可能性的概率,也反映了闪存的质量,而且UBER会随着使用寿命PE Cycle的增加而增加,而且Upper Page的RBER比Lower Page的RBER要高不少

功耗及其他剖析:
待完善
```

```
SSD上电过程:
主机BIOS开始自检,主机中BIOS作为第一层软件和SSD进行交互:
第一步,和SSD发生链接,SATA和PCIe走不同的底层链路链接,协商(negotiate)到正确的速度上,自此主机端和SSD连接成功
第二步,主机发出识别盘的命令来读取盘的基本信息,然后BIOS会验证信息格式和数据的正确性
第三步,BIOS读取盘的其他信息,如SMART,直到BIOS找到硬盘上的主引导记录MBR,并加载MBR
第四步,MBR开始读取硬盘分区表DPT,找到活动分区中的分区引导记录PBR,并将控制权交给PBR
最后,SSD通过数据读写功能来完成最后的OS加载
至此,标志BIOS和OS在SSD上电加载成功
```



#### 6、接口形态

待完善