#### 1.闪存物理结构

```
固态硬盘SSD的工作原理很多都是基于闪存特性的
1.闪存在写之前必须擦除,不能覆盖写-->需要垃圾回收Garbage Collection
2.闪存每个块block擦写次数达到一定值以后,要么变为坏块,要么存储在上面的数据不可靠-->需要磨损均衡WL

SLC(single Level Cell)
MLC(Multiple Level Cell)
TLC(Triple Level Cell):一个存储单元存储3 bit数据的闪存

擦除之后,闪存读出来的值是1
充过电以后,闪存读出来的值是0

存储单元划分越多,控制进入浮栅极的电子数目就要越精细,所以写耗费的时间就越长,所以在性能上,TLC不如MLC,MLC不如SLC

每个Block有多个Page
对SLC来说,这个Page只有1个,那么wordline上面就只有1个存储单元,也就是1个bitline
对MLC来说,这个Page有2个,那么wordline上面就有2个存储单元,也就是2个bitline
对TLC来说,这个Page有3个,那么wordline上面就有3个存储单元,也就是3个bitline
所以可以认为bitline就是一个小page，一个wordline就是一个大page
当SLC的时候，一个bitline才等于一个wordline

因为Die/LUN是接收和执行闪存命令的最小基本单元,所以LUN0和LUN1可以同时接收和执行不同的命令,比如一个读一个写
但在一个LUN里面,一次只能执行一个命令,只能对所有的Page进行读,或者写
一个LUN里面分为若干个Plane,每个Plane都有自己独立的Cache Register和Page Register,大小等于一个Page的大小
那么为什么需要两个寄存器Cache和Page?
主要目的是优化闪存的访问速度,可以将数据在闪存总线传输的时间隐藏在读闪存介质的时间里面或前一个Page写的时间里面
我们常说的闪存读写时间,指的是一个Page的数据从Page Register当中写入NAND Flash的时间,读取时间也是指的从NAND Flash读取一个Page的数据到Page Register的时间

闪存一般都支持Dual-Plane的操作，这种操作就是：主控先把数据写到第一个Plane的Cache Register中，但数据保持在里面，并不是立即去写入闪存介质，而是等主控把这个LUN的其他的plane的数据传输到Cache Register中，再一起将这个LUN的所有Plane的Cache Register的数据写入到闪存介质。
假设写入一个Page到NAND的时间为1.5ms，传输一个Cache Register的时间为50us，
那么如果按照Single-Plane的操作，写4个Plane的消耗的时间是(1.5ms + 0.5us)*4
如果按照Multi-Plane的操作，这个时间为(0.5us * 4 + 1.5ms),大大减少了写入时间

闪存从二维增加到三维，大大降低了单元之间互相干扰的幅度
克服了二维平面单元结构的技术瓶颈
CT：Charge Trap，闪存不止有Floating Gate，还有Charge Trap，CT与浮栅最大的不同在于存储电荷的元素不同，后者是使用的导体来存储电荷，而前者是用高电荷捕捉（Trap）密度的绝缘材料（一般是氮化硅）来存储电荷，CT的这种绝缘材料就像是布置了很多陷阱，让电子一旦身处其中，就难以逃脱，而浮栅是导体材料，电子可以在里面自由移动
所以CT不会遇到浮栅极那种时间长了氧化层老化导致电子溢出的问题，隧道氧化层磨损更慢
而且工艺上更容易实现

3D XPoint
闪存用在固态硬盘上，使用的是SATA接口，想要更快的话，就用SAS接口，甚至用PCIe接口
但其实最快的还是DRAM，可以支持字节级别的访问，但是DRAM有着易失性，上面的数据一旦不通电，马上就丢失了

一个通道的PCIe的SSD在理论上最高只能到1GB/s（PCIe 3.0）
一般SSD用4个通道，就是4GB/s
而SATA理论上最高是600MB/s
尽管PCIe SSD的速度是机械硬盘的20多倍以上，但DDR4却是PCIe SSD速度的十几倍
那么是否有一种存储器，既有着SSD的非易失性，又有着DRAM的速度？
3D XPoint

```



#### 2.闪存实战指南

```
闪存命令集
主控是通过闪存命令集与闪存进行通信的，这些命令是基于ONFI2.3协议等规定的
比如读数据，就是发送00h~30h
不同闪存支持的命令有差异，具体应遵守闪存芯片手册与闪存通信

闪存不止有ONFI协议，还有Toggle协议
ONFI：Open NAND Flash Interface
三星和东芝联合搞了个Toggle NAND协议

```



#### 3.闪存特性

```
1、闪存坏块
出厂坏块--FBB
新增坏块--GBB
所以需要坏块管理机制，BBM（bad block Manager）

2、读干扰
Read Disturb
施加正电压的边界处，容易发生电子被吸入到浮栅极，形成轻微写，从而导致1->0的比特翻转
但Read Disturb不是永久性的损伤，重新擦除闪存块就能正常使用
读干扰影响的是闪存块种其他的闪存页

3、写干扰
Program Disturb
当写一个闪存页的时候，数据是0和1混合的，由于擦除过的闪存块所有的存储单元初始值是1，只有写0的时候才是真正需要操作。
这种轻微写导致的比特翻转不是永久性的损伤，重新擦除闪存块就能正常使用

4、存储单元之间的耦合影响
存储单元之间存在耦合电容，会使存储单元内的电荷发生意外变化，最终导致数据读取出错

5、电荷泄露
长期不使用，则存储在闪存存储单元的电荷，会发生电荷泄露
这同样不是永久性的损伤，重新擦除闪存块就能正常使用

6、寿命
怎么解决寿命问题？
6.1 wear leveling
6.2 降低写放大，让磨损速度变慢
6.2 用更好的纠错算法，容许的出错率更高，可以延长SSD使用寿命


```



#### 4.闪存数据完整性

