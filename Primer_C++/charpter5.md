基于范围的for循环（C++11）

```
基于范围range-based的for循环，简化了一种常见的循环任务：对数组（或容器类，如vector和array）的每个元素执行相同的操作
```

```
double prices[5] = {4.99, 3.99, 2.99, 1.99, 5.99};
for (double x: prices) {
    cout << x << std::endl;
}
```

如果要修改数组中的元素，则

```
for (double &x : prices) {
    x = x * 0.8;
}
```

这种循环主要用于各种模板容器类