基础

```
C++对大小写敏感，区分大小写
C++的头文件没有扩展名，但可以兼容老式C的头文件.h,如果C头文件被转换为C++头文件，则头文件会重新命名，如math.h重命名为cmath
```

标准格式cpp

```c++
#include <iostream>
int main()
{
    using namespace std;
    cout << "Hello Word!";
    cout << endl;
    cout << "Byebye!" << endl;
    return 0;
}
```

注意，有些窗口环境在独立的窗口中运行程序，并在程序运行完毕以后自动关闭窗口，如果想要窗口一直打开，可以在return之前添加如下语句：

```c++
cin.get();
```

如果程序要使用C++输入或输出工具，需要以下两行代码

```
#include <iostream>
using namespace std; // 用于简化程序
```

名称空间

```
用于避免同名函数在不同文件中出现冲突
名称空间可以让厂商能将其产品封装到一个名称空间的单元中，例如Microflop的名称空间中有个wanda()函数
则全称为Microflop::wanda();
所以用using namespace std可以使得这个std这个名称空间中定义的名称都可用，而不必使用std::的前缀
但更好的方法是：
using std::cout;
using std::endl;
using std::cin;
这样便能使用cin和cout，而不需要加上std::的前缀
```

