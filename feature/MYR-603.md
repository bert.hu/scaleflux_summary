```
lpa bit63 enable
PF之后，IO的数据没有写满一个b2n，则hw会padding一段无效的数据0并写到NAND，这段无效的数据的header的lpa的bit 63需要置为1，表示这段数据是无效的数据
```

```c++
// USER TYPE
#define LP_USER_TYPE_OFFSET           (CFG_LPA_USER_LPA_BITS + CFG_LPA_RSVD_BITS + CFG_LPA_BAD_SECTOR_BITS)
#define LP_USER_TYPE_MASK             (N_BIT_MASK(CFG_LPA_USER_TYPE_BITS))
#define LP_GET_USER_TYPE(lpa)         (((lpa) >> LP_USER_TYPE_OFFSET) & LP_USER_TYPE_MASK)
#define CLEAR_USER_TYPE_FROM_L2P(lpa) ((lpa) &= (~((u64)LP_USER_TYPE_MASK << LP_USER_TYPE_OFFSET)))
#define LP_SET_USER_TYPE(lpa, user_type)                               \
    (((((u64)(user_type)) & LP_USER_TYPE_MASK) << LP_USER_TYPE_OFFSET) \
     | (~((u64)LP_USER_TYPE_MASK << LP_USER_TYPE_OFFSET) & (lpa)))

// BAD SECTOR
#define LP_BAD_SECTOR_OFFSET               (48)
#define LP_BAD_SECTOR_MASK                 (0xFF)
#define LP_GET_BAD_SECTOR(lpa)             (((lpa) >> LP_BAD_SECTOR_OFFSET) & LP_BAD_SECTOR_MASK)
#define GET_CLEAR_BAD_SECTOR_FROM_L2P(lpa) ((lpa) & (~((u64)LP_BAD_SECTOR_MASK << LP_BAD_SECTOR_OFFSET)))
#define LP_SET_BAD_SECTOR(lpa, bad_sector)                                \
    (((((u64)(bad_sector)) & LP_BAD_SECTOR_MASK) << LP_BAD_SECTOR_OFFSET) \
     | (~((u64)LP_BAD_SECTOR_MASK << LP_BAD_SECTOR_OFFSET) & (lpa)))

// real LPA
#define LP_LPA_MASK          (N_BIT_MASK(CFG_LPA_USER_LPA_BITS))
#define LP_GET_REAL_LPA(lpa) ((lpa)&LP_LPA_MASK)
```

```c++
union lpa_t
{
    struct
    {
        u64 user_lpa : CFG_LPA_USER_LPA_BITS;  // 36, 0-35
        u64 rsvd1 : CFG_LPA_RSVD_BITS; // 12, 36-47
        u64 bad_sector : CFG_LPA_BAD_SECTOR_BITS; // 8, 48-55
        u64 user_type : CFG_LPA_USER_TYPE_BITS; // 5, 56-60
        u64 rsvd2 : 1; // 61
        u64 gts : 1; // 62
        u64 pf : 1; // 63
    };
    u64 all;
};
```

```c++
// BIT 63 is dedicated used for PFerr
// BIT 62 is dedicated used for GTS
// BIT 56 ~ BIT 60 used for LP USER TYPE
//| 63   | 62  |  61  |  60 -56   |    55 - 48 |   47 - 36 |  35 - 0  |
//|PF ER | GTS | RSVD | USER TYPE | BAD SECTOR |    RSVD   | USER LPA |
enum DATA_USER_TYPE_E
{
    USER_HOST = 0,
    USER_ML,
    USER_FT,
    USER_L2P_META,
    USER_RFS,
    USER_PAD,
    USER_PATCH,
    USER_EVT_LOG,
    USER_BLK_MGR_LBA,
    USER_L1MAP_LBA,
    USER_TRIM_LBA,
    USER_SHARED_LBA,
    USER_MPAGE_SUMMARY_LBA,
    USER_MAPLOG_LBA,
    USER_FOOTER_HOT_LBA,
    USER_FOOTER_GC_LBA,
    USER_FOOTER_WL_LBA,
    USER_FOOTER_EH_LBA,
    USER_EVT_LOG_LBA,
    USER_SYS_BLK_MGR_LBA,
    USER_FW_STATISTICS_LBA,
    USER_GSD_METADATA_LBA,
};
```

使用的地方

```c++
hdr  = (header_abs_s *)(hdr_buffer + hdr_offset);
        while ((hdr->ccp_hdr.signature == HW_SIGNATURE) && (tot_count < 1024)) {
            pba_hdr.comp_len = hdr->ccp_hdr.comp_size_4B;// record the data pba, need comp_size_4B

            if (hdr->lpa == LP_HDR_GTS_SIGN) {
                pba_hdr = pba_append_by_comp_size(pba_hdr, hdr->ccp_hdr.comp_size_4B);
                hdr_offset += sizeof(header_abs_s);
                hdr = (header_abs_s *)(hdr_buffer + hdr_offset);
                continue;
            }

            if (LP_GET_USER_TYPE(hdr->lpa) == USER_ML) {
                pba_hdr = pba_append_by_comp_size(pba_hdr, hdr->ccp_hdr.comp_size_4B);
                hdr_offset += sizeof(header_abs_s);
                hdr = (header_abs_s *)(hdr_buffer + hdr_offset);
                continue;
            }

            if (LP_GET_USER_TYPE(hdr->lpa) == USER_PAD) {
                // g_pu_mgr.pblk_mgr[0].blk_update_space(pba_hdr, hdr->lpa);
                pba_hdr = pba_append_by_comp_size(pba_hdr, hdr->ccp_hdr.comp_size_4B);
                hdr_offset += sizeof(header_abs_s);
                hdr = (header_abs_s *)(hdr_buffer + hdr_offset);
                continue;
            }

            u64 temp_lpa = hdr->lpa;
            temp_lpa     = GET_CLEAR_BAD_SECTOR_FROM_L2P(temp_lpa);

            if (temp_lpa > MAX_LPA) {
                INF_LOG("get error lpa 0x%llx\n", hdr->lpa);
                pba_hdr = pba_append_by_comp_size(pba_hdr, hdr->ccp_hdr.comp_size_4B);
                hdr_offset += sizeof(header_abs_s);
                hdr = (header_abs_s *)(hdr_buffer + hdr_offset);
                continue;
            }

            if (temp_lpa <= MAX_LPA) {
                add_pending_update_ctx(GET_CLEAR_BAD_SECTOR_FROM_L2P(temp_lpa), pba_hdr.all);
                pba_hdr = pba_append_by_comp_size(pba_hdr, hdr->ccp_hdr.comp_size_4B);
                hdr_offset += sizeof(header_abs_s);
                hdr = (header_abs_s *)(hdr_buffer + hdr_offset);
            }
            tot_count++;
        }
    }
```

```c++
static bool parse_hdr_extraction_pba2lba(pbatolba_data *pdata, void *hdr_buf, pba_t pba, u32 target_plane)
  
static bool parse_hdr_extraction_pba2lba(pbatolba_data *pdata, void *hdr_buf, pba_t pba, u32 target_plane)
  
static bool      parse_hdr_extraction_pbadatatype(u32 target_plane, pba_t pba, u8 is_slc, void *hdr_buf, u64 *res)
  
VU_STATUS_E vu_handler_isparitygtsright(vector<string> *pargs __unused, isparitygtsright *param __unused,isparitygtsright_data *pdata __unused)
  
  
LP_HDR_GTS_SIGN

static void generate_fake_gts_ccp(const u64 gts, void *buf_addr)

folding
int p2l_loader::parse_header_extraction(p2l_table *table, u32 hdr_parse_idx)
void folding_rd_task::fd_gc_rd_cb(u32 pool_fct_idx)
  
padding
bool parity_padding_task::poll_padding_w2p(u32 qid, u32 stream, bool gts, bool inner_call)
bool parity_padding_task::submit_padding_w2p(u32 qid, u32 stream, bool gts, bool inner_call)
  
maplog
bool maplog_task::update_maplog_out_of_order(pba_t pba, u64 lba)
bool maplog_task::update_maplog_in_order(u32 stream, pba_t pba, u64 lba)
  
recovery
void rcvry_pf_ftl_ctrl::pb_handle_parse_hdr_extraction_result(u32 parse_mode)
void rcvry_pf_ftl_ctrl::full_scan_parse_hdr_extraction_result()
  
mpage
void mpage_gc_task::mpage_parse_header()
void smry_rcvr_mgr::parse_header_extraction_result()
  
packer & unpacker
NAND_ERROR_E simu_nand_read(u64 *nand_addr, u64 *lba_array, u32 &len, u64 vld_bm, u8 *&buf,u8 bad_pl_mask, u32 mode, u32 slc, bool from_raid_buffer, u8 *raid_buffer, u64 *err_info)

u32 packer_gen_header(PACKER_ACTIVE_B2N_U *packer_active_b2n, u8 *data_addr, u32 stream, u64 lba,u8 byps_cmprs_eng, u8 scheme, u32 *write_size, u8 encrypt)
  
u32 packer_accumulate_gc_data(PACKER_ACTIVE_B2N_U *packer_active_b2n, u8 *data_addr)
  
int unpacker_handle_newsqe(u32 qid, u32 fct_idx, u32 opcode, u8 pool_id)
```

```
#define HW_SIGNATURE            (0x434D50)
#define GTS_LP_CRC_PLACE_HOLDER (0xFEDC'BA98'7654'3210ULL)

#define ERROR_NO_GTS     BIT(20)
#define LP_HDR_PFER_SIGN BIT(63)
#define LP_HDR_GTS_SIGN  BIT(62)

    if (GET_CLEAR_BAD_SECTOR_FROM_L2P(temp_lpa) == LP_HDR_GTS_SIGN) {
        is_gts = true;
    }
    
    // gts need to update maplog B2N size
    if (stB2nParams.gts_mode) {
    pba_t temp_b2n_pbat    = {.all = b2n_pba};
    temp_b2n_pbat.comp_len = sizeof(gts_content) / 4;
    if (!g_maplog_task.update_maplog(pu_stream, temp_b2n_pbat, LP_HDR_GTS_SIGN)) {
    BOMB(MAPLOG_TASK_UPDATE_GTS_WRONG, 0);
    }
    // g_pu_mgr.pblk_mgr[0].inc_sblock_consume_space(block, 32);
    }
    
用户写数据的时候如果发生PF，HW会将b2n padding满，但这个padding的数据不是用户数据，而是HW padding的无效数据，所以HW会将ccp header的lpa的第63位置为1，就是LP_HDR_PFER_SIGN这个宏，那在做head extraction的时候，就要跳过这段数据，不能作为user data，Jeremy你这边有需要修改的吗？
如果有的话，合到下面的feature的分支吧
feature/MYR-603-lpa-bit63-enable

CKP_GSD_STATE_WRONG
```

```c++
static inline bool valid_ccp(header_content *ptr)
{
    return ptr->ccp_hdr.signature == CCP_HEADER_SIGN && ptr->ccp_hdr.comp_size_4B != 0;
}

void packer_gen_gts(PACKER_ACTIVE_B2N_U *packer_active_b2n)
  
void nand_rw_basic_test()
```

```c++
struct ccp_header {
    // total 8 bytes
    u64 signature:       24;     // "CMP" (0x434D50)
    u64 comp_size_4B:    12;     // compressed CP size (4B unit), includes CCP, LP header & payload
    u64 compressible:     1;     // 1, compressed;  0, uncompressed
    u64 pad_size_4B:      2;     // Padding size with 4B
    u64 aes:              1;
    u64 ccp_crc:         24;     // includes CCP, LP header & payload
};

struct lp_header {
    // 16 Bytes
    u64 lba;
    union {
        u64 crc_cp;
        u64 hw_sign; // used when GTS mode
    };
};


struct header_content {
    //total 24 bytes
    struct ccp_header ccp_hdr;
    struct lp_header lp_hdr;
};

struct gts_content {
    //total 32 bytes
    struct header_content header;
    u64 gts;
};

struct header_abs_s
{
    ccp_header ccp_hdr;
    u64        lpa;
};
```

打印某个地址的内容

```
print_mem((void *)(&(_valid_data_buf[_layout_table->entries[rd_cmd.start_rd_idx].ccp.ccp_off])),
tot_data_buf_size, "print rd cmd buf all info");

fct_manager.dump_fct(FCT_POOL_GC_RD);
```

```
static void generate_fake_gts_ccp(const u64 gts, void *buf_addr)
static void generate_fake_ccp(const u64 lba, const u32 comp_size_4B, void *buf_addr)

[10:26] Yijun Lu
wr_dma_update_map

[10:30] Yijun Lu
PC_MultiIOMixPFWithoutCompare.py

s32 micron_b47r_init::sdsw_reg_pre_cnfg()
{
    hw_init_base_type::sdsw_reg_pre_cnfg();
    SDSW_CSR_AUTO_WR_BITS(SDSW_CONTROL, 7, 6, 1);// 64KB page unit
    return 0;
}
```

```

```

```

```

```
[2022-06-13 11:43:22.588] 0x417f73000 |0x51c13bd2 0xc8b24c67 0xde0d28ab 0x5db4a8e7 0x2eb310f7 0xc72fdcb3 0xccb69ac1 0x398e76ae
[2022-06-13 11:43:22.759] 0x417f73020 |0x5945d214 0x24cc7b6a 0xe065604f 0xd180214c 0x68d33319 0x2d3e2f4a 0xdf5ba53f 0x7c4b7ce0
[2022-06-13 11:43:22.929] 0x417f73040 |0xa42cb857 0xbcab6e4e 0xb4d90512 0x9532057e 0x788c8ec9 0xde20bc07 0x434adddf 0x37288b66
[2022-06-13 11:43:23.100] 0x417f73060 |0xba70c067 0xe1c500f6 0x66894432 0xa9604f5d 0xd6e5fb86 0xae188d86 0x6459d929 0x18646ee5
[2022-06-13 11:43:23.271] 0x417f73080 |0x7d3e8c3c 0x26ce3a76 0xb72b1ca6 0xfbba206a 0xbcfee1d2 0x14a425ec 0x5259d35b 0x4c09458d
[2022-06-13 11:43:23.442] 0x417f730a0 |0x3a0d4d57 0x231c2955 0x1adbc20c 0xa0e5968f 0x274be4a2 0x9712ebaf 0x18a3832c 0x0c2b79e7
[2022-06-13 11:43:23.612] 0x417f730c0 |0x1e961e89 0xe97331b5 0xf96a110c 0xcf1c419a 0xfa1ee294 0xecd1164d 0x8cc4e2c1 0xfc0d7c4d
[2022-06-13 11:43:23.783] 0x417f730e0 |0xd9b9fd6a 0x301a4455 0x7dae6cd5 0x31801c6c 0x2de32c4b 0xc5da1a75 0x57e78820 0x59bc25b7
[2022-06-13 11:43:23.954] 0x417f73100 |0x8228f6a8 0x5b56d269 0x13477653 0xe86c4d29 0x5303931a 0xdb50db42 0x9fb26faf 0xa2d98dfa
[2022-06-13 11:43:24.125] 0x417f73120 |0x42a9d69a 0x93b9d348 0x85ca028f 0x88a6bcb6 0xe42785b4 0x8395d581 0x80242d23 0x055aa6c0
[2022-06-13 11:43:24.296] 0x417f73140 |0xb942bca5 0x2b0f44c0 0xa9ed4e66 0x0f4bcbcc 0xa2cf35a3 0x97e2368a 0xb25f7495 0x812d6140
[2022-06-13 11:43:24.467] 0x417f73160 |0x2997e810 0x7cc51ebf 0x577cbcdc 0x71802c5f 0xc8abcd1d 0x9cee0e95 0x4950b6ab 0xc896af3f
[2022-06-13 11:43:24.638] 0x417f73180 |0x64ed94ef 0x56a14cc2 0xac57893b 0x15ed649c 0x205a1fd5 0x3761c9de 0x507b185c 0x6129c8ca
[2022-06-13 11:43:24.809] 0x417f731a0 |0xace605db 0xcc51506d 0xb39c9321 0xb1602901 0x7756f66c 0xa5dfafd3 0x8ee8f001 0xed6b13f2
[2022-06-13 11:43:24.980] 0x417f731c0 |0x7ec14b65 0x00000001 0x00000000 0x00000000 0x00000000 0x3c434d50 0x046dad51 0x06d34c9c
[2022-06-13 11:43:25.150] 0x417f731e0 |0x00000000 0xc8de71ef 0x68b67541 0x6dda387d 0x38d2c91b 0xa38ed94e 0x81c49d8c 0xb1ec0f23

找到0x3c434d50
后面的0x06d34c9c就是lpa的低32位，0x00000000就是高32位
```

```
[0-0:0:0D|0:1:7:178]     io_task0:                   [wr_dma_update_map]pba 0xa3a301ca0117, lba 0x36ff0954
[0-0:0:0D|0:1:7:178]     io_task0:                   [wr_dma_update_map]invalid pf data, pba 0xa3a301ca0117, lba 0x36ff0954
```

```
void io_write::proc_wr_fcq_cpl(st_pending_io * pend, void * msg)

```

```c++
--- a/platform/aarch64/boards/asic/hw_init.cpp
+++ b/platform/aarch64/boards/asic/hw_init.cpp
@@ -1262,6 +1262,7 @@ s32 bics5_4pl_init::sdsw_reg_pre_cnfg()
 {
     hw_init_base_type::sdsw_reg_pre_cnfg();
     SDSW_CSR_AUTO_WR_BITS(SDSW_CONTROL, 7, 6, 1);// 64KB page unit
+    SDSW_CSR_AUTO_WR_BITS(SDSW_CONTROL, 4, 4, 1);// lpa bit 63 enable
     return 0;
 }

@@ -1372,6 +1373,7 @@ s32 micron_b47r_init::sdsw_reg_pre_cnfg()
 {
     hw_init_base_type::sdsw_reg_pre_cnfg();
     SDSW_CSR_AUTO_WR_BITS(SDSW_CONTROL, 7, 6, 1);// 64KB page unit
+    SDSW_CSR_AUTO_WR_BITS(SDSW_CONTROL, 4, 4, 1);// lpa bit 63 enable
     return 0;
 }

--- a/datapath/nvme_write.cpp
+++ b/datapath/nvme_write.cpp
@@ -498,7 +498,10 @@ UPDATE_MAP_RET io_write::wr_dma_update_map(st_pending_io *pend, void *fct_info,
         write_total_nxs[coreid] += (comp_size_4b * 4);

1,583,488
                                                                
```

```
header_content
u64 io_common::get_lba_from_rawdata(u64 data, u32 skip_first_ep, u64 ccp_offset __unused)
void io_write::check_send_sv_w2p(st_pending_io *pend)

SCHEME_UNFORMATTED_RAW_READ
SCHEME_HEADER_EXTRACTION

hdr->ccp_hdr.signature == HW_SIGNATURE
table->insert_p2l_entry(pba_hdr, hdr->lpa)
fct_gc_rd_cmd::set_lba_mis_bmp(fct_entry, lp_bitmap);

header_abs_s

g_l2p_mgr.l2p_map_lookup_sync(hdr->lpa, valid_pba) != L2P_NO_ERROR
g_namespace_mgr.lpa_to_lba(hdr->lpa)

bool maplog_task::update_maplog_out_of_order(pba_t pba, u64 lba)
bool maplog_task::update_maplog_in_order(u32 stream, pba_t pba, u64 lba)

g_maplog_task.update_maplog

l2p
l2p_map_update
l2p_map_update_4kio
l2p_map_multi_update

IO
UPDATE_MAP_RET io_write::wr_dma_update_map(st_pending_io *pend, void *fct_info, pba_t *last_pba)
UPDATE_MAP_RET io_write::w2p_update_map(st_pending_io *pend, void *fct_info, pba_t *last_pba)
void io_task::process_dma_acq(void)
UPDATE_MAP_RET io_write::update_holding_map(u32 core __unused, holding_map* map)

folding
bool folding_wr_task::update_one_entry(u64 cur_entry_idx)
bool gc_update_l2p_task::update_one_entry(l2p_update_ctx& ctx, u64 cur_entry_idx)

trim
bool trim_frontend_exector::clear_fcq_for_mat()
bool trim_frontend_exector::ckp_w2p_cpl(u8 fct_index)

enum READ_CRTL_SCHEME_E
{
    SCHEME_NORMAL_READ,
    SCHEME_UNFORMATTED_RAW_READ,// multiple 4120B
    SCHEME_GC_READ,
    SCHEME_MAP_DATA_READ,    // map data read 2032B
    SCHEME_HEADER_EXTRACTION,// HW returns 1st 16B for each CCP
    SCHEME_GTS_READ,         // HW returns 8B counter value
    SCHEME_NUM,
};

struct header_content {
    //total 24 bytes
    struct ccp_header ccp_hdr;
    struct lp_header lp_hdr;
};

operation_set
```

```c++
getcaptestcnt
```

测试

```
[wr_dma_update_map]pba 0xa3ac02a49ce3, lba 0x37cb1b24

__unlikely(pend->_skip_l2p_up=1

            pend->_skip_l2p_up = 1;
            pend->_manual_cqe = 1;
            pend->_sct = 0;
            pend->_sc  = nvme_sc_data_transfer_error;
            
update_maplog_out_of_order
update_maplog

isPfErData
bool folding_wr_task::update_one_entry(u64 cur_entry_idx)
```

```
sudo ./2618.sh --testcase Host_Idle_Recovery --disk nvme1n1 --iostat_time 10 --comp 21     

sudo nvme fw-download -f bl3_test_0614.bin -x 0x20000 /dev/nvme1
sudo nvme fw-activate /dev/nvme1 -s 1 -a 1
cd fw_validation/

process_dma_acq()
pba 0xa2c800a40186, lba 0x358c2039
```

```
https://sfxsw.atlassian.net/wiki/spaces/SQA/pages/54362126/SH+Lab+Relay+Setup+Steps

```

```
0x37a956c6
0x20300002abe522 l2p查出来的
pba 0xa2c702c145ab, lba 0x37a956c6  pflog打印的

flashrd 0xa2c702c145ab 0 raw tlc
0x14434d50 0xe5b107d0 0x37a956c6 0x80000000

0x00af7816
0x417f735a0 |0x586aba77 0xb1aa6c31 0xbd35a334 0x15db4006 0x4615a9c1 0xeae7d9dc 0xaf855583 0x4c12602c
0x417f735c0 |0x649e12c5 0xefa5324f 0x075b40f7 0x409f8fad 0xd21b88f7 0x00000000 0x00000000 0x14434d50
0x417f735e0 |0x9b8f16d0 0x37a956c7 0x80000000 0x45008aea 0x537bbbc6 0xb1aa6c31 0xbd35a334 0x15db4006


flashrd 0x20300002abe522 0 raw tlc
0x06434d50 0x37169384 0x37a956c6 0x00000000

0xcc6dc4bc 0xcddf5705 0x56d318de 0x0b7eaf93
0x417f73020 |0x187fd3fa 0x5e5fc3d4 0x2c8fc53e 0x0ab76008 0xd34c25c9 0x9fefaff5 0xecaf5c38 0xef0d0b79
0x417f73040 |0x121be2e0 0xae68f893 0xc5abb191 0x2be87008 0x7c59e43b 0x66dd0e03 0xb071f3b0 0x04d232ba
0x417f73060 |0xb8ad156a 0x3b00e98c 0x00486a43 0xedecd10f 0x780ef99c 0x7fb2cbc4 0xcf3ef698 0x737e928c

last week:
1. MYR-3174 vu about capacity monitor -- coding finish
2. MYR-602 b2n flush no gts workaround -- coding finish
3. MYR-603 lpa bit63 enable
4. daily simulator and asic bug triage and fix
this week:
1. MYR-602 b2n flush no gts workaround
2. MYR-603 lpa bit63 enable
3. daily simulator and asic bug triage and fix


```

python3 Tool_UpdateNorImg.py  --fileName=/home/tcnsh/myrtle_fw/utility/tools/nor_config_page/ddr_2933.out --device=/dev/nvme0n1 --mode=2

```
LP_GET_PFER_SIGN(hdr->lpa) == LP_HDR_PFER_SIGN
```

[how to update pcie/ddr config page to NOR - Project: myrtle_fw - Confluence (atlassian.net)](https://sfxsw.atlassian.net/wiki/spaces/MYR/pages/1998849/how+to+update+pcie+ddr+config+page+to+NOR)

```c++
g_pu_mgr.pblk_mgr[0].deallocate_all_slc_blks();
g_format_task.reset_slc_stream_b2n(STREAM_ID_L2P);
g_b2n_task.set_max_outstanding_cmd_thd(STREAM_ID_L2P, MIN_OST_L2P_B2N_CMD);
g_format_task.reset_slc_stream_b2n(STREAM_ID_L2P_PATCH);

When deallocate slc blks, the b2n information of l2p and l2p patch is not reset, so l2p cannot get the block and update l2p during crash recovery

UD2214C0023M@CSDU5SPC38M1:/$ l2pdbg 3
scan_l2_id        1bf1f8
gc_flush_flag     0
dirty count       4039e 10123, 147
throttle          0 0 0 4039e 0
window size       12001
ckp_pend_wr       0
blk_4k_offset     0
ckp_wr_cnt        0
ckp_wr_cpl_cnt    0
ckp_gc_wr_cnt     0
ckp_wr_fail_cnt   0
prepare_wait_cnt  0
ckp state         1 0, 0 0
cur blk           ffffffff
next blk          ffffffff
l2p blk cnt       0
gc threshold      4
gsd flush         0 0
```

