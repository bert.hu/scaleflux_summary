```
u32 nor_task::submit_nor_erase_job(u32 nor_addr, bool sync, bool sector_enabled);
u32 nor_task::submit_nor_write_job(u32 nor_addr,  u32 *data_addr, u32 len, bool sync);
u32 nor_task::submit_nor_read_job(u32 nor_addr,  u32 *data_addr, u32 len, bool sync);
u32 nor_task::nor_add_job(nor_task_job_entry_t &nor_job_entry);
```

