```c++
/*
 * pick blk by free space
 */
u32 blk_mgr::get_max_freeratio_blk()
{
    u64 ref_free_ratio = 0;
    u32 chosen_blk     = INVALID_32BIT;
    for (u32 i = CFG_START_USER_BLOCK; i < CFG_BLOCK_NUM; ++i) {
        if ((get_sblock_status(i) == BS_SEALED_BLOCK) &&
            ((get_sblock_stream(i) == STREAM_ID_HOT) || (get_sblock_stream(i) == STREAM_ID_GC) ||
             (get_sblock_stream(i) == STREAM_ID_WL) || (get_sblock_stream(i) == STREAM_ID_EH))) {
            if (0 == get_sblock_valid_lpa_cnt(i)) {
                chosen_blk = i;
                break;
            }
            u64 cur_free_ratio = get_free_ratio_by_x(i);
            DBG_LOG("blk %u, cur free ratio %llu, ref free ratio %llu\n", i, cur_free_ratio, ref_free_ratio);
            if (cur_free_ratio >= ref_free_ratio) {
                ref_free_ratio = cur_free_ratio;
                chosen_blk     = i;
            }
        }
    }

    return chosen_blk;
}
```

