```c++
#if IS_VALID_ASIC_BICS5_OPN
    _mat_vqid_switch     	= 0;
#else 
     _mat_vqid_switch     	= 1;
#endif

u32 get_mat_vqid_flag()
```

```c++
case MAT_EWR_USE_VQID:{
    if (!get_mat_vqid_flag()) {
        change_mat_vqid_flag(true);
    }
    else {
        change_mat_vqid_flag(false);
    }
    
    break;
}
```

```c++
static bool is_valid_mat_mode(MAT_CFG_MODE_E mode) 
{
    if ((MAT_CREATE_RFS_SCAN_FBB == mode) || (MAT_CLEAN_CARD_MODE == mode) 
        || (MAT_PRE_MAT_MODE == mode) || (MAT_EWR_MODE_WITH_INCREMENT_GBB == mode)
        || (MAT_TRIGGER_SB0_EWR_AND_CREATE_RFS == mode) || (MAT_EWR_DEBUG == mode)){
        return true;
    }

    return false;
}
```

