```c++
void blk_mgr::get_blk_summary_info()
{
    u32 freeblknum      = 0;
    u32 openblknum      = 0;
    u32 sealedblknum    = 0;
    u32 candidateblknum = 0;
    u32 deallocblknum   = 0;
    u32 growndefectnum  = 0;
    u32 invalidblknum   = 0;
    for (u32 i = (CFG_START_USER_BLOCK); i < (CFG_BLOCK_NUM); i++) {
        u32 blkstatus = get_sblock_status(i);
        if(blkstatus == BS_FREE_BLOCK) {
            freeblknum++;
        }else if(blkstatus == BS_OPEN_BLOCK) {
            openblknum++;
        }else if(blkstatus == BS_SEALED_BLOCK) {
            sealedblknum++;
        }else if(blkstatus == BS_COLD_CANDIDATE) {
            candidateblknum++;
            if(candidateblknum < 5) {
                show_sblk_info(i);
            }
        }else if(blkstatus == BS_DEALLOCATED_BLOCK) {
            deallocblknum++;
            if(deallocblknum < 5) {
                show_sblk_info(i);
            }
        }else if(blkstatus == BS_GROWN_DEFECT) {
            growndefectnum++;
            if(growndefectnum < 5) {
                show_sblk_info(i);
            }
        }else if(blkstatus == BS_INVALID_BLOCK) {
            invalidblknum++;
        }
    }
    EVT_LOG(blk_summary_info_evtlog, freeblknum, openblknum, sealedblknum, candidateblknum, deallocblknum,
            growndefectnum, invalidblknum, g_pu_mgr.bb_mgr.get_fbb_count(), g_pu_mgr.bb_mgr.get_gbb_count(),
            g_pu_mgr.bb_mgr.get_mbb_count(), g_pu_mgr.bb_mgr.get_tbb_count(), g_pu_mgr.bb_mgr.get_ebb_count(),
            g_pu_mgr.bb_mgr.get_otp_cnt());

}
```

