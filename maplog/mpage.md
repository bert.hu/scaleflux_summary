```c++
struct mpage_summary {
    u32 type: 4; //MISRAC2004-6.4: can only define bit-field as unsigned int or singed int
    u32 rsv: 28;
    u32 l1_id;
    u64 pba;
};

union mpage_pba_t
{
    struct
    {
        u64 offset : CFG_PBA_OFFSET_BITS;// 4K unit
        u64 plane : CFG_PBA_PLANE_BITS;
        u64 ch : CFG_PBA_CHAN_BITS;
        u64 ce_lun : CFG_PBA_CE_LUN_BITS;
        u64 page : CFG_PBA_PAGE_BITS;
        u64 block : CFG_PBA_BLOCK_BITS;
        //        u64 ep_idx      : CFG_PBA_EP_IDX_BITS;  //bit 0 = first 2k, 1 = second 2k in 4KB read
        u64 ccp_off : CFG_MPAGE_PBA_CCP_OFF_BITS;// 9
    };
    u64 all : 40;
};
```

```
L2_MAP_ENTRY_SIZE 4K
每一个L2_MAP_ENTRY中包含的PBA的条数 4K/sizeof(pba_t)
L1_MAP_ENTRY_NUM （代表需要多少个L2_MAP_ENTRY）用总的LPA_NUM/ 每一个L2_MAP_ENTRY中包含的PBA的条数--这是根据容量算出来的需要多少个L2_MAP_ENTRY
L1_MAP_ENTRY_SIZE 8字节
L2_NODE_SIZE 16字节

那么就能算出来
ALLOC_L1_MAP_SIZE = ROUND_UP((L1_MAP_ENTRY_SIZE(8字节) * MAX_L1_MAP_ENTRY_NUM), DATA_LEN_4K)
ALLOC_L2_MAP_SIZE = L2_MAP_ENTRY_SIZE（4K） * MAX_CACHE_L2_MAP_ENTRY_NUM

ALLOC_L2_TOTAL_SIZE = DDR_L2_SIZE，最大的DDR容量

// if L1_MAP_ENTRY_NUM <= MAX_CACHE_L2_MAP_ENTRY_NUM, then it's all cached
MAX_CACHE_L2_MAP_ENTRY_NUM = ((ALLOC_L2_TOTAL_SIZE) / ((L2_MAP_ENTRY_SIZE) + (L2_NODE_SIZE)))--这是根据DDR算出来的需要多少个L2_MAP_ENTRY + L2_NODE
如果根据容量算出来的L2_MAP_ENTRY个数小于根据DDR算出来的L2_MAP_ENTRY，说明是full cached，否则则是partial cached

最后就是L2_MAP_ENTRY_NUM和L1_MAP_ENTRY_NUM的关系
L2_MAP_ENTRY_NUM =((L1_MAP_ENTRY_NUM >= MAX_CACHE_L2_MAP_ENTRY_NUM) ? MAX_CACHE_L2_MAP_ENTRY_NUM : L1_MAP_ENTRY_NUM)
意思是，如果根据容量算出来的需要的L2_MAP_ENTRY个数超过了根据最大DDR算出来的L2_MAP_ENTRY，那么L2_MAP_ENTRY_NUM就是根据最大DDR算出来的L2_MAP_ENTRY个数，否则就是根据实际容量算出来的需要L2_MAP_ENTRY的个数

#define INVALID_L1_ID         N_BIT_MASK(26)         // MAX_L1_MAP_ENTRY_NUM
#define INVALID_L2_ID         N_BIT_MASK(23)         // MAX_CACHE_L2_MAP_ENTRY_NUM
根据这个来看，根据容量算出来的L2_MAP_ENTRY个数肯定是大于根据最大DDR算出来的L2_MAP_ENTRY个数
根据容量算出来的L2_MAP_ENTRY个数不超过2^26,根据最大DDR算出来的L2_MAP_ENTRY个数不超过2^23

DDR_L2_NODE_SIZE = L2_NODE_SIZE * MAX_CACHE_L2_MAP_ENTRY_NUM
DDR_L2_MAP_SIZE = L2_MAP_ENTRY_SIZE * MAX_CACHE_L2_MAP_ENTRY_NUM
```

```c++
struct l1_map_entry
{
    u64 mpage_pba : 40;// mpage using slc uncompress write, some bit in pba_t can be ignore and convert to this 40bit
                       // format
    u64 l2_id         : 23;// index for l2 map, max bit should cover MAX_L2_MAP_NUM, INVALID_L2_ID for un-cached
    u64 in_dirty_list : 1; // to avoid dirty q full
};

struct l2_node
{
    u32 l1_id            : 26;
    u32 state            : 3;// L2P_STATE_E
    u32 dirty_again_flag : 1;// during flushing, hit write and dirty again.
    u32 hit_flag         : 1;// free pool manager will check this flag first to decide if we should free this l2 node
    u32 gc_flag          : 1;// before flush, mark gc node. during flush, 1 for clean node flush, 0 for dirty node flush
    u32 rsv;
    u32 patch_id;// for dirty again case, patch_id is different with flush_patch_id
    u32 flush_patch_id;
};
```

#### 校验l2p_patch是否有效

```c++
bool l2p_patch_mgr::is_valid_patch_buffer(u64 buffer)
{
    l2p_patch_entry *entry = (l2p_patch_entry *)buffer;
    if ((entry[0].start_entry.entry_type == L2P_PATCH_START_ENTRY) &&
        (entry[0].start_entry.signature == L2P_PATCH_START_SIGNATURE)) {
        return true;
    }
    else {
        return false;
    }
}
```

```c++
union l2p_patch_entry {
    u16 entry_type;
    struct {
        u64 entry_type  :16;
        u64 lpa         :48;
        u64 pba         :64;
    } write_entry;

    struct {
        u64 entry_type  :16;
        u64 l1_id       :48;
        u64 to_be_added1;
    } mpage_flush_entry;

    struct {
        u64 entry_type  :16;
        u64 array_idx   :32;
        u64 offset      :16;
        u64 bit_cnt     :16;
        u64 rsvd        :48;
    } trim_entry;

    struct {
        u64 entry_type  :16;
        u64 group_id    :48;
        u64 signature;
    } start_entry;

    struct {
        u64 entry_type  :16;
        u64 reserved1   :48;
        u64 reserved2;
    } end_entry;
} __attribute__((packed));
```

```
sector size=512，表示一个LBA有512字节
那么MAXLBA*512表示盘的最大容量（字节）
MAXLBA*512/1000/1000/1000=XXGB
```

```
如何计算ccp和head extraction

```

