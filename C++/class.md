定义一个基类

```c++
class fw_acq_base_type
{
public:
    virtual void  init(u32 qid, u32 stream, u32 prio, u32 direction, u32 acq_type) = 0;
    virtual u32   alloc_sq_entries(void** sq_entries, u32 cnt) = 0;
    virtual void  *alloc_sq_one_entry() { BOMB(FOR_IO_ONLY,0); return nullptr; }
    virtual bool  alloc_sq_cplt_entries(void** sq_entries, u32 cnt) = 0;
    virtual u32   alloc_cq_entries(void** cq_entries, u32 cnt) = 0;
    virtual void  send_sq_msg(u32 cnt) = 0;
    virtual void  send_cq_msg(u32 cnt) = 0;
    virtual void* fetch_sq_msg(void) = 0;
    virtual void* fetch_cq_msg(void) = 0;
    virtual void  release_sq_entry(void) = 0;
    virtual void  release_sq_entry_no_vld(void) = 0;
    virtual void  release_cq_entry(void) = 0;
    virtual bool  check_sq_full(void) = 0;
    virtual bool  check_cq_full(void) = 0;
    virtual bool  check_sq_empty(void) = 0;
    virtual bool  check_cq_empty(void) = 0;
    virtual void  show_queue_status(bool all __unused) {}
    virtual void  track_act_idx(u32 act_idx __unused) {}
    virtual u32   get_sq_num(void) { return 0x5AA5; }
    virtual void  force_clear_lock_state(void) = 0;
    virtual void  backup_queue(u64 buf_addr) = 0;
    virtual void  restore_queue(u64 buf_addr) = 0;
    virtual u32   queue_buf_size(void) = 0;
};
```

两个继承它的子类

一个是nvme_cmd，HW send to FW

```c++
template <u32 QDEPTH, typename Tsq, typename Tcq, bool NeedLock>
class fw_nvme_cmd_acq_pair : public fw_acq_base_type
{
    spinlock _fw_acq_lock;

public:
    // u32 stream_id;
    // u32 q_prio;      // 2'b00: lowest priority; 2'b11: highest priority
    u32          _q_dir; // 0: hw issue sq, fw return cq; 1: fw issue sq, hw return cq
    u32          _q_type;// ACQ_TYPE_NVME_IO/ACQ_TYPE_NVME_ADMIN/ACQ_TYPE_DMA
    u32          _qid;
    u32          _sq_status;
    u32          _sq_alloc_ptr;
    u32          _sq_head;
    u32          _sq_tail;
    u32          _cq_status;
    u32          _cq_alloc_ptr;
    u32          _cq_head;
    u32          _cq_tail;
    u32          _core_id;
    bool         _lock = NeedLock;

    volatile Tsq sq_buf[QDEPTH] __aligned(16);// queue base address should be 16bytes aligned
    volatile Tcq cq_buf[QDEPTH] __aligned(16);// queue base address should be 16bytes aligned

public:
    void init(u32 qid, u32 stream, u32 prio, u32 direction, u32 acq_type)
    {
        // qtype is TBD in hw definition
        // if ((ENTRYSIZE%4) != 0)
        //{
        //    pr_dbg("error: entry size should be 4 bytes aligned.");
        //    return;
        //}
        _qid = qid;
        _sq_head = 0;
        _sq_tail = 0;
        _sq_alloc_ptr = 0;
        _sq_status = ACQ_EMPTY;
        _cq_head = 0;
        _cq_tail = 0;
        _cq_alloc_ptr = 0;
        _cq_status = ACQ_EMPTY;
        _core_id = 0xFFFFFFFFU;

        _q_dir = direction;
        _q_type = acq_type;

        DBG_LOG("init fw acq %d  depth %d sq_buf:%llx cq_buf:%llx\n", _qid, QDEPTH, (u64)sq_buf, (u64)cq_buf);

        memset((void*)sq_buf, 0, sizeof(sq_buf));//cstat !MISRAC++2008-5-3-4
        memset((void*)cq_buf, 0, sizeof(cq_buf));//cstat !MISRAC++2008-5-3-4

        bsp_platform::flush_dcache((u64)sq_buf, sizeof(sq_buf));//cstat !MISRAC++2008-5-3-4
        bsp_platform::flush_dcache((u64)cq_buf, sizeof(cq_buf));//cstat !MISRAC++2008-5-3-4

        // config ACE
        fw_acq_config(QUEUE_TYPE_SQ, 1, _qid, stream, prio, direction, acq_type, (u64)sq_buf, QDEPTH,
                      sizeof(Tsq));// config sq
        fw_acq_config(QUEUE_TYPE_CQ, 1, _qid, stream, prio, direction, acq_type, (u64)cq_buf, QDEPTH,
                      sizeof(Tcq));// config cq
    }

    u32  alloc_sq_entries(void** sq_entries __unused, u32 cnt __unused) { return 0; }

    void force_clear_lock_state(void)
    {
        if (_lock) {
            _fw_acq_lock.spin_force_init();
        }
        return;
    }

    void backup_queue(u64 buf_addr)
    {
        u32 *ptr = (u32 *)buf_addr;
        // backup ptr
        ptr[0]  = _qid;
        ptr[1]  = _sq_head;
        ptr[2]  = _sq_tail;
        ptr[3]  = _sq_alloc_ptr;
        ptr[4]  = _sq_status;
        ptr[5]  = _cq_head;
        ptr[6]  = _cq_tail;
        ptr[7]  = _cq_alloc_ptr;
        ptr[8]  = _cq_status;
        ptr[9]  = _core_id;
        ptr[10] = _q_dir;
        ptr[11] = _q_type;

        // backup sq/cq buffer
        memcpy((void*)&ptr[12], (void*)sq_buf, sizeof(sq_buf));//cstat !MISRAC++2008-5-3-4
        memcpy((void*)((u64)&ptr[12] + sizeof(sq_buf)), (void*)cq_buf, sizeof(cq_buf));//cstat !MISRAC++2008-5-3-4

        bsp_platform::flush_dcache(buf_addr, sizeof(fw_nvme_cmd_acq_pair));//cstat !MISRAC++2008-5-3-4
    }

    void restore_queue(u64 buf_addr)
    {
        u32 *ptr = (u32 *)buf_addr;
        // restore ptr
        _qid            = ptr[0];
        _sq_head        = ptr[1];
        _sq_tail        = ptr[2];
        _sq_alloc_ptr   = ptr[3];
        _sq_status      = ptr[4];
        _cq_head        = ptr[5];
        _cq_tail        = ptr[6];
        _cq_alloc_ptr   = ptr[7];
        _cq_status      = ptr[8];
        _core_id        = ptr[9];
        _q_dir          = ptr[10];
        _q_type         = ptr[11];

        // restore sq/cq buffer
        memcpy((void*)sq_buf, (void*)&ptr[12], sizeof(sq_buf));//cstat !MISRAC++2008-5-3-4
        memcpy((void*)cq_buf, (void*)((u64)&ptr[12] + sizeof(sq_buf)), sizeof(cq_buf));//cstat !MISRAC++2008-5-3-4

        bsp_platform::flush_dcache((u64)sq_buf, sizeof(sq_buf));//cstat !MISRAC++2008-5-3-4
        bsp_platform::flush_dcache((u64)cq_buf, sizeof(cq_buf));//cstat !MISRAC++2008-5-3-4
    }

    u32 queue_buf_size(void)
    {
        return sizeof(fw_nvme_cmd_acq_pair);
    }

    bool alloc_sq_cplt_entries(void** sq_entries __unused, u32 cnt __unused) { return false; }

    u32  alloc_cq_entries(void** cq_entries, u32 cnt)
    {
        if (_lock) {
            _fw_acq_lock.spin_lock();
        }

        if (_cq_status == ACQ_FULL) {
            if (_lock) {
                _fw_acq_lock.spin_unlock();
            }
            u32 cq_tail = bsp_platform::ace_acq_get_ptr(QUEUE_TYPE_CQ, _qid,1);
            u32 cq_head = bsp_platform::ace_acq_get_ptr(QUEUE_TYPE_CQ, _qid,0);
            INF_LOG("qid: %x CQ FULL:  hw head %x  hw tail %x, fw head:%x, fw tail:%x\n",_qid, cq_head, cq_tail, _cq_head, _cq_tail);
            return 0;
        }

        u32 cq_alloc_tail = (_cq_alloc_ptr < _cq_head) ? _cq_head : (_cq_head + QDEPTH);

        if ((_cq_alloc_ptr + cnt) >= cq_alloc_tail) {
            cnt = cq_alloc_tail - _cq_alloc_ptr;
        }

        // ASSERT(cnt == 1);
        u32 to_alloc_cnt = cnt;

        while (to_alloc_cnt) {
            cq_entries[cnt - to_alloc_cnt] = (void*)(&cq_buf[_cq_alloc_ptr]);
            _cq_alloc_ptr = (_cq_alloc_ptr + 1) % QDEPTH;
            --to_alloc_cnt;
        }

        if (_cq_alloc_ptr == _cq_head) {
            u32 cq_head_update = bsp_platform::ace_acq_get_ptr(QUEUE_TYPE_CQ, _qid, 0);
            if(cq_head_update != _cq_head) {
                _cq_head = cq_head_update;
                _cq_status = ACQ_NORMAL;

            }
            else {
                _cq_status = ACQ_FULL;
            }
        }
        else {
            _cq_status = ACQ_NORMAL;
        }

        if (_lock) {
            _fw_acq_lock.spin_unlock();
        }

        return cnt;
    }

    void send_sq_msg(u32 cnt __unused) { return; }

    void send_cq_msg(u32 cnt)
    {
        if (_lock) {
            _fw_acq_lock.spin_lock();
        }

        // check if cnt is valid
        if (((_cq_alloc_ptr > _cq_tail) && (cnt > (_cq_alloc_ptr - _cq_tail))) || ((_cq_alloc_ptr <= _cq_tail) && (cnt > (QDEPTH + _cq_alloc_ptr - _cq_tail)))) {
            ERR_LOG("send cq fail(cnt %d overflow)\n", cnt);
            if (_lock) {
                _fw_acq_lock.spin_unlock();
            }
            BOMB(ACQ_SEND_CQ_ERROR, 0);
            return;
        }

        _cq_tail = (_cq_tail + cnt) % QDEPTH;
        // call ace api to send msg, and update tail
        fw_acq_doorbell(QUEUE_TYPE_CQ, _qid, 1, cnt, _q_dir);

        if (_lock) {
            _fw_acq_lock.spin_unlock();
        }
        return;
    }

    u32 get_all_cmd(u32 *cmds)
    {
        u32 cnt = 0;
        u32 start = _sq_head;

        while(1) {
            if (sq_buf[start].fields.vld == 0) {
                break;
            }
            cmds[cnt++] = sq_buf[start].fields.act_idx;
            start++;
            if (start == QDEPTH) {
                start = 0;
            }
        }
        
        return cnt;
    }

    void* fetch_sq_msg(void)
    {
#if 0 //((__INCLUDE_UNIT_TEST__ != 1) && (__BL3__ == 1) && (__DCS__ == 0))
        if (g_kernel.get_admin_smpmgr_state() == SMP_STATE_RUNNING) {
            u32 core_id = bsp_platform::get_coreid();

            if (_core_id != 0xFFFFFFFFU) {
                BOMB(ACQ_CORE_CONFLICT, (_core_id == core_id));
            }
            else {
                _core_id = core_id;
            }
        }
#endif
        if (_lock) {
            _fw_acq_lock.spin_lock();
        }
        u32 new_cq_head;

        // check if new msg come in
        if (__unlikely(sq_buf[_sq_head].fields.vld == 0)) {
            if (_lock) {
                _fw_acq_lock.spin_unlock();
            }
            return nullptr;
        }

        // update head ptr for paired cq
        new_cq_head = sq_buf[_sq_head].fields.sq_hd_ptr;

        // update paired cq status
        _cq_head = new_cq_head;
        if (_cq_head == _cq_tail) {
            _cq_status = ACQ_EMPTY;
        }
        else {
            _cq_status = ACQ_NORMAL;
        }

        // get entry address and opcode
        volatile Tsq* entry_ptr = &sq_buf[_sq_head];
        if (_lock) {
            _fw_acq_lock.spin_unlock();
        }
        return (void*)entry_ptr;
    }

    u32 pop_all_sq_msg(u16* act_list, u32 max_cnt)
    {
        u32 new_cq_head = 0;
        u32 valid_cnt = 0;

        // check if new msg come in
        while(max_cnt--) {
            if (sq_buf[_sq_head].fields.vld == 0) {
                break;
            }

            new_cq_head = sq_buf[_sq_head].fields.sq_hd_ptr;
            act_list[valid_cnt] = sq_buf[_sq_head].fields.act_idx;
            sq_buf[_sq_head].fields.vld = 0;
            _sq_head ++;
            valid_cnt ++;
            if (_sq_head == QDEPTH) {
                _sq_head = 0;
            }
        }

        if (valid_cnt > 0) {
            //INF_LOG("fetch %x, new cq_head %x, new _sq_head %x\n", valid_cnt, new_cq_head, _sq_head);
            _cq_head = new_cq_head;
            if (_cq_head == _cq_tail) {
                _cq_status = ACQ_EMPTY;
            }
            else {
                _cq_status = ACQ_NORMAL;
            }
            fw_acq_doorbell(QUEUE_TYPE_SQ, _qid, 0, valid_cnt, _q_dir);
        }
        
        return valid_cnt;
    }

    void* fetch_cq_msg(void) { return nullptr; }

    void  release_sq_entry(void)
    {
        if (_lock) {
            _fw_acq_lock.spin_lock();
        }
        // clear valid flag
        sq_buf[_sq_head].fields.vld = 0;

        // update sq head ptr
        _sq_head = (_sq_head + 1);
        if (_sq_head == QDEPTH) {
            _sq_head = 0;
        }
        fw_acq_doorbell(QUEUE_TYPE_SQ, _qid, 0, 1, _q_dir);
        if (_lock) {
            _fw_acq_lock.spin_unlock();
        }
        return;
    }

    void release_sq_entry_no_vld(void)
    {
        if (_lock) {
            _fw_acq_lock.spin_lock();
        }
        // update sq head ptr
        _sq_head = (_sq_head + 1) % QDEPTH;
        fw_acq_doorbell(QUEUE_TYPE_SQ, _qid, 0, 1, _q_dir);
        if (_lock) {
            _fw_acq_lock.spin_unlock();
        }
        return;
    }

    u32  get_sq_num(void) {
        u32 sq_cnt = 0;
        u32 head = _sq_head;

        while(1) {
            if (sq_buf[head].fields.vld == 0) {
                break;
            }
            else {
                sq_cnt++;
            }

            head++;
            if (head >= QDEPTH) {
                head = 0;
            }
        }

        return sq_cnt;
    }

    void release_cq_entry(void) { return; }

    bool check_sq_full(void) { return _sq_status == ACQ_FULL; }

    bool check_cq_full(void) { return _cq_status == ACQ_FULL; }

    bool check_sq_empty(void)
    {
#if ((__WITH_SIMULATOR__ == 1) && (__ASIC_SIMULATOR__ == 0))
            if (_sq_status == ACQ_EMPTY || (_sq_head + 1) % QDEPTH  == _sq_tail)
		    return true;
	    else
		    return false;
#else
	    return _sq_status == ACQ_EMPTY;
#endif
    }

    bool check_cq_empty(void) { return _cq_status == ACQ_EMPTY; }

    void show_queue_status(bool all)
    {
        pr_dbg("qid:%d, (cq_base:0x%llx head:0x%x-addr:0x%llx tail:0x%x sts:%x), ",
            _qid, (u64)cq_buf, _cq_head, (u64)&cq_buf[_cq_head], _cq_tail, _cq_status);
        pr_dbg("(sq_base:0x%llx head:0x%x-addr:0x%llx tail:0x%x sts:%x), _cq_alloc_ptr:0x%x\n",
            (u64)sq_buf, _sq_head, (u64)&sq_buf[_sq_head], _sq_tail, _sq_status, _cq_alloc_ptr);

        pr_dbg("sq idx opc subopc length   actidx act_addr vld sqhdprt | cq opc subopc length   ctag   sts\n");
        for (u32 idx = 0; idx < QDEPTH; idx++) {
            NVME_CMD_MSG_U* sq = (NVME_CMD_MSG_U*)&sq_buf[idx];
            NVME_CPL_MSG_U* cq = (NVME_CPL_MSG_U*)&cq_buf[idx];
            if (all || (sq->fields.vld != 0)) {
                pr_dbg("   %-3x %-3x %-6x %-8x %-6x %-8llx %-3x %-7x |    %-3x %-6x %-8x %-4x   %-3x\n",
                        idx, sq->fields.opcode, sq->fields.sub_opc, sq->fields.len, sq->fields.act_idx,
                        (u64)act_get_entry(sq->fields.act_idx), sq->fields.vld, sq->fields.sq_hd_ptr,
                        cq->fields.opcode, cq->fields.sub_opc, cq->fields.len, cq->fields.ctag, cq->fields.status);
            }
        }
        pr_dbg("\n");
        pr_dbg("DWORD FORMAT\n");
        for (u32 idx = 0; idx < QDEPTH; idx++) {
            u32* sq_data = (u32*)&sq_buf[idx];
            u32* cq_data = (u32*)&cq_buf[idx];

            pr_dbg("   %-3x %-8x %-8x %-8x %-8x %-8x %-8x |    %-8x %-8x\n",
                    idx, *sq_data, *(sq_data+1), *(sq_data+2), *(sq_data+3), *(sq_data+4), *(sq_data+5), *cq_data, *(cq_data+1));

        }
        pr_dbg("\n");
    }

    void track_act_idx(u32 act_idx)
    {
        for (u32 i = 0; i < QDEPTH; i++) {
            if (sq_buf[i].fields.act_idx == act_idx) {
                pr_dbg("qid %x's index %-3x, opcode:0x%x, subopc:0x%x, vld:0x%x\n", _qid, i, sq_buf[i].fields.opcode, sq_buf[i].fields.sub_opc, sq_buf[i].fields.vld);
            }
        }
        return;
    }
};
```

另一个是从FW send to HW

```c++

template <u32 QDEPTH, typename Tsq, typename Tcq, bool NeedLock>
class fw_dma_trig_acq_pair : public fw_acq_base_type
{
    spinlock _fw_acq_lock;

public:
    // u32 stream_id;
    // u32 q_prio;      // 2'b00: lowest priority; 2'b11: highest priority
    u32          _q_dir; // 0: hw issue sq, fw return cq; 1: fw issue sq, hw return cq
    u32          _q_type;// ACQ_TYPE_NVME_IO/ACQ_TYPE_NVME_ADMIN/ACQ_TYPE_DMA
    u32          _qid;
    u32          _sq_status;
    u32          _sq_alloc_ptr;
    u32          _sq_head;
    u32          _sq_tail;
    u32          _cq_status;
    u32          _cq_alloc_ptr;
    u32          _cq_head;
    u32          _cq_tail;
    u32          _core_id;
    bool         _lock = NeedLock;

    volatile Tsq sq_buf[QDEPTH] __aligned(16);// queue base address should be 16bytes aligned
    volatile Tcq cq_buf[QDEPTH] __aligned(16);// queue base address should be 16bytes aligned

public:
    void init(u32 qid, u32 stream, u32 prio, u32 direction, u32 acq_type)
    {
        // qtype is TBD in hw definition
        // if ((ENTRYSIZE%4) != 0)
        //{
        //    pr_dbg("error: entry size should be 4 bytes aligned.");
        //    return;
        //}
        _qid = qid;
        _sq_head = 0;
        _sq_tail = 0;
        _sq_alloc_ptr = 0;
        _sq_status = ACQ_EMPTY;
        _cq_head = 0;
        _cq_tail = 0;
        _cq_alloc_ptr = 0;
        _cq_status = ACQ_EMPTY;
        _core_id = 0xFFFFFFFFU;

        _q_dir = direction;
        _q_type = acq_type;

        DBG_LOG("init fw acq %d  depth %d sq_buf:%llx cq_buf:%llx\n", _qid, QDEPTH, (u64)sq_buf, (u64)cq_buf);

        memset((void*)sq_buf, 0, sizeof(sq_buf));//cstat !MISRAC++2008-5-3-4
        memset((void*)cq_buf, 0, sizeof(cq_buf));//cstat !MISRAC++2008-5-3-4

        bsp_platform::flush_dcache((u64)sq_buf, sizeof(sq_buf));//cstat !MISRAC++2008-5-3-4
        bsp_platform::flush_dcache((u64)cq_buf, sizeof(cq_buf));//cstat !MISRAC++2008-5-3-4

        // config ACE
        fw_acq_config(QUEUE_TYPE_SQ, 1, _qid, stream, prio, direction, acq_type, (u64)sq_buf, QDEPTH,
                      sizeof(Tsq));// config sq
        fw_acq_config(QUEUE_TYPE_CQ, 1, _qid, stream, prio, direction, acq_type, (u64)cq_buf, QDEPTH,
                      sizeof(Tcq));// config cq
    }

    void force_clear_lock_state(void)
    {
        if (_lock) {
            _fw_acq_lock.spin_force_init();
        }
        return;
    }

    void backup_queue(u64 buf_addr)
    {
        u32 *ptr = (u32 *)buf_addr;
        // backup ptr
        ptr[0]  = _qid;
        ptr[1]  = _sq_head;
        ptr[2]  = _sq_tail;
        ptr[3]  = _sq_alloc_ptr;
        ptr[4]  = _sq_status;
        ptr[5]  = _cq_head;
        ptr[6]  = _cq_tail;
        ptr[7]  = _cq_alloc_ptr;
        ptr[8]  = _cq_status;
        ptr[9]  = _core_id;
        ptr[10] = _q_dir;
        ptr[11] = _q_type;

        // backup sq/cq buffer
        memcpy((void*)&ptr[12], (void*)sq_buf, sizeof(sq_buf));//cstat !MISRAC++2008-5-3-4
        memcpy((void*)((u64)&ptr[12] + sizeof(sq_buf)), (void*)cq_buf, sizeof(cq_buf));//cstat !MISRAC++2008-5-3-4

        bsp_platform::flush_dcache(buf_addr, sizeof(fw_dma_trig_acq_pair));//cstat !MISRAC++2008-5-3-4
    }

    void restore_queue(u64 buf_addr)
    {
        u32 *ptr = (u32 *)buf_addr;
        // restore ptr
        _qid            = ptr[0];
        _sq_head        = ptr[1];
        _sq_tail        = ptr[2];
        _sq_alloc_ptr   = ptr[3];
        _sq_status      = ptr[4];
        _cq_head        = ptr[5];
        _cq_tail        = ptr[6];
        _cq_alloc_ptr   = ptr[7];
        _cq_status      = ptr[8];
        _core_id        = ptr[9];
        _q_dir          = ptr[10];
        _q_type         = ptr[11];

        // restore sq/cq buffer
        memcpy((void*)sq_buf, (void*)&ptr[12], sizeof(sq_buf));//cstat !MISRAC++2008-5-3-4
        memcpy((void*)cq_buf, (void*)((u64)&ptr[12] + sizeof(sq_buf)), sizeof(cq_buf));//cstat !MISRAC++2008-5-3-4

        bsp_platform::flush_dcache((u64)sq_buf, sizeof(sq_buf));//cstat !MISRAC++2008-5-3-4
        bsp_platform::flush_dcache((u64)cq_buf, sizeof(cq_buf));//cstat !MISRAC++2008-5-3-4
    }

    u32 queue_buf_size(void)
    {
        return sizeof(fw_dma_trig_acq_pair);
    }

    void* alloc_sq_one_entry()
    {
        void *ret = (void*)(&sq_buf[_sq_alloc_ptr]);
        _sq_alloc_ptr++;
        if (__unlikely(_sq_alloc_ptr == QDEPTH)) {
            _sq_alloc_ptr = 0;
        }

        if (((_sq_alloc_ptr + 1) % QDEPTH) == _sq_head) {
            _sq_status = ACQ_FULL;
        }
        else {
            _sq_status = ACQ_NORMAL;
        }

        return ret;
    }

    u32 alloc_sq_entries(void** sq_entries, u32 cnt)
    {
#if 0 //((__INCLUDE_UNIT_TEST__ != 1) && (__BL3__ == 1) && (__DCS__ == 0))
        if (g_kernel.get_admin_smpmgr_state() == SMP_STATE_RUNNING) {
            u32 core_id = bsp_platform::get_coreid();

            if (_core_id != 0xFFFFFFFFU) {
                BOMB(ACQ2_CORE_CONFLICT, (_core_id == core_id));
            }
            else {
                _core_id = core_id;
            }
        }
#endif
        if (_lock) {
            _fw_acq_lock.spin_lock();
        }

        if (_sq_status == ACQ_FULL) {
            if (_lock) {
                _fw_acq_lock.spin_unlock();
            }
            return 0;
        }

        u32 sq_alloc_tail = (_sq_alloc_ptr < _sq_head) ? _sq_head : (_sq_head + QDEPTH);

        if ((_sq_alloc_ptr + cnt) >= sq_alloc_tail) {
            BOMB(ACQ_SQ_TAIL_WRONG, sq_alloc_tail > _sq_alloc_ptr);
            cnt = sq_alloc_tail - _sq_alloc_ptr - 1;
        }

        // ASSERT(cnt == 1);
        u32 to_alloc_cnt = cnt;

        while (to_alloc_cnt) {
            sq_entries[cnt - to_alloc_cnt] = (void*)(&sq_buf[_sq_alloc_ptr]);
            _sq_alloc_ptr = (_sq_alloc_ptr + 1) % QDEPTH;
            --to_alloc_cnt;
        }

        if (((_sq_alloc_ptr + 1) % QDEPTH) == _sq_head) {
            _sq_status = ACQ_FULL;
        }
        else {
            _sq_status = ACQ_NORMAL;
        }

        if (_lock) {
            _fw_acq_lock.spin_unlock();
        }

        return cnt;
    }

    bool alloc_sq_cplt_entries(void** sq_entries, u32 cnt)
    {
        if (_lock) {
            _fw_acq_lock.spin_lock();
        }

        if (_sq_status == ACQ_FULL) {
            if (_lock) {
                _fw_acq_lock.spin_unlock();
            }
            return false;
        }

        u32 sq_alloc_tail = (_sq_alloc_ptr < _sq_head) ? _sq_head : (_sq_head + QDEPTH);

        if ((_sq_alloc_ptr + cnt) >= sq_alloc_tail) {
            // BOMB(ACQ_SQ_TAIL_WRONG, sq_alloc_tail > _sq_alloc_ptr);
            if (_lock) {
                _fw_acq_lock.spin_unlock();
            }
            return false;
        }

        // ASSERT(cnt == 1);
        u32 to_alloc_cnt = cnt;

        while (to_alloc_cnt) {
            sq_entries[cnt - to_alloc_cnt] = (void*)(&sq_buf[_sq_alloc_ptr]);
            _sq_alloc_ptr = (_sq_alloc_ptr + 1) % QDEPTH;
            --to_alloc_cnt;
        }

        if (((_sq_alloc_ptr + 1) % QDEPTH) == _sq_head) {
            _sq_status = ACQ_FULL;
        }
        else {
            _sq_status = ACQ_NORMAL;
        }

        if (_lock) {
            _fw_acq_lock.spin_unlock();
        }

        return true;
    }

    u32  alloc_cq_entries(void** sq_entries __unused, u32 cnt __unused) { return 0; }

    void send_sq_msg(u32 cnt)
    {
        if (_lock) {
            _fw_acq_lock.spin_lock();
        }
 
        // check if cnt is valid
        /*
        if (((_sq_alloc_ptr >= _sq_tail) && (cnt > (_sq_alloc_ptr - _sq_tail))) || ((_sq_alloc_ptr < _sq_tail) && (cnt > (QDEPTH + _sq_alloc_ptr - _sq_tail)))) {
            ERR_LOG("send sq fail(cnt %d overflow)\n", cnt);
            _fw_acq_lock[_qid].spin_unlock();
            BOMB(ACQ_SQ_CNT_WRONG, 0);
            return;
        }
        */

        _sq_tail = (_sq_tail + cnt) % QDEPTH;
        //INF_LOG("%s, %d  pi here, _qid:%x, _sq_tail:%x\n", __func__, __LINE__, _qid, _sq_tail);
        // call ace api to send msg, and update tail
        fw_acq_doorbell(QUEUE_TYPE_SQ, _qid, 1, cnt, _q_dir);

        if (_lock) {
            _fw_acq_lock.spin_unlock();
        }
        return;
    }

    void  send_cq_msg(u32 cnt __unused) { return; }

    void* fetch_sq_msg(void) { return nullptr; }

    void* fetch_cq_msg(void)
    {
        if (_lock) {
            _fw_acq_lock.spin_lock();
        }
        volatile Tcq* entry_ptr;
        u32           new_sq_head;
        // INF_LOG("fetch_cq_msgr _cp_head %d tail %d, vld %d, \n", _cq_head, _cq_tail, cq_buf[_cq_head].fields.vld);

        // check if new msg come in
        if (cq_buf[_cq_head].fields.vld == 0) {
            if (_lock) {
                _fw_acq_lock.spin_unlock();
            }
            return nullptr;
        }

        // update head ptr for paired sq
        new_sq_head = cq_buf[_cq_head].fields.sq_hd_ptr;

        // update paired cq status
        if (new_sq_head != _sq_head) {
            _sq_head = new_sq_head;

            if (_sq_head == _sq_tail) {
                _sq_status = ACQ_EMPTY;
            }
            else {
                _sq_status = ACQ_NORMAL;
            }
        }

        // get entry address and opcode
        entry_ptr = &cq_buf[_cq_head];
        if (_lock) {
            _fw_acq_lock.spin_unlock();
        }
        return (void*)entry_ptr;
    }

    void release_sq_entry(void) { return; }

    void release_sq_entry_no_vld(void) { return; }

    void release_cq_entry(void)
    {
        if (_lock) {
            _fw_acq_lock.spin_lock();
        }
        // clear valid flag
        cq_buf[_cq_head].fields.vld = 0;

        // update cq head ptr
        _cq_head = (_cq_head + 1) % QDEPTH;

        /*
         * about dmb here, please refer to QUC-1697
         */
        #if (__ZEBU__ == 0) // TODO: need to check why zebu fail
        bsp_platform::general_dmb();
        #endif
        fw_acq_doorbell(QUEUE_TYPE_CQ, _qid, 0, 1, _q_dir);
        if (_lock) {
            _fw_acq_lock.spin_unlock();
        }
        return;
    }

    bool check_sq_full(void) { return _sq_status == ACQ_FULL; }

    bool check_cq_full(void) { return _cq_status == ACQ_FULL; }

    bool check_sq_empty(void)
    {
#if ((__WITH_SIMULATOR__ == 1) && (__ASIC_SIMULATOR__ == 0))
            if (_sq_status == ACQ_EMPTY || (_sq_head + 1) % QDEPTH  == _sq_tail)
		    return true;
	    else
		    return false;
#else
	    return _sq_status == ACQ_EMPTY;
#endif
    }

    bool check_cq_empty(void) { return _cq_status == ACQ_EMPTY; }

    void show_queue_status(bool all)
    {
        pr_dbg("qid:%d, (cq_base:0x%llx head:0x%x-addr:0x%llx tail:0x%x sts:%x), ",
            _qid, (u64)cq_buf, _cq_head, (u64)&cq_buf[_cq_head], _cq_tail, _cq_status);
        pr_dbg("(sq_base:0x%llx head:0x%x-addr:0x%llx tail:0x%x sts:%x), _cq_alloc_ptr:0x%x\n",
            (u64)sq_buf, _sq_head, (u64)&sq_buf[_sq_head], _sq_tail, _sq_status, _cq_alloc_ptr);

        pr_dbg("sq idx opc sub act fct ltrg rst | cq opc sub act sts vld fct ltrg sqhdprt | sq fct_addr act_addr\n");
        for (u32 idx = 0; idx < QDEPTH; idx++) {
            NVME_DMA_TRIG_MSG_U* sq = (NVME_DMA_TRIG_MSG_U*)&sq_buf[idx];
            NVME_DMA_CPL_MSG_U*  cq = (NVME_DMA_CPL_MSG_U*)&cq_buf[idx];
            u32                  fct_idx = sq->fields.fct_idx;
            u32*                 fct_addr = (u32*)fct_manager.get_fct((fct_idx >> 7) & FCT_POOL_ID_MASK, fct_idx & FCT_POOL_ENTRY_MASK);

            if (all || (cq->fields.vld != 0)) {
                pr_dbg("   %-3x %-3x %-3x %-3x %-3x %-4x %-3x %-3x|    %-3x %-3x %-3x %-3x %-3x %-3x %-4x %-7x |    %-8llx %-8llx\n",
                        idx, sq->fields.opcode, sq->fields.sub_opc, sq->fields.act_idx, sq->fields.fct_idx, sq->fields.l_trig, sq->fields.rsv, sq->fields.length,
                        cq->fields.opcode, cq->fields.sub_opc, cq->fields.act_idx, cq->fields.sts, cq->fields.vld, cq->fields.fct_idx,
                        cq->fields.l_trig, cq->fields.sq_hd_ptr, (u64)fct_addr, (u64)act_get_entry(sq->fields.act_idx));
            }
        }
        pr_dbg("\n");
        pr_dbg("DWORD FORMAT\n");
        for (u32 idx = 0; idx < QDEPTH; idx++) {
            u32* sq_data = (u32*)&sq_buf[idx];
            u32* cq_data = (u32*)&cq_buf[idx];

            pr_dbg("   %-3x %-8x %-8x %-8x %-8x %-8x %-8x |    %-8x %-8x\n",
                    idx, *sq_data, *(sq_data+1), *(sq_data+2), *(sq_data+3), *(sq_data+4), *(sq_data+5), *cq_data, *(cq_data+1));

        }
    }
};
```

