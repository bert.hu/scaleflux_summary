### How to run Zebu
```
Log into 192.168.4.161
User: tcn
Pass: scaleflux
```

zebu workarea
```
DDR4
Workarea for zebu
/home/tcn/e2e_env/runtime_xtor_e2e_opt
Load bl3 images here
/home/tcn/e2e_env/ddr_load/xtor_e2e_opt

LPDDR5
/home/tcn/zebu_hex/script
/home/tcn/e2e_env/runtime_xtor_e2e_lp5_opt
sudo ./down_pcieq_16k /sys/bus/pci/devices/0000\:01\:00.0/resource0 {bl3_file} trigger
```
#### 1. prepare the bl3 img
For DDR4 images on Zebu you can backdoor load the bl3 by doing the following:
Copy the bl3 you want to run into this directory
`sudo scp {bl3} tcn@192.168.4.161:/opt/univa/zebu_workarea/zebu_hex/bl3_hex_gen/xtor_e2e/`
`cd /opt/univa/zebu_workarea/zebu_hex/bl3_hex_gen/xtor_e2e/`

Run the following command, replace {bl3} with the name of the bl3
`./read_bl3.py {bl3}`

#### 2. prepare the env, for device
Run in the following directory
`cd /opt/univa/zebu_workarea/user/tcn/zebu_env/quince/runtime_xtor/`

Source the following file to set Zebu variables
`source ./sourceme_zs4`

To start Zebu run the following command
`make run_zrci`

#### 3. prepare another env, for virtual host
Open a second terminal and go to the same directory 
`cd /opt/univa/zebu_workarea/user/tcn/zebu_env/quince/runtime_xtor/`

Source in the new window also (**if you use tmux, skip this**)
`source ./sourceme_zs4`

#### 4. wait for device init done
In the first terminal wait for the following prints

    INFO  : Log      [pci0     [ ProxyPCIeXtor   ] ]  0074.760578499               Ready to connect...
    DEBUG2: Log      [proxyTB  [ ProxyVisor      ] ]  0074.760722736               ProxyXtor "pci0" is ready.
    INFO  : Log      [proxyTB  [ ProxyVisor      ] ]  0074.760855305               All ProxyXtors are ready to connect to the VM...
    READY!
    Created regular file ready_to_connect
    INFO  : Log      [proxyTB  [ ProxyVisor      ] ]  0074.761216896               Waiting for client to connect via connection specification "/home/tcn/proxyTB_zRci_2020/proxyTB_zRci_snps_recovered/runtime/VM.socket".
    DEBUG2: Log      [proxyTB  [ ProxyVisor      ] ]  0074.761644252               Starting server accepting connection on port 41939...
    INFO  : Log      [proxyTB  [ ProxyVisor      ] ]  0074.761924260               Wrote "/home/tcn/proxyTB_zRci_2020/proxyTB_zRci_snps_recovered/runtime/VM.socket" with server connection specification.
    DEBUG1: Log      [proxyTB  [ ProxyVisor      ] ]  0074.761954437               Waiting for client connection on port 41939...

#### 5. run the virtual host
After you see those prints run the following command in the second terminal
`make VBoxRun`

#### 6. wait the device pcie init done
In the first terminal wait for the following prints

    DEBUG1: Log      [pci0     [ ProxyPCIeXtor   ] ]  0177.398045584  @00038423179 (  114) DN CfgRd  00:00.0/000 -> 01:00.0  A:0x00f8  L:   1  D:0x00000001  PwrMgmt Capability Structure
    DEBUG1: Log      [pci0     [ ProxyPCIeXtor   ] ]  0177.399264954  @00038423504 (  112) DN CfgRd  00:00.0/000 -> 01:00.0  A:0x0084  L:   4  D:0x10008022  PCIe.DevCaps
    DEBUG1: Log      [pci0     [ ProxyPCIeXtor   ] ]  0177.400458868  @00038423829 (  113) DN CfgRd  00:00.0/000 -> 01:00.0  A:0x00fc  L:   4  D:0x00000008  PwrMgmt.Ctrl+PwrMgmt.BrExt+PwrM
    DEBUG1: Log      [pci0     [ ProxyPCIeXtor   ] ]  0177.401676471  @00038424158 (  114) DN CfgRd  00:00.0/000 -> 01:00.0  A:0x008c  L:   4  D:0x00400c45  PCIe.LinkCaps
    DEBUG1: Log      [pci0     [ ProxyPCIeXtor   ] ]  0177.402889018  @00038424491 (  115) DN CfgRd  00:00.0/000 -> 01:00.0  A:0x0092  L:   2  D:0x00000045  PCIe.LinkStat


#### 7. login the virtual host
Run the following command in the second terminal after the prints
`make ConnectVM`


#### 8. Wait for following in the virtual host

    ssh -p 3022 root@localhost
    Last login: Fri Dec  9 03:29:49 2022 from gateway
    [root@localhost /]#

#### 9. goto BL3 
##### 9.1. run the fw img that you parsed by read_bl3.py
`cd zebu_hex/script/`
`sudo ./pcimem /sys/bus/pci/devices/0000\:01\:00.0/resource0  0xc014 w 0x446f6e65`
##### 9.2. run new img
`cd zebu_hex/script/`
`scp {new img} ./`
`./down_pciemem_q_nocheck /sys/bus/pci/devices/0000\:01\:00.0/resource0 {new img} trigger`
##### LPDDR5
`sudo ./down_pcieq_16k /sys/bus/pci/devices/0000\:01\:00.0/resource0 {bl3_file} trigger`

**BL3 Init should start now**

#### 10. uart log
The log from your run last night was saved here
`/opt/univa/zebu_workarea/user/tcn/zebu_env/quince/runtime_xtor/zRci.run_15/runtime_data/test_dump_uart_1.log`
it creates a new directory `zRci.run_{lastest number}`
If you go into the new directory and into runtime_data. The log file is saved as test_dump_uart_1.log

#### 检查环境
echo $ZEBU_PHYSICAL_LOCATION 

#### multi stream
```diff
diff --git a/ftl/ftl.h b/ftl/ftl.h
index 9a0d1f9..ec23768 100755
--- a/ftl/ftl.h
+++ b/ftl/ftl.h
@@ -16,7 +16,7 @@

#if (__ENABLE_MULTI_STREAM__ == 1)
#define HOT_STREAM_NUM 16
-#define MAX_DISPATCH_NUM  1 //1 for 4 multi-stream; 2 for 8 multi-stream; 4 for 16 muti-stream
+#define MAX_DISPATCH_NUM  2 //1 for 4 multi-stream; 2 for 8 multi-stream; 4 for 16 muti-stream
#define STREAM_OFFSET (HOT_STREAM_NUM - 2)
#else
#define HOT_STREAM_NUM 2
diff --git a/platform/aarch64/boards/quince/asic/hw_init.cpp b/platform/aarch64/boards/quince/asic/hw_init.cpp
index 51829eb..8132d28 100755
--- a/platform/aarch64/boards/quince/asic/hw_init.cpp
+++ b/platform/aarch64/boards/quince/asic/hw_init.cpp
@@ -1514,6 +1514,7 @@ s32 micron_b58r_init::fce_reg_pre_cnfg()
     FCE_CSR_AUTO_WR(RETRY_CONTROL, 0x00040202);

 

     FCE_CSR_AUTO_WR_BITS(SCRAMBLER_CONTROL, 11, 8, 1);
+    FCE_CSR_AUTO_WR_BITS(NPS_DEBUG_CTRL, 3, 0, 0x4);
     return 0;
}

diff --git a/utility/buildutil.py b/utility/buildutil.py
index 44d4024..551a9d9 100755
--- a/utility/buildutil.py
+++ b/utility/buildutil.py
@@ -81,7 +81,7 @@ bl3_macro             = (" -D__BL3__=1 ")
bl2_macro             = (" -D__BL2__=1 ")
bl1_macro             = (" -D__BL1__=1 ")
quince_fpga_macros    = quince_macro + " -D__FPGA__=1 -D__ONE_CLUSTER__=1 -D__ENABLE_MULTI_STREAM__=0"
-quince_zebu_macros    = quince_macro + " -D__ASIC__=1 -D__ZEBU__=1 -D__ENABLE_MULTI_STREAM__=0"
+quince_zebu_macros    = quince_macro + " -D__ASIC__=1 -D__ZEBU__=1 -D__ENABLE_MULTI_STREAM__=1"
quince_asic_macros    = quince_macro + " -D__ASIC__=1 -D__ZEBU__=0 -D__ENABLE_MULTI_STREAM__=0"
```
