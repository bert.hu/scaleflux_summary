## DDR4
### init constructor
total 104

#### step 1
3rd: so long 7000005c40 def_console_task
    bl      7000005a40 <_ZN12console_taskC1EPKc13PRINT_LEVEL_E>
    bl      7000008bc0 <_ZN9dimension4fullEv>
    bl      7000008be0 <_ZN6screen6createEjj>
    `cut down the screen and dimension`
53th: so long 70000d1610 g_err_injector
    b       70000cfe70 <_ZN15error_injectionC1Ev> 
    `cut down the memset`

BM_CSR_AUTO_WR(BM_CONTROL_2) 5mins

#### step 2
.........
700002dd60 g_evt_buf 
    (memset)
..............................................
70000b27a0  _GLOBAL__sub_I_g_blk_err_vali_task
    (memset)
.....................................................
70000cfbd0 _GLOBAL__sub_I_g_err_injector
    => _ZN15error_injectionC1Ev (memcpy)

#### step 3
46 70000b27a0 g_blk_err_vali_task
..............................................
54 70000ddcc0 folding_worker_adm_gc
......................................................
67 700011dd80 g_l2p_patch_mgr
...................................................................

#### now total 10min

## LPDDR5
### download
./down_pcieq_16k /sys/bus/pci/devices/0000\:01\:00.0/resource0 ./quince_fw.zebu.wout.bl3.dev_build.csdu4sbc38a0_vU.0.0@7558f6_14528.img trigger

start: 20:04
download : 20:19
constructor : 20:20
BM Reg INIT DONE cpl:  20:26
end 20:30

./down_pcieq_16k /sys/bus/pci/devices/0000\:01\:00.0/resource0 ./quince_fw.zebu.wout.bl3.dev_build.csdu4sbc38a0_vU.0.0@51ad3d_14529.img trigger

start: 20:55
download : 21:10
constructor : 21:11
BM Reg INIT DONE cpl: 21:16
end 21:20

## 16 stream
### DDR4
### LPDDR5
10:11
10:26 program done 
10:28 init done
10:35 rfs
10:40