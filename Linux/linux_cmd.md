### ps

linux上进程有5种状态:

1. 运行(正在运行或在运行队列中等待)
2. 中断(休眠中, 受阻, 在等待某个条件的形成或接受到信号)
3. 不可中断(收到信号不唤醒和不可运行, 进程必须等待直到有中断发生)
4. 僵死(进程已终止, 但进程描述符存在, 直到父进程调用wait4()系统调用后释放)
5. 停止(进程收到SIGSTOP, SIGSTP, SIGTIN, SIGTOU信号后停止运行运行)

ps工具标识进程的5种状态码:

D 不可中断 uninterruptible sleep (usually IO)

R 运行 runnable (on run queue)

S 中断 sleeping

T 停止 traced or stopped

Z 僵死 a defunct (”zombie”) process

```
ps [options] [--help]
```

举例：

显示所有进程信息

```python
ps -A
```

显示指定用户信息

```python
ps -u root
```

ps 与grep 常用组合用法，查找特定进程

```python
ps -ef|grep ssh
```

显示所有进程信息，连同命令行

```python
ps -ef
```

将目前属于您自己这次登入的 PID 与相关信息列示出来

```python
ps -l
```

各相关信息的意义：

F 代表这个程序的旗标 (flag)， 4 代表使用者为 super user

S 代表这个程序的状态 (STAT)，关于各 STAT 的意义将在内文介绍

UID 程序被该 UID 所拥有

PID 就是这个程序的 ID ！

PPID 则是其上级父程序的ID

C CPU 使用的资源百分比

PRI 这个是 Priority (优先执行序) 的缩写，详细后面介绍

NI 这个是 Nice 值，在下一小节我们会持续介绍

ADDR 这个是 kernel function，指出该程序在内存的那个部分。如果是个 running的程序，一般就是 “-”

SZ 使用掉的内存大小

WCHAN 目前这个程序是否正在运作当中，若为 - 表示正在运作

TTY 登入者的终端机位置

TIME 使用掉的 CPU 时间。

CMD 所下达的指令为何

在预设的情况下， ps 仅会列出与目前所在的 bash shell 有关的 PID 而已，所以， 当我使用 ps -l 的时候，只有三个 PID。

## 其他对于任务的操作

```
bg %n //将编号为n的任务转后台运行
fg %n //将编号为n的任务转前台运行
ctrl+z //挂起当前任务
ctrl+c //结束当前任务
```

## 终止后台运行的进程

```
kill -9 进程号
```
#### tmux串口

```
sudo tmux new -s 112.61
sudo minicom -D /dev/ttyUSB0 -c on -C 112_61_1018.log
sudo minicom -D /dev/ttyUSB5 -c on -C 112_61_1018.log

sudo ls -l /dev/ttyUSB*

UD2150C0019M@CSDU4SPC38A0
```
### 查找log文件

```
lsof | grep log | grep mini
<<<<<<< Updated upstream
```
### 如何只保留日志文件的最后n行？

```shell
$ wc -l myscript.log
475494 myscript.log

$ echo "$(tail -1000 myscript.log)" > myscript.log

echo "$(tail -25000000 UD2219G0157M.log)" > myscript.log

$ wc -l myscript.log
1000 myscript.log

(base) [root@host-kvm-sr650 MYR-9475]# wc -l 2023-10-25_nvme5_evt.log
158652362 2023-10-25_nvme5_evt.log

(base) [root@host-kvm-sr650 MYR-9475]# grep -n 'pf_cnt: *4b6' 2023-10-25_nvme5_evt.log
136446363:  > _pf_cnt:             4b6
136446900:  > pf_cnt:              4b6

25000000
echo "$(tail -25000000 2023-10-25_nvme5_evt.log)" > myscript.log
```

### 压缩和解压

```
tar -czvf xxx.tar.gz  source_file (tar -czvf 包名.tar.gz  源文件)        #以tar.gz方式打包并gz方式压缩
tar -xzvf xxx.tar.gz -C path (tar -xzvf xxx.tar.gz -C 目标路径)          #解压缩包
```

Jlink

```shell
J-Link>connect
Device "CORTEX-A53" selected.


Connecting to target via JTAG
TotalIRLen = 4, IRPrint = 0x01
JTAG chain detection found 1 devices:
 #0 Id: 0x6BA00477, IRLen: 04, CoreSight JTAG-DP
Scanning AP map
AP scan stopped (required AP found)
AP[0]: APB-AP
Scanning ROMTbl @ 0x80000000
[0]Comp[0] @ 0x80010000: TMC
[0]Comp[1] @ 0x80020000: TMC
[0]Comp[2] @ 0x80030000: STM-500
[0]Comp[3] @ 0x80040000: CSTF
[0]Comp[4] @ 0x80050000: CTI
[0]Comp[5] @ 0x80060000: CTI
[0]Comp[6] @ 0x80070000: ATBR
[0]Comp[7] @ 0x80080000: TPIU
[0]Comp[8] @ 0x80090000: ??? CID: 0x00000000, PID: 0x00000000
[0]Comp[9] @ 0x800A0000: TMC
[0]Comp[10] @ 0x800B0000: TSG
[0]Comp[11] @ 0x800C0000: CSTF
[0]Comp[12] @ 0x800D0000: CSTF
[0]Comp[13] @ 0x800E0000: TMC
[0]Comp[14] @ 0x80400000: ROM Table
Scanning ROMTbl @ 0x80400000
[1]Comp[0] @ 0x80410000: Cortex-A53
[1]Comp[1] @ 0x80420000: CTI-A53
Core found. Stopped ROM table scan: https://wiki.segger.com/ROMTableScan
Cortex-A53 @ 0x80410000 (detected)
CoreCTI @ 0x80420000 (detected)
Debug architecture: ARMv8
6 code breakpoints, 4 data breakpoints
Add. info (CPU temp. halted)
Cache info:
  Inner cache boundary: none
  LoU Uniprocessor: 1
  LoC: 2
  LoU Inner Shareable: 1
I-Cache L1: 1024 KB, 1024 Sets, 64 Bytes/Line, 16-Way
D-Cache L1: 1024 KB, 1024 Sets, 64 Bytes/Line, 16-Way
Unified-Cache L2: 64 KB, 256 Sets, 64 Bytes/Line, 4-Way
Exception AArch usage: All: AArch64
VMSAv8-64: Supports 48-bit VAs
Cortex-A53 identified.
J-Link>mem32 0x24108018 0x1
24108018 = C0001110

```

### linux环境变量

```
[root@haps-sj-1u myrtle_b0]# echo $PATH
/usr/local/sbin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin

[xiangli@localhost root]$ which confpro
/cad2/synopsys/protocomp/R-2020.12-SP1-1/bin/confpro
```

#### 挂载mount

```
cat /etc/fstab
```

其他

```
preprocess

ssh -T tcn@10.33.118.41 'ls -l ~/haps_env/hw_img'

+ lrwxrwxrwx. 1 tcn tcn 60 Jan 10 01:59 /home/tcn/haps_env/hw_img -> /home/tcn/Quince/VALID/S73_haps_release_11190_20230106@14-03

ssh -T tcn@10.33.118.41 'ls -l ~/haps_env/bl1.bin'

+ lrwxrwxrwx. 1 tcn tcn 80 Jan 10 01:59 /home/tcn/haps_env/bl1.bin -> /home/tcn/jouyang/quince_fw.fpga.bl1.dev_build.c3bics52t_vU.0.0@fc529d_13712.bin

ssh -T tcn@10.33.118.31 'ls -l ~/haps_env/bl3.bin'

+ lrwxrwxrwx. 1 tcn tcn 109 Jan 10 02:13 /home/tcn/haps_env/bl3.bin -> /home/tcn/loganli/quince_fw.fpga_ramdisk.wout.bl3.dev_build.c3ramdisk_pull_down_perst_vU.0.0@a53ac8_14387.img

ssh -T tcn@10.33.118.41 'sudo chmod 777 -R /workarea && mkdir -p /workarea/workspace/Quince_Validation_HAPS_Ramdisk_PF_PullDown_PERST_Test/logs/01_10_02_22_07'

ssh -T tcn@10.33.118.31 'sudo chmod 777 -R /workarea && mkdir -p /workarea/workspace/Quince_Validation_HAPS_Ramdisk_PF_PullDown_PERST_Test'
```

=======

```shell
dmesg -T | grep -iE 'Failed | error' | grep -E 'nvme0n1 | 0000:01:00.0
```
>>>>>>> Stashed changes
