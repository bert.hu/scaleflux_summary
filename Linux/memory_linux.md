```txt
cat /proc/meminfo | grep Huge
(base) [root@localhost AssertionID]# cat /proc/meminfo | grep Huge
AnonHugePages:   7176192 kB
HugePages_Total:     105
HugePages_Free:      105
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       2048 kB

1. 1GB Hugepages
The example complains about 1048576kB hugepages, i.e. 1GB. This is correct, you don't have 1GB pages configured as we can see in /proc/meminfo

But you do not need 1GB pages to start a DPDK application, so it just informs and continues.

2. Free Hugepages
The free hugepages are all gone because if we do not specify the amount of memory to use for the DPDK application with -mor --socket-mem option, it will reserve all the available hugepages.

More on that in DPDK Getting Started Guide here.

In case the hugepages remain allocated even when the application has ended (or crashed), you can free the hugepages manually by removing files in /mnt/huge


[root@localhost kni]# echo 256 >>  /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages
[root@localhost kni]# cat /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages
256
[root@localhost kni]# cat /sys/kernel/mm/hugepages/hugepages-524288kB/nr_hugepages
256


[root@localhost kni]# grep Hugepagesize /proc/meminfo
Hugepagesize:     524288 kB

cat /proc/meminfo | grep Huge

FCQ_FPGA_TEST_ERASE
mat_poll_erase
mat_prepare_stb_block
INF_LOG("Debug:MAT_TASK_STATE_DO_EWR\n");

REL_BMP_ERR
```

```c++
bool erase_specific_blk_die_and_poll_cpl(u64 pba, bool slc)
{
    u32       sblock = GET_BLOCK_FROM_PBA(pba);
    u32       die    = GET_DIE_FROM_PBA(pba);
    const u32 qid    = FCQ_FPGA_TEST_ERASE;
    // 1. send cmd
    if (!erase_specific_blk_die(sblock, die, slc, qid)) {
        INF_LOG("ERROR send erase cmd failed! sblk: %u, slc: %u\n", sblock, ((u32)slc));
        return false;
    }
    // 2. poll cpl
    u32 cnt = be_poll_cpl(qid, erase_task_check_erase_cpl, 1, 10000000, 0);
    if (cnt < 1) {
        INF_LOG("ERROR get erase cpl failed! sblk: %u, slc: %u, cnt: %u\n", sblock, ((u32)slc), cnt);
        return false;
    }
    return true;
}

//cmd_cpl_cb should release fct_entry
int be_poll_cpl(u32 qid, cmd_cpl_cb_t cmd_cpl_cb, u32 cnt, u32 timeout, u32 assert_imm)
{
    u32 getcnt = 0;
    void *cpl;
    // default timeout is 1, in case wrong usage to set it to 0, treat them the same.
    bool single_poll = (timeout <= 1);
    if (!single_poll) {
        timeout *= 1000;
    }

    do {
        cpl = fcq_fetch_cq_msg(qid);
        if (cpl) {
            cmd_cpl_cb(fcq_ace_cpl_msg::get_fct_idx(cpl));
            fcq_release_cq_entry(qid);
            getcnt++;
        }
        if (single_poll) {
            return getcnt;
        }
        --timeout;
    } while ((getcnt < cnt) && timeout);

    if (getcnt < cnt) {
        COLOR_LOG(COLOR_RED, "%s poll qid %d cpl time out.\n", __FUNCTION__, qid);
        if (assert_imm) {
            BOMB(POLL_CMPL_TIMEOUT, 0);
        }
    }

    return getcnt;
}


void erase_task_check_erase_cpl(u32 fct_idx) { u32 die __unused = check_die_erase_cpl(fct_idx); }


mat_prepare_stb_block
    INF_LOG("Debug:BKOPS_TASK_STATE_INIT\n");
    INF_LOG("Debug:BKOPS_TASK_STATE_FW_STAT_ERASE_DIE_BLOCK\n");
INF_LOG("Debug:BKOPS_TASK_STATE_FW_STAT_POLL_ERASE_CMPL\n");


assert_panic_body
    
    
```

```
Thread 8 (Thread 0x7ffff46b6700 (LWP 18087)):
#0  arch_spin_lock (lock=@0x90dcf8: 1) at /home/tcnsh/myrtle-dev/myrtle_fw2/platform/x86_64/plat_ext.h:257
#1  bsp_platform::spin_lock (lock=@0x90dcf8: 1) at /home/tcnsh/myrtle-dev/myrtle_fw2/platform/plat.h:307
#2  spinlock::spin_lock (this=0x90dcf8 <fw_fct_erase+1048>) at /home/tcnsh/myrtle-dev/myrtle_fw2/lib/liblock.h:33
#3  fw_fct_template<FCT_ENTRY_FOR_ACE_ERASE_CMD_U, true>::alloc_entries_idx (this=0x90d8e0 <fw_fct_erase>, fct_indexes=0x
7ffff46b5cc7 "", cnt=1) at /home/tcnsh/myrtle-dev/myrtle_fw2/datapath/fct/fw_fct_manager.h:224
#4  0x00000000004c0c15 in fw_fct_manager::alloc_entries_idx (cnt=1, fct_entries_idx=0x7ffff46b5cc7 "", pool_ID=6, this=<o
ptimized out>) at /home/tcnsh/myrtle-dev/myrtle_fw2/datapath/fct/fw_fct_manager.h:456
#5  fct_alloc_entries_idx (cnt=1, fct_indexes=0x7ffff46b5cc7 "", pool_ID=6) at /home/tcnsh/myrtle-dev/myrtle_fw2/datapath
/fct/fw_fct_manager.h:456
#6  erase_specific_blk_die (block=block@entry=188, die=die@entry=17, slc=slc@entry=false, qid=qid@entry=48, pool_id=pool_
id@entry=6) at /home/tcnsh/myrtle-dev/myrtle_fw2/ftl/erase.cpp:766
#7  0x00000000004c0dcc in erase_specific_blk_die_and_poll_cpl (pba=pba@entry=394265664, slc=slc@entry=false) at /home/tcn
sh/myrtle-dev/myrtle_fw2/ftl/erase.cpp:794
#8  0x00000000004c0f3b in erase_sblock_and_poll_cpl_by_die (sblock=sblock@entry=188, slc=slc@entry=false) at /home/tcnsh/
myrtle-dev/myrtle_fw2/ftl/erase.cpp:741
#9  0x000000000054c015 in mat_task::mat_prepare_stb_block (this=<optimized out>) at /home/tcnsh/myrtle-dev/myrtle_fw2/mat
/mat.cpp:2700
#10 0x0000000000550985 in mat_task::run (this=0x4f10b00 <g_mat_task>) at /home/tcnsh/myrtle-dev/myrtle_fw2/mat/mat.cpp:74
2
#11 0x000000000055d6b7 in task_scheduler::run_one_task (this=0x4f45e90 <g_kernel+11024>, task=0x4f10b00 <g_mat_task>) at
/home/tcnsh/myrtle-dev/myrtle_fw2/os-kern/task_scheduler.cpp:386
#12 0x000000000055d904 in task_scheduler::<lambda(u64)>::operator() (__closure=<synthetic pointer>, __closure=<synthetic
pointer>, bit=25) at /home/tcnsh/myrtle-dev/myrtle_fw2/os-kern/task_scheduler.cpp:477
#13 task_scheduler::run_all_task (this=0x4f45e90 <g_kernel+11024>) at /home/tcnsh/myrtle-dev/myrtle_fw2/os-kern/task_sche
duler.cpp:498
#14 0x000000000055c75e in smpmgr::smpmgr_start (this=0x4f43588 <g_kernel+520>) at /home/tcnsh/myrtle-dev/myrtle_fw2/os-ke
rn/smpmgr.cpp:218
#15 0x0000000000542b09 in fw_slave_main (coreid=5) at /home/tcnsh/myrtle-dev/myrtle_fw2/os-kern/kern.h:44
#16 0x000000000058da1b in slave_core_thread_func (usrPtr=<optimized out>) at /home/tcnsh/myrtle-dev/myrtle_fw2/platform/x
86_64/platform.cpp:363


Thread 1 (Thread 0x7ffff7fe2740 (LWP 18074)):
#0  assert_panic_body (id=<optimized out>, id@entry=REL_BMP_ERR, cond=<optimized out>, cond@entry=0x903da4 <fw_fct_templa
te<FCT_ENTRY_FOR_ACE_ERASE_CMD_U, true>::release_entries_by_idx(unsigned char*, unsigned int)::cond> "0", fileln=<optimiz
ed out>, fileln@entry=0x903d60 <fw_fct_template<FCT_ENTRY_FOR_ACE_ERASE_CMD_U, true>::release_entries_by_idx(unsigned cha
r*, unsigned int)::fileln> "/home/tcnsh/myrtle-dev/myrtle_fw2/datapath/fct/fw_fct_manager.h:305", fmt=fmt@entry=0x4f1050d
 <fw_fct_template<FCT_ENTRY_FOR_ACE_ERASE_CMD_U, true>::release_entries_by_idx(unsigned char*, unsigned int)::fmt_array>
"") at /home/tcnsh/myrtle-dev/myrtle_fw2/8ag/assert.cpp:224
#1  0x000000000053ff2e in fw_fct_template<FCT_ENTRY_FOR_ACE_ERASE_CMD_U, true>::release_entries_by_idx (this=0x90d8e0 <fw
_fct_erase>, fct_indexes=<optimized out>, cnt=1) at /home/tcnsh/myrtle-dev/myrtle_fw2/datapath/fct/fw_fct_manager.h:295
#2  0x00000000004bf99a in fw_fct_manager::release_entries_by_idx (cnt=1, fct_indexes=0x7fffffffe0e7 "/", pool_ID=<optimiz
ed out>, this=<optimized out>) at /home/tcnsh/myrtle-dev/myrtle_fw2/datapath/fct/fw_fct_manager.h:464
#3  fct_release_entries_by_idx (cnt=1, fct_indexes=0x7fffffffe0e7 "/", pool_ID=<optimized out>) at /home/tcnsh/myrtle-dev
/myrtle_fw2/datapath/fct/fw_fct_manager.h:464
#4  check_die_erase_cpl (fct_idx=fct_idx@entry=815) at /home/tcnsh/myrtle-dev/myrtle_fw2/ftl/erase.cpp:512
#5  0x00000000004bf9c5 in erase_task_check_erase_cpl (fct_idx=fct_idx@entry=815) at /home/tcnsh/myrtle-dev/myrtle_fw2/ftl
/erase.cpp:516
#6  0x000000000040aa16 in static_blk_mgr::poll_erase_cpl (this=0x3767a48 <g_fw_statistics+65672>, qid=qid@entry=48) at /h
ome/tcnsh/myrtle-dev/myrtle_fw2/8ag/console/static_blk.cpp:286
#7  0x000000000045d8ff in bkops_task::run (this=0x3767c00 <g_bkops_task>) at /home/tcnsh/myrtle-dev/myrtle_fw2/admin/idle
_bkops.cpp:412
#8  0x000000000055d6b7 in task_scheduler::run_one_task (this=0x4f437f0 <g_kernel+1136>, task=0x3767c00 <g_bkops_task>) at
 /home/tcnsh/myrtle-dev/myrtle_fw2/os-kern/task_scheduler.cpp:386
#9  0x000000000055d904 in task_scheduler::<lambda(u64)>::operator() (__closure=<synthetic pointer>, __closure=<synthetic
pointer>, bit=18) at /home/tcnsh/myrtle-dev/myrtle_fw2/os-kern/task_scheduler.cpp:477
#10 task_scheduler::run_all_task (this=0x4f437f0 <g_kernel+1136>) at /home/tcnsh/myrtle-dev/myrtle_fw2/os-kern/task_sched
uler.cpp:498
#11 0x000000000055c897 in smpmgr::smpmgr_start (this=0x4f43380 <g_kernel>) at /home/tcnsh/myrtle-dev/myrtle_fw2/os-kern/s
mpmgr.cpp:178
#12 0x0000000000542d60 in fw_admin_main () at /home/tcnsh/myrtle-dev/myrtle_fw2/os-kern/kern.h:42


mat_prepare_stb_block
init evt block
_write_ready = false
is_write_ready()
BKOPS_TASK_STATE_INIT

init_stb_die_info()
_die_info_initialized = true;
is_die_info_initialized()
EVT_STATE_INIT


FCQ_NAND_ADMIN
```

