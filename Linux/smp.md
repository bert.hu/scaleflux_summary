```
void task_base::inactive(void) { BIT_CLEAR(_scheduler->_active_flag, _idx_of_task_array); }
bool task_base::is_active(void) { return (BIT_TEST(_scheduler->_active_flag, _idx_of_task_array) == 0); }
void task_base::active(void) { BIT_SET(_scheduler->_active_flag, _idx_of_task_array); }

void task_base::start(SMP_STATE_E to_state)
{
    _smp_state_resume_to = to_state;
    resume(to_state);
    active();
}

bool task_base::stop(PAUSE_TYPE_E type)
{
    TASK_STATUS_E idleStatus = this->pause(type);

    if (idleStatus == TASK_PAUSED) {
        inactive();
    }

    return idleStatus;
}
```

```
pcie空间初始化在BL2阶段进行，如果Bl2初始化失败，则pcie空间初始化失败，那么肯定ERROR Get ctrl rdy failed
```

```c++
static inline u64 read_daif(void) { u64 v; __asm__ volatile("mrs %0, " "daif" : "=r"(v)); return v; }
```

