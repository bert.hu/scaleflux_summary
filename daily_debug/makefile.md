```
Last login: Sun Nov  5 22:07:39 2023 from 192.168.38.99
[root@localhost ~]# tmux a

all :           debug release
debug :         burnin_asic_img         asic_img                bl1asic simu tools  burnin_e1s_asic_img
release :       burnin_asic_img_rel asic_img_rel        bl1asic simu tools  burnin_e1s_asic_img_rel
#target to release the img with NEW BL2 build from tip code
release_wbl2 :  burnin_asic_img_rel_wbl2 asic_img_rel_wbl2      bl1asic simu tools  burnin_e1s_asic_img_rel_wbl2
simu:           x86 x86wout x86bics5 x86bics5wout e1simu
gcov:           asicgcov x86gcov
x86 : x86-64_build.sh
        bash ./$<

x86wout : x86-64_wout_build.sh
        bash ./$<

x86bics5 : x86-64_build_bics5.sh
        bash ./$<

x86bics5wout : x86-64_wout_build_bics5.sh
        bash ./$<

x86gcov : x86-64_gcov_build.sh
        bash ./$<

x86gcovbics5 : x86-64_gcov_build_bics5.sh
        bash ./$<

asicgcov : bl3_asic_gcov_build.sh
        bash ./$<

burnin_asic_img : burnin_asic_build.sh
        bash ./$< DEV_BUILD NEW_BL2

burnin_asic_img_rel : burnin_asic_build.sh
        bash ./$< RELEASE_BUILD

burnin_e1s_asic_img : burnin_e1s_asic_build.sh
        bash ./$< DEV_BUILD NEW_BL2

burnin_e1s_asic_img_rel : burnin_e1s_asic_build.sh
        bash ./$< RELEASE_BUILD


asic_img : asic_build.sh
        bash ./$< DEV_BUILD NEW_BL2

asic_img_rel : asic_build.sh
        bash ./$< RELEASE_BUILD REL_BL2

bl1asic : bl1_asic_build.sh
        bash ./$<

asic_img_rel_wbl2 : asic_build.sh
        bash ./$< RELEASE_BUILD NEW_BL2

burnin_asic_img_rel_wbl2 : burnin_asic_build.sh
        bash ./$< RELEASE_BUILD NEW_BL2

burnin_e1s_asic_img_rel_wbl2 : burnin_e1s_asic_build.sh
        bash ./$< RELEASE_BUILD NEW_BL2


user4t:
        ./utility/prebuild.sh asic.wout.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.wout.bl3 SRC38 DEV_BUILD

user8t:
        ./utility/prebuild.sh asic.wout.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.wout.bl3 SRC76 DEV_BUILD

user4tb47:
        ./utility/prebuild.sh asic.wout.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.wout.bl3 SPC38 DEV_BUILD

user8tb47:
        ./utility/prebuild.sh asic.wout.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.wout.bl3 SPC76 DEV_BUILD

user8tb47e1:
        ./utility/prebuild.sh asic.wout.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.wout.bl3 SUC76 DEV_BUILD

user16t:
        ./utility/prebuild.sh asic.wout.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.wout.bl3 SPC15 DEV_BUILD

user4tv7:
        ./utility/prebuild.sh asic.wout.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.wout.bl3 SXC38 DEV_BUILD

user8tv7:
        ./utility/prebuild.sh asic.wout.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.wout.bl3 SXC76 DEV_BUILD

user8to4v7:
        ./utility/prebuild.sh asic.v78t_to_4t.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.v78t_to_4t.bl3 SXC76 DEV_BUILD

user16tv7:
        ./utility/prebuild.sh asic.wout.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.wout.bl3 SXC15 DEV_BUILD

user4tstv7:
        ./utility/prebuild.sh asic.shortstroke.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.shortstroke.bl3 SXC38 DEV_BUILD
user8tstv7:
        ./utility/prebuild.sh asic.shortstroke.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.shortstroke.bl3 SXC76 DEV_BUILD
user16tstv7:
        ./utility/prebuild.sh asic.shortstroke.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.shortstroke.bl3 SXC15 DEV_BUILD
user4tstb47:
        ./utility/prebuild.sh asic.shortstroke.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.shortstroke.bl3 SPC38 DEV_BUILD
user8tstb47:
        ./utility/prebuild.sh asic.shortstroke.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.shortstroke.bl3 SPC76 DEV_BUILD
user16tstb47:
        ./utility/prebuild.sh asic.shortstroke.bl3 DEV_BUILD
        ./utility/dobuild.sh asic.shortstroke.bl3 SPC15 DEV_BUILD

tools :
        python3 ./utility/packet_tools.py ./utility/scripts

#simulator E1S on U.2 Card
e1simu:
        ./e1simu_build.sh


# how to use make to commit and push together?
#       make git m="xxxxxx"
git:
        make clean
        git commit -m "$m"
        git push

clean:
        -rm -rf platform/obj/*
        -rm -rf *.assert_id
        -rm -rf *.elf
        -rm -rf *.map
        -rm -rf *.img
        -rm -rf *.bin
        -rm -rf *.asm
        -rm -rf *.strip.*
        -rm -rf *.signed
        -rm -rf *.signed.*
        -rm -rf *.append
        -rm -rf lib/libver_tmp.cpp
        -rm -rf inc/assert_id.h
        -rm -rf tcg/*def*
        -rm -rf utility/__pycache__
        -rm -rf utility/fwver.pyc
        -rm -rf SfxVuCommands.py
        -rm -rf makefile.myrtle_fw*
        -rm -rf ut_myrtle_fw*
        -rm -rf gcov_run_myrtle_fw*
        -rm -rf ./8ag/enum_strings/*
        -rm -rf myrtle*.burnin.nor*.out
        -rm -rf *.tar
        find -name *.deb | xargs rm -f
        find -name *.rpm | xargs rm -f

```

