```
meta data遇到了read err
> meta_data_start_pba: 655000        
> meta_data_end_pba:   655e80

[2-1970:1:1|0:0:0:941]    U0021425_READ_ERR:

>  _rd_usr: 0                         
>  _scheme: 0                         
>  _event: 1                          
>  _vt: 0

>  _retry_pass: 0                     
>  _pba: 655080 
>  _nand_addr: 200000001855           
>  _lp_bitmap: ffff

cur nandaddr 0x200000001855
pba : 0x655080

>  _ctrl: c000000                     
>  _ext_ctrl: 1ff00100                
>  _cpl_status: fff40ef               :e
>  _ext_cpl_status: c008000f

>  _cw_err_bmp: ffffffff              
>  _ccp_crc_err_bmp: ffff             
>  _lp_crc_err_bmp: 0                 
>  _lba_mis_bmp: 0

>  _dma_err_bmp: 0                    
>  _shim_err: 1                       
>  _rd_cmd_err: 0                     
>  _bad_pl_mask: 0

>  _lbas:

[0]:2000000000000  [1]:2000000000000  [2]:2000000000000  [3]:2000000000000  [4]:2000000000000  [5]:2000000000000  [6]:2000000000000  [7]:2000000000000

flash rd retry failed, do rebuild, do retry 1, cur vt 10

```

```
[2-1970:1:1|0:2:5:510]    U0021425_PARTIAL_BLK_HD_SEC_SCAN: > start_read_pba: 5370540            > end_read_pba: 5371000

Dump memory: addr: 0x438970000, len:0x18000(decimal:98304)

 0x0000000438970000:  06434d50 5b87a704 03a50b71 00000000 06434d50 b0b54b04 03a50b72 00000000

 0x0000000438970020:  06434d50 e9a4ef04 03a50b73 00000000 06434d50 e6d0f004 03a50b74 00000000

 0x0000000438970040:  f9556107 0a82ecd0 2d771feb 4c895ef6 00000000 00000000 00000000 00000000

 0x0000000438970060:  00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000


--> Msg:    orig nandaddr 0xffffffffffffffff, ctx ptr 0x7fff7a90, cur nandaddr 0x2c0000014970
pba : 0x5370680
pba : 0x5370684
@:/$ combpba 0 2 0 2 3 0 368 41 0 0 0

[0-0:0:0D|8:43:54:890]        bgops:                      pba : 0x5370688

@:/$
pba : 0x537068c
```

```c++
bool task_scheduler::reinit()
{
    task_array_element* pTaskArray = this->_ptask_array;
    INF_LOG("reinit task for core %d, high prio bitmap 0x%llx\n", this->_coreid, this->_high_priority_flag);
    this->_high_priority_flag = 0;
    this->_always_sched_flag = 0;

    for (u32 idx = 0; idx < MAX_TASK_NUMBER_IN_SCHEDULER; idx++) {
        // make sure the task is not null
        if ((pTaskArray[idx]._ptask) != nullptr) {
            // only check task that running in this scheduler
            if (pTaskArray[idx]._enabled_core == this->_coreid) {
                if (pTaskArray[idx]._always_schedule) {
                    BIT_SET(this->_always_sched_flag, idx);
                }
                if (pTaskArray[idx]._high_priority) {
                    BIT_SET(this->_high_priority_flag, idx);
                }
            }
        }
    }
    INF_LOG("after reinit, high prio bitmap 0x%llx\n", this->_high_priority_flag);
    return true;
}
```

```c++
void adjust_task_priority(task_base *tgt_task, u32 high_prio)
{
    u32 coreid = INVALID_32BIT;
    for (u32 taskid = 0; taskid < MAX_TASK_NUMBER_IN_SCHEDULER; taskid++) {
        task_base *ptask = task_tbl[taskid]._ptask;
        if (ptask == tgt_task) {
            task_tbl[taskid]._high_priority = high_prio;
            coreid = task_tbl[taskid]._enabled_core;
            break;
        }
    }
    if (coreid != INVALID_32BIT) {
        task_scheduler *psched = g_kernel.get_sched(coreid);
        psched->reinit();
    }
   else {
        INF_LOG("Can't find target task in task table!, addr %p\n", tgt_task);
    }
}

```

```
//B47A
#elif (OPN == SPC38)
	#define NAND_MANAGER	MICRON_B47R
    #define RAMDISK_TYPE RAMDISK_NONE
    #define NAND_TYPE MICRON_NAND_B47R_4T
    #define DDR_SIZE DDR_16GB
```

```
struct NOR_MAT_META_HDR_T {
    u8 sn[SN_OPN_MODEL_SINGLE_ENTRY_SIZE];
    u8 opn[SN_OPN_MODEL_SINGLE_ENTRY_SIZE];
    u8 model[SN_OPN_MODEL_SINGLE_ENTRY_SIZE];
    u8 hw_ver[SN_OPN_MODEL_SINGLE_ENTRY_SIZE];
    //TODO: adding the program count to META hdr
    //u32 hdr_program_cnt;
    
    u8* get_sn() {
        return sn;
    };

    u8* get_opn() {
        return opn;
    };

    u8* get_model() {
        return model;
    };
    
    NOR_MAT_META_HDR_T();
    void force_init();
};
```

```
bsp_platform::get_opn()
rdmatmeta
```

```
Dump memory: addr: 0x4389ee000, len:0x18000(decimal:98304)                                 
 0x00000004389EE000:  645ae349 a119e3b2 7473c043 7bf1bb86 00000000 00000000 00000000 00000000
 0x00000004389EE020:  00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 
 0x00000004389EE040:  00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 
 
 nandaddr 0x28000001084e
 pba : 0x424e480
 @:/$ parsepba 0x424e480
 
 plane1 pba : 0x424e484
 plane2 pba : 0x424e488
 @:/$ combpba 0 2 0 2 2 0 78 33 0 0 0                                                     
[0-0:0:0D|14:39:38:349]     io_task0:                     pba : 0x424e488
pba.all 0x424e480:                                                                         
ch:       2                                                                               
ce_lun:   2                                                                               
block:    33                                                                               
page:     78                                                                               
plane:    0                                                                               
offset:   0x0                                                                             
type:     0                                                                               
ep_idx:   0                                                                               
ccp_off:  0x0                                                                             
comp_len: 0x0
ln:       0x0                                                                             
ce:       0x2                                                                             
ch:       0x2     
```

```
0x24000000c852
pba : 0x3252280
pba.all 0x24000000c852:

ch:       1

ce_lun:   4

block:    0

page:     12

plane:    0

offset:   0x2

type:     1

ep_idx:   0

ccp_off:  0x0

comp_len: 0x9

unc:      0

ln:       0x1

ce:       0x0

ch:       0x1

```

```
@:/$ wrmatmeta 0 CSDU5SPC38A0 0 0
[4-2-2023:4:11|10:0:12:764]    write_mat_hdr_once[372] --> ERASE address [16777216] SUCCESS
[4-2-2023:4:11|10:0:12:764]    The write buffer address is 0x43d8aa000
[4-2-2023:4:11|10:0:12:764]    write_mat_hdr_once[409] --> The crc result is 630682160
[4-2-2023:4:11|10:0:12:765]    Write copy to 0 address, its write status is 0
[4-2-2023:4:11|10:0:12:776]    write_mat_hdr_once[372] --> ERASE address [16781312] SUCCESS
[4-2-2023:4:11|10:0:12:776]    Write copy to 1 address, its write status is 0
[4-2-2023:4:11|10:0:12:788]    write_mat_hdr_once[372] --> ERASE address [16785408] SUCCESS
[4-2-2023:4:11|10:0:12:788]    Write copy to 2 address, its write status is 0
[4-2-2023:4:11|10:0:12:788]    request hdr read success at load_mat_meta()
[4-2-2023:4:11|10:0:12:788]    from function get_one_valid_index_from_crc_bitmap[526] to load one vaild index[0]
[4-2-2023:4:11|10:0:12:788]    Load MAT Meta to class done!
[4-2-2023:4:11|10:0:12:788]    G0021966_SN_MN:
  > _sn:                 0
  > _mn:                 0
[4-2-2023:4:11|10:0:12:788]    SN -->> 0
[4-2-2023:4:11|10:0:12:788]    OPN -->> CSDU5SPC38A0
[4-2-2023:4:11|10:0:12:788]    MODEL -->> 0
[4-2-2023:4:11|10:0:12:788]    HW_VER -->> 0
[4-2-2023:4:11|10:0:12:788]    MAT Meta log write success

```

```
u32 start_time_us = bsp_platform::get_time_us();
_memcpy_time_cost[0] = bsp_platform::get_time_us() - start_time_us;
```

root@192.168.33.36:/home/tcnsh/junw/myrtle_bin/up.bin