```
u64            remain_len = len;
// maximum len of gdma_copy is 0x4000000, split to 0x4000000.
    while (remain_len > DATA_LEN_128K) {
        bsp_platform::gdma_copy(0, dest, g_gdma_temp_buf, DATA_LEN_128K);
        while (1) {
            // 0 means gdma is not busy
            if (bsp_platform::gdma_get_ch_status(0) == 0) {
                break;
            }
        }
        remain_len -= DATA_LEN_128K;
        dest += DATA_LEN_128K;
    }
    bsp_platform::gdma_copy(0, dest, g_gdma_temp_buf, remain_len);
    while (1) {
        // 0 means gdma is not busy
        if (bsp_platform::gdma_get_ch_status(0) == 0) {
            break;
        }
    }
```

```
		for(u64 addr = DDR_START + DATA_LEN_128K; addr < ddr_end; addr += DATA_LEN_128K) {
        	bsp_platform::gdma_copy(0, (void *)addr, (void *)DDR_START, DATA_LEN_128K);
			while (1) {
				// 0 means gdma is not busy
				if (bsp_platform::gdma_get_ch_status(0) == 0) {
					break;
				}
			}
		}
```

```
bool l2p_mgr::gdma_memset_l2_map()
{
    u64 copy_len = 0;
    // use all available channels to memset l2 map
    for (u32 ch = 0; ch < SYS_GDMA_TOTAL_CH && g_ckp_task.get_l2_remain_len() > 0; ++ch){
        if (bsp_platform::gdma_get_ch_status(ch) == 0) {
                copy_len = g_ckp_task.get_l2_remain_len() >= DATA_LEN_128K ? DATA_LEN_128K : g_ckp_task.get_l2_remain_len();
                bsp_platform::gdma_copy(ch, g_ckp_task.get_l2_init_buf(), g_gdma_temp_buf, copy_len);
                g_ckp_task.inc_l2_init_buf_offset(copy_len);
                g_ckp_task.dec_l2_remain_len(copy_len);
        }
    }
    if (g_ckp_task.get_l2_remain_len() == 0){
        // check if all channel's jobs are done, since we use an async interface
        for (u32 ch = 0; ch < SYS_GDMA_TOTAL_CH; ++ch){
            if (bsp_platform::gdma_get_ch_status(ch) != 0){
                return false;
            }
        }
        return true;
    }
    else
        return false;
}
```

```
    if (n > DATA_LEN_128K) {
        ASSERT(GDMA_COPY_SIZE_EXCEED_MAXIMUM_LEN, 0, "copy size %llu exceed maximum len of gdma_copy 128k\n", n);
    }
```

