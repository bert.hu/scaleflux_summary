```
/*
 * OPN define
 *
 */
#define C3BICS41T 1
#define C3BICS42T 2
#define C3BICS52T 3
#define C3BICS54T 4// haps only
#define C3SIMU800G 5
#define C36818 6
#define C3RAMDISK 7// for MAT flow, build for burnin image
#define C3B47A2T 8
#define C3SIMUBICS5800G 9
#define C3HOSTLOGGER 10 // fake opn for host logger, just to pass compile
#define CSDU5SRA38A0    11
#define CSDU4SRC1SA0    12
#define CSDU5SRC1SA0    13
#define SRC38           14
#define SRC76           15
#define SPC38           16
#define SPC76           17
#define SPC15           18
#define SUC76           19
```

```
[root@32_136_Bert myrtle_fw]# cat opnmap
#OPN likely CSDU5SPC38A0

opn_arry="
SRC38
SRC76
SPC38
SPC76
SPC15
"

dev_type_arry="
CSD
NSD
"

intf_type_arry="
U5
U4
E1
"

hw_type_arry="
C1
A0
M1
C2
"

// Form Factor & Board
//      P1	Cheetah, HHHL PCIe AIC x4 G3
// 		P2	Puma, HHHL PCIe AIC x4 G3
// 		U2	Lynx, U.2 PCIe x4 G3
// 		P3	Panther, HHHL PCIe AIC x4 G3 (KU+)
// 		U3	Leopard, U.2 PCIe x4 G3 (KU+)
// 		U4	Maui, U.2 PCIe x4 G4
// 		P4	HHHL PCIe AIC x4 G4 (Myrtle)
// 		P5	HHHL PCIe AIC x8 G4 (Myrtle)
// 		U5	Maui, U.3 PCIe x4 G4
// 		E1	E1.S 15mm Thick, PCIe x4 G4 (1C Connector)
// 		E2	E1.S 25mm Thick, PCIe x4 G4 (1C Connector)U4	Yes, In Dev	Maui, U.2 PCIe x4 G4
//      P4	HHHL PCIe AIC x4 G4 (Myrtle)
//      P5	HHHL PCIe AIC x8 G4 (Myrtle)
//      U5	U.3 PCIe x4 G4
//      E1	E1.S 15mm Thick, PCIe x4 G4 (1C Connector)
//      E2	E1.S 25mm Thick, PCIe x4 G4 (1C Connector)

// FPGA/SoC
//      A	Altera Stratix 5 90
// 		M	Xilinx KU 35
// 		N	Xilinx KU 40
// 		P	Xilinx KU60
// 		R	Xilinx KU15P
// 		S	Myrtle

// Flash
//      A	Micron L95B
// 		B	Micron L95B 5K
// 		C	Micron L06B MLC FortisFlash 3K
// 		D	Micron L06B eMLC FortisMax 10K
// 		E	Micron B16C eTLC FortisMax 5K
// 		F	Micron B17A eTLC FortisMax 10K
// 		G	Micron B27A eTLC FortisMax 10K
// 		H	Micron N28A QLC 1Tb
// 		J	Toshiba BICS3 TLC 256Gb 3K
// 		K	Production	Toshiba BICS3 eTLC 256Gb 7K
// 		L	Toshiba BICS3 TLC 512Gb 3K
// 		M	Toshiba BiCS4 eTLC 256Gb
// 		N	Intel N38A QLC 1Tb
// 		P	Micron B47R, eTLC FortisMax
// 		R	Toshiba BiCS5 eTLC 512Gb

// DRAM Capacity (CSD 3000+)
//      0-9	Yes	Host FTL (CSS 1000, CSD 2000) --> Use Host-FTL Decoding
// 		Z	No DRAM
// 		A	4GiB
// 		B	8Gib
// 		C	16GiB
// 		D	32GiB
// 		E	64GiB

// User Capacity (CSD 3000+)
//      16	1.6TB
// 		19	1.92TB
// 		20	2.0TB
// 		32	3.2TB
// 		38	3.84TB
// 		40	4.0TB
// 		64	6.4TB
// 		76	7.68TB
// 		80	8.0TB
// 		12	12.8TB
// 		15	15.36TB
// 		1S	16.0TB
// 		25	25.6TB
// 		30	30.72TB
// 		3T	32.0TB
// 		51	51.2TB
// 		61	61.44TB
// 		6F	64.0TB
```

MICRON_NAND_B47R_8T_E1

```
/*
 * NAND config, customer specific
 */

#if NAND_TYPE == MICRON_NAND_N28A_2T
    #include "cfg_2T_n28a.h"
#elif NAND_TYPE == TSB_NAND_BICS4_256Gb_2T
    #include "cfg_2T_bics4_etlc_256Gb.h"
#elif NAND_TYPE == TSB_NAND_BICS4_256Gb_1T
    #include "cfg_1T_bics4_etlc_256Gb.h"
#elif NAND_TYPE == TSB_NAND_BICS5_4PL_256Gb_2T
    #include "cfg_2T_bics5_etlc_256Gb.h"
#elif NAND_TYPE == TSB_NAND_BICS5_4PL_256Gb_4T_HAPS
    #include "cfg_4T_bics5_etlc_256Gb_haps.h"
#elif NAND_TYPE == TSB_NAND_BICS5_4PL_256Gb_4T
    #include "cfg_4T_bics5_etlc_256Gb.h"
#elif NAND_TYPE == TSB_NAND_BICS5_4PL_256Gb_8T
    #include "cfg_8T_bics5_etlc_256Gb.h"
#elif NAND_TYPE == TSB_NAND_BICS5_4PL_256Gb_16T
    #include "cfg_16T_bics5_etlc_256Gb.h"
#elif NAND_TYPE == TSB_NAND_BICS5_4PL_256Gb_1T
    #include "cfg_1T_bics5_etlc_256Gb.h"
#elif NAND_TYPE == MICRON_NAND_B47R_2T
    #include "cfg_2T_b47.h"
#elif NAND_TYPE == MICRON_NAND_B47R_4T
    #include "cfg_4T_b47.h"
#elif NAND_TYPE == MICRON_NAND_B47R_8T
    #include "cfg_8T_b47.h"
#elif NAND_TYPE == MICRON_NAND_B47R_8T_E1
    #include "cfg_8T_b47_e1.h"
#elif NAND_TYPE == MICRON_NAND_B47R_16T
    #include "cfg_16T_b47.h"
#elif NAND_TYPE == SIM_NAND_MODEL
    #if __ARM_6818__ == 1
        #include "cfg_32G_sim_nand.h"
    #else
        #include "cfg_2T_sim_nand_b47.h"
    #endif
#elif NAND_TYPE == SIM_NAND_MODEL_BICS5
    #include "cfg_2T_sim_nand_bics5.h"
#else
    #error "unsupported NAND type!"
#endif
```

```
#if (NAND_MANAGER == BICS5_4PL)
//dealy 15s for src76 will lead nand reset stuck
#define NAND_RESET_DELAY_US (10)
#else
#define NAND_RESET_DELAY_US (15)
#endif
```

E1.S build

```
buildType="DEV_BUILD"，"RELEASE_BUILD"
$BL2_NEW_BUILD = NEW_BL2

#4T
./utility/prebuild.sh asic.e1simu.bl2
./utility/dobuild.sh asic.e1simu.bl2 SPC38 $buildType
./utility/prebuild.sh asic.e1simu.bl3 $buildType
./utility/dobuild.sh asic.e1simu.bl3 SPC38 $buildType


#8T
./utility/prebuild.sh asic.e1simu.bl2
./utility/dobuild.sh asic.e1simu.bl2 SUC76 $buildType ONE SPC15
./utility/prebuild.sh asic.e1simu.bl3 DEV_BUILD
./utility/dobuild.sh asic.e1simu.bl3 SUC76 $buildType ONE SPC15
```

