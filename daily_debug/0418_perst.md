```
--- a/admin/nvme_perst_monitor.cpp
+++ b/admin/nvme_perst_monitor.cpp
@@ -197,14 +197,6 @@ u64 perst_monitor_task::run(void)
         last_pcie_link_status = pcie_link_status;
     }

-    // WA for MYR-4642, force change to D0 state
-    u8 reg_pmc;
-    reg_pmc = bsp_platform::reg_rd8(0x283210fc);
-    if ((reg_pmc & 0x3) != 0) {
-        reg_pmc &= 0xfc;
-        bsp_platform::reg_wr8(0x283210fc, reg_pmc);
-    }
-

```

