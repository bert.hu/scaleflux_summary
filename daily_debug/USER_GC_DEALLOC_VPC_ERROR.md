```
15875007 [7-2023:1:18|8:47:29:109]    U4020373_HANDLE_ASSERT:
15875008  > _coreid: 7                         > _assert_id: USER_GC_DEALLOC_VPC_ERROR > _cond: 0 == g_pu_mgr.pblk_mgr[0].get_sblock_valid_lpa_cnt(dealloc_blk) > _fileln: /home/tcnsh/jenkins_workspac         e/workspace/build-release/myrtle_fw/ftl/gc.cpp:470
15875009  > _msg: blk: 164, cur vpc 35174

```

```
https://scaleflux.atlassian.net/browse/MYR-6591
./utility/prebuild.sh asic.wout.bl3
./utility/dobuild.sh asic.wout.bl3 CSDU5SPC38M1 DEV_BUILD
./utility/dobuild.sh elog_parser CSDU5SPC38M1 DEV_BUILD

./utility/prebuild.sh asic.wout.bl3 RELEASE_BUILD
./utility/dobuild.sh asic.wout.bl3 CSDU5SPC38M1 RELEASE_BUILD

python3 DNLD_FWUpgradeByPFInReleaseBoard.py --baseFWImg /home/tcnsh/low.bin --newFWImg /home/tcnsh/up.bin

0xd0000000c28f
n2p
pba : 0xcdd340
e8f340

%！ xxd

0xd0000048028f
n2p
pba : 0xcdd340

> current_pf_sblock: 3

U3219141_PF_BLOCK_DEALLOCATE: > block: 3
U3219141_PF_BLOCK_GET_CANDIDATE: > block: 4a

U3219141_ADD_BLK_INTO_SLC_FIFO:
4413048  > _sblock: 3                         > _cur_pe_cnt: a9                    > _avg_pe_cnt: c1                    > _slc_fifo_cnt: 0

U3219141_ERASE_NORMAL:
4413052  > _cur_erase_blk: 3                  > _tot_free_blk_cnt: 17              > _free_fifo_blk_cnt: 14             > _max_erase_duration_us: 204f

U3219141_PF_DUMP_TASK_SAVE_RT_D:
> _nor_write_result_0: 0             
> _nor_write_result_1: 0             
> _rfs_write_result: ffffffff        
> _pf_cnt: 1b6
> _current_pf_sblock: 4a             
> _nor_sub_sector_idx: 5             
> _summary_data_pba: 12801340        
> _last_written_pba: 128017c0
> _user_data_start_pba: ffffffffffffffff 
> _user_data_end_pba: ffffffffffffffff 
> _parity_data_start_pba: ffffffffffffffff 
> _parity_data_end_pba: ffffffffffffffff
> _meta_data_start_pba: 12800800     
> _meta_data_end_pba: 12801300       
> _save_root_data_start_time_us: 1232 
> _duration_of_save_root_data_us: 137

newFWImg: /home/tcnsh/farm/samanea/Samanea_Lib/XScope/U4020373/myrtle_fw.asic.wout.bl3.release_build.csdu5spc38_U4020373.strip.elf.enc.signed.bin
baseFWImg: /home/tcnsh/farm/samanea/Samanea_Lib/XScope/U3219141/myrtle_fw.asic.wout.bl3.release_build.csdu5spc38m1_U3219141.strip.elf.enc.signed.bin

newFWVer: U4020373 baseFWVer: U3219141

从44升级到50的版本
dump的时候，是44的版本
_pf_meta_data_rw_buf_start
_pf_summary_data_rw_buf_start
_pf_root_data_rw_buf_start
_pf_padding_data_rw_buf_start
_pf_user_data_rw_buf_start

pf的buffer
44*64k + 64k + 4k + 4K + user

高版本降级到低版本
pf dump:
buffer
44*64k + 6*64k + 64k + 4k + 4K + user
对于64k的summary_data，有效数据的大小是随着PF_META_DATA_PBA_CNT变化的

buffer写到nand上
user
parity
padding到边界

meta 44*64k + 6*64k
summary 64k 高版本的summary
padding到边界

recovery的时候，是低版本
准备的buffer是：
44*64k + 64k + 4k + 4K + user
在pf restore的时候read_meta_data()
因为读取的是PF_META_DATA_PBA_CNT低版本小数量的数据，所以是没问题的，高版本中pf_meta_data中新增的数据被丢弃，此时的buffer中的数据也是没有问题的
这里是问题，读取的是高版本的summary，从中取的数据完整的64k，所以这里的summary是高版本的summary，存到了buffer里面，且这里高版本的多出的meta data是有有效数据的，这样在summary中是有pba记录的


然后继续从低版本升级到高版本
pf dump:
buffer：
44*64k + 64k + 4k + 4K + user
buffer写到nand上
user
parity
padding到边界

meta
summary 高版本的summary
padding到边界

recovery的时候，是高版本
准备的buffer是：
44*64k + 6*64k + 64k + 4k + 4K + user
在pf restore的时候read_meta_data()
因为读取的是summary中记录的pba，但读取的数据是PF_META_DATA_PBA_CNT高版本大数量的数据，但此时由于在低版本升级到高版本的时候，刚好pf block被deallocate掉了，旧的pf block被erase掉，但summary中的后半部分记录的是旧的无效pba，这个pba指向的是旧的pf block，导致读取的时候读到erase page


recovery的时候是50的版本，准备的buffer更大
pf_data_parser::read_summary_data()的时候，将g_pf_mgr.root_data.summary_data_pba的pba读取64k到g_pf_mgr._pf_summary_data_rw_buf_start

struct pf_summary_data {
    pf_mapping_info mapping_info[STREAM_ID_MAX];
    pf_parity_info parity_info[STREAM_ID_MAX];
    u64 meta_data_pba[PF_META_DATA_PBA_CNT];
} __attribute__((packed));

44*64k + 6*64k + 64k + 4k + 4K + user

写到block上的时候
user
parity
padding到边界

meta
summary
padding到边界

pf_data_parser::read_meta_data()
读取的是g_pf_mgr.summary_data.meta_data_pba[_sent_cnt]
将这个64k的内容读到
g_pf_mgr._pf_meta_data_rw_buf_start + (_sent_cnt * CFG_LUN_PAGE_SIZE * DATA_LEN_4K)
然后出错了
这时候会多读一些，因为_sent_cnt的上限从44到了50
但这个时候，由于buffer变大了

UD2214C0028M@CSDU5SPC38M1:/$ parsepba 0xcdd340                                     
pba.all 0xcdd340:                                                                  
ch:       13                                                                       
ce_lun:   0                                                                        
block:    3

g_pf_mgr.pf_block_init();


测试流程：
关闭RSA校验
nordisrsa 1 s@Y%7a2Ybvf#nLz6Ox

python3 Tool/Tool_TCGRevertByPSID.py
newFWVer: U4020389 baseFWVer: U4020389
Current FW support TCG, but base FW U3219141 not support
INFO newFWVer: U4020389 baseFWVer: U3219141
fail的这次
newFWVer: U4020389 baseFWVer: U3319572

sudo nvme fw-download /dev/nvme0n1 -f /home/tcnsh/up.bin -x 0x20000
sudo nvme fw-activate /dev/nvme0n1 -s 1 -a 1

meta_data_end_pba: 12401300
meta_data_start_pba: 12400800

0x418668a48
0x418669954
0x41866a860
= 0xF0C = 3852 (pf_mapping_info的大小)
pf_parity_info = 22个u32 = 88


> current_pf_sblock: 3
> meta_data_start_pba: c0b000
> meta_data_end_pba: c0bec0

0x00000000 0x00000000 0x00000000 0x00000000 0x00c0b000 0x00000000 0x00c0b040 0x00000000
0x00c0b080 0x00000000 0x00c0b0c0 0x00000000 0x00c0b100 0x00000000 0x00c0b140 0x00000000
0x00c0b180 0x00000000 0x00c0b1c0 0x00000000 0x00c0b200 0x00000000 0x00c0b240 0x00000000
0x00c0b280 0x00000000 0x00c0b2c0 0x00000000 0x00c0b300 0x00000000 0x00c0b340 0x00000000
0x00c0b380 0x00000000 0x00c0b3c0 0x00000000 0x00c0b400 0x00000000 0x00c0b440 0x00000000
0x00c0b480 0x00000000 0x00c0b4c0 0x00000000 0x00c0b500 0x00000000 0x00c0b540 0x00000000
0x00c0b580 0x00000000 0x00c0b5c0 0x00000000 0x00c0b600 0x00000000 0x00c0b640 0x00000000
0x00c0b680 0x00000000 0x00c0b6c0 0x00000000 0x00c0b700 0x00000000 0x00c0b740 0x00000000
0x00c0b780 0x00000000 0x00c0b800 0x00000000 0x00c0b840 0x00000000 0x00c0b880 0x00000000
0x00c0b8c0 0x00000000 0x00c0b900 0x00000000 0x00c0b940 0x00000000 0x00c0b980 0x00000000
0x00c0b9c0 0x00000000 0x00c0ba00 0x00000000 0x00c0ba40 0x00000000 0x00c0ba80 0x00000000
0x00c0bac0 0x00000000 0x00c0bb00 0x00000000 0x00c0bb40 0x00000000 0x00c0bb80 0x00000000
0x00c0bbc0 0x00000000 0x00c0bc00 0x00000000 0x00c0bc40 0x00000000 0x00c0bc80 0x00000000
0x00c0bcc0 0x00000000 0x00c0bd00 0x00000000 0x00c0bd40 0x00000000 0x00c0bd80 0x00000000
0x00c0bdc0 0x00000000 0x00c0be00 0x00000000 0x00c0be40 0x00000000 0x00c0be80 0x00000000
0x00c0bec0 0x00000000 0x00000000 0x00000000 0x00000000 0x00000000 0x00000000 0x00000000

11：02
sudo nvme fw-download /dev/nvme0n1 -f /home/tcnsh/low.bin -x 0x20000
sudo nvme fw-activate /dev/nvme0n1 -s 1 -a 0
sudo nvme fw-download /dev/nvme0n1 -f /home/tcnsh/low.bin -x 0x20000
sudo nvme fw-activate /dev/nvme0n1 -s 1 -a 1
sudo nvme fw-download /dev/nvme0n1 -f /home/tcnsh/low.bin -x 0x20000
sudo nvme fw-activate /dev/nvme0n1 -s 1 -a 2
sudo nvme fw-download /dev/nvme0n1 -f /home/tcnsh/low.bin -x 0x20000
sudo nvme fw-activate /dev/nvme0n1 -s 1 -a 3

11：07
sudo nvme fw-download /dev/nvme0n1 -f /home/tcnsh/up.bin -x 0x20000
sudo nvme fw-activate /dev/nvme0n1 -s 1 -a 0
sudo nvme fw-download /dev/nvme0n1 -f /home/tcnsh/up.bin -x 0x20000
sudo nvme fw-activate /dev/nvme0n1 -s 1 -a 1
sudo nvme fw-download /dev/nvme0n1 -f /home/tcnsh/up.bin -x 0x20000
sudo nvme fw-activate /dev/nvme0n1 -s 1 -a 2
sudo nvme fw-download /dev/nvme0n1 -f /home/tcnsh/up.bin -x 0x20000
sudo nvme fw-activate /dev/nvme0n1 -s 1 -a 3


newFWVer: U4020373 baseFWVer: U3219141
Update FW to U3219141 commit slot is 2 commit action is 3

57344 2023-01-14 00:26:05,772 INFO Check the FW info
57345 2023-01-14 00:26:05,913 INFO Init FW info is
57346 ----active_slot                   2
57347 ----next_reset_slot               2
57348 frs1                          U4020373
57349 frs2                          U4020373
57350 frs3                          U4020373
57351 frs4                          U4020373
57352 frs5
57353 frs6
57354 frs7
57355 2023-01-14 00:26:05,913 INFO Current FW info is
57356 ----active_slot                   2
57357 ----next_reset_slot               2
57358 frs1                          U4020373
57359 frs2                          U3219141
57360 frs3                          U4020373
57361 frs4                          U4020373

Update FW to U4020373 commit slot is 2 commit action is 3

```

```
relay：35.168
asic：32.110
```

```
224487 [2023-02-01 22:23:24] PF
224488 [2023-02-01 22:23:24] ps 0
224489 [2023-02-01 22:23:24] hs 1
224490 [2023-02-01 22:23:24] hf 22, 1
```

```
bad plane的信息
g_pu_mgr.pu_info[0][start_pba.block].total_bad_plane
u32 total_bad_plane = g_pu_mgr.bb_mgr.get_blk_ebb_cnt(sblock);

stream的信息
g_pu_mgr.pblk_mgr[0].get_sblock_stream(start_pba.block)

u32 full_page_total_parity_cnt = g_pu_mgr.pu_info[0][start_pba.block].parity_grp_cnt;
u32 full_page_die_num = include_parity ? CFG_DIE_NUM : (CFG_DIE_NUM - full_page_total_parity_cnt);
u32 full_page_plane_num = ((full_page_die_num * CFG_PLANE_NUM) - total_bad_plane);

初始化工作
void pu_mgr::init_block_dist(u8 pu_id, u32 sblock, u8 stream)

bb初始化的时候
bool load_bb = g_pu_mgr.bb_mgr.init_from_root_fs();
bool bad_block_manager::init_from_root_fs()->
void bad_block_manager::init_ebb_info(void)

bbdshowebb -s 73 -e 73
blksts 29


0x7400000
0x7400140


markbb 0x3402000 0
markbb 0xC10000 0
markbb 0xC106C0 0

1800

g_pu_mgr.bb_mgr.is_bad_die_blk(cdie, _recycling_blk)
g_pu_mgr.bb_mgr.is_bad_die_blk(pba.all)
g_pu_mgr.bb_mgr.is_bad_plane(die, blk, pl)

(gdb) p /x g_pf_mgr.root_data.meta_data_start_pba
$4 = 0x600800
block 3, page 1, die 0

(gdb) p /x g_pf_mgr.root_data.meta_data_end_pba
$5 = 0x601800
block 3, page 3, die 0

(gdb) p g_pu_mgr.pu_info[0][3].total_good_die
$1 = 30

(gdb) p g_pu_mgr.pu_info[0][3].good_die[0].raid_cmd
$2 = RAID_START

(gdb) p g_pu_mgr.pu_info[0][3].good_die[1].raid_cmd
$3 = RAID_XOR

(gdb) p g_pu_mgr.pu_info[0][3].good_die[28].raid_cmd
$22 = RAID_XOR
(gdb) p g_pu_mgr.pu_info[0][3].good_die[29].raid_cmd
$23 = RAID_PARITY

```

