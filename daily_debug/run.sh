#! /bin/bash

dev_list=("nvme0n1 nvme1n1 nvme2n1")

bs_list=("512B" "1K" "2k" "4k" "8k" "16k" "32k" "64k" "128k" "256k" "512k" "1M")
bs_list_len=${#bs_list[*]}
pattern_list=("read" "write" "randread" "randwrite" "randrw" "rw")
pattern_list_len=${#pattern_list[*]}
run_time=120

for((i=1;i<100;i++));do
    echo "==========Loop ${i}==========" >> test.log
    for dev in ${dev_list}
    do
        echo "=======================================================================" >> test.log
        echo "==========`date` start ${dev} test==========" >> test.log
        bs_1=${bs_list[$((RANDOM%bs_list_len))]}	
        qd_1=$((RANDOM%256+1))
        mix_1=$((RANDOM%100+1))
        pattern_1=${pattern_list[$((RANDOM%pattern_list_len))]}
	    bs_1="512B"
	    qd_1=256
	    pattern_1="read"	
        echo "Job1: dev=${dev},IO=${pattern_1},bs=${bs_1},qd=${qd_1},mix=${mix_1},loop=${i}" >> test.log

        bs_2=${bs_list[$((RANDOM%bs_list_len))]}
        qd_2=$((RANDOM%256+1))
        mix_2=$((RANDOM%100+1))
        pattern_2=${pattern_list[$((RANDOM%pattern_list_len))]}	
        	bs_2="256k"
		qd_2=43
		pattern_2="write"
	echo "Job2: dev=${dev},IO=${pattern_2},bs=${bs_2},qd=${qd_2},mix=${mix_2},loop=${i}" >> test.log

        bs_3=${bs_list[$((RANDOM%bs_list_len))]}
        qd_3=$((RANDOM%256+1))
        mix_3=$((RANDOM%100+1))
        pattern_3=${pattern_list[$((RANDOM%pattern_list_len))]}       
        echo "Job3: dev=${dev},IO=${pattern_3},bs=${bs_3},qd=${qd_3},mix=${mix_3},loop=${i}" >> test.log

        bs_4=${bs_list[$((RANDOM%bs_list_len))]}
        qd_4=$((RANDOM%256+1))
        mix_4=$((RANDOM%100+1))
        pattern_4=${pattern_list[$((RANDOM%pattern_list_len))]}       
        echo "Job4: dev=${dev},IO=${pattern_4},bs=${bs_4},qd=${qd_4},mix=${mix_4},loop=${i}" >> test.log
        echo "=======================================================================" >> test.log
        echo "" >> test.log

        fio  --name=myfio1 --numjobs=1 --rw=${pattern_1} --direct=1 --ioengine=libaio --bs=${bs_1} --iodepth=${qd_1} --runtime=${run_time} --rwmixread=${mix_1} --filename=/dev/${dev}  --time_based --group_reporting --sync=0 &
        fio  --name=myfio2 --numjobs=1 --rw=${pattern_2} --direct=1 --ioengine=libaio --bs=${bs_2} --iodepth=${qd_2} --runtime=${run_time} --rwmixread=${mix_2} --filename=/dev/${dev}  --time_based --group_reporting --sync=0 &
        fio  --name=myfio3 --numjobs=1 --rw=${pattern_3} --direct=1 --ioengine=libaio --bs=${bs_3} --iodepth=${qd_3} --runtime=${run_time} --rwmixread=${mix_3} --filename=/dev/${dev}  --time_based --group_reporting --sync=0 &
        fio  --name=myfio4 --numjobs=1 --rw=${pattern_4} --direct=1 --ioengine=libaio --bs=${bs_4} --iodepth=${qd_4} --runtime=${run_time} --rwmixread=${mix_4} --filename=/dev/${dev}  --time_based --group_reporting --sync=0 

        wait
    done
done
