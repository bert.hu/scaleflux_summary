https://scaleflux.atlassian.net/wiki/spaces/CST/pages/152011070/sfx-win-tool+Introduction#4.-Sfx-Tools-introduction

### **4.1 Update Nor Image**

```
sudo sfx-recovery --cmd "updateNorImg /dev/nvme0 -f xxx.bin"
```

VU “updateNorImg“ to update BL2 image

```
sudo sfx-recovery --cmd "updateNorImg /dev/nvme0 file=./bl2.signed.bin image=0"
```

VU “UpdateNorImg “ to update BL3 image in nor flash

```
sudo sfx-recovery --cmd "updateNorImg /dev/nvme0 file=./bl3.signed.bin image=1"
```

if the card is C type  need to execute this cmd to extend the timeout period  

```
sudo su -c "echo 240 >/sys/module/nvme_core/parameters/admin_timeout"
```



### **Factory Reset**

#### 4.2.1 CleanCard

```
sudo sfx-recovery --cmd "cleanCard /dev/nvme1"
```

#### 4.2.2.RebuildRFS

```
sudo sfx-recovery --cmd "rebuildRFS /dev/nvme1"
```

### **Boot Option**

```
sudo sfx-recovery --cmd  "setBootOption /dev/nvme1 slot=?"
```

#### 4.3.1 Boot to golden (set boot slot = 0)

#### 4.3.2 Boot to user mode (set boot slot = 1)

### **Drive Information**

#### 4.4.1 Version ShowInfo

```
The feature gets the same data as using sfx_tools_lib.py (Version().showInfo()) tool under Linux.
```

#### 4.4.2 Additional smart log

```
sudo sfx-nvme sfx smart-log-add /dev/nvme1
```

#### Query Capacity

```
sudo sfx-nvme sfx query-cap /dev/nvme1
```

#### TcgGetPsid

```
python3 sfx_tools_lib.py

>>TcgGetPsid(dev='/dev/nvme0').showInfo()
```

```
[root@localhost tcnsh]# sudo nvme get-log -i 0xcc -l 128 /dev/nvme0n1
Device:nvme0n1 log-id:204 namespace-id:0xffffffff
       0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
0000: 53 cc 53 cc 66 d2 80 00 53 63 61 6c 65 46 6c 75 "S.S.f...ScaleFlu"
0010: 78 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 "x..............."
0020: 00 00 00 00 00 00 00 00 30 00 00 00 00 00 00 00 "........0......."
0030: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 "................"
0040: 00 00 00 00 00 00 00 00 30 00 00 00 00 00 00 00 "........0......."
0050: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 "................"
0060: 00 00 00 00 00 00 00 00 43 53 44 55 35 53 50 43 "........CSDU5SPC"
0070: 33 38 41 30 00 00 00 00 00 00 00 00 00 00 00 00 "38A0............"
```

### fw update

```
nvme fw-download /dev/nvme0n1 --fw=<image absolute location> --xfer=0x2000
nvme fw-commit(fw-activate) /dev/nvme0n1 --slot=1 --action=1
Power cycle the server. 

sudo nvme fw-download /dev/nvme0n1  -f ./bl3.signed.bin  -x 0x2000
sudo nvme fw-commit /dev/nvme0n1 -s 1 -a 1
```

# How to get the sfx-tools for CSD3000 management

```
git checkout feature/MYR-818-customer-tools-rpm-deb-build
cd utility/scripts/customer_tools
make
```

# How to upgrade a union image

A union image is a image contains the bl2 (bootloader) and golden bl3 (production fw) and the PCIe/DDR config file. 

In the release directory download the asic_img.tar.gz and find the union image in the tarball. Current  DDR frequency is 2933MHz, so choose the 2933 version with matching OPN. 

**Use sfx-recovery tool mentioned in Q3 to upgrade the union image that stored in NOR flash:**

```
sudo sfx-recovery --cmd "updateNorImg /dev/nvme0n1 file=/home/tcn/myrtle_fw.asic.union.dev_build.csdu5spc38m1_U2516051.ddr_2933.signed.bin image=255"
```

After upgrading the union image, then upgrade the user image in root filesystem in NAND flash following the instruction in Q2. Then clean the drive by following command:

```
sudo sfx-recovery --cmd "cleanCard /dev/nvme0n1"
```

Then power cycle the server to make the new FW and PCIe/DDR config effective. 

# How to get event log on drive and parse the event log

Get the event log stored on NAND flash by following command:

```
sudo sfx-nvme sfx evt-log-dump /dev/nvme0n1 --file=/home/tcn/eventlog.raw
```

And parse the log by following command, so it can be read as a text file. 

```
./myrtle_fw.elog_parser.dev_build.csdu5spc38m1.elf eventlog.raw >eventlog.parsed
```

# How to recover a drive if enter read-only or read/write lock mode

```
sudo sfx-recovery --cmd "cleanCard /dev/nvme0n1"
```

# How to update the pcie cfg or ddr cfg.

Usually we don't have to update these two files separately, because they are included in union image. But update union image  can take a long time.

Update it in a similar way as union.

```
sudo sfx-recovery --cmd "updateNorImg /dev/nvme0 file=./pcie_maui.out image=2"
```

`image=2` is used for pcie cfg,`image=3`is used for ddr cfg.

Then power cycle the server to make the  PCIe/DDR config effective.



### Change Capacity with Thin Provisioned Namespaces

An example, extend a 3.84TB drive to 7.68T with sector size 4096:

```
sudo nvme delete-ns /dev/nvme0 --namespace-id=0xffffffff
sudo nvme ns-rescan /dev/nvme0
sudo nvme create-ns /dev/nvme0 --nsze=1875366486 --ncap=937684566 --block-size=4096  
sudo nvme attach-ns /dev/nvme0 --namespace-id=1 --controllers=0
sudo nvme ns-rescan /dev/nvme0
```

#### Conventional Reset(hot reset)：

```
1. Check PCIe link address：0000:01:00.0
[root@localhost ~]# lspci |grep No
01:00.0 Non-Volatile memory controller: ScaleFlux Inc. Device 0010

2. Get PCIe bridge address：0000:00:01.0
[root@localhost ~]# ll /sys/bus/pci/devices/0000\:01\:00.0
lrwxrwxrwx. 1 root root 0 Jun 6 14:10 /sys/bus/pci/devices/0000:01:00.0 -> ../../../devices/pci0000:00/0000:00:01.0/0000:01:00.0

3. trigger hot reset:
sudo setpci -s 0000:00:01.0 3E.b=0x42
sudo setpci -s 0000:00:01.0 3E.b=0x02

4. PCIe remove/rescan:
echo 1 >/sys/bus/pci/devices/0000\:01\:00.0/remove
echo 1 >/sys/bus/pci/rescan
```

#### PCIe link down/up:

```
1. Check PCIe link address：0000:01:00.0
[root@localhost ~]# lspci |grep No
01:00.0 Non-Volatile memory controller: ScaleFlux Inc. Device 0010

2. Get PCIe bridge address：0000:00:01.0
[root@localhost ~]# ll /sys/bus/pci/devices/0000\:01\:00.0
lrwxrwxrwx. 1 root root 0 Jun 6 14:10 /sys/bus/pci/devices/0000:01:00.0 -> ../../../devices/pci0000:00/0000:00:01.0/0000:01:00.0

3. trigger pcie link down/up：
sudo setpci -s 0000:00:01.0 CAP_EXP+10.b=0x50
sudo setpci -s 0000:00:01.0 CAP_EXP+10.b=0x60

4. PCIe remove/rescan:
echo 1 >/sys/bus/pci/devices/0000\:01\:00.0/remove
echo 1 >/sys/bus/pci/rescan
```

#### Function Level Reset:

```
1. Check PCIe link address：0000:01:00.0
[root@localhost ~]# lspci |grep No
01:00.0 Non-Volatile memory controller: ScaleFlux Inc. Device 0010

2. trigger FLR
echo 1 > /sys/bus/pci/devices/0000:02:00.0/reset
```

#### Controller Reset (CC.EN transitions from ‘1’ to ‘0’):

```
1. trigger controller reset：
nvme reset /dev/nvme0
```

#### NVM Subsystem Reset:

```
1. trigger subsystem reset：
nvme subsystem-reset /dev/nvme1
```

