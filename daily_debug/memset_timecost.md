@:/$ [4-2-2023:7:14|9:13:59:998]    fwver:

  > build_time:            Jul 14 2023 17:10:58
  > origin_info:           git@bitbucket.org:sfxsw/myrtle_fw.git
  > branch_info:           HEAD -> feature/MYR-8452-todo-handling-and-log-enhance
  > hash_id:               466aaa303db26d9ae0671fb6e1c9d8dc7076e622
  > author:                CollinYang <collin.yang@scaleflux.com>
  > time:                  2023-07-13 13:39:31 +0800
  > fw_version:            vU.0.0@466aaa_23584
  > idfy_ver:              U0023584
  > build_type:            DEV
  > opn:                   CSDU5SPC76M1
  > hw pcb_ver:            M11
  > hw fbid:               5
  > runmode:               USER_MODE
  > ddr_freq:              **2933MHz**
  > ddr_size:              16GB
  > raw_cap:               8192GiB
  > default_provision_cap: 7680GB
  > default_format_cap:    7680GB
  > default_max_lba:       0x37e3e92af
  > default_real_op:       6%
  > sys pll_clk:           **225MHZ**
  > cpu pll_clk:           **250MHZ**
  > vdd:                   0.83v
  > vcc:                   disable
  > vpp:                   disable
  > pcie_pba:              0x9000
  > bl2_version:           B0023124@3d8a1
  > latest_compatible_ver: 16893

```
[4-2-2023:7:14|9:12:31:949]    memset 8M tasks 945 us
[4-2-2023:7:14|9:12:31:950]    flush_dcache 8M tasks 718 us
```

@:/$ [4-2-2023:7:14|9:20:58:182]    fwver:

  > build_time:            Jul 14 2023 17:10:58
  > origin_info:           git@bitbucket.org:sfxsw/myrtle_fw.git
  > branch_info:           HEAD -> feature/MYR-8452-todo-handling-and-log-enhance
  > hash_id:               466aaa303db26d9ae0671fb6e1c9d8dc7076e622
  > author:                CollinYang <collin.yang@scaleflux.com>
  > time:                  2023-07-13 13:39:31 +0800
  > fw_version:            vU.0.0@466aaa_23584
  > idfy_ver:              U0023584
  > build_type:            DEV
  > opn:                   CSDU5SPC76M1
  > hw pcb_ver:            M11
  > hw fbid:               5
  > runmode:               USER_MODE
  > ddr_freq:              1600MHz
  > ddr_size:              16GB
  > raw_cap:               8192GiB
  > default_provision_cap: 7680GB
  > default_format_cap:    7680GB
  > default_max_lba:       0x37e3e92af
  > default_real_op:       6%
  > sys pll_clk:           225MHZ
  > cpu pll_clk:           250MHZ
  > vdd:                   0.83v
  > vcc:                   disable
  > vpp:                   disable
  > pcie_pba:              0x9000
  > bl2_version:           B0023124@3d8a1
  > latest_compatible_ver: 16893

```
[4-2-2023:7:14|9:22:27:291]    memset 8M tasks 7183 us
[4-2-2023:7:14|9:22:27:296]    flush_dcache 8M tasks 5607 us
```

@:/$ [4-2-2023:7:14|9:28:44:577]    fwver:

  > build_time:            Jul 14 2023 17:10:58
  > origin_info:           git@bitbucket.org:sfxsw/myrtle_fw.git
  > branch_info:           HEAD -> feature/MYR-8452-todo-handling-and-log-enhance
  > hash_id:               466aaa303db26d9ae0671fb6e1c9d8dc7076e622
  > author:                CollinYang <collin.yang@scaleflux.com>
  > time:                  2023-07-13 13:39:31 +0800
  > fw_version:            vU.0.0@466aaa_23584
  > idfy_ver:              U0023584
  > build_type:            DEV
  > opn:                   CSDU5SPC76M1
  > hw pcb_ver:            M11
  > hw fbid:               5
  > runmode:               USER_MODE
  > ddr_freq:              2666MHz
  > ddr_size:              16GB
  > raw_cap:               8192GiB
  > default_provision_cap: 7680GB
  > default_format_cap:    7680GB
  > default_max_lba:       0x37e3e92af
  > default_real_op:       6%
  > sys pll_clk:           225MHZ
  > cpu pll_clk:           250MHZ
  > vdd:                   0.83v
  > vcc:                   disable
  > vpp:                   disable
  > pcie_pba:              0x9000
  > bl2_version:           B0023124@3d8a1
  > latest_compatible_ver: 16893

```
[4-2-2023:7:14|9:29:12:855]    memset 8M tasks 6991 us
[4-2-2023:7:14|9:29:12:861]    flush_dcache 8M tasks 5577 us
```

