```
newFWImg: /home/tcnsh/farm/Asic-G4-112.190/samanea/Samanea_Lib/XScope/U0022759/myrtle_fw.asic.wout.bl3.dev_build.src76_U0022759.strip.elf.enc.signed.bin 
baseFWImg: /home/tcnsh/farm/Asic-G4-112.190/samanea/Samanea_Lib/XScope/U0022759/myrtle_fw.asic.wout.bl3.dev_build.src76_U0022759.strip.elf.enc.signed.bin
2023-06-02 06:10:16,025 INFO newFWVer: U0022759 baseFWVer: U0022759
```

```
469488 >> loading section body to memory and loading string and symbol tables
469489 copy section from 0x410720100 to 0x7fe00000, size 4096
469490 copy section from 0x410721100 to 0x400001000, size 2981888
469491 copy section from 0x4109f9100 to 0x4002d9000, size 393216
469492 copy section from 0x410a59100 to 0x400339000, size 537808
469493 copy section from 0x410adc5d0 to 0x4003bc4d0, size 912
469494 >> Relocation finished

```

```
2023-06-20 12:03:15,322 INFO Step4: Send async active fw immediately command
2023-06-20 12:03:15,322 INFO Send Firmware Activate Command
2023-06-20 12:03:15,322 INFO Step5: Sleep 5.10s and send PF
INFO Step5: Sleep 5.20s and send PF
```

```c++
void fw_commit_cmd::diag_state(void)
{
    EVT_LOG(fw_commit_state_evtlog, _state, _immed_state, _reset_active_state, _action, _cur_slot, _fw_slot, _ctag,
            _act_idx, _nor_job_id, polling_b2n_use_up());
}
```

python3 DNLD_PFDuringFWCommit.py --newFWImg /home/tcnsh/junw/myrtle_bin/up.bin --baseFWImg /home/tcnsh/junw/myrtle_bin/up.bin

scp myrtle_fw.asic.wout.bl3.dev_build.spc76_vU.0.0@*.strip.elf.signed.bin root@192.168.38.29:/home/tcnsh/junw/myrtle_bin/up.bin

scp myrtle_fw.host_logger.dev_build.c3hostlogger_vU.0.0@9f9f74_23125.elf root@192.168.38.29:/home/tcnsh/junw/myrtle_bin/host_logger.elf



```
sudo sfx-recovery --cmd "updateNorImg /dev/nvme0n1 file=/home/tcnsh/U23124/myrtle_fw.asic.wout.union.dev_build.spc76_U0023124.strip.elf.ddr_2933.signed.bin image=255"
python3 DNLD_PFDuringFWCommit.py --newFWImg /home/tcnsh/junw/myrtle_bin/up.bin --baseFWImg /home/tcnsh/junw/myrtle_bin/up.bin
```

