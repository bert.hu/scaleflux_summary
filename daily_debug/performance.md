========================Start analyze the test data===========================
WARNING...........In 128kB,Sequential,write,1,128 test value is 2718.781, baseline is 4100, gap is -33%
WARNING...........In 4kB,Sequential,write,1,1 test value is 10, baseline is 8, gap is -25%
WARNING...........In 128kB,Sequential,read,1,128 test value is 3569.558, baseline is 7100, gap is -49%
WARNING...........In 4kB,Sequential,read,1,1 test value is 72, baseline is 8, gap is -800%
WARNING...........In 4kB,Random,write,4,128 test value is 115, baseline is 150, gap is -23%
WARNING...........In 4kB,Random,read,4,128 test value is 804, baseline is 1250, gap is -35%
WARNING...........In 4kB,Random mix-70-30,write,4,128 test value is 87, baseline is 117, gap is -25%
WARNING...........In 4kB,Random mix-70-30,read,4,128 test value is 205, baseline is 273, gap is -24%
WARNING...........In 4kB,Random,write,1,1 test value is 14, baseline is 10, gap is -40%
In 4kB,Random,read,1,1 test value is 76, baseline is 70, gap is -8%.



sudo sh basic_items_test.sh --device nvme0n1 --runtime 1800 --comp_ratio 1

Seq read 128k 1job qd128

```
echo "==========`date`: Seq read 128k 1job qd128 Start=========="
iostat -xmdct $d 1 > iostat_128kB_seq_read_1job_QD128.log &
iostat_pid=$!
sudo fio --percentile_list=10:20:30:40:50:60:70:80:90:99:99.9:99.99:99.999:99.9999:99.99999:99.999999:99.9999999 --ioengine=libaio --direct=1 --norandommap --randrepeat=0 --log_avg_msec=1000 --group_reporting --buffer_compress_percentage=$comp_ratio --buffer_compress_chunk=4k --filename=/dev/$d --name=128kB_seq_read_1job_QD128 --rw=read  --bs=128k --numjobs=1 --iodepth=128 --ramp_time=$ramp_time --time_based --runtime=$runtime --minimal >> fio_data.log
CheckError
kill $iostat_pid
echo "==========`date`: Seq read 128k 1job qd128 End=========="
```

```
sudo fio --percentile_list=10:20:30:40:50:60:70:80:90:99:99.9:99.99:99.999:99.9999:99.99999:99.999999:99.9999999 --ioengine=libaio --direct=1 --norandommap --randrepeat=0 --log_avg_msec=1000 --group_reporting --buffer_compress_percentage=1 --buffer_compress_chunk=4k --filename=/dev/nvme0n1 --name=128kB_seq_read_1job_QD128 --rw=read  --bs=128k --numjobs=1 --iodepth=128 --ramp_time=10s --time_based --runtime=1800 --minimal >> seq_read.log
```

mio 1010 mr 50469 w     0 rc   15598 wc       0 qd  128 len 32 de 1 0 0 0 intr 1 0000 rp 1 co 1 loc 0 noc 0 0 rb 0

```
U0022993_FIO_STATUS: 
  > _qd_estimate:        80
  > _is_rd:              true
  > _is_wr:              false
  > _is_seq_rd:          true
  > _is_seq_wr:          false
  > _fourk_seq_write_flag: 0
  > _single_io_core:     1
  > _fixed_req_len:      1
  > _prev_len:           20
  > g_delay_limit:       0
  > g_split_credit:      true
  > g_one_qd_delay:      0
  > g_delay_active:      0
  > _intr_switch:        true
  > _intr_reg:           61a80000
```

