exception信息

```
ERROR:
!!!Enter Exceptions!!! Core:5
Core 5 - Exception lr:0x4002645a8, Exception esr:0x96000021 far:0x100421ee41087d1
daif:0x3c0, curEL:0xc, spSel:0x1, vbar:0x4002e9000, sp:0x7fffbd70
DDR freq:2666, ECC status: 0x0
cpu context addr: 0x403a2aa20

GP Regs:
  x0:   0x0000000402fc02d8, x1:   0x0100421ee41087b9
  x2:   0x0000000000000000, x3:   0x000000040026d49c
  x4:   0x0000000000000000, x5:   0x0000000000015000
  x6:   0x0000000000000000, x7:   0x0000000000000000
  x8:   0x000000040800a810, x9:   0x00000000005bca04
  x10:  0x0000000000000000, x11:  0x00000000000079ac
  x12:  0x00000000deadbeef, x13:  0x0000000000000000
  x14:  0x0000000000000000, x15:  0x0000000000000000
  x16:  0x0000000000000000, x17:  0x0000000000000000
  x18:  0x0000000000000000, x19:  0x00000004003cab40
  x20:  0x00000004039f2740, x21:  0x000000040d174440
  x22:  0x0000000000000001, x23:  0x00000004003e66c0
  x24:  0x00000000ffffffff, x25:  0x0000000000000010
  x26:  0x0000000000000005, x27:  0x00000004003cabb8
  x28:  0x000000040d174400, x29:  0x000000007ffffcc0

Sys Regs:
  spsr: 0x000000008000000d, elr:  0x000000040026a060
  sctlr:0x0000000030c5183f, actlr:0x0000000000000000
  scr:  0x000000000000043e, esr:  0x0000000096000021
  ttbr0:0x0000000403aa0000, rvbar:0x0000000000000000
  mair: 0x000000ff440c0400, amair:0x0000000000000000
  tcr:  0x0000000080826518, tpidr:0xe8add7dfbd480e55
  afsr0:0x0000000000000000, afsr1:0x0000000000000000
  far:  0x0100421ee41087e9, vbar: 0x00000004002e9000

Exception Syndrome Register: EC code:0x25, iss:0x21


backtrace:core(5)
  0: 0x4000026c0
  1: 0x40028a4d4
  2: 0x40028a76c
  3: 0x4002e922c
  4: 0x4002632f0    4002632f0:   94000460        bl      400264470 <_ZN14task_scheduler14pause_all_taskE12PAUSE_TYPE_E>
  5: 0x400263a6c
  6: 0x40024207c
  7: 0x40000141c
  8: 0x400001664
  9: 0x4000011c8


[4-2-1970:1:1|0:0:4:38]    fwver:
  > build_time:            Jul 28 2023 14:29:08
  > origin_info:           git@bitbucket.org:sfxsw/myrtle_fw.git
  > branch_info:           HEAD  origin/release/rc_4.6.0  release/rc_4.6.0
  > hash_id:               cda9c44c89271782b236026d5eb9ec25e5f507a2
  > author:                Xin Li <xin.li@scaleflux.com>
  > time:                  2023-07-27 09:18:10 +0000
  > fw_version:            vU.6.0@cda9c4_23590
  > idfy_ver:              U6023590
  > build_type:            RELEASE
  > opn:                   CSDU5SXC76C3
  > hw pcb_ver:            A21E004100
  > hw fbid:               14
  > runmode:               USER_MODE
  > ddr_freq:              2666MHz
  > ddr_size:              16GB
  > raw_cap:               4096GiB
  > default_provision_cap: 3840GB
  > default_format_cap:    3840GB
  > default_max_lba:       0x1bf1f72af
  > default_real_op:       6%
  > sys pll_clk:           2250MHZ
  > cpu pll_clk:           1500MHZ
  > vdd:                   0.83v
  > vcc:                   enable
  > vpp:                   enable
  > pcie_pba:              0x9000
  > bl2_version:           B6023550@03e73
  > latest_compatible_ver: 16893

```

先看Exception esr:0x96000021

ESR的描述可参考：

[Arm Armv8-A Architecture Registers](https://developer.arm.com/documentation/ddi0595/2021-06/AArch64-Registers/ESR-EL1--Exception-Syndrome-Register--EL1-?lang=en#fieldset_0-24_0_15)

1）esr解析网址：

[AArch64 ESR decoder (arm64.dev)](https://esr.arm64.dev/)

<https://esr.arm64.dev/>

# AArch64 ESR decoder

[Source and command-line version](https://github.com/google/aarch64-esr-decoder)

ESR: 

| 0                                        | 0                             | 0                                        | 0                          | 0                                        | 0            | 0         | 0         | 9            | 6                                   | 0                   | 0    | 0    | 0    | 2    | 1    |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| ---------------------------------------- | ----------------------------- | ---------------------------------------- | -------------------------- | ---------------------------------------- | ------------ | --------- | --------- | ------------ | ----------------------------------- | ------------------- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| 0                                        | 0                             | 0                                        | 0                          | 0                                        | 0            | 0         | 0         | 0            | 0                                   | 0                   | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 1    | 0    | 0    | 1    | 0    | 1    | 1    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 0    | 1    | 0    | 0    | 0    | 0    | 1    |
| RES0: 0x0000000 0b000000000000000000000000000 | ISS2: 0x00 0b00000            | EC: 0x25 0b100101                        | IL: true                   | ISS: 0x0000021 0b0000000000000000000100001 |              |           |           |              |                                     |                     |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
|                                          |                               | Data Abort taken without a change in Exception level | 32-bit instruction trapped |                                          |              |           |           |              |                                     |                     |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
|                                          | ISV: false                    | RES0: 0x000 0b0000000000                 | VNCR: false                | RES0: 0x0 0b00                           | FnV: false   | EA: false | CM: false | S1PTW: false | WnR: false                          | DFSC: 0x21 0b100001 |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
|                                          | No valid instruction syndrome |                                          |                            |                                          | FAR is valid |           |           |              | Abort caused by reading from memory | Alignment fault.    |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |

可以看到是  Alignment fault.

地址非8byte对齐造成的访问异常

far（Fault Address Register）失效地址寄存器，即访问的该地址导致了exception

far:  0x0100421ee41087e9，说明在访问0x0100421ee41087e9时异常，该地址应该为非法地址，大概率为踩内存

再看elr:  0x000000040026a060

找到对应的asm文件,查找40026a060

找到exception的位置在

```
   40026a05c:   f9400021        ldr     x1, [x1]
   40026a060:   f9401821        ldr     x1, [x1, #48]
   40026a064:   d63f0020        blr     x1
   40026a068:   34000420        cbz     w0, 40026a0ec <_ZN15pf_data_flusher15save_debug_dataEj+0x66c>
```

先找到对应的elf文件，gdb ./myrtle_fw.asic.v78t_to_4t.bl3.release_build.sxc76.elf

**disassemble/s pf_data_flusher::save_debug_data**

找到对应位置

```
/home/tcnsh/jenkins_workspace/workspace/build-release/myrtle_fw/pfail/pf_dump.cpp:
1551    /home/tcnsh/jenkins_workspace/workspace/build-release/myrtle_fw/pfail/pf_dump.cpp: No such file or directory.
   0x000000040026a054 <+1492>:  ldr     x1, [x27]
   0x000000040026a058 <+1496>:  mov     x0, x1
   0x000000040026a05c <+1500>:  ldr     x1, [x1]
   0x000000040026a060 <+1504>:  ldr     x1, [x1, #48]
   0x000000040026a064 <+1508>:  blr     x1
   0x000000040026a068 <+1512>:  cbz     w0, 0x40026a0ec <pf_data_flusher::save_debug_data(unsigned int)+1644>
```

在1551行

```c++
if (TASK_PAUSED != task_tbl[i]._ptask->check_status()) {
  BIT_SET(debug_data->task_paused, i);
}
```

再看X1的值，x1:   0x0100421ee41087b9

ldr     x1, [x27]，以及x27的值，x27:  0x00000004003cabb8

```
(gdb) p /x &task_tbl
$1 = 0x4003cab40
```

task_array_element task_tbl[MAX_TASK_NUMBER_IN_SCHEDULER]

```
(gdb) p sizeof(task_array_element)
$2 = 24
```

我们可以看到exception的时候，x27:  0x00000004003cabb8，距离task_tbl的首地址是120个字节，120/24=5

```
(gdb) p /x &task_tbl[5]
$3 = 0x4003cabb8
```

