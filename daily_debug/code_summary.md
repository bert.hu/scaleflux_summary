1. DEV_BUILD和RELEASE_BUILD的开关闭打印的位置

   ```c++
   void nor_mat_support::load_footer_to_class(u32 index)
   {
       u32 *data    = (u32 *)nor_mat_footer_read_ddr_addr[index];
       _meta_footer = *((NOR_MAT_META_FOOTER_T *)data);
       INF_LOG("MAT footer info is loaded to ddr\n");
       INF_LOG("mat version --> %u\n", _meta_footer.mat_meta_version);
       INF_LOG("mat count --> %u\n", _meta_footer.mat_count);
       INF_LOG("mat clean card count --> %u\n", _meta_footer.clean_card_count);
       INF_LOG("gbblk count --> %u\n", _meta_footer.burnin_bb_count);
       INF_LOG("ext meta addr --> %u\n", _meta_footer.ext_meta_addr);
       INF_LOG("time_stamp --> %u\n", _meta_footer.time_stamp);
       INF_LOG("log_switch --> 0x%X\n",_meta_footer.log_switch);
       INF_LOG("format trigger cnt --> 0x%X\n",_meta_footer.format_trigger_mat);
       INF_LOG("mat ewr retest cnt --> %d\n",_meta_footer.mat_ewr_retest_cnt);
       INF_LOG("mat ewr rc --> %d\n",_meta_footer.get_mat_ewr_rc());
       INF_LOG("mat_gbb_pre_process_rc --> %d\n",_meta_footer.get_mat_pro_gbb_rc());
       if(_meta_footer.log_switch == LOG_OFF_VALUE){
           disable_logger();
           printf("\n\tDisable INF_LOG\n");
       }
       else if(_meta_footer.log_switch == LOG_ON_VALUE){
           enable_logger();
           MAT_LOG("\n\t\tEnable BL3 INF_LOG\n"); 
       }
       else{
           if(BUILD_TYPE==DEV_BUILD){
               enable_logger();
               MAT_LOG("DEV_BUILD enable BL3 INF_LOG\n"); 
           }
           else if(BUILD_TYPE==RELEASE_BUILD){
               disable_logger();
               printf("\n\tRELEASE BUILD disable INF_LOG\n");
           }
       }
       
       _is_footer_loaded = true;
   }
   ```

   ​

2. MAT_META_FOOTER的nor的地址是nor_mat_footer_read_ddr_addr为CFG_NOR_DDR_MAT_META_FOOTER_READ_START，CFG_NOR_DDR_MAT_META_FOOTER_READ_START + DATA_LEN_4K，CFG_NOR_DDR_MAT_META_FOOTER_READ_START + 2*DATA_LEN_4K，每块的大小是128bytes

3. 对应的mat meta vu

   ```c++
   EA2309G0011M@CSDE1SUC76:/$ rdmatmeta
       mat meta hdr nor address 16777216
       load footer from ddr addr 0x43bcc5000 from function request_mat_footer_read[1701] --> gbb count from footer class member[0]
       mat_version -->> 1
       mat_count -->> 3
       mat_ext_addr -->> 3452816845
       gbb_count -->> 0
       time_stamp -->> 3452816845
       request hdr read and hdr is already loaded to ddr
       SN -->> EA2309G0011M
       OPN -->> CSDE1SUC76
       MODEL -->> CSD-3310
       HW_VER -->> A11
       evt log flush ready, size 64k
   ```

4. main函数里面来load mat meta

   ```c++
   g_nor_mat_support.mat_meta_load(true);
   ```

   ​

5. ​