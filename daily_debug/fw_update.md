```
[2023-05-30 15:57:19.556] [4-2-2023:5:30|7:57:18:946]    >>> WARNING: Task: admin_task, SMP: 0x10000, State: 3, Time cost: 30261 us, Exceed allowed exec time!
[2023-05-30 15:57:19.614] [4-2-2023:5:30|7:57:18:947]    Validate FW image, file header: ver 0x57c3 len 0x3bdfb0 crc 0xb2608df8
[2023-05-30 15:57:19.614] [4-2-2023:5:30|7:57:18:947]    OPN of FW image: CSDE1SPC76M1
[2023-05-30 15:57:19.614] [4-2-2023:5:30|7:57:18:956]    U0022739_FW_IMG_INFO:
[2023-05-30 15:57:19.614]   > _major_ver:          0
[2023-05-30 15:57:19.614]   > _minor_ver:          57c3
[2023-05-30 15:57:19.614]   > _build_type:         2
[2023-05-30 15:57:19.614]   > _bl_type:            3
[2023-05-30 15:57:19.614]   > _img_len:            3bdfb0
[2023-05-30 15:57:19.614]   > _crc32:              b2608df8
[2023-05-30 15:57:19.614]   > _opn:                CSDE1SPC76M1
[2023-05-30 15:57:19.614]   > _compatible_ver:     16893
[2023-05-30 15:57:19.669] [4-2-2023:5:30|7:57:18:960]    detect feature tcg support
[2023-05-30 15:57:19.669] [4-2-2023:5:30|7:57:18:960]    detect feature big-map-unit support
[2023-05-30 15:57:19.670] [4-2-2023:5:30|7:57:18:960]    detect feature cap-9600GB support
[2023-05-30 15:57:19.670] [4-2-2023:5:30|7:57:18:960]    feature new-changecap compatible check fail
[2023-05-30 15:57:19.670] [4-2-2023:5:30|7:57:18:960]    U0022739_FW_UPDATE_ERR:
[2023-05-30 15:57:19.670]   > _bl:                 3
[2023-05-30 15:57:19.670]   > _line:               1fa
[2023-05-30 15:57:19.670]   > _err_type:           ERR_FEATURE_COMPATIBLE_CHECK_FAIL
[2023-05-30 15:57:19.670] [4-2-2023:5:30|7:57:18:960]    U0022739_FW_IMG_ERR:
[2023-05-30 15:57:19.729]   > _ctag:               72
[2023-05-30 15:57:19.729]   > _act:                72
[2023-05-30 15:57:19.729]   > _img_len:            3bf000
[2023-05-30 15:57:19.729]   > _buf:

```

```c++
/**
 * command specific status codes
 */
enum nvme_command_specific_status_code {
    nvme_sc_completion_queue_invalid            = 0x00,
    nvme_sc_invalid_queue_identifier            = 0x01,
    nvme_sc_invalid_queue_size                  = 0x02,
    nvme_sc_abort_command_limit_exceeded        = 0x03,
    /* 0x04 - reserved */
    nvme_sc_async_event_request_limit_exceeded  = 0x05,
    nvme_sc_invalid_firmware_slot               = 0x06,
    nvme_sc_invalid_firmware_image              = 0x07,
    nvme_sc_invalid_interrupt_vector            = 0x08,
    nvme_sc_invalid_log_page                    = 0x09,
    nvme_sc_invalid_format                      = 0x0a,
    nvme_sc_firmware_req_conventional_reset     = 0x0b,
    nvme_sc_invalid_queue_deletion              = 0x0c,
    nvme_sc_feature_id_not_saveable             = 0x0d,
    nvme_sc_feature_not_changeable              = 0x0e,
    nvme_sc_feature_not_namespace_specific      = 0x0f,
    nvme_sc_firmware_req_nvm_reset              = 0x10,
    nvme_sc_firmware_req_reset                  = 0x11,
    nvme_sc_firmware_req_max_time_violation     = 0x12,
    nvme_sc_firmware_activation_prohibited      = 0x13,
    nvme_sc_overlapping_range                   = 0x14,
    nvme_sc_namespace_insufficient_capacity     = 0x15,
    nvme_sc_namespace_id_unavailable            = 0x16,
    /* 0x17 - reserved */
    nvme_sc_namespace_already_attached          = 0x18,
    nvme_sc_namespace_is_private                = 0x19,
    nvme_sc_namespace_not_attached              = 0x1a,
    nvme_sc_thinprovisioning_not_supported      = 0x1b,
    nvme_sc_controller_list_invalid             = 0x1c,
    nvme_sc_device_self_test_in_progress        = 0x1d,
    nvme_sc_boot_partition_write_prohibited     = 0x1e,
    nvme_sc_invalid_ctrlr_id                    = 0x1f,
    nvme_sc_invalid_secondary_ctrlr_state       = 0x20,
    nvme_sc_invalid_num_ctrlr_resources         = 0x21,
    nvme_sc_invalid_resource_id                 = 0x22,

    nvme_sc_conflicting_attributes              = 0x80,
    nvme_sc_invalid_protection_info             = 0x81,
    nvme_sc_attempted_write_to_ro_range         = 0x82,
};
```

```c++
/**
 * admin opcodes
 */
enum nvme_admin_opcode {
    nvme_opc_delete_io_sq           = 0x00,
    nvme_opc_create_io_sq           = 0x01,
    nvme_opc_get_log_page           = 0x02,
    /* 0x03 - reserved */
    nvme_opc_delete_io_cq           = 0x04,
    nvme_opc_create_io_cq           = 0x05,
    nvme_opc_identify               = 0x06,
    /* 0x07 - reserved */
    nvme_opc_abort                  = 0x08,
    nvme_opc_set_features           = 0x09,
    nvme_opc_get_features           = 0x0a,
    /* 0x0b - reserved */
    nvme_opc_async_event_request    = 0x0c,
    nvme_opc_ns_management          = 0x0d,
    /* 0x0e-0x0f - reserved */
    nvme_opc_firmware_commit        = 0x10,
    nvme_opc_firmware_image_download    = 0x11,

    nvme_opc_device_self_test       = 0x14,
    nvme_opc_ns_attachment          = 0x15,

    nvme_opc_keep_alive             = 0x18,
    nvme_opc_directive_send         = 0x19,
    nvme_opc_directive_receive      = 0x1a,

    nvme_opc_virtualization_management      = 0x1c,
    nvme_opc_nvme_mi_send           = 0x1d,
    nvme_opc_nvme_mi_receive        = 0x1e,

    nvme_opc_doorbell_buffer_config = 0x7c,

    nvme_opc_format_nvm             = 0x80,
    nvme_opc_security_send          = 0x81,
    nvme_opc_security_receive       = 0x82,
    nvme_opc_sanitize               = 0x84,
    nvme_opc_get_lba_status         = 0x86,

    //sfx self-defined cmd
    nvme_opc_vu                     = 0xc3, /*incompatible if iommu on or for nvme-cli 2.0*/
    nvme_opc_vu_set                 = 0xc5, /*set to device, have to set bit0 to 1*/
    nvme_opc_vu_get                 = 0xc6, /*get from device, have to set bit 1 to 1 and bit 0 to 0*/
    nvme_opc_query_cap_info         = 0xd3,
    nvme_opc_change_cap             = 0xd4,
    nvme_opc_change_provision_cap   = 0xd5,
    nvme_opc_query_cap_info_comp    = 0xd6,
    nvme_opc_vu_ns_management       = 0xd7,
    nvme_opc_vu_ns_attachment       = 0xd9,
    nvme_opc_undefined,
};
```

<<<<<<< Updated upstream
CSDU5SPC15C1 



wrmatmeta 1 CSDU5SPC15C1 0 0
=======

python3 DNLD_PFDuringFWCommit.py --newFWImg /home/tcnsh/junw/myrtle_bin/U0023045.bin --baseFWImg /home/tcnsh/junw/myrtle_bin/U0023045.bin

python3 Tool_EventLogDump.py

scp myrtle_fw.asic.wout.bl3.dev_build.spc76_vU.0.0@*.strip.elf.signed.bin root@192.168.38.29:/home/tcnsh/junw/myrtle_bin/U0023045.bin

scp myrtle_fw.elog_parser.dev_build.spc76_vU.0.0@*.elf root@192.168.38.29:/home/tcnsh/samanea/Samanea_Scripts/Tool/U0023045_evtlog.elf

```c++
bool pf_hw_aborter::check_all_stream_last_program_pba_after_abort()
{
    for (u32 stream = 0; stream <= STREAM_ID_L2P_PATCH; stream++) {
        active_b2n *act_pu_b2n = &g_b2n_task.act_b2n[stream];
        if (act_pu_b2n->last_prog_b2n_pba != INVALID_64BIT) {
            _last_pba_block[stream] = GET_BLOCK_FROM_PBA(act_pu_b2n->last_prog_b2n_pba);
            u32 phy_die = GET_DIE_FROM_PBA(act_pu_b2n->last_prog_b2n_pba);
            u32 parity_groug_index = phy_die / MAX_PARITY_RATIO;
            pu_xlat_info *xlat_info = (pu_xlat_info *)&g_pu_mgr.pu_info[0][_last_pba_block[stream]];
            u32 xlat_seq_die = xlat_info->phy2xlat_seq_die[phy_die];
            _last_xlat_die[stream] = (u8)xlat_seq_die;
            _last_prog_pba[stream] = act_pu_b2n->last_prog_b2n_pba;
            // check if reach second-to-last B2N of a parity group
            if (xlat_seq_die == xlat_info->parity_grp_cfg[parity_groug_index].end_die - 1) {
                _partial_parity_group[stream] = true;
                _is_send_second_to_last_b2n = true;
                return false;
            } else if (xlat_seq_die == xlat_info->parity_grp_cfg[parity_groug_index].end_die) {
                _partial_parity_group[stream] = false;
            } else {
                _partial_parity_group[stream] = true;
            }
        }
    }
    return true;
}
```