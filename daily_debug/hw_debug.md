4319

```
搜ccs counter dump，看这个ccs 0x41， 0x241寄存器，如果0x241比0x41少1，然后再看CCS monitor 0x1f0的低16bit为8就能断定是#4319
```

```
mon_sel 0x500 = 0x9aaa0408
bit[8]=1:表示sdsw 向ccs拿不到b2n, b2n_stream=bit[15:12];
bit[9]=1:表示sdsw 向ccs send b2n cpl request, ccs 不响应
bit[10]=1:表示sdsw 向ccs send w2p cpl request, ccs 不响应
bit[11]=1:表示sdsw 向key cache request aes key, key cache不响应
```

```
AES key拿不到的问题
先找 "sdsr monitor sel dump" in hwdbg往下，找mon_sel 0xd0，如果bit[3:0]=1，就是在request aes key，等key cache回
```

```
Step2 读寄存器
write 'h500 to 'h231a0044
read 'h231a0048 for mon_sig_out

如果 bit8=1，就是在等b2n，
然后看 bit[15:12] 看是哪个stream


w4 0x231a0044 0x500
mem32 0x231a0048 1
昨天开会讨论的时候粗略统计了一下，现在pf这边少说也做过2w+次了，而且hw大概率每次都会有b2n flush的操作，但从大家的debug记录来看，没在这个环节遇到过4319或者5521,所以想问下是不是PF时HW abort触发的b2n flush本身就可以避免这个问题
```

```
下面是判断BM-DDR write traffic hang的方法， 

1)首先write path hang；

2)在HW dump 里面 search keyword “bm monitor dump“, 找到 mon_ctrl = 0x203 和 mon_ctrl = 0x204  对应的32bit monitor 的值，如果下面三个16bit monitor value 不相等的话 就说明 BM wirte DDR hang了
0x203 的[31:16] / 0x203 的[15:0]/ 0x204 的[15:0]

请FW/QA @Jie Zhou  @Jupiter Wang 在下次碰到BM-DDR 之间write traffic hang的时候：

1）记录test 信息，保留现场；

2）FW 可以试试直接通过CPU向DDR进行简单的读写操作，看看是否正常 ？
```

```
sdsr的monitor 0x21高16bit为0x10说明obuf是满的
```

