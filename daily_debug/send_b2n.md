```
submit_b2n
u8 fct_idx;
fct_alloc_entries_idx(pool_id, &fct_idx, 1)
void *fct_entry;
fct_entry = fct_get_entry(pool_id, fct_idx);
fw_b2n_cmd::send_b2n_cmd(fct_entry, GEN_FULL_FCT_IDX(pool_id, fct_idx), pba, stream, qid, true);
```

```
submit_w2p
u8   fct_idx;
fct_alloc_entries_idx(pool_id, &fct_idx, 1)
void *fct_entry = fct_get_entry(pool_id, fct_idx);
fw_w2p_cmd::send_normal_wr_cmd(fct_entry, GEN_FULL_FCT_IDX(pool_id, fct_idx), stream, data_addr, lpa, qid);
```

```
poll_b2n
u32 full_fct_idx = poll_single_b2n_cpl(qid, true);
u8    fct_idx     = GET_FCT_IDX(full_fct_idx);
u32   pool_id     = GET_POOL_ID(full_fct_idx);
void *fct_entry   = fct_get_entry(pool_id, fct_idx);
u32   nand_status = fct_b2n_cmd::get_cpl_nand_status(fct_entry);
u64   nand_addr   = fct_b2n_cmd::get_ccs_cmd_pba_h(fct_entry);
bool  slc         = fct_b2n_cmd::get_ccs_cmd_slc(fct_entry);
nand_addr = (nand_addr << 32) + fct_b2n_cmd::get_ccs_cmd_pba_l(fct_entry);
nand_addr = g_err_injector.nand_addr_update(nand_addr);
pba_t pba = g_be_mgr.nand_addr_to_pba(slc, nand_addr, 0, 0);
```

```
poll_w2p
ACE_W2P_CPL_MSG_U *cpl_msg = (ACE_W2P_CPL_MSG_U *)fcq_fetch_cq_msg(qid);
u32 opcode = fcq_ace_cpl_msg::get_opcode(cpl_msg);
u8    fct_idx    = GET_FCT_IDX(cpl_msg->fields.fct_idx);
u32   pool_id    = GET_POOL_ID(cpl_msg->fields.fct_idx);
void *fct_entry  = fct_get_entry(pool_id, fct_idx);
u64   nand_addr  = ((u64)(fct_w2p_cmd::get_pba_h(fct_entry)) << 32) + fct_w2p_cmd::get_pba_l(fct_entry);
u32   ccp_offset = (fct_w2p_cmd::get_cpl_pl_ep_ofst(fct_entry) & 0x3ff);
u32   comp_len   = fct_w2p_cmd::get_cpl_comp_size_4b(fct_entry);
pba_t pba        = {.all = 0};
nand_addr        = g_err_injector.nand_addr_update(nand_addr);
bool  slc        = IS_SLC_STREAM(fct_w2p_cmd::get_ccs_cmd_stream_id(fct_entry));
pba              = g_be_mgr.nand_addr_to_pba(slc, nand_addr, ccp_offset, comp_len);
fcq_release_cq_entry(qid);
fct_release_entries_by_idx(pool_id, &fct_idx, 1);
```

B2N_FLUSH

is_mode1_flush --> g_padding_task.submit_b2n_flush(_qid, _fd_worker->_stream_id)

g_padding_task.submit_padding_w2p(_qid, _fd_worker->_stream_id, false, false)

MLOG_INFO

g_maplog_task.maplog_submit_w2p_cmd



g_b2n_task.get_stream_tgt_blk(u32 stream)

