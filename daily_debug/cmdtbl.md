```c++
/**
 * admin opcodes
 */
enum nvme_admin_opcode {
    nvme_opc_delete_io_sq           = 0x00,
    nvme_opc_create_io_sq           = 0x01,
    nvme_opc_get_log_page           = 0x02,
    /* 0x03 - reserved */
    nvme_opc_delete_io_cq           = 0x04,
    nvme_opc_create_io_cq           = 0x05,
    nvme_opc_identify               = 0x06,
    /* 0x07 - reserved */
    nvme_opc_abort                  = 0x08,
    nvme_opc_set_features           = 0x09,
    nvme_opc_get_features           = 0x0a,
    /* 0x0b - reserved */
    nvme_opc_async_event_request    = 0x0c,
    nvme_opc_ns_management          = 0x0d,
    /* 0x0e-0x0f - reserved */
    nvme_opc_firmware_commit        = 0x10,
    nvme_opc_firmware_image_download    = 0x11,

    nvme_opc_device_self_test       = 0x14,
    nvme_opc_ns_attachment          = 0x15,

    nvme_opc_keep_alive             = 0x18,
    nvme_opc_directive_send         = 0x19,
    nvme_opc_directive_receive      = 0x1a,

    nvme_opc_virtualization_management      = 0x1c,
    nvme_opc_nvme_mi_send           = 0x1d,
    nvme_opc_nvme_mi_receive        = 0x1e,

    nvme_opc_doorbell_buffer_config = 0x7c,

    nvme_opc_format_nvm             = 0x80,
    nvme_opc_security_send          = 0x81,
    nvme_opc_security_receive       = 0x82,
    nvme_opc_sanitize               = 0x84,
    nvme_opc_get_lba_status         = 0x86,

    //sfx self-defined cmd
    nvme_opc_vu                     = 0xc3, /*incompatible if iommu on or for nvme-cli 2.0*/
    nvme_opc_vu_set                 = 0xc5, /*set to device, have to set bit0 to 1*/
    nvme_opc_vu_get                 = 0xc6, /*get from device, have to set bit 1 to 1 and bit 0 to 0*/
    nvme_opc_query_cap_info         = 0xd3,
    nvme_opc_change_cap             = 0xd4,
    nvme_opc_change_provision_cap   = 0xd5,
    nvme_opc_query_cap_info_comp    = 0xd6,
    nvme_opc_vu_ns_management       = 0xd7,
    nvme_opc_vu_ns_attachment       = 0xd9,
    nvme_opc_undefined,
};
```

```
[37196.105767] perf: interrupt took too long (2502 > 2500), lowering kernel.perf_event_max_sample_rate to 79000
[72693.825978] nvme nvme0: I/O 7 QID 0 timeout, reset controller
[72814.471071] nvme nvme0: Device not ready; aborting reset, CSTS=0x1
```

```
[2023-06-05 10:05:28] ==============================vld cmdtbl dump, all are hex============================
[2023-06-05 10:05:28] idx  v  opc  cid  adm  ns    dw9      dw8      dw11      dw10      dw12      sts  pn  rsn0  rsn1  sq  err err_slb err_ext acpl
[2023-06-05 10:05:28] 37   1  2    10   1    ffff  0        0        0         7f020d    0         1    0   8     0     0   0   0       0       0
[2023-06-05 10:05:28] =================vld_num:0x1, err_num:0x0======================
                      EA2309G0011M@CSDE1SUC76:/$ getevtloginfo
[2023-06-05 10:06:45] [4-2-2023:6:5|2:6:50:231]    get evt log info
[2023-06-05 10:06:45] [4-2-2023:6:5|2:6:50:231]    evt block 4294967295, tot die 128, cur prog page 65535, cur_die 0 cur idx 0, next die 0
[2023-06-05 10:06:45] [4-2-2023:6:5|2:6:50:231]    tot flush size 0x0, cached size 0x1fffed, tot flushed entries 0
[2023-06-05 10:06:45] [4-2-2023:6:5|2:6:50:231]    evt buf head 0, tail 1fffed


[2023-06-05 10:07:11] vu_handler_rdnor with test buffer 0x400c20aa8
[2023-06-05 10:07:11] 0x400c20aa8 |0x00000223 0x0000001c 0x1fc89e4a 0x87828744 0x00000000 0x00000000 0x00000001 0x00000000
[2023-06-05 10:07:11] 0x400c20ac8 |0x7c7c8c8c 0x00000000 0x1fc89e4a 0x87828744 0x00000000 0x00000000 0x00000000 0x00000000
[2023-06-05 10:07:11] 0x400c20ae8 |0x00000000 0x00000000 0x00000000 0x00000000 0x00000000 0x00000000 0x00000000 0x00000000
[2023-06-05 10:07:11] 0x400c20b08 |0x00000000 0x00000000 0x7c7c8c8c 0x00000000 0xffffffff 0xffffffff 0xffffffff 0xffffffff
[2023-06-05 10:07:11] 0x400c20b28 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
[2023-06-05 10:07:11] 0x400c20b48 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
[2023-06-05 10:07:11] 0x400c20b68 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
[2023-06-05 10:07:11] 0x400c20b88 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
[2023-06-05 10:07:11] [4-2-2023:6:5|2:7:15:488]    Address --> 18284544, Buffer --> 0x400c20aa8 nor read success

```

```
                      EA2309G0011M@CSDE1SUC76:/$ rdnor 0x100d000 0x100
[2023-06-05 10:22:53] [4-2-2023:6:5|2:22:58:293]    The addr passed in --> 0x100d000
[2023-06-05 10:22:53] [4-2-2023:6:5|2:22:58:293]    vu nor job id[40] released
[2023-06-05 10:22:53] vu_handler_rdnor with test buffer 0x400c20aa8
[2023-06-05 10:22:53] 0x400c20aa8 |0x00000001 0x0000001c 0x00000002 0x00000000 0xcdcdcdcd 0xcdcdcdcd 0x00000000 0x00000000
[2023-06-05 10:22:53] 0x400c20ac8 |0x00000224 0x00000000 0x00182840 0xc9186151 0x31402558 0x00400c30 0x00000000 0x00000008
[2023-06-05 10:22:53] 0x400c20ae8 |0x00000000 0x00000000 0x00000000 0x00000000 0x00000000 0x00000000 0x00000000 0x00000000
[2023-06-05 10:22:53] 0x400c20b08 |0x00000000 0x00000000 0x00000000 0x00000000 0x00000000 0x00000000 0x23232323 0x4f518802
[2023-06-05 10:22:54] 0x400c20b28 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
[2023-06-05 10:22:54] 0x400c20b48 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
[2023-06-05 10:22:54] 0x400c20b68 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
[2023-06-05 10:22:54] 0x400c20b88 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
[2023-06-05 10:22:54] [4-2-2023:6:5|2:22:58:293]    Address --> 16830464, Buffer --> 0x400c20aa8 nor read success

```

```
                      EA2309G0011M@CSDE1SUC76:/$ parsepba 0x111d7e600
[2023-06-05 10:32:34] pba.all 0x111d7e600:
[2023-06-05 10:32:34] ch:       8
[2023-06-05 10:32:34] ce_lun:   1
[2023-06-05 10:32:34] block:    547
[2023-06-05 10:32:34] page:     703
[2023-06-05 10:32:34] plane:    0
[2023-06-05 10:32:34] offset:   0x0
[2023-06-05 10:32:34] type:     0
[2023-06-05 10:32:34] ep_idx:   0
[2023-06-05 10:32:34] ccp_off:  0x0
[2023-06-05 10:32:34] comp_len: 0x0
[2023-06-05 10:32:34] unc:      0

```

