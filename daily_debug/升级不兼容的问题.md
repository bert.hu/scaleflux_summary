将E1.s的8T的config文件中channel从16改为8以后

更新bl2和bl3，但进入FDL mode中mat创建RFS会fail

查看以后发现console task会卡死在EVT_STATE_SCAN_LAST_PROG_DIE_WAIT=12这个状态

原因是console task会从nor里面读取config，但会将修改channel之前的读取出来，导致scan的时候会读超过channel 8的die，这样会导致hang住

但mat才能擦掉nor的配置，所以这里需要使用vu来擦nor config

```
EA2309G0011M@CSDE1SUC76:/$ ernor 0x1170000 128
EA2309G0011M@CSDE1SUC76:/$ rdnor 0x1171000 128
[4-2-1970:1:1|0:3:19:227]    The addr passed in --> 0x1171000
vu_handler_rdnor with test buffer 0x400c20540
0x400c20540 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
0x400c20560 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
0x400c20580 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
0x400c205a0 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
[4-2-1970:1:1|0:3:19:227]    Address --> 18288640, Buffer --> 0x400c20540 nor read success
EA2309G0011M@CSDE1SUC76:/$ rdnor 0x1170000 128
[4-2-1970:1:1|0:3:31:720]    The addr passed in --> 0x1170000
vu_handler_rdnor with test buffer 0x400c20540
0x400c20540 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
0x400c20560 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
0x400c20580 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
0x400c205a0 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
[4-2-1970:1:1|0:3:31:720]    Address --> 18284544, Buffer --> 0x400c20540 nor read success
EA2309G0011M@CSDE1SUC76:/$
EA2309G0011M@CSDE1SUC76:/$
EA2309G0011M@CSDE1SUC76:/$ erasematlog 3
[4-2-1970:1:1|0:4:43:914]    erase with option[3]
EA2309G0011M@CSDE1SUC76:/$ rdnor 0x100d000 128
[4-2-1970:1:1|0:0:56:716]    The addr passed in --> 0x100d000
vu_handler_rdnor with test buffer 0x400c20540
0x400c20540 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
0x400c20560 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
0x400c20580 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
0x400c205a0 |0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff 0xffffffff
[4-2-1970:1:1|0:0:56:717]    Address --> 16830464, Buffer --> 0x400c20540 nor read success
```

擦除nor以后，重新pf上电进入FDL mode，发现console task已经正常了，进入IDLE状态了

但mat创建RFS还是会fail，原因也是在于RFS也会读超出channel 8的die

死在了rfs_nand_erase_fblock这个函数里面，在do while里面出不来，原因是poll不回来erase cpl

```c++
u32 rfs_nand_erase_fblock(RFS_FBLOCK_ADDRESS fba)
{
    if(IS_PF_COMING()) {
        return RFS_RC_NEED_PAUSE;
    }
    bool status = rfs_send_erase_cmd(rfs_fba_to_pba(fba, 0, "erase"));
    if((!status) && IS_PF_COMING()) {
        return RFS_RC_NEED_PAUSE;
    }
    if (false == status) {
        // failed to send erase comand
        return RFS_RC_NAND_ERASE_FAILED;
    }
    EVT_LOG(rfs_nand_erase_fblock_evtlog, fba);

#if (RFS_NAND_ERASE_POLLING != 0)

    u32 rc;
    do {
        if(IS_PF_COMING()) {
            return RFS_RC_NEED_PAUSE;
        }
        rc = rfs_poll_erase_cpl();
    } while (RFS_RC_NAND_IN_CMD_POLLING == rc); //cstat !MISRAC2004-13.7_b !MISRAC++2008-0-1-2_b

    return rc;

#else
```

原因在于CFG_PBA_CHAN_BITS这个宏，因为channel从16改成了8，所以这个宏应该从4改为3

因为RFS就是通过这个宏来计算要去擦的DIE的channel

对于不兼容的问题，有以下步骤

So, the steps to run E1.S FW on 16T card:

1. eraserfs
2. erasematlog 255
3. ernor 0x1170000 16
4. pf
5. rescanpecie if connected with host logging tool
6. mat