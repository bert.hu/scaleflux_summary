```c++
以8T 16g b47为例
#define CFG_CHAN_NUM            (16)
#define CFG_CE_NUM              (2)
#define CFG_LUN_NUM             (2)
#define CFG_BLOCK_NUM           (556)
#define CFG_PAGE_NUM            (712)
#define CFG_SLC_PAGE_NUM        (704)
#define CFG_NAND_PAGE_NUM       (2112)
#define CFG_PAGE_TYPE_NUM       (3)
#define CFG_PLANE_NUM           (4)
MAX_PARITY_GROUP_NUM = 1
    // We reach the end of a parity group
    if (((phy_die % MAX_PARITY_RATIO) == (MAX_PARITY_RATIO - 1)) || (phy_die == end_die)) 
        
    FOOTER_BUF_WRONG
        get_gsd_recovery_data_addr
            if (_data_addr != 0) {
                free_footer_resource(_data_addr);
            }

// check whether the pba address out of bound
static bool check_pba_boundary(pba_t pba)
{
    return ((pba.block < CFG_BLOCK_NUM) && (pba.page < CFG_PAGE_NUM) && (pba.page_type < CFG_PAGE_TYPE_NUM)
            && (pba.ch < CFG_CHAN_NUM) && (pba.ce_lun < (CFG_CE_NUM * CFG_LUN_NUM)) && (pba.plane < CFG_PLANE_NUM));
}

VU_STATUS_E vu_handler_pbatolba(vector<string> *pargs __unused, pbatolba *param __unused, pbatolba_data *pdata __unused)
{
    pba_t pba;
    // 1. get input
    if (!get_input_for_pba2lba_pbadatatype(true, pargs, pdata, &pba.all)) {
        INF_LOG("Usage: pba2lba <pba>\n");
        return VU_EXEC_FAILED;
    }
    // 2. prepare
    // 2.1 Is pba out of bound?
    if (!check_pba_boundary(pba)) {
        INF_LOG("pba2lba: pba out of bound\n");
        return VU_EXEC_FAILED;
    }
    // 2.2 blk status
    blk_mgr *pblk_mgr   = &g_pu_mgr.pblk_mgr[0];
    u8       blk_status = pblk_mgr->get_sblock_status(pba.block);
    if (!IS_DATA_BLOCK(blk_status)) {
        INF_LOG("pba2lba: should be a data block\n");
        return VU_EXEC_FAILED;
    }
    // 2.3 bad plane?
    if (g_pu_mgr.bb_mgr.is_bad_plane(pba.all)) {
        INF_LOG("pba2lba: bad plane\n");
        return VU_EXEC_FAILED;
    }
    // 2.4 xor?
    if (on_parity_die(pba.all)) {
        INF_LOG("pba2lba: pba on parity die\n");
        if (pdata) {
            pdata->output.count = 0;
        }
        return VU_EXEC_SUCCESS;
    }
    // 2.5 slc?
    u8 blk_stream = pblk_mgr->get_sblock_stream(pba.block);
    u8 is_slc     = IS_SLC_STREAM(blk_stream);
    if (is_slc) {
        INF_LOG("pba2lba: slc pba\n");
        if (pdata) {
            pdata->output.count = 0;
        }
        return VU_EXEC_SUCCESS;
    }

    // 3. header extraction
    // 3.1 clear hdr_buf
    void *hdr_buf = (void *)FTEST_RW_BUF_START;
    memset(hdr_buf, 0, HDR_BUF_SIZE);
    // 3.2 find first good plane
    u32 target_plane   = pba.plane;
    u8  bad_plane_mask = 0;
    if (!hdr_extraction_set_first_good_plane(&pba, &bad_plane_mask)) {
        INF_LOG("pba2lba: no good plane\n");
        return VU_EXEC_FAILED;
    }
    // 3.3 send
    u8 fct_idx = 0;
    if (!hdr_extraction_send(pba, bad_plane_mask, 0, (u64)hdr_buf, &fct_idx)) {
        INF_LOG("pba2lba: send header extraction failed\n");
        return VU_EXEC_FAILED;
    }
    // 3.4 poll
    if (!hdr_extraction_poll(fct_idx)) {
        INF_LOG("pba2lba: poll header extraction failed\n");
        return VU_EXEC_FAILED;
    }
    // 3.5 parse
    if (!parse_hdr_extraction_pba2lba(pdata, hdr_buf, pba, target_plane)) {
        return VU_EXEC_FAILED;
    }
    return VU_EXEC_SUCCESS;
}
```

#### 案例分析

```
header extraction hit shim error while doing parity level folding, and data not moved after folding done.

confirm with Fei, even shim error happened, do not mean no ECC error. so should check ECC error first, and even there is no ECC error, does not mean data is really correct, we met ECC miscorrect before. so should be parity rebuid.

So I used vu pba2lba to read the valid lba in this page type (the function of this vu is to use the header extration to read the page type of the corresponding die, and then compare it with the l2p table to find all the valid lba), and found that there are many lba, which means that the page type 2 of the die has a lot of data that has not been removed.And in these valid LBA, there is no lba  0x79a06 that the script suggests that it has not moved.

So the problem can be summed up in the fact that folding did read the page type of the corresponding die when doing the header extration, but folding did not parse out a lot of valid lba. So either the header extration read will have problems in the case of the inject error, or the l2p query will sometimes have problems, the former is more likely, and I need to add more debug vu with pengli to confirm it.

slba 0x7c0bf10
lpa = lba / 8 = 0xF817E2
NOT-SET@:/$ l2p 0xF817E2
[0-0:0:0D|1:50:39:821]     io_task0:           lpa: 0xf817e2, the corresponding pba is: 0x8185a12e8259f

elba 0x2d978188
lpa = lba / 8 = 0x5B2F031
NOT-SET@:/$ l2p 0x5B2F031
[0-0:0:0D|1:52:13:12]     io_task0:            lpa: 0x5b2f031, the corresponding pba is: 0x8183c1825b96b

spba = 0x8185a12e8259f
epba = 0x8183c1825b96b

NOT-SET@:/$ parsepba 0x8185a12e8259f
pba.all 0x8185a12e8259f:
ch:       2
ce_lun:   5
block:    151
page:     260
plane:    3
offset:   0x3
type:     1
ep_idx:   0
ccp_off:  0x5a
comp_len: 0x206

nand addr fields:
lun:      0x1
block:    0x97
plane:    0x3
page:     0x305
row_addr: 0x125f305
la:       0x6
type:     0x0
ln:       0x1
ce:       0x1
ch:       0x2
high32:   0x2506
NOT-SET@:/$

NOT-SET@:/$ parsepba 0x8183c1825b96b
pba.all 0x8183c1825b96b:
ch:       1
ce_lun:   1
block:    193
page:     183
plane:    2
offset:   0x3
type:     2
ep_idx:   0
ccp_off:  0x3c
comp_len: 0x206

nand addr fields:
lun:      0x0
block:    0xc1
plane:    0x2
page:     0x21f
row_addr: 0x30621f
la:       0x6
type:     0x0
ln:       0x0
ce:       0x1
ch:       0x1
high32:   0x1406
NOT-SET@:/$

Send read command from slba 680542720 * 512 and dmc slba 680542768 nsid 1
slba 680542720
slpa = slba / 8 = 0x5120840
NOT-SET@:/$ l2p 0x5120840
[0-0:0:0D|1:59:44:323]     io_task0:           lpa: 0x5120840, the corresponding pba is: 0x818579709906e
NOT-SET@:/$ parsepba 0x818579709906e
pba.all 0x818579709906e:
ch:       1
ce_lun:   0
block:    184
page:     306
plane:    3
offset:   0x2
type:     2
ep_idx:   1
ccp_off:  0x57
comp_len: 0x206

nand addr fields:
lun:      0x0
block:    0xb8
plane:    0x3
page:     0x390
row_addr: 0x2e3390
la:       0x5
type:     0x0
ln:       0x0
ce:       0x0
ch:       0x1
high32:   0x1005


NOT-SET@:/$ getpba 0x5120840 10
lba       pba               ch  ce_lun  blk    page  pln  off  type  epidx  ccpoff  comp_len
5120840   818579709906e     1   0       b8     132   3    2    2     1      57      206
5120841   8185a1709906f     1   0       b8     132   3    3    2     0      5a      206
5120842   81800170990a0     2   0       b8     132   0    0    2     0      0       206
5120843   81803970990a0     2   0       b8     132   0    0    2     1      3       206
5120844   81806170990a1     2   0       b8     132   0    1    2     0      6       206
5120845   81809970990a1     2   0       b8     132   0    1    2     1      9       206
5120846   101800166cd20d    0   2       b3     19a   3    1    0     0      0       406
5120847   101800166cd20e    0   2       b3     19a   3    2    0     0      0       406
5120848   101800166cd20f    0   2       b3     19a   3    3    0     0      0       406
5120849   101800166cd210    0   2       b3     19a   0    0    1     0      0       406

NOT-SET@:/$ blksts 184
[0-0:0:0D|2:14:51:404]     io_task0:           pu 0x0 block 0xb8 info:

[0-0:0:0D|6:20:46:53]   blk_message_show:
  > _sblk:                 b8
  > _status:                2
  > _stream:                0
  > _pe_cnt:                4
  > _valid_lpa_cnt:      1c7900
  > _tot_lpa_cnt:        1c7938
  > _is_bad_footer:         0
  > _release_space:       7fce360
  > _consume_space:      eec71d40
  > _last_append_PBA:    10180017163f8f
  > _last_maplog_PBA:    10180017163800
  > _footer_PBA:         10180017163801
  > _end_footer_PBA:     1018001716384d
  > _final_user_PBA:     171637c0
  > _avg_gts:               4437f
  > _tot_aval_space:     f6c23b60
  > _open_seq_num:            36c

```

