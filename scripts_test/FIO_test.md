```shell
for((i=1;i<=500;i++));
do
echo "This is $i loops"
#sudo fio -filename=/dev/nvme0n1 -direct=1 -iodepth=32 -rw=write -ioengine=libaio -bs=128k -offset=0 -size=100% -numjobs=1 -runtime=100s -time_based -group_reporting -name=mytest &
sudo fio --filename=/dev/nvme1n1 --ioengine=libaio --norandommap=1 --randrepeat=0 --direct=1 --name=mytest --size=100% --group_reporting --numjobs=8 --rw=randwrite --iodepth=128 --bs=128k &
sleep 10
sudo python3 /home/tcnsh/samanea/Samanea_Scripts/Tool/Tool_RelayPowerOff.py
sleep 2
sudo python3 /home/tcnsh/samanea/Samanea_Scripts/Tool/Tool_RelayPowerOn.py
sleep 5
sudo ./rescanpcie.sh
sleep 5
done

curl http://admin:1234@192.168.36.7/script.cgi?run=power_cycle_outlet5

sudo fio -filename=/dev/nvme0n1 -direct=1 -iodepth=128 -rw=write -ioengine=libaio -bs=128k -offset=0 -size=100% -numjobs=1 -runtime=0.1h -time_based -group_reporting -name=mytest &
```

#### flashrd命令

```
Tim 在fix MYR-2716时，raw read遇到了HW的一个限制，大家如果后续有用到raw read时，得注意一下。

raw read的nxs必须得是4KB的倍数，因为nxs 本身是0 based，所以这个值就肯定是奇数。

如果raw read的PBA是一个page的最后一个codeword，因为读4KB实际就超了，会引起异常，这时得把PBA往前偏移到4K对齐的地方。
```

#### 如何查看nand的内容

```
[lba]/8 -- 拿到[lpa]
l2p [lpa] --拿到[pba]
lpa: 0x0, the corresponding pba is: 0x3800058d06de7

getpba [lba] [个数] -- 拿到pba的list
flashrd [pba] [lba] normal/raw slc/tlc -- 拿到nand的内容

parsepba [pba] --拿到block信息
pba.all 0x3800058d06de7:
ch:       7
ce_lun:   3
block:    710
page:     262
plane:    1
offset:   0x3
type:     2
ep_idx:   0
ccp_off:  0x0
comp_len: 0xe0

nand addr fields:
lun:      0x1
block:    0x2c6
plane:    0x1
page:     0x106
row_addr: 0x363306
la:       0x6
type:     0x2
ln:       0x1
ce:       0x1
ch:       0x7
high32:   0x7526

blksts [blk_id:%d] -- 拿到block信息
[4-0:0:0D|10:24:28:338]   blk_message_show:
  > _sblk:                2c6
  > _status:                2
  > _stream:                0
  > _pe_cnt:                1
  > _valid_lpa_cnt:      349309
  > _tot_lpa_cnt:        365409
  > _is_bad_footer:         0
  > _how_long_lived:     78b30c400
  > _release_space:       a737c00
  > _consume_space:      143e74660
  > _last_append_PBA:    10180058dbffaf
  > _last_maplog_PBA:    10180058dbd010
  > _footer_PBA:         10180058dbd011
  > _end_footer_PBA:     10180058dbd057
  > _final_user_PBA:     58dbcfc0
  > _avg_gts:               8dab6
  > _tot_aval_space:     143e74660
  > _open_seq_num:            2c1
  > _free_ratio:            4


getblkinfo [blk_id:%d] -- 拿到block信息
getblkinfo(blk 710): status: 2, stream: 0, pe_cnt: 1, valid_lpa_cnt: 3445513, tot_lpa_cnt: 3560457
getblkinfo(blk 710): release_space: 175340544, consume_space: 5434197600, tot_aval_space: 0x143e7460
getblkinfo(blk 710): last_append_PBA: 0x10180058dbffaf, last_maplog_PBA: 0x10180058dbd010, final_us0
getblkinfo(blk 710): footer_PBA: 0x10180058dbd011, end_footer_PBA: 0x10180058dbd057
getblkinfo(blk 710): avg_gts: 0x8dab6, open_seq_num: 0x2c1
getblkinfo(blk 710): last_sent_b2n_pba: 0x0, last_prog_b2n_pba: 0x58dbffc0
getblkinfo(blk 710): prog_err_happen: 0, block_switch_flag: 1
getblkinfo(blk 710): has_bad_sector: 0, stb_flag: 0
getblkinfo(blk 710): maplog_sealed: 1


pf_data_collector::prepare_meta_data_fw_active()
pf_data_parser::restore_meta_data_fw_active()
```

#### 如何打印脚本写数据的log

```
python3 PC_FormatMixGCWithPF.py --enableIOLogger 1  --seed 193185

sudo python3 ./AssertionID/AID_FullQWriteMixAdminWihtMpageAssert.py --runType 'Daily Regression' --branchName active-dev-asic  --runID asic --testLevel daily --codeVersion U0016849
```

