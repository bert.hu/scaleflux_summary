```
AID_CapacityPageInWithMpageAssert.py
mpage_hdr_extr的时候遇到gc rd retry failed，并且rrb fail

10925348 ^[[0K[7-53:0:17D|22:28:12:978]   mpage_hdr_extr:^M
10925349 ^[[0K  > _current_pba:        19b00000^M
10925350 ^[[0K  > _last_hdr_pba:       80c0019bdef8f^M
10925351 ^[[0K^M

10925362 ^[[0K[7-53:0:17D|22:28:12:981]   read_err:^M
10925363 ^[[0K  > _event:                 2^M
10925364 ^[[0K  > _vt:                    0^M
10925365 ^[[0K  > _pba:                19b01780^M
10925366 ^[[0K  > _lp_bitmap:                3f^M
10925367 ^[[0K  > _ctrl:               a4000^M
10925368 ^[[0K  > _ext_ctrl:           bf00100^M
10925369 ^[[0K  > _cpl_status:            0^M
10925370 ^[[0K  > _ext_cpl_status:        0^M
10925371 ^[[0K  > _cw_err_bmp:         4942920^M
10925372 ^[[0K  > _ccp_crc_err_bmp:    494292000000000^M
10925373 ^[[0K  > _lp_crc_err_bmp:     494292000000000^M
10925374 ^[[0K  > _lba_mis_bmp:        494292000000000^M
10925375 ^[[0K  > _dma_err_bmp:        494292000000000^M
10925376 ^[[0K  > _shim_err:                  0^M

10925466 ^[[0K[7-0:0:0D|0:0:40:446] mpage_gc_tas:            gc rd retry failed, do rebuild, do retry 1, cur vt 1         0^M

10925485 ^[[0K[6-53:0:17D|22:28:12:989]   raid_rebuild_job_failed:^M
10925486 ^[[0K  > _job:                   0^M
10925487 ^[[0K  > _ctx_ptr:             5435d20^M
10925488 ^[[0K  > _nand_addr:          2d00002cd802^M
10925489 ^[[0K  > _rebuild_mode:       BE_RD_REBUILD^M
10925490 ^[[0K  > _blk:                 19b^M
10925491 ^[[0K  > _stream:                6^M
10925492 ^[[0K  > _blk_status:         BS_SEALED_BLOCK^M
10925493 ^[[0K^M
10925494 ^[[0K[6-0:0:0D|0:0:40:454] raid_rebuild:         Get rebuild cpl, nand_addr 0x2d00002cd802, jobid 0, fct          57f^M
10925495 ^[[0K                                    ^M
10925496 ^[[0K[6-0:0:0D|0:0:40:454] raid_rebuild:         hit double raid error!!^M

AID_FullDriverWithAssertPFRecovery.py
FOLDING_WR_CCP_CHECK_ERR

AID_MpageGCFlushWithMpageAssert.py
脚本报恢复时间超时，但实际是脚本未连上，lspci未找到设备
lspci -d cc53:

[root@localhost b47_img]# cat /root/rescanpcie.sh
#!/usr/bin/env bash
modprobe -r nvme
echo "remove pcie"
echo 1 >/sys/bus/pci/devices/0000\:02\:00.0/remove
sleep 10
echo "rescan pcie"
echo 1 >/sys/bus/pci/rescan
lspci -d cc53:
echo "rescan done"


AID_NormalGCWithMpageAssert.py
ERROR_RD_CMD_ERR
mpage_hdr_extr的时候遇到gc rd retry failed，并且rrb fail

^[[0K[7-53:0:17D|21:24:49:453]   mpage_hdr_extr:
^[[0K  > _current_pba:        17200000
^[[0K  > _last_hdr_pba:       1018001735e78f

^[[0K[7-53:0:17D|21:24:49:568]   read_err:
^[[0K  > _event:                 2
^[[0K  > _vt:                    0
^[[0K  > _pba:                1720af80
^[[0K  > _lp_bitmap:                3f
^[[0K  > _ctrl:               a4000
^[[0K  > _ext_ctrl:           bf00100
^[[0K  > _cpl_status:            0
^[[0K  > _ext_cpl_status:        0
^[[0K  > _cw_err_bmp:         46d24c0
^[[0K  > _ccp_crc_err_bmp:    46d24c000000000
^[[0K  > _lp_crc_err_bmp:     46d24c000000000
^[[0K  > _lba_mis_bmp:        46d24c000000000
^[[0K  > _dma_err_bmp:        46d24c000000000
^[[0K  > _shim_err:                  0

^[[0K[7-0:0:0D|0:0:3:577] mpage_gc_tas:             gc rd retry failed, do rebuild, do retry 1, cur vt 7

^[[0K[6-53:0:17D|21:24:49:576]   raid_rebuild_job_failed:
^[[0K  > _job:                   0
^[[0K  > _ctx_ptr:             4f90fa0
^[[0K  > _nand_addr:          2d00012e4015
^[[0K  > _rebuild_mode:       BE_RD_REBUILD
^[[0K  > _blk:                  b9
^[[0K  > _stream:                6
^[[0K  > _blk_status:         BS_SEALED_BLOCK


AID_SyncIOWithAssertPFRecovery.py
FOLDING_WR_CCP_CHECK_ERR
```

#### 2020/5/7

```
AID_GSDWithAssertPFRecovery.py
AID_ProgramErrorWithMpageAssert.py--'NoneType' object is not subscriptable
PC_AsyncWriteMixRandomPF.py
PC_CapacityOverlapMixWithPF.py
PC_CapacityWithPFCrashRecovery.py --crash recovery need take long recovery time; [simon]: 脚本Lib传参有误
PC_PFDuringQuickRead.py --- 'NoneType' object is not subscriptable
PC_RandomIOMixPFFullCompair.py --need init LPTable
PC_RandomIOPFAndAsyncPF.py
PC_RandomWriteWithPFCrashRecovery.py----crash recovery need take long recovery time [simon]: 脚本Lib传参有误
PC_TrimMixIOWithPFCrashRecovery.py----crash recovery need take long recovery time [simon]: 脚本Lib传参有误
```

#### 2020/5/4

```
PC_OverlapDataMixAsyncPF.py
```

#### 2020/5/3

```
PC_CapacityOverlapMixWithPF.py
PC_CapacityPageInMixMutilWriteAndTrimWithPF.py
PC_CapacityWithPFCrashRecovery.py
PC_OverlapDataMixAsyncPF.py
PC_PFDuringQuickRead.py
PC_RandomIOMixPFFullCompair.py
PC_RandomWriteWithPFCrashRecovery.py
PC_TrimMixIOWithPFCrashRecovery.py
```

#### 2022/5/13

```
PC_CapacityOverlapMixWithPF.py -- DMC

NOT-SET@:/$ parsepba 0x818038a0b1d17
pba.all 0x818038a0b1d17:
ch:       0
ce_lun:   5
block:    80
page:     355
plane:    1
offset:   0x3
type:     1
ep_idx:   1
ccp_off:  0x3
comp_len: 0x206

nand addr fields:
lun:      0x1
block:    0x50
plane:    0x1
page:     0x41f
row_addr: 0x114141f
la:       0x7
type:     0x0
ln:       0x1
ce:       0x1
ch:       0x0
high32:   0x507
EVT_STATE_IDLE
evtlog_flush_ready
EVT_LOG(gsd_exit_warn_evtlog, GSD_FETCH_CQ_NULL);
ASSERT_GET_HOT_RECOVER_SBLOCK
PF_RESTORE_START_READ_ROOT_DATA
set_evtlog_write_ready()
g_evtbuf
g_evtbuf._commit_log_lock.spin_unlock();
commit_log

/home/tcnsh/MYR-2622.bin
[4-53:1:19D|4:13:26:459]   fwver:
  > build_time:            May 13 2022 23:28:05
  > origin_info:           git@bitbucket.org:sfxsw/myrtle_fw.git
  > branch_info:           HEAD  origin/active-dev  origin/HEAD  active-dev
  > hash_id:               3b5e2dd8647338f041231ffc73dc0cdfdf573761
  > author:                Mars Ma <mars.ma@scaleflux.com>
  > time:                  2022-05-13 14:56:05 +0000
  > fw_version:            vU.0.0@3b5e2d_15803
  > idfy_ver:              U0015803
  > opn:                   0x14C38A0
  > hw pcb_ver:            A21
  > hw fbid:               2
  > runmode:               USER_MODE
  > ddr_freq:              2400MHz
  > ddr_size:              16GB
  > raw_cap:               4096GiB
  > default_provision_cap: 3840GB
  > default_format_cap:    3840GB
  > default_max_lba:       0x1bf1f72af
  > default_real_op:       6%
  > sys pll_clk:           2250MHZ
  > cpu pll_clk:           1500MHZ
  > pcie_pba:              0x9000
  > bl2_version:           B0015803@3b5e2

```

![image-20220517110557899](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220517110557899.png)

![image-20220517110614589](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220517110614589.png)

![image-20220517110929659](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220517110929659.png)

![image-20220517111056553](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220517111056553.png)

![image-20220517142309277](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220517142309277.png)

![image-20220517150245862](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220517150245862.png)

![image-20220517151253678](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220517151253678.png)

![image-20220517151331988](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220517151331988.png)![image-20220517151348983](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220517151348983.png)![image-20220517153133234](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220517153133234.png)

![image-20220517154105697](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220517154105697.png)

#### ![image-20220517154343120](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220517154343120.png)![image-20220517154559691](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220517154559691.png)

#### 2022/5/17

```
#define IS_CRASH_RECOVERY()       ((get_tgt_smp_state() == SMP_STATE_RUNNING) && (g_recovery_task.get_force_full_recovery()))

def_console_task.set_evtlog_write_ready();
def_console_task.set_evtlog_read_ready();

     if (_force_flush_flag) {
         u32 padding_size = ROUND_UP(evtlog_cache_size, STB_DATA_FLUSH_SIZE) - evtlog_cache_size;
-        EVT_LOG_LOOP_SIZE(padding_size, dummy_evtlog);
+        //EVT_LOG_LOOP_SIZE(padding_size, dummy_evtlog);
+        u32 count = DIV_ROUND_UP(padding_size, sizeof(dummy_evtlog));
+        INF_LOG("console_debug:padding_size %d, count %d, sizeof(dummy_evtlog) %lu\n", padding_size, count, sizeof(dummy_evtlog));
+        for (u32 i = 0; i < count; i++) {
+            EVT_LOG(dummy_evtlog);
+        }
+        INF_LOG("console_debug:_force_flush_flag:%d, is pf? %d\n",_force_flush_flag, IS_PF());
         _force_flush_flag = false;
         if (IS_PF() || IS_PD()) {
             _pause_when_idle = true;
         }
         evtlog_cache_size = g_evtbuf.get_flushable_size();
-        INF_LOG("evt log force flush\n");
+        INF_LOG("evt log force flush, evtlog_cache_size %d\n", evtlog_cache_size);
     }

(gdb) p g_pf_dump_task ._state
$1 = 2
(gdb) p def_console_task ._event_handled
$2 = false
(gdb) p def_console_task ._event_status [18]
$3 = EVENT_HANDLING
(gdb) p def_console_task ._evtlog_read_ready
$4 = false
(gdb) p def_console_task ._evtlog_write_ready
$5 = false
(gdb) p def_console_task ._force_flush_flag
$6 = true
(gdb) p def_console_task ._wr_ctrl .pend_b2n_cnt
$7 = 0
(gdb) p def_console_task ._wr_ctrl .pend_w2p_cnt
$8 = 0
(gdb) p def_console_task ._state
$10 = 23

=============== Test Summary ==============
Test Case Name:    AID_GSDWithAssertPFRecovery
Test Case Execution Time:   8:18:42.086401 (hh:mm:ss.xxxx)
Test Case Result:  Pass

```

```

```

#### 2022/5/18

```
PC_CapacityOverlapMixWithPF.py
USER_GC_DEALLOC_VPC_ERROR

AID_FoldingWithAssertPFRecovery.py
INVALID_PBA_PASSED_FOR_COMPUTATION

=============== Test Summary ==============
Test Case Name:    AID_FoldingWithAssertPFRecovery
Test Case Execution Time:   1:00:37.931126 (hh:mm:ss.xxxx)
Test Case Result:  Pass

=============== Test Summary ==============
Test Case Name:    AID_SyncIOWithAssertPFRecovery
Test Case Execution Time:   2:25:15.131268 (hh:mm:ss.xxxx)
Test Case Result:  Pass

p /x g_b2n_task .act_b2n[0].outstanding_b2n_cnt

(gdb) p g_kernel ._admin_smpmgr ._scheduler ._active_task ._task_name
$29 = 0x5d82a8 "read_scrub_task"
(gdb) p g_kernel ._slave_smpmgr[0] ._scheduler ._active_task ._task_name
$30 = 0x5cd1c0 "io_task0"
(gdb) p g_kernel ._slave_smpmgr[1] ._scheduler ._active_task ._task_name
$31 = 0x5cd1c9 "io_task1"
(gdb) p g_kernel ._slave_smpmgr[2] ._scheduler ._active_task ._task_name
$32 = 0x5d3190 "folding_rd_task_gc"
(gdb) p g_kernel ._slave_smpmgr[3] ._scheduler ._active_task ._task_name
$33 = 0x5d889f "trim_backend_task"
(gdb) p g_kernel ._slave_smpmgr[4] ._scheduler ._active_task ._task_name
Cannot access memory at address 0x250
(gdb) p g_kernel ._slave_smpmgr[5] ._scheduler ._active_task ._task_name
$34 = 0x5d42fa "footer_task"
(gdb) p g_kernel ._slave_smpmgr[6] ._scheduler ._active_task ._task_name
$35 = 0x5d6034 "patch_flush_task"
(gdb) p g_kernel ._slave_smpmgr[7] ._scheduler ._active_task ._task_name
$36 = 0x5d1e2f "eh_task"
(gdb) p g_kernel ._slave_smpmgr[8] ._scheduler ._active_task ._task_name
$37 = 0x5e4f53 "simu_host_task"
(gdb) p g_kernel ._slave_smpmgr[9] ._scheduler ._active_task ._task_name
$38 = 0x5e4f62 "simu_dma_trig_task"
(gdb) p g_kernel ._slave_smpmgr[10] ._scheduler ._active_task ._task_name
$39 = 0x5e4f75 "simu_flash_cmd_task"
(gdb) p g_kernel ._slave_smpmgr[11] ._scheduler ._active_task ._task_name
$40 = 0x5e4f89 "simu_power_task"
(gdb) p g_kernel ._slave_smpmgr[12] ._scheduler ._active_task ._task_name
Cannot access memory at address 0x848

```

#### 2022/5/25

```
lpa: 0x3860c5, the corresponding pba is: 0x1018000a55d398

getpba 0x3860c5 0x16
lba       pba               ch  ce_lun  blk    page  pln  off  type  epidx  ccpoff  comp_len
3860c5    1018000a55d398    2   3       52     2ba   2    0    1     0      0       406
3860c6    818180a911ac4     3   2       54     223   1    0    0     0      18      206
3860c7    8181b8a911ac4     3   2       54     223   1    0    0     1      1b      206
3860c8    8181e0a911ac5     3   2       54     223   1    1    0     0      1e      206
3860c9    818218a911ac5     3   2       54     223   1    1    0     1      21      206
3860ca    818240a911ac6     3   2       54     223   1    2    0     0      24      206
3860cb    818278a911ac6     3   2       54     223   1    2    0     1      27      206
3860cc    8182a0a911ac7     3   2       54     223   1    3    0     0      2a      206
3860cd    8182d8a911ac7     3   2       54     223   1    3    0     1      2d      206
3860ce    818300a911ac8     3   2       54     223   2    0    0     0      30      206
3860cf    818338a911ac8     3   2       54     223   2    0    0     1      33      206
3860d0    818360a911ac9     3   2       54     223   2    1    0     0      36      206
3860d1    8181b8a911cd4     3   4       54     223   1    0    1     1      1b      206
3860d2    8181e0a911cd5     3   4       54     223   1    1    1     0      1e      206
3860d3    818218a911cd5     3   4       54     223   1    1    1     1      21      206
3860d4    818240a911cd6     3   4       54     223   1    2    1     0      24      206
3860d5    818278a911cd6     3   4       54     223   1    2    1     1      27      206
3860d6    8182a0a911cd7     3   4       54     223   1    3    1     0      2a      206
3860d7    8182d8a911cd7     3   4       54     223   1    3    1     1      2d      206
3860d8    818300a911cd8     3   4       54     223   2    0    1     0      30      206
3860d9    818338a911cd8     3   4       54     223   2    0    1     1      33      206
3860da    818360a911cd9     3   4       54     223   2    1    1     0      36      206

NOT-SET@:/$ parsepba 0x1018000a55d398
pba.all 0x1018000a55d398:
ch:       2
ce_lun:   3
block:    82
page:     698
plane:    2
offset:   0x0
type:     1
ep_idx:   0
ccp_off:  0x0
comp_len: 0x406

nand addr fields:
lun:      0x0
block:    0x52
plane:    0x2
page:     0x81f
row_addr: 0x14a81f
la:       0x0
type:     0x0
ln:       0x0
ce:       0x3
ch:       0x2
high32:   0x2c00


214685 [6-0:0:0D|0:0:0:66]   b2n_get_sblock:
214686   > _blk_id:               52
214687   > _stream:                0
214688   > _last_program_b2n_pba:  a55d400
214689   > _recovered_consume_space: f5218e00
214690   > _good_plane_num:       7c
214691   > _deallocated_sblock_num:    c
214692   > _real_free_sblock_num:    c

214923 [6-0:0:0D|0:0:0:70]   padding_add:
214924   > _stream:                0
214925   > _blk_id:               52
214926   > _density:               5
214927   > _priority:              0
214928   > _user_mix_write:        0
214929 [5-0:0:0D|0:0:0:70]   padding_start:
214930
214931   > _smp:                   4
214932
214933   > _stream:                0
214934 [6-0:0:0D|0:0:0:70]   trig_b2n_padding_stream:
214935   > _blk_id:               52
214936   > stream:                 0
214937   > _density:               5
214938   > padding_density:        5
214939   > _priority:              0
214940   > slc_only:            false
214941   > _boundary_open:         1
214942   > pfrst:               true
214943   > _wait_b2n:              1
214944
214945   > _user_mix_write:        0

214952 [5-0:0:0D|0:0:0:70] padding_task:            padding task useup b2n, last sent b2n pba 173397056, block 82, page 698, d       ie 17


215357 [6-0:0:0D|0:0:10:504]   partial_block_handle_get_footer_meta:
215358   > hot_open_blk1:         52
215359   > hot_open_blk2:       ffffffff
215360   > hot_offset1:         f250
215361   > hot_offset2:            0
215362   > is_hot_bad_footer:   0
215363
215364
215365 [6-0:0:0D|0:0:10:504]   scan_tlc_partial_candidate_sblock:
215366   > sblock:                52
215367   > open_seq_num:         31d
215368   > stream:                 0

215383 [6-0:0:0D|0:0:10:504]   partial_block_handle_get_block:
215384   > current_read_sblock:   52
215385   > stream:                 0
215386   > last_maplog_pba:     1018000a55d3a6
215387   > footer_pba:          ffffffffffffffff
215388   > last_prog_b2n_pba:    a55d7c0
215389   > final_user_b2n_pba:  ffffffffffffffff
215390
215391
215392 [6-0:0:0D|0:0:10:504]   partial_block_handle_get_b2n_pba_end:
215393   > hot_open_blk_index:     0
215394   > footer_data_addr:    42617d000
215395   > hot_offset:          f250
215396   > footer_entry_size:     10
215397   > b2n_pba_end:          a55d300
215398
215399
215400 [6-0:0:0D|0:0:10:504]   partial_block_handle_hot_stream_footer_scan:
215401   > hot_stream_block:          52
215402   > scan_start_pba:       a55d300
215403   > hot_open_blk_index:     0
215404   > b2n_end_page:         2ba
215405
215406
215407 [6-0:0:0D|0:0:10:504]   partial_block_handle_second_scan:
215408   > start_read_pba:       a55d300
215409   > end_read_pba:         a55d800

215521 [6-0:0:0D|0:0:10:505]   Recovery Task Complete Successfully.

blksts 82
[0-0:0:0D|12:17:24:986]   blk_message_show:
  > _sblk:                 52
  > _status:                1
  > _stream:                0
  > _pe_cnt:                4
  > _valid_lpa_cnt:      ee9c9
  > _tot_lpa_cnt:        f2569
  > _is_bad_footer:         0
  > _how_long_lived:     89afcd800
  > _release_space:       42003c0
  > _consume_space:      f54bcd00
  > _last_append_PBA:    1018000a55d7af
  > _last_maplog_PBA:    1018000a55d3a6
  > _footer_PBA:         ffffffffffffffff
  > _end_footer_PBA:     ffffffffffffffff
  > _final_user_PBA:     ffffffffffffffff
  > _avg_gts:               4c351
  > _tot_aval_space:     f8d51300
  > _open_seq_num:            31d
  > _free_ratio:            2


NOT-SET@:/$ getblkinfo 82
getblkinfo(blk 82): status: 1, stream: 0, pe_cnt: 4, valid_lpa_cnt: 977353, tot_lpa_cnt: 992617
getblkinfo(blk 82): release_space: 69206976, consume_space: 4115385600, tot_aval_space: 0xf8d51300
getblkinfo(blk 82): last_append_PBA: 0x1018000a55d7af, last_maplog_PBA: 0x1018000a55d3a6, final_user_PBA: 0
xffffffffffffffff
getblkinfo(blk 82): footer_PBA: 0xffffffffffffffff, end_footer_PBA: 0xffffffffffffffff
getblkinfo(blk 82): avg_gts: 0x4c351, open_seq_num: 0x31d
getblkinfo(blk 82): last_sent_b2n_pba: 0x0, last_prog_b2n_pba: 0xa55d7c0
getblkinfo(blk 82): prog_err_happen: 0, block_switch_flag: 0
getblkinfo(blk 82): has_bad_sector: 0, stb_flag: 0
getblkinfo(blk 82): maplog_sealed: 0

```

#### 2022/5/26

```
AID_HotStramWithB2NAssert.py
assert name: ASSERT_GET_FOOTER_NEW_SBLOCK

partial blk handle的时候
141197 [6-0:0:0D|0:0:4:99]   partial_block_handle_get_footer_meta:
141198   > hot_open_blk1:         55
141199   > hot_open_blk2:       ffffffff
141200   > hot_offset1:            0
141201   > hot_offset2:            0
141202   > is_hot_bad_footer:   0

看到的hot stream blk是0x55
但scan出来的还有hot stream blk是0x3b
141205 [6-0:0:0D|0:0:4:99]   scan_tlc_partial_candidate_sblock:
141206   > sblock:                3b
141207   > open_seq_num:         380
141208   > stream:                 0

141223 [6-0:0:0D|0:0:4:99]   scan_tlc_partial_candidate_sblock:
141224   > sblock:                55
141225   > open_seq_num:         385
141226   > stream:                 0

所以会导致3b在这里被当做是cold stream对待
141229 [6-0:0:0D|0:0:4:99]   partial_block_handle_get_block:
141230   > current_read_sblock:   3b
141231   > stream:                 0
141232   > last_maplog_pba:     101800077636c1
141233   > footer_pba:          ffffffffffffffff
141234   > last_prog_b2n_pba:    77637c0
141235   > final_user_b2n_pba:   77637c0
141236
141237
141238 [6-0:0:0D|0:0:4:99]   partial_block_handle_cold_stream_footer_scan:
141239   > cold_stream_block:         3b
141240   > scan_start_pba:      ffffffffffffffff

而且hot stream的0x55在这里由于是0，没有进行head extraction，所以这个block肯定也没有sealed，这会不会有问题？
141261 [6-0:0:0D|0:0:4:99]   partial_block_handle_get_block:
141262   > current_read_sblock:   55
141263   > stream:                 0
141264   > last_maplog_pba:     ffffffffffffffff
141265   > footer_pba:          ffffffffffffffff
141266   > last_prog_b2n_pba:    aa007c0
141267   > final_user_b2n_pba:  ffffffffffffffff
141268
141269
141270 [6-0:0:0D|0:0:4:99]   partial_block_handle_hot_stream_footer_scan:
141271   > hot_stream_block:          55
141272   > scan_start_pba:      ffffffffffffffff
141273   > hot_open_blk_index:     0
141274   > b2n_end_page:           0

前面restore的时候也是
140701 [6-0:0:0D|0:0:0:113]   no_need_restore_footer_meta_data:
140702   > hot_open_blk1:         55
140703   > hot_open_blk2:       ffffffff
140704   > hot_offset1:            0
140705   > hot_offset2:            0
140706   > hot_open_blk_cnt:       1


PC_CapacityOverlapMixWithPF.py

329299 [4-0:0:0D|1:14:48:352]   risk_low_space:
329300   > _usage_phy:          a32c068c80
329301   > _usage_logic:        f4c148a000
329302   > _total_logic_size:   f4c148a000
329303   > _total_phy_size:     a33f9c2000
329304   > _total_free_blk:            d
329305
329306
329307 [4-0:0:0D|1:14:48:352]   risk_board:
329308   > _action:             RISK_BOARD_SET_RISK
329309   > _evt_bitmap:             1000
329310
329311
329312 [0-0:0:0D|1:14:48:353]   err_resp:
329313   > _ctag:                1e9
329314   > _act:                 1e9
329315   > _opc:                   1
329316   > _state:              10000
329317   > _sct:                0
329318   > _sc:                 81
329319
329320
329321 [0-0:0:0D|1:14:48:353]   dump_cmdtbl:
329322   > _ctag:                1e9
329323   > _type:               abcdef00
329324   > _data:
329325         Dump memory: addr: 0x3fb15c8, len:0x80(decimal:128)
329326          0x0000000003FB15C8:  00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
329327          0x0000000003FB15E8:  00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000

```

#### 2022/5/30

```
1819357 [5-0:0:0D|4:49:23:297]   gsd_exit_warn:
1819358   > _type:     


PC_RandomWriteWithPFCrashRecovery.py
full recovery 超时

```

