```
console task
EVT_STATE_IDLE

evtlog_flush_ready()
_force_flush_flag：pf dump task在一开始的时候会设置为true

如果EVT_LOG(dummy_evtlog)走下去了，则_pause_when_idle = true;

EVT_STATE_SEND_WR_CMD
write_evt_data()
evt_key_info._stb_mgr.check_fcq_free_and_ocupy()会拿g_static_blk_lock
enum STATIC_BLOCK_TYPE_E
{
    STB_NORMAL_BLK,
    STB_EVT_LOG,
    STB_STATISTICS,
    STB_PANIC_DUMP,
    STB_COUNT,
};

    void release_lock() {
        g_static_blk_lock = STB_NORMAL_BLK;
        return;
    }
    
EVT_STATE_POLL_WR_CMPL
poll_write_cpl()
主要是看_wr_ctrl.pend_w2p_cnt和_wr_ctrl.pend_b2n_cnt


EVT_STATE_ERASE_DIE_BLOCK
evt_key_info._stb_mgr.check_fcq_free_and_ocupy()

EVT_STATE_POLL_ERASE_CMPL
```

```
fw_statistics::fw_statistics_snapshot_flush()
_stat_mgr_info._stb_mgr.check_fcq_free_and_ocupy()
```

```
BKOPS_TASK_STATE_FW_STAT_ERASE_DIE_BLOCK
g_fw_statistics._stat_mgr_info._stb_mgr.check_fcq_free_and_ocupy()

BKOPS_TASK_STATE_FW_STAT_READ_TRIG
g_fw_statistics._stat_mgr_info._stb_mgr.check_fcq_free_and_ocupy()
```

```c++
init_stb_die_info()
_die_info_initialized = true

mat_task::mat_prepare_stb_block(void)
// init evt block
// init fw statistics blk
// init panic block

static_blk_mgr::prepare_stb_die()
static_blk_mgr::poll_erase_cpl(u32 qid)
post_erase_cpl_handle()

bool console_task::post_erase_cpl_handle()
{
    u32 start_die_idx = evt_key_info._stb_mgr.get_cur_stb_die_idx();
    start_die_idx %= evt_key_info._stb_mgr.get_total_stb_die();
    bool ret = evt_key_info._stb_mgr.init_stb_die_info();
    for (u32 i = start_die_idx + 1; i < (evt_key_info._stb_mgr.get_total_stb_die() - 1); i++) {
        evt_key_info._log_entry_ctrl.flush_entries_cnt_of_die[i]
            = evt_key_info._log_entry_ctrl.flush_entries_cnt_of_die[i + 1];
    }
    if (evt_key_info._stb_mgr.stb_log_die_is_full()) {
        update_log_size();
    }
    evt_key_info._stb_mgr.upadte_next_stb_die();
    return ret;
}

bool fw_statistics::post_erase_cpl_handle()
{
    bool ret = _stat_mgr_info._stb_mgr.init_stb_die_info();

    // reduce snapshot count when erase a used block
    if (_stat_mgr_info._stb_mgr.stb_log_die_is_full()) {
        _stat_mgr_info._flushed_snapshot_cnt -= CFG_SLC_PAGE_NUM;
    }

    _stat_mgr_info._stb_mgr.upadte_next_stb_die();
    return ret;
}
```

```
lspci -d cc53: -vvv
```

```
static_blk_mgr:
    bool check_fcq_free_and_ocupy()
    {
        if( g_static_blk_lock != STB_NORMAL_BLK ) {
            if (g_static_blk_lock == _blk_type ) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            g_static_blk_lock = _blk_type;
            return true;
        }
    }

    void release_lock() {
        g_static_blk_lock = STB_NORMAL_BLK;
        return;
    }
    
console_task:
write_evt_data()
submit_b2n:
FCQ:FCQ_B2N_STATIC_BLK_SHARE
POOL_ID:FCT_POOL_B2N_SHARED

submit_w2p:
FCQ:FCQ_W2P_STATIC_BLK_SHARE
FCT:FCT_POOL_W2P_META_DATA

EVT_STATE_ERASE_DIE_BLOCK
erase_specific_blk_die
FCQ:FCQ_W2P_STATIC_BLK_SHARE
FCT:FCT_POOL_ERASE

console_task::read_pba
poll_read_cmpl
FCQ:FCQ_FPGA_TEST_RD
FCT:FCT_POOL_FLASH_RD


g_fw_statistics:
BKOPS_TASK_STATE_FW_STAT_ERASE_DIE_BLOCK
erase_specific_blk_die
FCQ:FCQ_W2P_STATIC_BLK_SHARE
FCT:FCT_POOL_ERASE

fct_idx = poll_single_erase_cpl(FCQ)
pool_id = GET_POOL_ID(fct_idx)

BKOPS_TASK_STATE_FW_STAT_FLUSH_TRIG
fw_statistics_snapshot_flush()
submit_b2n:
FCQ:FCQ_B2N_STATIC_BLK_SHARE
POOL_ID:FCT_POOL_B2N_SHARED

submit_w2p:
FCQ:FCQ_W2P_STATIC_BLK_SHARE
FCT:FCT_POOL_W2P

BKOPS_TASK_STATE_FW_STAT_READ_TRIG
trig_data_read()
FCQ:FCQ_W2P_STATIC_BLK_SHARE
FCT:FCT_POOL_FLASH_RD

g_nand_store
nand_panic_store
FCQ:
_b2n_qid:FCQ_B2N_STATIC_BLK_SHARE
_w2p_qid:FCQ_FPGA_TEST_WR
FCT:
_b2n_pool_id:FCT_POOL_B2N_SHARED
_w2p_pool_id:FCT_POOL_W2P

erase_sblock_and_poll_cpl_by_die
FCQ:FCQ_FPGA_TEST_ERASE
FCT:FCT_POOL_ERASE

console_task::read_pba
FCQ:FCQ_FPGA_TEST_RD
FCT:FCT_POOL_FLASH_RD
```

```
dump_mem(fct_entry, 0x400, DW_INT);
fct_idx   = GET_FCT_IDX(full_fct_idx);
pool_id   = GET_POOL_ID(full_fct_idx);
```

```
根据pool_id去申请一个fct_idx
fct_alloc_entries_idx(pool_id, &fct_idx, 1)
根据pool_id和申请到的fct_idx来获取fct_entry
fct_entry = fct_get_entry(pool_id, fct_idx)
将fct_entry和rd_idx相关联
fct_flash_rd_cmd::set_rd_idx(fct_entry, rd_idx)
fw_flash_read_cmd::read_slc_page_by_pba_to_buf(fct_entry, GEN_FULL_FCT_IDX(pool_id, fct_idx), qid,(u64)read_buf, pba, lp_bitmap, lpa, scheme, nxs,bad_plane_mask)

#define MAX_FCT_POOL_CNT 32

```

```
Panic dump ddr size in bytes 0x[3d9bf000], required min [22]die for panic dump

bkops_task::check_enter_idle1(void)
mat_task::mat_prepare_stb_block(void)
```

```
typedef enum
{
//#if (__WITH_SIMULATOR__ == 0)
    /* HW issue: when read read-only register of ACE, acq0 and fcq0 will be disabled.
       to avoid this issue, let's don't use fcq0, so switch FCQ_B2N_HOT_DATA to fcq1.
     */
    FCQ_B2N_HOT_DATA2 = 0,
    FCQ_B2N_HOT_DATA  = 1,
// #else
//     FCQ_B2N_HOT_DATA  = 0,
//     FCQ_B2N_HOT_DATA2 = 1,
// #endif

    FCQ_B2N_GC        = 2,
    FCQ_B2N_L2P_PATCH = 3,
    FCQ_B2N_WL        = 4,
    FCQ_B2N_L2P       = 5,
    FCQ_B2N_PF        = 6,
    FCQ_B2N_RFS       = 7,
    FCQ_B2N_FOOTER    = 8,

    FCQ_IO_ADMIN    = 9,
    FCQ_IO_RD_TASK0 = 10,
    FCQ_IO_RD_TASK1 = 11,
    FCQ_IO_RD_TASK2 = 12,
    FCQ_IO_RD_TASK3 = 13,
    FCQ_IO_WR_TASK0 = 14,
    FCQ_IO_WR_TASK1 = 15,
    FCQ_IO_WR_TASK2 = 16,
    FCQ_IO_WR_TASK3 = 17,

    FCQ_IO_GC_RD = 18,
    FCQ_IO_GC_WR = 19,
    FCQ_IO_WL_RD = 20,
    FCQ_IO_WL_WR = 21,

    FCQ_IO_RD_SCRUB = 22,

    FCQ_IO_PF       = 23,
    FCQ_IO_RECOVERY = 24,

    // FCQ_IO_COMPUTE0 = 25,
    // FCQ_IO_COMPUTE1 = 26,

    FCQ_IO_EH_RD = 25,
    FCQ_IO_EH_WR = 26,

    FCQ_ERASE      = 27,
    FCQ_NAND_ADMIN = 28,

    FCQ_IO_RFS_READ = 29,
    FCQ_ERASE_RFS   = 30,

    FCQ_IO_CKP_RD_C0 = 31,
    FCQ_IO_CKP_RD_C1 = 32,
    FCQ_IO_CKP_RD_C2 = 33,
    FCQ_IO_CKP_RD_C3 = 34,
    FCQ_IO_CKP_RD_C4 = 35,
    FCQ_IO_CKP_RD_C5 = 36,
    FCQ_IO_CKP_RD_C6 = 37,
    FCQ_IO_CKP_RD_C7 = 38,

    FCQ_IO_CKP_RD_SMRY = 39,
    FCQ_IO_PF_RD_SMRY  = 40,

    FCQ_IO_CKP_WR            = 41,
    FCQ_RAID_REBUILD         = 42,
    FCQ_IO_MAPLOG_WR         = 43,
    FCQ_IO_FOOTER_WR         = 44,
    FCQ_IO_RFS_WRITE         = 45,
    FCQ_FPGA_TEST_WR         = 46,
    FCQ_FPGA_TEST_RD         = 47,
    FCQ_FPGA_TEST_ERASE      = 48,
    FCQ_UNALIGNED_TRIM_WR    = 49,
    FCQ_PADDING_WR           = 50,
    FCQ_L2P_PATCH            = 51,
    FCQ_B2N_SHARED           = 52,
    FCQ_IO_CKP_RD_RETRY      = 53,
    FCQ_B2N_EH               = 54,
    FCQ_IO_ERR_VALIDATION    = 55,
    FCQ_ERR_VALIDATION_WR    = 56,
    FCQ_B2N_STATIC_BLK_SHARE = 57,
    FCQ_UNALIGNED_TRIM_RD    = 58,
    FCQ_W2P_STATIC_BLK_SHARE = 59,
    FCQ_IO_GC_RD_MAPLOG      = 60,
    FCQ_PADDING_FT_WR        = 61,
    FCQ_USED_CNT             = 62,
} FCQ_ID_E;


typedef enum
{
    FCT_POOL_WR_DMA_TRIG = 0,
    FCT_POOL_FLASH_RD = 1,
    FCT_POOL_GC_RD = 2,
    FCT_POOL_GC_WR = 3,
    FCT_POOL_B2N = 4,
    FCT_POOL_W2P = 5,
    FCT_POOL_ERASE = 6,
    FCT_POOL_NAND_ADMIN = 7,
    FCT_POOL_B2N_FLUSH = 8,
    FCT_POOL_B2B = 9,
    FCT_POOL_HDR_EXTRACTION = 10,
    FCT_POOL_DCS = 11,
    FCT_POOL_B2N_CKPT = 12,
    FCT_POOL_B2N_COLD = 13,
    FCT_POOL_WR_DMA_TRIG_C0 = 14,
    FCT_POOL_WR_DMA_TRIG_C1 = 15,
    FCT_POOL_WR_DMA_TRIG_C2 = 16,
    FCT_POOL_WR_DMA_TRIG_C3 = 17,
    FCT_POOL_FLASH_RD_C0 = 18,
    FCT_POOL_FLASH_RD_C1 = 19,
    FCT_POOL_FLASH_RD_C2 = 20,
    FCT_POOL_FLASH_RD_C3 = 21,
    FCT_POOL_B2N_SHARED = 22,
    FCT_POOL_W2P_META_DATA = 23,
    FCT_POOL_W2P_PATCH = 24,
    FCT_POOL_W2P_CKP = 25,
    FCT_POOL_PAGEIN_C0 = 26,
    FCT_POOL_PAGEIN_C1 = 27,
    FCT_POOL_PAGEIN_C2 = 28,
    FCT_POOL_PAGEIN_C3 = 29,
    FCT_POOL_USED_CNT = 30,
} FCT_POOL_ID_E;

/*
 ! STREAM_ID_STATIC_BLK_SHARE, FCQ_W2P_STATIC_BLK_SHARE, FCQ_B2N_STATIC_BLK_SHARE are share by all static blk proc
 ! all static blk proc task must alloc to same CORE !!!!!!
 ! a global lock is used to restrict only one task use these stream and fcq same time
 */
```

```c++
void mat_task::mat_prepare_stb_block(void)
{
    static_blk_mgr *stb_blk_mgr;
    g_pu_mgr.prepare_static_block();
    // init evt block
    stb_blk_mgr = def_console_task.get_stb_mgr();
    stb_blk_mgr->reset();
    u32 evt_blk = g_pu_mgr.get_static_block(0, STB_EVT_LOG);
    stb_blk_mgr->set_static_block(evt_blk, STB_EVT_LOG);
    stb_blk_mgr->init_stb_die_info();
    if (bsp_platform::spi_nor_4k_erase(CFG_NOR_EVT_LOG_META_START)) {
        INF_LOG("nor erase fail\n");
    }
    def_console_task.set_request_init();
    INF_LOG("evt blk %u\n", evt_blk);

    // init fw statistics blk
    u32 fw_stat_blk = g_pu_mgr.get_static_block(0, STB_STATISTICS);
    g_fw_statistics._stat_mgr_info._stb_mgr.reset();
    g_fw_statistics._stat_mgr_info._stb_mgr.set_static_block(fw_stat_blk, STB_STATISTICS);
    INF_LOG("fw stat blk %u\n", fw_stat_blk);
    g_fw_statistics._stat_mgr_info._stb_mgr.init_stb_die_info();

    // init panic block
    stb_blk_mgr = g_nand_store.get_stb_mgr();
    stb_blk_mgr->reset();
    // erase all panic dump die block
    bool is_slc    = false;
    u32  panic_blk = g_pu_mgr.get_static_block(0, STB_PANIC_DUMP);
    stb_blk_mgr->set_static_block(panic_blk, STB_PANIC_DUMP);
    INF_LOG("erase panic blk %u\n", panic_blk);
    erase_sblock_and_poll_cpl_by_die(panic_blk, is_slc);
    stb_blk_mgr->init_stb_die_info();
}


prepare_stb_die
_die_switch_flag
fw_statistics_snapshot_trig()
fw_statistics::post_b2n_cpl_handle(pba_t pba)
    PF_TEST_STATIC_BLK_LOCK_RACE_CONDITION_WRITE
    PF_TEST_STATIC_BLK_LOCK_RACE_CONDITION_ERASE
```

```
                "PF_RESTORE_START_READ_ROOT_DATA",
                "PF_RESTORE_READ_ROOT_DATA_SUCCESS",
                "PF_RESTORE_START_READ_SUMMARY_DATA",
                "PF_RESTORE_READ_SUMMARY_DATA_SUCCESS",
                "PF_RESTORE_START_READ_META_DATA",
                "PF_RESTORE_READ_META_DATA_SUCCESS",
                "PF_RESTORE_DATA_COPY_BACK_START",
                "PF_RESTORE_DATA_COPY_BACK_B2N_CMD_CPL",
                "PF_RESTORE_DATA_COPY_BACK_COMPLETE",
                "PF_RESTORE_PARITY_REBUILD_START",
                "PF_RESTORE_DATA_PAGE_IN_CMD_CPL",
                "PF_RESTORE_PARITY_REBUILD_COMPLETE",
                "PF_RESTORE_CLEAN_UP_NOR",
                "PF_RESTORE_CLEAN_UP_PF_RESERVED_BLOCK",
                "FTL_RECOVERY_L1_RECOVERY_BEFORE_L2P_PRE_INIT",
                "FTL_RECOVERY_L1_RECOVERY_AFTER_L2P_PRE_INIT",
                "FTL_RECOVERY_PATCH_DISPATCH_BEFORE_INIT_DIRTY_BITMAP",
                "FTL_RECOVERY_PATCH_DISPATCH_AFTER_INIT_DIRTY_BITMAP",
                "FTL_RECOVERY_ALL_PATCH_READ_SENT",
                "FTL_RECOVERY_IN_THE_MIDDLE_OF_MPAGE_LOAD",
                "FTL_RECOVERY_PARTIAL_BLOCK_HANDLING_START",
                "SUMMARY_RECOVER_SENT_SUMMARY_READ_CMD",
                "SUMMARY_RECOVER_PARSE_SUMMARY_START",
                "SUMMARY_RECOVER_SENT_HEADER_EXTRACTION",
                "ASSERT_GET_HOT_RECOVER_SBLOCK",
                "ASSERT_GET_FOOTER_RECOVER_SBLOCK",
                "ASSERT_GET_MPAGE_RECOVER_SBLOCK",
                "ASSERT_GET_PATCH_RECOVER_SBLOCK",
                "ASSERT_GET_COLD_RECOVER_SBLOCK"

```

```
[2022-05-27 14:32:37.015] PF
[2022-05-27 14:32:37.066] wait stat
[2022-05-27 14:32:37.066] ps 1
[2022-05-27 14:32:37.066] pw 0
[2022-05-27 14:32:37.067] ue cpl, op 0x65
[2022-05-27 14:32:37.067] ue cpl, op 0x65

typedef enum
{
    FCQ_MSG_OP_ACE_ERASE_CMD_MSG = 0x00,
    FCQ_MSG_OP_ACE_NAND_ADMIN_CMD_MSG = 0x0A,
    FCQ_MSG_OP_ACE_B2N_FLUSH_CMD_MSG = 0x1A,
    FCQ_MSG_OP_ACE_B2B_CMD_MSG = 0x1B,
    FCQ_MSG_OP_ACE_B2N_CMD_MSG = 0x64,
    FCQ_MSG_OP_ACE_W2P_CMD_MSG = 0x65,
    FCQ_MSG_OP_ACE_FLASH_RD_CMD_MSG = 0x66,
    FCQ_MSG_OP_ACE_GC_RD_CMD_MSG = 0x67,
    FCQ_MSG_OP_ACE_GC_WR_CMD_MSG = 0x68,
    FCQ_MSG_OP_ACE_DCS_CMD_MSG = 0x70,
    FCQ_MSG_OP_ACE_ERASE_CPL_MSG = 0x80,
    FCQ_MSG_OP_ACE_NAND_ADMIN_CPL_MSG = 0x8A,
    FCQ_MSG_OP_ACE_B2N_FLUSH_CPL_MSG = 0x9A,
    FCQ_MSG_OP_ACE_B2B_CPL_MSG = 0x9B,
    FCQ_MSG_OP_ACE_B2N_CPL_MSG = 0xE4,
    FCQ_MSG_OP_ACE_W2P_CPL_MSG = 0xE5,
    FCQ_MSG_OP_ACE_FLASH_RD_CPL_MSG = 0xE6,
    FCQ_MSG_OP_ACE_GC_RD_CPL_MSG = 0xE7,
    FCQ_MSG_OP_ACE_GC_WR_CPL_MSG = 0xE8,
    FCQ_MSG_OP_ACE_DCS_CPL_MSG = 0xF0
} FCQ_MSG_OP_ENUM;

FCQ_FPGA_TEST_ERASE
GLP_EvtLogChangeBlock.py
self.vuexe.printEvtLog(evtlog_id=0xde, evtlog_size=27311 * 1075)
def_console_task.evtlog_force_flush();
EVT_STATE_IDLE
```

```
目前考虑了并解决以下三种情况：
1、发生PF的时候，如果fw statistics正在erase block，会占用g_static_blk_lock，导致console task不能及时拿到g_static_blk_lock，影响evtlog flush的开始时间和pf dump的整体时间
2、发生PF的时候，如果fw statistics正在write block，也会占用g_static_blk_lock，导致console task不能及时拿到g_static_blk_lock
3、发生PF的时候，如果是console task正在erase block，由于erase的时间很长，会影响evtlog flush的开始时间和pf dump的整体时间

处理方法：
对第一种情况，fw statistics的erase使用FCQ_FPGA_TEST_ERASE的qid，与console task分开，从而也不需要与console task共用g_static_blk_lock
对第二种情况，考虑到write的耗时较少，可以等待fw statistics write完成，再继续evtlog flush和pf dump
对第三种情况，由于console task erase block的时间很长，不能等待其完成erase，而且erase block的时候，会调用g_nor_task，这个task在PF的时候是pause的，从而会hang住导致pf dump fail，故如果在pf dump的时候在低概率的情况下发生了die_switch并erase block，则停止console task flush evtlog


The following three scenarios are currently considered:
1. When PF occurs, if fw statistics is erasing block, g_static_blk_lock will be occupied, resulting in console task unable to obtain g_static_blk_lock in time, affecting the start time of evtlog flush and the overall time of pf dump
2. When PF occurs, if fw statistics is writing block, it will also occupy g_static_blk_lock, so that the console task cannot get g_static_blk_lock in time
3. When PF occurs, if the console task is in the process of erasing the block, since the erase time is very long, it will affect the start time of evtlog flush and the overall time of pf dump.

In the above cases, if the impact time is too long, the pf dump will fail, and the power-on recovery will enter freeze.

Approach:
1. For the first case, the erase of fw statistics uses the qid of FCQ_FPGA_TEST_ERASE, which is separate from the console task, so there is no need to share g_static_blk_lock with the console task.
2. For the second case, considering that write takes less time, we can wait for fw statistics write to complete, and then continue evtlog flush and pf dump
3. For the third case, because the console task erase block takes a long time, it cannot wait for it to complete the erase, and when the erase block is executed, g_nor_task will be called. This task is paused during PF, which will hang and cause PF dump fail, so if die switch and erase block occur with low probability during pf dump, stop console task flush evtlog

23a76b67c7f7ac65495dfef913a688454ba3d41f

Mars，之前evtlog，fw statistics，在erase和write的时候会使用同一个fcq，也使用同一把锁，会导致evtlog或fw statistics在erase或者write的时候，其他的拿不到锁，进行不下去，尤其是evtlog或者fw statistics erase的时候发生PF，会导致PF dump不下去，起来进freeze

MYR-1833，我在做这个feature的时候，把evtlog erase的时候发生PF导致PF dump失败的问题解决了，而且发现fw statistics在poll erase cpl的时候，无论是否成功，都把锁释放了，导致其他的static blk在poll erase cpl的时候会poll到fw statistics的cpl，导致PF dump的问题，这个在1833中一并修改了

昨天那个问题是因为在1833中，我把fw statistics改成和panic dump用的同一个fcq，但mat的时候fw statistics和panic dump看code虽然是串行执行的，但console task和idle task可能在同一个时间poll这个erase cpl，导致其中一个poll到另一个的erase cpl并释放了

我昨晚和yijun讨论后，觉得可以让evtlog，fw statistics和panic dump的erase用一个fcq，共享一把锁，让evtlog，fw statistics和panic dump的write用原来的那个fcq，和原本的那把锁，这样evtlog，fw statistics在erase的时候，不会影响PF dump，evtlog，fw statistics在write的时候，PF dump等待其完成就行。我和yijun找了个暂时没人用的fcq(FCQ_NAND_ADMIN)，但不知道这样修改会不会有问题，这方面有人可以方便支撑一下吗？

FCQ_IO_RD_TASK3
FCQ_IO_WR_TASK3
void fw_fcq_manager::init(void)
void fw_fcq_manager::init(void)

FCQ_ERASE
FCQ_ERASE_RFS
FCQ_FPGA_TEST_ERASE

    FCQ_IO_ADMIN    = 9,
    FCQ_IO_RD_TASK0 = 10,
    FCQ_IO_RD_TASK1 = 11,
    FCQ_IO_RD_TASK2 = 12,
    FCQ_IO_WR_TASK0 = 13,
    FCQ_IO_WR_TASK1 = 14,
    FCQ_IO_WR_TASK2 = 15,
    FCQ_ERASE_FW_STAT = 16,
    FCQ_ERASE_EVT_LOG = 17,
    
    
    
_die_info_initialized
pmic_capacitance_test_request
set_evtlog_write_ready
set_evtlog_read_ready

FCQ_NAND_ADMIN
fw_fcq_nand_admin
FCQ_ERASE_STATIC_SHARE
g_static_blk_erase_lock
check_erase_fcq_free_and_ocupy
release_erase_lock

81734939448e8f6ab7ee77359c9a4fc28bd60125

Adjust the health voltage check threshold of capacitor test
Separate the erase qid of fw statistics and evtlog from the write qid, and use a new lock to ensure security

Separating the qid of erase and write is because the erase time is long, in order to avoid fw statistics or evt cannot be flushed when fw statistics or evt is in erase, resulting in pf dump fail
```

