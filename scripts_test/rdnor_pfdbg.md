```
[2022-03-23 16:34:14] [5-0:0:0D|0:0:0:600]   root_data:
[2022-03-23 16:34:14]   > root_data_error_code:    0
[2022-03-23 16:34:14]   > pf_cnt:                 1
[2022-03-23 16:34:14]   > current_pf_sblock:      1
[2022-03-23 16:34:14]   > summary_data_pba:      401240
[2022-03-23 16:34:14]   > last_written_pba:      4017c0
[2022-03-23 16:34:14]   > nor_sub_sector_idx:     0
[2022-03-23 16:34:14]   > user_data_start_pba:   400000
[2022-03-23 16:34:14]   > user_data_end_pba:     400040
[2022-03-23 16:34:14]   > parity_data_start_pba:   400080
[2022-03-23 16:34:14]   > parity_data_end_pba:   4000c0
[2022-03-23 16:34:14]   > meta_data_start_pba:   400800
[2022-03-23 16:34:14]   > meta_data_end_pba:     401200
```

```
^[[0K[4-0:0:0D|0:2:1:92]   pf_restore_info:
^[[0K  > _restore_state:         0
^[[0K  > _read_meta_state:       3
^[[0K  > _read_summary_state:    3
^[[0K  > _read_root_data_state:    6
^[[0K  > _data_parser_sent_cnt:   28
^[[0K  > _copy_back_state:       b
^[[0K  > _data_rewriter_stream:    8
^[[0K  > _data_rewriter_copy_index:    1
^[[0K  > _data_rewriter_sent_cnt:   10
^[[0K  > _parity_rebuild_state:    e
^[[0K  > _parity_rebuilder_stream:    8
^[[0K  > _parity_rebuilder_sent_cnt:   10
^[[0K  > _nor_cleanup_state:     9
^[[0K
^[[0K
^[[0K[4-0:0:0D|0:2:1:92]   recovery:
^[[0K  > _recovery_mode:         3
^[[0K  > _pre_state:             6
^[[0K  > _state:                 6
^[[0K  > _basetime:            737b931

^[[0K[4-0:0:0D|0:2:1:92]   ftl_recovery_info:
^[[0K  > _ftl_state:             5
^[[0K  > _full_recovery_state:    0
^[[0K  > _start_read_pba:      fe00000
^[[0K  > _end_read_pba:        ff27800
^[[0K  > _last_read_pba:      18867f8c
^[[0K  > _current_read_pba:    fe35f50
^[[0K  > _sent_read_cnt:        60
^[[0K  > _received_read_cnt:    60
^[[0K  > _l1_recovery_state:     5
^[[0K  > _summary_block_cnt:     3
^[[0K  > _summary_block_index:    3
^[[0K  > _mpage_load_state:      3
^[[0K  > _current_l1_id:      7a60b
^[[0K  > _partial_blk_handle_state:    b
^[[0K  > _partial_block_cnt:     3
^[[0K  > _partial_block_index:    3
^[[0K  > _full_drive_scan_state:    0
^[[0K  > _data_block_cnt:        0
^[[0K  > _data_block_index:      0
^[[0K  > _pending_read_cnt:      0
^[[0K  > _pending_read_index:    0
^[[0K  > _pending_update_cnt:    0
^[[0K  > _pending_update_index:    0
^[[0K  > _cur_lpa:            ffffffffffffffff
^[[0K  > _cur_pba:            ffffffffffffffff
^[[0K  > _patch_dispatch_state:    6
^[[0K  > _patch_block_cnt:       2
^[[0K  > _patch_block_index:     2
^[[0K  > _patch_parse_mode:          0
^[[0K  > _current_read_operation:    0
^[[0K  > _current_dispatch_operation:    0
^[[0K  > _ping_operation_state:    4
^[[0K  > _ping_sent_read_cnt: c088
^[[0K  > _ping_received_read_cnt: c088
^[[0K  > _ping_sent_parse_cnt: 2ffc6
^[[0K  > _ping_received_parse_cnt: 2ffc6
^[[0K  > _pong_operation_state:    4
^[[0K  > _pong_sent_read_cnt: bf80
^[[0K  > _pong_received_read_cnt: bf80
^[[0K  > _pong_sent_parse_cnt: 2fd38
^[[0K  > _pong_received_parse_cnt: 2fd38

```

```
rdmatfooter
clearpanicdump
setbootoption 1
nxs：nand transform size
```

```
this case current IOPS count 102022-04-02 04:33:17,086 INFO Send read command from slba 1033090304 * 512 and dmc slba 1033090424 nsid 1

#### NAND READ ERR
PC_RandomWriteWithPFCrashRecovery.py
```

