### B47 test(84)
**DP_IOWith4KWithRandomSize.py                Pass**
DP_4KAlignedRandomWriteReadCmp.py           Pass
DP_4KUnalignedSequentialWriteReadCmp.py     Pass
DP_AsyncIOWithRandomSize.py                 Pass

/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorBasicTest.py                    Pass
**/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorGCTarget.py                     Pass**
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorOnBlockStart.py                 Pass
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorOnSLCMLCTLCOpenParity.py        Pass
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorGSDPFRead.py                    Can't test

DP_TCGWithMultiRangesByFIO.py



### basic read/write
    script                                                  cost
DP_RandomWriteMixSequentialRead.py                          55s

DP_AsyncIOWithOverLap.py                                    1min
DP_AsyncIOMixThreadsWithoutCompare.py                       5min
DP_AsyncIOWithRandomSize.py                                 5min
DP_IOMixAdminWithThreads.py                                 5min

DP_4KAlignedRandomWriteReadCmp.py                           30min   Pass
DP_4KAlignedSequentialWriteReadCmp.py                       30min

DP_4KUnalignedRandomWriteReadCmp.py                         30min
DP_4KUnalignedSequentialWriteReadCmp.py                     30min

DP_MultiReadOnOneBlockAndErase.py                           30min
DP_MultiReadOnOneParityAndEraseOnMulitiBlocksMultidie.py    38min
DP_MultiReadOnDiffBlockAndErase.py                          50min

DP_ReadAfterWriteWithFlush.py                               12hour
DP_8KLOCWriteAndRead.py                                     5hours
DP_IOWith4KWithRandomSize.py                                19hour
DP_NormalTriggerGC.py                                       5hour

------------------------------Data Path Test---------------------------
SHORTSTROKE
DP_AsyncIOWithBigSize
    32sec   Pass
DP_IOWith5K
    0.03sec Pass
DP_IOWith16K
    57min   Pass
DP_IOWith64K
    2hour   Pass
DP_4KUnalignedSequentialWriteReadCmp
    3min    Pass
DP_4KUnalignedRandomWriteReadCmp
    3min    Pass
DP_4KAlignedSequentialWriteReadCmp
    3min    Pass
DP_4KAlignedRandomWriteReadCmp
    5min    Pass

STANDARD
DP_MultiReadOnDiffBlockAndErase.py
    hang (reproducing now)
-----------------------------------------------------------------------

### error handle
#### erase error
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_EraseErrorBasicTest.py           15min
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_EraseErrorByEraseDies.py         20min
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_MultiEraseError.py               30min
#### program error
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ProgramErrorEraseErrorFormatTrim.py  10min
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ProgramErrorWithTrim.py              10min

/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ProgramErrorOnMuiltiPagesOnDifferentDie.py   30min

/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ProgramErrorOnDifferentPageTypeOnMlcPage.py  30min
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ProgramErrorOnDifferentPageTypeOnTlcPage.py  30min

/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ProgramErrorOnOneDieBlock.py         30min

/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ProgramErrorBasicStress.py           1hour
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ProgramErrorEHTarget.py              1hour
#### read error
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorByEraseDie.py                           20min

/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorOnFooterPba.py                          20min
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorOnSealedBLK.py                          20min
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorOnSLCMLCTLCOpenParity.py                1hour 40min
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorOnStripeStart.py                        17sec
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorOnBlockStart.py                         45min

/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_MultiReadErrorGCSource.py                        30min
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorOnDiffPageTypeOnOpenParityTlcPage.py    35min

/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorWithMultiBlocks.py                      1hour
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorWithMultiPages.py                       2hour

/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorPlaneBasic.py

/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadRetryBasicTest.py                                    
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadRetryOnBlockStart.py
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadRetryOnDiePbas.py


#### mixed
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ProgramErrorMeetReadError.py                     10min
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ProgramErrorMeetMulitiReadError.py               25min


---------------------------ErrorHandle Test --------------------------
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorBasicTest.py
    40min       Pass
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadRetryBasicTest.py                            
    24min       Pass
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorOnStripeStart.py                        
    15min       Pass

/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ReadErrorOnDiffPageTypeOnOpenParityTlcPage.py    
    timeout (generate_open_parity只有10分钟的时间， 需要改脚本)
/home/tcnsh/samanea/Samanea_Scripts/ErrorHandle/EH_ProgramErrorOnMuiltiPagesOnDifferentDie.py
    todo 
-----------------------------------------------------------------------

-----------------------------------------------------------------------
nightly regression
EH_ReadErrorWithMultiBlocks.py              loop.1 pass  4hour
-----------------------------------------------------------------------


### B47 test
DP_IOWith4KWithRandomSize.py                Pass
DP_4KAlignedRandomWriteReadCmp.py           Pass
DP_4KUnalignedSequentialWriteReadCmp.py     Pass
DP_AsyncIOWithRandomSize.py                 Pass
EH_ReadErrorBasicTest.py                    Pass
EH_ReadErrorGCTarget.py                     Pass
EH_ReadErrorOnBlockStart.py                 Pass
EH_ReadErrorOnSLCMLCTLCOpenParity.py        Pass

### BICS5 test
ErrorHandle/EH_ReadErrorBasicTest.py Pass
ErrorHandle/EH_ReadRetryBasicTest.py Pass
ErrorHandle/EH_ReadErrorOnBlockStart.py Pass
ErrorHandle/EH_ReadErrorOnSLCMLCTLCOpenParity.py Pass
ErrorHandle/EH_ReadErrorGCTarget.py Pass
DP_4KAlignedRandomWriteReadCmp.py           Pass
DP_4KAlignedSequentialWriteReadCmp.py Pass
DP_4KUnalignedSequentialWriteReadCmp.py     Pass
DP_4KUnalignedRandomWriteReadCmp.py Pass
DP_MultiReadOnOneBlockAndErase.py Pass
DP_MultiReadOnDiffBlockAndErase.py Pass
DP_8KLOCWriteAndRead.py Pass

