                                                asic                fpga 
========================================    Basic Test    ========================================
erase(1)
ft *erase.basic                                 Pass                Pass

w2p(8)
ft *w2p.basic                                   Pass                Pass
ft *w2p.double_program                          Pass                Pass
ft *w2p.multi_stream                            Fail                Pass
ft *w2p.dataerr                                 Pass                Pass
ft *FPGA.multi_q                                NS                  Pass
ft *w2p.no_fuse                                 Pass                Pass
ft *w2p.fuse                                    Pass                Pass
ft *w2p.fuse_compress                           Pass                Pass
ft *w2p.fuse2                                   ASSERT              NS

B2N(6)
ft *B2N.b2n_send                                Pass                Pass
ft *B2N.b2n_gts_send                            Pass                Pass
ft *B2N.b2n_stripe_send                         Hang                Pass
ft *B2N.b2n_raid_cmd_send                       Pass                Pass
ft *B2N.b2n_skip_send                           Pass                Pass    // case null 
ft *B2N.b2n_mode1_flush                         Hang                Pass      

SLC(7)
ft *FPGA.slc_test                               TODO
ft *SLCT.pre => e                               Pass                Pass
ft *SLCT.sp => w r                              Pass                Pass
ft *SLCT.mp => w r                              Pass                Pass
ft *SLCT.vsc => e w r                           Pass                Pass
ft *SLCT.vsc1 => e w r                          Pass                Pass       
ft *SLCT.vsc2 => e w r                          Pass                Pass


B2B(5)
ft *B2BTest.ss                                  Pass                Pass
ft *B2BTest.ss2                                 Pass                Script Need Change, same as B2BTest.ss, dont need test

========================================    Enhancement    ========================================

MISC(4)
ft *FPGA.bad_pl_test                            Fail                Assert (LIB_VEC_INDEX_WRONG)
ft *CRCTest.gen_crc                             Pass                Pass
ft *FPGA.encrypt_test                           Fail                Pass

MPage(2)
ft *FPGA.mpage_multi_lba_test                   Pass                Pass
ft *FPGA.mpage_smry_test                        Pass                Pass

EH(7)
ft *FPGA.progerr_test                           Fail                Pass
ft *FPGA.eraseerr_test                          Fail                Pass
ft *FPGA.readerr_test                           Pass                Pass
ft *FPGA.raid_test                              Pass                Pass
ft *RAIDTest.basic_raid    Manual               Pass                Pass
ft *RAIDTest.raid_rebuild  Manual               Pass                Pass
ft *RAIDTest.raw_read                           Pass                Pass
ft *RRB.t0                 Manual               Task Test           

GC(15)
ft *flashrd.basic                               Pass                Pass
ft *flashrd.read_retry                          Fail                Pass
ft *flashrd.lba_mismatch                        Pass                Pass
ft *gc.gc_test                                  ASSERT              Pass
ft *gts.gts_basic_flash_rd                      Pass                Pass
ft *gts.gts_basic_gc_rd                         Pass                Pass
ft *gts.gts_base_gc_wr                          Pass                Pass
ft *gts.gts_header_extraction                   Pass                Pass
ft *gts.gts_gc_rd_wr                            Pass                Pass
ft *gts.gts_test_single                         Pass                Pass
ft *gts.gts_test_multi                          Pass                Pass
ft *hdr_extra.hdr_extra_simple_test             Pass                Pass
ft *hdr_extra.hdr_extra_tlc_slc                 Pass                Pass
ft *hdr_extra.hdr_extra_compression Manual      Pass                Pass
ft *hdr_extra.hdr_extra_special_pattern Manual  Denied Access RFS

PF(2), only support fpga testSLC
ft *FPGA.pf_with_vqid_write Manual              Pass                Pass
ft *FPGA.pf_with_hw_abort   Manual              Hang(50%)           Pass

========================================    New FT    ========================================
B2N
ft *B2N.b2n_mode1_flush_0                       Pass                Pass
ft *B2N.b2n_mode1_flush_1                       Hang                Pass
ft *B2N.b2n_pagetype_ccp_limit                  Hang                Pass

EH
ft *flashrd.b0_overall_ecc_status               Fail                Pass
ft *flashrd.b0_overall_lba_mismatch             Fail                Pass
ft *flashrd.b0_uncompress_out_order             Fail                Pass
ft *flashrd.b0_reread                           Fail                Pass
ft *flashrd.b0_read_retry                       Pass                Pass


ft *FPGA.fcq_zero_test                          Hang                Pass
