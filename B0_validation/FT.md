```c++
FPGA_TEST(compressTest, basic, BIT(0))

FPGA_TEST(compressTest, basic, BIT(0))
{
    COLOR_LOG(COLOR_GREEN, "######## compression_test %s ########\n", __FUNCTION__);
    const u32 block = 0x2d;    // test on this block
    pba_t b2n_pba = {.all = 0};
    b2n_pba.block = block;
    INF_LOG("erase block: 0x%x\n", block);
    ft_erase_sblock(block);
    fpga_wait_erase_cq_msg(CFG_CHAN_NUM * CFG_CE_NUM * CFG_LUN_NUM);

    g_comp_ft_start_lba = 1;
    // write
    ASSERT_EQ(true, send_single_b2n_cmd(b2n_pba.all));
    //g_be_mgr.set_block_stream(b2n_pba.block, STREAM_ID_HOT);
    u32 page_type_num  = g_be_mgr.get_page_type_num(false, b2n_pba.block, b2n_pba.page);
    u32 w2p_num_of_b2n = page_type_num * CFG_LUN_PAGE_SIZE;
    test_compressible_data(g_write_buf_du, g_comp_ft_start_lba, 0, w2p_num_of_b2n);
    fpga_wait_w2p_cq_msg(w2p_num_of_b2n, STREAM_ID_HOT, check_w2p_cpl_compressible);
    fpga_wait_b2n_cq_msg(1);

    // read
    read_incompressible_data(b2n_pba.all, g_comp_ft_start_lba);
    fpga_wait_flash_rd_cq_msg(w2p_num_of_b2n, g_write_buf_du, check_flash_rd_cpl_comp_ft);
}
```

```c++
bool fpga_test_write_data(u8 *write_buf, u64 start_lba, u8 &data_pattern, u8 compression_pattern, 
                          u8 cnt, u32 stream, u32 qid, u64 *raid_check)
{
    u8 fct_idx;
    u8 fourk_idx;
    u64 buff_addr;
    u64 lba;
    void *w2p_entry = nullptr;

    COLOR_LOG(COLOR_GREEN, "### %s write data from lba 0x%llx to 0x%llx ###\n",
              __FUNCTION__, start_lba, (start_lba + cnt - 1));

    for (u32 i = 0; i < cnt; i++) {

        while (1 != fct_alloc_entries_idx(FCT_POOL_W2P, &fct_idx, 1)) {
            COLOR_LOG(COLOR_GREEN, "### %s alloc entry fail, looping...\n", __FUNCTION__);
        }

        w2p_entry = fct_get_entry(FCT_POOL_W2P, fct_idx);
        fourk_idx = i;
        memset(write_buf + (fourk_idx * 4096), compression_pattern, 1);
        memset(write_buf + (fourk_idx * 4096) + 1, data_pattern, 4096 - 1);
        data_pattern++;
        buff_addr = (u64)(write_buf + (fourk_idx * 4096));
        lba = fourk_idx + start_lba;

        while(!fw_w2p_cmd::send_normal_wr_cmd(w2p_entry, ((FCT_POOL_W2P << 7) | fct_idx),
            stream, buff_addr, lba, qid, (compression_pattern == 0))) {
            COLOR_LOG(COLOR_GREEN, "### %s send_normal_wr_cmd, looping...\n", __FUNCTION__);
        }
        if (raid_check) {
            *(raid_check + i) ^= *(u64 *)(write_buf + (fourk_idx * 4096));
            DBG_LOG("raid chk buf: i: %d, 0x%llx\n",
                    i, *(raid_check + i));
        }
    }

    return true;
}
```

```
Name:	sdsw_control
Address:	0x00000000
Reset Value:	0x01021840 (undefined is taken as 0)
Access:	read-write
Description:	Sdsw Control Register.

[21:20]	byps_dcmpr_eng	read-write	0x0	1,bypass decompressor engines to reduce write path latency. 0,enable decompressor for compressed data check

[3:0]	byps_cmprs_engs	read-write	0x0	bit[i]: 1, bypass cmprs engine i for debug, data still driven to compr engine, compr engine will force to generate uncompressed blocks.
```

```
    SDSW_CONTROL_U sdsw_control_u;
    sdsw_control_u.data = SDSW_CSR_AUTO_RD(SDSW_CONTROL);
    pr_dbg("byps_dcmpr_eng = 0x%x\n", sdsw_control_u.bits.byps_dcmpr_eng);
    pr_dbg("byps_comprs_eng = 0x%x\n", sdsw_control_u.bits.byps_cmprs_engs);
    
    1、byps_cmprs_engs 1，可以跑压缩的数据 no
    2、byps_cmprs_engs 1，不能跑压缩的数据 yes
    3、byps_cmprs_engs 1，可以跑不压缩的数据 yes
    4、byps_cmprs_engs 1，不能跑不压缩的数据 no
    5、byps_cmprs_engs 0，可以跑压缩的数据 yes
    6、byps_cmprs_engs 0，不能跑压缩的数据 no
    7、byps_cmprs_engs 0，可以跑不压缩的数据 yes
    8、byps_cmprs_engs 0，不能跑不压缩的数据 no
    
    getbypassvalue
    setbypassvalue byps_cmprs_engs byps_dcmpr_eng
    ft *compressTest.basic
    ft *compressTest.rate2
    
    压缩数据：0x80000b400000
    非压缩数据：0x2030000b400000
    0x2030000b400000
```

```c++
// SDSW_CONTROL
typedef union {
    u32 data;
    struct {
        u32 byps_cmprs_engs:4;
        u32 mark_err_in_lba:1;
        u32 aes_encryp_dis:1;
        u32 page_unit:2;
        u32 rsc_th_cmprs:6;
        u32 rsc_th_byps:6;
        u32 byps_dcmpr_eng:2;
        u32 sdsw_buf_axi_rd_en:1;
        u32 sdsw_dcmp_err_inj_en:1;
        u32 sdsw_dcmp_err_inj_mode:1;
        u32 Reserved0:7;
    } bits;
} SDSW_CONTROL_U;
```

```
s32 hw_init_base_type::sdsw_reg_pre_cnfg(void)
{
    // bypass two decompressors in the write path
    // SDSW_CSR_AUTO_WR_BITS(SDSW_CONTROL, 21, 20, 0x3);

    // skip obuf sanity check
    u32 val = SDSW_CSR_AUTO_RD(SDSR_CTRL);
    SDSW_CSR_AUTO_WR(SDSR_CTRL, val | 0x1);
    return 0;
}
```

```
#define SDSW_CSR_AUTO_BASE_ADDR     (0x231A0000)
#define SDSW_CONTROL        (0x000) /* Sdsw Control Register. */

#define SDSW_CSR_AUTO_RD(reg_off)           bsp_platform::reg_rd32(SDSW_CSR_AUTO_BASE_ADDR + reg_off)
#define SDSW_CSR_AUTO_WR(reg_off, value)    bsp_platform::reg_wr32(SDSW_CSR_AUTO_BASE_ADDR + reg_off, value)
```

```
pba_to_nand_addr
```

