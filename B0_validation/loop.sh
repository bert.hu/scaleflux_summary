#! /bin/sh

for script in `cat $1`
do
	if [[ $script == "#"* ]]
	then
		echo SKIP $script
		continue
	fi

	if test -e $script
	then
		echo TEST $script START
		sudo python3 $script
		echo TEST $script END
	else
		echo $script doesnt exist!!!
		continue
	fi

	if test $? -eq 0
	then
		echo TEST $script SUCCESS
	else
		echo TEST $script FAILED, NEED DEBUG
		exit 1
	fi
	
done
