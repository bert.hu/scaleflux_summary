```
416126 [6-0:0:0D|0:0:32:290] recovery_tas:           Full Scan Progress: 0 / 105761
bsp_platform::get_opn()
idfy:UD2150C0065M@CSDU4SPC38A0

（费用分析用）
邮箱帐号 wudahht@163.com

联系手机 176***0693

查询网站 u.163.com/t1/8cRyU

fbid = bsp_platform::get_fbid_from_gpio()

enum {
    FBID_BOARD_VERSION_0 = 0, // A11
    FBID_BOARD_VERSION_1,     // A12
    FBID_BOARD_VERSION_2,     // A13/A21
    FBID_BOARD_VERSION_3,
    FBID_BOARD_VERSION_4,     // B21/B11
    FBID_BOARD_VERSION_5,     // M11
    FBID_BOARD_VERSION_6,
    FBID_BOARD_VERSION_7,
    FBID_BOARD_VERSION_8,
    FBID_BOARD_VERSION_9,
    FBID_BOARD_VERSION_A,     // C11
    FBID_BOARD_VERSION_B,     // C21
    FBID_BOARD_VERSION_C,     // C31
    FBID_BOARD_VERSION_D,
    FBID_BOARD_VERSION_E,
    FBID_BOARD_VERSION_F,
    FBID_BOARD_VERSION_BUTT
} FBID_BOARD_VERSION;

change cap start:f4c148a, f4c148a
new_total_lpa_num f4c148a
cur_total_lpa_num f4c148a

600200 [6-0:0:0D|0:0:0:483]   l2p_init:^M
600201   > _event:              INIT_START^M
600202   > _value:                   41b^M
600203   > _l2p_prov_cap:            2bd^M
600204   > _l2p_fmt_cap:             41b^M

g_l2p_mgr._provision_capacity_in_GB 2bd(701GB),实际用户可用的物理容量
g_l2p_mgr._format_capacity_in_GB 41b(1051GB)，实际用户可用的逻辑容量(用户最关心的)

l2p_update_format_cap: fullcache old 1 new 1,fmt_cap old 1051 new 1051,L1_MAP_ENTRY_NUM 501259,max_cache_l2_num 606569

600210 [6-0:0:0D|0:0:0:483]   l2p_init:^M
600211   > _event:              INIT_L1_MAP^M
600212   > _value:                     0^M
600213   > _l2p_prov_cap:            2bd^M
600214   > _l2p_fmt_cap:             41b^M
600215 ^M
600216 ^M
600217 [6-0:0:0D|0:0:0:512]   l2p_init:^M
600218   > _event:              INIT_L2_MAP^M
600219   > _value:                 77201^M
600220   > _l2p_prov_cap:            2bd^M
600221   > _l2p_fmt_cap:             41b^M
600222 ^M
600223 ^M
600224 [6-0:0:0D|0:0:0:526]   l2p_init:^M
600225   > _event:              INIT_FREE_POOL^M
600226   > _value:                 91e12^M
600227   > _l2p_prov_cap:            2bd^M
600228   > _l2p_fmt_cap:             41b^M
600229 ^M
600230 ^M
600231 [6-0:0:0D|0:0:0:528]   l2p_init:^M
600232   > _event:              INIT_CHECK_GDMA^M
600233   > _value:                     0^M
600234   > _l2p_prov_cap:            2bd^M
600235   > _l2p_fmt_cap:             41b^M
600236 ^M
600237 ^M
600238 [6-0:0:0D|0:0:15:945]   l2p_init:^M
600239   > _event:              INIT_FIN^M
600240   > _value:                   41b^M
600241   > _l2p_prov_cap:            2bd^M
600242   > _l2p_fmt_cap:             41b^M

600245 2K_MPAGE 0, RAW_CAPACITY 0xc800000000, DDR_SIZE 3221225472, provision_capacity 701, format_capacity 1051, cache ratio 100, MAX_LBA 0x7a60a44f, MAX_LPA 0xf4c1489^M
600246 L1_MAP_ENTRY_NUM 0x7a60b, L2_MAP_ENTRY_NUM 0x7a60b, max_cache_l2_num 0x94169, MAX_CACHE_L2_MAP_ENTRY_NUM 0x94169, DEFAULT_L1_MAP_ENTRY_NUM 0x519fd, MAX_L1_MAP_ENTRY_NUM 0xa33f5, MAX_DIRTY_LI       ST_LEN 0x37887^M
600247 DDR_L2P_LOCK_SIZE 0x28cfd4, DDR_L1_MAP_SIZE 0x51a000, DDR_L2_MAP_SIZE 0x94169000, DDR_L2_NODE_SIZE 0x941690, ALLOC_L2_MAP_SIZE 0x94169000^M
600248 ptr: lock 0x42a82e000, l1_map 0x42b03b000, l2_map 0x42be97000, l2_node 0x42b555000^M


```

```c++
void rcvry_pf_ftl_ctrl::scan_tlc_data_candidate_sblock()
{
    DBG_LOG("Search tlc data sblock..\n");
    for (u32 sblock = CFG_START_USER_BLOCK; sblock < CFG_BLOCK_NUM; sblock++) {
        if (((g_pu_mgr.pblk_mgr[0].get_sblock_status(sblock) == BS_SEALED_BLOCK) ||
             (g_pu_mgr.pblk_mgr[0].get_sblock_status(sblock) == BS_OPEN_BLOCK))) {
            u8 stream = g_pu_mgr.pblk_mgr[0].get_sblock_stream(sblock);
            if ((stream == STREAM_ID_HOT) || IS_COLD_STREAM(stream)) {
                append_read_candidate_sblock(sblock, _data_blocks, _data_block_cnt);
                BOMB(READ_CANDIDATE_FIFO_FULL, _data_block_cnt <= CFG_BLOCK_NUM);
                DBG_LOG("Find a tlc data candidate block: %d, seq: %lld, stream: %d\n", sblock,
                        g_pu_mgr.pblk_mgr[0].get_sblock_open_seq_num(sblock), stream);
                EVT_LOG(scan_tlc_data_candidate_sblock_evtlog, sblock,
                        g_pu_mgr.pblk_mgr[0].get_sblock_open_seq_num(sblock), stream);
            }
        }
    }
}
```

```c++
void rcvry_pf_ftl_ctrl::scan_tlc_partial_candidate_sblock()
{
    DBG_LOG("Search tlc partial sblock..\n");
    for (u32 sblock = CFG_START_USER_BLOCK; sblock < CFG_BLOCK_NUM; sblock++) {
        if (g_pu_mgr.pblk_mgr[0].get_sblock_status(sblock) == BS_OPEN_BLOCK) {
            u8 stream = g_pu_mgr.pblk_mgr[0].get_sblock_stream(sblock);
            if (IS_HAS_FOOTER_STREAM(stream) && (!IS_B2N_TASK_SKIPPED_STREAM(stream))) {
                append_read_candidate_sblock(sblock, _partial_blocks, _partial_block_cnt);
                BOMB(READ_CANDIDATE_FIFO_FULL, _partial_block_cnt <= MAX_READ_CANDIDATE_SBLOCK);
                DBG_LOG("Find a tlc partial candidate block: %d, seq: %lld, stream: %d\n", sblock,
                        g_pu_mgr.pblk_mgr[0].get_sblock_open_seq_num(sblock), stream);
                EVT_LOG(scan_tlc_partial_candidate_sblock_evtlog, sblock,
                        g_pu_mgr.pblk_mgr[0].get_sblock_open_seq_num(sblock), stream);
            }
        }
    }
}

Exec VU: vu unlock
FULL_DRIVE_SCAN_STATE_UPDATE_L2P
(g_recovery_task.get_force_full_recovery() && g_recovery_task.get_need_enable_tasks())
```

```c++
12404 [6-0:0:0D|0:28:28:892] recovery_tas:          Full Scan Progress: 37151 / 37151

12515 [4-0:0:0D|0:35:23:438]   changecap_task:^M
12516   > _event:              INITED_TO_WAIT_CMD^M
12517   > _state:              CHANGE_CAP_TASK_STATE_INIT^M
12518   > _pre_state:          CHANGE_CAP_TASK_STATE_INIT^M
12519   > _cmd_cpl_status:     CHANGE_CAP_NO_ERROR^M
12520   > _type:               CHANGE_CAP_TYPE_INVALID^M
12521   > _job_obj:            CHANGE_CAP_OBJ_PROVISION^M
12522   > _job_caller:         CHANGE_CAP_CALLER_DEFAULT^M
12523   > _job_cap_in_gb:             0^M
12524   > _provision_capacity_in_GB:      2bd^M
12525   > _format_capacity_in_GB:      57a^M
12526   > _full_cache_flag:    false^M
12527   > _cmd_cpl_flag:       false^M
12528   > _rfs_comp_signal:       0^M
12529   > _old_l1_map_num:        0^M
12530   > _ckp_event_handled:     0^M
12531   > _folding_clear:         0^M
12532   > _format_cpl:            0^M
12533   > _vu_stop_bitmap:            0^M



2022-08-13 23:20:54,955 INFO IO --- FullSectorCmp: Data miscompare at offset 0x0 and lcnt 0x0 (0) DMC LPA 0x1279846e DMC LBA 0x93cc2370 (2479629168)
2022-08-13 23:20:54,955 INFO IO ---------Dump Write Buffer---------
2022-08-13 23:20:54,955 INFO              0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f 10 11 12 13 14 15 16 17 18 19 1a 1b 1c 1d 1e 1f
2022-08-13 23:20:54,955 INFO             -----------------------------------------------------------------------------------------------
2022-08-13 23:20:54,955 INFO
2022-08-13 23:20:54,955 INFO 0x00000000: 65 6c 74 73 2e 78 6d 6c 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
2022-08-13 23:20:54,955 INFO 0x00000020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
2022-08-13 23:20:54,955 INFO 0x00000040: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
2022-08-13 23:20:54,955 INFO 0x00000060: 00 00 00 00 31 30 30 37 37 35 20 00 20 20 20 37 36 34 20 00 20 20 20 37 36 34 20 00 20 20 20 20
2022-08-13 23:20:54,955 INFO 0x00000080: 20 33 33 34 37 35 37 20 20 37 31 37 34 33 36 36 33 37 30 20 20 31 32 32 30 31 00 20 30 00 00 00
2022-08-13 23:20:54,955 INFO 0x000000a0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
2022-08-13 23:20:54,955 INFO 0x000000c0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
2022-08-13 23:20:54,955 INFO 0x000000e0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
2022-08-13 23:20:54,955 INFO IO ---------Dump Read Buffer---------
2022-08-13 23:20:54,955 INFO              0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f 10 11 12 13 14 15 16 17 18 19 1a 1b 1c 1d 1e 1f
2022-08-13 23:20:54,955 INFO             -----------------------------------------------------------------------------------------------
2022-08-13 23:20:54,955 INFO
2022-08-13 23:20:54,955 INFO 0x00000000: 22 4b 65 6c 76 69 6e 22 3e 31 38 36 30 3c 2f 42 4f 49 4c 49 4e 47 5f 50 4f 49 4e 54 3e 0d 20 20
2022-08-13 23:20:54,955 INFO 0x00000020: 20 20 3c 4d 45 4c 54 49 4e 47 5f 50 4f 49 4e 54 20 55 4e 49 54 53 3d 22 4b 65 6c 76 69 6e 22 3e
2022-08-13 23:20:54,955 INFO 0x00000040: 39 30 33 2e 39 31 3c 2f 4d 45 4c 54 49 4e 47 5f 50 4f 49 4e 54 3e 0d 20 20 20 20 3c 53 59 4d 42
2022-08-13 23:20:54,955 INFO 0x00000060: 4f 4c 3e 53 62 3c 2f 53 59 4d 42 4f 4c 3e 0d 20 20 20 20 3c 44 45 4e 53 49 54 59 20 55 4e 49 54
2022-08-13 23:20:54,955 INFO 0x00000080: 53 3d 22 67 72 61 6d 73 2f 63 75 62 69 63 20 63 65 6e 74 69 6d 65 74 65 72 22 3e 3c 21 2d 2d 20
2022-08-13 23:20:54,955 INFO 0x000000a0: 41 74 20 33 30 30 4b 20 2d 2d 3e 0d 20 20 20 20 20 20 36 2e 36 39 0d 20 20 20 20 3c 2f 44 45 4e
2022-08-13 23:20:54,955 INFO 0x000000c0: 53 49 54 59 3e 0d 20 20 20 20 3c 45 4c 45 43 54 52 4f 4e 5f 43 4f 4e 46 49 47 55 52 41 54 49 4f
2022-08-13 23:20:54,955 INFO 0x000000e0: 4e 3e 5b 4b 72 5d 20 34 64 31 30 20 35 73 32 20 70 33 20 3c 2f 45 4c 45 43 54 52 4f 4e 5f 43 4f
2022-08-13 23:20:54,982 ERROR Traceback (most recent call last):



    // if unexpected chip reset occurred, then write reset record to nor
    if (bsp_platform::check_unexpected_chip_reset()) {
        pr_info("Waring!!! it is reset\n");
        u32 nor_addr = CFG_NOR_RESET_RECORD_START;
        u32 nor_data = (u32)(0<<24 | reset_value);
        u32 *data_ptr  = (u32 *)(&nor_data);
        NOR_OPERATION_STATUS_E job_status = (NOR_OPERATION_STATUS_E)bsp_platform::spi_nor_write_data(nor_addr, data_ptr, sizeof(nor_data));
        if (job_status != NOR_OPERATION_SUCCESS) {
            pr_info("%s, reset flag write nor failed\n", __FUNCTION__);
        }
    }
    
    
    
        nor_addr = GET_PANIC_DUMP_NOR_ADDR(type, panic_dump_info_t, magic_dump_start);
        buf_addr = (u32 *)(&_panic_dump_info[type]);
        bsp_platform::spi_nor_indirect_read_data(nor_addr, buf_addr, NOR_ACCESS_LEN(sizeof(panic_dump_info_t)));
        PLAIN_LOG("read nor addr %x, got magic st %x\n", nor_addr, _panic_dump_info[type].magic_dump_start);
        if (_panic_dump_info[type].magic_dump_start == PANIC_DUMP_START) {
            if (_panic_dump_info[type].magic_dump_end == PANIC_DUMP_END) {
                file_size = _panic_dump_info[type]._len_of_dw * 4;
                pr_dbg("find a complete panic dump, type %d, file len 0x%x\n", type, file_size);
                if (type == PANIC_FILE_NOR) {
                    _cache_size = MIN(file_size, _cache_size);
                    _cache_size = ROUND_UP(_cache_size, DATA_LEN_64K);
                    file_size   = _cache_size;
                }
            }
            else {
                PLAIN_LOG("find a incomplete panic dump, type %d\n", type);
            }

            _has_valid_dump = true;
        }
        else {
            PLAIN_LOG("no valid panic dump, type %d\n", type);
        }
```

