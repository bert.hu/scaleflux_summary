### 获取qid

```c++
    // current only check b2n queue
    for (u32 stream = 0; stream < STREAM_ID_MAX; stream++) {
        u32 qid = g_b2n_qid_of_stream[stream];
        if (FCQ_B2N_RFS == qid) {
            continue;
        }
        if (bsp_platform::ace_fcq_get_ptr(QUEUE_TYPE_CQ, qid, true) != bsp_platform::ace_fcq_get_ptr(QUEUE_TYPE_CQ, qid, false)) {
            return false;
        }
    }
```

```mermaid
graph LR;
　　pf_dump_task-->|process_write_pending|hw_aborter.abort_hw;
　　pf_dump_task-->|handle_b2n_cpl中收集data|data_flusher.flush_user_data;
　　pf_dump_task-->|data_collector收集data|data_flusher.flush_parity_data;
　　pf_dump_task-->|PADDING_0 phase|data_flusher.parity_padding_0;
　　pf_dump_task-->|收集准备meta数据|data_collector.prepare_meta_data;
　　pf_dump_task-->|下刷meta数据|data_flusher.flush_meta_data;
　　pf_dump_task-->|下刷summary数据|data_flusher.flush_summary_data;
　　pf_dump_task-->|PADDING_1 phase|data_flusher.parity_padding_1;
　　pf_dump_task-->|等待所有write命令完成|g_pf_mgr.is_all_pending_write_cmd_cpl;
　　pf_dump_task-->|下刷root data到nor flash|data_flusher.save_root_data;
　　pf_dump_task-->|最后设置flag|set_pf_handled;
```

### flush_user_data

```txt
_flush_stream, 从0-7，即STREAM_ID_HOT --> STREAM_ID_L2P_PATCH
flush_cnt:代表该stream有多少笔需要下刷的pending flushed pba，我理解是w2p，每4k为一个单位
_flush_index， 从0-flush_cnt，如果是SLC的，就是4K一笔，但如果是TLC的，就要考虑page type
graph LR;
_flush_page_type，通过_flush_page_type=0-2来获取vqid，然后申请b2n，将data vqid write to 申请的b2n的pba，并记录到target_pba里面，这样就完成了从data_collector中搜集到的user data通过b2n写到了nand，并记录了origin pba和target pba
```

### flush_parity_data

```txt
_flush_stream, 从0-7，即STREAM_ID_HOT --> STREAM_ID_L2P_PATCH
flush_cnt:代表该stream有多少笔需要下刷的pending flushed pba，我理解是w2p，每4k为一个单位
_flush_index， 从0-flush_cnt，如果是SLC的，就是4K一笔，但如果是TLC的，就要考虑page type
graph LR;
act_pu_b2n->partial_parity_group：用来控制是否需要将parity info写到parity buffer中，true才需要
_flush_page_type，通过_flush_page_type=0-2来获取parity_vqid，然后申请b2n，将Parity vqid write to 申请的b2n的pba，并记录到target_pba里面，这样就完成了从data_collector中搜集到的Parity data通过b2n写到了nand，并记录了origin pba和target pba
```

### parity_padding

```txt
首先对每个4K 全都padding 0
如果成功提交一次b2n，则_flush_index++
需要检查是否处在parity boundary
_chunk_cnt = _flush_index  * (CFG_ONE_PROG_NUM / CFG_PAGE_TYPE_NUM (16)),指的是发多少个16*4K数据，即多少个16*w2p
```

### flush_meta_data

```txt
_flush_index写满到PF_META_DATA_PBA_CNT = 40为止
否则g_pf_mgr.pf_w2p_submit
```

### flush_summary_data

```txt
目的是Dump pf summary to 申请的b2n pba里面
发到_chunk_cnt的w2p为止，即_flush_index = _chunk_cnt
(u8*)g_pf_mgr._pf_summary_data_rw_buf_start + (_flush_index * DATA_LEN_4K)
```

### save_root_data

```txt
NOR_SECTOR_SIZE的大小是64k
NOR_SUB_SECTOR_SIZE的大小是4k
ping_base_nor_addr和pong_base_nor_addr为主备

```

lspci -d cc53: -vvv

![image-20220704124841647](C:\Users\wudahht\AppData\Roaming\Typora\typora-user-images\image-20220704124841647.png)

Samanea_Scripts\AliCase\072_TR_PowerCycleAfterBigRangeTrim.py
